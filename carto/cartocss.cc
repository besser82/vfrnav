//
// C++ Implementation: cartocss
//
// Description: CartoCSS
//
//
// Author: Thomas Sailer <t.sailer@alumni.ethz.ch>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glibmm.h>
#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <cmath>

#include "geom.h"
#include "cartocss.h"

const std::string& to_str(CartoCSS::compositeop_t x)
{
	switch (x) {
	case CartoCSS::compositeop_t::clear:
	{
		static const std::string r("clear");
		return r;
	}

	case CartoCSS::compositeop_t::src:
	{
		static const std::string r("src");
		return r;
	}

	case CartoCSS::compositeop_t::dst:
	{
		static const std::string r("dst");
		return r;
	}

	case CartoCSS::compositeop_t::srcover:
	{
		static const std::string r("srcover");
		return r;
	}

	case CartoCSS::compositeop_t::dstover:
	{
		static const std::string r("dstover");
		return r;
	}

	case CartoCSS::compositeop_t::srcin:
	{
		static const std::string r("srcin");
		return r;
	}

	case CartoCSS::compositeop_t::dstin:
	{
		static const std::string r("dstin");
		return r;
	}

	case CartoCSS::compositeop_t::srcout:
	{
		static const std::string r("srcout");
		return r;
	}

	case CartoCSS::compositeop_t::dstout:
	{
		static const std::string r("dstout");
		return r;
	}

	case CartoCSS::compositeop_t::srcatop:
	{
		static const std::string r("srcatop");
		return r;
	}

	case CartoCSS::compositeop_t::dstatop:
	{
		static const std::string r("dstatop");
		return r;
	}

	case CartoCSS::compositeop_t::xor_:
	{
		static const std::string r("xor_");
		return r;
	}

	case CartoCSS::compositeop_t::plus:
	{
		static const std::string r("plus");
		return r;
	}

	case CartoCSS::compositeop_t::minus:
	{
		static const std::string r("minus");
		return r;
	}

	case CartoCSS::compositeop_t::multiply:
	{
		static const std::string r("multiply");
		return r;
	}

	case CartoCSS::compositeop_t::screen:
	{
		static const std::string r("screen");
		return r;
	}

	case CartoCSS::compositeop_t::overlay:
	{
		static const std::string r("overlay");
		return r;
	}

	case CartoCSS::compositeop_t::darken:
	{
		static const std::string r("darken");
		return r;
	}

	case CartoCSS::compositeop_t::lighten:
	{
		static const std::string r("lighten");
		return r;
	}

	case CartoCSS::compositeop_t::colordodge:
	{
		static const std::string r("colordodge");
		return r;
	}

	case CartoCSS::compositeop_t::colorburn:
	{
		static const std::string r("colorburn");
		return r;
	}

	case CartoCSS::compositeop_t::hardlight:
	{
		static const std::string r("hardlight");
		return r;
	}

	case CartoCSS::compositeop_t::softlight:
	{
		static const std::string r("softlight");
		return r;
	}

	case CartoCSS::compositeop_t::difference:
	{
		static const std::string r("difference");
		return r;
	}

	case CartoCSS::compositeop_t::exclusion:
	{
		static const std::string r("exclusion");
		return r;
	}

	case CartoCSS::compositeop_t::contrast:
	{
		static const std::string r("contrast");
		return r;
	}

	case CartoCSS::compositeop_t::invert:
	{
		static const std::string r("invert");
		return r;
	}

	case CartoCSS::compositeop_t::invertrgb:
	{
		static const std::string r("invertrgb");
		return r;
	}

	case CartoCSS::compositeop_t::grainmerge:
	{
		static const std::string r("grainmerge");
		return r;
	}

	case CartoCSS::compositeop_t::grainextract:
	{
		static const std::string r("grainextract");
		return r;
	}

	case CartoCSS::compositeop_t::hue:
	{
		static const std::string r("hue");
		return r;
	}

	case CartoCSS::compositeop_t::saturation:
	{
		static const std::string r("saturation");
		return r;
	}

	case CartoCSS::compositeop_t::color:
	{
		static const std::string r("color");
		return r;
	}

	case CartoCSS::compositeop_t::value:
	{
		static const std::string r("value");
		return r;
	}

	case CartoCSS::compositeop_t::invalid:
	{
		static const std::string r("invalid");
		return r;
	}

	default:
	{
		static const std::string r("??");
		return r;
	}
	}
}

const std::string& to_str(CartoCSS::placement_t x)
{
	switch (x) {
	case CartoCSS::placement_t::line:
	{
		static const std::string r("line");
		return r;
	}

	case CartoCSS::placement_t::point:
	{
		static const std::string r("point");
		return r;
	}

	case CartoCSS::placement_t::interior:
	{
		static const std::string r("interior");
		return r;
	}

	case CartoCSS::placement_t::invalid:
	{
		static const std::string r("invalid");
		return r;
	}

	default:
	{
		static const std::string r("??");
		return r;
	}
	}
}

const std::string& to_str(CartoCSS::variable_t var)
{
	switch (var) {
	case CartoCSS::variable_t::compop:			{ static const std::string r("comp-op"); return r; }
	case CartoCSS::variable_t::imagefilters:		{ static const std::string r("image-filters"); return r; }
	case CartoCSS::variable_t::opacity:			{ static const std::string r("opacity"); return r; }
	case CartoCSS::variable_t::mapbackgroundcolor:		{ static const std::string r("background-color"); return r; }
	case CartoCSS::variable_t::linecap:			{ static const std::string r("line-cap"); return r; }
	case CartoCSS::variable_t::lineclip:			{ static const std::string r("line-clip"); return r; }
	case CartoCSS::variable_t::linecolor:			{ static const std::string r("line-color"); return r; }
	case CartoCSS::variable_t::linedasharray:		{ static const std::string r("line-dasharray"); return r; }
	case CartoCSS::variable_t::linejoin:			{ static const std::string r("line-join"); return r; }
	case CartoCSS::variable_t::lineoffset:			{ static const std::string r("line-offset"); return r; }
	case CartoCSS::variable_t::lineopacity:			{ static const std::string r("line-opacity"); return r; }
	case CartoCSS::variable_t::linesimplify:		{ static const std::string r("line-simplify"); return r; }
	case CartoCSS::variable_t::linesimplifyalgorithm:	{ static const std::string r("line-simplify-algorithm"); return r; }
	case CartoCSS::variable_t::linewidth:			{ static const std::string r("line-width"); return r; }
	case CartoCSS::variable_t::linepatternfile:		{ static const std::string r("line-pattern-file"); return r; }
	case CartoCSS::variable_t::markerallowoverlap:		{ static const std::string r("marker-allow-overlap"); return r; }
	case CartoCSS::variable_t::markerclip:			{ static const std::string r("marker-clip"); return r; }
	case CartoCSS::variable_t::markerfile:			{ static const std::string r("marker-file"); return r; }
	case CartoCSS::variable_t::markerfill:			{ static const std::string r("marker-fill"); return r; }
	case CartoCSS::variable_t::markerheight:		{ static const std::string r("marker-height"); return r; }
	case CartoCSS::variable_t::markerignoreplacement:	{ static const std::string r("marker-ignore-placement"); return r; }
	case CartoCSS::variable_t::markerlinecolor:		{ static const std::string r("marker-line-color"); return r; }
	case CartoCSS::variable_t::markerlinewidth:		{ static const std::string r("marker-line-width"); return r; }
	case CartoCSS::variable_t::markermaxerror:		{ static const std::string r("marker-max-error"); return r; }
	case CartoCSS::variable_t::markeropacity:		{ static const std::string r("marker-opacity"); return r; }
	case CartoCSS::variable_t::markerplacement:		{ static const std::string r("marker-placement"); return r; }
	case CartoCSS::variable_t::markerspacing:		{ static const std::string r("marker-spacing"); return r; }
	case CartoCSS::variable_t::markerwidth:			{ static const std::string r("marker-width"); return r; }
	case CartoCSS::variable_t::polygonclip:			{ static const std::string r("polygon-clip"); return r; }
	case CartoCSS::variable_t::polygonfill:			{ static const std::string r("polygon-fill"); return r; }
	case CartoCSS::variable_t::polygongamma:		{ static const std::string r("polygon-gamma"); return r; }
	case CartoCSS::variable_t::polygonpatternalignment:	{ static const std::string r("polygon-pattern-alignment"); return r; }
	case CartoCSS::variable_t::polygonpatternfile:		{ static const std::string r("polygon-pattern-file"); return r; }
	case CartoCSS::variable_t::polygonpatterngamma:		{ static const std::string r("polygon-pattern-gamma"); return r; }
	case CartoCSS::variable_t::polygonpatternopacity:	{ static const std::string r("polygon-pattern-opacity"); return r; }
	case CartoCSS::variable_t::shieldclip:			{ static const std::string r("shield-clip"); return r; }
	case CartoCSS::variable_t::shieldfacename:		{ static const std::string r("shield-face-name"); return r; }
	case CartoCSS::variable_t::shieldfile:			{ static const std::string r("shield-file"); return r; }
	case CartoCSS::variable_t::shieldfill:			{ static const std::string r("shield-fill"); return r; }
	case CartoCSS::variable_t::shieldhalofill:		{ static const std::string r("shield-halo-fill"); return r; }
	case CartoCSS::variable_t::shieldhaloradius:		{ static const std::string r("shield-halo-radius"); return r; }
	case CartoCSS::variable_t::shieldhorizontalalignment:	{ static const std::string r("shield-horizontal-alignment"); return r; }
	case CartoCSS::variable_t::shieldlinespacing:		{ static const std::string r("shield-line-spacing"); return r; }
	case CartoCSS::variable_t::shieldmargin:		{ static const std::string r("shield-margin"); return r; }
	case CartoCSS::variable_t::shieldname:			{ static const std::string r("shield-name"); return r; }
	case CartoCSS::variable_t::shieldplacement:		{ static const std::string r("shield-placement"); return r; }
	case CartoCSS::variable_t::shieldplacementtype:		{ static const std::string r("shield-placement-type"); return r; }
	case CartoCSS::variable_t::shieldplacements:		{ static const std::string r("shield-placements"); return r; }
	case CartoCSS::variable_t::shieldrepeatdistance:	{ static const std::string r("shield-repeat-distance"); return r; }
	case CartoCSS::variable_t::shieldsize:			{ static const std::string r("shield-size"); return r; }
	case CartoCSS::variable_t::shieldspacing:		{ static const std::string r("shield-spacing"); return r; }
	case CartoCSS::variable_t::shieldtextdx:		{ static const std::string r("shield-text-dx"); return r; }
	case CartoCSS::variable_t::shieldtextdy:		{ static const std::string r("shield-text-dy"); return r; }
	case CartoCSS::variable_t::shieldunlockimage:		{ static const std::string r("shield-unlock-image"); return r; }
	case CartoCSS::variable_t::shieldverticalalignment:	{ static const std::string r("shield-vertical-alignment"); return r; }
	case CartoCSS::variable_t::shieldwrapwidth:		{ static const std::string r("shield-wrap-width"); return r; }
	case CartoCSS::variable_t::textcharacterspacing:	{ static const std::string r("text-character-spacing"); return r; }
	case CartoCSS::variable_t::textclip:			{ static const std::string r("text-clip"); return r; }
	case CartoCSS::variable_t::textdx:			{ static const std::string r("text-dx"); return r; }
	case CartoCSS::variable_t::textdy:			{ static const std::string r("text-dy"); return r; }
	case CartoCSS::variable_t::textfacename:		{ static const std::string r("text-face-name"); return r; }
	case CartoCSS::variable_t::textfill:			{ static const std::string r("text-fill"); return r; }
	case CartoCSS::variable_t::texthalofill:		{ static const std::string r("text-halo-fill"); return r; }
	case CartoCSS::variable_t::texthaloradius:		{ static const std::string r("text-halo-radius"); return r; }
	case CartoCSS::variable_t::texthorizontalalignment:	{ static const std::string r("text-horizontal-alignment"); return r; }
	case CartoCSS::variable_t::textlargestbboxonly:		{ static const std::string r("text-largest-bbox-only"); return r; }
	case CartoCSS::variable_t::textlinespacing:		{ static const std::string r("text-line-spacing"); return r; }
	case CartoCSS::variable_t::textmargin:			{ static const std::string r("text-margin"); return r; }
	case CartoCSS::variable_t::textmaxcharangledelta:	{ static const std::string r("text-max-char-angle-delta"); return r; }
	case CartoCSS::variable_t::textmindistance:		{ static const std::string r("text-min-distance"); return r; }
	case CartoCSS::variable_t::textname:			{ static const std::string r("text-name"); return r; }
	case CartoCSS::variable_t::textopacity:			{ static const std::string r("text-opacity"); return r; }
	case CartoCSS::variable_t::textplacement:		{ static const std::string r("text-placement"); return r; }
	case CartoCSS::variable_t::textplacementtype:		{ static const std::string r("text-placement-type"); return r; }
	case CartoCSS::variable_t::textplacements:		{ static const std::string r("text-placements"); return r; }
	case CartoCSS::variable_t::textrepeatdistance:		{ static const std::string r("text-repeat-distance"); return r; }
	case CartoCSS::variable_t::textsize:			{ static const std::string r("text-size"); return r; }
	case CartoCSS::variable_t::textspacing:			{ static const std::string r("text-spacing"); return r; }
	case CartoCSS::variable_t::textupright:			{ static const std::string r("text-upright"); return r; }
	case CartoCSS::variable_t::textverticalalignment:	{ static const std::string r("text-vertical-alignment"); return r; }
	case CartoCSS::variable_t::textwrapcharacter:		{ static const std::string r("text-wrap-character"); return r; }
	case CartoCSS::variable_t::textwrapwidth:		{ static const std::string r("text-wrap-width"); return r; };
	default:						{ static const std::string r("?"); return r; };
	}
}

CartoCSS::Color::Color(void)
	: m_r(std::numeric_limits<double>::quiet_NaN()), m_g(std::numeric_limits<double>::quiet_NaN()),
	  m_b(std::numeric_limits<double>::quiet_NaN()), m_a(std::numeric_limits<double>::quiet_NaN())
{
}

CartoCSS::Color::Color(double r, double g, double b, double a)
	: m_r(r), m_g(g), m_b(b), m_a(a)
{
}

bool CartoCSS::Color::is_valid(void) const
{
	return !(std::isnan(m_r) || std::isnan(m_g) || std::isnan(m_b) || std::isnan(m_a));
}

double CartoCSS::Color::clamp(double x)
{
	return std::max(std::min(x, 1.0), 0.0);
}

CartoCSS::Color CartoCSS::Color::clamp(void) const
{
	return Color(clamp(m_r), clamp(m_g), clamp(m_b), clamp(m_a));
}

void CartoCSS::Color::set_rgba_uint8(uint8_t r, uint8_t g, uint8_t b, uint8_t a)
{
	static constexpr double m = 1.0 / 255.0;
	m_r = r * m;
	m_g = g * m;
	m_b = b * m;
	m_a = a * m;
}

void CartoCSS::Color::set_rgba_uint4(uint8_t r, uint8_t g, uint8_t b, uint8_t a)
{
	static constexpr double m = 1.0 / 15.0;
	m_r = (r & 15) * m;
	m_g = (g & 15) * m;
	m_b = (b & 15) * m;
	m_a = (a & 15) * m;
}

double CartoCSS::Color::hslvalue(double n1, double n2, double hue)
{
	if (hue > 6)
		hue -= 6;
	else if (hue < 0)
		hue += 6;
	if (hue < 1)
		return n1 + (n2 - n1) * hue;
	if (hue < 3)
		return n2;
	if (hue < 4)
		return n1 + (n2 - n1) * (4 - hue);
	return n1;
}

void CartoCSS::Color::set_hsl(double h, double s, double l)
{
	if (std::isnan(h) || std::isnan(s) || std::isnan(l)) {
		m_r = m_g = m_b = std::numeric_limits<double>::quiet_NaN();
		return;
	}
	s = std::max(std::min(s, 1.0), 0.0);
	l = std::max(std::min(l, 1.0), 0.0);
	if (s == 0) {
		m_r = m_g = m_b = l;
		return;
	}
	double m2;
	double h1(modf(h * (1.0 / 360), &m2));
	h1 *= 6;
	if (l <= 0.5)
		m2 = l * (1 + s);
	else
		m2 = l + s - l * s;
	double m1(2 * l - m2);
	m_r = hslvalue(m1, m2, h1+2);
	m_g = hslvalue(m1, m2, h1);
	m_b = hslvalue(m1, m2, h1-2);
	if (false)
		std::cerr << "h " << h << " s " << s << " l " << l << " h1 " << h1 << " m1 " << m1 << " m2 " << m2
			  << " r " << m_r << " g " << m_g << " b " << m_b << std::endl;
}

void CartoCSS::Color::get_hsl(double& h, double& s, double& l)
{
	double xmax(m_r);
	int wxmax(0);
	if (m_g > xmax) {
		xmax = m_g;
		wxmax = 1;
	}
	if (m_b > xmax) {
		xmax = m_b;
		wxmax = 2;
	}
	double xmin(std::min(std::min(m_r, m_g), m_b));
	double c(xmax - xmin);
	if (c == 0) {
		h = 0;
	} else {
		switch (wxmax) {
		default:
			h = 60 * (0 + (m_g - m_b) / c);
			if (h < 0)
				h += 360;
			break;

		case 1:
			h = 60 * (2 + (m_b - m_r) / c);
			break;

		case 2:
			h = 60 * (4 + (m_r - m_g) / c);
			break;
		}
	}
	double l1(0.5 * (xmax + xmin));
	l = l1;
	if (l1 <= 0 || l1 >= 1)
		s = 0;
	else
		s = (xmax - l1) / std::min(l1, 1 - l1);
}

std::string CartoCSS::Color::to_str(void) const
{
	std::ostringstream oss;
	oss << "rgba(" << std::setprecision(3) << get_red() << ','
	    << get_green() << ',' << get_blue() << ',' << get_alpha() << ')';
	return oss.str();
}

std::string CartoCSS::Color::to_mss_str(void) const
{
	std::ostringstream oss;
	oss << "rgba(" << static_cast<unsigned int>(get_red() * 255) << ','
	    << static_cast<unsigned int>(get_green() * 255) << ','
	    << static_cast<unsigned int>(get_blue() * 255) << ','
	    << std::setprecision(3) << get_alpha() << ')';
	return oss.str();
}

CartoCSS::LayerAllocator::LayerAllocator(void)
	: m_nextlayer(layer_t::first)
{
}
		
CartoCSS::LayerAllocator::layer_t CartoCSS::LayerAllocator::allocate(const std::string& name)
{
	std::pair<layermap_t::iterator,bool> ins(m_layermap.insert(layermap_t::value_type(name, m_nextlayer)));
	if (ins.second) {
		layer_t x(inc(m_nextlayer));
		if (x == layer_t::invalid)
			throw std::runtime_error("too many layers");
		m_nextlayer = x;
	}
	return ins.first->second;
}

CartoCSS::LayerAllocator::layer_t CartoCSS::LayerAllocator::get_default(void) const
{
	layermap_t::const_iterator i(m_layermap.find(""));
	if (i == m_layermap.end())
		return layer_t::invalid;
	return i->second;
}

const std::string& CartoCSS::LayerAllocator::get_name(layer_t l) const
{
	for (const auto& x : m_layermap)
		if (x.second == l)
			return x.first;
	throw std::runtime_error("invalid layer");
}

CartoCSS::PrefixAllocator::PrefixAllocator(void)
	: m_nextprefix(prefix_t::first)
{
}
		
CartoCSS::PrefixAllocator::prefix_t CartoCSS::PrefixAllocator::allocate(const std::string& name)
{
	std::pair<prefixmap_t::iterator,bool> ins(m_prefixmap.insert(prefixmap_t::value_type(name, m_nextprefix)));
	if (ins.second) {
		prefix_t x(inc(m_nextprefix));
		if (x == prefix_t::invalid)
			throw std::runtime_error("too many prefixes");
		m_nextprefix = x;
	}
	return ins.first->second;
}

CartoCSS::PrefixAllocator::prefix_t CartoCSS::PrefixAllocator::get_default(void) const
{
	prefixmap_t::const_iterator i(m_prefixmap.find(""));
	if (i == m_prefixmap.end())
		return prefix_t::invalid;
	return i->second;
}

const std::string& CartoCSS::PrefixAllocator::get_name(prefix_t l) const
{
	for (const auto& x : m_prefixmap)
		if (x.second == l)
			return x.first;
	throw std::runtime_error("invalid prefix");
}

CartoCSS::Value::ValueError::ValueError(const std::string& x)
	: std::runtime_error(x)
{
}

CartoCSS::Value::Value(void)
	: m_type(type_t::null)
{
}

CartoCSS::Value::Value(const std::string& val)
	: m_type(type_t::string)
{
	::new(&m_string) string_t(val);
}

CartoCSS::Value::Value(const char *val)
	: m_type(type_t::null)
{
	if (!val)
		return;
	m_type = type_t::string;
	::new(&m_string) string_t(val);
}

CartoCSS::Value::Value(double val)
	: m_double(val), m_type(type_t::floatingpoint)
{
}
	
CartoCSS::Value::Value(int64_t val)
	: m_int(val), m_type(type_t::integer)
{
}

CartoCSS::Value::Value(bool val)
	: m_bool(val), m_type(type_t::boolean)
{
}

CartoCSS::Value::Value(const Value& v)
	: m_type(type_t::null)
{
	operator=(v);
}

CartoCSS::Value::Value(Value&& v)
	: m_type(type_t::null)
{
	operator=(v);
}

CartoCSS::Value::~Value()
{
	dealloc();
}

void CartoCSS::Value::dealloc(void)
{
	switch (m_type) {
	case type_t::string:
		m_string.~string_t();
		break;

	default:
		break;
	}
	m_type = type_t::null;
}

CartoCSS::Value& CartoCSS::Value::operator=(const std::string& v)
{
	dealloc();
	m_type = type_t::string;
	::new(&m_string) string_t(v);
	return *this;
}

CartoCSS::Value& CartoCSS::Value::operator=(const char *v)
{
	dealloc();
	if (!v)
		return *this;
	m_type = type_t::string;
	::new(&m_string) string_t(v);
	return *this;
}

CartoCSS::Value& CartoCSS::Value::operator=(double v)
{
	dealloc();
	m_type = type_t::floatingpoint;
	m_double = v;
	return *this;
}

CartoCSS::Value& CartoCSS::Value::operator=(int64_t v)
{
	dealloc();
	m_type = type_t::integer;
	m_int = v;
	return *this;
}

CartoCSS::Value& CartoCSS::Value::operator=(bool v)
{
	dealloc();
	m_type = type_t::boolean;
	m_bool = v;
	return *this;
}

CartoCSS::Value& CartoCSS::Value::operator=(const Value& v)
{
	dealloc();
	m_type = v.m_type;
	switch (m_type) {
	case type_t::string:
		::new(&m_string) string_t(v.m_string);
		break;

	case type_t::floatingpoint:
		m_double = v.m_double;
		break;

	case type_t::integer:
		m_int = v.m_int;
		break;

	case type_t::boolean:
		m_bool = v.m_bool;
		break;

	default:
		break;
	}
	return *this;
}

CartoCSS::Value& CartoCSS::Value::operator=(Value&& v)
{
	dealloc();
	m_type = v.m_type;
	switch (m_type) {
	case type_t::string:
		::new(&m_string) string_t();
		m_string.swap(v.m_string);
		break;

	case type_t::floatingpoint:
		m_double = v.m_double;
		break;

	case type_t::integer:
		m_int = v.m_int;
		break;

	case type_t::boolean:
		m_bool = v.m_bool;
		break;

	default:
		break;
	}
	return *this;
}

int CartoCSS::Value::compare(const Value& x) const
{
	// null comes first
	if (m_type == type_t::null)
		return (x.m_type == type_t::null) ? 0 : -1;
	if (x.m_type == type_t::null)
		return 1;
	if (m_type == x.m_type) {
		switch (m_type) {
		case type_t::null:
			return 0;

		case type_t::string:
			return m_string.compare(x.m_string);

		case type_t::floatingpoint:
			if (m_double < x.m_double)
				return -1;
			if (x.m_double < m_double)
				return 1;
			return 0;

		case type_t::integer:
			if (m_int < x.m_int)
				return -1;
			if (x.m_int < m_int)
				return 1;
			return 0;

		case type_t::boolean:
			if (m_bool < x.m_bool)
				return -1;
			if (x.m_bool < m_bool)
				return 1;
			return 0;
		}
	}
	if (is_integral() && x.is_integral()) {
		int64_t v0(*this), v1(x);
		if (v0 < v1)
			return -1;
		if (v1 < v0)
			return 1;
		return 0;
	}
	if (is_numeric() && x.is_numeric()) {
		double v0(*this), v1(x);
		if (v0 < v1)
			return -1;
		if (v1 < v0)
			return 1;
		return 0;
	}
	std::string v0(*this), v1(x);
	if (v0 < v1)
		return -1;
	if (v1 < v0)
		return 1;
	return 0;
}

CartoCSS::Value::operator bool(void) const
{
	switch (m_type) {
	case type_t::null:
		throw ValueError("operator bool: type is null");

	case type_t::string:
		if (m_string == "true")
			return true;
		if (m_string == "false")
			return false;
		throw ValueError("operator bool: type is string (" + m_string + ")");

	case type_t::floatingpoint:
		return !!m_double;

	case type_t::integer:
		return !!m_int;

	case type_t::boolean:
		return m_bool;
	}
	throw ValueError("operator bool: unhandled type");
}

CartoCSS::Value::operator int64_t(void) const
{
	switch (m_type) {
	case type_t::null:
		throw ValueError("operator int64_t: type is null");

	case type_t::string:
	{
		char *cp(0);
		int64_t v(strtoll(m_string.c_str(), &cp, 10));
		if (*cp)
			throw ValueError("operator int64_t: type is string (" + m_string + ")");
		return v;
	}

	case type_t::floatingpoint:
		return m_double;

	case type_t::integer:
		return m_int;

	case type_t::boolean:
		return m_bool;
	}
	throw ValueError("operator int64_t: unhandled type");
}

CartoCSS::Value::operator double(void) const
{
	switch (m_type) {
	case type_t::null:
		throw ValueError("operator double: type is null");

	case type_t::string:
	{
		char *cp(0);
		double v(strtod(m_string.c_str(), &cp));
		if (*cp)
			throw ValueError("operator double: type is string (" + m_string + ")");
		return v;
	}

	case type_t::floatingpoint:
		return m_double;

	case type_t::integer:
		return m_int;

	case type_t::boolean:
		return m_bool;
	}
	throw ValueError("operator double: unhandled type");
}

CartoCSS::Value::operator std::string(void) const
{
	switch (m_type) {
	case type_t::null:
		throw ValueError("operator string: type is null");

	case type_t::string:
		return m_string;

	case type_t::floatingpoint:
	{
		std::ostringstream v;
		v << m_double;
		return v.str();
	}

	case type_t::integer:
	{
		std::ostringstream v;
		v << m_int;
		return v.str();
	}

	case type_t::boolean:
		return m_bool ? "true" : "false";
	}
	throw ValueError("operator string: unhandled type");
}

CartoCSS::MarkerParams::MarkerParams(void)
	: m_fill(0, 0, 1, 1), m_linecolor(0, 0, 0, 1), m_height(10), m_linewidth(0.5), m_maxerror(0.2),
	  m_opacity(1.0), m_spacing(100), m_width(10), m_placement(placement_t::point), m_allowoverlap(false),
	  m_clip(false), m_ignoreplacement(false)
{
}

bool CartoCSS::MarkerParams::set_variable(variable_t var, const Expr::const_ptr_t& expr)
{
	if (!expr)
		return false;
	switch (var) {
	case variable_t::markerplacement:
	{
		Expr::keyword_t kw(expr->as_keyword());
		placement_t placement(placement_t::invalid);
		switch (kw) {
		case Expr::keyword_t::line:
			placement = placement_t::line;
			break;

		case Expr::keyword_t::point:
			placement = placement_t::point;
			break;

		case Expr::keyword_t::interior:
			placement = placement_t::interior;
			break;

		default:
			throw std::runtime_error("invalid placement symbol " + to_str(kw));
		}
		if (placement == placement_t::invalid)
			return false;
		set_placement(placement);
		return true;
	}

	case variable_t::markerallowoverlap:
		set_allowoverlap(expr->as_bool());
		return true;

	case variable_t::markerclip:
		set_clip(expr->as_bool());
		return true;

	case variable_t::markerignoreplacement:
		set_ignoreplacement(expr->as_bool());
		return true;

	case variable_t::markerfill:
		set_fill(expr->as_color());
		return true;

	case variable_t::markerlinecolor:
		set_linecolor(expr->as_color());
		return true;

	case variable_t::markerheight:
		set_height(expr->as_double());
		return true;

	case variable_t::markerlinewidth:
		set_linewidth(expr->as_double());
		return true;

	case variable_t::markermaxerror:
		set_maxerror(expr->as_double());
		return true;

	case variable_t::markeropacity:
		set_opacity(expr->as_double());
		return true;

	case variable_t::markerspacing:
		set_spacing(expr->as_double());
		return true;

	case variable_t::markerwidth:
		set_width(expr->as_double());
		return true;

	case variable_t::markerfile:
		set_file(expr->as_url());
		return true;

	default:
		return false;
	}
}

std::ostream& CartoCSS::MarkerParams::print(std::ostream& os, unsigned int indent) const
{
	return os << std::string(indent, ' ') << "marker-fill " << get_fill().to_str() << std::endl
		  << std::string(indent, ' ') << "marker-linecolor " << get_linecolor().to_str() << std::endl
		  << std::string(indent, ' ') << "marker-file " << get_file() << std::endl
		  << std::string(indent, ' ') << "marker-height " << get_height() << std::endl
		  << std::string(indent, ' ') << "marker-linewidth " << get_linewidth() << std::endl
		  << std::string(indent, ' ') << "marker-maxerror " << get_maxerror() << std::endl
		  << std::string(indent, ' ') << "marker-opacity " << get_opacity() << std::endl
		  << std::string(indent, ' ') << "marker-spacing " << get_spacing() << std::endl
		  << std::string(indent, ' ') << "marker-width " << get_width() << std::endl
		  << std::string(indent, ' ') << "marker-placement " << get_placement() << std::endl
		  << std::string(indent, ' ') << "marker-allowoverlap " << get_allowoverlap() << std::endl
		  << std::string(indent, ' ') << "marker-clip " << get_clip() << std::endl
		  << std::string(indent, ' ') << "marker-ignoreplacement " << get_ignoreplacement() << std::endl;
}

std::ostream& CartoCSS::MarkerParams::print_mss(std::ostream& os, const std::string& pfx, unsigned int indent) const
{
	const char *sep = "/";
	sep += pfx.empty();
	return os << std::string(indent, ' ') << pfx << sep << "marker-fill: " << get_fill().to_mss_str() << ';' << std::endl
		  << std::string(indent, ' ') << pfx << sep << "marker-line-color: " << get_linecolor().to_mss_str() << ';' << std::endl
		  << std::string(indent, ' ') << pfx << sep << "marker-file: \"" << get_file() << "\";" << std::endl
		  << std::string(indent, ' ') << pfx << sep << "marker-height: " << get_height() << ';' << std::endl
		  << std::string(indent, ' ') << pfx << sep << "marker-line-width: " << get_linewidth() << ';' << std::endl
		  << std::string(indent, ' ') << pfx << sep << "marker-maxerror: " << get_maxerror() << ';' << std::endl
		  << std::string(indent, ' ') << pfx << sep << "marker-opacity: " << get_opacity() << ';' << std::endl
		  << std::string(indent, ' ') << pfx << sep << "marker-spacing: " << get_spacing() << ';' << std::endl
		  << std::string(indent, ' ') << pfx << sep << "marker-width: " << get_width() << ';' << std::endl
		  << std::string(indent, ' ') << pfx << sep << "marker-placement: " << get_placement() << ';' << std::endl
		  << std::string(indent, ' ') << pfx << sep << "marker-allow-overlap: " << get_allowoverlap() << ';' << std::endl
		  << std::string(indent, ' ') << pfx << sep << "marker-clip: " << get_clip() << ';' << std::endl
		  << std::string(indent, ' ') << pfx << sep << "marker-ignore-placement: " << get_ignoreplacement() << ';' << std::endl;
}

const std::string& to_str(CartoCSS::LineParams::cap_t x)
{
	switch (x) {
	case CartoCSS::LineParams::cap_t::butt:
	{
		static const std::string r("butt");
		return r;
	}
	
	case CartoCSS::LineParams::cap_t::round:
	{
		static const std::string r("round");
		return r;
	}

	case CartoCSS::LineParams::cap_t::square:
	{
		static const std::string r("square");
		return r;
	}

	case CartoCSS::LineParams::cap_t::invalid:
	{
		static const std::string r("invalid");
		return r;
	}

	default:
	{
		static const std::string r("??");
		return r;
	}
	}
}

const std::string& to_str(CartoCSS::LineParams::join_t x)
{
	switch (x) {
	case CartoCSS::LineParams::join_t::bevel:
	{
		static const std::string r("bevel");
		return r;
	}

	case CartoCSS::LineParams::join_t::miter:
	{
		static const std::string r("miter");
		return r;
	}

	case CartoCSS::LineParams::join_t::round:
	{
		static const std::string r("round");
		return r;
	}

	case CartoCSS::LineParams::join_t::invalid:
	{
		static const std::string r("invalid");
		return r;
	}

	default:
	{
		static const std::string r("??");
		return r;
	}
	}
}

const std::string& to_str(CartoCSS::LineParams::simplifyalgo_t x)
{
	switch (x) {
	case CartoCSS::LineParams::simplifyalgo_t::visvalingamwhyatt:
	{
		static const std::string r("visvalingamwhyatt");
		return r;
	}

	case CartoCSS::LineParams::simplifyalgo_t::invalid:
	{
		static const std::string r("invalid");
		return r;
	}

	default:
	{
		static const std::string r("??");
		return r;
	}
	}
}

CartoCSS::LineParams::LineParams(void)
	: m_color(0, 0, 0, 1), m_offset(0), m_opacity(1), m_simplify(0.0), m_width(1.0),
	  m_cap(cap_t::butt), m_join(join_t::miter), m_simplifyalgo(simplifyalgo_t::invalid), m_clip(false)
{
}

bool CartoCSS::LineParams::set_variable(variable_t var, const Expr::const_ptr_t& expr)
{
	if (!expr)
		return false;
	switch (var) {
	case variable_t::linecap:
	{
		Expr::keyword_t kw(expr->as_keyword());
		switch (kw) {
		case Expr::keyword_t::round:
			set_cap(LineParams::cap_t::round);
			return true;

		case Expr::keyword_t::square:
			set_cap(LineParams::cap_t::square);
			return true;

		case Expr::keyword_t::butt:
			set_cap(LineParams::cap_t::butt);
			return true;

		default:
			throw std::runtime_error("invalid line-cap symbol " + to_str(kw));
		}
		return false;;
	}

	case variable_t::linejoin:
	{
		Expr::keyword_t kw(expr->as_keyword());
		switch (kw) {
		case Expr::keyword_t::round:
			set_join(LineParams::join_t::round);
			return true;

		case Expr::keyword_t::miter:
			set_join(LineParams::join_t::miter);
			return true;

		case Expr::keyword_t::bevel:
			set_join(LineParams::join_t::bevel);
			return true;

		default:
			throw std::runtime_error("invalid line-join symbol " + to_str(kw));
		}
		return false;
	}

	case variable_t::linesimplifyalgorithm:
	{
		Expr::keyword_t kw(expr->as_keyword());
		switch (kw) {
		case Expr::keyword_t::visvalingamwhyatt:
			set_simplifyalgo(LineParams::simplifyalgo_t::visvalingamwhyatt);
			return true;

		default:
			throw std::runtime_error("invalid line-simplify-algorithm symbol " + to_str(kw));
		}
		return false;
	}

	case variable_t::lineclip:
		set_clip(expr->as_bool());
		return true;

	case variable_t::linecolor:
		set_color(expr->as_color());
		return true;

	case variable_t::linedasharray:
		set_dasharray(expr->as_doublelist());
		return true;

	case variable_t::lineoffset:
		set_offset(expr->as_double());
		return true;

	case variable_t::lineopacity:
		set_opacity(expr->as_double());
		return true;

	case variable_t::linesimplify:
		set_simplify(expr->as_double());
		return true;

	case variable_t::linewidth:
		set_width(expr->as_double());
		return true;

	default:
		return false;
	}
}

std::ostream& CartoCSS::LineParams::print(std::ostream& os, unsigned int indent) const
{
	os << std::string(indent, ' ') << "line-color " << get_color().to_str() << std::endl;
	{
	        os << std::string(indent, ' ') << "line-dasharray ";
		bool subseq(false);
		for (double v : get_dasharray()) {
			if (subseq)
				os << ',';
			subseq = true;
			os << v;
		}
		os << std::endl;
	}
	return os << std::string(indent, ' ') << "line-offset " << get_offset() << std::endl
		  << std::string(indent, ' ') << "line-opacity " << get_opacity() << std::endl
		  << std::string(indent, ' ') << "line-simplify " << get_simplify() << std::endl
		  << std::string(indent, ' ') << "line-width " << get_width() << std::endl
		  << std::string(indent, ' ') << "line-cap " << get_cap() << std::endl
		  << std::string(indent, ' ') << "line-join " << get_join() << std::endl
		  << std::string(indent, ' ') << "line-simplifyalgo " << get_simplifyalgo() << std::endl
		  << std::string(indent, ' ') << "line-clip " << get_clip() << std::endl;
	return os;
}

std::ostream& CartoCSS::LineParams::print_mss(std::ostream& os, const std::string& pfx, unsigned int indent) const
{
	const char *sep = "/";
	sep += pfx.empty();
	os << std::string(indent, ' ') << pfx << sep << "line-color: " << get_color().to_mss_str() << ';' << std::endl;
	{
	        os << std::string(indent, ' ') << pfx << sep << "line-dasharray: ";
		bool subseq(false);
		for (double v : get_dasharray()) {
			if (subseq)
				os << ',';
			subseq = true;
			os << v;
		}
		os << ';' << std::endl;
	}
	return os << std::string(indent, ' ') << pfx << sep << "line-offset: " << get_offset() << ';' << std::endl
		  << std::string(indent, ' ') << pfx << sep << "line-opacity: " << get_opacity() << ';' << std::endl
		  << std::string(indent, ' ') << pfx << sep << "line-simplify: " << get_simplify() << ';' << std::endl
		  << std::string(indent, ' ') << pfx << sep << "line-width: " << get_width() << ';' << std::endl
		  << std::string(indent, ' ') << pfx << sep << "line-cap: " << get_cap() << ';' << std::endl
		  << std::string(indent, ' ') << pfx << sep << "line-join: " << get_join() << ';' << std::endl
		  << std::string(indent, ' ') << pfx << sep << "line-simplify-algorithm: " << get_simplifyalgo() << ';' << std::endl
		  << std::string(indent, ' ') << pfx << sep << "line-clip: " << get_clip() << ';' << std::endl;
}

CartoCSS::LinePatternParams::LinePatternParams(void)
{
}

bool CartoCSS::LinePatternParams::set_variable(variable_t var, const Expr::const_ptr_t& expr)
{
	if (!expr)
		return false;
	switch (var) {
	case variable_t::linepatternfile:
		set_file(expr->as_url());
		return true;

	default:
		return false;
	}
}

std::ostream& CartoCSS::LinePatternParams::print(std::ostream& os, unsigned int indent) const
{
	return os << std::string(indent, ' ') << "line-pattern-file " << get_file() << std::endl;
}

std::ostream& CartoCSS::LinePatternParams::print_mss(std::ostream& os, const std::string& pfx, unsigned int indent) const
{
	const char *sep = "/";
	sep += pfx.empty();
	return os << std::string(indent, ' ') << pfx << sep << "line-pattern-file: \"" << get_file() << "\";" << std::endl;
}

CartoCSS::PolygonParams::PolygonParams(void)
	: m_fill(0.5, 0.5, 0.5, 1), m_gamma(1.0), m_clip(false)
{
}

bool CartoCSS::PolygonParams::set_variable(variable_t var, const Expr::const_ptr_t& expr)
{
	if (!expr)
		return false;
	switch (var) {
	case variable_t::polygonclip:
		set_clip(expr->as_bool());
		return true;

	case variable_t::polygonfill:
		set_fill(expr->as_color());
		return true;

	case variable_t::polygongamma:
		set_gamma(expr->as_double());
		return true;

	default:
		return false;
	}
}

std::ostream& CartoCSS::PolygonParams::print(std::ostream& os, unsigned int indent) const
{
	return os << std::string(indent, ' ') << "polygon-fill " << get_fill().to_str() << std::endl
		  << std::string(indent, ' ') << "polygon-gamma " << get_gamma() << std::endl
		  << std::string(indent, ' ') << "polygon-clip " << get_clip() << std::endl;
}

std::ostream& CartoCSS::PolygonParams::print_mss(std::ostream& os, const std::string& pfx, unsigned int indent) const
{
	const char *sep = "/";
	sep += pfx.empty();
	return os << std::string(indent, ' ') << pfx << sep << "polygon-fill: " << get_fill().to_mss_str() << ';' << std::endl
		  << std::string(indent, ' ') << pfx << sep << "polygon-gamma: " << get_gamma() << ';' << std::endl
		  << std::string(indent, ' ') << pfx << sep << "polygon-clip: " << get_clip() << ';' << std::endl;
}

const std::string& to_str(CartoCSS::PolygonPatternParams::alignment_t x)
{
	switch (x) {
	case CartoCSS::PolygonPatternParams::alignment_t::global:
	{
		static const std::string r("global");
		return r;
	}

	case CartoCSS::PolygonPatternParams::alignment_t::invalid:
	{
		static const std::string r("invalid");
		return r;
	}

	default:
	{
		static const std::string r("??");
		return r;
	}
	}
}

CartoCSS::PolygonPatternParams::PolygonPatternParams(void)
	: m_gamma(1.0), m_opacity(1.0), m_alignment(alignment_t::invalid)
{
}

bool CartoCSS::PolygonPatternParams::set_variable(variable_t var, const Expr::const_ptr_t& expr)
{
	if (!expr)
		return false;
	switch (var) {
	case variable_t::polygonpatternalignment:
	{
		Expr::keyword_t kw(expr->as_keyword());
		switch (kw) {
		case Expr::keyword_t::global:
			set_alignment(PolygonPatternParams::alignment_t::global);
			return true;

		default:
			throw std::runtime_error("invalid polygon-pattern-alignment symbol " + to_str(kw));
		}
		return false;
	}

	case variable_t::polygonpatterngamma:
		set_gamma(expr->as_double());
		return true;

	case variable_t::polygonpatternopacity:
		set_opacity(expr->as_double());
		return true;

	case variable_t::polygonpatternfile:
		set_file(expr->as_url());
		return true;

	default:
		return false;
	}
}

std::ostream& CartoCSS::PolygonPatternParams::print(std::ostream& os, unsigned int indent) const
{
	return os << std::string(indent, ' ') << "polygon-pattern-file " << get_file() << std::endl
		  << std::string(indent, ' ') << "polygon-pattern-gamma " << get_gamma() << std::endl
		  << std::string(indent, ' ') << "polygon-pattern-opacity " << get_opacity() << std::endl
		  << std::string(indent, ' ') << "polygon-pattern-alignment " << get_alignment() << std::endl;
}

std::ostream& CartoCSS::PolygonPatternParams::print_mss(std::ostream& os, const std::string& pfx, unsigned int indent) const
{
	const char *sep = "/";
	sep += pfx.empty();
	return os << std::string(indent, ' ') << pfx << sep << "polygon-pattern-file: \"" << get_file() << "\";" << std::endl
		  << std::string(indent, ' ') << "polygon-pattern-gamma: " << get_gamma() << ';' << std::endl
		  << std::string(indent, ' ') << "polygon-pattern-opacity: " << get_opacity() << ';' << std::endl
		  << std::string(indent, ' ') << "polygon-pattern-alignment: " << get_alignment() << ';' << std::endl;
}

const std::string& to_str(CartoCSS::TextParams::horizontalalignment_t x)
{
	switch (x) {
	case CartoCSS::TextParams::horizontalalignment_t::auto_:
	{
		static const std::string r("auto");
		return r;
	}

	case CartoCSS::TextParams::horizontalalignment_t::left:
	{
		static const std::string r("left");
		return r;
	}

	case CartoCSS::TextParams::horizontalalignment_t::middle:
	{
		static const std::string r("middle");
		return r;
	}

	case CartoCSS::TextParams::horizontalalignment_t::right:
	{
		static const std::string r("right");
		return r;
	}

	case CartoCSS::TextParams::horizontalalignment_t::invalid:
	{
		static const std::string r("invalid");
		return r;
	}

	default:
	{
		static const std::string r("??");
		return r;
	}
	}
}

const std::string& to_str(CartoCSS::TextParams::verticalalignment_t x)
{
	switch (x) {
	case CartoCSS::TextParams::verticalalignment_t::auto_:
	{
		static const std::string r("auto");
		return r;
	}

	case CartoCSS::TextParams::verticalalignment_t::top:
	{
		static const std::string r("top");
		return r;
	}

	case CartoCSS::TextParams::verticalalignment_t::middle:
	{
		static const std::string r("middle");
		return r;
	}

	case CartoCSS::TextParams::verticalalignment_t::bottom:
	{
		static const std::string r("bottom");
		return r;
	}

	case CartoCSS::TextParams::verticalalignment_t::invalid:
	{
		static const std::string r("invalid");
		return r;
	}

	default:
	{
		static const std::string r("??");
		return r;
	}
	}
}

const std::string& to_str(CartoCSS::BasicTextParams::placementtype_t x)
{
	switch (x) {
	case CartoCSS::BasicTextParams::placementtype_t::dummy:
	{
		static const std::string r("dummy");
		return r;
	}

	case CartoCSS::BasicTextParams::placementtype_t::simple:
	{
		static const std::string r("simple");
		return r;
	}

	case CartoCSS::BasicTextParams::placementtype_t::invalid:
	{
		static const std::string r("invalid");
		return r;
	}

	default:
	{
		static const std::string r("??");
		return r;
	}
	}
}

CartoCSS::BasicTextParams::BasicTextParams(void)
	: m_fill(0, 0, 0, 1), m_halofill(1, 1, 1, 1), m_characterspacing(0.0), m_haloradius(0.0),
	  m_linespacing(0.0), m_margin(0.0), m_opacity(1.0), m_size(10.0), m_wrapwidth(0.0),
	  m_wrapcharacter(' '), m_horizontalalignment(horizontalalignment_t::auto_),
	  m_verticalalignment(verticalalignment_t::auto_), m_placementtype(placementtype_t::dummy)
{
}

bool CartoCSS::BasicTextParams::set_variable(variable_t var, const Expr::const_ptr_t& expr)
{
	if (!expr)
		return false;
	switch (var) {
	case variable_t::shieldhorizontalalignment:
	case variable_t::texthorizontalalignment:
	{
		Expr::keyword_t kw(expr->as_keyword());
		BasicTextParams::horizontalalignment_t alignment(BasicTextParams::horizontalalignment_t::invalid);
		switch (kw) {
		case Expr::keyword_t::left:
			alignment = BasicTextParams::horizontalalignment_t::left;
			break;

		case Expr::keyword_t::middle:
			alignment = BasicTextParams::horizontalalignment_t::middle;
			break;

		case Expr::keyword_t::right:
			alignment = BasicTextParams::horizontalalignment_t::right;
			break;

		default:
			throw std::runtime_error("invalid " + to_str(var) + " symbol " + to_str(kw));
		}
		if (alignment == BasicTextParams::horizontalalignment_t::invalid)
			return false;
		set_horizontalalignment(alignment);
		return true;
	}

	case variable_t::shieldverticalalignment:
	case variable_t::textverticalalignment:
	{
		Expr::keyword_t kw(expr->as_keyword());
		BasicTextParams::verticalalignment_t alignment(BasicTextParams::verticalalignment_t::invalid);
		switch (kw) {
		case Expr::keyword_t::top:
			alignment = BasicTextParams::verticalalignment_t::top;
			break;

		case Expr::keyword_t::middle:
			alignment = BasicTextParams::verticalalignment_t::middle;
			break;

		case Expr::keyword_t::bottom:
			alignment = BasicTextParams::verticalalignment_t::bottom;
			break;

		default:
			throw std::runtime_error("invalid " + to_str(var) + " symbol " + to_str(kw));
		}
		if (alignment == BasicTextParams::verticalalignment_t::invalid)
			return false;
		set_verticalalignment(alignment);
		return true;
	}

	case variable_t::shieldfill:
	case variable_t::textfill:
		set_fill(expr->as_color());
		return true;

	case variable_t::shieldhalofill:
	case variable_t::texthalofill:
		set_halofill(expr->as_color());
		return true;

	case variable_t::shieldcharacterspacing:
	case variable_t::textcharacterspacing:
		set_characterspacing(expr->as_double());
		return true;

	case variable_t::shieldhaloradius:
	case variable_t::texthaloradius:
		set_haloradius(expr->as_double());
		return true;

	case variable_t::shieldlinespacing:
	case variable_t::textlinespacing:
		set_linespacing(expr->as_double());
		return true;

	case variable_t::shieldmargin:
	case variable_t::textmargin:
		set_margin(expr->as_double());
		return true;

	case variable_t::shieldopacity:
	case variable_t::textopacity:
		set_opacity(expr->as_double());
		return true;

	case variable_t::shieldsize:
	case variable_t::textsize:
		set_size(expr->as_double());
		return true;

	case variable_t::shieldwrapwidth:
	case variable_t::textwrapwidth:
		set_wrapwidth(expr->as_double());
		return true;

	case variable_t::shieldwrapcharacter:
	case variable_t::textwrapcharacter:
	{
		std::string x(expr->as_string());
		if (x.size() != 1)
			throw std::runtime_error(to_str(var) + " must be exactly one character");
		set_wrapcharacter(x[0]);
		return true;
	}

	case variable_t::shieldfacename:
	case variable_t::textfacename:
		set_facename(expr->as_stringlist());
		return true;

	case variable_t::shieldname:
	case variable_t::textname:
		if (expr->is_type())
			set_name(std::string());
		else
			set_name(expr->as_string());
		return true;

	case variable_t::shieldplacements:
	case variable_t::textplacements:
		set_placements(expr->as_string());
		return true;

	case variable_t::shieldplacementtype:
	case variable_t::textplacementtype:
	{
		Expr::keyword_t kw(expr->as_keyword());
		switch (kw) {
		case Expr::keyword_t::simple:
			set_placementtype(ShieldParams::placementtype_t::simple);
			return true;

		default:
			throw std::runtime_error("invalid shield-placement-type symbol " + to_str(kw));
		}
		return false;
	}

	default:
		return false;
	}
}

CartoCSS::BasicTextParams::placementorigins_t CartoCSS::BasicTextParams::parse_placements(const std::string& pl)
{
	placementorigins_t r, rf;
	PlacementOrigin cur;
	unsigned int fontsize(0);
	for (char ch : pl) {
		switch (ch) {
		case 'X':
		case 'x':
			cur.set_horizontalalignment(horizontalalignment_t::middle);
			cur.set_verticalalignment(verticalalignment_t::middle);
			break;

		case 'N':
		case 'n':
			cur.set_verticalalignment(verticalalignment_t::bottom);
			break;

		case 'S':
		case 's':
			cur.set_verticalalignment(verticalalignment_t::top);
			break;

		case 'E':
		case 'e':
			cur.set_horizontalalignment(horizontalalignment_t::left);
			break;

		case 'W':
		case 'w':
			cur.set_horizontalalignment(horizontalalignment_t::right);
			break;

		case '0' ... '9':
			fontsize *= 10;
			fontsize += ch - '0';
			break;

		case ',':
			if (cur.get_horizontalalignment() != horizontalalignment_t::invalid ||
			    cur.get_verticalalignment() != verticalalignment_t::invalid) {
				if (cur.get_horizontalalignment() == horizontalalignment_t::invalid)
					cur.set_horizontalalignment(horizontalalignment_t::middle);
				if (cur.get_verticalalignment() == verticalalignment_t::invalid)
					cur.set_verticalalignment(verticalalignment_t::middle);
				r.push_back(cur);
				cur = PlacementOrigin();
			}
			if (fontsize) {
				for (const PlacementOrigin& po : r) {
					rf.push_back(po);
					rf.back().set_fontsize(fontsize);
				}
				fontsize = 0;
			}
			break;

		default:
			break;
		}
	}
	if (cur.get_horizontalalignment() != horizontalalignment_t::invalid ||
	    cur.get_verticalalignment() != verticalalignment_t::invalid) {
		if (cur.get_horizontalalignment() == horizontalalignment_t::invalid)
			cur.set_horizontalalignment(horizontalalignment_t::middle);
		if (cur.get_verticalalignment() == verticalalignment_t::invalid)
			cur.set_verticalalignment(verticalalignment_t::middle);
		r.push_back(cur);
		cur = PlacementOrigin();
	}
	if (fontsize) {
		for (const PlacementOrigin& po : r) {
			rf.push_back(po);
			rf.back().set_fontsize(fontsize);
		}
		fontsize = 0;
	}
	r.insert(r.end(), rf.begin(), rf.end());
	if (r.empty())
		r.push_back(PlacementOrigin());
	return r;
}

std::ostream& CartoCSS::BasicTextParams::print(std::ostream& os, unsigned int indent, const char *tpfx) const
{
	if (!tpfx)
		tpfx = "";
	os << std::string(indent, ' ') << tpfx << "fill " << get_fill().to_str() << std::endl
	   << std::string(indent, ' ') << tpfx << "halofill " << get_halofill().to_str() << std::endl;
	 {
		os << std::string(indent, ' ') << tpfx << "facename ";
		bool subseq(false);
		for (const auto& f : get_facename()) {
			if (subseq)
				os << ',';
			subseq = true;
			os << f;
		}
		os << std::endl;
	 }
	 return os << std::string(indent, ' ') << tpfx << "name " << get_name() << std::endl
		   << std::string(indent, ' ') << tpfx << "placements " << get_placements() << std::endl
		   << std::string(indent, ' ') << tpfx << "characterspacing " << get_characterspacing() << std::endl
		   << std::string(indent, ' ') << tpfx << "haloradius " << get_haloradius() << std::endl
		   << std::string(indent, ' ') << tpfx << "linespacing " << get_linespacing() << std::endl
		   << std::string(indent, ' ') << tpfx << "margin " << get_margin() << std::endl
		   << std::string(indent, ' ') << tpfx << "opacity " << get_opacity() << std::endl
		   << std::string(indent, ' ') << tpfx << "size " << get_size() << std::endl
		   << std::string(indent, ' ') << tpfx << "wrapwidth " << get_wrapwidth() << std::endl
		   << std::string(indent, ' ') << tpfx << "wrapcharacter '" << get_wrapcharacter() << '\'' << std::endl
		   << std::string(indent, ' ') << tpfx << "horizontalalignment " << get_horizontalalignment() << std::endl
		   << std::string(indent, ' ') << tpfx << "verticalalignment " << get_verticalalignment() << std::endl
		   << std::string(indent, ' ') << tpfx << "placementtype " << get_placementtype() << std::endl;
}

std::ostream& CartoCSS::BasicTextParams::print_mss(std::ostream& os, const std::string& pfx, unsigned int indent, const char *tpfx) const
{
	if (!tpfx)
		tpfx = "";
	const char *sep = "/";
	sep += pfx.empty();
	os << std::string(indent, ' ') << pfx << sep << tpfx << "fill: " << get_fill().to_mss_str() << ';' << std::endl
	   << std::string(indent, ' ') << pfx << sep << tpfx << "halo-fill: " << get_halofill().to_mss_str() << ';' << std::endl;
	{
		os << std::string(indent, ' ') << pfx << sep << tpfx << "face-name: ";
		bool subseq(false);
		for (const auto& f : get_facename()) {
			if (subseq)
				os << ',';
			subseq = true;
			os << '"' << f << '"';
		}
		os << ';' << std::endl;
	}
	return os << std::string(indent, ' ') << pfx << sep << tpfx << "name: " << get_name() << ';' << std::endl
		  << std::string(indent, ' ') << pfx << sep << tpfx << "placements: " << get_placements() << ';' << std::endl
		  << std::string(indent, ' ') << pfx << sep << tpfx << "character-spacing: " << get_characterspacing() << ';' << std::endl
		  << std::string(indent, ' ') << pfx << sep << tpfx << "halo-radius: " << get_haloradius() << ';' << std::endl
		  << std::string(indent, ' ') << pfx << sep << tpfx << "line-spacing: " << get_linespacing() << ';' << std::endl
		  << std::string(indent, ' ') << pfx << sep << tpfx << "margin: " << get_margin() << ';' << std::endl
		  << std::string(indent, ' ') << pfx << sep << tpfx << "opacity: " << get_opacity() << ';' << std::endl
		  << std::string(indent, ' ') << pfx << sep << tpfx << "size: " << get_size() << ';' << std::endl
		  << std::string(indent, ' ') << pfx << sep << tpfx << "wrap-width: " << get_wrapwidth() << ';' << std::endl
		  << std::string(indent, ' ') << pfx << sep << tpfx << "wrap-character: '" << get_wrapcharacter() << "';" << std::endl
		  << std::string(indent, ' ') << pfx << sep << tpfx << "horizontal-alignment: " << get_horizontalalignment() << ';' << std::endl
		  << std::string(indent, ' ') << pfx << sep << tpfx << "vertical-alignment: " << get_verticalalignment() << ';' << std::endl
		  << std::string(indent, ' ') << pfx << sep << tpfx << "placement-type: " << get_placementtype() << ';' << std::endl;
}

CartoCSS::ShieldParams::ShieldParams(void)
	: m_repeatdistance(0.0), m_spacing(0.0), m_textdx(0.0), m_textdy(0.0),
	  m_placement(placement_t::point), m_clip(false), m_unlockimage(false)
{
}

bool CartoCSS::ShieldParams::set_variable(variable_t var, const Expr::const_ptr_t& expr)
{
	if (!expr)
		return false;
	switch (var) {
	case variable_t::shieldplacement:
	{
		Expr::keyword_t kw(expr->as_keyword());
		placement_t placement(placement_t::invalid);
		switch (kw) {
		case Expr::keyword_t::line:
			placement = placement_t::line;
			break;

		case Expr::keyword_t::point:
			placement = placement_t::point;
			break;

		case Expr::keyword_t::interior:
			placement = placement_t::interior;
			break;

		default:
			throw std::runtime_error("invalid " + to_str(var) + " symbol " + to_str(kw));
		}
		if (placement == placement_t::invalid)
			return false;
		set_placement(placement);
		return true;
	}

	case variable_t::shieldclip:
		set_clip(expr->as_bool());
		return true;

	case variable_t::shieldunlockimage:
		set_unlockimage(expr->as_bool());
		return true;

	case variable_t::shieldrepeatdistance:
		set_repeatdistance(expr->as_double());
		return true;

	case variable_t::shieldspacing:
		set_spacing(expr->as_double());
		return true;

	case variable_t::shieldtextdx:
		set_textdx(expr->as_double());
		return true;

	case variable_t::shieldtextdy:
		set_textdy(expr->as_double());
		return true;

	case variable_t::shieldfile:
		set_file(expr->as_url());
		return true;

	default:
		if (var >= variable_t::shieldfirst_ && var <= variable_t::shieldlast_)
			return BasicTextParams::set_variable(var, expr);
		return false;
	}
}

std::ostream& CartoCSS::ShieldParams::print(std::ostream& os, unsigned int indent) const
{
	BasicTextParams::print(os, indent, "shield-");
	return os << std::string(indent, ' ') << "shield-file " << get_file() << std::endl
		  << std::string(indent, ' ') << "shield-repeatdistance " << get_repeatdistance() << std::endl
		  << std::string(indent, ' ') << "shield-spacing " << get_spacing() << std::endl
		  << std::string(indent, ' ') << "shield-textdx " << get_textdx() << std::endl
		  << std::string(indent, ' ') << "shield-textdy " << get_textdy() << std::endl
		  << std::string(indent, ' ') << "shield-placement " << get_placement() << std::endl
		  << std::string(indent, ' ') << "shield-clip " << get_clip() << std::endl
		  << std::string(indent, ' ') << "shield-unlockimage " << get_unlockimage() << std::endl;
}

std::ostream& CartoCSS::ShieldParams::print_mss(std::ostream& os, const std::string& pfx, unsigned int indent) const
{
	BasicTextParams::print_mss(os, pfx, indent, "shield-");
	const char *sep = "/";
	sep += pfx.empty();
	return os << std::string(indent, ' ') << pfx << sep << "shield-file: " << get_file() << ';' << std::endl
		  << std::string(indent, ' ') << pfx << sep << "shield-repeat-distance: " << get_repeatdistance() << ';' << std::endl
		  << std::string(indent, ' ') << pfx << sep << "shield-spacing: " << get_spacing() << ';' << std::endl
		  << std::string(indent, ' ') << pfx << sep << "shield-text-dx: " << get_textdx() << ';' << std::endl
		  << std::string(indent, ' ') << pfx << sep << "shield-text-dy: " << get_textdy() << ';' << std::endl
		  << std::string(indent, ' ') << pfx << sep << "shield-placement: " << get_placement() << ';' << std::endl
		  << std::string(indent, ' ') << pfx << sep << "shield-clip: " << get_clip() << ';' << std::endl
		  << std::string(indent, ' ') << pfx << sep << "shield-unlock-image: " << get_unlockimage() << ';' << std::endl;
}

const std::string& to_str(CartoCSS::TextParams::upright_t x)
{
	switch (x) {
	case CartoCSS::TextParams::upright_t::auto_:
	{
		static const std::string r("auto");
		return r;
	}

	case CartoCSS::TextParams::upright_t::left:
	{
		static const std::string r("left");
		return r;
	}

	case CartoCSS::TextParams::upright_t::right:
	{
		static const std::string r("right");
		return r;
	}

	case CartoCSS::TextParams::upright_t::left_only:
	{
		static const std::string r("left_only");
		return r;
	}

	case CartoCSS::TextParams::upright_t::right_only:
	{
		static const std::string r("right_only");
		return r;
	}

	case CartoCSS::TextParams::upright_t::invalid:
	{
		static const std::string r("invalid");
		return r;
	}

	default:
	{
		static const std::string r("??");
		return r;
	}
	}
}

CartoCSS::TextParams::TextParams(void)
	: m_dx(0.0), m_dy(0.0), m_maxcharangledelta(22.5), m_mindistance(0.0), m_repeatdistance(0.0),
	  m_spacing(0.0), m_placement(placement_t::point), m_upright(upright_t::auto_),
	  m_clip(false), m_largestbboxonly(true)
{
}

bool CartoCSS::TextParams::set_variable(variable_t var, const Expr::const_ptr_t& expr)
{
	if (!expr)
		return false;
	switch (var) {
	case variable_t::textplacement:
	{
		Expr::keyword_t kw(expr->as_keyword());
		placement_t placement(placement_t::invalid);
		switch (kw) {
		case Expr::keyword_t::line:
			placement = placement_t::line;
			break;

		case Expr::keyword_t::point:
			placement = placement_t::point;
			break;

		case Expr::keyword_t::interior:
			placement = placement_t::interior;
			break;

		default:
			throw std::runtime_error("invalid " + to_str(var) + " symbol " + to_str(kw));
		}
		if (placement == placement_t::invalid)
			return false;
		set_placement(placement);
		return true;
	}

	case variable_t::textupright:
	{
		Expr::keyword_t kw(expr->as_keyword());
		switch (kw) {
		case Expr::keyword_t::auto_:
			set_upright(TextParams::upright_t::auto_);
			return true;

		case Expr::keyword_t::left:
			set_upright(TextParams::upright_t::left);
			return true;

		case Expr::keyword_t::right:
			set_upright(TextParams::upright_t::right);
			return true;

		case Expr::keyword_t::left_only:
			set_upright(TextParams::upright_t::left_only);
			return true;

		case Expr::keyword_t::right_only:
			set_upright(TextParams::upright_t::right_only);
			return true;

		default:
			throw std::runtime_error("invalid text-upright symbol " + to_str(kw));
		}
		return false;
	}

	case variable_t::textclip:
		set_clip(expr->as_bool());
		return true;

	case variable_t::textlargestbboxonly:
		set_largestbboxonly(expr->as_bool());
		return true;

	case variable_t::textdx:
		set_dx(expr->as_double());
		return true;

	case variable_t::textdy:
		set_dy(expr->as_double());
		return true;

	case variable_t::textmaxcharangledelta:
		set_maxcharangledelta(expr->as_double());
		return true;

	case variable_t::textmindistance:
		set_mindistance(expr->as_double());
		return true;

	case variable_t::textrepeatdistance:
		set_repeatdistance(expr->as_double());
		return true;

	case variable_t::textspacing:
		set_spacing(expr->as_double());
		return true;

	default:
		if (var >= variable_t::textfirst_ && var <= variable_t::textlast_)
			return BasicTextParams::set_variable(var, expr);
		return false;
	}
}

std::ostream& CartoCSS::TextParams::print(std::ostream& os, unsigned int indent) const
{
	BasicTextParams::print(os, indent, "text-");
	return os << std::string(indent, ' ') << "text-dx " << get_dx() << std::endl
		  << std::string(indent, ' ') << "text-dy " << get_dy() << std::endl
		  << std::string(indent, ' ') << "text-maxcharangledelta " << get_maxcharangledelta() << std::endl
		  << std::string(indent, ' ') << "text-mindistance " << get_mindistance() << std::endl
		  << std::string(indent, ' ') << "text-repeatdistance " << get_repeatdistance() << std::endl
		  << std::string(indent, ' ') << "text-spacing " << get_spacing() << std::endl
		  << std::string(indent, ' ') << "text-placement " << get_placement() << std::endl
		  << std::string(indent, ' ') << "text-upright " << get_upright() << std::endl
		  << std::string(indent, ' ') << "text-clip " << get_clip() << std::endl
		  << std::string(indent, ' ') << "text-largestbboxonly " << get_largestbboxonly() << std::endl;
}

std::ostream& CartoCSS::TextParams::print_mss(std::ostream& os, const std::string& pfx, unsigned int indent) const
{
	BasicTextParams::print_mss(os, pfx, indent, "text-");
	const char *sep = "/";
	sep += pfx.empty();
	return os << std::string(indent, ' ') << pfx << sep << "text-dx: " << get_dx() << ';' << std::endl
		  << std::string(indent, ' ') << pfx << sep << "text-dy: " << get_dy() << ';' << std::endl
		  << std::string(indent, ' ') << pfx << sep << "text-max-char-angle-delta: " << get_maxcharangledelta() << ';' << std::endl
		  << std::string(indent, ' ') << pfx << sep << "text-min-distance: " << get_mindistance() << ';' << std::endl
		  << std::string(indent, ' ') << pfx << sep << "text-repeat-distance: " << get_repeatdistance() << ';' << std::endl
		  << std::string(indent, ' ') << pfx << sep << "text-spacing: " << get_spacing() << ';' << std::endl
		  << std::string(indent, ' ') << pfx << sep << "text-placement: " << get_placement() << ';' << std::endl
		  << std::string(indent, ' ') << pfx << sep << "text-upright: " << get_upright() << ';' << std::endl
		  << std::string(indent, ' ') << pfx << sep << "text-clip: " << get_clip() << ';' << std::endl
		  << std::string(indent, ' ') << pfx << sep << "text-largest-bbox-only: " << get_largestbboxonly() << ';' << std::endl;
}

CartoCSS::ImageFilter::ImageFilter(void)
	: m_filter(filter_t::invalid)
{
	for (unsigned int i = 0; i < 8; ++i)
		m_par[i] = std::numeric_limits<double>::quiet_NaN();
}			

const std::string& to_str(CartoCSS::ImageFilter::filter_t x)
{
	switch (x) {
	case CartoCSS::ImageFilter::filter_t::scalehsla:
	{
		static const std::string r("scalehsla");
		return r;
	}

	case CartoCSS::ImageFilter::filter_t::invalid:
	{
		static const std::string r("invalid");
		return r;
	}

	default:
	{
		static const std::string r("??");
		return r;
	}
	}
}

CartoCSS::ImageFilter::ImageFilter(filter_t filt, const std::tuple<double,double,double,double,double,double,double,double>& par)
	: m_filter(filt)
{
	m_par[0] = std::get<0>(par);
	m_par[1] = std::get<1>(par);
	m_par[2] = std::get<2>(par);
	m_par[3] = std::get<3>(par);
	m_par[4] = std::get<4>(par);
	m_par[5] = std::get<5>(par);
	m_par[6] = std::get<6>(par);
	m_par[7] = std::get<7>(par);
}

std::string CartoCSS::ImageFilter::to_str(void) const
{
	switch (get_filter()) {
	case filter_t::scalehsla:
	{
		std::ostringstream oss;
		oss << "scale-hsla(" << std::setprecision(3) << m_par[0];
		for (unsigned int i = 1; i < 8; ++i)
			oss << ',' << m_par[i];
		oss << ')';
		return oss.str();
	}

	default:
		return "none";
	}
}

CartoCSS::GlobalParameters::GlobalParameters(void)
	: m_opacity(1.0), m_compop(compositeop_t::srcover)
{
}

bool CartoCSS::GlobalParameters::set_variable(variable_t var, const Expr::const_ptr_t& expr)
{
	if (!expr)
		return false;
	switch (var) {
	case variable_t::compop:
		set_compop(expr->as_compositeop());
		return true;

	case variable_t::imagefilters:
		{
			Expr::func_t func;
			Expr::const_ptr_t args;
			if (!expr->is_type(func, args))
				throw std::runtime_error("function expected");
			if (func != Expr::func_t::scalehsla)
				throw std::runtime_error("only scale-hsla allowed");
			Expr::doublelist_t argd(args->as_doublelist());
			if (argd.size() != 8)
				throw std::runtime_error("expression list size 8 expected");
			typedef std::tuple<double,double,double,double,double,double,double,double> args_t;
			args_t par(argd[0], argd[1], argd[2], argd[3], argd[4], argd[5], argd[6], argd[7]);
			set_imagefilters(ImageFilter(ImageFilter::filter_t::scalehsla, par));
			return true;
		}

	case variable_t::opacity:
		set_opacity(expr->as_double());
		return true;

	default:
		return false;
	}
}

std::ostream& CartoCSS::GlobalParameters::print(std::ostream& os, unsigned int indent) const
{
	return os << std::string(indent, ' ') << "imagefilters " << get_imagefilters().to_str() << std::endl
		  << std::string(indent, ' ') << "opacity " << get_opacity() << std::endl
		  << std::string(indent, ' ') << "compop " << get_compop() << std::endl;
}

std::ostream& CartoCSS::GlobalParameters::print_mss(std::ostream& os, unsigned int indent) const
{
	return os << std::string(indent, ' ') << "image-filters: " << get_imagefilters().to_str() << ';' << std::endl
		  << std::string(indent, ' ') << "opacity: " << get_opacity() << ';' << std::endl
		  << std::string(indent, ' ') << "comp-op: " << get_compop() << ';' << std::endl;
}

CartoCSS::ObjectParameters::ObjectParameters(void)
{
}

const CartoCSS::MarkerParams& CartoCSS::ObjectParameters::get_marker(PrefixAllocator::prefix_t x) const
{
	marker_t::const_iterator i(m_marker.find(x));
	if (i == m_marker.end()) {
		static const MarkerParams null;
		return null;
	}
	return i->second;
}

const CartoCSS::LineParams& CartoCSS::ObjectParameters::get_line(PrefixAllocator::prefix_t x) const
{
	line_t::const_iterator i(m_line.find(x));
	if (i == m_line.end()) {
		static const LineParams null;
		return null;
	}
	return i->second;
}

const CartoCSS::LinePatternParams& CartoCSS::ObjectParameters::get_linepattern(PrefixAllocator::prefix_t x) const
{
	linepattern_t::const_iterator i(m_linepattern.find(x));
	if (i == m_linepattern.end()) {
		static const LinePatternParams null;
		return null;
	}
	return i->second;
}

const CartoCSS::PolygonParams& CartoCSS::ObjectParameters::get_polygon(PrefixAllocator::prefix_t x) const
{
	polygon_t::const_iterator i(m_polygon.find(x));
	if (i == m_polygon.end()) {
		static const PolygonParams null;
		return null;
	}
	return i->second;
}

const CartoCSS::PolygonPatternParams& CartoCSS::ObjectParameters::get_polygonpattern(PrefixAllocator::prefix_t x) const
{
	polygonpattern_t::const_iterator i(m_polygonpattern.find(x));
	if (i == m_polygonpattern.end()) {
		static const PolygonPatternParams null;
		return null;
	}
	return i->second;
}

const CartoCSS::ShieldParams& CartoCSS::ObjectParameters::get_shield(PrefixAllocator::prefix_t x) const
{
	shield_t::const_iterator i(m_shield.find(x));
	if (i == m_shield.end()) {
		static const ShieldParams null;
		return null;
	}
	return i->second;
}

const CartoCSS::TextParams& CartoCSS::ObjectParameters::get_text(PrefixAllocator::prefix_t x) const
{
	text_t::const_iterator i(m_text.find(x));
	if (i == m_text.end()) {
		static const TextParams null;
		return null;
	}
	return i->second;
}

bool CartoCSS::ObjectParameters::is_marker(PrefixAllocator::prefix_t x) const
{
	marker_t::const_iterator i(m_marker.find(x));
	return i != m_marker.end();
}

bool CartoCSS::ObjectParameters::is_line(PrefixAllocator::prefix_t x) const
{
	line_t::const_iterator i(m_line.find(x));
	return i != m_line.end();
}

bool CartoCSS::ObjectParameters::is_linepattern(PrefixAllocator::prefix_t x) const
{
	linepattern_t::const_iterator i(m_linepattern.find(x));
	return i != m_linepattern.end();
}

bool CartoCSS::ObjectParameters::is_polygon(PrefixAllocator::prefix_t x) const
{
	polygon_t::const_iterator i(m_polygon.find(x));
	return i != m_polygon.end();
}

bool CartoCSS::ObjectParameters::is_polygonpattern(PrefixAllocator::prefix_t x) const
{
	polygonpattern_t::const_iterator i(m_polygonpattern.find(x));
	return i != m_polygonpattern.end();
}

bool CartoCSS::ObjectParameters::is_shield(PrefixAllocator::prefix_t x) const
{
	shield_t::const_iterator i(m_shield.find(x));
	return i != m_shield.end();
}

bool CartoCSS::ObjectParameters::is_text(PrefixAllocator::prefix_t x) const
{
	text_t::const_iterator i(m_text.find(x));
	return i != m_text.end();
}

std::set<CartoCSS::PrefixAllocator::prefix_t> CartoCSS::ObjectParameters::get_sets(void) const
{
	std::set<PrefixAllocator::prefix_t> r;
	for (const auto& p : get_marker())
		r.insert(p.first);
	for (const auto& p : get_line())
		r.insert(p.first);
	for (const auto& p : get_linepattern())
		r.insert(p.first);
	for (const auto& p : get_polygon())
		r.insert(p.first);
	for (const auto& p : get_polygonpattern())
		r.insert(p.first);
	for (const auto& p : get_shield())
		r.insert(p.first);
	for (const auto& p : get_text())
		r.insert(p.first);
	return r;
}

bool CartoCSS::ObjectParameters::empty(void) const
{
	return get_marker().empty() && get_line().empty() && get_linepattern().empty() &&
		get_polygon().empty() && get_polygonpattern().empty() &&
		get_shield().empty() && get_text().empty();
}

bool CartoCSS::ObjectParameters::set_variable(variable_t var, PrefixAllocator::prefix_t prefix, const Expr::const_ptr_t& expr)
{
	if (var >= variable_t::linefirst_ && var <= variable_t::linelast_) {
		std::pair<line_t::iterator,bool> x(get_line().insert(line_t::value_type(prefix, LineParams())));
		bool s(x.first->second.set_variable(var, expr));
		if (x.second && !s)
			get_line().erase(x.first);
		return s;
	}
	if (var >= variable_t::linepatternfirst_ && var <= variable_t::linepatternlast_) {
		std::pair<linepattern_t::iterator,bool> x(get_linepattern().insert(linepattern_t::value_type(prefix, LinePatternParams())));
		bool s(x.first->second.set_variable(var, expr));
		if (x.second && !s)
			get_linepattern().erase(x.first);
		return s;
	}
	if (var >= variable_t::markerfirst_ && var <= variable_t::markerlast_) {
		std::pair<marker_t::iterator,bool> x(get_marker().insert(marker_t::value_type(prefix, MarkerParams())));
		bool s(x.first->second.set_variable(var, expr));
		if (x.second && !s)
			get_marker().erase(x.first);
		return s;
	}
	if (var >= variable_t::polygonfirst_ && var <= variable_t::polygonlast_) {
		std::pair<polygon_t::iterator,bool> x(get_polygon().insert(polygon_t::value_type(prefix, PolygonParams())));
		bool s(x.first->second.set_variable(var, expr));
		if (x.second && !s)
			get_polygon().erase(x.first);
		return s;
	}
	if (var >= variable_t::polygonpatternfirst_ && var <= variable_t::polygonpatternlast_) {
		std::pair<polygonpattern_t::iterator,bool> x(get_polygonpattern().insert(polygonpattern_t::value_type(prefix, PolygonPatternParams())));
		bool s(x.first->second.set_variable(var, expr));
		if (x.second && !s)
			get_polygonpattern().erase(x.first);
		return s;
	}
	if (var >= variable_t::shieldfirst_ && var <= variable_t::shieldlast_) {
		std::pair<shield_t::iterator,bool> x(get_shield().insert(shield_t::value_type(prefix, ShieldParams())));
		bool s(x.first->second.set_variable(var, expr));
		if (x.second && !s)
			get_shield().erase(x.first);
		return s;
	}
	if (var >= variable_t::textfirst_ && var <= variable_t::textlast_) {
		std::pair<text_t::iterator,bool> x(get_text().insert(text_t::value_type(prefix, TextParams())));
		bool s(x.first->second.set_variable(var, expr));
		if (x.second && !s)
			get_text().erase(x.first);
		return s;
	}
	return false;
}

std::ostream& CartoCSS::ObjectParameters::print(std::ostream& os, const PrefixAllocator& palloc, unsigned int indent) const
{
	std::set<PrefixAllocator::prefix_t> ss(get_sets());
	for (const auto& s : ss) {
		os << std::string(indent, ' ') << "Parameter Set \"" << palloc.get_name(s) << "\"" << std::endl;
		if (is_marker(s))
			get_marker(s).print(os, indent + 2);
		if (is_line(s))
			get_line(s).print(os, indent + 2);
		if (is_linepattern(s))
			get_linepattern(s).print(os, indent + 2);
		if (is_polygon(s))
			get_polygon(s).print(os, indent + 2);
		if (is_polygonpattern(s))
			get_polygonpattern(s).print(os, indent + 2);
		if (is_shield(s))
			get_shield(s).print(os, indent + 2);
		if (is_text(s))
			get_text(s).print(os, indent + 2);
	}
	return os;
}

std::ostream& CartoCSS::ObjectParameters::print_mss(std::ostream& os, const PrefixAllocator& palloc, unsigned int indent) const
{
	for (auto& p : get_marker())
		p.second.print_mss(os, palloc.get_name(p.first), indent);
	for (auto& p : get_line())
		p.second.print_mss(os, palloc.get_name(p.first), indent);
	for (auto& p : get_linepattern())
		p.second.print_mss(os, palloc.get_name(p.first), indent);
	for (auto& p : get_polygon())
		p.second.print_mss(os, palloc.get_name(p.first), indent);
	for (auto& p : get_polygonpattern())
		p.second.print_mss(os, palloc.get_name(p.first), indent);
	for (auto& p : get_shield())
		p.second.print_mss(os, palloc.get_name(p.first), indent);
	for (auto& p : get_text())
		p.second.print_mss(os, palloc.get_name(p.first), indent);
	return os;
}

std::ostream& CartoCSS::ObjectParameters::print(std::ostream& os, unsigned int indent) const
{
	std::set<PrefixAllocator::prefix_t> ss(get_sets());
	for (const auto& s : ss) {
		os << std::string(indent, ' ') << "Parameter Set " << static_cast<unsigned int>(s) << std::endl;
		get_marker(s).print(os, indent + 2);
		get_line(s).print(os, indent + 2);
		get_linepattern(s).print(os, indent + 2);
		get_polygon(s).print(os, indent + 2);
		get_polygonpattern(s).print(os, indent + 2);
		get_shield(s).print(os, indent + 2);
		get_text(s).print(os, indent + 2);
	}
	return os;
}

std::ostream& CartoCSS::ObjectParameters::print_mss(std::ostream& os, unsigned int indent) const
{
	for (auto& p : get_marker()) {
		std::ostringstream name;
		name << static_cast<unsigned int>(p.first);
		p.second.print_mss(os, name.str(), indent);
	}
	for (auto& p : get_line()) {
		std::ostringstream name;
		name << static_cast<unsigned int>(p.first);
		p.second.print_mss(os, name.str(), indent);
	}
	for (auto& p : get_linepattern()) {
		std::ostringstream name;
		name << static_cast<unsigned int>(p.first);
		p.second.print_mss(os, name.str(), indent);
	}
	for (auto& p : get_polygon()) {
		std::ostringstream name;
		name << static_cast<unsigned int>(p.first);
		p.second.print_mss(os, name.str(), indent);
	}
	for (auto& p : get_polygonpattern()) {
		std::ostringstream name;
		name << static_cast<unsigned int>(p.first);
		p.second.print_mss(os, name.str(), indent);
	}
	for (auto& p : get_shield()) {
		std::ostringstream name;
		name << static_cast<unsigned int>(p.first);
		p.second.print_mss(os, name.str(), indent);
	}
	for (auto& p : get_text()) {
		std::ostringstream name;
		name << static_cast<unsigned int>(p.first);
		p.second.print_mss(os, name.str(), indent);
	}
	return os;
}

CartoCSS::LayerParameters::LayerParameters(void)
{
}

std::ostream& CartoCSS::LayerParameters::print(std::ostream& os, const LayerAllocator& lalloc, const PrefixAllocator& palloc, unsigned int indent) const
{
	for (const auto& x : *this) {
		os << std::string(indent, ' ') << "Layer ";
		{
			const std::string& n(lalloc.get_name(x.first));
			if (n.empty())
				os << "default";
			else
				os << '"' << n << '"';
		}
		x.second.print(os << std::endl, palloc, indent + 2);
	}
	return os;
}

std::ostream& CartoCSS::LayerParameters::print(std::ostream& os, const LayerAllocator& lalloc, const prefixallocators_t& pallocs, unsigned int indent) const
{
	for (const auto& x : *this) {
		os << std::string(indent, ' ') << "Layer ";
		{
			const std::string& n(lalloc.get_name(x.first));
			if (n.empty())
				os << "default";
			else
				os << '"' << n << '"';
		}
		PrefixAllocator palloc;
		prefixallocators_t::const_iterator pait(pallocs.find(x.first));
		x.second.print(os << std::endl, (pait == pallocs.end()) ? palloc : pait->second, indent + 2);
	}
	return os;
}

CartoCSS::LayerGlobalParameters::LayerGlobalParameters(void)
{
}

std::ostream& CartoCSS::LayerGlobalParameters::print(std::ostream& os, const LayerAllocator& lalloc, unsigned int indent) const
{
	for (const auto& x : *this) {
		os << std::string(indent, ' ') << "Layer ";
		{
			const std::string& n(lalloc.get_name(x.first));
			if (n.empty())
				os << "default";
			else
				os << '"' << n << '"';
		}
		x.second.print(os << std::endl, indent + 2);
	}
	return os;
}

CartoCSS::MapParameters::MapParameters(void)
	: m_backgroundcol(0.5, 0.5, 0.5, 1)
{
}

bool CartoCSS::MapParameters::set_variable(variable_t var, const Expr::const_ptr_t& expr)
{
	if (!expr)
		return false;
	switch (var) {
	case variable_t::mapbackgroundcolor:
		set_backgroundcol(expr->as_color());
		return true;

	default:
		return false;
	}
}

std::ostream& CartoCSS::MapParameters::print(std::ostream& os, unsigned int indent) const
{
	if (get_backgroundcol().is_valid())
		os << std::string(indent, ' ') << "background-color " << get_backgroundcol().to_str() << std::endl;
	return os;
}

std::ostream& CartoCSS::MapParameters::print_mss(std::ostream& os, unsigned int indent) const
{
	if (get_backgroundcol().is_valid())
		os << std::string(indent, ' ') << "background-color: " << get_backgroundcol().to_mss_str() << ';' << std::endl;
	return os;
}

CartoCSS::Statement::Statement(void)
	: m_refcount()
{
}

CartoCSS::Statement::~Statement()
{
}

CartoCSS::Statements::Statements(void)
{
}

CartoCSS::Statements::Statements(const statements_t& stmt)
	: m_statements(stmt)
{
}

CartoCSS::Statements::Statements(const statements_t&& stmt)
	: m_statements(stmt)
{
}

CartoCSS::Statement::const_ptr_t CartoCSS::Statements::simplify(void) const
{
	return simplify_int();
}

CartoCSS::Statement::const_ptr_t CartoCSS::Statements::simplify(const std::string& field, const Value& value) const
{
	return simplify_int(field, value);
}

CartoCSS::Statement::const_ptr_t CartoCSS::Statements::simplify(const Fields& fields) const
{
	return simplify_int(fields);
}

CartoCSS::Statement::const_ptr_t CartoCSS::Statements::simplify(const std::string& var, const Expr::const_ptr_t& expr) const
{
	return simplify_int(var, expr);
}

CartoCSS::Statement::const_ptr_t CartoCSS::Statements::simplify(const variables_t& vars) const
{
	return simplify_int(vars);
}

CartoCSS::Statement::const_ptr_t CartoCSS::Statements::simplify(const LayerID& layer) const
{
	return simplify_int(layer);
}

CartoCSS::Statement::const_ptr_t CartoCSS::Statements::simplify(const ClassID& cls) const
{
	return simplify_int(cls);
}

CartoCSS::Statement::const_ptr_t CartoCSS::Statements::simplify(Visitor& v) const
{
	Statement::const_ptr_t p(simplify_int(v));
	if (!p)
		return simplify_cb(v);
	Statement::const_ptr_t p1(p->simplify_cb(v));
	return p1 ? p1 : p;
}

void CartoCSS::Statement::apply(ApplyErrorVisitor& vis, LayerParameters& par, LayerAllocator& lalloc, prefixallocators_t& pallocs) const
{
	apply(vis, par, lalloc, pallocs, std::string());
}

void CartoCSS::Statement::apply(ApplyErrorVisitor& vis, LayerGlobalParameters& par, LayerAllocator& lalloc) const
{
	apply(vis, par, lalloc, std::string(), false);
}

void CartoCSS::Statements::visit(Visitor& v) const
{
	v.visit(*this);
	for (const auto& p : m_statements)
		p->visit(v);
	v.visitend(*this);
}

bool CartoCSS::Statements::simplify_stmt_more(statements_t& stmt)
{
	bool work(false);
	// merge statement blocks without condition
	for (statements_t::iterator i(stmt.begin()), e(stmt.end()); i != e; ) {
		if ((*i)->is_empty()) {
			i = stmt.erase(i);
			e = stmt.end();
			work = true;
			continue;
		}
		Statements::const_ptr_t p(boost::dynamic_pointer_cast<const Statements>(*i));
		if (!p || boost::dynamic_pointer_cast<const SubBlock>(p)) {
			++i;
			continue;
		}
		i = stmt.erase(i);
		i = stmt.insert(i, p->get_statements().begin(), p->get_statements().end());
		e = stmt.end();
		work = true;
	}
	// try to merge adjacent statements
	for (statements_t::iterator i(stmt.begin()), e(stmt.end()); i != e; ) {
		statements_t::iterator i0(i);
		++i;
		if (i == e)
			break;
		Statement::const_ptr_t p((*i0)->merge(*i));
		if (!p)
			continue;
		work = true;
		*i0 = p;
		i = stmt.erase(i);
		e = stmt.end();
		--i;
	}		
	// kill previous variable assignments
	{
		Statement::const_ptr_t prev;
		if (false)
			prev = Statement::const_ptr_t(new Statements(stmt));
		for (statements_t::iterator i(stmt.begin()), e(stmt.end()); i != e; ++i) {
			VariableAssignments::const_ptr_t p(boost::dynamic_pointer_cast<const VariableAssignments>(*i));
			if (!p)
				continue;
			for (const auto& a : p->get_assignments()) {
				bool vwork(false);
				for (statements_t::iterator j(stmt.begin()); j != i; ++j) {
					Statement::const_ptr_t q((*j)->kill_variable_assignment(a.get_var(), a.get_prefix()));
					if (!q)
						continue;
					*j = q;
					vwork = true;
				}
				if (!vwork)
					continue;
				work = true;
				if (false) {
					std::cerr << "WARNING: variable ";
					if (!a.get_prefix().empty())
						std::cerr << a.get_prefix() << '/';
					std::cerr << a.get_var() << " = " << a.get_expr()->to_str()
						  << " superseded by later assignment" << std::endl;
					if (prev)
						prev->print(std::cerr, 2);
				}
			}
		}
	}
	return work;
}

template <typename... Args>
bool CartoCSS::Statements::simplify_stmt(statements_t& stmt, Args&&... args) const
{
	stmt.clear();
	stmt.reserve(m_statements.size());
	bool work(false);
	for (statements_t::const_iterator i(m_statements.begin()), e(m_statements.end()); i != e; ++i) {
		if ((*i)->is_empty()) {
			work = true;
			continue;
		}
		CartoCSS::Statement::const_ptr_t p((*i)->simplify(std::forward<Args>(args)...));
		if (p) {
			work = true;
			if (!p->is_empty())
				stmt.push_back(p);
		} else {
			stmt.push_back(*i);
		}
	}
	work = simplify_stmt_more(stmt) || work;
	return work;
}

template <typename... Args>
CartoCSS::Statements::ptr_t CartoCSS::Statements::simplify_int(Args&&... args) const
{
	statements_t stmt;
	if (simplify_stmt(stmt, std::forward<Args>(args)...))
		return ptr_t(new Statements(stmt));
	return ptr_t();
}

void CartoCSS::Statements::apply_int(ApplyErrorVisitor& vis, LayerParameters& par, LayerAllocator& lalloc, prefixallocators_t& pallocs, const std::string& layer) const
{
	for (statements_t::const_iterator i(m_statements.begin()), e(m_statements.end()); i != e; ++i)
		(*i)->apply(vis, par, lalloc, pallocs, layer);
}

void CartoCSS::Statements::apply_int(ApplyErrorVisitor& vis, LayerGlobalParameters& par, LayerAllocator& lalloc, const std::string& layer, bool layeruncertain) const
{
	for (statements_t::const_iterator i(m_statements.begin()), e(m_statements.end()); i != e; ++i)
		(*i)->apply(vis, par, lalloc, layer, layeruncertain);
}

void CartoCSS::Statements::apply(ApplyErrorVisitor& vis, LayerParameters& par, LayerAllocator& lalloc, prefixallocators_t& pallocs, const std::string& layer) const
{
	vis.visit(*this);
	apply_int(vis, par, lalloc, pallocs, layer);
	vis.visitend(*this);
}

void CartoCSS::Statements::apply(ApplyErrorVisitor& vis, LayerGlobalParameters& par, LayerAllocator& lalloc, const std::string& layer, bool layeruncertain) const
{
	vis.visit(*this);
	apply_int(vis, par, lalloc, layer, layeruncertain);
	vis.visitend(*this);
}

std::ostream& CartoCSS::Statements::print(std::ostream& os, unsigned int indent) const
{
	for (statements_t::const_iterator i(m_statements.begin()), e(m_statements.end()); i != e; ++i) {
		(*i)->print(os, indent);
		if (false)
			os << std::string(indent, ' ') << "// --" << std::endl;
	}
	return os;
}

bool CartoCSS::Statements::kill_variable_assignment_stmt(statements_t& stmt, variable_t var, const std::string& pfx) const
{
	stmt.clear();
	stmt.reserve(m_statements.size());
	bool work(false);
	for (statements_t::const_iterator i(m_statements.begin()), e(m_statements.end()); i != e; ++i) {
		Statement::const_ptr_t p((*i)->kill_variable_assignment(var, pfx));
		work = work || !!p;
		stmt.push_back(p ? p : *i);
	}
	return work;		
}

CartoCSS::Statement::const_ptr_t CartoCSS::Statements::kill_variable_assignment(variable_t var, const std::string& pfx) const
{
	statements_t stmt;
	if (kill_variable_assignment_stmt(stmt, var, pfx))
		return ptr_t(new Statements(stmt));
	return ptr_t();
}

CartoCSS::Statement::const_ptr_t CartoCSS::Statements::merge(const Statement::const_ptr_t& p) const
{
	Statements::const_ptr_t p1(boost::dynamic_pointer_cast<const Statements>(p));
	if (!p1 || boost::dynamic_pointer_cast<const SubBlock>(p1))
		return ptr_t();
	statements_t stmt(p1->get_statements());
	stmt.insert(stmt.begin(), m_statements.begin(), m_statements.end());
	return ptr_t(new Statements(stmt));
}

void CartoCSS::Statements::add_statement(const Statement::const_ptr_t& p)
{
	if (!p)
		return;
	m_statements.push_back(p);
}

void CartoCSS::Statements::add_statements(const statements_t& stmt)
{
	for (const auto& p : stmt)
		add_statement(p);
}

CartoCSS::VariableAssignments::Assignment::Assignment(variable_t var, const Expr::const_ptr_t& expr, const std::string& pfx)
	: m_prefix(pfx), m_expr(expr), m_var(var)
{
}

bool CartoCSS::VariableAssignments::Assignment::simplify(Assignment& a) const
{
	return simplify_int(a);
}

bool CartoCSS::VariableAssignments::Assignment::simplify(Assignment& a, const std::string& field, const Value& value) const
{
	return simplify_int(a, field, value);
}

bool CartoCSS::VariableAssignments::Assignment::simplify(Assignment& a, const Fields& fields) const
{
	return simplify_int(a, fields);
}

bool CartoCSS::VariableAssignments::Assignment::simplify(Assignment& a, const std::string& var, const Expr::const_ptr_t& expr) const
{
	return simplify_int(a, var, expr);
}

bool CartoCSS::VariableAssignments::Assignment::simplify(Assignment& a, const variables_t& vars) const
{
	return simplify_int(a, vars);
}

bool CartoCSS::VariableAssignments::Assignment::simplify(Assignment& a, const LayerID& layer) const
{
	return simplify_int(a, layer);
}

bool CartoCSS::VariableAssignments::Assignment::simplify(Assignment& a, const ClassID& cls) const
{
	return simplify_int(a, cls);
}

bool CartoCSS::VariableAssignments::Assignment::simplify(Assignment& a, Visitor& v) const
{
	return simplify_int(a, v);
}

template <typename... Args>
bool CartoCSS::VariableAssignments::Assignment::simplify_int(Assignment& a, Args&&... args) const
{
	if (!get_expr()) {
		a = Assignment();
		return false;
	}
	Expr::const_ptr_t e(get_expr()->simplify(std::forward<Args>(args)...));
	if (!e) {
		a = *this;
		return false;
	}
	a = Assignment(get_var(), e, get_prefix());
	return true;
}

bool CartoCSS::VariableAssignments::Assignment::apply(ApplyErrorVisitor& vis, ObjectParameters& par, PrefixAllocator& palloc) const
{
	if (get_var() >= variable_t::commonfirst_ && get_var() <= variable_t::commonlast_)
		return false;
	if (!get_expr() || get_var() == variable_t::invalid) {
		vis.error(*this);
		return false;
	}
	try {
		if (par.set_variable(get_var(), palloc.allocate(get_prefix()), get_expr()))
			return true;
		vis.error(*this);
	} catch (const std::exception& e) {
		vis.error(*this, e);
	}
	return false;
}

bool CartoCSS::VariableAssignments::Assignment::apply(ApplyErrorVisitor& vis, GlobalParameters& par, bool layeruncertain) const
{
	if (get_var() < variable_t::commonfirst_ || get_var() > variable_t::commonlast_)
		return false;
	if (!get_expr() || get_var() == variable_t::invalid || layeruncertain) {
		vis.error(*this);
		return false;
	}
	try {
		if (par.set_variable(get_var(), get_expr()))
			return true;
		vis.error(*this);
	} catch (const std::exception& e) {
		vis.error(*this, e);
	}
	return false;
}

std::ostream& CartoCSS::VariableAssignments::Assignment::print(std::ostream& os, unsigned int indent) const
{
	if (!get_expr() || get_var() == variable_t::invalid)
		return os;
	os << std::string(indent, ' ');
	if (!get_prefix().empty())
		os << get_prefix() << '/';
	return os << get_var() << ": " << get_expr()->to_str() << ';' << std::endl;
}

CartoCSS::VariableAssignments::VariableAssignments(void)
{
}

CartoCSS::VariableAssignments::VariableAssignments(const assignments_t& a)
	: m_assignments(a)
{
	remove_superseded_assignments(m_assignments);
}

CartoCSS::VariableAssignments::VariableAssignments(assignments_t&& a)
	: m_assignments(a)
{
	remove_superseded_assignments(m_assignments);
}

CartoCSS::Statement::const_ptr_t CartoCSS::VariableAssignments::simplify(void) const
{
	return simplify_int();
}

CartoCSS::Statement::const_ptr_t CartoCSS::VariableAssignments::simplify(const std::string& field, const Value& value) const
{
	return simplify_int(field, value);
}

CartoCSS::Statement::const_ptr_t CartoCSS::VariableAssignments::simplify(const Fields& fields) const
{
	return simplify_int(fields);
}

CartoCSS::Statement::const_ptr_t CartoCSS::VariableAssignments::simplify(const std::string& var, const Expr::const_ptr_t& expr) const
{
	return simplify_int(var, expr);
}

CartoCSS::Statement::const_ptr_t CartoCSS::VariableAssignments::simplify(const variables_t& vars) const
{
	return simplify_int(vars);
}

CartoCSS::Statement::const_ptr_t CartoCSS::VariableAssignments::simplify(const LayerID& layer) const
{
	return simplify_int(layer);
}

CartoCSS::Statement::const_ptr_t CartoCSS::VariableAssignments::simplify(const ClassID& cls) const
{
	return simplify_int(cls);
}

CartoCSS::Statement::const_ptr_t CartoCSS::VariableAssignments::simplify(Visitor& v) const
{
	Statement::const_ptr_t p(simplify_int(v));
	if (!p)
		return simplify_cb(v);
	Statement::const_ptr_t p1(p->simplify_cb(v));
	return p1 ? p1 : p;
}

void CartoCSS::VariableAssignments::visit(Visitor& v) const
{
	v.visit(*this);
	for (const auto& a : m_assignments)
		a.get_expr()->visit(v);
	v.visitend(*this);
}

bool CartoCSS::VariableAssignments::remove_superseded_assignments(assignments_t& a)
{
	typedef std::set<variable_t> varset_t;
	typedef std::map<std::string,varset_t> varpfxset_t;
	varpfxset_t vps;
	bool work(false);
	for (assignments_t::size_type i(a.size()); i; ) {
		--i;
		{
			varset_t& vs(vps[a[i].get_prefix()]);
			if (vs.insert(a[i].get_var()).second)
				continue;
		}
		work = true;
		if (false) {
			std::cerr << "WARNING: variable ";
			if (!a[i].get_prefix().empty())
				std::cerr << a[i].get_prefix() << '/';
			std::cerr << a[i].get_var() << " = " << a[i].get_expr()->to_str()
				  << " superseded by later assignment" << std::endl;
		}
		a.erase(a.begin() + i);
	}
	return work;
}

template <typename... Args>
CartoCSS::VariableAssignments::ptr_t CartoCSS::VariableAssignments::simplify_int(Args&&... args) const
{
	typedef std::set<variable_t> varset_t;
	typedef std::map<std::string,varset_t> varpfxset_t;
	varpfxset_t vps;
	assignments_t a(m_assignments);
	bool work(remove_superseded_assignments(a));
	for (assignments_t::iterator i(a.begin()), e(a.end()); i != e; ++i) {
		Assignment aa(*i);
		work = aa.simplify(*i, std::forward<Args>(args)...) || work;
	}
	if (work)
		return ptr_t(new VariableAssignments(a));
	return ptr_t();
}

std::ostream& CartoCSS::VariableAssignments::print(std::ostream& os, unsigned int indent) const
{
	for (assignments_t::const_iterator i(m_assignments.begin()), e(m_assignments.end()); i != e; ++i)
		i->print(os, indent);
	return os;
}

CartoCSS::Statement::const_ptr_t CartoCSS::VariableAssignments::kill_variable_assignment(variable_t var, const std::string& pfx) const
{
	bool r(false);
	assignments_t a;
	a.reserve(m_assignments.size());
	for (assignments_t::const_iterator i(m_assignments.begin()), e(m_assignments.end()); i != e; ++i) {
		if (i->get_var() != var || i->get_prefix() != pfx) {
			a.push_back(*i);
			continue;
		}
		r = true;
	}
	if (r)
		return ptr_t(new VariableAssignments(a));
	return ptr_t();
}

CartoCSS::Statement::const_ptr_t CartoCSS::VariableAssignments::merge(const Statement::const_ptr_t& p) const
{
	VariableAssignments::const_ptr_t p1(boost::dynamic_pointer_cast<const VariableAssignments>(p));
	if (!p1)
		return ptr_t();
	assignments_t assgn(p1->get_assignments());
	assgn.insert(assgn.begin(), m_assignments.begin(), m_assignments.end());
	Statement::const_ptr_t p2(new VariableAssignments(assgn));
	Statement::const_ptr_t p3(p->simplify());
	return p3 ? p3 : p2;
}

void CartoCSS::VariableAssignments::add_assignment(const Assignment& a)
{
	m_assignments.push_back(a);
}

void CartoCSS::VariableAssignments::apply(ApplyErrorVisitor& vis, LayerParameters& par, LayerAllocator& lalloc, prefixallocators_t& pallocs, const std::string& layer) const
{
	vis.visit(*this);
	if (m_assignments.empty()) {
		vis.visitend(*this);
		return;
	}
	LayerAllocator::layer_t lid(lalloc.allocate(layer));
	for (assignments_t::const_iterator i(m_assignments.begin()), e(m_assignments.end()); i != e; ++i) {
		std::pair<LayerParameters::iterator,bool> x(par.insert(LayerParameters::value_type(lid, ObjectParameters())));
		std::pair<prefixallocators_t::iterator,bool> y(pallocs.insert(prefixallocators_t::value_type(lid, PrefixAllocator())));
		bool s(i->apply(vis, x.first->second, y.first->second));
		if (!s) {
			if (x.second)
				par.erase(x.first);
			if (y.second)
				pallocs.erase(y.first);
		}
	}
	vis.visitend(*this);
}

void CartoCSS::VariableAssignments::apply(ApplyErrorVisitor& vis, LayerGlobalParameters& par, LayerAllocator& lalloc, const std::string& layer, bool layeruncertain) const
{
	vis.visit(*this);
	if (m_assignments.empty()) {
		vis.visitend(*this);
		return;
	}
	LayerAllocator::layer_t lid(lalloc.allocate(layer));
	for (assignments_t::const_iterator i(m_assignments.begin()), e(m_assignments.end()); i != e; ++i) {
		std::pair<LayerGlobalParameters::iterator,bool> x(par.insert(LayerGlobalParameters::value_type(lid, GlobalParameters())));
		bool s(i->apply(vis, x.first->second, layeruncertain));
		if (!s && x.second)
			par.erase(x.first);
	}
	vis.visitend(*this);
}

CartoCSS::SubBlock::ConditionChain::simplify_t CartoCSS::SubBlock::ConditionChain::simplify(ConditionChain& c) const
{
	return simplify_int(c);
}

CartoCSS::SubBlock::ConditionChain::simplify_t CartoCSS::SubBlock::ConditionChain::simplify(ConditionChain& c, const std::string& field, const Value& value) const
{
	return simplify_int(c, field, value);
}

CartoCSS::SubBlock::ConditionChain::simplify_t CartoCSS::SubBlock::ConditionChain::simplify(ConditionChain& c, const Fields& fields) const
{
	return simplify_int(c, fields);
}

CartoCSS::SubBlock::ConditionChain::simplify_t CartoCSS::SubBlock::ConditionChain::simplify(ConditionChain& c, const std::string& var, const Expr::const_ptr_t& expr) const
{
	return simplify_int(c, var, expr);
}

CartoCSS::SubBlock::ConditionChain::simplify_t CartoCSS::SubBlock::ConditionChain::simplify(ConditionChain& c, const variables_t& vars) const
{
	return simplify_int(c, vars);
}

CartoCSS::SubBlock::ConditionChain::simplify_t CartoCSS::SubBlock::ConditionChain::simplify(ConditionChain& c, const LayerID& layer) const
{
	return simplify_int(c, layer);
}

CartoCSS::SubBlock::ConditionChain::simplify_t CartoCSS::SubBlock::ConditionChain::simplify(ConditionChain& c, const ClassID& cls) const
{
	return simplify_int(c, cls);
}

CartoCSS::SubBlock::ConditionChain::simplify_t CartoCSS::SubBlock::ConditionChain::simplify(ConditionChain& c, Visitor& v) const
{
	return simplify_int(c, v);
}

template <typename... Args>
CartoCSS::SubBlock::ConditionChain::simplify_t CartoCSS::SubBlock::ConditionChain::simplify_int(ConditionChain& c, Args&&... args) const
{
	c.clear();
	c.reserve(size());
	simplify_t work(simplify_t::unchanged);
	for (const_iterator i(begin()), e(end()); i != e; ++i) {
		Expr::const_ptr_t expr((*i)->simplify(std::forward<Args>(args)...));
		if (expr)
			work = simplify_t::changed;
		else
			expr = *i;		
		bool v;
		if (expr->is_convertible(v)) {
			if (!v) {
				c.clear();
				return simplify_t::false_;
			}
			work = simplify_t::changed;
		} else {
			c.push_back(expr);
		}
	}
	if (c.empty())
		return simplify_t::true_;
	return work;
}

bool CartoCSS::SubBlock::ConditionChain::is_copylayer(std::string& layer) const
{
	layer.clear();
	if (size() != 1)
		return false;
	if (!front())
		return false;
	return front()->is_copylayer(layer);
}

bool CartoCSS::SubBlock::ConditionChain::is_condcopylayer(std::string& layer) const
{
	for (const_reverse_iterator i(rbegin()), e(rend()); i != e; ++i)
		if ((*i)->is_copylayer(layer))
			return true;
	layer.clear();
	return false;
}

std::ostream& CartoCSS::SubBlock::ConditionChain::print(std::ostream& os) const
{
	for (Expr::exprlist_t::const_iterator ci(begin()), ce(end()); ci != ce; ++ci) {
		bool paren;
		{
			LayerID x1;
			ClassID x2;
			std::string x3;
		        paren = !(*ci)->is_type(x1) && !(*ci)->is_type(x2) && !(*ci)->is_copylayer(x3);
		}
		if (paren)
			os << '[';
		os << (*ci)->to_str();
		if (paren)
			os << ']';
	}
	return os;
}

CartoCSS::SubBlock::Conditions::simplify_t CartoCSS::SubBlock::Conditions::simplify(Conditions& c) const
{
	return simplify_int(c);
}

CartoCSS::SubBlock::Conditions::simplify_t CartoCSS::SubBlock::Conditions::simplify(Conditions& c, const std::string& field, const Value& value) const
{
	return simplify_int(c, field, value);
}

CartoCSS::SubBlock::Conditions::simplify_t CartoCSS::SubBlock::Conditions::simplify(Conditions& c, const Fields& fields) const
{
	return simplify_int(c, fields);
}

CartoCSS::SubBlock::Conditions::simplify_t CartoCSS::SubBlock::Conditions::simplify(Conditions& c, const std::string& var, const Expr::const_ptr_t& expr) const
{
	return simplify_int(c, var, expr);
}

CartoCSS::SubBlock::Conditions::simplify_t CartoCSS::SubBlock::Conditions::simplify(Conditions& c, const variables_t& vars) const
{
	return simplify_int(c, vars);
}

CartoCSS::SubBlock::Conditions::simplify_t CartoCSS::SubBlock::Conditions::simplify(Conditions& c, const LayerID& layer) const
{
	return simplify_int(c, layer);
}

CartoCSS::SubBlock::Conditions::simplify_t CartoCSS::SubBlock::Conditions::simplify(Conditions& c, const ClassID& cls) const
{
	return simplify_int(c, cls);
}

CartoCSS::SubBlock::Conditions::simplify_t CartoCSS::SubBlock::Conditions::simplify(Conditions& c, Visitor& v) const
{
	return simplify_int(c, v);
}

template <typename... Args>
CartoCSS::SubBlock::Conditions::simplify_t CartoCSS::SubBlock::Conditions::simplify_int(Conditions& c, Args&&... args) const
{
	c.clear();
	c.reserve(size());
	simplify_t work(simplify_t::unchanged);
	for (const_iterator i(begin()), e(end()); i != e; ++i) {
		c.push_back(ConditionChain());
		ConditionChain::simplify_t r(i->simplify(c.back(), std::forward<Args>(args)...));
		if (r == ConditionChain::simplify_t::true_) {
			c.clear();
			return simplify_t::true_;
		}
		if (r == ConditionChain::simplify_t::false_ || r == ConditionChain::simplify_t::changed)
			work = simplify_t::changed;
		if (r == ConditionChain::simplify_t::false_)
			c.resize(c.size() - 1);
	}
	if (c.empty())
		return simplify_t::false_;
	return work;
}

bool CartoCSS::SubBlock::Conditions::is_copylayer(std::set<std::string>& layer) const
{
	layer.clear();
	if (empty())
		return false;
	bool ret(true);
	for (Conditions::const_iterator i(begin()), e(end()); i != e; ++i) {
		std::string l;
		bool ret1(i->is_copylayer(l));
		if (ret1)
			layer.insert(l);
		ret = ret && ret1;
	}
	return ret;
}

bool CartoCSS::SubBlock::Conditions::is_condcopylayer(std::set<std::string>& layer) const
{
	layer.clear();
	if (empty())
		return false;
	bool all(true), none(true);
	for (Conditions::const_iterator i(begin()), e(end()); i != e; ++i) {
		std::string l;
		bool ret1(i->is_condcopylayer(l));
		if (ret1)
			layer.insert(l);
		all = all && ret1;
		none = none && !ret1;
	}
	return all || none;
}

std::ostream& CartoCSS::SubBlock::Conditions::print(std::ostream& os, unsigned int indent) const
{
	bool cond(false);
	for (Conditions::const_iterator i(begin()), e(end()); i != e; ++i) {
		if (cond)
			os << ',' << std::endl;
		cond = true;
		i->print(os << std::string(indent, ' '));
	}
	return os << std::string(cond ? 1 : indent, ' ');
}

CartoCSS::SubBlock::SubBlock(void)
{
}

CartoCSS::SubBlock::SubBlock(const Conditions& cond, const statements_t& stmt)
	: Statements(stmt), m_conditions(cond)
{
}

CartoCSS::SubBlock::SubBlock(Conditions&& cond, statements_t&& stmt)
	: Statements(stmt), m_conditions(cond)
{
}

CartoCSS::Statement::const_ptr_t CartoCSS::SubBlock::simplify(void) const
{
	return simplify_int();
}

CartoCSS::Statement::const_ptr_t CartoCSS::SubBlock::simplify(const std::string& field, const Value& value) const
{
	return simplify_int(field, value);
}

CartoCSS::Statement::const_ptr_t CartoCSS::SubBlock::simplify(const Fields& fields) const
{
	return simplify_int(fields);
}

CartoCSS::Statement::const_ptr_t CartoCSS::SubBlock::simplify(const std::string& var, const Expr::const_ptr_t& expr) const
{
	return simplify_int(var, expr);
}

CartoCSS::Statement::const_ptr_t CartoCSS::SubBlock::simplify(const variables_t& vars) const
{
	return simplify_int(vars);
}

CartoCSS::Statement::const_ptr_t CartoCSS::SubBlock::simplify(const LayerID& layer) const
{
	return simplify_int(layer);
}

CartoCSS::Statement::const_ptr_t CartoCSS::SubBlock::simplify(const ClassID& cls) const
{
	return simplify_int(cls);
}

CartoCSS::Statement::const_ptr_t CartoCSS::SubBlock::simplify(Visitor& v) const
{
	Statement::const_ptr_t p(simplify_int(v));
	if (!p)
		return simplify_cb(v);
	Statement::const_ptr_t p1(p->simplify_cb(v));
	return p1 ? p1 : p;
}

void CartoCSS::SubBlock::visit(Visitor& v) const
{
	v.visit(*this);
	for (const auto& cc : m_conditions)
		for (const auto& c : cc)
			c->visit(v);
	v.visitstatements(*this);
	for (const auto& p : m_statements)
		p->visit(v);
	v.visitend(*this);
}

template <typename... Args>
CartoCSS::Statements::ptr_t CartoCSS::SubBlock::simplify_int(Args&&... args) const
{
	Conditions c;
	Conditions::simplify_t cond(m_conditions.simplify(c, std::forward<Args>(args)...));
	if (cond == Conditions::simplify_t::false_)
		return Statements::ptr_t(new Statements());
	bool work(cond != Conditions::simplify_t::unchanged);
	statements_t stmt;
	work = simplify_stmt(stmt, std::forward<Args>(args)...) || work;
	if (!work)
		return ptr_t();
	if (cond == Conditions::simplify_t::true_)
		return Statements::ptr_t(new Statements(stmt));
	ptr_t p(new SubBlock());
	p->m_conditions.swap(c);
	p->m_statements.swap(stmt);
	return p;
}

CartoCSS::Statement::const_ptr_t CartoCSS::SubBlock::kill_variable_assignment(variable_t var, const std::string& pfx) const
{
	statements_t stmt;
	if (kill_variable_assignment_stmt(stmt, var, pfx))
		return ptr_t(new SubBlock(get_conditions(), stmt));
	return ptr_t();
}

void CartoCSS::SubBlock::apply(ApplyErrorVisitor& vis, LayerParameters& par, LayerAllocator& lalloc, prefixallocators_t& pallocs, const std::string& layer) const
{
	std::set<std::string> copylayers;
	vis.visit(*this);
	// check if we can handle the condition
	if (!m_conditions.is_copylayer(copylayers)) {
		copylayers.clear();
		vis.error(*this, copylayers);
		vis.visitend(*this);
		return;
	}
	// return if we have no or multiple layers
	std::string layer1;
	{
		std::set<std::string>::const_iterator i(copylayers.begin()), e(copylayers.end());
		if (i == e) {
			vis.error(*this, copylayers);
			vis.visitend(*this);
			return;
		}
		layer1 = *i++;
		if (i != e) {
			vis.error(*this, copylayers);
			vis.visitend(*this);
			return;
		}
	}
	Statements::apply_int(vis, par, lalloc, pallocs, layer1);
	vis.visitend(*this);
}

void CartoCSS::SubBlock::apply(ApplyErrorVisitor& vis, LayerGlobalParameters& par, LayerAllocator& lalloc, const std::string& layer, bool layeruncertain) const
{
	std::set<std::string> copylayers;
	vis.visit(*this);
	std::string layer1(layer);
	if (!layeruncertain) {
		// check if we can handle the condition
		layeruncertain = !m_conditions.is_condcopylayer(copylayers);
		if (!layeruncertain) {
			std::set<std::string>::const_iterator i(copylayers.begin()), e(copylayers.end());
			if (i != e) {
				layer1 = *i++;
				if (i != e)
					layeruncertain = true;
			}
		}
	}
	Statements::apply_int(vis, par, lalloc, layer1, layeruncertain);
	vis.visitend(*this);	
}

std::ostream& CartoCSS::SubBlock::print(std::ostream& os, unsigned int indent) const
{
	m_conditions.print(os, indent) << '{' << std::endl;
	Statements::print(os, indent + 2);
	return os << std::string(indent, ' ') << '}' << std::endl;
}

void CartoCSS::SubBlock::add_conditions(const ConditionChain& c)
{
	m_conditions.insert(m_conditions.end(), c);
}

void CartoCSS::SubBlock::add_conditions(const Conditions& c)
{
	m_conditions.insert(m_conditions.end(), c.begin(), c.end());
}

void CartoCSS::parse_project(std::istream& is, const std::string& dirname, std::ostream *msg)
{
	if (!is)
                throw std::runtime_error("Cannot read project file");
	std::vector<std::string> mss;
	bool instylesheet(false);
	while (!is.eof()) {
                if (!is)
                        throw std::runtime_error("Project file read error");
                std::string line;
                std::getline(is, line);
                if (is.bad())
                        throw std::runtime_error("Project file read error");
		if (!line.compare(0, 11, "Stylesheet:")) {
			instylesheet = true;
			continue;
		}
		if (!instylesheet)
			continue;
		std::string::const_iterator li(line.begin()), le(line.end());
		if (li == le || !std::isspace(*li)) {
			instylesheet = false;
			continue;
		}
		while (li != le && std::isspace(*li))
			++li;
		if (li == le || *li != '-') {
			instylesheet = false;
			continue;
		}
		++li;
		while (li != le && std::isspace(*li))
			++li;
		mss.push_back(Glib::build_filename(dirname, std::string(li, le)));
	}
	parse_stylefiles(mss, dirname, msg);
}

void CartoCSS::parse_project(const std::string& fn, std::ostream *msg)
{
	std::ifstream is(fn.c_str());
	if (!is.is_open())
		throw std::runtime_error("Cannot open file " + fn);
	parse_project(is, Glib::path_get_dirname(fn));
}
