//
// C++ Implementation: cartorenderdb
//
// Description: CartoCSS Rendering, database layer queries
//
//
// Author: Thomas Sailer <t.sailer@alumni.ethz.ch>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glibmm.h>
#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <cmath>

#include "cartorender.h"

bool CartoRender::is_layer_enabled(layerid_t id, zoom_t zoom)
{
	switch (id) {
	default:
		return false;

	case layerid_t::addresses:
		if (zoom < 17)
			return false;
		return true;

	case layerid_t::adminhighzoom:
		if (zoom < 13)
			return false;
		return true;

	case layerid_t::adminlowzoom:
		if (zoom < 4 || zoom > 7)
			return false;
		return true;

	case layerid_t::adminmidzoom:
		if (zoom < 8 || zoom > 12)
			return false;
		return true;

	case layerid_t::admintext:
		if (zoom < 11)
			return false;
		return true;

	case layerid_t::aerialways:
		if (zoom < 12)
			return false;
		return true;

	case layerid_t::aeroways:
		if (zoom < 11)
			return false;
		return true;

	case layerid_t::amenityline:
		if (zoom < 16)
			return false;
		return true;

	case layerid_t::amenitylowpriority:
		if (zoom < 14)
			return false;
		return true;

	case layerid_t::amenitypoints:
		if (zoom < 10)
			return false;
		return true;

	case layerid_t::barriers:
		if (zoom < 15)
			return false;
		return true;

	case layerid_t::bridge:
		if (zoom < 12)
			return false;
		return true;

	case layerid_t::bridges:
		if (zoom < 10)
			return false;
		return true;

	case layerid_t::bridgetext:
		if (zoom < 11)
			return false;
		return true;

	case layerid_t::buildings:
		if (zoom < 14)
			return false;
		return true;

	case layerid_t::buildingtext:
		if (zoom < 14)
			return false;
		return true;

	case layerid_t::capitalnames:
		if (zoom < 3 || zoom > 15)
			return false;
		return true;

	case layerid_t::cliffs:
		if (zoom < 13)
			return false;
		return true;

	case layerid_t::countrynames:
		if (zoom < 2)
			return false;
		return true;

	case layerid_t::countynames:
		if (zoom < 8)
			return false;
		return true;

	case layerid_t::entrances:
		if (zoom < 18)
			return false;
		return true;

	case layerid_t::ferryroutes:
		if (zoom < 8)
			return false;
		return true;

	case layerid_t::ferryroutestext:
		if (zoom < 13)
			return false;
		return true;

	case layerid_t::guideways:
		if (zoom < 11)
			return false;
		return true;

	case layerid_t::highwayareacasing:
		if (zoom < 14)
			return false;
		return true;

	case layerid_t::highwayareafill:
		if (zoom < 14)
			return false;
		return true;

	case layerid_t::icesheetoutlines:
		if (zoom < 5)
			return false;
		return true;

	case layerid_t::icesheetpoly:
		if (zoom < 5)
			return false;
		return true;

	case layerid_t::interpolation:
		if (zoom < 17)
			return false;
		return true;

	case layerid_t::junctions:
		if (zoom < 11)
			return false;
		return true;

	case layerid_t::landcover:
		if (zoom < 10)
			return false;
		return true;

	case layerid_t::landcoverareasymbols:
		if (zoom < 5)
			return false;
		return true;

	case layerid_t::landcoverline:
		if (zoom < 14)
			return false;
		return true;

	case layerid_t::landcoverlowzoom:
		if (zoom < 5 || zoom > 9)
			return false;
		return true;

	case layerid_t::landuseoverlay:
		if (zoom < 8)
			return false;
		return true;

	case layerid_t::marinasarea:
		if (zoom < 14)
			return false;
		return true;

	case layerid_t::necountries:
		if (zoom < 1 || zoom > 3)
			return false;
		return true;

	case layerid_t::ocean:
		if (zoom < 10)
			return false;
		return true;

	case layerid_t::oceanlz:
		if (zoom > 9)
			return false;
		return true;

	case layerid_t::pathstextname:
		if (zoom < 15)
			return false;
		return true;

	case layerid_t::piersline:
		if (zoom < 12)
			return false;
		return true;

	case layerid_t::pierspoly:
		if (zoom < 12)
			return false;
		return true;

	case layerid_t::placenamesmedium:
		if (zoom < 4 || zoom > 15)
			return false;
		return true;

	case layerid_t::placenamessmall:
		if (zoom < 12)
			return false;
		return true;

	case layerid_t::powerline:
		if (zoom < 14)
			return false;
		return true;

	case layerid_t::powerminorline:
		if (zoom < 16)
			return false;
		return true;

	case layerid_t::powertowers:
		if (zoom < 14)
			return false;
		return true;

	case layerid_t::protectedareas:
		if (zoom < 8)
			return false;
		return true;

	case layerid_t::protectedareastext:
		if (zoom < 13)
			return false;
		return true;

	case layerid_t::railwaystextname:
		if (zoom < 11)
			return false;
		return true;

	case layerid_t::roadsareatextname:
		if (zoom < 15)
			return false;
		return true;

	case layerid_t::roadscasing:
	case layerid_t::roadsfill:
		if (zoom < 10)
			return false;
		return true;

	case layerid_t::roadslowzoom:
		if (zoom < 6 || zoom > 9)
			return false;
		return true;

	case layerid_t::roadstextname:
		if (zoom < 13)
			return false;
		return true;

	case layerid_t::roadstextref:
		if (zoom < 13)
			return false;
		return true;

	case layerid_t::roadstextreflowzoom:
		if (zoom < 10 || zoom > 12)
			return false;
		return true;

	case layerid_t::roadstextrefminor:
		if (zoom < 15)
			return false;
		return true;

	case layerid_t::statenames:
		if (zoom < 4)
			return false;
		return true;

	case layerid_t::stations:
		if (zoom < 12)
			return false;
		return true;

	case layerid_t::textline:
		if (zoom < 10)
			return false;
		return true;

	case layerid_t::textlowpriority:
		if (zoom < 17)
			return false;
		return true;

	case layerid_t::textpoint:
		if (zoom < 10)
			return false;
		return true;

	case layerid_t::textpolylowzoom:
		if (zoom < 0 || zoom > 9)
			return false;
		return true;

	case layerid_t::tourismboundary:
		if (zoom < 10)
			return false;
		return true;

	case layerid_t::trees:
		if (zoom < 16)
			return false;
		return true;

	case layerid_t::tunnels:
		if (zoom < 10)
			return false;
		return true;

	case layerid_t::turningcirclecasing:
	case layerid_t::turningcirclefill:
		if (zoom < 15)
			return false;
		return true;

	case layerid_t::waterareas:
		if (zoom < 0)
			return false;
		return true;

	case layerid_t::waterbarriersline:
		if (zoom < 13)
			return false;
		return true;

	case layerid_t::waterbarrierspoint:
		if (zoom < 17)
			return false;
		return true;

	case layerid_t::waterbarrierspoly:
		if (zoom < 13)
			return false;
		return true;

	case layerid_t::waterlines:
		if (zoom < 12)
			return false;
		return true;

	case layerid_t::waterlinescasing:
		if (zoom < 13)
			return false;
		return true;

	case layerid_t::waterlineslowzoom:
		if (zoom < 8 || zoom > 11)
			return false;
		return true;

	case layerid_t::waterlinestext:
		if (zoom < 13)
			return false;
		return true;

	case layerid_t::waterwaybridges:
		if (zoom < 12)
			return false;
		return true;
	}	
}

CartoRender::renderobjs_t CartoRender::dbquery(layerid_t id) const
{
	if (!is_layer_enabled(id))
		return renderobjs_t();
	switch (id) {
	default:
		return renderobjs_t();

	case layerid_t::addresses:
		return dbquery_addresses();

	case layerid_t::adminhighzoom:
		return dbquery_adminhighzoom();

	case layerid_t::adminlowzoom:
		return dbquery_adminlowzoom();

	case layerid_t::adminmidzoom:
		return dbquery_adminmidzoom();

	case layerid_t::admintext:
		return dbquery_admintext();

	case layerid_t::aerialways:
		return dbquery_aerialways();

	case layerid_t::aeroways:
		return dbquery_aeroways();

	case layerid_t::amenityline:
		return dbquery_amenityline();

	case layerid_t::amenitylowpriority:
		return dbquery_amenitylowpriority();

	case layerid_t::amenitypoints:
		return dbquery_amenitypoints();

	case layerid_t::barriers:
		return dbquery_barriers();

	case layerid_t::bridge:
		return dbquery_bridge();

	case layerid_t::bridges:
		return dbquery_bridges();

	case layerid_t::bridgetext:
		return dbquery_bridgetext();

	case layerid_t::buildings:
		return dbquery_buildings();

	case layerid_t::buildingtext:
		return dbquery_buildingtext();

	case layerid_t::capitalnames:
		return dbquery_capitalnames();

	case layerid_t::cliffs:
		return dbquery_cliffs();

	case layerid_t::countrynames:
		return dbquery_countrynames();

	case layerid_t::countynames:
		return dbquery_countynames();

	case layerid_t::entrances:
		return dbquery_entrances();

	case layerid_t::ferryroutes:
		return dbquery_ferryroutes();

	case layerid_t::ferryroutestext:
		return dbquery_ferryroutestext();

	case layerid_t::guideways:
		return dbquery_guideways();

	case layerid_t::highwayareacasing:
		return dbquery_highwayareacasing();

	case layerid_t::highwayareafill:
		return dbquery_highwayareafill();

	case layerid_t::icesheetoutlines:
		return dbquery_icesheetoutlines();

	case layerid_t::icesheetpoly:
		return dbquery_icesheetpoly();

	case layerid_t::interpolation:
		return dbquery_interpolation();

	case layerid_t::junctions:
		return dbquery_junctions();

	case layerid_t::landcover:
		return dbquery_landcover();

	case layerid_t::landcoverareasymbols:
		return dbquery_landcoverareasymbols();

	case layerid_t::landcoverline:
		return dbquery_landcoverline();

	case layerid_t::landcoverlowzoom:
		return dbquery_landcoverlowzoom();

	case layerid_t::landuseoverlay:
		return dbquery_landuseoverlay();

	case layerid_t::marinasarea:
		return dbquery_marinasarea();

	case layerid_t::necountries:
		return dbquery_necountries();

	case layerid_t::ocean:
		return dbquery_ocean();

	case layerid_t::oceanlz:
		return dbquery_oceanlz();

	case layerid_t::pathstextname:
		return dbquery_pathstextname();

	case layerid_t::piersline:
		return dbquery_piersline();

	case layerid_t::pierspoly:
		return dbquery_pierspoly();

	case layerid_t::placenamesmedium:
		return dbquery_placenamesmedium();

	case layerid_t::placenamessmall:
		return dbquery_placenamessmall();

	case layerid_t::powerline:
		return dbquery_powerline();

	case layerid_t::powerminorline:
		return dbquery_powerminorline();

	case layerid_t::powertowers:
		return dbquery_powertowers();

	case layerid_t::protectedareas:
		return dbquery_protectedareas();

	case layerid_t::protectedareastext:
		return dbquery_protectedareastext();

	case layerid_t::railwaystextname:
		return dbquery_railwaystextname();

	case layerid_t::roadsareatextname:
		return dbquery_roadsareatextname();

	case layerid_t::roadscasing:
	case layerid_t::roadsfill:
		return dbquery_roads();

	case layerid_t::roadslowzoom:
		return dbquery_roadslowzoom();

	case layerid_t::roadstextname:
		return dbquery_roadstextname();

	case layerid_t::roadstextref:
		return dbquery_roadstextref();

	case layerid_t::roadstextreflowzoom:
		return dbquery_roadstextreflowzoom();

	case layerid_t::roadstextrefminor:
		return dbquery_roadstextrefminor();

	case layerid_t::statenames:
		return dbquery_statenames();

	case layerid_t::stations:
		return dbquery_stations();

	case layerid_t::textline:
		return dbquery_textline();

	case layerid_t::textlowpriority:
		return dbquery_amenitylowpriority();

	case layerid_t::textpoint:
		return dbquery_amenitypoints();

	case layerid_t::textpolylowzoom:
		return dbquery_textpolylowzoom();

	case layerid_t::tourismboundary:
		return dbquery_tourismboundary();

	case layerid_t::trees:
		return dbquery_trees();

	case layerid_t::tunnels:
		return dbquery_tunnels();

	case layerid_t::turningcirclecasing:
	case layerid_t::turningcirclefill:
		return dbquery_turningcircle();

	case layerid_t::waterareas:
		return dbquery_waterareas();

	case layerid_t::waterbarriersline:
		return dbquery_waterbarriersline();

	case layerid_t::waterbarrierspoint:
		return dbquery_waterbarrierspoint();

	case layerid_t::waterbarrierspoly:
		return dbquery_waterbarrierspoly();

	case layerid_t::waterlines:
		return dbquery_waterlines();

	case layerid_t::waterlinescasing:
		return dbquery_waterlinescasing();

	case layerid_t::waterlineslowzoom:
		return dbquery_waterlineslowzoom();

	case layerid_t::waterlinestext:
		return dbquery_waterlinestext();

	case layerid_t::waterwaybridges:
		return dbquery_waterwaybridges();
	}
}

// Sorter Classes

class CartoRender::DbQueryNoSorter {
protected:
	static constexpr bool coalesce_sameattrobj = true;
	static constexpr bool coalesce_multilines = true;
	static constexpr bool sortarea_multipolygons = true;

	class Obj {
	public:
		typedef boost::intrusive_ptr<Obj> ptr_t;
		typedef boost::intrusive_ptr<const Obj> const_ptr_t;

		Obj(const RenderObj::ptr_t& ro) : m_obj(ro), m_refcount(0) {
			if (ro)
				m_attr = ro->get_binary_attributes();
		}
		virtual ~Obj() {}

		unsigned int breference(void) const { return ++m_refcount; }
		unsigned int bunreference(void) const { return --m_refcount; }
		unsigned int get_refcount(void) const { return m_refcount; }
		ptr_t get_ptr(void) { return ptr_t(this); }
		const_ptr_t get_ptr(void) const { return const_ptr_t(this); }
		friend inline void intrusive_ptr_add_ref(const Obj *expr) { expr->breference(); }
		friend inline void intrusive_ptr_release(const Obj *expr) { if (!expr->bunreference()) delete expr; }

		const std::string& get_attr(void) const { return m_attr; }
		operator const RenderObj::ptr_t&(void) const { return m_obj; }
		RenderObjPoints::ptr_t get_point(void) const { return boost::dynamic_pointer_cast<RenderObjPoints>(m_obj); }
		RenderObjLines::ptr_t get_line(void) const { return boost::dynamic_pointer_cast<RenderObjLines>(m_obj); }
		RenderObjPolygons::ptr_t get_polygon(void) const { return boost::dynamic_pointer_cast<RenderObjPolygons>(m_obj); }
		virtual int compare(const Obj& x) const { return m_attr.compare(x.m_attr); }
		virtual int compare_coalesce(const Obj& x) const { return Obj::compare(x); }
		virtual bool coalesce(const Obj& x) {
			if (compare_coalesce(x))
				return false;
			{
				RenderObjPoints::ptr_t p0(get_point()), p1(x.get_point());
				if (p0 && p1) {
					p0->merge_ids(p1->get_ids());
					MultiPoint& x0(p0->get_points());
					const MultiPoint& x1(p1->get_points());
					x0.insert(x0.end(), x1.begin(), x1.end());
					return true;
				}
			}
			{
				RenderObjLines::ptr_t p0(get_line()), p1(x.get_line());
				if (p0 && p1) {
					p0->merge_ids(p1->get_ids());
					MultiLineString& x0(p0->get_lines());
					const MultiLineString& x1(p1->get_lines());
					x0.insert(x0.end(), x1.begin(), x1.end());
					return true;
				}
			}
			{
				RenderObjPolygons::ptr_t p0(get_polygon()), p1(x.get_polygon());
				if (p0 && p1) {
					p0->merge_ids(p1->get_ids());
					MultiPolygonHole& x0(p0->get_polygons());
					const MultiPolygonHole& x1(p1->get_polygons());
					x0.insert(x0.end(), x1.begin(), x1.end());
					return true;
				}
			}
			return false;
		}
		std::ostream& debugprint(std::ostream& os) const {
			if (!m_obj)
				return os;
			m_obj->print(os, 2) << std::endl;
			if (true) {
				std::cerr << "    Binary Attributes:";
				std::ostringstream blob;
				blob << std::hex;
				for (char ch : m_attr)
					blob << ' ' << std::setw(2) << std::setfill('0') << (ch & 0xff);
				std::cerr << blob.str() << std::endl;
			}
			return os;
		}

	protected:
		RenderObj::ptr_t m_obj;
		std::string m_attr;
		mutable std::atomic<unsigned int> m_refcount;
	};

	struct Sorter {
		bool operator()(const Obj& a, const Obj& b) const { return a.compare(b) < 0; }
		bool operator()(const Obj::const_ptr_t& a, const Obj::const_ptr_t& b) const {
			if (!b)
				return false;
			if (!a)
				return true;
			return operator()(*a, *b);
		}
	};

public:
	void add(const RenderObj::ptr_t& ro) { add(new Obj(ro)); }
	renderobjs_t result(void);

protected:
	typedef std::vector<Obj::ptr_t> objs_t;
	objs_t m_objs, m_points, m_lines, m_polygons;

	void add(const Obj::ptr_t& p);
	void coalesce(objs_t& objs);
	void coalesce(void);
};

void CartoRender::DbQueryNoSorter::add(const Obj::ptr_t& p)
{
	if (!p || !static_cast<const RenderObj::ptr_t&>(*p))
		return;
	if (coalesce_sameattrobj) {
		if (p->get_point()) {
			m_points.push_back(p);
			return;
		}
		if (p->get_line()) {
			m_lines.push_back(p);
			return;
		}
		if (p->get_polygon()) {
			m_polygons.push_back(p);
			return;
		}
	}
	m_objs.push_back(p);
}

void CartoRender::DbQueryNoSorter::coalesce(objs_t& objs)
{
	std::sort(objs.begin(), objs.end(), Sorter());
	objs_t::iterator i(objs.begin()), e(objs.end());
	while (i != e) {
		objs_t::iterator i0(i);
		++i;
		if (i == e)
			break;
		if (!(*i0)->coalesce(**i)) {
			if (false && (*i0)->get_attr() == (*i)->get_attr()) {
				std::cerr << "Cannot coalesce: " << (*i0)->compare_coalesce(**i) << '/' << (*i)->compare_coalesce(**i0)
					  << " NoSorter " << (*i0)->Obj::compare_coalesce(**i) << '/' << (*i)->Obj::compare_coalesce(**i0) << std::endl;
				(*i0)->debugprint(std::cerr);
				(*i)->debugprint(std::cerr);
			}
			continue;
		}
		i = objs.erase(i);
		e = objs.end();
		--i;
	}
}

void CartoRender::DbQueryNoSorter::coalesce(void)
{
	coalesce(m_points);
	coalesce(m_lines);
	coalesce(m_polygons);
	m_objs.insert(m_objs.end(), m_points.begin(), m_points.end());
	m_objs.insert(m_objs.end(), m_lines.begin(), m_lines.end());
	m_objs.insert(m_objs.end(), m_polygons.begin(), m_polygons.end());
	m_points.clear();
	m_lines.clear();
	m_polygons.clear();
	std::sort(m_objs.begin(), m_objs.end(), Sorter());
}

CartoRender::renderobjs_t CartoRender::DbQueryNoSorter::result(void)
{
	coalesce();
	renderobjs_t r;
	r.reserve(m_objs.size());
	for (const auto& x : m_objs) {
		if (coalesce_multilines)
			static_cast<const RenderObj::ptr_t&>(*x)->coalesce_multilines();
		if (sortarea_multipolygons)
			static_cast<const RenderObj::ptr_t&>(*x)->sortarea_multipolygons();
		r.push_back(static_cast<const RenderObj::ptr_t&>(*x));
	}
	return r;
}

class CartoRender::DbQueryWayPixelSorter : public DbQueryNoSorter {
protected:
	class Obj : public DbQueryNoSorter::Obj {
	public:
		Obj(const RenderObj::ptr_t& ro) : DbQueryNoSorter::Obj(ro), m_waypixels(0) {
			if (ro) {
				const RenderObj::fieldval_t& f(ro->find_field("way_pixels"));
				if (!f.is_null()) {
					try {
						m_waypixels = static_cast<double>(f);
					} catch (...) {
						m_waypixels = 0;
					}
				}
			}
		}
		virtual int compare(const DbQueryNoSorter::Obj& xx) const {
			const Obj& x(static_cast<const Obj&>(xx));
			if (x.m_waypixels < m_waypixels)
				return -1;
			if (m_waypixels < x.m_waypixels)
				return 1;
			return DbQueryNoSorter::Obj::compare(x);
		}
		virtual bool coalesce(const DbQueryNoSorter::Obj& xx) {
			const Obj& x(static_cast<const Obj&>(xx));
			if (!DbQueryNoSorter::Obj::coalesce(x))
				return false;
			m_waypixels += x.m_waypixels;
			return true;
		}

	protected:
		double m_waypixels;
	};

public:
	void add(const RenderObj::ptr_t& ro) { DbQueryNoSorter::add(new Obj(ro)); }
};

class CartoRender::DbQueryWayPixelFeatureSorter : public DbQueryNoSorter {
protected:
	class Obj : public DbQueryNoSorter::Obj {
	public:
		Obj(const RenderObj::ptr_t& ro) : DbQueryNoSorter::Obj(ro), m_waypixels(0) {
			if (ro) {
				{
					const RenderObj::fieldval_t& f(ro->find_field("way_pixels"));
					if (!f.is_null()) {
						try {
							m_waypixels = static_cast<double>(f);
						} catch (...) {
							m_waypixels = 0;
						}
					}
				}
				{
					const RenderObj::fieldval_t& f(ro->find_field("feature"));
					if (!f.is_null())
						m_feature = static_cast<std::string>(f);
				}
			}
		}
		virtual int compare(const DbQueryNoSorter::Obj& xx) const {
			const Obj& x(static_cast<const Obj&>(xx));
			if (x.m_waypixels < m_waypixels)
				return -1;
			if (m_waypixels < x.m_waypixels)
				return 1;
			if (m_feature < x.m_feature)
				return -1;
			if (x.m_feature < m_feature)
				return 1;
			return DbQueryNoSorter::Obj::compare(x);
		}
		virtual bool coalesce(const DbQueryNoSorter::Obj& xx) {
			const Obj& x(static_cast<const Obj&>(xx));
			if (!DbQueryNoSorter::Obj::coalesce(x))
				return false;
			m_waypixels += x.m_waypixels;
			return true;
		}

	protected:
		std::string m_feature;
		double m_waypixels;
	};

public:
	void add(const RenderObj::ptr_t& ro) { DbQueryNoSorter::add(new Obj(ro)); }
};

class CartoRender::DbQueryAerowaysSorter : public DbQueryNoSorter {
protected:
	class Obj : public DbQueryNoSorter::Obj {
	public:
		Obj(const RenderObj::ptr_t& ro) : DbQueryNoSorter::Obj(ro), m_runway(false) {
			if (ro) {
				{
					const RenderObj::fieldval_t& f(ro->find_field("aeroway"));
					if (!f.is_null())
						m_runway = static_cast<std::string>(f) == "runway";
				}
				{
					const RenderObj::fieldval_t& f(ro->find_field("bridge"));
					if (!f.is_null())
						m_bridge = static_cast<std::string>(f);
				}
			}
		}
		virtual int compare(const DbQueryNoSorter::Obj& xx) const {
			const Obj& x(static_cast<const Obj&>(xx));
			int c(m_bridge.compare(x.m_bridge));
			if (c)
				return c;
			if (!m_runway && x.m_runway)
				return -1;
			if (m_runway && !x.m_runway)
				return 1;
			return DbQueryNoSorter::Obj::compare(x);
		}

	protected:
		std::string m_bridge;
		bool m_runway;
	};

public:
	void add(const RenderObj::ptr_t& ro) { DbQueryNoSorter::add(new Obj(ro)); }
};

class CartoRender::DbQueryLayerSorter : public DbQueryNoSorter {
protected:
	class Obj : public DbQueryNoSorter::Obj {
	public:
		Obj(const RenderObj::ptr_t& ro) : DbQueryNoSorter::Obj(ro), m_layer(0) {
			if (ro) {
				const RenderObj::fieldval_t& f(ro->find_field("layer"));
				if (!f.is_null()) {
					try {
						m_layer = static_cast<int64_t>(f);
					} catch (...) {
						m_layer = 0;
					}
				}
			}
		}
		Obj(const RenderObj::ptr_t& ro, const char *layer) : DbQueryNoSorter::Obj(ro), m_layer(0) {
			if (layer) {
				char *cp(0);
				m_layer = strtol(layer, &cp, 10);
				if (cp == layer || *cp)
					m_layer = 0;
			}
		}
		virtual int compare(const DbQueryNoSorter::Obj& xx) const {
			const Obj& x(static_cast<const Obj&>(xx));
			if (m_layer < x.m_layer)
				return -1;
			if (x.m_layer < m_layer)
				return 1;
			return DbQueryNoSorter::Obj::compare(x);
		}

	protected:
		long m_layer;
	};

public:
	void add(const RenderObj::ptr_t& ro) { DbQueryNoSorter::add(new Obj(ro)); }
	void add(const RenderObj::ptr_t& ro, const char *layer) { DbQueryNoSorter::add(new Obj(ro, layer)); }
};

class CartoRender::DbQueryRoadsBridgesSorter : public DbQueryNoSorter {
protected:
	class Obj : public DbQueryNoSorter::Obj {
	public:
		Obj(const RenderObj::ptr_t& ro) : DbQueryNoSorter::Obj(ro), m_osmid(0), m_layer(0), m_zorder(0),
						  m_feature(0), m_featuressy(0), m_access(0), m_surface(0) {
			if (ro) {
				if (!ro->get_ids().empty())
					m_osmid = ro->get_ids().front();
				m_zorder = ro->get_zorder();
				{
					const RenderObj::fieldval_t& f(ro->find_field("layer"));
					if (!f.is_null()) {
						try {
							m_layer = static_cast<int64_t>(f);
						} catch (...) {
							m_layer = 0;
						}
					}
				}
				{
					const RenderObj::fieldval_t& f(ro->find_field("feature"));
					if (f.is_string()) {
						std::string v(static_cast<std::string>(f));
						m_feature = v.compare(0, 8, "railway_") ? 1 : 2;
						m_featuressy = !(v == "railway_INT-preserved-ssy" ||
								 v == "railway_INT-spur-siding-yard" ||
								 v == "railway_tram-service");
					}
				}
				{
					const RenderObj::fieldval_t& f(ro->find_field("access"));
					if (f.is_string()) {
						std::string v(static_cast<std::string>(f));
						if (v == "no" || v == "private")
							m_access = 0;
						else if (v == "destination")
							m_access = 1;
						else
							m_access = 2;
					}
				}
				{
					const RenderObj::fieldval_t& f(ro->find_field("int_surface"));
					if (f.is_string()) {
						std::string v(static_cast<std::string>(f));
						m_surface = (v == "unpaved") ? 0 : 2;
					}
				}
			}
		}
		virtual int compare(const DbQueryNoSorter::Obj& xx) const {
			const Obj& x(static_cast<const Obj&>(xx));
			int c(compare_main(x));
			if (c)
				return c;
			if (m_osmid < x.m_osmid)
				return -1;
			if (x.m_osmid < m_osmid)
				return 1;
			return DbQueryNoSorter::Obj::compare(x);
		}
		virtual int compare_coalesce(const Obj& xx) const {
			const Obj& x(static_cast<const Obj&>(xx));
			int c(compare_main(x));
			if (c)
				return c;
			return DbQueryNoSorter::Obj::compare_coalesce(x);
		}

	protected:
		int64_t m_osmid;
		long m_layer;
		int32_t m_zorder;
		int m_feature;
		int m_featuressy;
		int m_access;
		int m_surface;

		int compare_main(const Obj& x) const {
			if (m_layer < x.m_layer)
				return -1;
			if (x.m_layer < m_layer)
				return 1;
			if (m_zorder < x.m_zorder)
				return -1;
			if (x.m_zorder < m_zorder)
				return 1;
			if (m_feature < x.m_feature)
				return -1;
			if (x.m_feature < m_feature)
				return 1;
			if (m_featuressy < x.m_featuressy)
				return -1;
			if (x.m_featuressy < m_featuressy)
				return 1;
			if (m_access < x.m_access)
				return -1;
			if (x.m_access < m_access)
				return 1;
			if (m_surface < x.m_surface)
				return -1;
			if (x.m_surface < m_surface)
				return 1;
			return 0;
		}
	};

public:
	void add(const RenderObj::ptr_t& ro) { DbQueryNoSorter::add(new Obj(ro)); }
};

class CartoRender::DbQueryLayerWayPixelSorter : public DbQueryNoSorter {
protected:
	class Obj : public DbQueryNoSorter::Obj {
	public:
		Obj(const RenderObj::ptr_t& ro) : DbQueryNoSorter::Obj(ro), m_waypixels(0), m_layer(0) {
			if (ro) {
				{
					const RenderObj::fieldval_t& f(ro->find_field("layer"));
					if (!f.is_null()) {
						try {
							m_layer = static_cast<int64_t>(f);
						} catch (...) {
							m_layer = 0;
						}
					}
				}
				{
					const RenderObj::fieldval_t& f(ro->find_field("way_pixels"));
					if (!f.is_null()) {
						try {
							m_waypixels = static_cast<double>(f);
						} catch (...) {
							m_waypixels = 0;
						}
					}
				}
			}
		}
		Obj(const RenderObj::ptr_t& ro, const char *layer) : DbQueryNoSorter::Obj(ro), m_waypixels(0), m_layer(0) {
			if (layer) {
				char *cp(0);
				m_layer = strtol(layer, &cp, 10);
				if (cp == layer || *cp)
					m_layer = 0;
			}
			if (ro) {
				{
					const RenderObj::fieldval_t& f(ro->find_field("way_pixels"));
					if (!f.is_null()) {
						try {
							m_waypixels = static_cast<double>(f);
						} catch (...) {
							m_waypixels = 0;
						}
					}
				}
			}
		}
		Obj(const RenderObj::ptr_t& ro, long layer) : DbQueryNoSorter::Obj(ro), m_waypixels(0), m_layer(layer) {
			if (ro) {
				{
					const RenderObj::fieldval_t& f(ro->find_field("way_pixels"));
					if (!f.is_null()) {
						try {
							m_waypixels = static_cast<double>(f);
						} catch (...) {
							m_waypixels = 0;
						}
					}
				}
			}
		}
		virtual int compare(const DbQueryNoSorter::Obj& xx) const {
			const Obj& x(static_cast<const Obj&>(xx));
			if (m_layer < x.m_layer)
				return -1;
			if (x.m_layer < m_layer)
				return 1;
			if (x.m_waypixels < m_waypixels)
				return -1;
			if (m_waypixels < x.m_waypixels)
				return 1;
			return DbQueryNoSorter::Obj::compare(x);
		}
		virtual bool coalesce(const DbQueryNoSorter::Obj& xx) {
			const Obj& x(static_cast<const Obj&>(xx));
			if (!DbQueryNoSorter::Obj::coalesce(x))
				return false;
			m_waypixels += x.m_waypixels;
			return true;
		}

	protected:
		double m_waypixels;
		long m_layer;
	};

public:
	void add(const RenderObj::ptr_t& ro) { DbQueryNoSorter::add(new Obj(ro)); }
	void add(const RenderObj::ptr_t& ro, const char *layer) { DbQueryNoSorter::add(new Obj(ro, layer)); }
	void add(const RenderObj::ptr_t& ro, long layer) { DbQueryNoSorter::add(new Obj(ro, layer)); }
};

class CartoRender::DbQueryIntegerSorter : public DbQueryNoSorter {
protected:
	class Obj : public DbQueryNoSorter::Obj {
	public:
		Obj(const RenderObj::ptr_t& ro, long order) : DbQueryNoSorter::Obj(ro), m_order(order) {}
		virtual int compare(const DbQueryNoSorter::Obj& xx) const {
			const Obj& x(static_cast<const Obj&>(xx));
			if (m_order < x.m_order)
				return -1;
			if (x.m_order < m_order)
				return 1;
			return DbQueryNoSorter::Obj::compare(x);
		}

	protected:
		long m_order;
	};

public:
	void add(const RenderObj::ptr_t& ro, long order) { DbQueryNoSorter::add(new Obj(ro, order)); }
};

class CartoRender::DbQueryZLayerNameIdSorter : public DbQueryNoSorter {
protected:
	class Obj : public DbQueryNoSorter::Obj {
	public:
		Obj(const RenderObj::ptr_t& ro) : DbQueryNoSorter::Obj(ro), m_osmid(0), m_layer(0), m_zorder(0) {
			if (ro) {
				if (!ro->get_ids().empty())
					m_osmid = ro->get_ids().front();
				m_zorder = ro->get_zorder();
				{
					const RenderObj::fieldval_t& f(ro->find_field("layer"));
					if (!f.is_null()) {
						try {
							m_layer = static_cast<int64_t>(f);
						} catch (...) {
							m_layer = 0;
						}
					}
				}
				{
					const RenderObj::fieldval_t& f(ro->find_field("name"));
					if (!f.is_null()) {
						try {
							m_name = static_cast<std::string>(f);
						} catch (...) {
							m_name.clear();
						}
					}
				}
			}
		}
		Obj(const RenderObj::ptr_t& ro, const char *layer) : DbQueryNoSorter::Obj(ro), m_osmid(0), m_layer(0), m_zorder(0) {
			if (layer) {
				char *cp(0);
				m_layer = strtol(layer, &cp, 10);
				if (cp == layer || *cp)
					m_layer = 0;
			}
			if (ro) {
				if (!ro->get_ids().empty())
					m_osmid = ro->get_ids().front();
				m_zorder = ro->get_zorder();
				{
					const RenderObj::fieldval_t& f(ro->find_field("name"));
					if (!f.is_null()) {
						try {
							m_name = static_cast<std::string>(f);
						} catch (...) {
							m_name.clear();
						}
					}
				}
			}
		}
		virtual int compare(const DbQueryNoSorter::Obj& xx) const {
			const Obj& x(static_cast<const Obj&>(xx));
			int c(compare_main(x));
			if (c)
				return c;
			if (x.m_osmid < m_osmid)
				return -1;
			if (m_osmid < x.m_osmid)
				return 1;
			return DbQueryNoSorter::Obj::compare(x);
		}
		virtual int compare_coalesce(const Obj& xx) const {
			const Obj& x(static_cast<const Obj&>(xx));
			int c(compare_main(x));
			if (c)
				return c;
			return DbQueryNoSorter::Obj::compare_coalesce(x);
		}

	protected:
		std::string m_name;
		int64_t m_osmid;
		long m_layer;
		int32_t m_zorder;

		int compare_main(const Obj& x) const {
			if (x.m_zorder < m_zorder)
				return -1;
			if (m_zorder < x.m_zorder)
				return 1;
			if (m_layer < x.m_layer)
				return -1;
			if (x.m_layer < m_layer)
				return 1;
			if (x.m_name.size() < m_name.size())
				return -1;
			if (m_name.size() < x.m_name.size())
				return 1;
			return x.m_name.compare(m_name);
		}
	};

public:
	void add(const RenderObj::ptr_t& ro) { DbQueryNoSorter::add(new Obj(ro)); }
	void add(const RenderObj::ptr_t& ro, const char *layer) { DbQueryNoSorter::add(new Obj(ro, layer)); }
};

class CartoRender::DbQueryZSorter : public DbQueryNoSorter {
protected:
	class Obj : public DbQueryNoSorter::Obj {
	public:
		Obj(const RenderObj::ptr_t& ro) : DbQueryNoSorter::Obj(ro), m_zorder(0) {
			if (ro)
				m_zorder = ro->get_zorder();
		}
		virtual int compare(const DbQueryNoSorter::Obj& xx) const {
			const Obj& x(static_cast<const Obj&>(xx));
			if (x.m_zorder < m_zorder)
				return -1;
			if (m_zorder < x.m_zorder)
				return 1;
			return DbQueryNoSorter::Obj::compare(x);
		}

	protected:
		int32_t m_zorder;
	};

public:
	void add(const RenderObj::ptr_t& ro) { DbQueryNoSorter::add(new Obj(ro)); }
};

class CartoRender::DbQueryAdminLevelSorter : public DbQueryNoSorter {
protected:
	class Obj : public DbQueryNoSorter::Obj {
	public:
		Obj(const RenderObj::ptr_t& ro) : DbQueryNoSorter::Obj(ro), m_adminlevel(0) {
			if (ro) {
				const RenderObj::fieldval_t& f(ro->find_field("admin_level"));
				if (!f.is_null()) {
					try {
						m_adminlevel = static_cast<int64_t>(f);
					} catch (...) {
						m_adminlevel = 0;
					}
				}
			}
		}
		Obj(const RenderObj::ptr_t& ro, const char *adminlevel) : DbQueryNoSorter::Obj(ro), m_adminlevel(0) {
			if (adminlevel) {
				char *cp(0);
				m_adminlevel = strtol(adminlevel, &cp, 10);
				if (cp == adminlevel || *cp)
					m_adminlevel = 0;
			}
		}
		virtual int compare(const DbQueryNoSorter::Obj& xx) const {
			const Obj& x(static_cast<const Obj&>(xx));
			if (x.m_adminlevel < m_adminlevel)
				return -1;
			if (x.m_adminlevel < m_adminlevel)
				return 1;
			return DbQueryNoSorter::Obj::compare(x);
		}

	protected:
		long m_adminlevel;
	};

public:
	void add(const RenderObj::ptr_t& ro) { DbQueryNoSorter::add(new Obj(ro)); }
	void add(const RenderObj::ptr_t& ro, const char *adminlevel) { DbQueryNoSorter::add(new Obj(ro, adminlevel)); }
};

class CartoRender::DbQueryAdminLevelWayPixelSorter : public DbQueryNoSorter {
protected:
	class Obj : public DbQueryNoSorter::Obj {
	public:
		Obj(const RenderObj::ptr_t& ro) : DbQueryNoSorter::Obj(ro), m_waypixels(0), m_adminlevel(0) {
			if (ro) {
				{
					const RenderObj::fieldval_t& f(ro->find_field("way_pixels"));
					if (!f.is_null()) {
						try {
							m_waypixels = static_cast<double>(f);
						} catch (...) {
							m_waypixels = 0;
						}
					}
				}
				{
					const RenderObj::fieldval_t& f(ro->find_field("admin_level"));
					if (!f.is_null()) {
						try {
							m_adminlevel = static_cast<int64_t>(f);
						} catch (...) {
							m_adminlevel = 0;
						}
					}
				}
			}
		}
		virtual int compare(const DbQueryNoSorter::Obj& xx) const {
			const Obj& x(static_cast<const Obj&>(xx));
			if (m_adminlevel < x.m_adminlevel)
				return -1;
			if (x.m_adminlevel < m_adminlevel)
				return 1;
			if (x.m_waypixels < m_waypixels)
				return -1;
			if (m_waypixels < x.m_waypixels)
				return 1;
			return DbQueryNoSorter::Obj::compare(x);
		}
		virtual bool coalesce(const DbQueryNoSorter::Obj& xx) {
			const Obj& x(static_cast<const Obj&>(xx));
			if (!DbQueryNoSorter::Obj::coalesce(x))
				return false;
			m_waypixels += x.m_waypixels;
			return true;
		}

	protected:
		double m_waypixels;
		long m_adminlevel;
	};

public:
	void add(const RenderObj::ptr_t& ro) { DbQueryNoSorter::add(new Obj(ro)); }
};

class CartoRender::DbQueryScoreWayPixelSorter : public DbQueryNoSorter {
protected:
	class Obj : public DbQueryNoSorter::Obj {
	public:
		Obj(const RenderObj::ptr_t& ro) : DbQueryNoSorter::Obj(ro), m_waypixels(0), m_score(0) {
			if (ro) {
				{
					const RenderObj::fieldval_t& f(ro->find_field("way_pixels"));
					if (!f.is_null()) {
						try {
							m_waypixels = static_cast<double>(f);
						} catch (...) {
							m_waypixels = 0;
						}
					}
				}
				{
					const RenderObj::fieldval_t& f(ro->find_field("score"));
					if (!f.is_null()) {
						try {
							m_score = static_cast<double>(f);
						} catch (...) {
							m_score = 0;
						}
					}
				}
			}
		}
		Obj(const RenderObj::ptr_t& ro, double score) : DbQueryNoSorter::Obj(ro), m_waypixels(0), m_score(score) {
			if (ro) {
				const RenderObj::fieldval_t& f(ro->find_field("way_pixels"));
				if (!f.is_null()) {
					try {
						m_waypixels = static_cast<double>(f);
					} catch (...) {
						m_waypixels = 0;
					}
				}
			}
		}
		virtual int compare(const DbQueryNoSorter::Obj& xx) const {
			const Obj& x(static_cast<const Obj&>(xx));
			if (x.m_score < m_score)
				return -1;
			if (m_score < x.m_score)
				return 1;
			if (x.m_waypixels < m_waypixels)
				return -1;
			if (m_waypixels < x.m_waypixels)
				return 1;
			return DbQueryNoSorter::Obj::compare(x);
		}
		virtual bool coalesce(const DbQueryNoSorter::Obj& xx) {
			const Obj& x(static_cast<const Obj&>(xx));
			if (!DbQueryNoSorter::Obj::coalesce(x))
				return false;
			m_waypixels += x.m_waypixels;
			return true;
		}

	protected:
		double m_waypixels;
		double m_score;
	};

public:
	void add(const RenderObj::ptr_t& ro) { DbQueryNoSorter::add(new Obj(ro)); }
	void add(const RenderObj::ptr_t& ro, double score) { DbQueryNoSorter::add(new Obj(ro, score)); }
};

class CartoRender::DbQueryRoadsTextSorter : public DbQueryNoSorter {
protected:
	class Obj : public DbQueryNoSorter::Obj {
	public:
		Obj(const RenderObj::ptr_t& ro) : DbQueryNoSorter::Obj(ro), m_prio(0), m_height(0), m_width(0) {
			if (ro) {
				if (!ro->get_ids().empty())
					m_osmid = ro->get_ids().front();
				{
					const RenderObj::fieldval_t& f(ro->find_field("highway"));
					if (!f.is_null()) {
						try {
							std::string hwy(f);
							if (hwy == "motorway")
								m_prio = 38;
							else if (hwy == "trunk")
								m_prio = 37;
							else if (hwy == "primary")
								m_prio = 36;
							else if (hwy == "secondary")
								m_prio = 35;
							else if (hwy == "tertiary")
								m_prio = 34;
							else if (hwy == "unclassified")
								m_prio = 33;
							else if (hwy == "residential")
								m_prio = 32;
							else if (hwy == "track")
								m_prio = 30;
							else if (hwy == "runway")
								m_prio = 6;
							else if (hwy == "taxiway")
								m_prio = 5;
						} catch (...) {
							m_prio = 0;
						}
					}
				}
				{
					const RenderObj::fieldval_t& f(ro->find_field("refs"));
					if (!f.is_null()) {
						try {
							m_refs = static_cast<std::string>(f);
						} catch (...) {
							m_refs.clear();
						}
					}
				}
				{
					const RenderObj::fieldval_t& f(ro->find_field("height"));
					if (!f.is_null()) {
						try {
							m_height = static_cast<int64_t>(f);
						} catch (...) {
							m_height = 0;
						}
					}
				}
				{
					const RenderObj::fieldval_t& f(ro->find_field("width"));
					if (!f.is_null()) {
						try {
							m_width = static_cast<int64_t>(f);
						} catch (...) {
							m_width = 0;
						}
					}
				}
			}
		}
		virtual int compare(const DbQueryNoSorter::Obj& xx) const {
			const Obj& x(static_cast<const Obj&>(xx));
			int c(compare_main(x));
			if (c)
				return c;
			if (m_osmid < x.m_osmid)
				return -1;
			if (m_osmid < x.m_osmid)
				return 1;
			return DbQueryNoSorter::Obj::compare(x);
		}
		virtual int compare_coalesce(const Obj& xx) const {
			const Obj& x(static_cast<const Obj&>(xx));
			int c(compare_main(x));
			if (c)
				return c;
			return DbQueryNoSorter::Obj::compare_coalesce(x);
		}

	protected:
		std::string m_refs;
		int64_t m_osmid;
		unsigned int m_prio;
		unsigned int m_height;
		unsigned int m_width;

		int compare_main(const Obj& x) const {
			if (x.m_prio < m_prio)
				return -1;
			if (m_prio < x.m_prio)
				return 1;
			if (x.m_height < m_height)
				return -1;
			if (m_height < x.m_height)
				return 1;
			if (x.m_width < m_width)
				return -1;
			if (m_width < x.m_width)
				return 1;
			return m_refs.compare(x.m_refs);
		}
	};

public:
	void add(const RenderObj::ptr_t& ro) { DbQueryNoSorter::add(new Obj(ro)); }
};

class CartoRender::DbQueryTurningCircleSorter : public DbQueryNoSorter {
protected:
	class Obj : public DbQueryNoSorter::Obj {
	public:
		Obj(const RenderObj::ptr_t& ro, const Point& pt, long prio) : DbQueryNoSorter::Obj(ro), m_pt(pt), m_prio(prio) {}
		const Point& get_pt(void) const { return m_pt; }
		virtual int compare(const DbQueryNoSorter::Obj& xx) const {
			const Obj& x(static_cast<const Obj&>(xx));
			int c(m_pt.compare(x.m_pt));
			if (c)
				return c;
			if (m_prio < x.m_prio)
				return -1;
			if (x.m_prio < m_prio)
				return 1;
			return DbQueryNoSorter::Obj::compare(x);
		}

	protected:
		Point m_pt;
		long m_prio;
	};

public:
	void add(const RenderObj::ptr_t& ro, const Point& pt, long prio) { DbQueryNoSorter::add(new Obj(ro, pt, prio)); }
	renderobjs_t result(void);
};

CartoRender::renderobjs_t CartoRender::DbQueryTurningCircleSorter::result(void)
{
	coalesce();
	renderobjs_t r;
	r.reserve(m_objs.size());
	Point pt(Point::invalid);
	for (const auto& x : m_objs) {
		boost::intrusive_ptr<Obj> xx(boost::static_pointer_cast<Obj>(x));
		if (xx->get_pt() == pt)
			continue;
		if (coalesce_multilines)
			static_cast<const RenderObj::ptr_t&>(*x)->coalesce_multilines();
		r.push_back(static_cast<const RenderObj::ptr_t&>(*x));
		pt = xx->get_pt();
	}
	return r;
}

class CartoRender::DbQueryScoreNameSorter : public DbQueryNoSorter {
protected:
	class Obj : public DbQueryNoSorter::Obj {
	public:
		Obj(const RenderObj::ptr_t& ro) : DbQueryNoSorter::Obj(ro), m_score(0) {
			if (ro) {
				{
					const RenderObj::fieldval_t& f(ro->find_field("score"));
					if (!f.is_null()) {
						try {
							m_score = static_cast<int64_t>(f);
						} catch (...) {
							m_score = 0;
						}
					}
				}
				{
					const RenderObj::fieldval_t& f(ro->find_field("name"));
					if (!f.is_null()) {
						try {
							m_name = static_cast<std::string>(f);
						} catch (...) {
							m_name.clear();
						}
					}
				}
			}
		}
		Obj(const RenderObj::ptr_t& ro, long score) : DbQueryNoSorter::Obj(ro), m_score(score) {
			if (ro) {
				{
					const RenderObj::fieldval_t& f(ro->find_field("name"));
					if (!f.is_null()) {
						try {
							m_name = static_cast<std::string>(f);
						} catch (...) {
							m_name.clear();
						}
					}
				}
			}
		}
		virtual int compare(const DbQueryNoSorter::Obj& xx) const {
			const Obj& x(static_cast<const Obj&>(xx));
			if (x.m_score < m_score)
				return -1;
			if (m_score < x.m_score)
				return 1;
			if (x.m_name.size() < m_name.size())
				return -1;
			if (m_name.size() < x.m_name.size())
				return 1;
			int c(x.m_name.compare(m_name));
			if (c)
				return c;
			return DbQueryNoSorter::Obj::compare(x);
		}

	protected:
		std::string m_name;
		long m_score;
	};

public:
	void add(const RenderObj::ptr_t& ro) { DbQueryNoSorter::add(new Obj(ro)); }
	void add(const RenderObj::ptr_t& ro, long score) { DbQueryNoSorter::add(new Obj(ro, score)); }
};

// Layers

CartoRender::renderobjs_t CartoRender::dbquery_addresses(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	DbQueryWayPixelSorter robjs;
	tagkey_t key_addrhousenumber(odb->find_tagkey("addr:housenumber"));
	tagkey_t key_addrhousename(odb->find_tagkey("addr:housename"));
	tagkey_t key_addrunit(odb->find_tagkey("addr:unit"));
	tagkey_t key_addrflats(odb->find_tagkey("addr:flats"));
	tagkey_t key_building(odb->find_tagkey("building"));
	if (key_building != tagkey_t::invalid) {
		for (const OSMDB::Object::const_ptr_t& pobj : m_areas) {
			tagname_t val_addrhousenumber(pobj->find_tag(key_addrhousenumber));
			tagname_t val_addrhousename(pobj->find_tag(key_addrhousename));
			tagname_t val_addrunit(pobj->find_tag(key_addrunit));
			tagname_t val_addrflats(pobj->find_tag(key_addrflats));
			tagname_t val_building(pobj->find_tag(key_building));
			if (val_building == tagname_t::invalid)
				continue;
			if (val_addrhousenumber == tagname_t::invalid && val_addrhousename == tagname_t::invalid &&
			    val_addrunit == tagname_t::invalid && val_addrflats == tagname_t::invalid)
				continue;
			RenderObjPoints::ptr_t ro(create_points(boost::dynamic_pointer_cast<const OSMDB::ObjArea>(pobj)));
			if (!ro)
				continue;
			ro->copy_field_string(*odb, "addr_housenumber", val_addrhousenumber);
			ro->copy_field_string(*odb, "addr_housename", val_addrhousename);
			ro->copy_field_string(*odb, "addr_unit", val_addrunit);
			ro->copy_field_string(*odb, "addr_flats", val_addrflats);
			robjs.add(ro);
		}
	}
	for (const OSMDB::Object::const_ptr_t& pobj : m_points) {
		tagname_t val_addrhousenumber(pobj->find_tag(key_addrhousenumber));
		tagname_t val_addrhousename(pobj->find_tag(key_addrhousename));
		tagname_t val_addrunit(pobj->find_tag(key_addrunit));
		tagname_t val_addrflats(pobj->find_tag(key_addrflats));
		if (val_addrhousenumber == tagname_t::invalid && val_addrhousename == tagname_t::invalid &&
		    val_addrunit == tagname_t::invalid && val_addrflats == tagname_t::invalid)
			continue;
		RenderObjPoints::ptr_t ro(create_points(boost::dynamic_pointer_cast<const OSMDB::ObjPoint>(pobj)));
		if (!ro)
			continue;
		ro->set_field("way_pixels");
		ro->copy_field_string(*odb, "addr_housenumber", val_addrhousenumber);
		ro->copy_field_string(*odb, "addr_housename", val_addrhousename);
		ro->copy_field_string(*odb, "addr_unit", val_addrunit);
		ro->copy_field_string(*odb, "addr_flats", val_addrflats);
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_adminhighzoom(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryAdminLevelSorter robjs;
	tagkey_t key_boundary(odb->find_tagkey("boundary"));
	tagkey_t key_adminlevel(odb->find_tagkey("admin_level"));
	tagname_t name_administrative(odb->find_tagname("administrative"));
	TagNameSet cond_adminlevel(odb->create_tagnameset({ "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }));
	for (const OSMDB::Object::const_ptr_t& pobj : m_roads) {
		tagname_t val_boundary(pobj->find_tag(key_boundary));
		tagname_t val_adminlevel(pobj->find_tag(key_adminlevel));
		if (val_boundary != name_administrative || !cond_adminlevel.is_in_set(val_adminlevel) || pobj->get_id() >= 0)
			continue;
		RenderObjLines::ptr_t ro(create_lines(boost::dynamic_pointer_cast<const OSMDB::ObjLine>(pobj)));
		if (!ro)
			continue;
		ro->copy_field_string(*odb, "admin_level", val_adminlevel);
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_adminlowzoom(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryAdminLevelSorter robjs;
	tagkey_t key_boundary(odb->find_tagkey("boundary"));
	tagkey_t key_adminlevel(odb->find_tagkey("admin_level"));
	tagname_t name_administrative(odb->find_tagname("administrative"));
	TagNameSet cond_adminlevel(odb->create_tagnameset({ "0", "1", "2", "3", "4" }));
	for (const OSMDB::Object::const_ptr_t& pobj : m_roads) {
		tagname_t val_boundary(pobj->find_tag(key_boundary));
		tagname_t val_adminlevel(pobj->find_tag(key_adminlevel));
		if (val_boundary != name_administrative || !cond_adminlevel.is_in_set(val_adminlevel) || pobj->get_id() >= 0)
			continue;
		RenderObjLines::ptr_t ro(create_lines(boost::dynamic_pointer_cast<const OSMDB::ObjLine>(pobj)));
		if (!ro)
			continue;
		ro->copy_field_string(*odb, "admin_level", val_adminlevel);
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_adminmidzoom(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryAdminLevelSorter robjs;
	tagkey_t key_boundary(odb->find_tagkey("boundary"));
	tagkey_t key_adminlevel(odb->find_tagkey("admin_level"));
	tagname_t name_administrative(odb->find_tagname("administrative"));
	TagNameSet cond_adminlevel(odb->create_tagnameset({ "0", "1", "2", "3", "4", "5", "6", "7", "8" }));
	for (const OSMDB::Object::const_ptr_t& pobj : m_roads) {
		tagname_t val_boundary(pobj->find_tag(key_boundary));
		tagname_t val_adminlevel(pobj->find_tag(key_adminlevel));
		if (val_boundary != name_administrative || !cond_adminlevel.is_in_set(val_adminlevel) || pobj->get_id() >= 0)
			continue;
		RenderObjLines::ptr_t ro(create_lines(boost::dynamic_pointer_cast<const OSMDB::ObjLine>(pobj)));
		if (!ro)
			continue;
		ro->copy_field_string(*odb, "admin_level", val_adminlevel);
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_admintext(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryAdminLevelWayPixelSorter robjs;
	tagkey_t key_boundary(odb->find_tagkey("boundary"));
	tagkey_t key_adminlevel(odb->find_tagkey("admin_level"));
	tagkey_t key_name(odb->find_tagkey("name"));
	tagname_t name_administrative(odb->find_tagname("administrative"));
	TagNameSet cond_adminlevel(odb->create_tagnameset({ "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }));
	for (const OSMDB::Object::const_ptr_t& pobj : m_areas) {
		tagname_t val_boundary(pobj->find_tag(key_boundary));
		tagname_t val_adminlevel(pobj->find_tag(key_adminlevel));
		tagname_t val_name(pobj->find_tag(key_name));
		if (val_boundary != name_administrative || !cond_adminlevel.is_in_set(val_adminlevel) || val_name == tagname_t::invalid || pobj->get_id() >= 0)
			continue;
		RenderObjLines::ptr_t ro(create_lines(boost::dynamic_pointer_cast<const OSMDB::ObjArea>(pobj)));
		if (!ro)
			continue;
		{
			const RenderObj::fieldval_t& f(ro->find_field("way_pixels"));
			if (f.is_null())
				continue;
			try {
				if (static_cast<double>(f) < 196000)
					continue;
			} catch (...) {
				continue;
			}
		}
		ro->copy_field_string(*odb, "admin_level", val_adminlevel);
		ro->copy_field_string(*odb, "name", val_name);
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_aerialways(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryIntegerSorter robjs;
	tagkey_t key_aerialway(odb->find_tagkey("aerialway"));
	tagkey_t key_name(odb->find_tagkey("name"));
	tagkey_t key_manmade(odb->find_tagkey("man_made"));
	tagkey_t key_location(odb->find_tagkey("location"));
	tagkey_t key_bridge(odb->find_tagkey("bridge"));
	tagkey_t key_substance(odb->find_tagkey("substance"));
	tagname_t name_pipeline(odb->find_tagname("pipeline"));
	tagname_t name_overhead(odb->find_tagname("overhead"));
	TagNameSet cond_location(odb->create_tagnameset({ "overground", "overhead", "surface", "outdoor" }));
	TagNameSet cond_bridge(odb->create_tagnameset({ "yes", "aqueduct", "cantilever", "covered", "trestle", "viaduct" }));
	for (OSMDB::objects_t::const_iterator li(m_lines.begin()), le(m_lines.end()), ri(m_roads.begin()), re(m_roads.end()); li != re; ++li) {
		if (li == le)
			li = ri;
		const OSMDB::Object::const_ptr_t& pobj(*li);
		tagname_t val_aerialway(pobj->find_tag(key_aerialway));
		tagname_t val_manmade(pobj->find_tag(key_manmade));
		tagname_t val_location(pobj->find_tag(key_location));
		tagname_t val_bridge(pobj->find_tag(key_bridge));
		if (!((val_aerialway != tagname_t::invalid) ||
		      (val_manmade == name_pipeline && (cond_location.is_in_set(val_location) || cond_bridge.is_in_set(val_bridge)))))
			continue;
		tagname_t val_name(pobj->find_tag(key_name));
		tagname_t val_substance(pobj->find_tag(key_substance));
		RenderObjLines::ptr_t ro(create_lines(boost::dynamic_pointer_cast<const OSMDB::ObjLine>(pobj)));
		if (!ro)
			continue;
		ro->copy_field_string(*odb, "aerialway", val_aerialway);
		ro->copy_field_string(*odb, "name", val_name);
		ro->copy_field_string(*odb, "man_made", val_manmade);
		ro->copy_field_string(*odb, "substance", val_substance);
		robjs.add(ro, (val_manmade == name_pipeline) ? 1 : (val_location == name_overhead) ? 2 : (val_bridge != tagname_t::invalid) ? 3 : (val_aerialway != tagname_t::invalid) ? 4 : 0);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_aeroways(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryAerowaysSorter robjs;
	tagkey_t key_aeroway(odb->find_tagkey("aeroway"));
	tagkey_t key_bridge(odb->find_tagkey("bridge"));
	TagNameSet cond_aeroway(odb->create_tagnameset({ "runway", "taxiway" }));
	TagNameSet cond_bridge(odb->create_tagnameset({ "yes", "boardwalk", "cantilever", "covered", "low_water_crossing", "movable", "trestle", "viaduct" }));
	for (OSMDB::objects_t::const_iterator li(m_lines.begin()), le(m_lines.end()), ri(m_roads.begin()), re(m_roads.end()); li != re; ++li) {
		if (li == le)
			li = ri;
		const OSMDB::Object::const_ptr_t& pobj(*li);
		tagname_t val_aeroway(pobj->find_tag(key_aeroway));
		if (!cond_aeroway.is_in_set(val_aeroway))
			continue;
		tagname_t val_bridge(pobj->find_tag(key_bridge));
		RenderObjLines::ptr_t ro(create_lines(boost::dynamic_pointer_cast<const OSMDB::ObjLine>(pobj)));
		if (!ro)
			continue;
		ro->copy_field_string(*odb, "aeroway", val_aeroway);
		ro->set_field("bridge");
		if (cond_bridge.is_in_set(val_bridge))
			ro->copy_field_string(*odb, "bridge", val_bridge);
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_amenityline(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryLayerSorter robjs;
	tagkey_t key_name(odb->find_tagkey("name"));
	tagkey_t key_layer(odb->find_tagkey("layer"));
	tagkey_t key_ford(odb->find_tagkey("ford"));
	tagkey_t key_leisure(odb->find_tagkey("leisure"));
	tagkey_t key_attraction(odb->find_tagkey("attraction"));
	tagname_t name_water_slide(odb->find_tagname("water_slide"));
	TagNameSet cond_ford(odb->create_tagnameset({ "yes", "stepping_stones" }));
	TagNameSet cond_leisure(odb->create_tagnameset({ "slipway", "track" }));
	for (OSMDB::objects_t::const_iterator li(m_lines.begin()), le(m_lines.end()), ri(m_roads.begin()), re(m_roads.end()); li != re; ++li) {
		if (li == le)
			li = ri;
		const OSMDB::Object::const_ptr_t& pobj(*li);
		tagname_t val_ford(pobj->find_tag(key_ford));
		tagname_t val_leisure(pobj->find_tag(key_leisure));
		tagname_t val_attraction(pobj->find_tag(key_attraction));
		std::string ftr;
		if (cond_ford.is_in_set(val_ford))
			ftr = "highway_ford";
		else if (cond_leisure.is_in_set(val_leisure))
			ftr = std::string("leisure_") + odb->find_tagname(val_leisure);
		else if (val_attraction == name_water_slide)
			ftr = "attraction_water_slide";
		else
			continue;
		tagname_t val_name(pobj->find_tag(key_name));
		tagname_t val_layer(pobj->find_tag(key_layer));
		RenderObjLines::ptr_t ro(create_lines(boost::dynamic_pointer_cast<const OSMDB::ObjLine>(pobj)));
		if (!ro)
			continue;
		ro->copy_field_string(*odb, "name", val_name);
		ro->copy_field_int(*odb, "layer", val_layer);
		ro->set_field("feature", ftr);
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_amenitylowpriority(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryScoreWayPixelSorter robjs;
	tagkey_t key_highway(odb->find_tagkey("highway"));
	tagkey_t key_railway(odb->find_tagkey("railway"));
	tagkey_t key_amenity(odb->find_tagkey("amenity"));
	tagkey_t key_historic(odb->find_tagkey("historic"));
	tagkey_t key_manmade(odb->find_tagkey("man_made"));
	tagkey_t key_barrier(odb->find_tagkey("barrier"));
	tagkey_t key_name(odb->find_tagkey("name"));
	tagkey_t key_access(odb->find_tagkey("access"));
	tagname_t name_miniroundabout(odb->find_tagname("mini_roundabout"));
	tagname_t name_cross(odb->find_tagname("cross"));
	TagNameSet cond_railway(odb->create_tagnameset({ "level_crossing", "crossing" }));
	TagNameSet cond_amenity(odb->create_tagnameset({ "bench", "waste_basket", "waste_disposal" }));
	TagNameSet cond_historic(odb->create_tagnameset({ "wayside_cross", "wayside_shrine" }));
	TagNameSet cond_barrier(odb->create_tagnameset({ "bollard", "gate", "lift_gate", "swing_gate", "block", "log", "cattle_grid", "stile",
							   "motorcycle_barrier", "cycle_barrier", "full-height_turnstile", "turnstile", "kissing_gate" }));
	TagNameSet cond_amenity_lowscore(odb->create_tagnameset({ "waste_basket", "waste_disposal" }));
	for (const OSMDB::Object::const_ptr_t& pobj : m_areas) {
		tagname_t val_highway(pobj->find_tag(key_highway));
		tagname_t val_railway(pobj->find_tag(key_railway));
		tagname_t val_amenity(pobj->find_tag(key_amenity));
		tagname_t val_historic(pobj->find_tag(key_historic));
		tagname_t val_manmade(pobj->find_tag(key_manmade));
		tagname_t val_barrier(pobj->find_tag(key_barrier));
		if (!(val_highway == name_miniroundabout ||
		      cond_railway.is_in_set(val_railway) ||
		      cond_amenity.is_in_set(val_amenity) ||
		      cond_historic.is_in_set(val_historic) ||
		      val_manmade == name_cross ||
		      cond_barrier.is_in_set(val_barrier)))
			continue;
		RenderObjPoints::ptr_t ro(create_points(boost::dynamic_pointer_cast<const OSMDB::ObjArea>(pobj)));
		if (!ro)
			continue;
		tagname_t val_name(pobj->find_tag(key_name));
		tagname_t val_access(pobj->find_tag(key_access));
		ro->copy_field_string(*odb, "name", val_name);
		ro->set_field("feature");
		if (val_manmade == name_cross) {
			const char *cp(odb->find_tagname(val_manmade));
			if (cp)
				ro->set_field("feature", std::string("man_made_") + cp);
		} else if (cond_barrier.is_in_set(val_barrier)) {
			const char *cp(odb->find_tagname(val_barrier));
			if (cp)
				ro->set_field("feature", std::string("barrier_") + cp);
		}
		ro->copy_field_string(*odb, "access", val_access);
		robjs.add(ro, cond_amenity_lowscore.is_in_set(val_amenity) ? 1 : (val_amenity == tagname_t::invalid) ? 3 : 1);
	}
	for (const OSMDB::Object::const_ptr_t& pobj : m_points) {
		tagname_t val_highway(pobj->find_tag(key_highway));
		tagname_t val_railway(pobj->find_tag(key_railway));
		tagname_t val_amenity(pobj->find_tag(key_amenity));
		tagname_t val_historic(pobj->find_tag(key_historic));
		tagname_t val_manmade(pobj->find_tag(key_manmade));
		tagname_t val_barrier(pobj->find_tag(key_barrier));
		if (!(val_highway == name_miniroundabout ||
		      cond_railway.is_in_set(val_railway) ||
		      cond_amenity.is_in_set(val_amenity) ||
		      cond_historic.is_in_set(val_historic) ||
		      val_manmade == name_cross ||
		      cond_barrier.is_in_set(val_barrier)))
			continue;
		RenderObjPoints::ptr_t ro(create_points(boost::dynamic_pointer_cast<const OSMDB::ObjPoint>(pobj)));
			if (!ro)
			continue;
		tagname_t val_name(pobj->find_tag(key_name));
		tagname_t val_access(pobj->find_tag(key_access));
		ro->copy_field_string(*odb, "name", val_name);
		ro->set_field("feature");
		if (val_highway == name_miniroundabout) {
			const char *cp(odb->find_tagname(val_highway));
			if (cp)
				ro->set_field("feature", std::string("highway_") + cp);
		} else if (cond_railway.is_in_set(val_railway)) {
			const char *cp(odb->find_tagname(val_railway));
			if (cp)
				ro->set_field("feature", std::string("railway_") + cp);
		} else if (cond_amenity.is_in_set(val_amenity)) {
			const char *cp(odb->find_tagname(val_amenity));
			if (cp)
				ro->set_field("feature", std::string("amenity_") + cp);
		} else if (cond_historic.is_in_set(val_historic)) {
			const char *cp(odb->find_tagname(val_historic));
			if (cp)
				ro->set_field("feature", std::string("historic_") + cp);
		} else if (val_manmade == name_cross) {
			const char *cp(odb->find_tagname(val_manmade));
			if (cp)
				ro->set_field("feature", std::string("man_made_") + cp);
		} else if (cond_barrier.is_in_set(val_barrier)) {
			const char *cp(odb->find_tagname(val_barrier));
			if (cp)
				ro->set_field("feature", std::string("barrier_") + cp);
		}
		ro->copy_field_string(*odb, "access", val_access);
		robjs.add(ro, cond_amenity_lowscore.is_in_set(val_amenity) ? 1 : (val_amenity == tagname_t::invalid) ? 3 : 1);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_amenitypoints(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryScoreWayPixelSorter robjs;
	tagkey_t key_aeroway(odb->find_tagkey("aeroway"));
	tagkey_t key_tourism(odb->find_tagkey("tourism"));
	tagkey_t key_amenity(odb->find_tagkey("amenity"));
	tagkey_t key_vending(odb->find_tagkey("vending"));
	tagkey_t key_advertising(odb->find_tagkey("advertising"));
	tagkey_t key_emergency(odb->find_tagkey("emergency"));
	tagkey_t key_shop(odb->find_tagkey("shop"));
	tagkey_t key_leisure(odb->find_tagkey("leisure"));
	tagkey_t key_power(odb->find_tagkey("power"));
	tagkey_t key_manmade(odb->find_tagkey("man_made"));
	tagkey_t key_location(odb->find_tagkey("location"));
	tagkey_t key_landuse(odb->find_tagkey("landuse"));
	tagkey_t key_natural(odb->find_tagkey("natural"));
	tagkey_t key_waterway(odb->find_tagkey("waterway"));
	tagkey_t key_place(odb->find_tagkey("place"));
	tagkey_t key_historic(odb->find_tagkey("historic"));
	tagkey_t key_military(odb->find_tagkey("military"));
	tagkey_t key_highway(odb->find_tagkey("highway"));
	tagkey_t key_ford(odb->find_tagkey("ford"));
	tagkey_t key_boundary(odb->find_tagkey("boundary"));
	tagkey_t key_protectclass(odb->find_tagkey("protect_class"));
	tagkey_t key_office(odb->find_tagkey("office"));
	tagkey_t key_barrier(odb->find_tagkey("barrier"));
	tagkey_t key_parking(odb->find_tagkey("parking"));
	tagkey_t key_access(odb->find_tagkey("access"));
	tagkey_t key_ele(odb->find_tagkey("ele"));
	tagkey_t key_height(odb->find_tagkey("height"));
	tagkey_t key_name(odb->find_tagkey("name"));
	tagkey_t key_information(odb->find_tagkey("information"));
	tagkey_t key_religion(odb->find_tagkey("religion"));
	tagkey_t key_denomination(odb->find_tagkey("denomination"));
	tagkey_t key_generatorsource(odb->find_tagkey("generator:source"));
	tagkey_t key_icao(odb->find_tagkey("icao"));
	tagkey_t key_iata(odb->find_tagkey("iata"));
	tagkey_t key_recyclingtype(odb->find_tagkey("recycling_type"));
	tagkey_t key_towerconstruction(odb->find_tagkey("tower:construction"));
	tagkey_t key_towertype(odb->find_tagkey("tower:type"));
	tagkey_t key_telescopetype(odb->find_tagkey("telescope:type"));
	tagkey_t key_telescopediameter(odb->find_tagkey("telescope:diameter"));
	tagkey_t key_castletype(odb->find_tagkey("castle_type"));
	tagkey_t key_sport(odb->find_tagkey("sport"));
	tagkey_t key_memorial(odb->find_tagkey("memorial"));
	tagkey_t key_artworktype(odb->find_tagkey("artwork_type"));
	tagkey_t key_building(odb->find_tagkey("building"));
	tagkey_t key_operator(odb->find_tagkey("operator"));
	tagkey_t key_ref(odb->find_tagkey("ref"));
	tagname_t name_wastedisposal(odb->find_tagname("waste_disposal"));
	tagname_t name_vendingmachine(odb->find_tagname("vending_machine"));
	tagname_t name_column(odb->find_tagname("column"));
	tagname_t name_phone(odb->find_tagname("phone"));
	tagname_t name_waterfall(odb->find_tagname("waterfall"));
	tagname_t name_protectedarea(odb->find_tagname("protected_area"));
	tagname_t name_information(odb->find_tagname("information"));
	tagname_t name_tollbooth(odb->find_tagname("toll_booth"));
	tagname_t name_parking(odb->find_tagname("parking"));
	tagname_t name_underground(odb->find_tagname("underground"));
	tagname_t name_parkingentrance(odb->find_tagname("parking_entrance"));
	tagname_t name_locality(odb->find_tagname("locality"));
	tagname_t name_alpinehut(odb->find_tagname("alpine_hut"));
	tagname_t name_guidepost(odb->find_tagname("guidepost"));
	tagname_t name_shelter(odb->find_tagname("shelter"));
	tagname_t name_telescope(odb->find_tagname("telescope"));
	tagname_t name_no(odb->find_tagname("no"));
	TagNameSet cond_aeroway(odb->create_tagnameset({ "gate", "apron", "helipad", "aerodrome" }));
	TagNameSet cond_tourism1(odb->create_tagnameset({ "alpine_hut", "apartment", "artwork", "camp_site", "caravan_site", "chalet", "gallery", "guest_house",
							    "hostel", "hotel", "motel", "museum", "picnic_site", "theme_park", "wilderness_hut",
							    "zoo" }));
	TagNameSet cond_amenity1(odb->create_tagnameset({ "arts_centre", "atm", "bank", "bar", "bbq", "bicycle_rental",
							    "bicycle_repair_station","biergarten", "boat_rental", "bureau_de_change", "bus_station", "cafe",
							    "car_rental", "car_wash", "casino", "charging_station", "childcare", "cinema", "clinic", "college",
							    "community_centre", "courthouse", "dentist", "doctors", "drinking_water", "driving_school", "embassy",
							    "fast_food", "ferry_terminal", "fire_station", "food_court", "fountain", "fuel", "grave_yard",
							    "hospital", "hunting_stand", "ice_cream", "internet_cafe", "kindergarten", "library", "marketplace",
							    "nightclub", "nursing_home", "pharmacy", "place_of_worship", "police", "post_box",
							    "post_office", "prison", "pub", "public_bath", "public_bookcase", "recycling", "restaurant", "school",
							    "shelter", "shower", "social_facility", "taxi", "telephone", "theatre", "toilets", "townhall",
							    "university", "vehicle_inspection", "veterinary" }));
	TagNameSet cond_vending(odb->create_tagnameset({ "excrement_bags", "parking_tickets", "public_transport_tickets" }));
	TagNameSet cond_shop1(odb->create_tagnameset({ "yes", "no", "vacant", "closed", "disused", "empty" }));
	TagNameSet cond_leisure(odb->create_tagnameset({ "amusement_arcade", "beach_resort", "bird_hide", "bowling_alley", "dog_park", "firepit", "fishing",
							   "fitness_centre", "fitness_station", "garden", "golf_course", "ice_rink", "marina", "miniature_golf",
							   "nature_reserve", "outdoor_seating", "park", "picnic_table", "pitch", "playground", "recreation_ground",
							   "sauna", "slipway", "sports_centre", "stadium", "swimming_area", "swimming_pool", "track", "water_park" }));
	TagNameSet cond_power(odb->create_tagnameset({ "plant", "generator", "substation" }));
	TagNameSet cond_manmade1(odb->create_tagnameset({ "chimney", "communications_tower", "crane", "lighthouse", "mast", "obelisk", "silo", "storage_tank",
							    "telescope", "tower", "wastewater_plant", "water_tower", "water_works", "windmill", "works" }));
	TagNameSet cond_location(odb->create_tagnameset({ "roof", "rooftop" }));
	TagNameSet cond_landuse(odb->create_tagnameset({ "reservoir", "basin", "recreation_ground", "village_green", "quarry", "vineyard", "orchard", "cemetery",
							   "residential", "garages", "meadow", "grass", "allotments", "forest", "farmyard", "farmland",
							   "greenhouse_horticulture", "retail", "industrial", "railway", "commercial", "brownfield", "landfill",
							   "construction", "salt_pond", "military", "plant_nursery" }));
	TagNameSet cond_natural1(odb->create_tagnameset({ "peak", "volcano", "saddle", "cave_entrance" }));
	TagNameSet cond_natural2(odb->create_tagnameset({ "wood", "peak", "volcano", "saddle", "cave_entrance", "water", "mud", "wetland", "bay", "spring",
							    "scree", "shingle", "bare_rock", "sand", "heath", "grassland", "scrub", "beach", "glacier", "tree", "strait", "cape" }));
	TagNameSet cond_place(odb->create_tagnameset({ "island", "islet" }));
	TagNameSet cond_historic(odb->create_tagnameset({ "memorial", "monument", "archaeological_site", "fort", "castle", "manor", "city_gate" }));
	TagNameSet cond_military(odb->create_tagnameset({ "danger_area", "bunker" }));
	TagNameSet cond_highway(odb->create_tagnameset({ "services", "rest_area", "bus_stop", "elevator", "traffic_signals" }));
	TagNameSet cond_ford(odb->create_tagnameset({ "yes", "stepping_stones" }));
	TagNameSet cond_boundary(odb->create_tagnameset({ "aboriginal_lands", "national_park" }));
	TagNameSet cond_protectclass(odb->create_tagnameset({ "1","1a","1b","2","3","4","5","6","7","24","97","98","99" }));
	TagNameSet cond_office(odb->create_tagnameset({ "no", "vacant", "closed", "disused", "empty" }));
	TagNameSet cond_waterway(odb->create_tagnameset({ "dam", "weir", "dock" }));
	TagNameSet cond_amenity2(odb->create_tagnameset({ "bicycle_parking", "motorcycle_parking" }));
	TagNameSet cond_parking(odb->create_tagnameset({ "multi-storey", "underground" }));
	TagNameSet cond_access(odb->create_tagnameset({ "private", "no" }));
	TagNameSet cond_tourism2(odb->create_tagnameset({ "viewpoint", "attraction" }));
	TagNameSet cond_natural3(odb->create_tagnameset({ "peak", "volcano", "saddle" }));
	TagNameSet cond_manmade2(odb->create_tagnameset({ "mast", "tower", "chimney", "crane" }));
	TagNameSet cond_shop2(odb->create_tagnameset({ "supermarket", "bag", "bakery", "beauty", "bed", "bookmaker", "books", "butcher", "carpet", "clothes", "computer",
							 "confectionery", "fashion", "convenience", "department_store", "doityourself", "hardware", "fabric", "fishmonger", "florist",
							 "garden_centre", "hairdresser", "hifi", "car", "car_repair", "bicycle", "mall", "pet",
							 "photo", "photo_studio", "photography", "seafood", "shoes", "alcohol", "gift", "furniture", "kiosk",
							 "mobile_phone", "motorcycle", "musical_instrument", "newsagent", "optician", "jewelry", "jewellery",
							 "electronics", "chemist", "toys", "travel_agency", "car_parts", "greengrocer", "farm", "stationery",
							 "laundry", "dry_cleaning", "beverages", "perfumery", "cosmetics", "variety_store", "wine", "outdoor",
				"copyshop", "sports", "deli", "tobacco", "art", "tea", "coffee", "tyres", "pastry", "chocolate",
				"music", "medical_supply", "dairy", "video_games", "houseware", "ticket", "charity", "second_hand",
				"interior_decoration", "video", "paint", "massage", "trade", "wholesale" }));
	for (OSMDB::objects_t::const_iterator li(m_areas.begin()), le(m_areas.end()), ri(m_points.begin()), re(m_points.end()); li != re; ++li) {
		if (li == le)
			li = ri;
		const OSMDB::Object::const_ptr_t& pobj(*li);
		RenderObjPoints::ptr_t ro;
		bool isarea(false);
		{
			OSMDB::ObjArea::const_ptr_t p(boost::dynamic_pointer_cast<const OSMDB::ObjArea>(pobj));
			if (p) {
				isarea = true;			
				ro = create_points(p);
				if (!ro)
					continue;
				{
					const RenderObj::fieldval_t& f(ro->find_field("way_pixels"));
					if (f.is_null())
						continue;
					try {
						if (static_cast<double>(f) >= 768000)
							continue;
					} catch (...) {
						continue;
					}
				}
			}
		}
		if (!isarea) {
			OSMDB::ObjPoint::const_ptr_t p(boost::dynamic_pointer_cast<const OSMDB::ObjPoint>(pobj));
			if (p)
				ro = create_points(p);
		}
		if (!ro)
			continue;
		tagname_t val_aeroway(pobj->find_tag(key_aeroway));
		tagname_t val_tourism(pobj->find_tag(key_tourism));
		tagname_t val_amenity(pobj->find_tag(key_amenity));
		tagname_t val_vending(pobj->find_tag(key_vending));
		tagname_t val_advertising(pobj->find_tag(key_advertising));
		tagname_t val_emergency(pobj->find_tag(key_emergency));
		tagname_t val_shop(pobj->find_tag(key_shop));
		tagname_t val_leisure(pobj->find_tag(key_leisure));
		tagname_t val_power(pobj->find_tag(key_power));
		tagname_t val_manmade(pobj->find_tag(key_manmade));
		tagname_t val_location(pobj->find_tag(key_location));
		tagname_t val_landuse(pobj->find_tag(key_landuse));
		tagname_t val_natural(pobj->find_tag(key_natural));
		tagname_t val_waterway(pobj->find_tag(key_waterway));
		tagname_t val_place(pobj->find_tag(key_place));
		tagname_t val_historic(pobj->find_tag(key_historic));
		tagname_t val_military(pobj->find_tag(key_military));
		tagname_t val_highway(pobj->find_tag(key_highway));
		tagname_t val_ford(pobj->find_tag(key_ford));
		tagname_t val_boundary(pobj->find_tag(key_boundary));
		tagname_t val_protectclass(pobj->find_tag(key_protectclass));
		tagname_t val_office(pobj->find_tag(key_office));
		tagname_t val_barrier(pobj->find_tag(key_barrier));
		tagname_t val_parking(pobj->find_tag(key_parking));
		tagname_t val_access(pobj->find_tag(key_access));
		tagname_t val_ele(pobj->find_tag(key_ele));
		tagname_t val_height(pobj->find_tag(key_height));
		tagname_t val_name(pobj->find_tag(key_name));
		tagname_t val_information(pobj->find_tag(key_information));
		tagname_t val_religion(pobj->find_tag(key_religion));
		tagname_t val_denomination(pobj->find_tag(key_denomination));
		tagname_t val_generatorsource(pobj->find_tag(key_generatorsource));
		tagname_t val_icao(pobj->find_tag(key_icao));
		tagname_t val_iata(pobj->find_tag(key_iata));
		tagname_t val_recyclingtype(pobj->find_tag(key_recyclingtype));
		tagname_t val_towerconstruction(pobj->find_tag(key_towerconstruction));
		tagname_t val_towertype(pobj->find_tag(key_towertype));
		tagname_t val_telescopetype(pobj->find_tag(key_telescopetype));
		tagname_t val_telescopediameter(pobj->find_tag(key_telescopediameter));
		tagname_t val_castletype(pobj->find_tag(key_castletype));
		tagname_t val_sport(pobj->find_tag(key_sport));
		tagname_t val_memorial(pobj->find_tag(key_memorial));
		tagname_t val_artworktype(pobj->find_tag(key_artworktype));
        	tagname_t val_building(pobj->find_tag(key_building));
		tagname_t val_operator(pobj->find_tag(key_operator));
		tagname_t val_ref(pobj->find_tag(key_ref));
		if (cond_aeroway.is_in_set(val_aeroway)) {
			const char *cp(odb->find_tagname(val_aeroway));
			if (cp)
				ro->set_field("feature", std::string("aeroway_") + cp);
		} else if (cond_tourism1.is_in_set(val_tourism)) {
			const char *cp(odb->find_tagname(val_tourism));
			if (cp)
				ro->set_field("feature", std::string("tourism_") + cp);
		} else if (cond_amenity1.is_in_set(val_amenity)) {
			const char *cp(odb->find_tagname(val_amenity));
			if (cp)
				ro->set_field("feature", std::string("amenity_") + cp);
		} else if (val_amenity == name_wastedisposal && isarea) {
			const char *cp(odb->find_tagname(val_amenity));
			if (cp)
				ro->set_field("feature", std::string("amenity_") + cp);
		} else if (val_amenity == name_vendingmachine && cond_vending.is_in_set(val_vending)) {
			const char *cp(odb->find_tagname(val_amenity));
			if (cp)
				ro->set_field("feature", std::string("amenity_") + cp);
		} else if (val_advertising == name_column) {
			const char *cp(odb->find_tagname(val_advertising));
			if (cp)
				ro->set_field("feature", std::string("advertising_") + cp);
		} else if (val_emergency == name_phone && !isarea) {
			const char *cp(odb->find_tagname(val_emergency));
			if (cp)
				ro->set_field("feature", std::string("emergency_") + cp);
		} else if (val_shop != tagname_t::invalid && !cond_shop1.is_in_set(val_shop)) {
			ro->set_field("feature", "shop");
		} else if (cond_leisure.is_in_set(val_leisure)) {
			const char *cp(odb->find_tagname(val_leisure));
			if (cp)
				ro->set_field("feature", std::string("leisure_") + cp);
		} else if (cond_power.is_in_set(val_power)) {
			const char *cp(odb->find_tagname(val_power));
			if (cp)
				ro->set_field("feature", std::string("power_") + cp);
		} else if (cond_manmade1.is_in_set(val_manmade) && !cond_location.is_in_set(val_location)) {
			const char *cp(odb->find_tagname(val_manmade));
			if (cp)
				ro->set_field("feature", std::string("man_made_") + cp);
		} else if (cond_landuse.is_in_set(val_landuse)) {
			const char *cp(odb->find_tagname(val_landuse));
			if (cp)
				ro->set_field("feature", std::string("landuse_") + cp);
		} else if (cond_natural1.is_in_set(val_natural) && !isarea) {
			const char *cp(odb->find_tagname(val_natural));
			if (cp)
				ro->set_field("feature", std::string("natural_") + cp);
		} else if (cond_natural2.is_in_set(val_natural)) {
			const char *cp(odb->find_tagname(val_natural));
			if (cp)
				ro->set_field("feature", std::string("natural_") + cp);
		} else if (val_waterway == name_waterfall && isarea) {
			const char *cp(odb->find_tagname(val_waterway));
			if (cp)
				ro->set_field("feature", std::string("waterway_") + cp);
		} else if (cond_place.is_in_set(val_place)) {
			const char *cp(odb->find_tagname(val_place));
			if (cp)
				ro->set_field("feature", std::string("place_") + cp);
		} else if (cond_historic.is_in_set(val_historic)) {
			const char *cp(odb->find_tagname(val_historic));
			if (cp)
				ro->set_field("feature", std::string("historic_") + cp);
		} else if (cond_military.is_in_set(val_military)) {
			const char *cp(odb->find_tagname(val_military));
			if (cp)
				ro->set_field("feature", std::string("military_") + cp);
		} else if (cond_highway.is_in_set(val_highway)) {
			const char *cp(odb->find_tagname(val_highway));
			if (cp)
				ro->set_field("feature", std::string("highway_") + cp);
		} else if (cond_ford.is_in_set(val_ford)) {
			ro->set_field("feature", "highway_ford");
		} else if (cond_boundary.is_in_set(val_boundary) ||
			   (val_boundary == name_protectedarea && cond_protectclass.is_in_set(val_protectclass) )) {
			const char *cp(odb->find_tagname(val_boundary));
			if (cp)
				ro->set_field("feature", std::string("boundary_") + cp);
		} else if (val_tourism == name_information) {
			const char *cp(odb->find_tagname(val_tourism));
			if (cp)
				ro->set_field("feature", std::string("tourism_") + cp);
		} else if (val_office != tagname_t::invalid && !cond_office.is_in_set(val_office)) {
			ro->set_field("feature", "office");
		} else if (val_barrier == name_tollbooth && !isarea) {
			const char *cp(odb->find_tagname(val_barrier));
			if (cp)
				ro->set_field("feature", std::string("barrier_") + cp);
		} else if (cond_waterway.is_in_set(val_waterway)) {
			const char *cp(odb->find_tagname(val_waterway));
			if (cp)
				ro->set_field("feature", std::string("waterway_") + cp);
		} else if (cond_amenity2.is_in_set(val_amenity) ||
			   (val_amenity == name_parking && val_parking != name_underground) ||
			   (val_amenity == name_parkingentrance && cond_parking.is_in_set(val_parking) &&
			    !cond_access.is_in_set(val_access) && !isarea)) {
			const char *cp(odb->find_tagname(val_amenity));
			if (cp)
				ro->set_field("feature", std::string("amenity_") + cp);
		} else if (cond_tourism2.is_in_set(val_tourism)) {
			const char *cp(odb->find_tagname(val_tourism));
			if (cp)
				ro->set_field("feature", std::string("tourism_") + cp);
		} else if (val_place == name_locality) {
			const char *cp(odb->find_tagname(val_place));
			if (cp)
				ro->set_field("feature", std::string("place_") + cp);
		} else {
			continue;
		}
		ro->copy_field_string(*odb, "access", val_access);
		{
			double ele(std::numeric_limits<double>::quiet_NaN());
			double height(std::numeric_limits<double>::quiet_NaN());
			std::string elestr, heightstr, name;
			const char *cp(odb->find_tagname(val_ele));
			if (cp) {
				char *cp1;
				double x(strtod(cp, &cp1));
				if (!*cp1 && cp != cp1 && !std::isnan(x) && x > -10000 && x < 10000) {
					ele = x;
					elestr = cp;
				}
			}
			cp = odb->find_tagname(val_height);
			if (cp) {
				char *cp1;
				double x(strtod(cp, &cp1));
				if (cp != cp1 && !std::isnan(x) && x >= 0 && x < 1000) {
					while (std::isspace(*cp1))
						++cp1;
					if (*cp1 == 'm')
						++cp1;
					if (!*cp1) {					
						height = x;
						heightstr = cp;
					}
				}
			}
			cp = odb->find_tagname(val_name);
			if (cp)
				name = cp;
			if (!std::isnan(ele) && (cond_natural3.is_in_set(val_natural) ||
						 val_tourism == name_alpinehut ||
						 (val_tourism == name_information && val_information == name_guidepost) ||
						 val_amenity == name_shelter)) {
				if (!name.empty())
					name.push_back('\n');
				std::ostringstream oss;
				oss << Point::round<long,double>(ele) << 'm';
				name += oss.str();
			}
			if (!std::isnan(height) && val_waterway == name_waterfall) {
				if (!name.empty())
					name.push_back('\n');
				std::ostringstream oss;
				oss << Point::round<long,double>(height) << 'm';
				name += oss.str();
			}
			ro->set_field("name", name);
			double score(std::numeric_limits<double>::quiet_NaN());
			if (cond_natural3.is_in_set(val_natural))
				score = ele;
			else if (val_waterway == name_waterfall)
				score = height;
			if (std::isnan(score))
				ro->set_field("score");
			else
				ro->set_field("score", score);
			if (((cond_manmade2.is_in_set(val_manmade) && !cond_location.is_in_set(val_location)) ||
			     val_waterway == name_waterfall) && !std::isnan(height))
				ro->set_field("height", height);
			else
				ro->set_field("height");
		}
		ro->copy_field_string(*odb, "parking", val_parking);
		ro->copy_field_string(*odb, "religion", val_religion);
		ro->copy_field_string(*odb, "denomination", val_denomination);
		ro->copy_field_string(*odb, "generator:source", val_generatorsource);
		ro->copy_field_string(*odb, "location", val_location);
		ro->copy_field_string(*odb, "icao", val_icao);
		ro->copy_field_string(*odb, "iata", val_iata);
		ro->copy_field_string(*odb, "office", val_office);
		ro->copy_field_string(*odb, "recycling_type", val_recyclingtype);
		ro->copy_field_string(*odb, "tower:construction", val_towerconstruction);
		ro->copy_field_string(*odb, "tower:type", val_towertype);
		ro->copy_field_string(*odb, "telescope:type", val_telescopetype);
		ro->set_field("telescope:diameter");
		if (val_manmade == name_telescope) {
			const char *cp(odb->find_tagname(val_telescopediameter));
			if (cp) {
				char *cp1;
				double x(strtod(cp, &cp1));
				if (!*cp1 && cp != cp1 && !std::isnan(x) && x > -10000 && x < 10000)
					ro->set_field("telescope:diameter", x);
			}
		}
		ro->copy_field_string(*odb, "castle_type", val_castletype);
		ro->copy_field_string(*odb, "sport", val_sport);
		ro->copy_field_string(*odb, "information", val_information);
		ro->copy_field_string(*odb, "memorial", val_memorial);
		ro->copy_field_string(*odb, "artwork_type", val_artworktype);
		ro->copy_field_string(*odb, "vending", val_vending);
		if (cond_shop2.is_in_set(val_shop))
			ro->copy_field_string(*odb, "shop", val_shop);
		else
			ro->set_field("shop", "other");
		ro->set_field("is_building", (val_building == name_no || val_building == tagname_t::invalid) ? "no" : "yes");
		ro->copy_field_string(*odb, "operator", val_operator);
		ro->copy_field_string(*odb, "ref", val_ref);
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_barriers(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryNoSorter robjs;
	tagkey_t key_barrier(odb->find_tagkey("barrier"));
	tagkey_t key_historic(odb->find_tagkey("historic"));
	tagkey_t key_waterway(odb->find_tagkey("waterway"));
	tagname_t name_citywalls(odb->find_tagname("citywalls"));
	TagNameSet cond_barrier(odb->create_tagnameset({ "chain", "city_wall", "ditch", "fence", "guard_rail",
							   "handrail", "hedge", "retaining_wall", "wall" }));
	TagNameSet cond_waterway(odb->create_tagnameset({ "river", "canal", "stream", "drain", "ditch" }));
	for (const OSMDB::Object::const_ptr_t& pobj : m_areas) {
		tagname_t val_barrier(pobj->find_tag(key_barrier));
		tagname_t val_historic(pobj->find_tag(key_historic));
		tagname_t val_waterway(pobj->find_tag(key_waterway));
		if (!(cond_barrier.is_in_set(val_barrier) ||
		      (val_historic == name_citywalls && !cond_waterway.is_in_set(val_waterway))))
			continue;
		RenderObjPoints::ptr_t ro(create_points(boost::dynamic_pointer_cast<const OSMDB::ObjArea>(pobj)));
		if (!ro)
			continue;
		ro->set_field("feature");
		if (val_historic == name_citywalls) {
			const char *cp(odb->find_tagname(val_historic));
			if (cp)
				ro->set_field("feature", std::string("historic_") + cp);
		} else if (cond_barrier.is_in_set(val_barrier)) {
			const char *cp(odb->find_tagname(val_barrier));
			if (cp)
				ro->set_field("feature", std::string("barrier_") + cp);
		}
		robjs.add(ro);
	}
	for (const OSMDB::Object::const_ptr_t& pobj : m_points) {
		tagname_t val_barrier(pobj->find_tag(key_barrier));
		tagname_t val_historic(pobj->find_tag(key_historic));
		tagname_t val_waterway(pobj->find_tag(key_waterway));
		if (!(cond_barrier.is_in_set(val_barrier) ||
		      (val_historic == name_citywalls && !cond_waterway.is_in_set(val_waterway))))
			continue;
		RenderObjPoints::ptr_t ro(create_points(boost::dynamic_pointer_cast<const OSMDB::ObjPoint>(pobj)));
		if (!ro)
			continue;
		ro->set_field("feature");
		if (val_historic == name_citywalls) {
			const char *cp(odb->find_tagname(val_historic));
			if (cp)
				ro->set_field("feature", std::string("historic_") + cp);
		} else if (cond_barrier.is_in_set(val_barrier)) {
			const char *cp(odb->find_tagname(val_barrier));
			if (cp)
				ro->set_field("feature", std::string("barrier_") + cp);
		}
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_bridge(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	DbQueryNoSorter robjs;
	tagkey_t key_manmade(odb->find_tagkey("man_made"));
	tagname_t name_bridge(odb->find_tagname("bridge"));
	for (const OSMDB::Object::const_ptr_t& pobj : m_areas) {
		tagname_t val_manmade(pobj->find_tag(key_manmade));
		if (val_manmade != name_bridge)
			continue;
		RenderObjPolygons::ptr_t ro(create_polygons(boost::dynamic_pointer_cast<const OSMDB::ObjArea>(pobj)));
		if (!ro)
			continue;
		ro->copy_field_string(*odb, "man_made", val_manmade);
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_bridges(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryRoadsBridgesSorter robjs;
	tagkey_t key_bridge(odb->find_tagkey("bridge"));
	tagkey_t key_highway(odb->find_tagkey("highway"));
	tagkey_t key_railway(odb->find_tagkey("railway"));
	TagNameSet cond_bridge(odb->create_tagnameset({ "yes", "boardwalk", "cantilever", "covered", "low_water_crossing", "movable", "trestle", "viaduct" }));
	tagkey_t key_horse(odb->find_tagkey("horse"));
	tagkey_t key_foot(odb->find_tagkey("foot"));
	tagkey_t key_bicycle(odb->find_tagkey("bicycle"));
	tagkey_t key_tracktype(odb->find_tagkey("tracktype"));
	tagkey_t key_surface(odb->find_tagkey("surface"));
	tagkey_t key_access(odb->find_tagkey("access"));
	tagkey_t key_construction(odb->find_tagkey("construction"));
	tagkey_t key_service(odb->find_tagkey("service"));
	tagkey_t key_layer(odb->find_tagkey("layer"));
	tagname_t name_preserved(odb->find_tagname("preserved"));
	tagname_t name_rail(odb->find_tagname("rail"));
	tagname_t name_tram(odb->find_tagname("tram"));
	TagNameSet cond_surface_unpaved(odb->create_tagnameset({ "unpaved", "compacted", "dirt", "earth", "fine_gravel", "grass", "grass_paver", "gravel", "ground",
								   "mud", "pebblestone", "salt", "sand", "woodchips", "clay", "ice", "snow" }));
	TagNameSet cond_surface_paved(odb->create_tagnameset({ "paved", "asphalt", "cobblestone", "cobblestone:flattened", "sett", "concrete", "concrete:lanes",
								 "concrete:plates", "paving_stones", "metal", "wood", "unhewn_cobblestone" }));
	TagNameSet cond_access_dest(odb->create_tagnameset({ "destination" }));
	TagNameSet cond_access_no(odb->create_tagnameset({ "no", "private" }));
	TagNameSet cond_service_minor(odb->create_tagnameset({ "parking_aisle", "drive-through", "driveway" }));
	TagNameSet cond_service_ssy(odb->create_tagnameset({ "spur", "siding", "yard" }));
	for (OSMDB::objects_t::const_iterator li(m_lines.begin()), le(m_lines.end()), ri(m_roads.begin()), re(m_roads.end()); li != re; ++li) {
		if (li == le)
			li = ri;
		const OSMDB::Object::const_ptr_t& pobj(*li);
		tagname_t val_bridge(pobj->find_tag(key_bridge));
		if (!cond_bridge.is_in_set(val_bridge))
			continue;
		OSMDB::ObjLine::const_ptr_t pl(boost::dynamic_pointer_cast<const OSMDB::ObjLine>(pobj));
		if (!pl)
			continue;
		tagname_t val_highway(pobj->find_tag(key_highway));
		tagname_t val_railway(pobj->find_tag(key_railway));
		tagname_t val_horse(pobj->find_tag(key_horse));
		tagname_t val_foot(pobj->find_tag(key_foot));
		tagname_t val_bicycle(pobj->find_tag(key_bicycle));
		tagname_t val_tracktype(pobj->find_tag(key_tracktype));
		tagname_t val_surface(pobj->find_tag(key_surface));
		tagname_t val_access(pobj->find_tag(key_access));
		tagname_t val_construction(pobj->find_tag(key_construction));
		tagname_t val_service(pobj->find_tag(key_service));
		tagname_t val_layer(pobj->find_tag(key_layer));
		if (val_highway != tagname_t::invalid) {
			RenderObjLines::ptr_t ro(create_lines(pl));
			if (ro) {
				{
					const char *cp(odb->find_tagname(val_highway));
					if (cp) {
						std::string s(cp);
						if (s.size() > 5 && !s.compare(s.size() - 5, 5, "_link")) {
							s.resize(s.size() - 5);
							ro->set_field("link", "yes");
						} else {
							ro->set_field("link", "no");
						}
						ro->set_field("feature", "highway_" + s);
					}
				}
				ro->copy_field_string(*odb, "horse", val_horse);
				ro->copy_field_string(*odb, "foot", val_foot);
				ro->copy_field_string(*odb, "bicycle", val_bicycle);
				ro->copy_field_string(*odb, "tracktype", val_tracktype);
				if (cond_surface_unpaved.is_in_set(val_surface))
					ro->set_field("int_surface", "unpaved");
				else if (cond_surface_paved.is_in_set(val_surface))
					ro->set_field("int_surface", "paved");
				else
					ro->set_field("int_surface");
				if (cond_access_dest.is_in_set(val_access))
					ro->set_field("access", "destination");
				else if (cond_access_no.is_in_set(val_access))
					ro->set_field("access", "no");
				else
					ro->set_field("access");
				ro->copy_field_string(*odb, "construction", val_construction);
				ro->set_field("service", cond_service_minor.is_in_set(val_service) ? "INT-minor" : "INT-normal");
				ro->copy_field_int(*odb, "layernotnull", val_layer, 0);
				robjs.add(ro);
			}
		}
		if (val_railway != tagname_t::invalid) {
			RenderObjLines::ptr_t ro(create_lines(pl));
			if (ro) {
				{
					const char *cp(odb->find_tagname(val_railway));
					if (cp) {
						ro->set_field("feature", std::string("railway_") + cp);
						if (cond_service_ssy.is_in_set(val_service)) {
							if (val_railway == name_preserved)
								ro->set_field("feature", "INT-preserved-ssy");
							else if (val_railway == name_rail)
								ro->set_field("feature", "INT-spur-siding-yard");
							else if (val_railway == name_tram)
								ro->set_field("feature", "tram-service");
						}
					}
				}
				ro->copy_field_string(*odb, "horse", val_horse);
				ro->copy_field_string(*odb, "foot", val_foot);
				ro->copy_field_string(*odb, "bicycle", val_bicycle);
				ro->copy_field_string(*odb, "tracktype", val_tracktype);
				ro->set_field("int_surface");
				if (cond_access_dest.is_in_set(val_access))
					ro->set_field("access", "destination");
				else if (cond_access_no.is_in_set(val_access))
					ro->set_field("access", "no");
				else
					ro->set_field("access");
				ro->copy_field_string(*odb, "construction", val_construction);
				ro->set_field("service", cond_service_minor.is_in_set(val_service) ? "INT-minor" : "INT-normal");
				ro->set_field("link", "no");
				ro->copy_field_int(*odb, "layernotnull", val_layer, 0);
				robjs.add(ro);
			}
		}
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_bridgetext(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	DbQueryWayPixelSorter robjs;
	tagkey_t key_manmade(odb->find_tagkey("man_made"));
	tagkey_t key_name(odb->find_tagkey("name"));
	tagname_t name_bridge(odb->find_tagname("bridge"));
	for (const OSMDB::Object::const_ptr_t& pobj : m_areas) {
		tagname_t val_manmade(pobj->find_tag(key_manmade));
		if (val_manmade != name_bridge)
			continue;
		RenderObjPoints::ptr_t ro(create_points(boost::dynamic_pointer_cast<const OSMDB::ObjArea>(pobj)));
		if (!ro)
			continue;
		{
			const RenderObj::fieldval_t& f(ro->find_field("way_pixels"));
			if (f.is_null())
				continue;
			double way_pixels;
			try {
				way_pixels = static_cast<double>(f);
			} catch (...) {
				continue;
			}
			if (way_pixels <= 125 || way_pixels >= 768000)
				continue;
		}
		tagname_t val_name(pobj->find_tag(key_name));
		ro->copy_field_string(*odb, "man_made", val_manmade);
		ro->copy_field_string(*odb, "name", val_name);
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_buildings(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	DbQueryLayerWayPixelSorter robjs;
	tagkey_t key_building(odb->find_tagkey("building"));
	tagkey_t key_amenity(odb->find_tagkey("amenity"));
	tagkey_t key_aeroway(odb->find_tagkey("aeroway"));
	tagkey_t key_aerialway(odb->find_tagkey("aerialway"));
	tagkey_t key_publictransport(odb->find_tagkey("public_transport"));
	tagkey_t key_layer(odb->find_tagkey("layer"));
	tagname_t name_no(odb->find_tagname("no"));
	for (const OSMDB::Object::const_ptr_t& pobj : m_areas) {
		tagname_t val_building(pobj->find_tag(key_building));
		if (val_building == tagname_t::invalid || val_building == name_no)
			continue;
		RenderObjPolygons::ptr_t ro(create_polygons(boost::dynamic_pointer_cast<const OSMDB::ObjArea>(pobj)));
		if (!ro)
			continue;
		tagname_t val_amenity(pobj->find_tag(key_amenity));
		tagname_t val_aeroway(pobj->find_tag(key_aeroway));
		tagname_t val_aerialway(pobj->find_tag(key_aerialway));
		tagname_t val_publictransport(pobj->find_tag(key_publictransport));
		tagname_t val_layer(pobj->find_tag(key_layer));
		ro->copy_field_string(*odb, "building", val_building);
		ro->copy_field_string(*odb, "amenity", val_amenity);
		ro->copy_field_string(*odb, "aeroway", val_aeroway);
		ro->copy_field_string(*odb, "aerialway", val_aerialway);
		ro->copy_field_string(*odb, "public_transport", val_publictransport);
		robjs.add(ro, odb->find_tagname(val_layer));
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_buildingtext(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	DbQueryWayPixelSorter robjs;
	tagkey_t key_building(odb->find_tagkey("building"));
	tagkey_t key_name(odb->find_tagkey("name"));
	tagname_t name_no(odb->find_tagname("no"));
	for (const OSMDB::Object::const_ptr_t& pobj : m_areas) {
		tagname_t val_building(pobj->find_tag(key_building));
		tagname_t val_name(pobj->find_tag(key_name));
		if (val_building == tagname_t::invalid || val_building == name_no || val_name == tagname_t::invalid)
			continue;
		RenderObjPoints::ptr_t ro(create_points(boost::dynamic_pointer_cast<const OSMDB::ObjArea>(pobj)));
		if (!ro)
			continue;
		ro->copy_field_string(*odb, "name", val_name);
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_capitalnames(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryIntegerSorter robjs;
	tagkey_t key_place(odb->find_tagkey("place"));
	tagkey_t key_name(odb->find_tagkey("name"));
	tagkey_t key_capital(odb->find_tagkey("capital"));
	tagkey_t key_population(odb->find_tagkey("population"));
	tagname_t name_yes(odb->find_tagname("yes"));
	TagNameSet cond_place(odb->create_tagnameset({ "city", "town", "village", "hamlet" }));
	for (const OSMDB::Object::const_ptr_t& pobj : m_points) {
		tagname_t val_place(pobj->find_tag(key_place));
		tagname_t val_name(pobj->find_tag(key_name));
		tagname_t val_capital(pobj->find_tag(key_capital));
		if (!(cond_place.is_in_set(val_place) && val_name != tagname_t::invalid && val_capital == name_yes))
			continue;		
		RenderObjPoints::ptr_t ro(create_points(boost::dynamic_pointer_cast<const OSMDB::ObjPoint>(pobj)));
		if (!ro)
			continue;
		tagname_t val_population(pobj->find_tag(key_population));
		ro->copy_field_string(*odb, "name", val_name);
		int64_t pop(0);
		if (val_population != tagname_t::invalid) {
			const char *cp(odb->find_tagname(val_population));
			if (cp) {
				char *cp1;
				pop = strtoll(cp, &cp1, 10);
				if (*cp1 || cp1 == cp || pop < 0 || pop > 99999999)
					pop = 0;
			}
		}
		ro->set_field("population", pop);
		ro->set_field("dir", ro->get_ids_hash() & 1);
		robjs.add(ro, -pop);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_cliffs(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryNoSorter robjs;
	tagkey_t key_natural(odb->find_tagkey("natural"));
	tagkey_t key_manmade(odb->find_tagkey("man_made"));
	tagname_t name_embankment(odb->find_tagname("embankment"));
	TagNameSet cond_natural(odb->create_tagnameset({ "arete", "cliff", "ridge" }));
	for (OSMDB::objects_t::const_iterator li(m_lines.begin()), le(m_lines.end()), ri(m_roads.begin()), re(m_roads.end()); li != re; ++li) {
		if (li == le)
			li = ri;
		const OSMDB::Object::const_ptr_t& pobj(*li);
		tagname_t val_natural(pobj->find_tag(key_natural));
		tagname_t val_manmade(pobj->find_tag(key_manmade));
		if (!cond_natural.is_in_set(val_natural) && val_manmade != name_embankment)
			continue;
		RenderObjLines::ptr_t ro(create_lines(boost::dynamic_pointer_cast<const OSMDB::ObjLine>(pobj)));
		if (!ro)
			continue;
		ro->copy_field_string(*odb, "natural", val_natural);
		ro->copy_field_string(*odb, "man_made", val_manmade);
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_countrynames(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	DbQueryWayPixelSorter robjs;
	tagkey_t key_boundary(odb->find_tagkey("boundary"));
	tagkey_t key_adminlevel(odb->find_tagkey("admin_level"));
	tagkey_t key_name(odb->find_tagkey("name"));
	tagname_t name_administrative(odb->find_tagname("administrative"));
	tagname_t name_two(odb->find_tagname("2"));
	for (const OSMDB::Object::const_ptr_t& pobj : m_areas) {
		tagname_t val_boundary(pobj->find_tag(key_boundary));
		tagname_t val_adminlevel(pobj->find_tag(key_adminlevel));
		tagname_t val_name(pobj->find_tag(key_name));
		if (!(val_boundary == name_administrative && val_adminlevel == name_two &&
		      val_name != tagname_t::invalid && pobj->get_id() < 0))
			continue;
		RenderObjPoints::ptr_t ro(create_points(boost::dynamic_pointer_cast<const OSMDB::ObjArea>(pobj)));
		if (!ro)
			continue;
		ro->copy_field_string(*odb, "name", val_name);
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_countynames(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryAdminLevelWayPixelSorter robjs;
	tagkey_t key_boundary(odb->find_tagkey("boundary"));
	tagkey_t key_adminlevel(odb->find_tagkey("admin_level"));
	tagkey_t key_name(odb->find_tagkey("name"));
	tagname_t name_administrative(odb->find_tagname("administrative"));
	TagNameSet cond_adminlevel(odb->create_tagnameset({ "5", "6" }));
	for (const OSMDB::Object::const_ptr_t& pobj : m_areas) {
		tagname_t val_boundary(pobj->find_tag(key_boundary));
		tagname_t val_adminlevel(pobj->find_tag(key_adminlevel));
		tagname_t val_name(pobj->find_tag(key_name));
		if (val_boundary != name_administrative || !cond_adminlevel.is_in_set(val_adminlevel) || val_name == tagname_t::invalid || pobj->get_id() >= 0)
			continue;
		RenderObjLines::ptr_t ro(create_lines(boost::dynamic_pointer_cast<const OSMDB::ObjArea>(pobj)));
		if (!ro)
			continue;
		{
			const RenderObj::fieldval_t& f(ro->find_field("way_pixels"));
			if (f.is_null())
				continue;
			try {
				double waypixels(static_cast<double>(f));
				if (waypixels < 12000 || waypixels > 196000)
					continue;
			} catch (...) {
				continue;
			}
		}
		ro->copy_field_string(*odb, "admin_level", val_adminlevel);
		ro->copy_field_string(*odb, "name", val_name);
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_entrances(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	DbQueryNoSorter robjs;
	tagkey_t key_entrance(odb->find_tagkey("entrance"));
	tagkey_t key_indoor(odb->find_tagkey("indoor"));
	tagkey_t key_access(odb->find_tagkey("access"));
	tagname_t name_no(odb->find_tagname("no"));
	for (const OSMDB::Object::const_ptr_t& pobj : m_points) {
		tagname_t val_entrance(pobj->find_tag(key_entrance));
		tagname_t val_indoor(pobj->find_tag(key_indoor));
		if (!(val_entrance != tagname_t::invalid &&
		      (val_indoor == name_no || val_indoor == tagname_t::invalid)))
			continue;
		RenderObjPoints::ptr_t ro(create_points(boost::dynamic_pointer_cast<const OSMDB::ObjPoint>(pobj)));
		if (!ro)
			continue;
		tagname_t val_access(pobj->find_tag(key_access));
		ro->copy_field_string(*odb, "entrance", val_entrance);
		ro->copy_field_string(*odb, "access", val_access);
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_ferryroutes(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	DbQueryNoSorter robjs;
	tagkey_t key_route(odb->find_tagkey("route"));
	tagname_t name_ferry(odb->find_tagname("ferry"));
	for (OSMDB::objects_t::const_iterator li(m_lines.begin()), le(m_lines.end()), ri(m_roads.begin()), re(m_roads.end()); li != re; ++li) {
		if (li == le)
			li = ri;
		const OSMDB::Object::const_ptr_t& pobj(*li);
		tagname_t val_route(pobj->find_tag(key_route));
		if (val_route != name_ferry)
			continue;
		RenderObjLines::ptr_t ro(create_lines(boost::dynamic_pointer_cast<const OSMDB::ObjLine>(pobj)));
		if (!ro)
			continue;
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_ferryroutestext(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	DbQueryNoSorter robjs;
	tagkey_t key_route(odb->find_tagkey("route"));
	tagkey_t key_name(odb->find_tagkey("name"));
	tagname_t name_ferry(odb->find_tagname("ferry"));
	for (OSMDB::objects_t::const_iterator li(m_lines.begin()), le(m_lines.end()), ri(m_roads.begin()), re(m_roads.end()); li != re; ++li) {
		if (li == le)
			li = ri;
		const OSMDB::Object::const_ptr_t& pobj(*li);
		tagname_t val_route(pobj->find_tag(key_route));
		if (val_route != name_ferry)
			continue;
		tagname_t val_name(pobj->find_tag(key_name));
		if (val_name == tagname_t::invalid)
			continue;
		RenderObjLines::ptr_t ro(create_lines(boost::dynamic_pointer_cast<const OSMDB::ObjLine>(pobj)));
		if (!ro)
			continue;
		ro->copy_field_string(*odb, "name", val_name);
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_guideways(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	DbQueryNoSorter robjs;
	tagkey_t key_highway(odb->find_tagkey("highway"));
	tagname_t name_busguideway(odb->find_tagname("bus_guideway"));
	for (OSMDB::objects_t::const_iterator li(m_lines.begin()), le(m_lines.end()), ri(m_roads.begin()), re(m_roads.end()); li != re; ++li) {
		if (li == le)
			li = ri;
		const OSMDB::Object::const_ptr_t& pobj(*li);
		tagname_t val_highway(pobj->find_tag(key_highway));
		if (val_highway != name_busguideway)
			continue;
		RenderObjLines::ptr_t ro(create_lines(boost::dynamic_pointer_cast<const OSMDB::ObjLine>(pobj)));
		if (!ro)
			continue;
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_highwayareacasing(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryLayerWayPixelSorter robjs;
	tagkey_t key_highway(odb->find_tagkey("highway"));
	tagkey_t key_railway(odb->find_tagkey("railway"));
	tagkey_t key_location(odb->find_tagkey("location"));
	tagkey_t key_tunnel(odb->find_tagkey("tunnel"));
	tagkey_t key_covered(odb->find_tagkey("covered"));
	tagkey_t key_layer(odb->find_tagkey("layer"));
	tagname_t name_platform(odb->find_tagname("platform"));
	tagname_t name_underground(odb->find_tagname("underground"));
	tagname_t name_yes(odb->find_tagname("yes"));
	TagNameSet cond_highway(odb->create_tagnameset({ "residential", "unclassified", "pedestrian", "service", "footway", "track", "path", "platform" }));
	TagNameSet cond_tunnel(odb->create_tagnameset({ "yes", "building_passage" }));
	for (const OSMDB::Object::const_ptr_t& pobj : m_areas) {
		tagname_t val_highway(pobj->find_tag(key_highway));
		tagname_t val_railway(pobj->find_tag(key_railway));
		tagname_t val_location(pobj->find_tag(key_location));
		tagname_t val_tunnel(pobj->find_tag(key_tunnel));
		tagname_t val_covered(pobj->find_tag(key_covered));
		if (!(cond_highway.is_in_set(val_highway) ||
		      (val_railway == name_platform && 
		       val_location != name_underground && 
		       !cond_tunnel.is_in_set(val_tunnel) &&
		       val_covered != name_yes)))
			continue;
		RenderObjPolygons::ptr_t ro(create_polygons(boost::dynamic_pointer_cast<const OSMDB::ObjArea>(pobj)));
		if (!ro)
			continue;
		tagname_t val_layer(pobj->find_tag(key_layer));
		ro->set_field("feature");
		if (cond_highway.is_in_set(val_highway)) {
			const char *cp(odb->find_tagname(val_highway));
			if (cp)
				ro->set_field("feature", "highway_" + std::string(cp));
		} else if (val_railway == name_platform &&
			   val_location != name_underground &&
			   cond_tunnel.is_in_set(val_tunnel) &&
			   val_covered != name_yes) {
			const char *cp(odb->find_tagname(val_railway));
			if (cp)
				ro->set_field("feature", "railway_" + std::string(cp));
		}
		robjs.add(ro, odb->find_tagname(val_layer));
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_highwayareafill(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryLayerWayPixelSorter robjs;
	tagkey_t key_highway(odb->find_tagkey("highway"));
	tagkey_t key_railway(odb->find_tagkey("railway"));
	tagkey_t key_location(odb->find_tagkey("location"));
	tagkey_t key_tunnel(odb->find_tagkey("tunnel"));
	tagkey_t key_covered(odb->find_tagkey("covered"));
	tagkey_t key_aeroway(odb->find_tagkey("aeroway"));
	tagkey_t key_layer(odb->find_tagkey("layer"));
	tagname_t name_platform(odb->find_tagname("platform"));
	tagname_t name_underground(odb->find_tagname("underground"));
	tagname_t name_yes(odb->find_tagname("yes"));
	TagNameSet cond_highway(odb->create_tagnameset({ "residential", "unclassified", "pedestrian", "service", "footway",
							   "living_street", "track", "path", "platform", "services" }));
	TagNameSet cond_tunnel(odb->create_tagnameset({ "yes", "building_passage" }));
	TagNameSet cond_aeroway(odb->create_tagnameset({ "runway", "taxiway", "helipad" }));
	for (const OSMDB::Object::const_ptr_t& pobj : m_areas) {
		tagname_t val_highway(pobj->find_tag(key_highway));
		tagname_t val_railway(pobj->find_tag(key_railway));
		tagname_t val_location(pobj->find_tag(key_location));
		tagname_t val_tunnel(pobj->find_tag(key_tunnel));
		tagname_t val_covered(pobj->find_tag(key_covered));
		tagname_t val_aeroway(pobj->find_tag(key_aeroway));
		if (!(cond_highway.is_in_set(val_highway) ||
		      (val_railway == name_platform && 
		       val_location != name_underground && 
		       !cond_tunnel.is_in_set(val_tunnel) &&
		       val_covered != name_yes) ||
		      cond_aeroway.is_in_set(val_aeroway)))
			continue;
		RenderObjPolygons::ptr_t ro(create_polygons(boost::dynamic_pointer_cast<const OSMDB::ObjArea>(pobj)));
		if (!ro)
			continue;
		tagname_t val_layer(pobj->find_tag(key_layer));
		ro->set_field("feature");
		if (cond_highway.is_in_set(val_highway)) {
			const char *cp(odb->find_tagname(val_highway));
			if (cp)
				ro->set_field("feature", "highway_" + std::string(cp));
		} else if (val_railway == name_platform &&
			   val_location != name_underground &&
			   cond_tunnel.is_in_set(val_tunnel) &&
			   val_covered != name_yes) {
			const char *cp(odb->find_tagname(val_railway));
			if (cp)
				ro->set_field("feature", "railway_" + std::string(cp));
		} else if (cond_aeroway.is_in_set(val_aeroway)) {
			const char *cp(odb->find_tagname(val_aeroway));
			if (cp)
				ro->set_field("feature", "aeroway_" + std::string(cp));
		}
		robjs.add(ro, odb->find_tagname(val_layer));
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_icesheetoutlines(void) const
{
	DbQueryNoSorter robjs;
	for (const OSMStaticDB::Object::const_ptr_t& pobj : m_icesheetoutlines) {
		RenderObjLines::ptr_t ro(create_lines(boost::dynamic_pointer_cast<const OSMStaticDB::ObjLine>(pobj)));
		if (!ro)
			continue;
		copytags(ro, pobj);
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_icesheetpoly(void) const
{
	DbQueryNoSorter robjs;
	for (const OSMStaticDB::Object::const_ptr_t& pobj : m_icesheetpolygons) {
		RenderObjPolygons::ptr_t ro(create_polygons(boost::dynamic_pointer_cast<const OSMStaticDB::ObjArea>(pobj)));
		if (!ro)
			continue;
		copytags(ro, pobj);
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_interpolation(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	DbQueryNoSorter robjs;
	tagkey_t key_addrinterpolation(odb->find_tagkey("addr:interpolation"));
	for (OSMDB::objects_t::const_iterator li(m_lines.begin()), le(m_lines.end()), ri(m_roads.begin()), re(m_roads.end()); li != re; ++li) {
		if (li == le)
			li = ri;
		const OSMDB::Object::const_ptr_t& pobj(*li);
		tagname_t val_addrinterpolation(pobj->find_tag(key_addrinterpolation));
		if (val_addrinterpolation == tagname_t::invalid)
			continue;
		RenderObjLines::ptr_t ro(create_lines(boost::dynamic_pointer_cast<const OSMDB::ObjLine>(pobj)));
		if (!ro)
			continue;
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_junctions(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	DbQueryWayPixelSorter robjs;
	tagkey_t key_highway(odb->find_tagkey("highway"));
	tagkey_t key_junction(odb->find_tagkey("junction"));
	tagkey_t key_ref(odb->find_tagkey("ref"));
	tagkey_t key_name(odb->find_tagkey("name"));
	tagname_t name_motorwayjunction(odb->find_tagname("motorway_junction"));
	tagname_t name_trafficsignals(odb->find_tagname("traffic_signals"));
	tagname_t name_yes(odb->find_tagname("yes"));
	for (const OSMDB::Object::const_ptr_t& pobj : m_areas) {
		tagname_t val_junction(pobj->find_tag(key_junction));
		if (val_junction != name_yes)
			continue;
		tagname_t val_highway(pobj->find_tag(key_highway));
		tagname_t val_ref(pobj->find_tag(key_ref));
		tagname_t val_name(pobj->find_tag(key_name));
		RenderObjPoints::ptr_t ro(create_points(boost::dynamic_pointer_cast<const OSMDB::ObjArea>(pobj)));
		if (!ro)
			continue;
		ro->copy_field_string(*odb, "highway", val_highway);
		ro->copy_field_string(*odb, "junction", val_junction);
		ro->copy_field_string(*odb, "ref", val_ref);
		ro->copy_field_string(*odb, "name", val_name);
		robjs.add(ro);
	}
	for (const OSMDB::Object::const_ptr_t& pobj : m_points) {
		tagname_t val_junction(pobj->find_tag(key_junction));
		tagname_t val_highway(pobj->find_tag(key_highway));
		if (val_highway != name_motorwayjunction && val_highway != name_trafficsignals && val_junction != name_yes)
			continue;
		tagname_t val_ref(pobj->find_tag(key_ref));
		tagname_t val_name(pobj->find_tag(key_name));
		RenderObjPoints::ptr_t ro(create_points(boost::dynamic_pointer_cast<const OSMDB::ObjPoint>(pobj)));
		if (!ro)
			continue;
		ro->set_field("way_pixels");
		ro->copy_field_string(*odb, "highway", val_highway);
		ro->copy_field_string(*odb, "junction", val_junction);
		ro->copy_field_string(*odb, "ref", val_ref);
		ro->copy_field_string(*odb, "name", val_name);
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_landcover(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryWayPixelFeatureSorter robjs;
	tagkey_t key_leisure(odb->find_tagkey("leisure"));
	tagkey_t key_aeroway(odb->find_tagkey("aeroway"));
	tagkey_t key_amenity(odb->find_tagkey("amenity"));
	tagkey_t key_man_made(odb->find_tagkey("man_made"));
	tagkey_t key_natural(odb->find_tagkey("natural"));
	tagkey_t key_power(odb->find_tagkey("power"));
	tagkey_t key_shop(odb->find_tagkey("shop"));
	tagkey_t key_tourism(odb->find_tagkey("tourism"));
	tagkey_t key_highway(odb->find_tagkey("highway"));
	tagkey_t key_railway(odb->find_tagkey("railway"));
	tagkey_t key_religion(odb->find_tagkey("religion"));
	tagkey_t key_parking(odb->find_tagkey("parking"));
	tagkey_t key_location(odb->find_tagkey("location"));
	tagkey_t key_name(odb->find_tagkey("name"));
	tagkey_t key_landuse(odb->find_tagkey("landuse"));
	tagkey_t key_building(odb->find_tagkey("building"));
	tagkey_t key_wetland(odb->find_tagkey("wetland"));
	TagNameSet cond_aeroway(odb->create_tagnameset({ "apron", "aerodrome" }));
	TagNameSet cond_amenity(odb->create_tagnameset({ "parking", "bicycle_parking", "motorcycle_parking", "taxi",
							   "university", "college", "school", "hospital", "kindergarten",
							   "grave_yard", "place_of_worship", "prison", "clinic", "ferry_terminal",
							   "marketplace", "community_centre", "social_facility",
							   "arts_centre", "parking_space", "bus_station", "fire_station", "police" }));
	TagNameSet cond_man_made(odb->create_tagnameset({ "works", "wastewater_plant", "water_works" }));
	TagNameSet cond_natural(odb->create_tagnameset({ "beach", "shoal", "heath", "mud", "wetland", "grassland", "wood", "sand",
							   "scree", "shingle", "bare_rock", "scrub" }));
	TagNameSet cond_power(odb->create_tagnameset({ "plant", "substation", "generator" }));
	TagNameSet cond_shop(odb->create_tagnameset({ "mall" }));
	TagNameSet cond_tourism(odb->create_tagnameset({ "camp_site", "caravan_site", "picnic_site" }));
	TagNameSet cond_highway(odb->create_tagnameset({ "services", "rest_area" }));
	TagNameSet cond_railway(odb->create_tagnameset({ "station" }));
	TagNameSet ftr_amenity(odb->create_tagnameset({ "bicycle_parking", "motorcycle_parking", "university", "college", "school", "taxi",
							  "hospital", "kindergarten", "grave_yard", "prison", "place_of_worship", "clinic", "ferry_terminal",
							  "marketplace", "community_centre", "social_facility", "arts_centre", "parking_space", "bus_station",
							  "fire_station", "police" }));
	TagNameSet ftr_natural(odb->create_tagnameset({ "beach", "shoal", "heath", "grassland", "wood", "sand", "scree", "shingle", "bare_rock", "scrub" }));
	TagNameSet ftr_leisure(odb->create_tagnameset({ "swimming_pool", "playground", "park", "recreation_ground", "garden",
							  "golf_course", "miniature_golf", "sports_centre", "stadium", "pitch", "ice_rink",
							  "track", "dog_park", "fitness_station", "water_park" }));
	TagNameSet ftr_landuse(odb->create_tagnameset({ "quarry", "vineyard", "orchard", "cemetery", "residential", "garages", "meadow", "grass",
							  "allotments", "forest", "farmyard", "farmland", "greenhouse_horticulture",
							  "recreation_ground", "village_green", "retail", "industrial", "railway", "commercial",
							  "brownfield", "landfill", "salt_pond", "construction", "plant_nursery", "religious" }));
	TagNameSet ftr_religion(odb->create_tagnameset({ "christian", "jewish", "muslim" }));
	tagname_t name_underground(odb->find_tagname("underground"));
	tagname_t name_wetland(odb->find_tagname("wetland"));
	tagname_t name_mud(odb->find_tagname("mud"));
	tagname_t name_no(odb->find_tagname("no"));
	tagname_t name_parking(odb->find_tagname("parking"));
	for (const OSMDB::Object::const_ptr_t& pobj : m_areas) {
		tagname_t val_landuse(pobj->find_tag(key_landuse));
		tagname_t val_leisure(pobj->find_tag(key_leisure));
		tagname_t val_aeroway(pobj->find_tag(key_aeroway));
		tagname_t val_amenity(pobj->find_tag(key_amenity));
		tagname_t val_man_made(pobj->find_tag(key_man_made));
		tagname_t val_natural(pobj->find_tag(key_natural));
		tagname_t val_power(pobj->find_tag(key_power));
		tagname_t val_shop(pobj->find_tag(key_shop));
		tagname_t val_tourism(pobj->find_tag(key_tourism));
		tagname_t val_highway(pobj->find_tag(key_highway));
		tagname_t val_railway(pobj->find_tag(key_railway));
		if (val_landuse == tagname_t::invalid &&
		    val_leisure == tagname_t::invalid &&
		    !cond_aeroway.is_in_set(val_aeroway) &&
		    !cond_amenity.is_in_set(val_amenity) &&
		    !cond_man_made.is_in_set(val_man_made) &&
		    !cond_natural.is_in_set(val_natural) &&
		    !cond_power.is_in_set(val_power) &&
		    !cond_shop.is_in_set(val_shop) &&
		    !cond_tourism.is_in_set(val_tourism) &&
		    !cond_highway.is_in_set(val_highway) &&
		    !cond_railway.is_in_set(val_railway))
			continue;
		RenderObjPolygons::ptr_t ro(create_polygons(boost::dynamic_pointer_cast<const OSMDB::ObjArea>(pobj)));
		if (!ro)
			continue;
		tagname_t val_name(pobj->find_tag(key_name));
		{
			const char *cp(odb->find_tagname(val_name));
			ro->set_field("name", cp ? cp : "");
		}
		tagname_t val_location(pobj->find_tag(key_location));
		tagname_t val_wetland(pobj->find_tag(key_wetland));
		tagname_t val_building(pobj->find_tag(key_building));
		tagname_t val_parking(pobj->find_tag(key_parking));
		tagname_t val_religion(pobj->find_tag(key_religion));
		// feature: railway
		if (cond_railway.is_in_set(val_railway)) {
			const char *cp(odb->find_tagname(val_railway));
			if (cp)
				ro->set_field("feature", "railway_" + std::string(cp));
		}
		// feature: highway
		if (cond_highway.is_in_set(val_highway)) {
			const char *cp(odb->find_tagname(val_highway));
			if (cp)
				ro->set_field("feature", "highway_" + std::string(cp));
		}
		// feature: tourism
		if (cond_tourism.is_in_set(val_tourism)) {
			const char *cp(odb->find_tagname(val_tourism));
			if (cp)
				ro->set_field("feature", "tourism_" + std::string(cp));
		}
		// feature: shop
		if (cond_shop.is_in_set(val_shop) && (val_location == tagname_t::invalid || val_location != name_underground)) {
			const char *cp(odb->find_tagname(val_shop));
			if (cp)
				ro->set_field("feature", "shop_" + std::string(cp));
		}
		// feature: natural
		if (ftr_natural.is_in_set(val_natural)) {
			const char *cp(odb->find_tagname(val_natural));
			if (cp)
				ro->set_field("feature", "natural_" + std::string(cp));
		}
		// feature: man_made
		if (cond_man_made.is_in_set(val_man_made)) {
			const char *cp(odb->find_tagname(val_man_made));
			if (cp)
				ro->set_field("feature", "man_made_" + std::string(cp));
		}
		// feature: leisure
		if (ftr_leisure.is_in_set(val_leisure)) {
			const char *cp(odb->find_tagname(val_leisure));
			if (cp)
				ro->set_field("feature", "leisure_" + std::string(cp));
		}
		// feature: landuse
		if (ftr_landuse.is_in_set(val_landuse)) {
			const char *cp(odb->find_tagname(val_landuse));
			if (cp)
				ro->set_field("feature", "landuse_" + std::string(cp));
		}
		// feature: power
		if (cond_power.is_in_set(val_power)) {
			const char *cp(odb->find_tagname(val_power));
			if (cp)
				ro->set_field("feature", "power_" + std::string(cp));
		}
		// feature: wetland
		if (val_natural != tagname_t::invalid) {
			if (val_natural == name_mud) {
				ro->set_field("feature", "wetland_natural");
			} else if (val_natural == name_wetland && val_wetland != tagname_t::invalid) {
				const char *cp(odb->find_tagname(val_power));
				if (cp)
					ro->set_field("feature", "wetland_" + std::string(cp));
			}
		}
		// feature: amenity
		if (cond_amenity.is_in_set(val_amenity) ||
		    (val_amenity != tagname_t::invalid && val_amenity == name_parking &&
		     (val_parking == tagname_t::invalid || val_parking != name_underground))) {
			const char *cp(odb->find_tagname(val_amenity));
			if (cp)
				ro->set_field("feature", "amenity_" + std::string(cp));
		}
		// feature: aeroway
		if (cond_aeroway.is_in_set(val_aeroway)) {
			const char *cp(odb->find_tagname(val_aeroway));
			if (cp)
				ro->set_field("feature", "aeroway_" + std::string(cp));
		}
		// religion
		ro->set_field("religion", "INT-generic");
		if (ftr_religion.is_in_set(val_religion))
			ro->copy_field_string(*odb, "religion", val_religion);
		// is_building
		ro->set_field("is_building", (val_building == tagname_t::invalid || val_building == name_no) ? "no" : "yes");
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_landcoverareasymbols(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryLayerWayPixelSorter robjs;
	tagkey_t key_natural(odb->find_tagkey("natural"));
	tagkey_t key_landuse(odb->find_tagkey("landuse"));
	tagkey_t key_building(odb->find_tagkey("building"));
	tagkey_t key_surface(odb->find_tagkey("surface"));
	tagkey_t key_wetland(odb->find_tagkey("wetland"));
	tagkey_t key_leaftype(odb->find_tagkey("leaf_type"));
	tagkey_t key_layer(odb->find_tagkey("layer"));
	tagname_t name_forest(odb->find_tagname("forest"));
	tagname_t name_mud(odb->find_tagname("mud"));
	tagname_t name_wetland(odb->find_tagname("wetland"));
	TagNameSet cond_natural(odb->create_tagnameset({ "mud", "wetland", "wood", "beach", "shoal", "reef", "scrub", "sand" }));
	TagNameSet cond_landuse(odb->create_tagnameset({ "forest", "salt_pond" }));
	for (const OSMDB::Object::const_ptr_t& pobj : m_areas) {
		tagname_t val_landuse(pobj->find_tag(key_landuse));
		tagname_t val_natural(pobj->find_tag(key_natural));
		tagname_t val_building(pobj->find_tag(key_building));
		if ((!cond_natural.is_in_set(val_natural) && !cond_landuse.is_in_set(val_landuse)) || val_building != tagname_t::invalid)
			continue;
		RenderObjPolygons::ptr_t ro(create_polygons(boost::dynamic_pointer_cast<const OSMDB::ObjArea>(pobj)));
		if (!ro)
			continue;
		tagname_t val_surface(pobj->find_tag(key_surface));
		tagname_t val_wetland(pobj->find_tag(key_wetland));
		tagname_t val_leaftype(pobj->find_tag(key_leaftype));
		tagname_t val_layer(pobj->find_tag(key_layer));
		ro->copy_field_string(*odb, "surface", val_surface);
		if (val_landuse == name_forest)
			ro->set_field("natural", "wood");
		else
			ro->copy_field_string(*odb, "natural", val_natural);
		if (val_natural == name_mud) {
			ro->set_field("int_wetland", "mud");
		} else if (val_natural == name_wetland) {
			if (val_wetland == tagname_t::invalid)
				ro->set_field("int_wetland", "wetland");
			else
				ro->copy_field_string(*odb, "int_wetland", val_wetland);
		} else {
			ro->set_field("int_wetland");
		}
		ro->copy_field_string(*odb, "landuse", val_landuse);
		ro->copy_field_string(*odb, "leaf_type", val_leaftype);
		robjs.add(ro, odb->find_tagname(val_layer));
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_landcoverline(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	DbQueryNoSorter robjs;
	tagkey_t key_manmade(odb->find_tagkey("man_made"));
	tagname_t name_cutline(odb->find_tagname("cutline"));
	for (OSMDB::objects_t::const_iterator li(m_lines.begin()), le(m_lines.end()), ri(m_roads.begin()), re(m_roads.end()); li != re; ++li) {
		if (li == le)
			li = ri;
		const OSMDB::Object::const_ptr_t& pobj(*li);
		tagname_t val_manmade(pobj->find_tag(key_manmade));
		if (val_manmade != name_cutline)
			continue;
		RenderObjLines::ptr_t ro(create_lines(boost::dynamic_pointer_cast<const OSMDB::ObjLine>(pobj)));
		if (!ro)
			continue;
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_landcoverlowzoom(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryWayPixelFeatureSorter robjs;
	tagkey_t key_landuse(odb->find_tagkey("landuse"));
	tagkey_t key_natural(odb->find_tagkey("natural"));
	tagkey_t key_wetland(odb->find_tagkey("wetland"));
	tagkey_t key_building(odb->find_tagkey("building"));
	tagname_t name_wetland(odb->find_tagname("wetland"));
	tagname_t name_mud(odb->find_tagname("mud"));
	TagNameSet cond_landuse(odb->create_tagnameset({ "forest", "farmland", "residential", "commercial", "retail", "industrial",
							   "meadow", "grass", "village_green", "vineyard", "orchard" }));
	TagNameSet cond_natural(odb->create_tagnameset({ "wood", "wetland", "mud", "sand", "scree", "shingle", "bare_rock", "heath",
							   "grassland", "scrub" }));
	TagNameSet ftr_landuse(odb->create_tagnameset({ "forest", "farmland", "residential", "commercial", "retail", "industrial",
							  "meadow", "grass", "village_green", "vineyard", "orchard" }));
	TagNameSet ftr_natural(odb->create_tagnameset({ "wood", "sand", "scree", "shingle", "bare_rock", "heath", "grassland", "scrub" }));
	for (const OSMDB::Object::const_ptr_t& pobj : m_areas) {
		tagname_t val_landuse(pobj->find_tag(key_landuse));
		tagname_t val_natural(pobj->find_tag(key_natural));
		tagname_t val_building(pobj->find_tag(key_building));
		if ((!cond_landuse.is_in_set(val_landuse) && !cond_natural.is_in_set(val_natural)) || val_building != tagname_t::invalid)
			continue;
		RenderObjPolygons::ptr_t ro(create_polygons(boost::dynamic_pointer_cast<const OSMDB::ObjArea>(pobj)));
		if (!ro)
			continue;
		tagname_t val_wetland(pobj->find_tag(key_wetland));
		if (val_natural == name_mud)
			ro->set_field("feature", "wetland_mud");
		else if (val_natural == name_wetland && val_wetland != tagname_t::invalid)
			ro->set_field("feature", std::string("wetland_") + odb->find_tagname(val_wetland));
		else if (ftr_landuse.is_in_set(val_landuse))
			ro->set_field("feature", std::string("landuse_") + odb->find_tagname(val_landuse));
		else if (ftr_natural.is_in_set(val_natural))
			ro->set_field("feature", std::string("natural_") + odb->find_tagname(val_natural));
		else
			ro->set_field("feature");
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_landuseoverlay(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	DbQueryNoSorter robjs;
	tagkey_t key_landuse(odb->find_tagkey("landuse"));
	tagkey_t key_military(odb->find_tagkey("military"));
	tagkey_t key_building(odb->find_tagkey("building"));
	tagname_t name_military(odb->find_tagname("military"));
	tagname_t name_dangerarea(odb->find_tagname("danger_area"));
	for (const OSMDB::Object::const_ptr_t& pobj : m_areas) {
		tagname_t val_landuse(pobj->find_tag(key_landuse));
		tagname_t val_military(pobj->find_tag(key_military));
		tagname_t val_building(pobj->find_tag(key_building));
		if ((val_landuse != name_military && val_military != name_dangerarea) || val_building != tagname_t::invalid)
			continue;
		RenderObjPolygons::ptr_t ro(create_polygons(boost::dynamic_pointer_cast<const OSMDB::ObjArea>(pobj)));
		if (!ro)
			continue;
		ro->copy_field_string(*odb, "landuse", val_landuse);
		ro->copy_field_string(*odb, "military", val_military);
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_marinasarea(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	DbQueryNoSorter robjs;
	tagkey_t key_leisure(odb->find_tagkey("leisure"));
	tagname_t name_marina(odb->find_tagname("marina"));
	for (const OSMDB::Object::const_ptr_t& pobj : m_areas) {
		tagname_t val_leisure(pobj->find_tag(key_leisure));
		if (val_leisure != name_marina)
			continue;
		RenderObjPolygons::ptr_t ro(create_polygons(boost::dynamic_pointer_cast<const OSMDB::ObjArea>(pobj)));
		if (!ro)
			continue;
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_necountries(void) const
{
	DbQueryNoSorter robjs;
	for (const OSMStaticDB::Object::const_ptr_t& pobj : m_boundarylinesland) {
		RenderObjLines::ptr_t ro(create_lines(boost::dynamic_pointer_cast<const OSMStaticDB::ObjLine>(pobj)));
		if (!ro)
			continue;
		copytags(ro, pobj);
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_ocean(void) const
{
	DbQueryNoSorter robjs;
	for (const OSMStaticDB::Object::const_ptr_t& pobj : m_waterpolygons) {
		RenderObjPolygons::ptr_t ro(create_polygons(boost::dynamic_pointer_cast<const OSMStaticDB::ObjArea>(pobj)));
		if (!ro)
			continue;
		copytags(ro, pobj);
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_oceanlz(void) const
{
	DbQueryNoSorter robjs;
	for (const OSMStaticDB::Object::const_ptr_t& pobj : m_simplifiedwaterpolygons) {
		RenderObjPolygons::ptr_t ro(create_polygons(boost::dynamic_pointer_cast<const OSMStaticDB::ObjArea>(pobj)));
		if (!ro)
			continue;
		copytags(ro, pobj);
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_pathstextname(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryNoSorter robjs;
	tagkey_t key_highway(odb->find_tagkey("highway"));
	tagkey_t key_name(odb->find_tagkey("name"));
	tagkey_t key_oneway(odb->find_tagkey("oneway"));
	tagkey_t key_junction(odb->find_tagkey("junction"));
	tagkey_t key_construction(odb->find_tagkey("construction"));
	tagkey_t key_horse(odb->find_tagkey("horse"));
	tagkey_t key_bicycle(odb->find_tagkey("bicycle"));
	tagname_t name_roundabout(odb->find_tagname("roundabout"));
	TagNameSet cond_highway(odb->create_tagnameset({ "bridleway", "footway", "cycleway", "path", "track", "steps", "construction" }));
	TagNameSet cond_oneway(odb->create_tagnameset({ "yes", "-1" }));
	TagNameSet cond_notoneway(odb->create_tagnameset({ "no", "reversible" }));
	for (OSMDB::objects_t::const_iterator li(m_lines.begin()), le(m_lines.end()), ri(m_roads.begin()), re(m_roads.end()); li != re; ++li) {
		if (li == le)
			li = ri;
		const OSMDB::Object::const_ptr_t& pobj(*li);
		tagname_t val_highway(pobj->find_tag(key_highway));
		tagname_t val_name(pobj->find_tag(key_name));
		tagname_t val_oneway(pobj->find_tag(key_oneway));
		tagname_t val_junction(pobj->find_tag(key_junction));
		if (!(cond_highway.is_in_set(val_highway) &&
		      (val_name != tagname_t::invalid ||
		       cond_oneway.is_in_set(val_oneway) ||
		       val_junction == name_roundabout)))
			continue;
		RenderObjLines::ptr_t ro(create_lines(boost::dynamic_pointer_cast<const OSMDB::ObjLine>(pobj)));
		if (!ro)
			continue;
		tagname_t val_construction(pobj->find_tag(key_construction));
		tagname_t val_horse(pobj->find_tag(key_horse));
		tagname_t val_bicycle(pobj->find_tag(key_bicycle));
		ro->copy_field_string(*odb, "highway", val_highway);
		ro->copy_field_string(*odb, "name", val_name);
		ro->copy_field_string(*odb, "construction", val_construction);
		if (cond_oneway.is_in_set(val_oneway))
			ro->copy_field_string(*odb, "oneway", val_oneway);
		else if (val_junction == name_roundabout && !cond_notoneway.is_in_set(val_oneway))
			ro->set_field("oneway", "yes");
		else
			ro->set_field("oneway");
		ro->copy_field_string(*odb, "horse", val_horse);
		ro->copy_field_string(*odb, "bicycle", val_bicycle);
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_piersline(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryNoSorter robjs;
	tagkey_t key_manmade(odb->find_tagkey("man_made"));
	TagNameSet cond_manmade(odb->create_tagnameset({ "pier", "breakwater", "groyne" }));
	for (OSMDB::objects_t::const_iterator li(m_lines.begin()), le(m_lines.end()), ri(m_roads.begin()), re(m_roads.end()); li != re; ++li) {
		if (li == le)
			li = ri;
		const OSMDB::Object::const_ptr_t& pobj(*li);
		tagname_t val_manmade(pobj->find_tag(key_manmade));
		if (!cond_manmade.is_in_set(val_manmade))
			continue;
		RenderObjLines::ptr_t ro(create_lines(boost::dynamic_pointer_cast<const OSMDB::ObjLine>(pobj)));
		if (!ro)
			continue;
		ro->copy_field_string(*odb, "man_made", val_manmade);
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_pierspoly(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryNoSorter robjs;
	tagkey_t key_manmade(odb->find_tagkey("man_made"));
	TagNameSet cond_manmade(odb->create_tagnameset({ "pier", "breakwater", "groyne" }));
	for (const OSMDB::Object::const_ptr_t& pobj : m_areas) {
		tagname_t val_manmade(pobj->find_tag(key_manmade));
		if (!cond_manmade.is_in_set(val_manmade))
			continue;
		RenderObjPolygons::ptr_t ro(create_polygons(boost::dynamic_pointer_cast<const OSMDB::ObjArea>(pobj)));
		if (!ro)
			continue;
		ro->copy_field_string(*odb, "man_made", val_manmade);
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_placenamesmedium(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryScoreNameSorter robjs;
	tagkey_t key_place(odb->find_tagkey("place"));
	tagkey_t key_name(odb->find_tagkey("name"));
	tagkey_t key_capital(odb->find_tagkey("capital"));
	tagkey_t key_population(odb->find_tagkey("population"));
	tagname_t name_yes(odb->find_tagname("yes"));
	tagname_t name_four(odb->find_tagname("4"));
	tagname_t name_city(odb->find_tagname("city"));
	tagname_t name_town(odb->find_tagname("town"));
	TagNameSet cond_place(odb->create_tagnameset({ "city", "town" }));
	for (const OSMDB::Object::const_ptr_t& pobj : m_points) {
		tagname_t val_place(pobj->find_tag(key_place));
		tagname_t val_name(pobj->find_tag(key_name));
		tagname_t val_capital(pobj->find_tag(key_capital));
		if (!(cond_place.is_in_set(val_place) && val_name != tagname_t::invalid &&
		      val_capital != name_yes))
			continue;
		RenderObjPoints::ptr_t ro(create_points(boost::dynamic_pointer_cast<const OSMDB::ObjPoint>(pobj)));
		if (!ro)
			continue;
		tagname_t val_population(pobj->find_tag(key_population));
		ro->copy_field_string(*odb, "name", val_name);
		int64_t score(1);
		if (val_place == name_town)
			score = 1000;
		else if (val_place == name_city)
			score = 100000;
		{
			const char *cp(odb->find_tagname(val_population));
			if (cp) {
                                char *cp1;
                                long x(strtoll(cp, &cp1, 10));
                                if (!*cp1 && cp1 != cp && x >= 0 && x <= 99999999)
					score = x;
			}
		}
		if (val_capital == name_four)
			score *= 2;
		ro->set_field("score", score);
		ro->set_field("category", static_cast<int64_t>((val_place == name_city) ? 1 : 2));
		ro->set_field("dir", ro->get_ids_hash() & 1);
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_placenamessmall(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryScoreNameSorter robjs;
	tagkey_t key_place(odb->find_tagkey("place"));
	tagkey_t key_capital(odb->find_tagkey("capital"));
	tagkey_t key_leisure(odb->find_tagkey("leisure"));
	tagkey_t key_name(odb->find_tagkey("name"));
	tagname_t name_yes(odb->find_tagname("yes"));
	tagname_t name_square(odb->find_tagname("square"));
	tagname_t name_suburb(odb->find_tagname("suburb"));
	tagname_t name_village(odb->find_tagname("village"));
	tagname_t name_hamlet(odb->find_tagname("hamlet"));
	tagname_t name_quarter(odb->find_tagname("quarter"));
	tagname_t name_neighbourhood(odb->find_tagname("neighbourhood"));
	tagname_t name_isolateddwelling(odb->find_tagname("isolated_dwelling"));
	tagname_t name_farm(odb->find_tagname("farm"));
	TagNameSet cond_place1(odb->create_tagnameset({ "village", "hamlet" }));
	TagNameSet cond_place2(odb->create_tagnameset({ "suburb", "quarter", "neighbourhood", "isolated_dwelling", "farm" }));
	TagNameSet cond_leisure(odb->create_tagnameset({ "park", "recreation_ground", "garden" }));
	for (const OSMDB::Object::const_ptr_t& pobj : m_points) {
		tagname_t val_place(pobj->find_tag(key_place));
		tagname_t val_capital(pobj->find_tag(key_capital));
		tagname_t val_leisure(pobj->find_tag(key_leisure));
		tagname_t val_name(pobj->find_tag(key_name));
		if (val_name == tagname_t::invalid ||
		    (!(cond_place1.is_in_set(val_place) && val_capital != name_yes) &&
		     !(cond_place2.is_in_set(val_place) || (val_place == name_square && !cond_leisure.is_in_set(val_leisure)))))
			continue;
		RenderObjPoints::ptr_t ro(create_points(boost::dynamic_pointer_cast<const OSMDB::ObjPoint>(pobj)));
		if (!ro)
			continue;
		ro->copy_field_string(*odb, "place", val_place);
		ro->copy_field_string(*odb, "leisure", val_leisure);
		ro->copy_field_string(*odb, "name", val_name);
		long score(0);
		if (val_place == name_suburb)
			score = 8;
		else if (val_place == name_village)
			score = 7;
		else if (val_place == name_hamlet)
			score = 6;
		else if (val_place == name_quarter)
			score = 5;
		else if (val_place == name_neighbourhood)
			score = 4;
		else if (val_place == name_isolateddwelling)
			score = 3;
		else if (val_place == name_farm)
			score = 2;
		else if (val_place == name_square)
			score = 1;
		robjs.add(ro, score);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_powerline(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryNoSorter robjs;
	tagkey_t key_power(odb->find_tagkey("power"));
	tagkey_t key_location(odb->find_tagkey("location"));
	tagname_t name_line(odb->find_tagname("line"));
	tagname_t name_cable(odb->find_tagname("cable"));
	TagNameSet cond_location(odb->create_tagnameset({ "overground", "overhead", "surface", "outdoor", "platform" }));
	for (OSMDB::objects_t::const_iterator li(m_lines.begin()), le(m_lines.end()), ri(m_roads.begin()), re(m_roads.end()); li != re; ++li) {
		if (li == le)
			li = ri;
		const OSMDB::Object::const_ptr_t& pobj(*li);
		tagname_t val_power(pobj->find_tag(key_power));
		tagname_t val_location(pobj->find_tag(key_location));
		if (val_power != name_line && (val_power != name_cable || !cond_location.is_in_set(val_location)))
			continue;
		RenderObjLines::ptr_t ro(create_lines(boost::dynamic_pointer_cast<const OSMDB::ObjLine>(pobj)));
		if (!ro)
			continue;
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_powerminorline(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	DbQueryNoSorter robjs;
	tagkey_t key_power(odb->find_tagkey("power"));
	tagname_t name_minorline(odb->find_tagname("minor_line"));
	for (OSMDB::objects_t::const_iterator li(m_lines.begin()), le(m_lines.end()), ri(m_roads.begin()), re(m_roads.end()); li != re; ++li) {
		if (li == le)
			li = ri;
		const OSMDB::Object::const_ptr_t& pobj(*li);
		tagname_t val_power(pobj->find_tag(key_power));
		if (val_power != name_minorline)
			continue;
		RenderObjLines::ptr_t ro(create_lines(boost::dynamic_pointer_cast<const OSMDB::ObjLine>(pobj)));
		if (!ro)
			continue;
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_powertowers(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	DbQueryIntegerSorter robjs;
	tagkey_t key_power(odb->find_tagkey("power"));
	tagname_t name_pole(odb->find_tagname("pole"));
	tagname_t name_tower(odb->find_tagname("tower"));
	for (const OSMDB::Object::const_ptr_t& pobj : m_points) {
		tagname_t val_power(pobj->find_tag(key_power));
		if (val_power != name_tower && val_power != name_pole)
			continue;
		RenderObjPoints::ptr_t ro(create_points(boost::dynamic_pointer_cast<const OSMDB::ObjPoint>(pobj)));
		if (!ro)
			continue;
		ro->copy_field_string(*odb, "power", val_power);
		robjs.add(ro, (val_power == name_tower) ? -2 : (val_power == name_pole) ? -1 : 0);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_protectedareas(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryNoSorter robjs;
	tagkey_t key_boundary(odb->find_tagkey("boundary"));
	tagkey_t key_protectclass(odb->find_tagkey("protect_class"));
	tagkey_t key_building(odb->find_tagkey("building"));
	tagkey_t key_leisure(odb->find_tagkey("leisure"));
	tagname_t name_naturereserve(odb->find_tagname("nature_reserve"));
	tagname_t name_protectedarea(odb->find_tagname("protected_area"));
	TagNameSet cond_boundary(odb->create_tagnameset({ "aboriginal_lands", "national_park" }));
	TagNameSet cond_protectclass(odb->create_tagnameset({ "1", "1a", "1b", "2", "3", "4", "5", "6", "7", "24", "97", "98", "99" }));
	for (const OSMDB::Object::const_ptr_t& pobj : m_areas) {
		tagname_t val_boundary(pobj->find_tag(key_boundary));
		tagname_t val_leisure(pobj->find_tag(key_leisure));
		tagname_t val_protectclass(pobj->find_tag(key_protectclass));
		tagname_t val_building(pobj->find_tag(key_building));
		if ((!cond_boundary.is_in_set(val_leisure) && val_leisure != name_naturereserve &&
		     (val_boundary != name_protectedarea || !cond_protectclass.is_in_set(val_protectclass)))
		    || val_building != tagname_t::invalid)
			continue;
		RenderObjPolygons::ptr_t ro(create_polygons(boost::dynamic_pointer_cast<const OSMDB::ObjArea>(pobj)));
		if (!ro)
			continue;
		ro->copy_field_string(*odb, "boundary", val_boundary);
		ro->copy_field_string(*odb, "protect_class", val_protectclass);
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_protectedareastext(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryNoSorter robjs;
	tagkey_t key_boundary(odb->find_tagkey("boundary"));
	tagkey_t key_protectclass(odb->find_tagkey("protect_class"));
	tagkey_t key_name(odb->find_tagkey("name"));
	tagkey_t key_leisure(odb->find_tagkey("leisure"));
	tagname_t name_naturereserve(odb->find_tagname("nature_reserve"));
	tagname_t name_protectedarea(odb->find_tagname("protected_area"));
	TagNameSet cond_boundary(odb->create_tagnameset({ "aboriginal_lands", "national_park" }));
	TagNameSet cond_protectclass(odb->create_tagnameset({ "1", "1a", "1b", "2", "3", "4", "5", "6", "7", "24", "97", "98", "99" }));
	for (const OSMDB::Object::const_ptr_t& pobj : m_areas) {
		tagname_t val_boundary(pobj->find_tag(key_boundary));
		tagname_t val_leisure(pobj->find_tag(key_leisure));
		tagname_t val_protectclass(pobj->find_tag(key_protectclass));
		tagname_t val_name(pobj->find_tag(key_name));
		if ((!cond_boundary.is_in_set(val_leisure) && val_leisure != name_naturereserve &&
		     (val_boundary != name_protectedarea || !cond_protectclass.is_in_set(val_protectclass)))
		    || val_name == tagname_t::invalid)
			continue;
		RenderObjLines::ptr_t ro(create_lines(boost::dynamic_pointer_cast<const OSMDB::ObjArea>(pobj)));
		if (!ro)
			continue;
		ro->copy_field_string(*odb, "name", val_name);
		ro->copy_field_string(*odb, "boundary", val_boundary);
		ro->copy_field_string(*odb, "protect_class", val_protectclass);
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_railwaystextname(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryZLayerNameIdSorter robjs;
	tagkey_t key_railway(odb->find_tagkey("railway"));
	tagkey_t key_highway(odb->find_tagkey("highway"));
	tagkey_t key_tunnel(odb->find_tagkey("tunnel"));
	tagkey_t key_name(odb->find_tagkey("name"));
	tagkey_t key_covered(odb->find_tagkey("covered"));
	tagkey_t key_highspeed(odb->find_tagkey("highspeed"));
	tagkey_t key_usage(odb->find_tagkey("usage"));
	tagkey_t key_construction(odb->find_tagkey("construction"));
	tagkey_t key_service(odb->find_tagkey("service"));
	tagkey_t key_layer(odb->find_tagkey("layer"));
	tagname_t name_yes(odb->find_tagname("yes"));
	tagname_t name_preserved(odb->find_tagname("preserved"));
	tagname_t name_rail(odb->find_tagname("rail"));
	tagname_t name_tram(odb->find_tagname("tram"));
	TagNameSet cond_railway(odb->create_tagnameset({ "rail", "subway", "narrow_gauge", "light_rail", "preserved", "funicular",
							   "monorail", "miniature", "tram", "disused", "construction" }));
	TagNameSet cond_tunnel(odb->create_tagnameset({ "yes", "building_passage" }));
	TagNameSet cond_service_ssy(odb->create_tagnameset({ "spur", "siding", "yard" }));
	for (OSMDB::objects_t::const_iterator li(m_lines.begin()), le(m_lines.end()), ri(m_roads.begin()), re(m_roads.end()); li != re; ++li) {
		if (li == le)
			li = ri;
		const OSMDB::Object::const_ptr_t& pobj(*li);
		tagname_t val_highway(pobj->find_tag(key_highway));
		tagname_t val_railway(pobj->find_tag(key_railway));
		tagname_t val_tunnel(pobj->find_tag(key_tunnel));
		tagname_t val_name(pobj->find_tag(key_name));
		if (!(cond_railway.is_in_set(val_railway) &&
		      !cond_tunnel.is_in_set(val_tunnel) &&
		      val_highway == tagname_t::invalid &&
		      val_name != tagname_t::invalid))
			continue;
		RenderObjLines::ptr_t ro(create_lines(boost::dynamic_pointer_cast<const OSMDB::ObjLine>(pobj)));
		if (!ro)
			continue;
		tagname_t val_covered(pobj->find_tag(key_covered));
		tagname_t val_service(pobj->find_tag(key_service));
		tagname_t val_highspeed(pobj->find_tag(key_highspeed));
		tagname_t val_usage(pobj->find_tag(key_usage));
		tagname_t val_construction(pobj->find_tag(key_construction));
		tagname_t val_layer(pobj->find_tag(key_layer));
		{
			const char *cp(odb->find_tagname(val_railway));
			if (cp) {
				ro->set_field("feature", cp);
				if (cond_service_ssy.is_in_set(val_service)) {
					if (val_railway == name_preserved)
						ro->set_field("feature", "INT-preserved-ssy");
					else if (val_railway == name_rail)
						ro->set_field("feature", "INT-spur-siding-yard");
					else if (val_railway == name_tram)
						ro->set_field("feature", "tram-service");
				}
			}
		}
		ro->set_field("tunnel", (val_covered == name_yes) ? "yes" : "no");
		ro->copy_field_string(*odb, "highspeed", val_highspeed);
		ro->copy_field_string(*odb, "usage", val_usage);
		ro->copy_field_string(*odb, "construction", val_construction);
		ro->copy_field_string(*odb, "name", val_name);
		robjs.add(ro, odb->find_tagname(val_layer));
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_roadsareatextname(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryWayPixelSorter robjs;
	tagkey_t key_highway(odb->find_tagkey("highway"));
	tagkey_t key_railway(odb->find_tagkey("railway"));
	tagkey_t key_location(odb->find_tagkey("location"));
	tagkey_t key_tunnel(odb->find_tagkey("tunnel"));
	tagkey_t key_covered(odb->find_tagkey("covered"));
	tagkey_t key_place(odb->find_tagkey("place"));
	tagkey_t key_leisure(odb->find_tagkey("leisure"));
	tagkey_t key_name(odb->find_tagkey("name"));
	tagname_t name_platform(odb->find_tagname("platform"));
	tagname_t name_underground(odb->find_tagname("underground"));
	tagname_t name_yes(odb->find_tagname("yes"));
	tagname_t name_square(odb->find_tagname("square"));
	TagNameSet cond_highway(odb->create_tagnameset({ "residential", "unclassified", "pedestrian", "service", "footway",
							   "cycleway", "living_street", "track", "path", "platform" }));
	TagNameSet cond_tunnel(odb->create_tagnameset({ "yes", "building_passage" }));
	TagNameSet cond_leisure(odb->create_tagnameset({ "park", "recreation_ground", "garden" }));
	for (const OSMDB::Object::const_ptr_t& pobj : m_areas) {
		tagname_t val_highway(pobj->find_tag(key_highway));
		tagname_t val_railway(pobj->find_tag(key_railway));
		tagname_t val_location(pobj->find_tag(key_location));
		tagname_t val_tunnel(pobj->find_tag(key_tunnel));
		tagname_t val_covered(pobj->find_tag(key_covered));
		tagname_t val_place(pobj->find_tag(key_place));
		tagname_t val_leisure(pobj->find_tag(key_leisure));
		tagname_t val_name(pobj->find_tag(key_name));
		if (!((cond_highway.is_in_set(val_highway) ||
		       (val_railway == name_platform && val_location == name_underground &&
			!cond_tunnel.is_in_set(val_tunnel) && val_covered != name_yes) ||
		       (val_place == name_square && !cond_leisure.is_in_set(val_leisure))) &&
		      val_name != tagname_t::invalid))
			continue;
		RenderObjPoints::ptr_t ro(create_points(boost::dynamic_pointer_cast<const OSMDB::ObjArea>(pobj)));
		if (!ro)
			continue;
		ro->copy_field_string(*odb, "highway", val_highway);
		ro->copy_field_string(*odb, "place", val_place);
		ro->copy_field_string(*odb, "leisure", val_leisure);
		ro->copy_field_string(*odb, "name", val_name);
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_roads(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryRoadsBridgesSorter robjs;
	tagkey_t key_tunnel(odb->find_tagkey("tunnel"));
	tagkey_t key_covered(odb->find_tagkey("covered"));
	tagkey_t key_bridge(odb->find_tagkey("bridge"));
	tagkey_t key_highway(odb->find_tagkey("highway"));
	tagkey_t key_railway(odb->find_tagkey("railway"));
	tagkey_t key_horse(odb->find_tagkey("horse"));
	tagkey_t key_foot(odb->find_tagkey("foot"));
	tagkey_t key_bicycle(odb->find_tagkey("bicycle"));
	tagkey_t key_tracktype(odb->find_tagkey("tracktype"));
	tagkey_t key_surface(odb->find_tagkey("surface"));
	tagkey_t key_access(odb->find_tagkey("access"));
	tagkey_t key_construction(odb->find_tagkey("construction"));
	tagkey_t key_service(odb->find_tagkey("service"));
	tagkey_t key_layer(odb->find_tagkey("layer"));
	tagkey_t key_leisure(odb->find_tagkey("leisure"));
	tagname_t name_yes(odb->find_tagname("yes"));
	tagname_t name_slipway(odb->find_tagname("slipway"));
	tagname_t name_preserved(odb->find_tagname("preserved"));
	tagname_t name_rail(odb->find_tagname("rail"));
	tagname_t name_tram(odb->find_tagname("tram"));
	TagNameSet cond_tunnel(odb->create_tagnameset({ "yes", "building_passage" }));
	TagNameSet cond_bridge(odb->create_tagnameset({ "yes", "boardwalk", "cantilever", "covered", "low_water_crossing",
							  "movable", "trestle", "viaduct" }));
	TagNameSet cond_access_dest(odb->create_tagnameset({ "destination" }));
	TagNameSet cond_access_no(odb->create_tagnameset({ "no", "private" }));
	TagNameSet cond_service_minor(odb->create_tagnameset({ "parking_aisle", "drive-through", "driveway" }));
	TagNameSet cond_road_surface_unpaved(odb->create_tagnameset({ "unpaved", "compacted", "dirt", "earth", "fine_gravel",
									"grass", "grass_paver", "gravel", "ground",
									"mud", "pebblestone", "salt", "sand",
									"woodchips", "clay", "ice", "snow" }));
	TagNameSet cond_road_surface_paved(odb->create_tagnameset({ "paved", "asphalt", "cobblestone", "cobblestone:flattened",
								      "sett", "concrete", "concrete:lanes", "concrete:plates",
								      "paving_stones", "metal", "wood", "unhewn_cobblestone" }));
	TagNameSet cond_service_ssy(odb->create_tagnameset({ "spur", "siding", "yard" }));
	// the osm2pgsql lines table also contains all roads
	for (OSMDB::objects_t::const_iterator li(m_lines.begin()), le(m_lines.end()), ri(m_roads.begin()), re(m_roads.end()); li != re; ++li) {
		if (li == le)
			li = ri;
		const OSMDB::Object::const_ptr_t& pobj(*li);
		tagname_t val_tunnel(pobj->find_tag(key_tunnel));
		tagname_t val_covered(pobj->find_tag(key_covered));
		tagname_t val_bridge(pobj->find_tag(key_bridge));
		if (cond_tunnel.is_in_set(val_tunnel) || val_covered == name_yes || cond_bridge.is_in_set(val_bridge))
			continue;
		tagname_t val_highway(pobj->find_tag(key_highway));
		if (val_highway != tagname_t::invalid) {
			// road
			RenderObjLines::ptr_t ro(create_lines(boost::dynamic_pointer_cast<const OSMDB::ObjLine>(pobj)));
			if (!ro)
				continue;
			{
				const char *cp(odb->find_tagname(val_highway));
				if (cp) {
					std::string s(cp);
					if (s.size() > 5 && !s.compare(s.size() - 5, 5, "_link")) {
						s.resize(s.size() - 5);
						ro->set_field("link", "yes");
					} else {
						ro->set_field("link", "no");
					}
					ro->set_field("feature", "highway_" + s);
				}
			}
			ro->copy_field_string(*odb, "horse", pobj->find_tag(key_horse));
			ro->copy_field_string(*odb, "foot", pobj->find_tag(key_foot));
			ro->copy_field_string(*odb, "bicycle", pobj->find_tag(key_bicycle));
			ro->copy_field_string(*odb, "tracktype", pobj->find_tag(key_tracktype));
			tagname_t val_surface(pobj->find_tag(key_surface));
			if (cond_road_surface_unpaved.is_in_set(val_surface))
				ro->set_field("int_surface", "unpaved");
			else if (cond_road_surface_paved.is_in_set(val_surface))
				ro->set_field("int_surface", "paved");
			else
				ro->set_field("int_surface");
			tagname_t val_access(pobj->find_tag(key_access));
			if (cond_access_dest.is_in_set(val_access))
				ro->set_field("access", "destination");
			else if (cond_access_no.is_in_set(val_access))
				ro->set_field("access", "no");
			else
				ro->set_field("access");
			ro->copy_field_string(*odb, "construction", pobj->find_tag(key_construction));
			tagname_t val_service(pobj->find_tag(key_service));
			tagname_t val_leisure(pobj->find_tag(key_leisure));
			ro->set_field("service", (cond_service_minor.is_in_set(val_service) ||
						  (val_leisure != tagname_t::invalid && val_leisure == name_slipway)) ?
				      "INT-minor" : "INT-normal");
			ro->copy_field_double(*odb, "layernotnull", pobj->find_tag(key_layer), 0);
			robjs.add(ro);
			continue;
		}
		tagname_t val_railway(pobj->find_tag(key_railway));
		if (val_railway != tagname_t::invalid) {
			// rail
			RenderObjLines::ptr_t ro(create_lines(boost::dynamic_pointer_cast<const OSMDB::ObjLine>(pobj)));
			if (!ro)
				continue;
			tagname_t val_service(pobj->find_tag(key_service));
			{
				const char *cp(odb->find_tagname(val_railway));
				if (cp)
					ro->set_field("feature", std::string("railway_") + cp);
			}
			if (cond_service_ssy.is_in_set(val_service)) {
				if (val_railway == name_preserved)
					ro->set_field("feature", "railway_INT-preserved-ssy");
				else if (val_railway == name_rail)
					ro->set_field("feature", "railway_INT-spur-siding-yard");
				else if (val_railway == name_tram)
					ro->set_field("feature", "railway_tram-service");
			}
			ro->copy_field_string(*odb, "horse", pobj->find_tag(key_horse));
			ro->copy_field_string(*odb, "foot", pobj->find_tag(key_foot));
			ro->copy_field_string(*odb, "bicycle", pobj->find_tag(key_bicycle));
			ro->copy_field_string(*odb, "tracktype", pobj->find_tag(key_tracktype));
			ro->set_field("int_surface");
			tagname_t val_access(pobj->find_tag(key_access));
			if (cond_access_dest.is_in_set(val_access))
				ro->set_field("access", "destination");
			else if (cond_access_no.is_in_set(val_access))
				ro->set_field("access", "no");
			else
				ro->set_field("access");
			ro->copy_field_string(*odb, "construction", pobj->find_tag(key_construction));
			tagname_t val_leisure(pobj->find_tag(key_leisure));
			ro->set_field("service", (cond_service_minor.is_in_set(val_service) ||
						  (val_leisure != tagname_t::invalid && val_leisure == name_slipway)) ?
				      "INT-minor" : "INT-normal");
			ro->set_field("link", "no");
			ro->copy_field_double(*odb, "layernotnull", pobj->find_tag(key_layer), 0);
			robjs.add(ro);
			continue;
		}
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_roadslowzoom(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryZSorter robjs;
	tagkey_t key_highway(odb->find_tagkey("highway"));
	tagkey_t key_railway(odb->find_tagkey("railway"));
	tagkey_t key_tunnel(odb->find_tagkey("tunnel"));
	tagkey_t key_covered(odb->find_tagkey("covered"));
	tagkey_t key_service(odb->find_tagkey("service"));
	tagkey_t key_surface(odb->find_tagkey("surface"));
	tagname_t name_preserved(odb->find_tagname("preserved"));
	tagname_t name_rail(odb->find_tagname("rail"));
	tagname_t name_yes(odb->find_tagname("yes"));
	TagNameSet cond_service_ssy(odb->create_tagnameset({ "spur", "siding", "yard" }));
	TagNameSet cond_railway(odb->create_tagnameset({ "rail", "tram", "light_rail", "funicular", "narrow_gauge" }));
	TagNameSet cond_tunnel(odb->create_tagnameset({ "yes", "building_passage" }));
	TagNameSet cond_surface_unpaved(odb->create_tagnameset({ "unpaved", "compacted", "dirt", "earth", "fine_gravel", "grass", "grass_paver", "gravel", "ground",
								   "mud", "pebblestone", "salt", "sand", "woodchips", "clay", "ice", "snow" }));
	TagNameSet cond_surface_paved(odb->create_tagnameset({ "paved", "asphalt", "cobblestone", "cobblestone:flattened", "sett", "concrete", "concrete:lanes",
								 "concrete:plates", "paving_stones", "metal", "wood", "unhewn_cobblestone" }));
	for (const OSMDB::Object::const_ptr_t& pobj : m_roads) {
		tagname_t val_highway(pobj->find_tag(key_highway));
		tagname_t val_railway(pobj->find_tag(key_railway));
		tagname_t val_service(pobj->find_tag(key_service));
		if (!(val_highway != tagname_t::invalid ||
		      (val_railway != tagname_t::invalid && val_railway != name_preserved &&
		       !cond_service_ssy.is_in_set(val_service))))
			continue;
		RenderObjLines::ptr_t ro(create_lines(boost::dynamic_pointer_cast<const OSMDB::ObjLine>(pobj)));
		if (!ro)
			continue;
		tagname_t val_tunnel(pobj->find_tag(key_tunnel));
		tagname_t val_covered(pobj->find_tag(key_covered));
		tagname_t val_surface(pobj->find_tag(key_surface));
		ro->set_field("link", "no");
		if (val_highway != tagname_t::invalid) {
			const char *cp(odb->find_tagname(val_highway));
			if (cp) {
				std::string s(cp);
				if (s.size() > 5 && !s.compare(s.size() - 5, 5, "_link")) {
					s.resize(s.size() - 5);
					ro->set_field("link", "yes");
				}
				ro->set_field("feature", "highway_" + s);
			}
		} else if (val_railway == name_rail && cond_service_ssy.is_in_set(val_service)) {
			ro->set_field("feature", "INT-spur-siding-yard");
		} else if (cond_railway.is_in_set(val_railway)) {
			ro->copy_field_string(*odb, "feature", val_railway);
		}
		ro->set_field("int_tunnel", (cond_tunnel.is_in_set(val_tunnel) || val_covered == name_yes) ? "yes" : "no");
		if (cond_surface_unpaved.is_in_set(val_surface))
			ro->set_field("int_surface", "unpaved");
		else if (cond_surface_paved.is_in_set(val_surface))
			ro->set_field("int_surface", "paved");
		else
			ro->set_field("int_surface");
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_roadstextname(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryZLayerNameIdSorter robjs;
	tagkey_t key_highway(odb->find_tagkey("highway"));
	tagkey_t key_tunnel(odb->find_tagkey("tunnel"));
	tagkey_t key_covered(odb->find_tagkey("covered"));
	tagkey_t key_construction(odb->find_tagkey("construction"));
	tagkey_t key_name(odb->find_tagkey("name"));
	tagkey_t key_oneway(odb->find_tagkey("oneway"));
	tagkey_t key_junction(odb->find_tagkey("junction"));
	tagkey_t key_horse(odb->find_tagkey("horse"));
	tagkey_t key_bicycle(odb->find_tagkey("bicycle"));
	tagkey_t key_layer(odb->find_tagkey("layer"));
	tagname_t name_roundabout(odb->find_tagname("roundabout"));
	tagname_t name_yes(odb->find_tagname("yes"));
	TagNameSet cond_highway(odb->create_tagnameset({ "motorway", "motorway_link", "trunk", "trunk_link", "primary", "primary_link",
							   "secondary", "secondary_link", "tertiary", "tertiary_link", "residential",
							   "unclassified", "road", "service", "pedestrian", "raceway", "living_street", "construction" }));
	TagNameSet cond_oneway(odb->create_tagnameset({ "yes", "-1" }));
	TagNameSet cond_tunnel(odb->create_tagnameset({ "yes", "building_passage" }));
	TagNameSet cond_notoneway(odb->create_tagnameset({ "no", "reversible" }));
	for (OSMDB::objects_t::const_iterator li(m_lines.begin()), le(m_lines.end()), ri(m_roads.begin()), re(m_roads.end()); li != re; ++li) {
		if (li == le)
			li = ri;
		const OSMDB::Object::const_ptr_t& pobj(*li);
		tagname_t val_highway(pobj->find_tag(key_highway));
		tagname_t val_name(pobj->find_tag(key_name));
		tagname_t val_oneway(pobj->find_tag(key_oneway));
		tagname_t val_junction(pobj->find_tag(key_junction));
		if (!(cond_highway.is_in_set(val_highway) &&
		      (val_name != tagname_t::invalid || cond_oneway.is_in_set(val_oneway) || val_junction == name_roundabout)))
			continue;
		RenderObjLines::ptr_t ro(create_lines(boost::dynamic_pointer_cast<const OSMDB::ObjLine>(pobj)));
		if (!ro)
			continue;
		tagname_t val_tunnel(pobj->find_tag(key_tunnel));
		tagname_t val_covered(pobj->find_tag(key_covered));
		tagname_t val_construction(pobj->find_tag(key_construction));
		tagname_t val_layer(pobj->find_tag(key_layer));
		tagname_t val_horse(pobj->find_tag(key_horse));
		tagname_t val_bicycle(pobj->find_tag(key_bicycle));
		ro->copy_field_string(*odb, "highway", val_highway);
		{
			const char *cp(odb->find_tagname(val_highway));
			if (cp) {
				std::string s(cp);
				if (s.size() > 5 && !s.compare(s.size() - 5, 5, "_link"))
					s.resize(s.size() - 5);
				ro->set_field("highway", s);
			}
		}
		ro->set_field("tunnel", (cond_tunnel.is_in_set(val_tunnel) || val_covered == name_yes) ? "yes" : "no");
		ro->copy_field_string(*odb, "construction", val_construction);
		ro->copy_field_string(*odb, "name", val_name);
		if (cond_oneway.is_in_set(val_oneway))
			ro->copy_field_string(*odb, "oneway", val_oneway);
		else if (val_junction == name_roundabout && !cond_notoneway.is_in_set(val_oneway))
			ro->set_field("oneway", "yes");
		else
			ro->set_field("oneway");
		ro->copy_field_string(*odb, "horse", val_horse);
		ro->copy_field_string(*odb, "bicycle", val_bicycle);
		robjs.add(ro, odb->find_tagname(val_layer));
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_roadstextref(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryRoadsTextSorter robjs;
	tagkey_t key_highway(odb->find_tagkey("highway"));
	tagkey_t key_aeroway(odb->find_tagkey("aeroway"));
	tagkey_t key_ref(odb->find_tagkey("ref"));
	TagNameSet cond_highway(odb->create_tagnameset({ "motorway", "trunk", "primary", "secondary", "tertiary" }));
	TagNameSet cond_aeroway(odb->create_tagnameset({ "runway", "taxiway" }));
	for (OSMDB::objects_t::const_iterator li(m_lines.begin()), le(m_lines.end()), ri(m_roads.begin()), re(m_roads.end()); li != re; ++li) {
		if (li == le)
			li = ri;
		const OSMDB::Object::const_ptr_t& pobj(*li);
		tagname_t val_highway(pobj->find_tag(key_highway));
		tagname_t val_aeroway(pobj->find_tag(key_aeroway));
		tagname_t val_ref(pobj->find_tag(key_ref));
		if (val_ref == tagname_t::invalid || !(cond_highway.is_in_set(val_highway) || cond_aeroway.is_in_set(val_aeroway)))
			continue;
		std::string refs;
		unsigned int width(0U), height(0U);
		{
			const char *cp(odb->find_tagname(val_ref));
			if (!cp)
				continue;
			while (*cp) {
				const char *cpb(cp);
				cp = strchr(cpb, ';');
				const char *cpe(cp++);
				if (!cpe)
					cp = cpe = cpb + strlen(cpb);
				while (cpb != cpe && std::isspace(*cpb))
					++cpb;
				while (cpe != cpb && std::isspace(cpe[-1]))
					--cpe;
				if (cpb == cpe)
					continue;
				++height;
				width = std::max(width, static_cast<unsigned int>(cpe - cpb));
				if (!refs.empty())
					refs.push_back('\n');
				refs.append(cpb, cpe - cpb);
			}
			if (height > 4 || width > 11)
				continue;
		}
		RenderObjLines::ptr_t ro(create_lines(boost::dynamic_pointer_cast<const OSMDB::ObjLine>(pobj)));
		if (!ro)
			continue;
		if (cond_highway.is_in_set(val_highway))
			ro->copy_field_string(*odb, "highway", val_highway);
		else if (cond_aeroway.is_in_set(val_aeroway))
			ro->copy_field_string(*odb, "highway", val_aeroway);
		else
			ro->set_field("highway");
		ro->set_field("refs", refs);
		ro->set_field("width", static_cast<int64_t>(width));
		ro->set_field("height", static_cast<int64_t>(height));
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_roadstextreflowzoom(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryRoadsTextSorter robjs;
	tagkey_t key_highway(odb->find_tagkey("highway"));
	tagkey_t key_ref(odb->find_tagkey("ref"));
	TagNameSet cond_highway(odb->create_tagnameset({ "motorway", "trunk", "primary", "secondary" }));
	for (const OSMDB::Object::const_ptr_t& pobj : m_roads) {
		tagname_t val_highway(pobj->find_tag(key_highway));
		tagname_t val_ref(pobj->find_tag(key_ref));
		if (val_ref == tagname_t::invalid || !cond_highway.is_in_set(val_highway))
			continue;
		std::string refs;
		unsigned int width(0U), height(0U);
		{
			const char *cp(odb->find_tagname(val_ref));
			if (!cp)
				continue;
			while (*cp) {
				const char *cpb(cp);
				cp = strchr(cpb, ';');
				const char *cpe(cp++);
				if (!cpe)
					cp = cpe = cpb + strlen(cpb);
				while (cpb != cpe && std::isspace(*cpb))
					++cpb;
				while (cpe != cpb && std::isspace(cpe[-1]))
					--cpe;
				if (cpb == cpe)
					continue;
				++height;
				width = std::max(width, static_cast<unsigned int>(cpe - cpb));
				if (!refs.empty())
					refs.push_back('\n');
				refs.append(cpb, cpe - cpb);
			}
			if (height > 4 || width > 11)
				continue;
		}
		RenderObjLines::ptr_t ro(create_lines(boost::dynamic_pointer_cast<const OSMDB::ObjLine>(pobj)));
		if (!ro)
			continue;
		ro->copy_field_string(*odb, "highway", val_highway);
		ro->set_field("refs", refs);
		ro->set_field("width", static_cast<int64_t>(width));
		ro->set_field("height", static_cast<int64_t>(height));
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_roadstextrefminor(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryRoadsTextSorter robjs;
	tagkey_t key_highway(odb->find_tagkey("highway"));
	tagkey_t key_ref(odb->find_tagkey("ref"));
	TagNameSet cond_highway(odb->create_tagnameset({ "unclassified", "residential", "track" }));
	for (OSMDB::objects_t::const_iterator li(m_lines.begin()), le(m_lines.end()), ri(m_roads.begin()), re(m_roads.end()); li != re; ++li) {
		if (li == le)
			li = ri;
		const OSMDB::Object::const_ptr_t& pobj(*li);
		tagname_t val_highway(pobj->find_tag(key_highway));
		tagname_t val_ref(pobj->find_tag(key_ref));
		if (val_ref == tagname_t::invalid || !cond_highway.is_in_set(val_highway))
			continue;
				std::string refs;
		unsigned int width(0U), height(0U);
		{
			const char *cp(odb->find_tagname(val_ref));
			if (!cp)
				continue;
			while (*cp) {
				const char *cpb(cp);
				cp = strchr(cpb, ';');
				const char *cpe(cp++);
				if (!cpe)
					cp = cpe = cpb + strlen(cpb);
				while (cpb != cpe && std::isspace(*cpb))
					++cpb;
				while (cpe != cpb && std::isspace(cpe[-1]))
					--cpe;
				if (cpb == cpe)
					continue;
				++height;
				width = std::max(width, static_cast<unsigned int>(cpe - cpb));
				if (!refs.empty())
					refs.push_back('\n');
				refs.append(cpb, cpe - cpb);
			}
			if (height > 4 || width > 11)
				continue;
		}
		RenderObjLines::ptr_t ro(create_lines(boost::dynamic_pointer_cast<const OSMDB::ObjLine>(pobj)));
		if (!ro)
			continue;
		ro->copy_field_string(*odb, "highway", val_highway);
		ro->set_field("refs", refs);
		ro->set_field("width", static_cast<int64_t>(width));
		ro->set_field("height", static_cast<int64_t>(height));
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_statenames(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	DbQueryWayPixelSorter robjs;
	tagkey_t key_boundary(odb->find_tagkey("boundary"));
	tagkey_t key_adminlevel(odb->find_tagkey("admin_level"));
	tagkey_t key_name(odb->find_tagkey("name"));
	tagkey_t key_ref(odb->find_tagkey("ref"));
	tagname_t name_administrative(odb->find_tagname("administrative"));
	tagname_t name_four(odb->find_tagname("4"));
	for (const OSMDB::Object::const_ptr_t& pobj : m_areas) {
		tagname_t val_boundary(pobj->find_tag(key_boundary));
		tagname_t val_adminlevel(pobj->find_tag(key_adminlevel));
		tagname_t val_name(pobj->find_tag(key_name));
		if (val_boundary != name_administrative ||
		    val_adminlevel != name_four ||
		    val_name == tagname_t::invalid ||
		    pobj->get_id() >= 0)
			continue;
		RenderObjPoints::ptr_t ro(create_points(boost::dynamic_pointer_cast<const OSMDB::ObjArea>(pobj)));
		if (!ro)
			continue;
		{
			const RenderObj::fieldval_t& f(ro->find_field("way_pixels"));
			if (f.is_null())
				continue;
			try {
				double waypixels(static_cast<double>(f));
				if (waypixels < 3000 || waypixels > 4000000)
					continue;
			} catch (...) {
				continue;
			}
		}
		tagname_t val_ref(pobj->find_tag(key_ref));
		ro->copy_field_string(*odb, "name", val_name);
		ro->copy_field_string(*odb, "admin_level", val_adminlevel);
		ro->copy_field_string(*odb, "ref", val_ref);
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_stations(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryLayerWayPixelSorter robjs;
	tagkey_t key_railway(odb->find_tagkey("railway"));
	tagkey_t key_aerialway(odb->find_tagkey("aerialway"));
	tagkey_t key_name(odb->find_tagkey("name"));
	tagkey_t key_ref(odb->find_tagkey("ref"));
	tagkey_t key_station(odb->find_tagkey("station"));
	tagname_t name_subwayentrance(odb->find_tagname("subway_entrance"));
	tagname_t name_station(odb->find_tagname("station"));
	TagNameSet cond_railway(odb->create_tagnameset({ "station", "halt", "tram_stop" }));
	for (const OSMDB::Object::const_ptr_t& pobj : m_areas) {
		tagname_t val_railway(pobj->find_tag(key_railway));
		tagname_t val_aerialway(pobj->find_tag(key_aerialway));
		if (!cond_railway.is_in_set(val_railway) && val_aerialway != name_station)
			continue;
		RenderObjPoints::ptr_t ro(create_points(boost::dynamic_pointer_cast<const OSMDB::ObjArea>(pobj)));
		if (!ro)
			continue;
		tagname_t val_name(pobj->find_tag(key_name));
		tagname_t val_ref(pobj->find_tag(key_ref));
		tagname_t val_station(pobj->find_tag(key_station));
		ro->copy_field_string(*odb, "name", val_name);
		ro->copy_field_string(*odb, "ref", val_ref);
		ro->copy_field_string(*odb, "railway", val_railway);
		ro->copy_field_string(*odb, "aerialway", val_aerialway);
		ro->copy_field_string(*odb, "station", val_station);
		robjs.add(ro, (val_railway == name_station) ? 1 : 2);
	}
	for (const OSMDB::Object::const_ptr_t& pobj : m_points) {
		tagname_t val_railway(pobj->find_tag(key_railway));
		tagname_t val_aerialway(pobj->find_tag(key_aerialway));
		if (!cond_railway.is_in_set(val_railway) && val_railway != name_subwayentrance && val_aerialway != name_station)
			continue;
		RenderObjPoints::ptr_t ro(create_points(boost::dynamic_pointer_cast<const OSMDB::ObjArea>(pobj)));
		if (!ro)
			continue;
		tagname_t val_name(pobj->find_tag(key_name));
		tagname_t val_ref(pobj->find_tag(key_ref));
		tagname_t val_station(pobj->find_tag(key_station));
		ro->copy_field_string(*odb, "name", val_name);
		ro->copy_field_string(*odb, "ref", val_ref);
		ro->copy_field_string(*odb, "railway", val_railway);
		ro->copy_field_string(*odb, "aerialway", val_aerialway);
		ro->copy_field_string(*odb, "station", val_station);
		robjs.add(ro, (val_railway == name_station) ? 1 : (val_railway == name_subwayentrance) ? 3 : 2);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_textline(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryNoSorter robjs;
	tagkey_t key_manmade(odb->find_tagkey("man_made"));
	tagkey_t key_waterway(odb->find_tagkey("waterway"));
	tagkey_t key_natural(odb->find_tagkey("natural"));
	tagkey_t key_name(odb->find_tagkey("name"));
	tagkey_t key_access(odb->find_tagkey("access"));
	tagkey_t key_operator(odb->find_tagkey("operator"));
	tagkey_t key_building(odb->find_tagkey("building"));
	tagkey_t key_ref(odb->find_tagkey("ref"));
	tagname_t name_no(odb->find_tagname("no"));
	TagNameSet cond_manmade(odb->create_tagnameset({ "pier", "breakwater", "groyne", "embankment" }));
	TagNameSet cond_waterway(odb->create_tagnameset({ "dam", "weir" }));
	TagNameSet cond_natural(odb->create_tagnameset({ "arete", "cliff", "ridge" }));
	for (OSMDB::objects_t::const_iterator li(m_lines.begin()), le(m_lines.end()), ri(m_roads.begin()), re(m_roads.end()); li != re; ++li) {
		if (li == le)
			li = ri;
		const OSMDB::Object::const_ptr_t& pobj(*li);
		tagname_t val_manmade(pobj->find_tag(key_manmade));
		tagname_t val_waterway(pobj->find_tag(key_waterway));
		tagname_t val_natural(pobj->find_tag(key_natural));
		tagname_t val_name(pobj->find_tag(key_name));
		if (val_name == tagname_t::invalid ||
		    (!cond_manmade.is_in_set(val_manmade) && !cond_waterway.is_in_set(val_waterway) && !cond_natural.is_in_set(val_natural)))
			continue;
		RenderObjLines::ptr_t ro(create_lines(boost::dynamic_pointer_cast<const OSMDB::ObjLine>(pobj)));
		if (!ro)
			continue;
		tagname_t val_access(pobj->find_tag(key_access));
		tagname_t val_operator(pobj->find_tag(key_operator));
		tagname_t val_building(pobj->find_tag(key_building));
		tagname_t val_ref(pobj->find_tag(key_ref));
		ro->set_field("way_pixels");
		ro->set_field("feature");
		if (val_manmade != tagname_t::invalid) {
			const char *cp(odb->find_tagname(val_manmade));
			if (cp)
				ro->set_field("feature", std::string("man_made_") + cp);
		} else if (val_waterway != tagname_t::invalid) {
			const char *cp(odb->find_tagname(val_waterway));
			if (cp)
				ro->set_field("feature", std::string("waterway_") + cp);
		} else if (val_natural != tagname_t::invalid) {
			const char *cp(odb->find_tagname(val_natural));
			if (cp)
				ro->set_field("feature", std::string("natural_") + cp);
		}
		ro->copy_field_string(*odb, "access", val_access);
		ro->copy_field_string(*odb, "name", val_name);
		ro->copy_field_string(*odb, "operator", val_operator);
		ro->copy_field_string(*odb, "ref", val_ref);
		ro->set_field("is_building", (val_building == name_no || val_building == tagname_t::invalid) ? "no" : "yes");
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_textpolylowzoom(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryWayPixelSorter robjs;
	tagkey_t key_landuse(odb->find_tagkey("landuse"));
	tagkey_t key_military(odb->find_tagkey("military"));
	tagkey_t key_natural(odb->find_tagkey("natural"));
	tagkey_t key_place(odb->find_tagkey("place"));
	tagkey_t key_boundary(odb->find_tagkey("boundary"));
	tagkey_t key_protectclass(odb->find_tagkey("protect_class"));
	tagkey_t key_leisure(odb->find_tagkey("leisure"));
	tagkey_t key_building(odb->find_tagkey("building"));
	tagkey_t key_name(odb->find_tagkey("name"));
	tagname_t name_dangerarea(odb->find_tagname("danger_area"));
	tagname_t name_island(odb->find_tagname("island"));
	tagname_t name_protectedarea(odb->find_tagname("protected_area"));
	tagname_t name_twofour(odb->find_tagname("24"));
	tagname_t name_naturereserve(odb->find_tagname("nature_reserve"));
	tagname_t name_no(odb->find_tagname("no"));
	TagNameSet cond_landuse(odb->create_tagnameset({ "forest", "military", "farmland" }));
	TagNameSet cond_natural(odb->create_tagnameset({ "wood", "glacier", "sand", "scree", "shingle", "bare_rock", "water", "bay", "strait" }));
	TagNameSet cond_boundary(odb->create_tagnameset({ "aboriginal_lands", "national_park" }));
	TagNameSet cond_protectclass(odb->create_tagnameset({ "1","1a","1b","2","3","4","5","6","7","97","24","98","99" }));
	for (const OSMDB::Object::const_ptr_t& pobj : m_areas) {
		tagname_t val_landuse(pobj->find_tag(key_landuse));
		tagname_t val_military(pobj->find_tag(key_military));
		tagname_t val_natural(pobj->find_tag(key_natural));
		tagname_t val_place(pobj->find_tag(key_place));
		tagname_t val_boundary(pobj->find_tag(key_boundary));
		tagname_t val_protectclass(pobj->find_tag(key_protectclass));
		tagname_t val_leisure(pobj->find_tag(key_leisure));
		tagname_t val_building(pobj->find_tag(key_building));
		tagname_t val_name(pobj->find_tag(key_name));
		if (!((cond_landuse.is_in_set(val_landuse) ||
		       val_military == name_dangerarea ||
		       cond_natural.is_in_set(val_natural) ||
		       val_place == name_island ||
		       cond_boundary.is_in_set(val_boundary) ||
		       (val_boundary == name_protectedarea && cond_protectclass.is_in_set(val_protectclass)) ||
		       val_leisure == name_naturereserve) &&
		      val_building == tagname_t::invalid && val_name != tagname_t::invalid))
			continue;
		RenderObjPoints::ptr_t ro(create_points(boost::dynamic_pointer_cast<const OSMDB::ObjArea>(pobj)));
		if (!ro)
			continue;
		{
			const RenderObj::fieldval_t& f(ro->find_field("way_pixels"));
			if (f.is_null())
				continue;
			try {
				if (static_cast<double>(f) < 768000)
					continue;
			} catch (...) {
				continue;
			}
		}
		ro->set_field("feature");
		if (cond_landuse.is_in_set(val_landuse)) {
			const char *cp(odb->find_tagname(val_landuse));
			if (cp)
				ro->set_field("feature", std::string("landuse_") + cp);
		} else if (val_military == name_dangerarea) {
			const char *cp(odb->find_tagname(val_military));
			if (cp)
				ro->set_field("feature", std::string("military_") + cp);
		} else if (cond_natural.is_in_set(val_natural)) {
			const char *cp(odb->find_tagname(val_natural));
			if (cp)
				ro->set_field("feature", std::string("natural_") + cp);
		} else if (val_place == name_island) {
			const char *cp(odb->find_tagname(val_place));
			if (cp)
				ro->set_field("feature", std::string("place_") + cp);
		} else if (val_boundary == name_protectedarea && val_protectclass == name_twofour) {
			ro->set_field("feature", "boundary_aboriginal_lands");
		} else if (cond_boundary.is_in_set(val_boundary) || (val_boundary == name_protectedarea && cond_protectclass.is_in_set(val_protectclass))) {
			const char *cp(odb->find_tagname(val_boundary));
			if (cp)
				ro->set_field("feature", std::string("boundary_") + cp);
		} else if (val_leisure == name_naturereserve) {
			const char *cp(odb->find_tagname(val_leisure));
			if (cp)
				ro->set_field("feature", std::string("leisure_") + cp);
		}
		ro->copy_field_string(*odb, "name", val_name);
		ro->set_field("is_building", (val_building == name_no || val_building == tagname_t::invalid) ? "no" : "yes");
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_tourismboundary(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryNoSorter robjs;
	tagkey_t key_tourism(odb->find_tagkey("tourism"));
	TagNameSet cond_tourism(odb->create_tagnameset({ "theme_park", "zoo" }));
	for (const OSMDB::Object::const_ptr_t& pobj : m_areas) {
		tagname_t val_tourism(pobj->find_tag(key_tourism));
		if (!cond_tourism.is_in_set(val_tourism))
			continue;
		RenderObjPolygons::ptr_t ro(create_polygons(boost::dynamic_pointer_cast<const OSMDB::ObjArea>(pobj)));
		if (!ro)
			continue;
                ro->copy_field_string(*odb, "tourism", val_tourism);
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_trees(void) const
{
	// return wants polygons??
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	DbQueryNoSorter robjs;
	tagkey_t key_natural(odb->find_tagkey("natural"));
	tagname_t name_tree(odb->find_tagname("tree"));
	tagname_t name_treerow(odb->find_tagname("tree_row"));
	for (const OSMDB::Object::const_ptr_t& pobj : m_points) {
		tagname_t val_natural(pobj->find_tag(key_natural));
		if (val_natural != name_tree)
			continue;
		RenderObjPoints::ptr_t ro(create_points(boost::dynamic_pointer_cast<const OSMDB::ObjPoint>(pobj)));
		if (!ro)
			continue;
		ro->copy_field_string(*odb, "natural", val_natural);
		robjs.add(ro);
	}
	for (OSMDB::objects_t::const_iterator li(m_lines.begin()), le(m_lines.end()), ri(m_roads.begin()), re(m_roads.end()); li != re; ++li) {
		if (li == le)
			li = ri;
		const OSMDB::Object::const_ptr_t& pobj(*li);
		tagname_t val_natural(pobj->find_tag(key_natural));
		if (val_natural != name_treerow)
			continue;
		RenderObjLines::ptr_t ro(create_lines(boost::dynamic_pointer_cast<const OSMDB::ObjLine>(pobj)));
		if (!ro)
			continue;
		ro->copy_field_string(*odb, "natural", val_natural);
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_tunnels(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryRoadsBridgesSorter robjs;
	tagkey_t key_highway(odb->find_tagkey("highway"));
	tagkey_t key_railway(odb->find_tagkey("railway"));
	tagkey_t key_tunnel(odb->find_tagkey("tunnel"));
	tagkey_t key_covered(odb->find_tagkey("covered"));
	tagkey_t key_horse(odb->find_tagkey("horse"));
	tagkey_t key_foot(odb->find_tagkey("foot"));
	tagkey_t key_bicycle(odb->find_tagkey("bicycle"));
	tagkey_t key_tracktype(odb->find_tagkey("tracktype"));
	tagkey_t key_surface(odb->find_tagkey("surface"));
	tagkey_t key_access(odb->find_tagkey("access"));
	tagkey_t key_construction(odb->find_tagkey("construction"));
	tagkey_t key_service(odb->find_tagkey("service"));
	tagkey_t key_layer(odb->find_tagkey("layer"));
	tagname_t name_buildingpassage(odb->find_tagname("building_passage"));
	tagname_t name_yes(odb->find_tagname("yes"));
	tagname_t name_platform(odb->find_tagname("platform"));
	tagname_t name_destination(odb->find_tagname("destination"));
	tagname_t name_preserved(odb->find_tagname("preserved"));
	tagname_t name_rail(odb->find_tagname("rail"));
	tagname_t name_tram(odb->find_tagname("tram"));
	TagNameSet cond_surface_unpaved(odb->create_tagnameset({ "unpaved", "compacted", "dirt", "earth", "fine_gravel", "grass", "grass_paver", "gravel", "ground",
								   "mud", "pebblestone", "salt", "sand", "woodchips", "clay", "ice", "snow" }));
	TagNameSet cond_surface_paved(odb->create_tagnameset({ "paved", "asphalt", "cobblestone", "cobblestone:flattened", "sett", "concrete", "concrete:lanes",
								 "concrete:plates", "paving_stones", "metal", "wood", "unhewn_cobblestone" }));
	TagNameSet cond_access(odb->create_tagnameset({ "no", "private" }));
	TagNameSet cond_service_minor(odb->create_tagnameset({ "parking_aisle", "drive-through", "driveway" }));
	TagNameSet cond_service_ssy(odb->create_tagnameset({ "spur", "siding", "yard" }));
	for (OSMDB::objects_t::const_iterator li(m_lines.begin()), le(m_lines.end()), ri(m_roads.begin()), re(m_roads.end()); li != re; ++li) {
		if (li == le)
			li = ri;
		const OSMDB::Object::const_ptr_t& pobj(*li);
		tagname_t val_highway(pobj->find_tag(key_highway));
		tagname_t val_tunnel(pobj->find_tag(key_tunnel));
		tagname_t val_covered(pobj->find_tag(key_covered));
		if (val_highway == tagname_t::invalid ||
		    !(val_tunnel == name_yes || val_tunnel == name_buildingpassage || val_covered == name_yes))
			continue;
		RenderObjLines::ptr_t ro(create_lines(boost::dynamic_pointer_cast<const OSMDB::ObjLine>(pobj)));
		if (!ro)
			continue;
		tagname_t val_horse(pobj->find_tag(key_horse));
		tagname_t val_foot(pobj->find_tag(key_foot));
		tagname_t val_bicycle(pobj->find_tag(key_bicycle));
		tagname_t val_tracktype(pobj->find_tag(key_tracktype));
		tagname_t val_surface(pobj->find_tag(key_surface));
		tagname_t val_access(pobj->find_tag(key_access));
		tagname_t val_construction(pobj->find_tag(key_construction));
		tagname_t val_service(pobj->find_tag(key_service));
		tagname_t val_layer(pobj->find_tag(key_layer));
		{
			const char *cp(odb->find_tagname(val_highway));
			if (cp) {
				std::string s(cp);
				if (s.size() > 5 && !s.compare(s.size() - 5, 5, "_link")) {
					s.resize(s.size() - 5);
					ro->set_field("link", "yes");
				} else {
					ro->set_field("link", "no");
				}
				ro->set_field("feature", "highway_" + s);
			}
		}
		ro->copy_field_string(*odb, "horse", val_horse);
		ro->copy_field_string(*odb, "foot", val_foot);
		ro->copy_field_string(*odb, "bicycle", val_bicycle);
		ro->copy_field_string(*odb, "tracktype", val_tracktype);
		if (cond_surface_unpaved.is_in_set(val_surface))
			ro->set_field("int_surface", "unpaved");
		else if (cond_surface_paved.is_in_set(val_surface))
			ro->set_field("int_surface", "paved");
		else
			ro->set_field("int_surface");
		if (val_access == name_destination)
			ro->set_field("access", "destination");
		else if (cond_access.is_in_set(val_access))
			ro->set_field("access", "no");
		else
			ro->set_field("access");
		ro->copy_field_string(*odb, "construction", val_construction);
		ro->set_field("service", cond_service_minor.is_in_set(val_service) ? "INT-minor" : "INT-normal");
		ro->copy_field_int(*odb, "layernotnull", val_layer, 0);
		robjs.add(ro);
	}	
	for (OSMDB::objects_t::const_iterator li(m_lines.begin()), le(m_lines.end()), ri(m_roads.begin()), re(m_roads.end()); li != re; ++li) {
		if (li == le)
			li = ri;
		const OSMDB::Object::const_ptr_t& pobj(*li);
		tagname_t val_railway(pobj->find_tag(key_railway));
		tagname_t val_tunnel(pobj->find_tag(key_tunnel));
		tagname_t val_covered(pobj->find_tag(key_covered));
		if (val_railway == tagname_t::invalid || val_railway == name_platform ||
		    !(val_tunnel == name_yes || val_tunnel == name_buildingpassage || val_covered == name_yes))
			continue;
		RenderObjLines::ptr_t ro(create_lines(boost::dynamic_pointer_cast<const OSMDB::ObjLine>(pobj)));
		if (!ro)
			continue;		
		tagname_t val_horse(pobj->find_tag(key_horse));
		tagname_t val_foot(pobj->find_tag(key_foot));
		tagname_t val_bicycle(pobj->find_tag(key_bicycle));
		tagname_t val_tracktype(pobj->find_tag(key_tracktype));
		tagname_t val_access(pobj->find_tag(key_access));
		tagname_t val_construction(pobj->find_tag(key_construction));
		tagname_t val_service(pobj->find_tag(key_service));
		tagname_t val_layer(pobj->find_tag(key_layer));
		{
			const char *cp(odb->find_tagname(val_railway));
			if (cp) {
				ro->set_field("feature", std::string("railway_") + cp);
				if (cond_service_ssy.is_in_set(val_service)) {
					if (val_railway == name_preserved)
						ro->set_field("feature", "INT-preserved-ssy");
					else if (val_railway == name_rail)
						ro->set_field("feature", "INT-spur-siding-yard");
					else if (val_railway == name_tram)
						ro->set_field("feature", "tram-service");
				}
			}
		}
		ro->copy_field_string(*odb, "horse", val_horse);
		ro->copy_field_string(*odb, "foot", val_foot);
		ro->copy_field_string(*odb, "bicycle", val_bicycle);
		ro->copy_field_string(*odb, "tracktype", val_tracktype);
		ro->set_field("int_surface");
		if (val_access == name_destination)
			ro->set_field("access", "destination");
		else if (cond_access.is_in_set(val_access))
			ro->set_field("access", "no");
		else
			ro->set_field("access");
		ro->copy_field_string(*odb, "construction", val_construction);
		ro->set_field("service", cond_service_minor.is_in_set(val_service) ? "INT-minor" : "INT-normal");
		ro->set_field("link", "no");
		ro->copy_field_int(*odb, "layernotnull", val_layer, 0);
		robjs.add(ro);
	}	
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_turningcircle(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	static constexpr bool exact = false;
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryTurningCircleSorter robjs;
	tagkey_t key_highway(odb->find_tagkey("highway"));
	tagkey_t key_service(odb->find_tagkey("service"));
	tagname_t name_tertiary(odb->find_tagname("tertiary"));
	tagname_t name_unclassified(odb->find_tagname("unclassified"));
	tagname_t name_residential(odb->find_tagname("residential"));
	tagname_t name_livingstreet(odb->find_tagname("living_street"));
	tagname_t name_service(odb->find_tagname("service"));
	tagname_t name_track(odb->find_tagname("track"));
	TagNameSet cond_highway(odb->create_tagnameset({ "turning_circle", "turning_loop" }));
	TagNameSet cond_service_minor(odb->create_tagnameset({ "parking_aisle", "drive-through", "driveway" }));
	for (const OSMDB::Object::const_ptr_t& pobj : m_points) {
		tagname_t val_highway(pobj->find_tag(key_highway));
		if (!cond_highway.is_in_set(val_highway))
			continue;
		OSMDB::ObjPoint::const_ptr_t pobjp(boost::dynamic_pointer_cast<const OSMDB::ObjPoint>(pobj));
		if (!pobjp)
			continue;
		for (OSMDB::objects_t::const_iterator li(m_lines.begin()), le(m_lines.end()), ri(m_roads.begin()), re(m_roads.end()); li != re; ++li) {
			if (li == le)
				li = ri;
			OSMDB::ObjLine::const_ptr_t pobjl(boost::dynamic_pointer_cast<const OSMDB::ObjLine>(*li));
			if (!pobjl)
				continue;
			tagname_t val_highway(pobjl->find_tag(key_highway));
			unsigned int prio(0);
			if (val_highway == name_tertiary)
				prio = 1;
			else if (val_highway == name_unclassified)
				prio = 2;
			else if (val_highway == name_residential)
				prio = 3;
			else if (val_highway == name_livingstreet)
				prio = 4;
			else if (val_highway == name_service)
				prio = 5;
			else if (val_highway == name_track)
				prio = 6;
			else
				continue;
			if (exact) {
				bool match(false);
				const Point& ptp(pobjp->get_location());
				for (const auto& pt : pobjl->get_line()) {
					if (pt != ptp)
						continue;
					match = true;
					break;
				}
				if (!match)
					continue;
			} else {
				Point pt;
				float dist;
				pobjl->get_line().nearest(pt, dist, pobjp->get_location());
				if (dist > (0.1 * 1e-3 * Point::km_to_nmi))
					continue;
			}
			RenderObjPoints::ptr_t ro(create_points(pobjp));
			if (!ro)
				continue;
			tagname_t val_service(pobjl->find_tag(key_service));
			ro->copy_field_string(*odb, "int_tc_type", val_highway);
			ro->set_field("service", cond_service_minor.is_in_set(val_service) ? "INT-minor" : "INT-normal");
			robjs.add(ro, pobjp->get_location(), prio);
		}
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_waterareas(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryLayerSorter robjs;
	tagkey_t key_waterway(odb->find_tagkey("waterway"));
	tagkey_t key_landuse(odb->find_tagkey("landuse"));
	tagkey_t key_natural(odb->find_tagkey("natural"));
	tagkey_t key_building(odb->find_tagkey("building"));
	tagkey_t key_intermittent(odb->find_tagkey("intermittent"));
	tagkey_t key_seasonal(odb->find_tagkey("seasonal"));
	tagkey_t key_basin(odb->find_tagkey("basin"));
	tagkey_t key_layer(odb->find_tagkey("layer"));
	tagname_t name_yes(odb->find_tagname("yes"));
	TagNameSet cond_waterway(odb->create_tagnameset({ "dock", "riverbank" }));
	TagNameSet cond_landuse(odb->create_tagnameset({ "reservoir", "basin" }));
	TagNameSet cond_natural(odb->create_tagnameset({ "water", "glacier" }));
	TagNameSet cond_seasonal(odb->create_tagnameset({ "yes", "spring", "summer", "autumn", "winter", "wet_season", "dry_season" }));
	TagNameSet cond_basin(odb->create_tagnameset({ "detention", "infiltration" }));
	for (const OSMDB::Object::const_ptr_t& pobj : m_areas) {
		tagname_t val_waterway(pobj->find_tag(key_waterway));
		tagname_t val_landuse(pobj->find_tag(key_landuse));
		tagname_t val_natural(pobj->find_tag(key_natural));
		tagname_t val_building(pobj->find_tag(key_building));
		if ((!cond_waterway.is_in_set(val_waterway) && !cond_landuse.is_in_set(val_landuse) && !cond_natural.is_in_set(val_natural)) ||
		    val_building != tagname_t::invalid)
			continue;
		RenderObjPolygons::ptr_t ro(create_polygons(boost::dynamic_pointer_cast<const OSMDB::ObjArea>(pobj)));
		if (!ro)
			continue;
		tagname_t val_intermittent(pobj->find_tag(key_intermittent));
		tagname_t val_seasonal(pobj->find_tag(key_seasonal));
		tagname_t val_basin(pobj->find_tag(key_basin));
		tagname_t val_layer(pobj->find_tag(key_layer));
		ro->copy_field_string(*odb, "natural", val_natural);
		ro->copy_field_string(*odb, "waterway", val_waterway);
		ro->copy_field_string(*odb, "landuse", val_landuse);
		ro->set_field("int_intermittent", (val_intermittent == name_yes ||
						   cond_seasonal.is_in_set(val_seasonal) ||
						   cond_basin.is_in_set(val_basin)) ? "yes" : "no");
		robjs.add(ro, odb->find_tagname(val_layer));
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_waterbarriersline(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryNoSorter robjs;
	tagkey_t key_waterway(odb->find_tagkey("waterway"));
	TagNameSet cond_waterway(odb->create_tagnameset({ "dam", "weir", "lock_gate" }));
	for (OSMDB::objects_t::const_iterator li(m_lines.begin()), le(m_lines.end()), ri(m_roads.begin()), re(m_roads.end()); li != re; ++li) {
		if (li == le)
			li = ri;
		const OSMDB::Object::const_ptr_t& pobj(*li);
		tagname_t val_waterway(pobj->find_tag(key_waterway));
		if (!cond_waterway.is_in_set(val_waterway))
			continue;
		RenderObjLines::ptr_t ro(create_lines(boost::dynamic_pointer_cast<const OSMDB::ObjLine>(pobj)));
		if (!ro)
			continue;
		ro->copy_field_string(*odb, "waterway", val_waterway);
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_waterbarrierspoint(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryNoSorter robjs;
	tagkey_t key_waterway(odb->find_tagkey("waterway"));
	TagNameSet cond_waterway(odb->create_tagnameset({ "dam", "weir", "lock_gate" }));
	for (const OSMDB::Object::const_ptr_t& pobj : m_points) {
		tagname_t val_waterway(pobj->find_tag(key_waterway));
		if (!cond_waterway.is_in_set(val_waterway))
			continue;
		RenderObjPoints::ptr_t ro(create_points(boost::dynamic_pointer_cast<const OSMDB::ObjPoint>(pobj)));
		if (!ro)
			continue;
		ro->copy_field_string(*odb, "waterway", val_waterway);
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_waterbarrierspoly(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryNoSorter robjs;
	tagkey_t key_waterway(odb->find_tagkey("waterway"));
	TagNameSet cond_waterway(odb->create_tagnameset({ "dam", "weir", "lock_gate" }));
	for (const OSMDB::Object::const_ptr_t& pobj : m_areas) {
		tagname_t val_waterway(pobj->find_tag(key_waterway));
		if (!cond_waterway.is_in_set(val_waterway))
			continue;
		RenderObjPolygons::ptr_t ro(create_polygons(boost::dynamic_pointer_cast<const OSMDB::ObjArea>(pobj)));
		if (!ro)
			continue;
		ro->copy_field_string(*odb, "waterway", val_waterway);
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_waterlines(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryLayerSorter robjs;
	tagkey_t key_waterway(odb->find_tagkey("waterway"));
	tagkey_t key_bridge(odb->find_tagkey("bridge"));
	tagkey_t key_intermittent(odb->find_tagkey("intermittent"));
	tagkey_t key_seasonal(odb->find_tagkey("seasonal"));
	tagkey_t key_tunnel(odb->find_tagkey("tunnel"));
	tagkey_t key_layer(odb->find_tagkey("layer"));
	tagname_t name_yes(odb->find_tagname("yes"));
	TagNameSet cond_waterway(odb->create_tagnameset({ "river", "canal", "stream", "drain", "ditch" }));
	TagNameSet cond_bridge(odb->create_tagnameset({ "yes", "aqueduct" }));
	TagNameSet cond_seasonal(odb->create_tagnameset({ "yes", "spring", "summer", "autumn", "winter", "wet_season", "dry_season" }));
	TagNameSet cond_tunnel(odb->create_tagnameset({ "yes", "culvert" }));
	for (OSMDB::objects_t::const_iterator li(m_lines.begin()), le(m_lines.end()), ri(m_roads.begin()), re(m_roads.end()); li != re; ++li) {
		if (li == le)
			li = ri;
		const OSMDB::Object::const_ptr_t& pobj(*li);
		tagname_t val_waterway(pobj->find_tag(key_waterway));
		tagname_t val_bridge(pobj->find_tag(key_bridge));
		if (!cond_waterway.is_in_set(val_waterway) || !(val_bridge == tagname_t::invalid || !cond_bridge.is_in_set(val_bridge)))
			continue;
		RenderObjLines::ptr_t ro(create_lines(boost::dynamic_pointer_cast<const OSMDB::ObjLine>(pobj)));
		if (!ro)
			continue;
		tagname_t val_intermittent(pobj->find_tag(key_intermittent));
		tagname_t val_seasonal(pobj->find_tag(key_seasonal));
		tagname_t val_tunnel(pobj->find_tag(key_tunnel));
		tagname_t val_layer(pobj->find_tag(key_layer));
		ro->copy_field_string(*odb, "waterway", val_waterway);
		ro->set_field("int_intermittent", (val_intermittent == name_yes ||
						   cond_seasonal.is_in_set(val_seasonal)) ? "yes" : "no");
		ro->set_field("int_tunnel", cond_tunnel.is_in_set(val_tunnel) ? "yes" : "no");
		ro->set_field("bridge", "no");
		robjs.add(ro, odb->find_tagname(val_layer));
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_waterlinescasing(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryNoSorter robjs;
	tagkey_t key_waterway(odb->find_tagkey("waterway"));
	tagkey_t key_intermittent(odb->find_tagkey("intermittent"));
	tagkey_t key_seasonal(odb->find_tagkey("seasonal"));
	tagkey_t key_tunnel(odb->find_tagkey("tunnel"));
	tagname_t name_yes(odb->find_tagname("yes"));
	TagNameSet cond_waterway(odb->create_tagnameset({ "stream", "drain", "ditch" }));
	TagNameSet cond_seasonal(odb->create_tagnameset({ "yes", "spring", "summer", "autumn", "winter", "wet_season", "dry_season" }));
	TagNameSet cond_tunnel(odb->create_tagnameset({ "yes", "culvert" }));
	for (OSMDB::objects_t::const_iterator li(m_lines.begin()), le(m_lines.end()), ri(m_roads.begin()), re(m_roads.end()); li != re; ++li) {
		if (li == le)
			li = ri;
		const OSMDB::Object::const_ptr_t& pobj(*li);
		tagname_t val_waterway(pobj->find_tag(key_waterway));
		if (!cond_waterway.is_in_set(val_waterway))
			continue;
		OSMDB::ObjLine::const_ptr_t pl(boost::dynamic_pointer_cast<const OSMDB::ObjLine>(pobj));
		if (!pl)
			continue;
		tagname_t val_intermittent(pobj->find_tag(key_intermittent));
		tagname_t val_seasonal(pobj->find_tag(key_seasonal));
		tagname_t val_tunnel(pobj->find_tag(key_tunnel));
		RenderObjLines::ptr_t ro(create_lines(pl->get_line(), pl->get_id(), pl->get_zorder()));
		if (!ro)
			continue;
		ro->copy_field_string(*odb, "waterway", val_waterway);
		ro->set_field("int_intermittent", (val_intermittent == name_yes ||
						   cond_seasonal.is_in_set(val_seasonal)) ? "yes" : "no");
		ro->set_field("int_tunnel", cond_tunnel.is_in_set(val_tunnel) ? "yes" : "no");
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_waterlineslowzoom(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryNoSorter robjs;
	tagkey_t key_waterway(odb->find_tagkey("waterway"));
	tagkey_t key_intermittent(odb->find_tagkey("intermittent"));
	tagkey_t key_seasonal(odb->find_tagkey("seasonal"));
	tagname_t name_river(odb->find_tagname("river"));
	tagname_t name_yes(odb->find_tagname("yes"));
	TagNameSet cond_seasonal(odb->create_tagnameset({ "yes", "spring", "summer", "autumn", "winter", "wet_season", "dry_season" }));
	for (OSMDB::objects_t::const_iterator li(m_lines.begin()), le(m_lines.end()), ri(m_roads.begin()), re(m_roads.end()); li != re; ++li) {
		if (li == le)
			li = ri;
		const OSMDB::Object::const_ptr_t& pobj(*li);
		tagname_t val_waterway(pobj->find_tag(key_waterway));
		if (val_waterway != name_river)
			continue;
		OSMDB::ObjLine::const_ptr_t pl(boost::dynamic_pointer_cast<const OSMDB::ObjLine>(pobj));
		if (!pl)
			continue;
		tagname_t val_intermittent(pobj->find_tag(key_intermittent));
		tagname_t val_seasonal(pobj->find_tag(key_seasonal));
		RenderObjLines::ptr_t ro(create_lines(pl->get_line(), pl->get_id(), pl->get_zorder()));
		if (!ro)
			continue;
		ro->copy_field_string(*odb, "waterway", val_waterway);
		ro->set_field("int_intermittent", (val_intermittent == name_yes ||
						   cond_seasonal.is_in_set(val_seasonal)) ? "yes" : "no");
		robjs.add(ro);
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_waterlinestext(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryLayerSorter robjs;
	tagkey_t key_waterway(odb->find_tagkey("waterway"));
	tagkey_t key_natural(odb->find_tagkey("natural"));
	tagkey_t key_tunnel(odb->find_tagkey("tunnel"));
	tagkey_t key_name(odb->find_tagkey("name"));
	tagkey_t key_lock(odb->find_tagkey("lock"));
	tagkey_t key_intermittent(odb->find_tagkey("intermittent"));
	tagkey_t key_seasonal(odb->find_tagkey("seasonal"));
	tagkey_t key_layer(odb->find_tagkey("layer"));
	tagname_t name_culvert(odb->find_tagname("culvert"));
	tagname_t name_yes(odb->find_tagname("yes"));
	TagNameSet cond_waterway(odb->create_tagnameset({ "river", "canal", "stream", "drain", "ditch" }));
	TagNameSet cond_natural(odb->create_tagnameset({ "bay", "strait" }));
	TagNameSet cond_tunnel(odb->create_tagnameset({ "yes", "culvert" }));
	TagNameSet cond_seasonal(odb->create_tagnameset({ "yes", "spring", "summer", "autumn", "winter", "wet_season", "dry_season" }));
	for (OSMDB::objects_t::const_iterator li(m_lines.begin()), le(m_lines.end()), ri(m_roads.begin()), re(m_roads.end()); li != re; ++li) {
		if (li == le)
			li = ri;
		const OSMDB::Object::const_ptr_t& pobj(*li);
		tagname_t val_waterway(pobj->find_tag(key_waterway));
		tagname_t val_natural(pobj->find_tag(key_natural));
		tagname_t val_tunnel(pobj->find_tag(key_tunnel));
		tagname_t val_name(pobj->find_tag(key_name));
		if ((!cond_waterway.is_in_set(val_waterway) && !cond_natural.is_in_set(val_natural)) ||
		    val_tunnel == name_culvert || val_name == tagname_t::invalid)
			continue;
		RenderObjLines::ptr_t ro(create_lines(boost::dynamic_pointer_cast<const OSMDB::ObjLine>(pobj)));
		if (!ro)
			continue;
		tagname_t val_lock(pobj->find_tag(key_lock));
		tagname_t val_intermittent(pobj->find_tag(key_intermittent));
		tagname_t val_seasonal(pobj->find_tag(key_seasonal));
		tagname_t val_layer(pobj->find_tag(key_layer));
		ro->copy_field_string(*odb, "waterway", val_waterway);
		ro->copy_field_string(*odb, "lock", val_lock);
		ro->copy_field_string(*odb, "natural", val_natural);
		ro->copy_field_string(*odb, "name", val_name);
		ro->set_field("int_intermittent", (val_intermittent == name_yes ||
						   cond_seasonal.is_in_set(val_seasonal)) ? "yes" : "no");
		ro->set_field("int_tunnel", cond_tunnel.is_in_set(val_tunnel) ? "yes" : "no");
		robjs.add(ro, odb->find_tagname(val_layer));
	}
	return robjs.result();
}

CartoRender::renderobjs_t CartoRender::dbquery_waterwaybridges(void) const
{
	const OSMDB *odb(get_osmdb());
	if (!odb)
		return renderobjs_t();
	typedef OSMDB::Object::tagkey_t tagkey_t;
	typedef OSMDB::Object::tagname_t tagname_t;
	typedef OSMDB::TagNameSet TagNameSet;
	DbQueryLayerSorter robjs;
	tagkey_t key_waterway(odb->find_tagkey("waterway"));
	tagkey_t key_bridge(odb->find_tagkey("bridge"));
	tagkey_t key_intermittent(odb->find_tagkey("intermittent"));
	tagkey_t key_seasonal(odb->find_tagkey("seasonal"));
	tagkey_t key_tunnel(odb->find_tagkey("tunnel"));
	tagkey_t key_layer(odb->find_tagkey("layer"));
	tagname_t name_yes(odb->find_tagname("yes"));
	TagNameSet cond_waterway(odb->create_tagnameset({ "river", "canal", "stream", "drain", "ditch" }));
	TagNameSet cond_bridge(odb->create_tagnameset({ "yes", "aqueduct" }));
	TagNameSet cond_seasonal(odb->create_tagnameset({ "yes", "spring", "summer", "autumn", "winter", "wet_season", "dry_season" }));
	TagNameSet cond_tunnel(odb->create_tagnameset({ "yes", "culvert" }));
	for (OSMDB::objects_t::const_iterator li(m_lines.begin()), le(m_lines.end()), ri(m_roads.begin()), re(m_roads.end()); li != re; ++li) {
		if (li == le)
			li = ri;
		const OSMDB::Object::const_ptr_t& pobj(*li);
		tagname_t val_waterway(pobj->find_tag(key_waterway));
		tagname_t val_bridge(pobj->find_tag(key_bridge));
		if (!cond_waterway.is_in_set(val_waterway) || !cond_bridge.is_in_set(val_bridge))
			continue;
		OSMDB::ObjLine::const_ptr_t pl(boost::dynamic_pointer_cast<const OSMDB::ObjLine>(pobj));
		if (!pl)
			continue;
		tagname_t val_intermittent(pobj->find_tag(key_intermittent));
		tagname_t val_seasonal(pobj->find_tag(key_seasonal));
		tagname_t val_tunnel(pobj->find_tag(key_tunnel));
		tagname_t val_layer(pobj->find_tag(key_layer));
		RenderObjLines::ptr_t ro(create_lines(pl->get_line(), pl->get_id(), pl->get_zorder()));
		if (!ro)
			continue;
		ro->copy_field_string(*odb, "waterway", val_waterway);
		ro->set_field("int_intermittent", (val_intermittent == name_yes ||
						   cond_seasonal.is_in_set(val_seasonal)) ? "yes" : "no");
		ro->set_field("int_tunnel", cond_tunnel.is_in_set(val_tunnel) ? "yes" : "no");
		ro->set_field("bridge", "yes");
		robjs.add(ro, odb->find_tagname(val_layer));
	}
	return robjs.result();
}
