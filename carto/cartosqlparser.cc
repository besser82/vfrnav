//
// C++ Implementation: cartosqlparser
//
// Description: CartoCSS SQL parser
//
//
// Author: Thomas Sailer <t.sailer@alumni.ethz.ch>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cartosqlparser.hh"

#undef BOOST_SPIRIT_X3_DEBUG
#include <boost/fusion/include/vector.hpp>
#include <boost/fusion/include/make_deque.hpp>
#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/fusion/include/io.hpp>
#include <boost/fusion/include/for_each.hpp>
#include <boost/fusion/include/pop_front.hpp>
#include <boost/fusion/adapted.hpp>

#include <boost/variant.hpp>

#include <boost/algorithm/string/predicate.hpp>

namespace CartoSQLParser {

	namespace ascii = boost::spirit::x3::ascii;
	namespace fusion = boost::fusion;

	namespace parser {
		using x3::get;
		using x3::no_case;
		using x3::no_skip;
		using x3::lit;
		using x3::string;
		using x3::omit;
		using x3::lexeme;
		using x3::repeat;
		using x3::eps;
		using x3::eol;
		using x3::eoi;
		using x3::attr;
		using x3::with;
		using x3::_val;
		using x3::_attr;
		using x3::_where;
		using x3::_pass;
		using x3::ulong_;
		using x3::long_;
		using x3::double_;
		using ascii::char_;
		using ascii::alnum;
		using ascii::alpha;
		using ascii::digit;
		using ascii::xdigit;
		using ascii::space;
		using fusion::at_c;

		//-----------------------------------------------------------------------------
		// Operators
		//-----------------------------------------------------------------------------

		const struct functable_ : x3::symbols<CartoSQL::ExprTableFunc::func_t> {
			functable_() {
				add
					("unnest",                      CartoSQL::ExprTableFunc::func_t::unnest)
					("generate_series",             CartoSQL::ExprTableFunc::func_t::generate_series);
			}
		} functable;

		//------------------------------------------------------------------------------
		// Select Statement
		//------------------------------------------------------------------------------

		struct action_notsupported {
			const char *m_msg;
			action_notsupported(const char *msg) : m_msg(msg) {}
			template<typename Context>
			void operator()(Context& ctx) {
				boost::throw_exception(x3::expectation_failure<iterator_type>(_where(ctx).begin(), m_msg));
			}
		};

		template<ExprSelect::distinct_t d>
		struct action_select_distinct_tag {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx).first = d;
			}
		};

		struct action_select_distinct_expr {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx).second = _attr(ctx);
			}
		};

		struct selectcoresel {
			Expr::exprlist_t m_distinctexpr;
			ExprSelectBase::distinct_t m_distinct;
			ExprTable::Columns m_columns;
			ExprTable::const_ptr_t m_from;
			Expr::const_ptr_t m_where;
			Expr::exprlist_t m_group;
			Expr::const_ptr_t m_having;
			selectcoresel(void) : m_distinct(ExprSelectBase::distinct_t::all) {}
		};

		struct action_select_core_select_distinct {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx).m_distinct = _attr(ctx).first;
				_val(ctx).m_distinctexpr = _attr(ctx).second;
			}
		};

		struct action_select_core_select_columns {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx).m_columns = _attr(ctx);
			}
		};

		struct action_select_core_select_from {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx).m_from = _attr(ctx);
			}
		};

		struct action_select_core_select_where {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx).m_where = _attr(ctx);
			}
		};

		struct action_select_core_select_group {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx).m_group = _attr(ctx);
			}
		};

		struct action_select_core_select_having {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx).m_having = _attr(ctx);
			}
		};

		struct action_select_core_select_window {
			template<typename Context>
			void operator()(Context& ctx) {
				boost::throw_exception(x3::expectation_failure<decltype(_where(ctx).begin())>(_where(ctx).begin(), "window not supported"));
			}
		};

		struct action_select_core_select {
			template<typename Context>
			void operator()(Context& ctx) {
				auto& g(get<parser_tag>(ctx).get());
				typedef x3::expectation_failure<decltype(_where(ctx).begin())> ex_t;
				boost::intrusive_ptr<ExprSelect> p(new ExprSelect());
				p->set_fromexpr(_attr(ctx).m_from);
				p->set_whereexpr(_attr(ctx).m_where);
				p->set_columns(_attr(ctx).m_columns);
				p->set_distinct(_attr(ctx).m_distinct);
		        	p->set_distinctexpr(_attr(ctx).m_distinctexpr);
		        	p->set_groupexpr(_attr(ctx).m_group);
				p->set_havingexpr(_attr(ctx).m_having);
				try {
					p->compute_columns();
					if (false) {
						for (const ExprTable::Column& c : p->get_columns()) {
							std::cerr << "Column: table \"" << c.get_table() << "\" name \"" << c.get_name() << "\" wildcard "
								  << (c.is_wildcard() ? "yes" : "no") << " expr " << (c.get_expr() ? "yes" : "no") << " aggregate "
								  << ((c.get_expr() && c.get_expr()->is_aggregate()) ? "yes" : "no") << std::endl;
							g.m_sql.print_expr(std::cerr << "  EXPR: ", c.get_expr(), 2) << std::endl;
						}
					}
				} catch (const std::exception& e) {
					boost::throw_exception(ex_t(_where(ctx).begin(), e.what()));
				}
				if (false)
					g.m_sql.print_expr(std::cerr << "action_select_core_select: expr before resolution:", p, 2) << std::endl;
				{
					ExprTable::ResolveColRefSubtable resolve(p->get_subtables());
					p->visitor_call_visitend(resolve);
					ExprTable::const_ptr_t p1(p->simplify(&g.m_sql, resolve));
					boost::intrusive_ptr<ExprSelect> p2(boost::dynamic_pointer_cast<ExprSelect>(boost::const_pointer_cast<ExprTable>(p1)));
					if (p1 && !p2)
						boost::throw_exception(x3::expectation_failure<decltype(_where(ctx).begin())>(_where(ctx).begin(), "sort term optimize error"));
					if (p2)
						p = p2;
				}
				if (false)
					g.m_sql.print_expr(std::cerr << "action_select_core_select: expr:", p, 2) << std::endl;
				if (false && p->get_fromexpr()) {
					std::cerr << "action_select_core_select: from: " << p->get_fromexpr()->get_columns().size() << std::endl;
					for (const ExprTable::Column& c : p->get_fromexpr()->get_columns())
						std::cerr << "  column: " << c.to_str() << " type " << Value::get_sqltypename(c.get_type()) << std::endl;
				}
				_val(ctx) = p;
			}
		};

		struct action_result_column_tblwildcard {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx).set_name(_attr(ctx) + ".*");
			}
		};

		struct action_result_column_wildcard {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx).set_name("*");
			}
		};

		struct action_result_column_expr {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx).set_expr(_attr(ctx));
				_val(ctx).set_name("");
			}
		};

		struct action_result_column_ident {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx).set_name(_attr(ctx));
			}
		};

		struct action_result_columns {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx).push_back(_attr(ctx));
			}
		};

		struct action_select_core_values_init {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx) = boost::intrusive_ptr<ExprLiteralTable>(new ExprLiteralTable());
			}
		};

		struct action_select_core_values_addexpr {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx)->get_data().push_back(_attr(ctx));
			}
		};

		struct action_select_core_values_finalize {
			template<typename Context>
			void operator()(Context& ctx) {
				try {
					_val(ctx)->compute_columns();
				} catch (const std::exception& e) {
					boost::throw_exception(x3::expectation_failure<decltype(_where(ctx).begin())>(_where(ctx).begin(), e.what()));
				}
			}
		};

		struct joinop {
			ExprJoin::jointype_t m_jointype;
			bool m_natural;
			bool m_cross;

			joinop(void) : m_jointype(ExprJoin::jointype_t::inner), m_natural(false), m_cross(false) {}
		};

		struct action_joinop_settype {
			ExprJoin::jointype_t m_type;
			action_joinop_settype(ExprJoin::jointype_t typ) : m_type(typ) {}
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx).m_jointype = m_type;
			}
		};

		struct action_joinop_cross {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx).m_jointype = ExprJoin::jointype_t::inner;
				_val(ctx).m_cross = true;
			}
		};

		struct action_joinop_natural {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx).m_natural = true;
			}
		};

		struct joinconstr {
			Expr::const_ptr_t m_expr;
			std::vector<std::string> m_columns;
		};

		struct action_join_constraint_expr {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx).m_expr = _attr(ctx);
			}
		};

		struct action_join_constraint_cols {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx).m_columns = _attr(ctx);
			}
		};

		struct compoundop {
			ExprCompound::operator_t m_operator;
			ExprCompound::distinct_t m_distinct;

			compoundop(void) : m_operator(ExprCompound::operator_t::union_), m_distinct(ExprCompound::distinct_t::distinct) {}
		};

		struct action_compoundop_setop {
			ExprCompound::operator_t m_op;
			action_compoundop_setop(ExprCompound::operator_t op) : m_op(op) {}
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx).m_operator = m_op;
			}
		};

		struct action_compoundop_setdist {
			ExprCompound::distinct_t m_dist;
			action_compoundop_setdist(ExprCompound::distinct_t dist) : m_dist(dist) {}
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx).m_distinct = m_dist;
			}
		};

		struct action_common_table_expression_begin {
			template<typename Context>
			void operator()(Context& ctx) {
				auto& g(get<parser_tag>(ctx).get());
				boost::intrusive_ptr<ExprCommonTables> p(new ExprCommonTables(ExprTable::const_ptr_t(), _attr(ctx)));
				_val(ctx) = p;
				g.push_cte(p);
			}
		};

		struct action_common_table_expression_end {
			template<typename Context>
			void operator()(Context& ctx) {
				auto& g(get<parser_tag>(ctx).get());
				_val(ctx)->set_subtable(_attr(ctx));
				g.pop_cte();
			}
		};

		struct action_select_substmt_compound_op {
			template<typename Context>
			void operator()(Context& ctx) {
				const compoundop& c(_attr(ctx));
				boost::intrusive_ptr<ExprCompound> p(new ExprCompound());
				p->set_operator(c.m_operator);
				p->set_distinct(c.m_distinct);
				_val(ctx) = p;
			}
		};

		struct action_select_substmt_compound_subexpr {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx)->set_subtable(1, _attr(ctx));
			}
		};

		struct action_select_substmt_compound {
			template<typename Context>
			void operator()(Context& ctx) {
				auto& g(get<parser_tag>(ctx).get());
				ExprTable::const_ptr_t p(_val(ctx));
				try {
					// check if select can be converted to osmselect
					if (p && p->get_type() == ExprTable::type_t::select) {
						boost::intrusive_ptr<ExprOSMSelect> p1(boost::static_pointer_cast<const ExprSelect>(p)->convert_osmselect(&g.m_sql));
						if (p1)
							p = p1;
					}
				} catch (const std::exception& e) {
					boost::throw_exception(x3::expectation_failure<decltype(_where(ctx).begin())>(_where(ctx).begin(), e.what()));
				}
				_attr(ctx)->set_subtable(0, p);
				try {
					_attr(ctx)->compute_columns();
				} catch (const std::exception& e) {
					boost::throw_exception(x3::expectation_failure<decltype(_where(ctx).begin())>(_where(ctx).begin(), e.what()));
				}
				_val(ctx) = _attr(ctx);
			}
		};

		struct action_select_substmt_sort_terms {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx)->set_terms(_attr(ctx));
			}
		};

		struct action_select_substmt_sort_limit {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx)->set_limit(_attr(ctx));
			}
		};

		struct action_select_substmt_sort_offset {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx)->set_offset(_attr(ctx));
			}
		};

		struct action_select_substmt_finalize {
			template<typename Context>
			void operator()(Context& ctx) {
				auto& g(get<parser_tag>(ctx).get());
				ExprTable::ptr_t p(_attr(ctx));
				{
					ExprTable::ResolveColRefSubtable resolve(p->get_subtables());
					p->visitor_call_visitend(resolve);
					ExprTable::const_ptr_t p1(p->simplify(&g.m_sql, resolve));
					if (p1)
						p = boost::const_pointer_cast<ExprTable>(p1);
				}
				if (false)
					g.m_sql.print_expr(std::cerr << "action_select_substmt_finalize: expr:", p, 2) << std::endl;
				if (false) {
					for (const ExprTable::Column& c : p->get_columns()) {
						std::cerr << "Column after subtable resolve: table \"" << c.get_table() << "\" name \"" << c.get_name() << "\" wildcard "
							  << (c.is_wildcard() ? "yes" : "no") << " expr " << (c.get_expr() ? "yes" : "no") << " aggregate "
							  << ((c.get_expr() && c.get_expr()->is_aggregate()) ? "yes" : "no") << std::endl;
						g.m_sql.print_expr(std::cerr << "  EXPR: ", c.get_expr(), 2) << std::endl;
					}
				}
				// check if select can be converted to osmselect
				try {
					if (p->get_type() == ExprTable::type_t::select) {
						boost::intrusive_ptr<ExprOSMSelect> p1(boost::static_pointer_cast<ExprSelect>(p)->convert_osmselect(&g.m_sql));
						if (p1)
							p = p1;
					}
				} catch (const std::exception& e) {
					boost::throw_exception(x3::expectation_failure<decltype(_where(ctx).begin())>(_where(ctx).begin(), e.what()));
				}
				_val(ctx) = p;
			}
		};

		struct action_common_table_expression {
			template<typename Context>
			void operator()(Context& ctx) {
				ExprTable::ptr_t p(at_c<1>(_attr(ctx))->clone());
				p->get_columns().set_table(at_c<0>(_attr(ctx)).m_tablename);
				if (at_c<0>(_attr(ctx)).m_columnnames.size() > p->get_columns().size()) {
					if (false) {
						std::cerr << "Table Columns: " << p->get_type() << std::endl;
						for (const ExprTable::Column& col : p->get_columns())
							std::cerr << "  " << col.to_str() << std::endl;
					}
					boost::throw_exception(x3::expectation_failure<decltype(_where(ctx).begin())>(_where(ctx).begin(), "as clause has too many column names"));
				}
				for (std::string::size_type i(0), n(at_c<0>(_attr(ctx)).m_columnnames.size()); i < n; ++i)
					p->get_columns()[i].set_name(at_c<0>(_attr(ctx)).m_columnnames[i]);
				_val(ctx) = p;
        		}
		};

		struct jointail {
			ExprTable::const_ptr_t m_right;
			joinop m_op;
			joinconstr m_constr;
		};

		struct action_join_clause_tail_op {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx).m_op = _attr(ctx);
			}
		};

		struct action_join_clause_tail_subtable {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx).m_right = _attr(ctx);
			}
		};

		struct action_join_clause_tail_constr {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx).m_constr = _attr(ctx);
			}
		};

		struct action_join_clause_tail {
			template<typename Context>
			void operator()(Context& ctx) {
				auto& g(get<parser_tag>(ctx).get());
				boost::intrusive_ptr<ExprJoin> p(new ExprJoin());
				p->set_subtable(0, _val(ctx));
				p->set_subtable(1, _attr(ctx).m_right);
				p->set_jointype(_attr(ctx).m_op.m_jointype);
				if (_attr(ctx).m_op.m_natural)
					p->compute_joincolumns_natural();
				if (_attr(ctx).m_constr.m_expr) {
					if (_attr(ctx).m_op.m_natural)
						boost::throw_exception(x3::expectation_failure<decltype(_where(ctx).begin())>(_where(ctx).begin(), "natural join incompatible with on"));
					if (_attr(ctx).m_op.m_cross)
						boost::throw_exception(x3::expectation_failure<decltype(_where(ctx).begin())>(_where(ctx).begin(), "cross join incompatible with on"));
		        		p->set_joinexpr(_attr(ctx).m_constr.m_expr);
				}
				if (!_attr(ctx).m_constr.m_columns.empty()) {
					if (_attr(ctx).m_op.m_natural)
						boost::throw_exception(x3::expectation_failure<decltype(_where(ctx).begin())>(_where(ctx).begin(), "natural join incompatible with using"));
					if (_attr(ctx).m_op.m_cross)
						boost::throw_exception(x3::expectation_failure<decltype(_where(ctx).begin())>(_where(ctx).begin(), "cross join incompatible with using"));
					p->compute_joincolumns(_attr(ctx).m_constr.m_columns);
				}
				try {
					p->compute_columns();
				} catch (const std::exception& e) {
					boost::throw_exception(x3::expectation_failure<decltype(_where(ctx).begin())>(_where(ctx).begin(), e.what()));
				}
				if (false)
					g.m_sql.print_expr(std::cerr << "action_join_clause_tail: before resolve expr:", p, 2) << std::endl;
				{
					ExprTable::ResolveColRefSubtable resolve(p->get_subtables());
					p->visitor_call_visitend(resolve);
					ExprTable::const_ptr_t p1(p->simplify(&g.m_sql, resolve));
					boost::intrusive_ptr<ExprJoin> p2(boost::dynamic_pointer_cast<ExprJoin>(boost::const_pointer_cast<ExprTable>(p1)));
					if (p1 && !p2)
						boost::throw_exception(x3::expectation_failure<decltype(_where(ctx).begin())>(_where(ctx).begin(), "join optimize error"));
					if (p2)
						p = p2;
				}
				if (false)
					g.m_sql.print_expr(std::cerr << "action_join_clause_tail: expr:", p, 2) << std::endl;
				_val(ctx) = p;
			}
		};

		struct action_table_function {
			template<typename Context>
			void operator()(Context& ctx) {
				boost::intrusive_ptr<ExprTableFunc> p(new ExprTableFunc(at_c<0>(_attr(ctx)), at_c<1>(_attr(ctx))));
				try {
					p->compute_columns();
				} catch (const std::exception& e) {
					boost::throw_exception(x3::expectation_failure<decltype(_where(ctx).begin())>(_where(ctx).begin(), e.what()));
				}
				_val(ctx) = p;
			}
		};

		struct asclause {
			std::string m_tablename;
			std::vector<std::string> m_columnnames;
		};

		struct action_table_or_subquery_as_clause_tblname {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx).m_tablename = _attr(ctx);
			}
		};

		struct action_table_or_subquery_as_clause_colnames {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx).m_columnnames = _attr(ctx);
			}
		};

		struct action_table_or_subquery_as_clause {
			template<typename Context>
			void operator()(Context& ctx) {
				if (_attr(ctx).m_tablename.empty())
					return;
				ExprTable::ptr_t p(_val(ctx)->clone());
				p->get_columns().set_table(_attr(ctx).m_tablename);
				if (_attr(ctx).m_columnnames.size() > p->get_columns().size()) {
					if (false) {
						std::cerr << "Table Columns: " << p->get_type() << std::endl;
						for (const ExprTable::Column& col : p->get_columns())
							std::cerr << "  " << col.to_str() << std::endl;
					}
					boost::throw_exception(x3::expectation_failure<decltype(_where(ctx).begin())>(_where(ctx).begin(), "as clause has too many column names"));
				}
				for (std::string::size_type i(0), n(_attr(ctx).m_columnnames.size()); i < n; ++i)
					p->get_columns()[i].set_name(_attr(ctx).m_columnnames[i]);
				_val(ctx) = p;
			}
		};

		struct action_table_or_subquery_ident {
			template<typename Context>
			void operator()(Context& ctx) {
				auto& g(get<parser_tag>(ctx).get());
				if (boost::iequals(_attr(ctx), "values")) {
					_val(ctx) = nullptr;
					return;
				}
				{
					const ExprTable::const_ptr_t& p(g.find_cte(_attr(ctx)));
					if (p) {
						boost::intrusive_ptr<ExprTableReference> p1(new ExprTableReference(_attr(ctx)));
						p1->set_columns(p->get_columns());
						_val(ctx) = p;
						return;
					}
				}
				{
					ExprOSMTable::table_t tbl;
					if (ExprOSMTable::parse_tablename(tbl, _attr(ctx))) {
						boost::intrusive_ptr<ExprOSMTable> p(new ExprOSMTable(tbl));
						try {
							p->compute_columns(g.m_sql);
						} catch (const std::exception& e) {
							boost::throw_exception(x3::expectation_failure<decltype(_where(ctx).begin())>(_where(ctx).begin(), e.what()));
						}
						_val(ctx) = p;
						return;
					}
				}
				boost::throw_exception(x3::expectation_failure<decltype(_where(ctx).begin())>(_where(ctx).begin(), "unknown table " + _attr(ctx)));
			}
		};

		struct action_table_or_subquery_list {
			template<typename Context>
			void operator()(Context& ctx) {
				if (!_val(ctx)) {
					_val(ctx) = _attr(ctx);
					return;
				}
				boost::intrusive_ptr<ExprJoin> p(new ExprJoin());
				p->set_jointype(ExprJoin::jointype_t::inner);
				p->set_subtable(0, _val(ctx));
				p->set_subtable(1, _attr(ctx));
				try {
					p->compute_columns();
				} catch (const std::exception& e) {
					boost::throw_exception(x3::expectation_failure<decltype(_where(ctx).begin())>(_where(ctx).begin(), e.what()));
				}
				_val(ctx) = p;
			}
		};

		struct action_table_or_subquery_ident_ok {
			template<typename Context>
			bool operator()(Context& ctx) const {
				return !!_val(ctx);
			}

			bool operator()(x3::unused_type) const {
				return true;
			}
		};

		const rule<struct table_function, boost::intrusive_ptr<ExprTableFunc> > table_function = "table_function";
		const rule<struct identifier_list, std::vector<std::string> > identifier_list = "identifier_list";
		const rule<struct common_table_expressions, ExprCommonTables::namedtables_t> common_table_expressions = "common_table_expressions";
		const rule<struct common_table_expression, ExprTable::ptr_t> common_table_expression = "common_table_expression";
		const rule<struct result_column, ExprTable::Column> result_column = "result_column";
		const rule<struct result_columns, ExprTable::Columns> result_columns = "result_columns";
		const rule<struct table_or_subquery_as_clause_main, asclause> table_or_subquery_as_clause_main = "table_or_subquery_as_clause_main";
		const rule<struct table_or_subquery_as_clause, asclause> table_or_subquery_as_clause = "table_or_subquery_as_clause";
		const rule<struct table_or_subquery_ident, ExprTable::const_ptr_t> table_or_subquery_ident = "table_or_subquery_ident";
		const rule<struct table_or_subquery, ExprTable::const_ptr_t> table_or_subquery = "table_or_subquery";
		const rule<struct join_clause_tail, jointail> join_clause_tail = "join_clause_tail";
		const rule<struct join_clause, ExprTable::const_ptr_t> join_clause = "join_clause";
		const rule<struct compound_operator, compoundop> compound_operator = "compound_operator";
		const rule<struct select_distinctre, std::pair<ExprSelect::distinct_t,Expr::exprlist_t> > select_distinctre = "select_distinctre";
		const rule<struct select_distinctall, std::pair<ExprSelect::distinct_t,Expr::exprlist_t> > select_distinctall = "select_distinctall";
		const rule<struct select_distinct, std::pair<ExprSelect::distinct_t,Expr::exprlist_t> > select_distinct = "select_distinct";
		const rule<struct select_core_values, boost::intrusive_ptr<ExprLiteralTable> > select_core_values = "select_core_values";
		const rule<struct select_core_select_window> select_core_select_window = "select_core_select_window";
		const rule<struct select_core_select, selectcoresel> select_core_select = "select_core_select";
		const rule<struct select_core, boost::intrusive_ptr<ExprTableSort> > select_core = "select_core";
		const rule<struct select_substmt_compound, boost::intrusive_ptr<ExprCompound> > select_substmt_compound = "select_substmt_compound";
		const rule<struct select_substmt_main, boost::intrusive_ptr<ExprTableSort> > select_substmt_main = "select_substmt_main";
		const rule<struct select_substmt, ExprTable::ptr_t> select_substmt = "select_substmt";
		const rule<struct join_operator, joinop> join_operator = "join_operator";
		const rule<struct join_constraint, joinconstr> join_constraint = "join_constraint";
		const rule<struct select_stmt_with, boost::intrusive_ptr<ExprCommonTables> > select_stmt_with = "select_stmt_with";
		const select_stmt_type select_stmt INIT_PRIORITY(200) = "select_stmt";

		const auto table_function_def =
			(lexeme[no_case[functable] >> !char_("0-9a-zA-Z_")] >
			 lit('(') > func_exprlist > lit(')'))[action_table_function{}];

		const auto select_stmt_with_def =
			keyword("with") > -(keyword("recursive")[action_notsupported{"recursive common table expression not supported"}]) >
			common_table_expressions[action_common_table_expression_begin{}] >
			select_substmt[action_common_table_expression_end{}];

		const auto select_stmt_def =
			select_stmt_with[action_copy{}]	| select_substmt[action_copy{}];

		const auto select_distinctre_def =
			keyword("distinct")[action_select_distinct_tag<ExprSelect::distinct_t::distinctrows>{}] >>
			-(keyword("on")[action_select_distinct_tag<ExprSelect::distinct_t::distinctexpr>{}] > lit('(') >
			  func_exprlist[action_select_distinct_expr{}] > lit(')'));

		const auto select_distinctall_def =
			(-keyword("all"))[action_select_distinct_tag<ExprSelect::distinct_t::all>{}];

		const auto select_distinct_def =
			select_distinctre[action_copy{}]
			| select_distinctall[action_copy{}];

		const auto select_core_values_def =
			keyword("values")[action_select_core_values_init{}] >
			((lit('(') > func_exprlist[action_select_core_values_addexpr{}] > lit(')')) % lit(',')) >
			eps[action_select_core_values_finalize{}];

		const auto select_core_select_window_def =
			keyword("window") > ((omit[identifier] >> keyword("as") >> window_defn) % lit(','))[action_select_core_select_window{}];

		const auto select_core_select_def =
			keyword("select") > select_distinct[action_select_core_select_distinct{}] >
			result_columns[action_select_core_select_columns{}] >
			-(keyword("from") > join_clause[action_select_core_select_from{}]) >
			-(keyword("where") > expr[action_select_core_select_where{}]) >
			-(keyword("group") > keyword("by") > func_exprlist[action_select_core_select_group{}] >
			  -(keyword("having") > expr[action_select_core_select_having{}])) >
			-(select_core_select_window);

		const auto select_core_def =
			select_core_values[action_copy{}] | select_core_select[action_select_core_select{}];

		const auto select_substmt_compound_def =
			compound_operator[action_select_substmt_compound_op{}] >
			select_substmt[action_select_substmt_compound_subexpr{}];

		const auto select_substmt_main_def =
			select_core[action_copy{}] >>
			(select_substmt_compound[action_select_substmt_compound{}]
			 | (-(keyword("order") > keyword("by") > (ordering_terms[action_select_substmt_sort_terms{}])) >>
			    -(keyword("limit") > expr[action_select_substmt_sort_limit{}] >
			      -((keyword("offset") | lit(',')) > expr[action_select_substmt_sort_offset{}]))));

		const auto select_substmt_def =
			select_substmt_main[action_select_substmt_finalize{}];

		const auto identifier_list_def = identifier % lit(',');

		const auto common_table_expression_def =
			(table_or_subquery_as_clause_main >>
			 keyword("as") >> lit('(') >> select_stmt >> lit(')'))[action_common_table_expression{}];

		const auto common_table_expressions_def =
			common_table_expression % lit(',');

		const auto result_column_def =
			(identifier[action_result_column_tblwildcard{}] >> lit('.') >> lit('*'))
			| lit('*')[action_result_column_wildcard{}]
			| (expr[action_result_column_expr{}] >> -(-(keyword("as")) >> identifier[action_result_column_ident{}]));

		const auto result_columns_def =
			(result_column[action_result_columns{}] % lit(','));

		const auto table_or_subquery_as_clause_main_def =
			identifier[action_table_or_subquery_as_clause_tblname{}] >>
			-(lit('(') > identifier_list[action_table_or_subquery_as_clause_colnames{}] > lit(')'));

		const auto table_or_subquery_as_clause_def =
			-(-(keyword("as")) >> table_or_subquery_as_clause_main[action_copy{}]);

		const auto table_or_subquery_ident_def =
			identifier[action_table_or_subquery_ident{}] >> eps(action_table_or_subquery_ident_ok{});

		const auto table_or_subquery_def =
			(lit('(') >>
			   ((join_clause[action_copy{}] >> lit(')'))
			    | ((table_or_subquery[action_table_or_subquery_list{}] % lit(',')) >> lit(')'))
			    | (select_stmt[action_copy{}] >> lit(')') >>
			       table_or_subquery_as_clause[action_table_or_subquery_as_clause{}])))
			| (-(omit[identifier] >> lit('.')) >>
			   ((table_function[action_copy{}] >>
			     table_or_subquery_as_clause[action_table_or_subquery_as_clause{}])
			    | (table_or_subquery_ident[action_copy{}] >>
			       table_or_subquery_as_clause[action_table_or_subquery_as_clause{}] >>
			       -((keyword("indexed") >> keyword("by") >> omit[identifier])
				 | (keyword("not") >> keyword("indexed"))))));

		const auto join_clause_tail_def =
			join_operator[action_join_clause_tail_op{}] >>
			table_or_subquery[action_join_clause_tail_subtable{}] >>
			join_constraint[action_join_clause_tail_constr{}];

		const auto join_clause_def =
			table_or_subquery[action_copy{}] >> *(join_clause_tail[action_join_clause_tail{}]);

		const auto join_operator_def =
			lit(',')[action_joinop_cross{}]
			| keyword("join")[action_joinop_settype{ExprJoin::jointype_t::inner}]
			| (keyword("cross") >> keyword("join"))[action_joinop_cross{}]
			| (-(keyword("natural")[action_joinop_natural{}]) >>
			   (((keyword("left")[action_joinop_settype{ExprJoin::jointype_t::leftouter}]
			      | keyword("right")[action_joinop_settype{ExprJoin::jointype_t::rightouter}]
			      | keyword("full")[action_joinop_settype{ExprJoin::jointype_t::fullouter}]) >> -(keyword("outer")))
			    | keyword("inner")[action_joinop_settype{ExprJoin::jointype_t::inner}]) >> keyword("join"));

		const auto join_constraint_def =
			-((keyword("on") > expr[action_join_constraint_expr{}])
			  | (keyword("using") > lit('(') > identifier_list[action_join_constraint_cols{}] > lit(')')));

		const auto compound_operator_def =
			(keyword("union")[action_compoundop_setop{ExprCompound::operator_t::union_}]
			 | keyword("intersect")[action_compoundop_setop{ExprCompound::operator_t::intersect}]
			 | keyword("except")[action_compoundop_setop{ExprCompound::operator_t::except}]) >>
			-(keyword("all")[action_compoundop_setdist{ExprCompound::distinct_t::all}]);

		BOOST_SPIRIT_DEFINE(table_function, select_stmt_with, select_stmt, select_distinctre,
				    select_distinctall, select_distinct, select_core_values,
				    select_core_select_window, select_core_select, select_core,
				    select_substmt_compound, select_substmt_main, select_substmt,
				    identifier_list, common_table_expression, common_table_expressions,
				    result_column, result_columns, table_or_subquery_as_clause_main,
				    table_or_subquery_as_clause, table_or_subquery_ident,
				    table_or_subquery, join_clause_tail, join_clause, join_operator,
				    join_constraint, compound_operator);

		BOOST_SPIRIT_INSTANTIATE(select_stmt_type, iterator_type, context_type);

#if SPIRIT_X3_VERSION < 0x3006
		template bool parse_rule<iterator_type, context_type, x3::unused_type const>(select_stmt_type, iterator_type&, iterator_type const&, context_type const&, x3::unused_type const&);
#endif

		//------------------------------------------------------------------------------
		// Statement
		//------------------------------------------------------------------------------

		const rule<struct selectexpr, ExprTable::const_ptr_t> selectexpr = "selectexpr";

		const auto selectexpr_def = lit('(') >> select_stmt[action_copy{}] >> lit(')') >>
			table_or_subquery_as_clause[action_table_or_subquery_as_clause{}];

		BOOST_SPIRIT_DEFINE(selectexpr);
	};
};

namespace {
	void printcols(std::ostream& oss, const CartoSQL::ExprTable::Columns& cols)
	{
		bool subseq(false);
		for (const CartoSQL::ExprTable::Column& col : cols) {
			if (subseq)
				oss << ',';
			subseq = true;
			oss << ' ' << col.to_str();
		}
	}

	void check_unresolved(CartoSQLParser::cartosql_parser<boost::spirit::line_pos_iterator<boost::spirit::istream_iterator> >& g,
			      const CartoSQL::ExprTable::UnresolvedColRef& unresolved)
	{
		namespace x3 = boost::spirit::x3;
		using Iterator = boost::spirit::istream_iterator;
		using PosIterator = boost::spirit::line_pos_iterator<Iterator>;
		static constexpr const char *lrnames[2] = { "left", "right" };
		if (!unresolved.is_colref())
			return;
		std::ostringstream oss;
		oss << "expression has unresolved column references: " + unresolved.get_expressionstring();
		for (const auto& expr : unresolved.get_expressions()) {
			if (!expr.first || !expr.second)
				continue;
			PosIterator iter;
			if (!g.find_location(iter, expr.first))
				continue;
			oss << std::endl << "Contained in Table Expression: " << expr.second->get_type();
			printcols(oss, expr.second->get_columns());
			switch (expr.second->get_type()) {
			case CartoSQL::ExprTable::type_t::select:
			{
				boost::intrusive_ptr<const CartoSQL::ExprSelect> p(boost::static_pointer_cast<const CartoSQL::ExprSelect>(expr.second));
				if (!p->get_fromexpr())
					break;
				printcols(oss << std::endl << "  from: ", p->get_fromexpr()->get_columns());
				break;
			}

			case CartoSQL::ExprTable::type_t::compound:
			{
				boost::intrusive_ptr<const CartoSQL::ExprCompound> p(boost::static_pointer_cast<const CartoSQL::ExprCompound>(expr.second));
				for (unsigned int i = 0; i < 2; ++i) {
					if (!p->get_subtable(i))
						continue;
					printcols(oss << std::endl << "  " << lrnames[i] << ": ", p->get_subtable(i)->get_columns());
				}
				break;
			}

			case CartoSQL::ExprTable::type_t::join:
			{
				boost::intrusive_ptr<const CartoSQL::ExprJoin> p(boost::static_pointer_cast<const CartoSQL::ExprJoin>(expr.second));
				for (unsigned int i = 0; i < 2; ++i) {
					if (!p->get_subtable(i))
						continue;
					printcols(oss << std::endl << "  " << lrnames[i] << ": ", p->get_subtable(i)->get_columns());
				}
				break;
			}

			default:
				break;
			}
			boost::throw_exception(x3::expectation_failure<PosIterator>(iter, oss.str()));
		}
		throw CartoSQL::SQLError(oss.str());
	}
};

CartoSQL::ExprTable::const_ptr_t CartoSQL::parse_sql(std::istream& is, std::ostream *msg)
{
	if (!is)
		throw std::runtime_error("Cannot read sql");
	namespace x3 = boost::spirit::x3;
	using Iterator = boost::spirit::istream_iterator;
	using PosIterator = boost::spirit::line_pos_iterator<Iterator>;
	Iterator f(is >> std::noskipws), l;
	PosIterator pf(f), pi(pf), pl;
	CartoSQLParser::cartosql_parser<PosIterator> g(*this, pf, pl, msg);
	try {
		ExprTable::const_ptr_t p;
		if (!x3::phrase_parse(pi, pl, x3::with<CartoSQLParser::parser_tag>(std::ref(g))[CartoSQLParser::parser::selectexpr >> x3::eoi],
				      CartoSQLParser::skipper::skip, p))
			CartoSQLParser::parse_error(pf, pi, pl);
		if (p) {
			ExprTable::UnresolvedColRef unresolved;
			p->visit(unresolved);
			check_unresolved(g, unresolved);
		}
		return p;
	} catch (const x3::expectation_failure<PosIterator>& x) {
		CartoSQLParser::parse_error(pf, x.where(), pl, x.which());
	}
	return ExprTable::const_ptr_t();
}

CartoSQL::Expr::const_ptr_t CartoSQL::parse_sql_value(std::istream& is, std::ostream *msg)
{
	if (!is)
		throw std::runtime_error("Cannot read sql");
	namespace x3 = boost::spirit::x3;
	using Iterator = boost::spirit::istream_iterator;
	using PosIterator = boost::spirit::line_pos_iterator<Iterator>;
	Iterator f(is >> std::noskipws), l;
	PosIterator pf(f), pi(pf), pl;
	CartoSQLParser::cartosql_parser<PosIterator> g(*this, pf, pl, msg);
	try {
		Expr::const_ptr_t p;
		if (!x3::phrase_parse(pi, pl, x3::with<CartoSQLParser::parser_tag>(std::ref(g))[CartoSQLParser::parser::expr >> x3::eoi],
				      CartoSQLParser::skipper::skip, p))
			CartoSQLParser::parse_error(pf, pi, pl);
		if (p) {
			ExprTable::UnresolvedColRef unresolved;
			p->visit(unresolved);
			check_unresolved(g, unresolved);
		}
		return p;
	} catch (const x3::expectation_failure<PosIterator>& x) {
		CartoSQLParser::parse_error(pf, x.where(), pl, x.which());
	}
	return Expr::const_ptr_t();
}
