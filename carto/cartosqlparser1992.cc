//
// C++ Implementation: cartosqlparser
//
// Description: CartoCSS SQL parser
//
//
// Author: Thomas Sailer <t.sailer@alumni.ethz.ch>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#define BOOST_SPIRIT_X3_DEBUG
#include <boost/config/warning_disable.hpp>
#include <boost/spirit/home/x3.hpp>
#include <boost/fusion/include/vector.hpp>
#include <boost/fusion/include/make_deque.hpp>
#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/fusion/include/io.hpp>
#include <boost/fusion/adapted.hpp>
#include <boost/spirit/include/support_line_pos_iterator.hpp>
#include <boost/spirit/include/support_istream_iterator.hpp>

#include <boost/variant.hpp>
#include <boost/pool/object_pool.hpp>

#include <glibmm.h>
#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>

#include "cartosql.h"
#include "geom.h"

namespace {

	namespace x3 = boost::spirit::x3;
	namespace ascii = boost::spirit::x3::ascii;
	namespace fusion = boost::fusion;

	template<typename Iterator>
	void parse_error(const Iterator& begin, const Iterator& iter, const Iterator& end, const std::string& info = "") {
		std::ostringstream oss;
		oss << "parse error at";
		{
			std::size_t ln(boost::spirit::get_line(iter));
			if (ln != static_cast<std::size_t>(-1))
				oss << " line " << ln;
		}
		std::size_t col(boost::spirit::get_column(begin, iter, 8));
		oss << " column " << col << std::endl
		    << '\'' << boost::spirit::get_current_line(begin, iter, end) << '\'' << std::endl
		    <<std::setw(col) << ' ' << "^- here";
		if (!info.empty())
			oss << std::endl << info;
		throw std::runtime_error(oss.str());
	}

	template<typename Iterator>
	void parse_error(const Iterator& iter, const std::string& info = "") {
		std::ostringstream oss;
		oss << "parse error at";
		{
			std::size_t ln(boost::spirit::get_line(iter));
			if (ln != static_cast<std::size_t>(-1))
				oss << " line " << ln;
		}
		if (!info.empty())
			oss << std::endl << info;
		throw std::runtime_error(oss.str());
	}

	template<typename Iterator>
	struct cartosql_parser {
		cartosql_parser(CartoSQL& sql, Iterator const& first, Iterator const& last, std::ostream *msg = nullptr)
			: m_sql(sql), m_msg(msg), m_first(first), m_last(last) {
		}

		Iterator set_recursion(const char *tag, Iterator const& iter) {
			typename recursion_t::iterator i(m_recursion.find(tag));
			if (i == m_recursion.end()) {
				if (iter != m_last)
					m_recursion.insert(std::make_pair(tag, iter));
				return m_last;
			}
			Iterator ret(i->second);
			if (iter == m_last)
				m_recursion.erase(i);
			else
				i->second = iter;
			return ret;
		}

		void finalize(void) {
		}

		CartoSQL& m_sql;
		std::ostream *m_msg;
		typedef std::map<const char *, Iterator> recursion_t;
		recursion_t m_recursion;
		Iterator m_first;
		Iterator m_last;
	};

	namespace skipper {
		using x3::rule;
		using x3::lit;
		using x3::eol;
		using x3::eoi;
		using ascii::char_;
		using ascii::space;

		const rule<class single_line_comment> single_line_comment = "single_line_comment";
		const rule<class block_comment> block_comment = "block_comment";
		const rule<class skip> skip = "skip";

		const auto single_line_comment_def = lit("--") >> *(char_ - eol) >> (eol|eoi);
		const auto block_comment_def = (lit("/*") >> *(block_comment | (char_ - lit("*/")))) > lit("*/");
		const auto skip_def = space | single_line_comment | block_comment;

		BOOST_SPIRIT_DEFINE(single_line_comment, block_comment, skip);
	};

	namespace parser {
		using x3::rule;
		using x3::get;
		using x3::no_case;
		using x3::no_skip;
		using x3::lit;
		using x3::string;
		using x3::omit;
		using x3::lexeme;
		using x3::repeat;
		using x3::eps;
		using x3::eol;
		using x3::eoi;
		using x3::attr;
		using x3::with;
		using x3::_val;
		using x3::_attr;
		using x3::_where;
		using x3::_pass;
		using x3::ulong_;
		using x3::long_;
		using x3::double_;
		using ascii::char_;
		using ascii::alnum;
		using ascii::alpha;
		using ascii::digit;
		using ascii::xdigit;
		using ascii::space;
		using fusion::at_c;

		//-----------------------------------------------------------------------------
		// Line
		//-----------------------------------------------------------------------------

		struct parser_tag {};

 		//-----------------------------------------------------------------------------
		// Operators
		//-----------------------------------------------------------------------------

		const struct compoptable_ : x3::symbols<CartoSQL::Expr::compop_t> {
			compoptable_() {
				add
					(">=",			  CartoSQL::Expr::compop_t::greaterthan)
					("<=",			  CartoSQL::Expr::compop_t::lessthan)
					("<>",			  CartoSQL::Expr::compop_t::unequal)
					("=",			   CartoSQL::Expr::compop_t::equal)
					(">",			   CartoSQL::Expr::compop_t::greater)
					("<",			   CartoSQL::Expr::compop_t::less);
			}
		} compoptable;

		//-----------------------------------------------------------------------------
		// Forward Rule Definitions
		//-----------------------------------------------------------------------------

		// 5.2 <token> and <separator>
		const rule<class delimited_identifier> delimited_identifier = "delimited identifier";

		// 5.3 <literal>
		const rule<class literal> literal = "literal";
		const rule<class character_string_literal> character_string_literal = "character string literal";
		const rule<class introducer> introducer = "introducer";
		const rule<class character_representation> character_representation = "character representation";
		const rule<class nonquote_character> nonquote_character = "nonquote character";
		const rule<class quote_symbol> quote_symbol = "quote symbol";
		const rule<class national_character_string_literal> national_character_string_literal = "national character string literal";
		const rule<class bit_string_literal> bit_string_literal = "bit string literal";
		const rule<class hex_string_literal> hex_string_literal = "hex string literal";
		const rule<class unsigned_numeric_literal> unsigned_numeric_literal = "unsigned numeric literal";
		const rule<class date_string> date_string = "date string";
		const rule<class time_string> time_string = "time string";
		const rule<class timestamp_string> timestamp_string = "timestamp string";
		const rule<class interval_string> interval_string = "interval string";

		// 5.4 Names and identifiers
		const rule<class identifier> identifier = "identifier";
		const rule<class authorization_identifier> authorization_identifier = "authorization identifier";
		const rule<class table_name> table_name = "table name";
		const rule<class domain_name> domain_name = "domain name";
		const rule<class schema_name> schema_name = "schema name";
		const rule<class catalog_name> catalog_name = "catalog name";
		const rule<class qualified_name> qualified_name = "qualified name";
		const rule<class column_name> column_name = "column name";
		const rule<class module_name> module_name = "module name";
		const rule<class cursor_name> cursor_name = "cursor name";
		const rule<class procedure_name> procedure_name = "procedure name";
		const rule<class SQL_statement_name> SQL_statement_name = "L statement name";
		const rule<class statement_name> statement_name = "statement name";
		const rule<class extended_statement_name> extended_statement_name = "extended statement name";
		const rule<class extended_cursor_name> extended_cursor_name = "extended cursor name";
		const rule<class descriptor_name> descriptor_name = "descriptor name";
		const rule<class parameter_name> parameter_name = "parameter name";
		const rule<class collation_name> collation_name = "collation name";
		const rule<class translation_name> translation_name = "translation name";
		const rule<class connection_name> connection_name = "connection name";
		const rule<class SQL_server_name> SQL_server_name = "L-server name";
		const rule<class user_name> user_name = "user name";

		// 6.1 <data type>
		const rule<class data_type> data_type = "data type";
		const rule<class exact_numeric_type> exact_numeric_type = "exact numeric type";
		const rule<class approximate_numeric_type> approximate_numeric_type = "approximate numeric type";
		const rule<class length> length = "length";
		const rule<class precision> precision = "precision";
		const rule<class scale> scale = "scale";
		const rule<class time_fractional_seconds_precision> time_fractional_seconds_precision = "time fractional seconds precision";

		// 6.2 <value specification> and <target specification>
		const rule<class value_specification> value_specification = "value specification";
		const rule<class simple_value_specification> simple_value_specification = "simple value specification";
		const rule<class target_specification> target_specification = "target specification";

		// 6.3 <table reference>
		const rule<class table_reference> table_reference = "table reference";
		const rule<class column_name_list> column_name_list = "column name list";

		// 6.4 <column reference>
		const rule<class column_reference> column_reference = "column reference";

		// 6.5 <set function specification>
		const rule<class set_function_specification> set_function_specification = "set function specification";

		// 6.6 <numeric value function>
		const rule<class numeric_value_function> numeric_value_function = "numeric value function";
		const rule<class position_expression> position_expression = "position expression";
		const rule<class length_expression> length_expression = "length expression";
		const rule<class extract_expression> extract_expression = "extract expression";

		// 6.7 <string value function>
		const rule<class string_value_function> string_value_function = "string value function";
		const rule<class character_substring_function> character_substring_function = "character substring function";
		const rule<class fold> fold = "fold";
		const rule<class form_of_use_conversion> form_of_use_conversion = "form-of-use conversion";
		const rule<class character_translation> character_translation = "character translation";
		const rule<class trim_function> trim_function = "trim function";
		const rule<class bit_substring_function> bit_substring_function = "bit substring function";

		// 6.8 <datetime value function>
		const rule<class datetime_value_function> datetime_value_function = "datetime value function";

		// 6.9 <case expression>
		const rule<class case_expression> case_expression = "case expression";

		// 6.10 <cast specification>
		const rule<class cast_specification> cast_specification = "cast specification";

		// 6.11 <value expression>
		const rule<class value_expression> value_expression = "value expression";

		// 6.12 <numeric value expression>
		const rule<class numeric_value_expression> numeric_value_expression = "numeric value expression";

		// 6.13 <string value expression>
		const rule<class string_value_expression> string_value_expression = "string value expression";
		const rule<class character_value_expression> character_value_expression = "character value expression";
		const rule<class bit_value_expression> bit_value_expression = "bit value expression";
		const rule<class bit_concatenation> bit_concatenation = "bit concatenation";

		// 6.14 <datetime value expression>
		const rule<class datetime_value_expression> datetime_value_expression = "datetime value expression";

		// 6.15 <interval value expression>
		const rule<class interval_value_expression> interval_value_expression = "interval value expression";
		const rule<class interval_term> interval_term = "interval term";

		// 7.1 <row value constructor>
		const rule<class row_value_constructor> row_value_constructor = "row value constructor";

		// 7.2 <table value constructor>
		const rule<class table_value_constructor> table_value_constructor = "table value constructor";

		// 7.3 <table expression>
		const rule<class table_expression> table_expression = "table expression";

		// 7.4 <from clause>
		const rule<class from_clause> from_clause = "from clause";

		// 7.5 <joined table>
		const rule<class joined_table> joined_table = "joined table";
		const rule<class join_condition> join_condition = "join condition";

		// 7.6 <where clause>
		const rule<class where_clause> where_clause = "where clause";

		// 7.7 <group by clause>
		const rule<class group_by_clause> group_by_clause = "group by clause";

		// 7.8 <having clause>
		const rule<class having_clause> having_clause = "having clause";

		// 7.9 <query specification>
		const rule<class query_specification> query_specification = "query specification";
		const rule<class select_list> select_list = "select list";
		const rule<class derived_column> derived_column = "derived column";

		// 7.10 <query expression>
		const rule<class query_expression> query_expression = "query expression";

		// 7.11  <scalar subquery>, <row subquery>, and <table subquery>
		const rule<class scalar_subquery> scalar_subquery = "scalar subquery";
		const rule<class row_subquery> row_subquery = "row subquery";
		const rule<class table_subquery> table_subquery = "table subquery";
		const rule<class subquery> subquery = "subquery";

		// 8.1 <predicate>
		const rule<class predicate> predicate = "predicate";

		// 8.2 <comparison predicate>
		const rule<class comparison_predicate> comparison_predicate = "comparison predicate";

		// 8.3 <between predicate>
		const rule<class between_predicate> between_predicate = "between predicate";

		// 8.4 <in predicate>
		const rule<class in_predicate> in_predicate = "in predicate";

		// 8.5 <like predicate>
		const rule<class like_predicate> like_predicate = "like predicate";

		// 8.6 <null predicate>
		const rule<class null_predicate> null_predicate = "null predicate";

		// 8.7 <quantified comparison predicate>
		const rule<class quantified_comparison_predicate> quantified_comparison_predicate = "quantified comparison predicate";

		// 8.8 <exists predicate>
		const rule<class exists_predicate> exists_predicate = "exists predicate";

		// 8.9 <unique predicate>
		const rule<class unique_predicate> unique_predicate = "unique predicate";

		// 8.10 <match predicate>
		const rule<class match_predicate> match_predicate = "match predicate";

		// 8.11 <overlaps predicate>
		const rule<class overlaps_predicate> overlaps_predicate = "overlaps predicate";

		// 8.12 <search condition>
		const rule<class search_condition> search_condition = "search condition";
		const rule<class boolean_factor> boolean_factor = "boolean factor";

		// 10.1 <interval qualifier>
		const rule<class interval_qualifier> interval_qualifier = "interval qualifier";
		const rule<class start_field> start_field = "start field";
		const rule<class end_field> end_field = "end field";
		const rule<class datetime_field> datetime_field = "datetime field";
		const rule<class interval_leading_field_precision> interval_leading_field_precision = "interval leading field precision";

		// 10.2 <language clause>
		const rule<class language_clause> language_clause = "language clause";

		// 10.3 <privileges>
		const rule<class privileges> privileges = "privileges";
		const rule<class action> action = "action";

		// 10.4 <character set specification>
		const rule<class character_set_specification> character_set_specification = "character set specification";
		const rule<class implementation_defined_character_repertoire_name> implementation_defined_character_repertoire_name = "implementation-defined character repertoire name";

		// 10.5 <collate clause>
		const rule<class collate_clause> collate_clause = "collate clause";

		// 10.6 <constraint name definition> and <constraint attributes>
		const rule<class constraint_name_definition> constraint_name_definition = "constraint name definition";
		const rule<class constraint_attributes> constraint_attributes = "constraint attributes";

		// 11.1 <schema definition>
		const rule<class schema_definition> schema_definition = "schema definition";
		const rule<class schema_name_clause> schema_name_clause = "schema name clause";
		const rule<class schema_authorization_identifier> schema_authorization_identifier = "schema authorization identifier";
		const rule<class schema_character_set_specification> schema_character_set_specification = "schema character set specification";

		// 11.2 <drop schema statement>
		const rule<class drop_schema_statement> drop_schema_statement = "drop schema statement";

		// 11.3 <table definition>
		const rule<class table_definition> table_definition = "table definition";

		// 11.4 <column definition>
		const rule<class column_definition> column_definition = "column definition";

		// 11.5 <default clause>
		const rule<class default_clause> default_clause = "default clause";
		const rule<class default_option> default_option = "default option";

		// 11.6 <table constraint definition>
		const rule<class table_constraint_definition> table_constraint_definition = "table constraint definition";
		const rule<class table_constraint> table_constraint = "table constraint";

		// 11.7 <unique constraint definition>
		const rule<class unique_constraint_definition> unique_constraint_definition = "unique constraint definition";
		const rule<class unique_specification> unique_specification = "unique specification";
		const rule<class unique_column_list> unique_column_list = "unique column list";

		// 11.8 <referential constraint definition>
		const rule<class referential_constraint_definition> referential_constraint_definition = "referential constraint definition";
		const rule<class references_specification> references_specification = "references specification";
		const rule<class match_type> match_type = "match type";
		const rule<class referencing_columns> referencing_columns = "referencing columns";
		const rule<class referenced_table_and_columns> referenced_table_and_columns = "referenced table and columns";

		// 11.9 <check constraint definition>
		const rule<class check_constraint_definition> check_constraint_definition = "check constraint definition";

		// 11.10 <alter table statement>
		const rule<class alter_table_statement> alter_table_statement = "alter table statement";

		// 11.11 <add column definition>
		const rule<class add_column_definition> add_column_definition = "add column definition";

		// 11.12 <alter column definition>
		const rule<class alter_column_definition> alter_column_definition = "alter column definition";

		// 11.13 <set column default clause>
		const rule<class set_column_default_clause> set_column_default_clause = "set column default clause";

		// 11.14 <drop column default clause>
		const rule<class drop_column_default_clause> drop_column_default_clause = "drop column default clause";

		// 11.15 <drop column definition>
		const rule<class drop_column_definition> drop_column_definition = "drop column definition";

		// 11.16 <add table constraint definition>
		const rule<class add_table_constraint_definition> add_table_constraint_definition = "add table constraint definition";

		// 11.17 <drop table constraint definition>
		const rule<class drop_table_constraint_definition> drop_table_constraint_definition = "drop table constraint definition";

		// 11.18 <drop table statement>
		const rule<class drop_table_statement> drop_table_statement = "drop table statement";

		// 11.19 <view definition>
		const rule<class view_definition> view_definition = "view definition";

		// 11.20 <drop view statement>
		const rule<class drop_view_statement> drop_view_statement = "drop view statement";

		// 11.21 <domain definition>
		const rule<class domain_definition> domain_definition = "domain definition";
		const rule<class domain_constraint> domain_constraint = "domain constraint";

		// 11.22 <alter domain statement>
		const rule<class alter_domain_statement> alter_domain_statement = "alter domain statement";

		// 11.23 <set domain default clause>
		const rule<class set_domain_default_clause> set_domain_default_clause = "set domain default clause";

		// 11.24 <drop domain default clause>
		const rule<class drop_domain_default_clause> drop_domain_default_clause = "drop domain default clause";

		// 11.25 <add domain constraint definition>
		const rule<class add_domain_constraint_definition> add_domain_constraint_definition = "add domain constraint definition";

		// 11.26 <drop domain constraint definition>
		const rule<class drop_domain_constraint_definition> drop_domain_constraint_definition = "drop domain constraint definition";

		// 11.27 <drop domain statement>
		const rule<class drop_domain_statement> drop_domain_statement = "drop domain statement";

		// 11.28 <character set definition>
		const rule<class character_set_definition> character_set_definition = "character set definition";

		// 11.29 <drop character set statement>
		const rule<class drop_character_set_statement> drop_character_set_statement = "drop character set statement";

		// 11.30 <collation definition>
		const rule<class collation_definition> collation_definition = "collation definition";
		const rule<class collation_source> collation_source = "collation source";

		// 11.31 <drop collation statement>
		const rule<class drop_collation_statement> drop_collation_statement = "drop collation statement";

		// 11.32 <translation definition>
		const rule<class translation_definition> translation_definition = "translation definition";

		// 11.33 <drop translation statement>
		const rule<class drop_translation_statement> drop_translation_statement = "drop translation statement";

		// 11.34 <assertion definition>
		const rule<class assertion_definition> assertion_definition = "assertion definition";

		// 11.35 <drop assertion statement>
		const rule<class drop_assertion_statement> drop_assertion_statement = "drop assertion statement";

		// 11.36 <grant statement>
		const rule<class grant_statement> grant_statement = "grant statement";
		const rule<class object_name> object_name = "object name";

		// 11.37 <revoke statement>
		const rule<class revoke_statement> revoke_statement = "revoke statement";

		// 12.1 <module>
		const rule<class module> module = "module";
		const rule<class module_authorization_clause> module_authorization_clause = "module authorization clause";
		const rule<class module_authorization_identifier> module_authorization_identifier = "module authorization identifier";

		// 12.2 <module name clause>
		const rule<class module_name_clause> module_name_clause = "module name clause";
		const rule<class module_character_set_specification> module_character_set_specification = "module character set specification";

		// 12.3 <procedure>
		const rule<class procedure> procedure = "procedure";
		const rule<class parameter_declaration> parameter_declaration = "parameter declaration";

		// 12.5 <SQL procedure statement>
		const rule<class SQL_procedure_statement> SQL_procedure_statement = "L procedure statement";
		const rule<class SQL_schema_statement> SQL_schema_statement = "L schema statement";
		const rule<class SQL_connection_statement> SQL_connection_statement = "L connection statement";
		const rule<class SQL_dynamic_data_statement> SQL_dynamic_data_statement = "L dynamic data statement";
		const rule<class SQL_diagnostics_statement> SQL_diagnostics_statement = "L diagnostics statement";

		// 13.1 <declare cursor>
		const rule<class declare_cursor> declare_cursor = "declare cursor";
		const rule<class cursor_specification> cursor_specification = "cursor specification";
		const rule<class order_by_clause> order_by_clause = "order by clause";

		// 13.2 <open statement>
		const rule<class open_statement> open_statement = "open statement";

		// 13.3 <fetch statement>
		const rule<class fetch_statement> fetch_statement = "fetch statement";

		// 13.4 <close statement>
		const rule<class close_statement> close_statement = "close statement";

		// 13.5 <select statement single row>
		const rule<class select_statement_single_row> select_statement_single_row = "select statement: single row";

		// 13.6 <delete statement positioned>
		const rule<class delete_statement_positioned> delete_statement_positioned = "delete statement: positioned";

		// 13.7 <delete statement searched>
		const rule<class delete_statement_searched> delete_statement_searched = "delete statement: searched";

		// 13.8 <insert statement>
		const rule<class insert_statement> insert_statement = "insert statement";
		const rule<class insert_column_list> insert_column_list = "insert column list";

		// 13.9 <update statement: positioned>
		const rule<class update_statement_positioned> update_statement_positioned = "update statement: positioned";

		// 13.10 <update statement: searched>
		const rule<class update_statement_searched> update_statement_searched = "update statement: searched";

		// 13.11 <temporary table declaration>
		const rule<class temporary_table_declaration> temporary_table_declaration = "temporary table declaration";

		// 14.1 <set transaction statement>
		const rule<class set_transaction_statement> set_transaction_statement = "set transaction statement";

		// 14.2 <set constraints mode statement>
		const rule<class set_constraints_mode_statement> set_constraints_mode_statement = "set constraints mode statement";

		// 14.3 <commit statement>
		const rule<class commit_statement> commit_statement = "commit statement";

		// 14.4 <rollback statement>
		const rule<class rollback_statement> rollback_statement = "rollback statement";

		// 15.1 <connect statement>
		const rule<class connect_statement> connect_statement = "connect statement";

		// 15.2 <set connection statement>
		const rule<class set_connection_statement> set_connection_statement = "set connection statement";

		// 15.3 <disconnect statement>
		const rule<class disconnect_statement> disconnect_statement = "disconnect statement";

		// 16.1 <set catalog statement>
		const rule<class set_catalog_statement> set_catalog_statement = "set catalog statement";

		// 16.2 <set schema statement>
		const rule<class set_schema_statement> set_schema_statement = "set schema statement";

		// 16.3 <set names statement>
		const rule<class set_names_statement> set_names_statement = "set names statement";

		// 16.4 <set session authorization identifier statement>
		const rule<class set_session_authorization_identifier_statement> set_session_authorization_identifier_statement = "set session authorization identifier statement";

		// 16.5 <set local time zone statement>
		const rule<class set_local_time_zone_statement> set_local_time_zone_statement = "set local time zone statement";

		// 17.2 <allocate descriptor statement>
		const rule<class allocate_descriptor_statement> allocate_descriptor_statement = "allocate descriptor statement";

		// 17.3 <deallocate descriptor statement>
		const rule<class deallocate_descriptor_statement> deallocate_descriptor_statement = "deallocate descriptor statement";

		// 17.4 <get descriptor statement>
		const rule<class get_descriptor_statement> get_descriptor_statement = "get descriptor statement";

		// 17.5 <set descriptor statement>
		const rule<class set_descriptor_statement> set_descriptor_statement = "set descriptor statement";
		const rule<class item_number> item_number = "item number";

		// 17.6 <prepare statement>
		const rule<class prepare_statement> prepare_statement = "prepare statement";
		const rule<class SQL_statement_variable> SQL_statement_variable = "L statement variable";
		const rule<class preparable_statement> preparable_statement = "preparable statement";
		const rule<class dynamic_select_statement> dynamic_select_statement = "dynamic select statement";
		const rule<class dynamic_single_row_select_statement> dynamic_single_row_select_statement = "dynamic single row select statement";
		const rule<class preparable_implementation_defined_statement> preparable_implementation_defined_statement = "preparable implementation-defined statement";

		// 17.7 <deallocate prepared statement>
		const rule<class deallocate_prepared_statement> deallocate_prepared_statement = "deallocate prepared statement";

		// 17.8 <describe statement>
		const rule<class describe_statement> describe_statement = "describe statement";
		const rule<class describe_input_statement> describe_input_statement = "describe input statement";
		const rule<class describe_output_statement> describe_output_statement = "describe output statement";

		// 17.9 <using clause>
		const rule<class using_clause> using_clause = "using clause";
		const rule<class using_descriptor> using_descriptor = "using descriptor";

		// 17.10 <execute statement>
		const rule<class execute_statement> execute_statement = "execute statement";
		const rule<class result_using_clause> result_using_clause = "result using clause";
		const rule<class parameter_using_clause> parameter_using_clause = "parameter using clause";

		// 17.11 <execute immediate statement>
		const rule<class execute_immediate_statement> execute_immediate_statement = "execute immediate statement";

		// 17.12 <dynamic declare cursor>
		const rule<class dynamic_declare_cursor> dynamic_declare_cursor = "dynamic declare cursor";

		// 17.13 <allocate cursor statement>
		const rule<class allocate_cursor_statement> allocate_cursor_statement = "allocate cursor statement";

		// 17.14 <dynamic open statement>
		const rule<class dynamic_open_statement> dynamic_open_statement = "dynamic open statement";

		// 17.15 <dynamic fetch statement>
		const rule<class dynamic_fetch_statement> dynamic_fetch_statement = "dynamic fetch statement";

		// 17.16 <dynamic close statement>
		const rule<class dynamic_close_statement> dynamic_close_statement = "dynamic close statement";

		// 17.17 <dynamic delete statement: positioned>
		const rule<class dynamic_delete_statement_positioned> dynamic_delete_statement_positioned = "dynamic delete statement: positioned";

		// 17.18 <dynamic update statement: positioned>
		const rule<class dynamic_update_statement_positioned> dynamic_update_statement_positioned = "dynamic update statement: positioned";

		// 17.19 <preparable dynamic delete statement: positioned>
		const rule<class preparable_dynamic_delete_statement_positioned> preparable_dynamic_delete_statement_positioned = "preparable dynamic delete statement: positioned";

		// 17.20 <preparable dynamic update statement: positioned>
		const rule<class preparable_dynamic_update_statement_positioned> preparable_dynamic_update_statement_positioned = "preparable dynamic update statement: positioned";

		// 18.1 <get diagnostics statement>
		const rule<class get_diagnostics_statement> get_diagnostics_statement = "get diagnostics statement";

		// 19.1 <embedded SQL host program>
		const rule<class embedded_SQL_host_program> embedded_SQL_host_program = "embedded SQL host program";
		const rule<class embedded_SQL_statement> embedded_SQL_statement = "embedded SQL statement";
		const rule<class embedded_SQL_declare_section> embedded_SQL_declare_section = "embedded SQL declare section";
		const rule<class embedded_SQL_begin_declare> embedded_SQL_begin_declare = "embedded SQL begin declare";
		const rule<class embedded_variable_name> embedded_variable_name = "embedded variable name";

		// 19.2 <embedded exception declaration>
		const rule<class embedded_exception_declaration> embedded_exception_declaration = "embedded exception declaration";

		// 19.3 <embedded SQL Ada program>
		const rule<class embedded_SQL_Ada_program> embedded_SQL_Ada_program = "embedded SQL Ada program";
		const rule<class Ada_variable_definition> Ada_variable_definition = "Ada variable definition";
		const rule<class Ada_host_identifier> Ada_host_identifier = "Ada host identifier";

		// 19.4 <embedded SQL C program>
		const rule<class embedded_SQL_C_program> embedded_SQL_C_program = "embedded SQL C program";
		const rule<class C_variable_definition> C_variable_definition = "C variable definition";
		const rule<class C_host_identifier> C_host_identifier = "C host identifier";

		// 19.5 <embedded SQL COBOL program>
		const rule<class embedded_SQL_COBOL_program> embedded_SQL_COBOL_program = "embedded SQL L program";
		const rule<class COBOL_variable_definition> COBOL_variable_definition = "COBOL variable definition";
		const rule<class COBOL_host_identifier> COBOL_host_identifier = "COBOL host identifier";

		// 19.6 <embedded SQL Fortran program>
		const rule<class embedded_SQL_Fortran_program> embedded_SQL_Fortran_program = "embedded SQL Fortran program";
		const rule<class Fortran_variable_definition> Fortran_variable_definition = "Fortran variable definition";
		const rule<class Fortran_host_identifier> Fortran_host_identifier = "Fortran host identifier";

		// 19.7 <embedded SQL MUMPS program>
		const rule<class embedded_SQL_MUMPS_program> embedded_SQL_MUMPS_program = "embedded SQL MUMPS program";
		const rule<class MUMPS_variable_definition> MUMPS_variable_definition = "MUMPS variable definition";
		const rule<class MUMPS_host_identifier> MUMPS_host_identifier = "MUMPS host identifier";

		// 19.8 <embedded SQL Pascal program>
		const rule<class embedded_SQL_Pascal_program> embedded_SQL_Pascal_program = "embedded SQL Pascal program";
		const rule<class Pascal_variable_definition> Pascal_variable_definition = "Pascal variable definition";
		const rule<class Pascal_host_identifier> Pascal_host_identifier = "Pascal host identifier";

		// 19.9 <embedded SQL PL/I program>
		const rule<class embedded_SQL_PLI_program> embedded_SQL_PLI_program = "embedded SQL PL/I program";
		const rule<class PLI_variable_definition> PLI_variable_definition = "PL/I variable definition";
		const rule<class PLI_host_identifier> PLI_host_identifier = "PL/I host identifier";

		// 20.1 <direct SQL statement>
		const rule<class direct_SQL_statement> direct_SQL_statement = "direct SQL statement";

		// 20.2 <direct select statement: multiple rows>
		const rule<class direct_select_statement_multiple_rows> direct_select_statement_multiple_rows = "direct select statement: multiple rows";

		//-----------------------------------------------------------------------------
		// 5 Lexical elements
		//-----------------------------------------------------------------------------

		//-----------------------------------------------------------------------------
		// keywords
		//-----------------------------------------------------------------------------

		inline auto keyword(const char *kwd) {
			return lexeme[no_case[lit(kwd)] >> !char_("0-9a-zA-Z_")];
		};

		//-----------------------------------------------------------------------------
		// left recursion prevention
		//-----------------------------------------------------------------------------

		struct left_recursion_deleter {
			left_recursion_deleter() {
				std::cerr << ">>> RECURSION" << std::endl;
			}

			~left_recursion_deleter() {
				std::cerr << "<<< RECURSION" << std::endl;
			}
		};

		template <typename Subject>
		struct left_recursion_directive : x3::unary_parser<Subject, left_recursion_directive<Subject>> {
			typedef x3::unary_parser<Subject, left_recursion_directive<Subject>> base_type;
			static bool const is_pass_through_unary = true;
			static bool const handles_container = Subject::handles_container;

			left_recursion_directive(const char *name, Subject const& subject): base_type(subject), m_name(name) {}

			template <typename Iterator, typename Context, typename RContext, typename Attribute>
			bool parse(Iterator& first, Iterator const& last, Context const& context, RContext& rcontext, Attribute& attr) const {
				auto& g(get<parser_tag>(context).get());
				Iterator i(g.set_recursion(m_name, first));
				if (i == first) {
					if (false)
						std::cerr << "RECURSION: " << m_name << " detected" << std::endl;
					return false;
				}
				if (false)
					std::cerr << ">>> RECURSION: " << m_name << std::endl;
				bool ok(this->subject.parse(first, last, context, rcontext, attr));
				if (false)
					std::cerr << "<<< RECURSION: " << m_name << std::endl;
				g.set_recursion(m_name, i);
				return ok;
			}

			const char *m_name;
		};

		template <typename Rule, typename Subject>
		inline left_recursion_directive<typename x3::extension::as_parser<Subject>::value_type>
		recursion(Rule const& rule, Subject const& subject) {
			return left_recursion_directive<typename x3::extension::as_parser<Subject>::value_type>(rule.name, x3::as_parser(subject));
		}

		//-----------------------------------------------------------------------------
		// 5.2 <token> and <separator>
		//-----------------------------------------------------------------------------

		const rule<class regular_identifier, std::string> regular_identifier = "regular identifier";
		const rule<class delimited_identifier_part> delimited_identifier_part = "delimited identifier part";

		static const char * const reserved_words[] = {
			"absolute", "action", "add", "all", "allocate", "alter", "and", "any", "are",
			"as", "asc", "assertion", "at", "authorization", "avg",
			"begin", "between", "bit", "bit_length", "both", "by",
			"cascade", "cascaded", "case", "cast", "catalog", "char", "character",
			"char_length", "character_length", "check", "close", "coalesce", "collate",
			"collation", "column", "commit", "connect", "connection", "constraint",
			"constraints", "continue", "convert", "corresponding", "count", "create",
			"cross", "current", "current_date", "current_time", "current_timestamp",
			"current_user", "cursor",
			"date", "day", "deallocate", "dec", "decimal", "declare", "default",
			"deferrable", "deferred", "delete", "desc", "describe", "descriptor",
			"diagnostics", "disconnect", "distinct", "domain", "double", "drop",
			"else", "end", "end-exec", "escape", "except", "exception", "exec",
			"execute", "exists", "external", "extract",
			"false", "fetch", "first", "float", "for", "foreign", "found", "from", "full",
			"get", "global", "go", "goto", "grant", "group",
			"having", "hour",
			"identity", "immediate", "in", "indicator", "initially", "inner", "input",
			"insensitive", "insert", "int", "integer", "intersect", "interval", "into",
			"is", "isolation",
			"join",
			"key",
			"language", "last", "leading", "left", "level", "like", "local", "lower",
			"match", "max", "min", "minute", "module", "month",
			"names", "national", "natural", "nchar", "next", "no", "not", "null",
			"nullif", "numeric",
			"octet_length", "of", "on", "only", "open", "option", "or", "order",
			"outer", "output", "overlaps",
			"pad", "partial", "position", "precision", "prepare", "preserve", "primary",
			"prior", "privileges", "procedure", "public",
			"read", "real", "references", "relative", "restrict", "revoke", "right",
			"rollback", "rows",
			"schema", "scroll", "second", "section", "select", "session", "session_user",
			"set", "size", "smallint", "some", "space", "sql", "sqlcode", "sqlerror",
			"sqlstate", "substring", "sum", "system_user",
			"table", "temporary", "then", "time", "timestamp", "timezone_hour",
			"timezone_minute", "to", "trailing", "transaction", "translate",
			"translation", "trim", "true",
			"union", "unique", "unknown", "update", "upper", "usage", "user", "using",
			"value", "values", "varchar", "varying", "view",
			"when", "whenever", "where", "with", "work", "write",
			"year",
			"zone"
		};

		struct reserved_words_compare {
			bool operator()(const char *a, const char *b) const {
				if (!b)
					return false;
				if (!a)
					return true;
				return strcmp(a, b) < 0;
			}
		};

		struct regular_identifier_parser : boost::spirit::x3::parser<regular_identifier_parser> {
			typedef std::string attribute_type;
			static constexpr bool has_attribute = true;
			template <typename Iterator, typename Context, typename Attribute>
			bool parse(Iterator& first, Iterator const& last, Context const& context, x3::unused_type, Attribute& attr) const {
				x3::skip_over(first, last, context);
				std::string id;
				Iterator i = first;
				for (; i != last; ++i) {
					char ch(*i);
					if (ch >= 'A' && ch <= 'Z')
						ch += 'a' - 'A';
					if (!((ch >= 'a' && ch <= 'z') ||
					      (!id.empty() && ((ch >= '0' && ch <= '9') || ch == '_'))))
						break;
					id.push_back(ch);
				}
				if (id.empty())
					return false;
				const char * const *reserved_words_end = reserved_words + sizeof(reserved_words)/sizeof(reserved_words[0]);
				const char * const *rw(std::lower_bound(reserved_words, reserved_words_end, id.c_str(), reserved_words_compare()));
				if (rw != reserved_words_end && id == *rw)
					return false;
				if (false)
					x3::traits::move_to(id, attr);
				else
					x3::traits::move_to(first, i, attr);
				first = i;
				std::cerr << "regular_identifier: " << id << std::endl;
				return true;
			}
		};

		const auto regular_identifier_def = regular_identifier_parser{};

		const auto delimited_identifier_part_def = lexeme[+(lit('"') >> *(char_ - lit('"')) >> lit('"'))];
		const auto delimited_identifier_def = +delimited_identifier_part;

		BOOST_SPIRIT_DEFINE(regular_identifier,delimited_identifier_part,delimited_identifier);

		//-----------------------------------------------------------------------------
		// 5.3 <literal>
		//-----------------------------------------------------------------------------

		const rule<class unsigned_literal> unsigned_literal = "unsigned literal";
		const rule<class general_literal> general_literal = "general literal";
		const rule<class bit> bit = "bit";
		const rule<class signed_numeric_literal> signed_numeric_literal = "signed numeric literal";
		const rule<class exact_numeric_literal> exact_numeric_literal = "exact numeric literal";
		const rule<class sign> sign = "sign";
		const rule<class approximate_numeric_literal> approximate_numeric_literal = "approximate numeric literal";
		const rule<class mantissa> mantissa = "mantissa";
		const rule<class exponent> exponent = "exponent";
		const rule<class signed_integer> signed_integer = "signed integer";
		const rule<class unsigned_integer> unsigned_integer = "unsigned integer";
		const rule<class datetime_literal> datetime_literal = "datetime literal";
		const rule<class date_literal> date_literal = "date literal";
		const rule<class time_literal> time_literal = "time literal";
		const rule<class timestamp_literal> timestamp_literal = "timestamp literal";
		const rule<class time_zone_interval> time_zone_interval = "time zone interval";
		const rule<class date_value> date_value = "date value";
		const rule<class time_value> time_value = "time value";
		const rule<class interval_literal> interval_literal = "interval literal";
		const rule<class year_month_literal> year_month_literal = "year-month literal";
		const rule<class day_time_literal> day_time_literal = "day-time literal";
		const rule<class day_time_interval> day_time_interval = "day-time interval";
		const rule<class time_interval> time_interval = "time interval";
		const rule<class years_value> years_value = "years value";
		const rule<class months_value> months_value = "months value";
		const rule<class days_value> days_value = "days value";
		const rule<class hours_value> hours_value = "hours value";
		const rule<class minutes_value> minutes_value = "minutes value";
		const rule<class seconds_value> seconds_value = "seconds value";
		const rule<class datetime_value> datetime_value = "datetime value";

		const auto literal_def =
			signed_numeric_literal
			| general_literal;

		const auto unsigned_literal_def =
			unsigned_numeric_literal
			| general_literal;

		const auto general_literal_def =
			character_string_literal
			| national_character_string_literal
			| bit_string_literal
			| hex_string_literal
			| datetime_literal
			| interval_literal;

		const auto character_string_literal_def =
			-(introducer >> character_set_specification) >>
			+lexeme[+(lit('\'') >> *(char_ - lit('\'')) >> lit('\''))];

		const auto introducer_def = lit('_');

		const auto national_character_string_literal_def =
			lexeme[no_case[lit('n')] >> +(lit('\'') >> +(char_ - lit('\'')) >> lit('\''))];

		const auto bit_string_literal_def =
			lexeme[no_case[lit('b')] >> lit('\'') >> *bit >> lit('\'')] >> *lexeme[lit('\'') >> *bit >> lit('\'')];

		const auto hex_string_literal_def =
			lexeme[no_case[lit('x')] >> lit('\'') >> *xdigit >> lit('\'')] >> *lexeme[lit('\'') >> *xdigit >> lit('\'')];

		const auto bit_def = char_("01");

		const auto signed_numeric_literal_def =	lexeme[-(sign) >> +digit];

		const auto unsigned_numeric_literal_def =
			exact_numeric_literal
			| approximate_numeric_literal;

		const auto exact_numeric_literal_def =
			lexeme[+digit >> -(lit('.') >> (+digit))]
			| lexeme[lit('.') >> +digit];

		const auto sign_def = lit('+') | lit('-');

		const auto approximate_numeric_literal_def = lexeme[mantissa >> no_case[lit('e')] >> exponent];

		const auto mantissa_def = exact_numeric_literal;

		const auto exponent_def = signed_integer;

		const auto signed_integer_def = lexeme[-(sign) >> +digit];

		const auto unsigned_integer_def = +digit;

		const auto datetime_literal_def =
			date_literal
			| time_literal
			| timestamp_literal;

		const auto date_literal_def =
			keyword("date") >> date_string;

		const auto time_literal_def =
			keyword("time") >> time_string;

		const auto timestamp_literal_def =
			keyword("timestamp") >> timestamp_string;

		const auto date_string_def =
			lexeme[lit('\'') >> date_value >> lit('\'')];

		const auto time_string_def =
			lexeme[lit('\'') >> time_value >> -(time_zone_interval) >> lit('\'')];

		const auto timestamp_string_def =
			lexeme[lit('\'') >> date_value >> space >> time_value >> -(time_zone_interval) >> lit('\'')];

		const auto time_zone_interval_def =
			no_skip[sign >> hours_value >> lit(':') >> minutes_value];

		const auto date_value_def =
			no_skip[years_value >> lit('-') >> months_value >> lit('-') >> days_value];

		const auto time_value_def =
			no_skip[hours_value >> lit(':') >> minutes_value >> lit(':') >> seconds_value];

		const auto interval_literal_def =
			keyword("interval") >> -(sign) >> interval_string >> interval_qualifier;

		const auto interval_string_def =
			lexeme[lit('\'') >> (year_month_literal | day_time_literal) >> lit('\'')];

		const auto year_month_literal_def =
			no_skip[years_value]
			| no_skip[-(years_value >> lit('-')) >> months_value];

		const auto day_time_literal_def =
			no_skip[day_time_interval]
			| no_skip[time_interval];

		const auto day_time_interval_def =
			no_skip[days_value >> -(+space >> hours_value >> -(lit(':') >> minutes_value >> -(lit(':') >> seconds_value)))];

		const auto time_interval_def =
			no_skip[hours_value >> -(lit(':') >> minutes_value >> -(lit(':') >> seconds_value))];

		const auto years_value_def = no_skip[datetime_value];

		const auto months_value_def = no_skip[datetime_value];

		const auto days_value_def = no_skip[datetime_value];

		const auto hours_value_def = no_skip[datetime_value];

		const auto minutes_value_def = no_skip[datetime_value];

		const auto seconds_value_def =
			no_skip[+digit >> -(lit('.') >> *digit)];

		const auto datetime_value_def = no_skip[unsigned_integer];

		BOOST_SPIRIT_DEFINE(literal, unsigned_literal, general_literal, character_string_literal,
				    introducer, national_character_string_literal, bit_string_literal,
				    hex_string_literal, bit, signed_numeric_literal, unsigned_numeric_literal,
				    exact_numeric_literal, sign, approximate_numeric_literal, mantissa,
				    exponent, signed_integer, unsigned_integer, datetime_literal,
				    date_literal, time_literal, timestamp_literal, date_string, time_string,
				    timestamp_string, time_zone_interval, date_value, time_value,
				    interval_literal, interval_string, year_month_literal, day_time_literal,
				    day_time_interval, time_interval, years_value, months_value, days_value,
				    hours_value, minutes_value, seconds_value, datetime_value);

		//-----------------------------------------------------------------------------
		// 5.4 Names and identifiers
		//-----------------------------------------------------------------------------

		const rule<class actual_identifier> actual_identifier = "actual identifier";
		const rule<class SQL_language_identifier> SQL_language_identifier = "L language identifier";
		const rule<class qualified_local_table_name> qualified_local_table_name = "qualified local table name";
		const rule<class local_table_name> local_table_name = "local table name";
		const rule<class unqualified_schema_name> unqualified_schema_name = "unqualified schema name";
		const rule<class qualified_identifier> qualified_identifier = "qualified identifier";
		const rule<class correlation_name> correlation_name = "correlation name";
		const rule<class dynamic_cursor_name> dynamic_cursor_name = "dynamic cursor name";
		const rule<class scope_option> scope_option = "scope option";
		const rule<class constraint_name> constraint_name = "constraint name";
		const rule<class character_set_name> character_set_name = "character set name";
		const rule<class form_of_use_conversion_name> form_of_use_conversion_name = "form-of-use conversion name";

		const auto identifier_def = -(introducer >> character_set_specification) >> actual_identifier;

		const auto actual_identifier_def =
			regular_identifier
			| delimited_identifier;

		const auto SQL_language_identifier_def = alpha >> *alnum;

		const auto authorization_identifier_def = identifier;

		const auto table_name_def =
			qualified_name
			| qualified_local_table_name;

		const auto qualified_local_table_name_def =
			keyword("module") >> lit('.') >> local_table_name;

		const auto local_table_name_def = qualified_identifier;

		const auto domain_name_def = qualified_name;

		const auto schema_name_def =
			-(catalog_name >> lit('.')) >> unqualified_schema_name;

		const auto unqualified_schema_name_def = identifier;

		const auto catalog_name_def = identifier;

		const auto qualified_name_def =
			-(schema_name >> lit('.')) >> qualified_identifier;

		const auto qualified_identifier_def = identifier;

		const auto column_name_def = identifier;

		const auto correlation_name_def = identifier;

		const auto module_name_def = identifier;

		const auto cursor_name_def = identifier;

		const auto procedure_name_def = identifier;

		const auto statement_name_def = identifier;

		const auto extended_statement_name_def =
			-(scope_option) >> simple_value_specification;

		const auto dynamic_cursor_name_def =
			cursor_name
			| extended_cursor_name;

		const auto extended_cursor_name_def =
			-(scope_option) >> simple_value_specification;

		const auto descriptor_name_def =
			-(scope_option) >> simple_value_specification;

		const auto scope_option_def = keyword("global")	| keyword("local");

		const auto parameter_name_def = lit(':') >> identifier;

		const auto constraint_name_def = qualified_name;

		const auto collation_name_def = qualified_name;

		const auto character_set_name_def = -(schema_name >> lit('.')) >> SQL_language_identifier;

		const auto translation_name_def = qualified_name;

		const auto form_of_use_conversion_name_def = qualified_name;

		const auto connection_name_def = simple_value_specification;

		const auto SQL_server_name_def = simple_value_specification;

		const auto user_name_def = simple_value_specification;

		BOOST_SPIRIT_DEFINE(identifier, actual_identifier, SQL_language_identifier,
				    authorization_identifier, table_name, qualified_local_table_name,
				    local_table_name, domain_name, schema_name, unqualified_schema_name,
				    catalog_name, qualified_name, qualified_identifier, column_name,
				    correlation_name, module_name, cursor_name, procedure_name, statement_name,
				    extended_statement_name, dynamic_cursor_name, extended_cursor_name,
				    descriptor_name, scope_option, parameter_name, constraint_name,
				    collation_name, character_set_name, translation_name,
				    form_of_use_conversion_name, connection_name,
				    SQL_server_name, user_name);

		//-----------------------------------------------------------------------------
		// 6 Scalar expressions
		//-----------------------------------------------------------------------------

		//-----------------------------------------------------------------------------
		// 6.1 <data type>
		//-----------------------------------------------------------------------------

		const rule<class character_string_type> character_string_type = "character string type";
		const rule<class national_character_string_type> national_character_string_type = "national character string type";
		const rule<class bit_string_type> bit_string_type = "bit string type";
		const rule<class numeric_type> numeric_type = "numeric type";
		const rule<class datetime_type> datetime_type = "datetime type";
		const rule<class time_precision> time_precision = "time precision";
		const rule<class timestamp_precision> timestamp_precision = "timestamp precision";
		const rule<class interval_type> interval_type = "interval type";

		const auto data_type_def =
			(character_string_type >> -(keyword("character") >> keyword("set") >> character_set_specification))
			| national_character_string_type
			| bit_string_type
			| numeric_type
			| datetime_type
			| interval_type;

		const auto character_string_type_def =
			(keyword("character") >> -(lit('(') >> length >> lit(')')))
			| (keyword("char") >> -(lit('(') >> length >> lit(')')))
			| (keyword("character") >> keyword("varying") >> lit('(') >> length >> lit(')'))
			| (keyword("char") >> keyword("varying") >> lit('(') >> length >> lit(')'))
			| (keyword("varchar") >> lit('(') >> length >> lit(')'));

		const auto national_character_string_type_def =
			(keyword("national") >> keyword("character") >> -(lit('(') >> length >> lit(')')))
			| (keyword("national") >> keyword("char") >> -(lit('(') >> length >> lit(')')))
			| (keyword("nchar") >> -(lit('(') >> length >> lit(')')))
			| (keyword("national") >> keyword("character") >> keyword("varying") >> lit('(') >> length >> lit(')'))

			| (keyword("national") >> keyword("char") >> keyword("varying") >> lit('(') >> length >> lit(')'))
			| (keyword("nchar") >> keyword("varying") >> lit('(') >> length >> lit(')'));

		const auto bit_string_type_def =
			(keyword("bit") >> -(lit('(') >> length >> lit(')')))
			| (keyword("bit") >> keyword("varying") >> lit('(') >> length >> lit(')'));

		const auto numeric_type_def =
			exact_numeric_type
			| approximate_numeric_type;

		const auto exact_numeric_type_def =
			(keyword("numeric") >> -(lit('(') >> precision >> -(lit(',') >> scale) >> lit(')')))
			| (keyword("decimal") >> -(lit('(') >> precision >> -(lit(',') >> scale) >> lit(')')))
			| (keyword("dec") >> -(lit('(') >> precision >> -(lit(',') >> scale) >> lit(')')))
			| keyword("integer")
			| keyword("int")
			| keyword("smallint");

		const auto approximate_numeric_type_def =
			(keyword("float") >> -(lit('(') >> precision >> lit(')')))
			| keyword("real")
			| (keyword("double") >> keyword("precision"));

		const auto length_def = unsigned_integer;

		const auto precision_def = unsigned_integer;

		const auto scale_def = unsigned_integer;

		const auto datetime_type_def =
			keyword("date")
			| (keyword("time") >> -(lit('(') >> time_precision >> lit(')')) >>
			   -(keyword("with") >> keyword("time") >> keyword("zone")))
			| (keyword("timestamp") >> -(lit('(') >> timestamp_precision >> lit(')')) >>
			   -(keyword("with") >> keyword("time") >> keyword("zone")));

		const auto time_precision_def = time_fractional_seconds_precision;

		const auto timestamp_precision_def = time_fractional_seconds_precision;

		const auto time_fractional_seconds_precision_def = unsigned_integer;

		const auto interval_type_def = keyword("interval") >> interval_qualifier;

		BOOST_SPIRIT_DEFINE(data_type, character_string_type, national_character_string_type,
				    bit_string_type, numeric_type, exact_numeric_type,
				    approximate_numeric_type, length, precision, scale, datetime_type,
				    time_precision, timestamp_precision, time_fractional_seconds_precision,
				    interval_type);

		//-----------------------------------------------------------------------------
		// 6.2 <value specification> and <target specification>
		//-----------------------------------------------------------------------------

		const rule<class unsigned_value_specification> unsigned_value_specification = "unsigned value specification";
		const rule<class general_value_specification> general_value_specification = "general value specification";
		const rule<class simple_target_specification> simple_target_specification = "simple target specification";
		const rule<class parameter_specification> parameter_specification = "parameter specification";
		const rule<class indicator_parameter> indicator_parameter = "indicator parameter";
		const rule<class dynamic_parameter_specification> dynamic_parameter_specification = "dynamic parameter specification";
		const rule<class variable_specification> variable_specification = "variable specification";
		const rule<class indicator_variable> indicator_variable = "indicator variable";

		const auto value_specification_def =
			literal
			| general_value_specification;

		const auto unsigned_value_specification_def =
			unsigned_literal
			| general_value_specification;

		const auto general_value_specification_def =
			parameter_specification
			| dynamic_parameter_specification
			| variable_specification
			| keyword("user")
			| keyword("current_user")
			| keyword("session_user")
			| keyword("system_user")
			| keyword("value");

		const auto simple_value_specification_def =
			parameter_name
			| embedded_variable_name
			| literal;

		const auto target_specification_def =
			parameter_specification
			| variable_specification;

		const auto simple_target_specification_def =
			parameter_name
			| embedded_variable_name;

		const auto parameter_specification_def =
			parameter_name >> -(indicator_parameter);

		const auto indicator_parameter_def =
			-(keyword("indicator")) >> parameter_name;

		const auto dynamic_parameter_specification_def = lit('?');

		const auto variable_specification_def =
			embedded_variable_name >> -(indicator_variable);

		const auto indicator_variable_def =
			-(keyword("indicator")) >> embedded_variable_name;

		BOOST_SPIRIT_DEFINE(value_specification, unsigned_value_specification,
				    general_value_specification, simple_value_specification,
				    target_specification, simple_target_specification,
				    parameter_specification, indicator_parameter,
				    dynamic_parameter_specification, variable_specification,
				    indicator_variable);

		//-----------------------------------------------------------------------------
		// 6.3 <table reference>
		//-----------------------------------------------------------------------------

		const rule<class derived_table> derived_table = "derived table";
		const rule<class derived_column_list> derived_column_list = "derived column list";

		const auto table_reference_def =
			(table_name >> -(-(keyword("as")) >> correlation_name >>
					 -(lit('(') >> derived_column_list >> lit(')'))))
			| (derived_table >> -(keyword("as")) >> correlation_name >>
			   -(lit('(') >> derived_column_list >> lit(')')))
			| joined_table;

		const auto derived_table_def = table_subquery;

		const auto derived_column_list_def = column_name_list;

		const auto column_name_list_def = column_name % lit(',');

		BOOST_SPIRIT_DEFINE(table_reference, derived_table, derived_column_list, column_name_list);

		//-----------------------------------------------------------------------------
		// 6.4 <column reference>
		//-----------------------------------------------------------------------------

		const rule<class qualifier> qualifier = "qualifier";

		const auto column_reference_def = -(qualifier >> lit('.')) >> column_name;

		const auto qualifier_def =
			table_name
			| correlation_name;

		BOOST_SPIRIT_DEFINE(column_reference, qualifier);

		//-----------------------------------------------------------------------------
		// 6.5 <set function specification>
		//-----------------------------------------------------------------------------

		const rule<class general_set_function> general_set_function = "general set function";
		const rule<class set_function_type> set_function_type = "set function type";
		const rule<class set_quantifier> set_quantifier = "set quantifier";

		const auto set_function_specification_def =
			(keyword("count") >> lit('(') >> lit('*') >> lit(')'))
			| general_set_function;

		const auto general_set_function_def =
			set_function_type >>
			lit('(') >> -(set_quantifier) >> value_expression >> lit(')');

		const auto set_function_type_def =
			keyword("avg") | keyword("max") | keyword("min") | keyword("sum") | keyword("count");

		const auto set_quantifier_def = keyword("distinct") | keyword("all");

		BOOST_SPIRIT_DEFINE(set_function_specification, general_set_function, set_function_type, set_quantifier);

		//-----------------------------------------------------------------------------
		// 6.6 <numeric value function>
		//-----------------------------------------------------------------------------

		const rule<class char_length_expression> char_length_expression = "char length expression";
		const rule<class octet_length_expression> octet_length_expression = "octet length expression";
		const rule<class bit_length_expression> bit_length_expression = "bit length expression";
		const rule<class extract_field> extract_field = "extract field";
		const rule<class time_zone_field> time_zone_field = "time zone field";
		const rule<class extract_source> extract_source = "extract source";

		const auto numeric_value_function_def =
			position_expression
			| extract_expression
			| length_expression;

		const auto position_expression_def =
			keyword("position") >> lit('(') >> character_value_expression >>
			keyword("in") >> character_value_expression >> lit(')');

		const auto length_expression_def =
			char_length_expression
			| octet_length_expression
			| bit_length_expression;

		const auto char_length_expression_def =
			(keyword("char_length") | keyword("character_length")) >>
			lit('(') >> string_value_expression >> lit(')');

		const auto octet_length_expression_def =
			keyword("octet_length") >> lit('(') >> string_value_expression >> lit(')');


		const auto bit_length_expression_def =
			keyword("bit_length") >> lit('(') >> string_value_expression >> lit(')');

		const auto extract_expression_def =
			keyword("extract") >> lit('(') >> extract_field >>
			keyword("from") >> extract_source >> lit(')');

		const auto extract_field_def =
			datetime_field
			| time_zone_field;

		const auto time_zone_field_def =
			keyword("timezone_hour")
			| keyword("timezone_minute");

		const auto extract_source_def =
			datetime_value_expression
			| interval_value_expression;

		BOOST_SPIRIT_DEFINE(numeric_value_function, position_expression, length_expression,
				    char_length_expression, octet_length_expression, bit_length_expression,
				    extract_expression, extract_field, time_zone_field, extract_source);

		//-----------------------------------------------------------------------------
		// 6.7 <string value function>
		//-----------------------------------------------------------------------------

		const rule<class character_value_function> character_value_function = "character value function";
		const rule<class trim_operands> trim_operands = "trim operands";
		const rule<class trim_source> trim_source = "trim source";
		const rule<class trim_specification> trim_specification = "trim specification";
		const rule<class trim_character> trim_character = "trim character";
		const rule<class bit_value_function> bit_value_function = "bit value function";
		const rule<class start_position> start_position = "start position";
		const rule<class string_length> string_length = "string length";

		const auto string_value_function_def =
			character_value_function
			| bit_value_function;

		const auto character_value_function_def =
			character_substring_function
			| fold
			| form_of_use_conversion
			| character_translation
			| trim_function;

		const auto character_substring_function_def =
			keyword("substring") >> lit('(') >> character_value_expression >> keyword("from") >> start_position >>
			-(keyword("for") >> string_length) >> lit(')');

		const auto fold_def = (keyword("upper") | keyword("lower")) >> lit('(') >> character_value_expression >> lit(')');

		const auto form_of_use_conversion_def =
			keyword("convert") >> lit('(') >> character_value_expression >>
			keyword("using") >> form_of_use_conversion_name >> lit(')');

		const auto character_translation_def =
			keyword("translate") >> lit('(') >> character_value_expression >>
			keyword("using") >> translation_name >> lit(')');

		const auto trim_function_def =
			keyword("trim") >> lit('(') >> trim_operands >> lit(')');

		const auto trim_operands_def =
			-(-(trim_specification) >> -(trim_character) >> keyword("from")) >> trim_source;

		const auto trim_source_def = character_value_expression;

		const auto trim_specification_def =
			keyword("leading")
			| keyword("trailing")
			| keyword("both");

		const auto trim_character_def = character_value_expression;

		const auto bit_value_function_def =
			bit_substring_function;

		const auto bit_substring_function_def =
			keyword("substring") >> lit('(') >> bit_value_expression >>
			keyword("from") >> start_position >>
			-(keyword("for") >> string_length) >> lit(')');

		const auto start_position_def = numeric_value_expression;

		const auto string_length_def = numeric_value_expression;

		BOOST_SPIRIT_DEFINE(string_value_function, character_value_function,
				    character_substring_function, fold, form_of_use_conversion,
				    character_translation, trim_function, trim_operands,
				    trim_source, trim_specification, trim_character,
				    bit_value_function, bit_substring_function, start_position,
				    string_length);

		//-----------------------------------------------------------------------------
		// 6.8 <datetime value function>
		//-----------------------------------------------------------------------------

		const rule<class current_date_value_function> current_date_value_function = "current date value function";
		const rule<class current_time_value_function> current_time_value_function = "current time value function";
		const rule<class current_timestamp_value_function> current_timestamp_value_function = "current timestamp value function";

		const auto datetime_value_function_def =
			current_date_value_function
			| current_time_value_function
			| current_timestamp_value_function;

		const auto current_date_value_function_def = keyword("current_date");

		const auto current_time_value_function_def =
			keyword("current_time") >> -(lit('(') >> time_precision >> lit(')'));

		const auto current_timestamp_value_function_def =
			keyword("current_timestamp") >> -(lit('(') >> timestamp_precision >> lit(')'));

		BOOST_SPIRIT_DEFINE(datetime_value_function, current_date_value_function,
				    current_time_value_function, current_timestamp_value_function);

		//-----------------------------------------------------------------------------
		// 6.9 <case expression>
		//-----------------------------------------------------------------------------

		const rule<class case_abbreviation> case_abbreviation = "case abbreviation";
		const rule<class case_specification> case_specification = "case specification";
		const rule<class simple_case> simple_case = "simple case";
		const rule<class searched_case> searched_case = "searched case";
		const rule<class simple_when_clause> simple_when_clause = "simple when clause";
		const rule<class searched_when_clause> searched_when_clause = "searched when clause";
		const rule<class else_clause> else_clause = "else clause";
		const rule<class case_operand> case_operand = "case operand";
		const rule<class when_operand> when_operand = "when operand";
		const rule<class result> result = "result";
		const rule<class result_expression> result_expression = "result expression";

		const auto case_expression_def =
			case_abbreviation
			| case_specification;

		const auto case_abbreviation_def =
			(keyword("nullif") >> lit('(') >> value_expression >> lit(',') >> value_expression >> lit(')'))
			| (keyword("coalesce") >> lit('(') >> (value_expression % lit(',')) >> lit(')'));

		const auto case_specification_def =
			simple_case
			| searched_case;

		const auto simple_case_def =
			keyword("case") >> case_operand >>
			+simple_when_clause >> -(else_clause) >> keyword("end");

		const auto searched_case_def =
			keyword("case") >> +searched_when_clause >> -(else_clause) >> keyword("end");

		const auto simple_when_clause_def = keyword("when") >> when_operand >> keyword("then") >> result;

		const auto searched_when_clause_def = keyword("when") >> search_condition >> keyword("then") >> result;

		const auto else_clause_def = keyword("else") >> result;

		const auto case_operand_def = value_expression;

		const auto when_operand_def = value_expression;

		const auto result_def = result_expression | keyword("null");

		const auto result_expression_def = value_expression;

		BOOST_SPIRIT_DEFINE(case_expression, case_abbreviation, case_specification, simple_case,
				    searched_case, simple_when_clause, searched_when_clause, else_clause,
				    case_operand, when_operand, result, result_expression);

		//-----------------------------------------------------------------------------
		// 6.10 <cast specification>
		//-----------------------------------------------------------------------------

		const rule<class cast_operand> cast_operand = "cast operand";
		const rule<class cast_target> cast_target = "cast target";

		const auto cast_specification_def =
			keyword("cast") >> lit('(') >> cast_operand >> keyword("as") >> cast_target >> lit(')');

		const auto cast_operand_def =
			value_expression
			| keyword("null");

		const auto cast_target_def =
			domain_name
			| data_type;

		BOOST_SPIRIT_DEFINE(cast_specification, cast_operand, cast_target);

		//-----------------------------------------------------------------------------
		// 6.11 <value expression>
		//-----------------------------------------------------------------------------

		const rule<class value_expression_primary> value_expression_primary = "value expression primary";
		const rule<class mapnik_variable> mapnik_variable = "mapnik variable";

		const auto value_expression_def =
			recursion(value_expression,
				  (numeric_value_expression
				   | string_value_expression
				   | datetime_value_expression
				   | interval_value_expression) >>
				  -(lexeme[lit("::")] >> data_type));

		const auto value_expression_primary_def =
			unsigned_value_specification
			| column_reference
			| set_function_specification
			| scalar_subquery
			| case_expression
			| (lit('(') >> value_expression >> lit(')'))
			| cast_specification
			| lexeme[lit('!') >> mapnik_variable >> lit('!')];

		const auto mapnik_variable_def =
			lit("scale_denominator")
			| lit("pixel_width")
			| lit("pixel_height");

		BOOST_SPIRIT_DEFINE(value_expression, value_expression_primary, mapnik_variable);

		//-----------------------------------------------------------------------------
		// 6.12 <numeric value expression>
		//-----------------------------------------------------------------------------

		const rule<class term> term = "term";
		const rule<class factor> factor = "factor";
		const rule<class numeric_primary> numeric_primary = "numeric primary";

		const auto numeric_value_expression_def =
			term
			| (numeric_value_expression >> lit('+') >> term)
			| (numeric_value_expression >> lit('-') >> term);

		const auto term_def =
			factor
			| (term >> lit('*') >> factor)
			| (term >> lit('/') >> factor);

		const auto factor_def =
			-(sign) >> numeric_primary;

		const auto numeric_primary_def =
			value_expression_primary
			| numeric_value_function;

		BOOST_SPIRIT_DEFINE(numeric_value_expression, term, factor, numeric_primary);

		//-----------------------------------------------------------------------------
		// 6.13 <string value expression>
		//-----------------------------------------------------------------------------

		const rule<class concatenation> concatenation = "concatenation";
		const rule<class character_factor> character_factor = "character factor";
		const rule<class character_primary> character_primary = "character primary";
		const rule<class bit_factor> bit_factor = "bit factor";
		const rule<class bit_primary> bit_primary = "bit primary";

		const auto string_value_expression_def =
			character_value_expression
			| bit_value_expression;

		const auto character_value_expression_def =
			recursion(character_value_expression,
				  concatenation
				  | character_factor);

		const auto concatenation_def =
			character_value_expression >> lexeme[lit("||")] >> character_factor;

		const auto character_factor_def =
			character_primary >> -(collate_clause);

		const auto character_primary_def =
			value_expression_primary
			| string_value_function;

		const auto bit_value_expression_def =
			recursion(bit_value_expression,
				  bit_concatenation
				  | bit_factor);

		const auto bit_concatenation_def =
			bit_value_expression >> lexeme[lit("||")] >> bit_factor;

		const auto bit_factor_def = bit_primary;

		const auto bit_primary_def =
			value_expression_primary
			| string_value_function;

		BOOST_SPIRIT_DEFINE(string_value_expression, character_value_expression, concatenation, character_factor, character_primary, bit_value_expression, bit_concatenation, bit_factor, bit_primary);

		//-----------------------------------------------------------------------------
		// 6.14 <datetime value expression>
		//-----------------------------------------------------------------------------

		const rule<class datetime_term> datetime_term = "datetime term";
		const rule<class datetime_factor> datetime_factor = "datetime factor";
		const rule<class datetime_primary> datetime_primary = "datetime primary";
		const rule<class time_zone> time_zone = "time zone";
		const rule<class time_zone_specifier> time_zone_specifier = "time zone specifier";

		const auto datetime_value_expression_def =
			datetime_term
			| (interval_value_expression >> lit('+') >> datetime_term)
			| (datetime_value_expression >> lit('+') >> interval_term)
			| (datetime_value_expression >> lit('-') >> interval_term);

		const auto datetime_term_def =
			datetime_factor;

		const auto datetime_factor_def =
			datetime_primary >> -(time_zone);

		const auto datetime_primary_def =
			value_expression_primary
			| datetime_value_function;

		const auto time_zone_def =
			keyword("at") >> time_zone_specifier;

		const auto time_zone_specifier_def =
			keyword("local")
			| (keyword("time") >> keyword("zone") >> interval_value_expression);

		BOOST_SPIRIT_DEFINE(datetime_value_expression, datetime_term, datetime_factor,
				    datetime_primary, time_zone, time_zone_specifier);

		//-----------------------------------------------------------------------------
		// 6.15 <interval value expression>
		//-----------------------------------------------------------------------------

		const rule<class interval_factor> interval_factor = "interval factor";
		const rule<class interval_primary> interval_primary = "interval primary";
		const rule<class interval_value_expression_1> interval_value_expression_1 = "interval value expression 1";
		const rule<class interval_term_1> interval_term_1 = "interval term 1";
		const rule<class interval_term_2> interval_term_2 = "interval term 2";

		const auto interval_value_expression_def =
			interval_term
			| (interval_value_expression_1 >> lit('+') >> interval_term_1)
			| (interval_value_expression_1 >> lit('-') >> interval_term_1)
			| (lit('(') >> datetime_value_expression >> lit('-') >>
			   datetime_term >> lit(')') >> interval_qualifier);

		const auto interval_term_def =
			interval_factor
			| (interval_term_2 >> lit('*') >> factor)
			| (interval_term_2 >> lit('/') >> factor)
			| (term >> lit('*') >> interval_factor);

		const auto interval_factor_def =
			-(sign) >> interval_primary;

		const auto interval_primary_def =
			value_expression_primary >> -(interval_qualifier);

		const auto interval_value_expression_1_def = interval_value_expression;

		const auto interval_term_1_def = interval_term;

		const auto interval_term_2_def = interval_term;

		BOOST_SPIRIT_DEFINE(interval_value_expression, interval_term, interval_factor,
				    interval_primary, interval_value_expression_1, interval_term_1,
				    interval_term_2);

		//-----------------------------------------------------------------------------
		// 7 Query expressions
		//-----------------------------------------------------------------------------

		//-----------------------------------------------------------------------------
		// 7.1 <row value constructor>
		//-----------------------------------------------------------------------------

		const rule<class row_value_constructor_list> row_value_constructor_list = "row value constructor list";
		const rule<class row_value_constructor_element> row_value_constructor_element = "row value constructor element";
		const rule<class null_specification> null_specification = "null specification";
		const rule<class default_specification> default_specification = "default specification";

		const auto row_value_constructor_def =
			row_value_constructor_element
			| (lit('(') >> row_value_constructor_list >> lit(')'))
			| row_subquery;

		const auto row_value_constructor_list_def =
			row_value_constructor_element % lit(',');

		const auto row_value_constructor_element_def =
			value_expression
			| null_specification
			| default_specification;

		const auto null_specification_def =
			keyword("null");

		const auto default_specification_def =
			keyword("default");

		BOOST_SPIRIT_DEFINE(row_value_constructor, row_value_constructor_list,
				    row_value_constructor_element, null_specification, default_specification);

		//-----------------------------------------------------------------------------
		// 7.2 <table value constructor>
		//-----------------------------------------------------------------------------

		const rule<class table_value_constructor_list> table_value_constructor_list = "table value constructor list";

		const auto table_value_constructor_def =
			keyword("values") >> table_value_constructor_list;

		const auto table_value_constructor_list_def =
			row_value_constructor % lit(',');

		BOOST_SPIRIT_DEFINE(table_value_constructor, table_value_constructor_list);

		//-----------------------------------------------------------------------------
		// 7.3 <table expression>
		//-----------------------------------------------------------------------------

		const auto table_expression_def =
			from_clause >> -(where_clause) >> -(group_by_clause) >> -(having_clause);

		BOOST_SPIRIT_DEFINE(table_expression);

		//-----------------------------------------------------------------------------
		// 7.4 <from clause>
		//-----------------------------------------------------------------------------

		const auto from_clause_def = keyword("from") >> (table_reference % lit(','));

		BOOST_SPIRIT_DEFINE(from_clause);

		//-----------------------------------------------------------------------------
		// 7.5 <joined table>
		//-----------------------------------------------------------------------------

		const rule<class cross_join> cross_join = "cross join";
		const rule<class qualified_join> qualified_join = "qualified join";
		const rule<class join_specification> join_specification = "join specification";
		const rule<class named_columns_join> named_columns_join = "named columns join";
		const rule<class join_type> join_type = "join type";
		const rule<class outer_join_type> outer_join_type = "outer join type";
		const rule<class join_column_list> join_column_list = "join column list";

		const auto joined_table_def =
			recursion(joined_table,
				  cross_join
				  | qualified_join
				  | (lit('(') >> joined_table >> lit(')')));

		const auto cross_join_def =
			table_reference >> keyword("cross") >> keyword("join") >> table_reference;

		const auto qualified_join_def =
			table_reference >> -(keyword("natural")) >> -(join_type) >> keyword("join") >>
			table_reference >> -(join_specification);

		const auto join_specification_def =
			join_condition
			| named_columns_join;

		const auto join_condition_def = keyword("on") >> search_condition;

		const auto named_columns_join_def =
			keyword("using") >> lit('(') >> join_column_list >> lit(')');

		const auto join_type_def =
			keyword("inner")
			| (outer_join_type >> -(keyword("outer")))
			| keyword("union");

		const auto outer_join_type_def =
			keyword("left")
			| keyword("right")
			| keyword("full");

		const auto join_column_list_def = column_name_list;

		BOOST_SPIRIT_DEFINE(joined_table, cross_join, qualified_join, join_specification,
				    join_condition, named_columns_join, join_type, outer_join_type,
				    join_column_list);

		//-----------------------------------------------------------------------------
		// 7.6 <where clause>
		//-----------------------------------------------------------------------------

		const auto where_clause_def = keyword("where") >> search_condition;

		BOOST_SPIRIT_DEFINE(where_clause);

		//-----------------------------------------------------------------------------
		// 7.7 <group by clause>
		//-----------------------------------------------------------------------------

		const rule<class grouping_column_reference_list> grouping_column_reference_list = "grouping column reference list";
		const rule<class grouping_column_reference> grouping_column_reference = "grouping column reference";

		const auto group_by_clause_def =
			keyword("group") >> keyword("by") >> grouping_column_reference_list;

		const auto grouping_column_reference_list_def =
			grouping_column_reference % lit(',');

		const auto grouping_column_reference_def =
			column_reference >> -(collate_clause);

		BOOST_SPIRIT_DEFINE(group_by_clause, grouping_column_reference_list,
				    grouping_column_reference);

		//-----------------------------------------------------------------------------
		// 7.8 <having clause>
		//-----------------------------------------------------------------------------

		const auto having_clause_def = keyword("having") >> search_condition;

		BOOST_SPIRIT_DEFINE(having_clause);

		//-----------------------------------------------------------------------------
		// 7.9 <query specification>
		//-----------------------------------------------------------------------------

		const rule<class select_sublist> select_sublist = "select sublist";
		const rule<class as_clause> as_clause = "as clause";

		const auto query_specification_def =
			keyword("select") >> -(set_quantifier) >> select_list > table_expression;

		const auto select_list_def =
			lit('*')
			| (select_sublist % lit(','));

		const auto select_sublist_def =
			derived_column
			| (qualifier >> lit('.') >> lit('*'));

		const auto derived_column_def = value_expression >> -(as_clause);

		const auto as_clause_def = -(keyword("as")) >> column_name;

		BOOST_SPIRIT_DEFINE(query_specification,select_list,select_sublist,derived_column,as_clause);

		//-----------------------------------------------------------------------------
		// 7.10 <query expression>
		//-----------------------------------------------------------------------------

		const rule<class non_join_query_expression> non_join_query_expression = "non-join query expression";
		const rule<class query_term> query_term = "query term";
		const rule<class non_join_query_term> non_join_query_term = "non-join query term";
		const rule<class query_primary> query_primary = "query primary";
		const rule<class non_join_query_primary> non_join_query_primary = "non-join query primary";
		const rule<class simple_table> simple_table = "simple table";
		const rule<class explicit_table> explicit_table = "explicit table";
		const rule<class corresponding_spec> corresponding_spec = "corresponding spec";
		const rule<class corresponding_column_list> corresponding_column_list = "corresponding column list";

		const auto query_expression_def =
			recursion(query_expression,
				  non_join_query_expression
				  | joined_table);

		const auto non_join_query_expression_def =
			recursion(non_join_query_expression,
				  non_join_query_term
				  | (query_expression >> keyword("union") >> -(keyword("all")) >> -(corresponding_spec) >> query_term)
				  | (query_expression >> keyword("except") >> -(keyword("all")) >> -(corresponding_spec) >> query_term));

		const auto query_term_def =
			recursion(query_term,
				  non_join_query_term
				  | joined_table);

		const auto non_join_query_term_def =
        		recursion(non_join_query_term,
				  non_join_query_primary
				  | (query_term >> keyword("intersect") >> -(keyword("all")) >> -(corresponding_spec) >> query_primary));

		const auto query_primary_def =
			recursion(query_primary,
				  non_join_query_primary
				  | joined_table);

		const auto non_join_query_primary_def =
			simple_table
			| (lit('(') >> non_join_query_expression >> lit(')'));

		const auto simple_table_def =
			query_specification
			| table_value_constructor
			| explicit_table;

		const auto explicit_table_def = keyword("table") >> table_name;

		const auto corresponding_spec_def =
			keyword("corresponding") >> -(keyword("by") >> lit('(') >> corresponding_column_list >> lit(')'));

		const auto corresponding_column_list_def = column_name_list;

		BOOST_SPIRIT_DEFINE(query_expression, non_join_query_expression, query_term,
				    non_join_query_term, query_primary, non_join_query_primary,
				    simple_table, explicit_table, corresponding_spec,
				    corresponding_column_list);

		//-----------------------------------------------------------------------------
		// 7.11  <scalar subquery>, <row subquery>, and <table subquery>
		//-----------------------------------------------------------------------------

		const auto scalar_subquery_def = subquery;

		const auto row_subquery_def = subquery;

		const auto table_subquery_def = subquery;

		const auto subquery_def = lit('(') >> query_expression >> lit(')');

		BOOST_SPIRIT_DEFINE(scalar_subquery, row_subquery, table_subquery, subquery);

		//-----------------------------------------------------------------------------
		// 8 Predicates
		//-----------------------------------------------------------------------------

		//-----------------------------------------------------------------------------
		// 8.1 <predicate>
		//-----------------------------------------------------------------------------

		const auto predicate_def =
			comparison_predicate
			| between_predicate
			| in_predicate
			| like_predicate
			| null_predicate
			| quantified_comparison_predicate
			| exists_predicate
			| unique_predicate
			| match_predicate
			| overlaps_predicate;

		BOOST_SPIRIT_DEFINE(predicate);

		//-----------------------------------------------------------------------------
		// 8.2 <comparison predicate>
		//-----------------------------------------------------------------------------

		const rule<class comp_op> comp_op = "comp op";

		const auto comparison_predicate_def =
			row_value_constructor >> comp_op >> row_value_constructor;

		const auto comp_op_def = lexeme[compoptable];

		BOOST_SPIRIT_DEFINE(comparison_predicate, comp_op);

		//-----------------------------------------------------------------------------
		// 8.3 <between predicate>
		//-----------------------------------------------------------------------------

		const auto between_predicate_def =
			row_value_constructor >> -(keyword("not")) >> keyword("between") >>
			row_value_constructor >> keyword("and") >> row_value_constructor;

		BOOST_SPIRIT_DEFINE(between_predicate);

		//-----------------------------------------------------------------------------
		// 8.4 <in predicate>
		//-----------------------------------------------------------------------------

		const rule<class in_predicate_value> in_predicate_value = "in predicate value";
		const rule<class in_value_list> in_value_list = "in value list";

		const auto in_predicate_def =
			row_value_constructor >> -(keyword("not")) >> keyword("in") >> in_predicate_value;

		const auto in_predicate_value_def =
			table_subquery
			| (lit('(') >> in_value_list >> lit(')'));

		const auto in_value_list_def =
			value_expression % lit(',');

		BOOST_SPIRIT_DEFINE(in_predicate, in_predicate_value, in_value_list);

		//-----------------------------------------------------------------------------
		// 8.5 <like predicate>
		//-----------------------------------------------------------------------------

		const rule<class match_value> match_value = "match value";
		const rule<class pattern> pattern = "pattern";
		const rule<class escape_character> escape_character = "escape character";

		const auto like_predicate_def =
			match_value >> -(keyword("not")) >> keyword("like") >> pattern >>
			-(keyword("escape")) >> escape_character;

		const auto match_value_def = character_value_expression;

		const auto pattern_def = character_value_expression;

		const auto escape_character_def = character_value_expression;

		BOOST_SPIRIT_DEFINE(like_predicate, match_value, pattern, escape_character);

		//-----------------------------------------------------------------------------
		// 8.6 <null predicate>
		//-----------------------------------------------------------------------------

		const auto null_predicate_def = row_value_constructor >> keyword("is") >> -(keyword("not")) >> keyword("null");

		BOOST_SPIRIT_DEFINE(null_predicate);

		//-----------------------------------------------------------------------------
		// 8.7 <quantified comparison predicate>
		//-----------------------------------------------------------------------------

		const rule<class quantifier> quantifier = "quantifier";
		const rule<class all> all = "all";
		const rule<class some> some = "some";

		const auto quantified_comparison_predicate_def =
			row_value_constructor >> comp_op >> quantifier >> table_subquery;

		const auto quantifier_def = all | some;

		const auto all_def = keyword("all");

		const auto some_def = keyword("some") | keyword("any");

		BOOST_SPIRIT_DEFINE(quantified_comparison_predicate, quantifier, all, some);

		//-----------------------------------------------------------------------------
		// 8.8 <exists predicate>
		//-----------------------------------------------------------------------------

		const auto exists_predicate_def = keyword("exists") >> table_subquery;

		BOOST_SPIRIT_DEFINE(exists_predicate);

		//-----------------------------------------------------------------------------
		// 8.9 <unique predicate>
		//-----------------------------------------------------------------------------

		const auto unique_predicate_def = keyword("unique") >> table_subquery;

		BOOST_SPIRIT_DEFINE(unique_predicate);

		//-----------------------------------------------------------------------------
		// 8.10 <match predicate>
		//-----------------------------------------------------------------------------

		const auto match_predicate_def =
			row_value_constructor >> keyword("match") >> -(keyword("unique")) >>
			-(keyword("partial") | keyword("full")) >> table_subquery;

		BOOST_SPIRIT_DEFINE(match_predicate);

		//-----------------------------------------------------------------------------
		// 8.11 <overlaps predicate>
		//-----------------------------------------------------------------------------

		const rule<class row_value_constructor_1> row_value_constructor_1 = "row value constructor 1";
		const rule<class row_value_constructor_2> row_value_constructor_2 = "row value constructor 2";

		const auto overlaps_predicate_def =
			row_value_constructor_1 >> keyword("overlaps") >> row_value_constructor_2;

		const auto row_value_constructor_1_def = row_value_constructor;

		const auto row_value_constructor_2_def = row_value_constructor;

		BOOST_SPIRIT_DEFINE(overlaps_predicate, row_value_constructor_1, row_value_constructor_2);

		//-----------------------------------------------------------------------------
		// 8.12 <search condition>
		//-----------------------------------------------------------------------------

		const rule<class boolean_term> boolean_term = "boolean term";
		const rule<class boolean_test> boolean_test = "boolean test";
		const rule<class truth_value> truth_value = "truth value";
		const rule<class boolean_primary> boolean_primary = "boolean primary";

		const auto search_condition_def =
			boolean_term
			| (search_condition >> keyword("or") >> boolean_term);

		const auto boolean_term_def =
			boolean_factor
			| (boolean_term >> keyword("and") >> boolean_factor);

		const auto boolean_factor_def =
			-(keyword("not")) >> boolean_test;

		const auto boolean_test_def =
			boolean_primary >> -(keyword("is") >> -(keyword("not")) >> truth_value);

		const auto truth_value_def =
			keyword("true")
			| keyword("false")
			| keyword("unknown");

		const auto boolean_primary_def =
			predicate
			| (lit('(') >> search_condition >> lit(')'));

		BOOST_SPIRIT_DEFINE(search_condition, boolean_term, boolean_factor, boolean_test,
				    truth_value, boolean_primary);

		//-----------------------------------------------------------------------------
		// 10 Additional common elements
		//-----------------------------------------------------------------------------

		//-----------------------------------------------------------------------------
		// 10.1 <interval qualifier>
		//-----------------------------------------------------------------------------

		const rule<class single_datetime_field> single_datetime_field = "single datetime field";
		const rule<class non_second_datetime_field> non_second_datetime_field = "non-second datetime field";
		const rule<class interval_fractional_seconds_precision> interval_fractional_seconds_precision = "interval fractional seconds precision";

		const auto interval_qualifier_def =
			(start_field >> keyword("to") >> end_field)
			| single_datetime_field;

		const auto start_field_def =
			non_second_datetime_field >>
			-(lit('(') >> interval_leading_field_precision >> lit(')'));

		const auto end_field_def =
			non_second_datetime_field
			| (keyword("second") >> -(lit('(') >> interval_fractional_seconds_precision >> lit(')')));

		const auto single_datetime_field_def =
			(non_second_datetime_field >>
			 -(lit('(') >> interval_leading_field_precision >> lit(')')))
			| (keyword("second") >> -(lit('(') >> interval_leading_field_precision >>
						  -(lit(',') >> interval_fractional_seconds_precision) >> lit(')')));

		const auto datetime_field_def =
			non_second_datetime_field
			| keyword("second");

		const auto non_second_datetime_field_def =
			keyword("year") | keyword("month") | keyword("day") | keyword("hour") | keyword("minute");

		const auto interval_fractional_seconds_precision_def = unsigned_integer;

		const auto interval_leading_field_precision_def = unsigned_integer;

		BOOST_SPIRIT_DEFINE(interval_qualifier, start_field, end_field, single_datetime_field,
				    datetime_field, non_second_datetime_field,
				    interval_fractional_seconds_precision, interval_leading_field_precision);

		//-----------------------------------------------------------------------------
		// 10.2 <language clause>
		//-----------------------------------------------------------------------------

		const rule<class language_name> language_name = "language name";

		const auto language_clause_def =
			keyword("language") >> language_name;

		const auto language_name_def =
			keyword("ada") | keyword("c") | keyword("cobol") | keyword("fortran") |
			keyword("mumps") | keyword("pascal") | keyword("pli");


		BOOST_SPIRIT_DEFINE(language_clause, language_name);

		//-----------------------------------------------------------------------------
		// 10.3 <privileges>
		//-----------------------------------------------------------------------------

		const rule<class action_list> action_list = "action list";
		const rule<class privilege_column_list> privilege_column_list = "privilege column list";
		const rule<class grantee> grantee = "grantee";

		const auto privileges_def =
			(keyword("all") >> keyword("privileges"))
			| action_list;

		const auto action_list_def = action % lit(',');

		const auto action_def =
			keyword("select")
			| keyword("delete")
			| (keyword("insert") >> -(lit('(') >> privilege_column_list >> lit(')')))
			| (keyword("update") >> -(lit('(') >> privilege_column_list >> lit(')')))
			| (keyword("references") >> -(lit('(') >> privilege_column_list >> lit(')')))
			| keyword("usage");

		const auto privilege_column_list_def = column_name_list;

		const auto grantee_def =
			keyword("public")
			| authorization_identifier;

		BOOST_SPIRIT_DEFINE(privileges, action_list, action, privilege_column_list, grantee);

		//-----------------------------------------------------------------------------
		// 10.4 <character set specification>
		//-----------------------------------------------------------------------------

		const rule<class standard_character_repertoire_name> standard_character_repertoire_name = "standard character repertoire name";
		const rule<class user_defined_character_repertoire_name> user_defined_character_repertoire_name = "user-defined character repertoire name";
		const rule<class standard_universal_character_form_of_use_name> standard_universal_character_form_of_use_name = "standard universal character form-of-use name";
		const rule<class implementation_defined_universal_character_form_of_use_name> implementation_defined_universal_character_form_of_use_name = "implementation-defined universal character form-of-use name";

		const auto character_set_specification_def =
			standard_character_repertoire_name
			| implementation_defined_character_repertoire_name
			| user_defined_character_repertoire_name
			| standard_universal_character_form_of_use_name
			| implementation_defined_universal_character_form_of_use_name;

		const auto standard_character_repertoire_name_def = character_set_name;

		const auto implementation_defined_character_repertoire_name_def = character_set_name;

		const auto user_defined_character_repertoire_name_def = character_set_name;

		const auto standard_universal_character_form_of_use_name_def =
			character_set_name;

		const auto implementation_defined_universal_character_form_of_use_name_def =
			character_set_name;

		BOOST_SPIRIT_DEFINE(character_set_specification, standard_character_repertoire_name,
				    implementation_defined_character_repertoire_name,
				    user_defined_character_repertoire_name,
				    standard_universal_character_form_of_use_name,
				    implementation_defined_universal_character_form_of_use_name);

		//-----------------------------------------------------------------------------
		// 10.5 <collate clause>
		//-----------------------------------------------------------------------------

		const auto collate_clause_def = keyword("collate") >> collation_name;

		BOOST_SPIRIT_DEFINE(collate_clause);

		//-----------------------------------------------------------------------------
		// 10.6 <constraint name definition> and <constraint attributes>
		//-----------------------------------------------------------------------------

		const rule<class constraint_check_time> constraint_check_time = "constraint check time";

		const auto constraint_name_definition_def = keyword("constraint") >> constraint_name;

		const auto constraint_attributes_def =
			(constraint_check_time >> -(-(keyword("not")) >> keyword("deferrable")))
			| (-(keyword("not")) >> keyword("deferrable") >> -(constraint_check_time));

		const auto constraint_check_time_def =
			(keyword("initially") >> keyword("deferred"))
			| (keyword("initially") >> keyword("immediate"));

		BOOST_SPIRIT_DEFINE(constraint_name_definition, constraint_attributes,
				    constraint_check_time);

		//-----------------------------------------------------------------------------
		// 11 Schema definition and manipulation
		//-----------------------------------------------------------------------------

		//-----------------------------------------------------------------------------
		// 11.1 <schema definition>
		//-----------------------------------------------------------------------------

		const rule<class schema_element> schema_element = "schema element";

		const auto schema_definition_def =
			keyword("create") >> keyword("schema") >> schema_name_clause >>
			-(schema_character_set_specification) >> *schema_element;

		const auto schema_name_clause_def =
			schema_name
			| (keyword("authorization") >> schema_authorization_identifier)
			| (schema_name >> keyword("authorization") >> schema_authorization_identifier);

		const auto schema_authorization_identifier_def =
			authorization_identifier;

		const auto schema_character_set_specification_def =
			keyword("default") >> keyword("character") >> keyword("set") >> character_set_specification;

		const auto schema_element_def =
			domain_definition
			| table_definition
			| view_definition
			| grant_statement
			| assertion_definition
			| character_set_definition
			| collation_definition
			| translation_definition;

		BOOST_SPIRIT_DEFINE(schema_definition, schema_name_clause,
				    schema_authorization_identifier,
				    schema_character_set_specification, schema_element);

		//-----------------------------------------------------------------------------
		// 11.2 <drop schema statement>
		//-----------------------------------------------------------------------------

		const rule<class drop_behavior> drop_behavior = "drop behavior";

		const auto drop_schema_statement_def =
			keyword("drop") >> keyword("schema") >> schema_name >> drop_behavior;

		const auto drop_behavior_def = keyword("cascade") | keyword("restrict");

		BOOST_SPIRIT_DEFINE(drop_schema_statement, drop_behavior);

		//-----------------------------------------------------------------------------
		// 11.3 <table definition>
		//-----------------------------------------------------------------------------

		const rule<class table_element_list> table_element_list = "table element list";
		const rule<class table_element> table_element = "table element";

		const auto table_definition_def =
			keyword("create") >> -((keyword("global") | keyword("local")) >> keyword("temporary")) >>
			keyword("table") >> table_name >> table_element_list >>
			-(keyword("on") >> keyword("commit") >> (keyword("delete") | keyword("preserve")) >> keyword("rows"));

		const auto table_element_list_def =
			lit('(') >> (table_element % lit(',')) >> lit(')');

		const auto table_element_def =
			column_definition
			| table_constraint_definition;

		BOOST_SPIRIT_DEFINE(table_definition, table_element_list, table_element);

		//-----------------------------------------------------------------------------
		// 11.4 <column definition>
		//-----------------------------------------------------------------------------

		const rule<class column_constraint_definition> column_constraint_definition = "column constraint definition";
		const rule<class column_constraint> column_constraint = "column constraint";

		const auto column_definition_def =
			column_name >> (data_type | domain_name) >>
			-(default_clause) >> *column_constraint_definition >>
			-(collate_clause);

		const auto column_constraint_definition_def =
			-(constraint_name_definition) >>
			column_constraint >>
			-(constraint_attributes);

		const auto column_constraint_def =
			(keyword("not") >> keyword("null"))
			| unique_specification
			| references_specification
			| check_constraint_definition;

		BOOST_SPIRIT_DEFINE(column_definition, column_constraint_definition, column_constraint);

		//-----------------------------------------------------------------------------
		// 11.5 <default clause>
		//-----------------------------------------------------------------------------

		const auto default_clause_def =
			keyword("default") >> default_option;

		const auto default_option_def =
			literal
			| datetime_value_function
			| keyword("user")
			| keyword("current_user")
			| keyword("session_user")
			| keyword("system_user")
			| keyword("null");

		BOOST_SPIRIT_DEFINE(default_clause, default_option);

		//-----------------------------------------------------------------------------
		// 11.6 <table constraint definition>
		//-----------------------------------------------------------------------------

		const auto table_constraint_definition_def =
			-(constraint_name_definition) >>
			table_constraint >> -(constraint_attributes);

		const auto table_constraint_def =
			unique_constraint_definition
			| referential_constraint_definition
			| check_constraint_definition;

		BOOST_SPIRIT_DEFINE(table_constraint_definition, table_constraint);

		//-----------------------------------------------------------------------------
		// 11.7 <unique constraint definition>
		//-----------------------------------------------------------------------------

		const auto unique_constraint_definition_def =
			unique_specification >>
			lit('(') >> unique_column_list >> lit(')');

		const auto unique_specification_def =
			keyword("unique") | (keyword("primary") >> keyword("key"));

		const auto unique_column_list_def = column_name_list;

		BOOST_SPIRIT_DEFINE(unique_constraint_definition,unique_specification,unique_column_list);

		//-----------------------------------------------------------------------------
		// 11.8 <referential constraint definition>
		//-----------------------------------------------------------------------------

		const rule<class reference_column_list> reference_column_list = "reference column list";
		const rule<class referential_triggered_action> referential_triggered_action = "referential triggered action";
		const rule<class update_rule> update_rule = "update rule";
		const rule<class delete_rule> delete_rule = "delete rule";
		const rule<class referential_action> referential_action = "referential action";

		const auto referential_constraint_definition_def =
			keyword("foreign") >> keyword("key") >> lit('(') >> referencing_columns >> lit(')') >>
			references_specification;

		const auto references_specification_def =
			keyword("references") >> referenced_table_and_columns >>
			-(keyword("match") >> match_type) >> -(referential_triggered_action);

		const auto match_type_def =
			keyword("full")
			| keyword("partial");

		const auto referencing_columns_def =
			reference_column_list;

		const auto referenced_table_and_columns_def =
			table_name >> lit('(') >> reference_column_list >> lit(')');


		const auto reference_column_list_def = column_name_list;

		const auto referential_triggered_action_def =
			(update_rule >> -(delete_rule))
			| (delete_rule >> -(update_rule));

		const auto update_rule_def = keyword("on") >> keyword("update") >> referential_action;

		const auto delete_rule_def = keyword("on") >> keyword("delete") >> referential_action;

		const auto referential_action_def =
			keyword("cascade")
			| (keyword("set") >> keyword("null"))
			| (keyword("set") >> keyword("default"))
			| (keyword("no") >> keyword("action"));

		BOOST_SPIRIT_DEFINE(referential_constraint_definition, references_specification,
				    match_type, referencing_columns, referenced_table_and_columns,
				    reference_column_list, referential_triggered_action, update_rule,
				    delete_rule, referential_action);

		//-----------------------------------------------------------------------------
		// 11.9 <check constraint definition>
		//-----------------------------------------------------------------------------

		const auto check_constraint_definition_def =
			keyword("check") >> lit('(') >> search_condition >> lit(')');

		BOOST_SPIRIT_DEFINE(check_constraint_definition);

		//-----------------------------------------------------------------------------
		// 11.10 <alter table statement>
		//-----------------------------------------------------------------------------

		const rule<class alter_table_action> alter_table_action = "alter table action";

		const auto alter_table_statement_def =
			keyword("alter") >> keyword("table") >> table_name >> alter_table_action;

		const auto alter_table_action_def =
			add_column_definition
			| alter_column_definition
			| drop_column_definition
			| add_table_constraint_definition
			| drop_table_constraint_definition;

		BOOST_SPIRIT_DEFINE(alter_table_statement, alter_table_action);

		//-----------------------------------------------------------------------------
		// 11.11 <add column definition>
		//-----------------------------------------------------------------------------

		const auto add_column_definition_def =
			keyword("add") >> -(keyword("column")) >> column_definition;

		BOOST_SPIRIT_DEFINE(add_column_definition);

		//-----------------------------------------------------------------------------
		// 11.12 <alter column definition>
		//-----------------------------------------------------------------------------

		const rule<class alter_column_action> alter_column_action = "alter column action";

		const auto alter_column_definition_def =
			keyword("alter") >> -(keyword("column")) >> column_name >> alter_column_action;

		const auto alter_column_action_def =
			set_column_default_clause
			| drop_column_default_clause;

		BOOST_SPIRIT_DEFINE(alter_column_definition,alter_column_action);

		//-----------------------------------------------------------------------------
		// 11.13 <set column default clause>
		//-----------------------------------------------------------------------------

		const auto set_column_default_clause_def =
			keyword("set") >> default_clause;

		BOOST_SPIRIT_DEFINE(set_column_default_clause);

		//-----------------------------------------------------------------------------
		// 11.14 <drop column default clause>
		//-----------------------------------------------------------------------------

		const auto drop_column_default_clause_def =
			keyword("drop") >> keyword("default");

		BOOST_SPIRIT_DEFINE(drop_column_default_clause);

		//-----------------------------------------------------------------------------
		// 11.15 <drop column definition>
		//-----------------------------------------------------------------------------

		const auto drop_column_definition_def =
			keyword("drop") >> -(keyword("column")) >> column_name >> drop_behavior;

		BOOST_SPIRIT_DEFINE(drop_column_definition);

		//-----------------------------------------------------------------------------
		// 11.16 <add table constraint definition>
		//-----------------------------------------------------------------------------

		const auto add_table_constraint_definition_def =
			keyword("add") >> table_constraint_definition;

		BOOST_SPIRIT_DEFINE(add_table_constraint_definition);

		//-----------------------------------------------------------------------------
		// 11.17 <drop table constraint definition>
		//-----------------------------------------------------------------------------

		const auto drop_table_constraint_definition_def =
			keyword("drop") >> keyword("constraint") >> constraint_name >> drop_behavior;

		BOOST_SPIRIT_DEFINE(drop_table_constraint_definition);

		//-----------------------------------------------------------------------------
		// 11.18 <drop table statement>
		//-----------------------------------------------------------------------------

		const auto drop_table_statement_def =
			keyword("drop") >> keyword("table") >> table_name >> drop_behavior;

		BOOST_SPIRIT_DEFINE(drop_table_statement);

		//-----------------------------------------------------------------------------
		// 11.19 <view definition>
		//-----------------------------------------------------------------------------

		const rule<class levels_clause> levels_clause = "levels clause";
		const rule<class view_column_list> view_column_list = "view column list";

		const auto view_definition_def =
			keyword("create") >> keyword("view") >> table_name >>
			-(lit('(') >> view_column_list >> lit(')')) >>
			keyword("as") >> query_expression >>
			-(keyword("with") >> -(levels_clause) >> keyword("check") >> keyword("option"));

		const auto levels_clause_def =
			keyword("cascaded") | keyword("local");

		const auto view_column_list_def = column_name_list;

		BOOST_SPIRIT_DEFINE(view_definition, levels_clause, view_column_list);

		//-----------------------------------------------------------------------------
		// 11.20 <drop view statement>
		//-----------------------------------------------------------------------------

		const auto drop_view_statement_def =
			keyword("drop") >> keyword("view") >> table_name >> drop_behavior;

		BOOST_SPIRIT_DEFINE(drop_view_statement);

		//-----------------------------------------------------------------------------
		// 11.21 <domain definition>
		//-----------------------------------------------------------------------------

		const auto domain_definition_def =
			keyword("create") >> keyword("domain") >> domain_name >>
			-(keyword("as")) >> data_type >> -(default_clause) >>
			*domain_constraint >> -(collate_clause);

		const auto domain_constraint_def =
			-(constraint_name_definition) >> check_constraint_definition >>
			-(constraint_attributes);

		BOOST_SPIRIT_DEFINE(domain_definition, domain_constraint);

		//-----------------------------------------------------------------------------
		// 11.22 <alter domain statement>
		//-----------------------------------------------------------------------------

		const rule<class alter_domain_action> alter_domain_action = "alter domain action";

		const auto alter_domain_statement_def =
			keyword("alter") >> keyword("domain") >> domain_name >> alter_domain_action;

		const auto alter_domain_action_def =
			set_domain_default_clause
			| drop_domain_default_clause
			| add_domain_constraint_definition
			| drop_domain_constraint_definition;

		BOOST_SPIRIT_DEFINE(alter_domain_statement,alter_domain_action);

		//-----------------------------------------------------------------------------
		// 11.23 <set domain default clause>
		//-----------------------------------------------------------------------------

		const auto set_domain_default_clause_def = keyword("set") >> default_clause;

		BOOST_SPIRIT_DEFINE(set_domain_default_clause);

		//-----------------------------------------------------------------------------
		// 11.24 <drop domain default clause>
		//-----------------------------------------------------------------------------

		const auto drop_domain_default_clause_def = keyword("drop") >> keyword("default");

		BOOST_SPIRIT_DEFINE(drop_domain_default_clause);

		//-----------------------------------------------------------------------------
		// 11.25 <add domain constraint definition>
		//-----------------------------------------------------------------------------

		const auto add_domain_constraint_definition_def =
			keyword("add") >> domain_constraint;

		BOOST_SPIRIT_DEFINE(add_domain_constraint_definition);

		//-----------------------------------------------------------------------------
		// 11.26 <drop domain constraint definition>
		//-----------------------------------------------------------------------------

		const auto drop_domain_constraint_definition_def =
			keyword("drop") >> keyword("constraint") >> constraint_name;

		BOOST_SPIRIT_DEFINE(drop_domain_constraint_definition);

		//-----------------------------------------------------------------------------
		// 11.27 <drop domain statement>
		//-----------------------------------------------------------------------------

		const auto drop_domain_statement_def =
			keyword("drop") >> keyword("domain") >> domain_name >> drop_behavior;

		BOOST_SPIRIT_DEFINE(drop_domain_statement);

		//-----------------------------------------------------------------------------
		// 11.28 <character set definition>
		//-----------------------------------------------------------------------------

		const rule<class character_set_source> character_set_source = "character set source";
		const rule<class existing_character_set_name> existing_character_set_name = "existing character set name";
		const rule<class schema_character_set_name> schema_character_set_name = "schema character set name";
		const rule<class limited_collation_definition> limited_collation_definition = "limited collation definition";

		const auto character_set_definition_def =
			keyword("create") >> keyword("character") >> keyword("set") >> character_set_name >>
			-(keyword("as")) >> character_set_source >>
			-(collate_clause | limited_collation_definition);

		const auto character_set_source_def =
			keyword("get") >> existing_character_set_name;

		const auto existing_character_set_name_def =
			standard_character_repertoire_name
			| implementation_defined_character_repertoire_name
			| schema_character_set_name;

		const auto schema_character_set_name_def = character_set_name;

		const auto limited_collation_definition_def =
			keyword("collation") >> keyword("from") >> collation_source;

		BOOST_SPIRIT_DEFINE(character_set_definition, character_set_source,
				    existing_character_set_name, schema_character_set_name,
				    limited_collation_definition);

		//-----------------------------------------------------------------------------
		// 11.29 <drop character set statement>
		//-----------------------------------------------------------------------------

		const auto drop_character_set_statement_def =
			keyword("drop") >> keyword("character") >> keyword("set") >> character_set_name;

		BOOST_SPIRIT_DEFINE(drop_character_set_statement);

		//-----------------------------------------------------------------------------
		// 11.30 <collation definition>
		//-----------------------------------------------------------------------------

		const rule<class pad_attribute> pad_attribute = "pad attribute";
		const rule<class collating_sequence_definition> collating_sequence_definition = "collating sequence definition";
		const rule<class translation_collation> translation_collation = "translation collation";
		const rule<class external_collation> external_collation = "external collation";
		const rule<class schema_collation_name> schema_collation_name = "schema collation name";
		const rule<class external_collation_name> external_collation_name = "external collation name";
		const rule<class standard_collation_name> standard_collation_name = "standard collation name";
		const rule<class implementation_defined_collation_name> implementation_defined_collation_name = "implementation-defined collation name";

		const auto collation_definition_def =
			keyword("create") >> keyword("collation") >> collation_name >> keyword("for") >> character_set_specification >>
			keyword("from") >> collation_source >> -(pad_attribute);

		const auto pad_attribute_def =
			(keyword("no") >> keyword("pad"))
			| (keyword("pad") >> keyword("space"));

		const auto collation_source_def =
			collating_sequence_definition
			| translation_collation;

		const auto collating_sequence_definition_def =
			external_collation
			| schema_collation_name
			| (keyword("desc") >> lit('(') >> collation_name >> lit(')'))
			| keyword("default");

		const auto translation_collation_def =
			keyword("translation") >> translation_name >>
			-(keyword("then") >> keyword("collation") >> collation_name);

		const auto external_collation_def =
			keyword("external") >> lit('(') >> lit('\'') >> external_collation_name >> lit('\'') >> lit(')');

		const auto schema_collation_name_def = collation_name;

		const auto external_collation_name_def =
			standard_collation_name
			| implementation_defined_collation_name;

		const auto standard_collation_name_def = collation_name;

		const auto implementation_defined_collation_name_def = collation_name;

		BOOST_SPIRIT_DEFINE(collation_definition, pad_attribute, collation_source,
				    collating_sequence_definition, translation_collation,
				    external_collation, schema_collation_name, external_collation_name,
				    standard_collation_name, implementation_defined_collation_name);

		//-----------------------------------------------------------------------------
		// 11.31 <drop collation statement>
		//-----------------------------------------------------------------------------

		const auto drop_collation_statement_def =
			keyword("drop") >> keyword("collation") >> collation_name;

		BOOST_SPIRIT_DEFINE(drop_collation_statement);

		//-----------------------------------------------------------------------------
		// 11.32 <translation definition>
		//-----------------------------------------------------------------------------

		const rule<class source_character_set_specification> source_character_set_specification = "source character set specification";
		const rule<class target_character_set_specification> target_character_set_specification = "target character set specification";
		const rule<class translation_source> translation_source = "translation source";
		const rule<class translation_specification> translation_specification = "translation specification";
		const rule<class external_translation> external_translation = "external translation";
		const rule<class external_translation_name> external_translation_name = "external translation name";
		const rule<class standard_translation_name> standard_translation_name = "standard translation name";
		const rule<class implementation_defined_translation_name> implementation_defined_translation_name = "implementation-defined translation name";
		const rule<class schema_translation_name> schema_translation_name = "schema translation name";

		const auto translation_definition_def =
			keyword("create") >> keyword("translation") >> translation_name >>
			keyword("for") >> source_character_set_specification >>
			keyword("to") >> target_character_set_specification >>
			keyword("from") >> translation_source;

		const auto source_character_set_specification_def = character_set_specification;


		const auto target_character_set_specification_def = character_set_specification;


		const auto translation_source_def =
			translation_specification;

		const auto translation_specification_def =
			external_translation
			| keyword("identity")
			| schema_translation_name;

		const auto external_translation_def =
			keyword("external") >> lit('(') >> lit('\'') >> external_translation_name >> lit('\'') >> lit(')');


		const auto external_translation_name_def =
			standard_translation_name
			| implementation_defined_translation_name;

		const auto standard_translation_name_def = translation_name;

		const auto implementation_defined_translation_name_def = translation_name;

		const auto schema_translation_name_def = translation_name;

		BOOST_SPIRIT_DEFINE(translation_definition, source_character_set_specification,
				    target_character_set_specification, translation_source,
				    translation_specification, external_translation,
				    external_translation_name, standard_translation_name,
				    implementation_defined_translation_name, schema_translation_name);

		//-----------------------------------------------------------------------------
		// 11.33 <drop translation statement>
		//-----------------------------------------------------------------------------

		const auto drop_translation_statement_def =
			keyword("drop") >> keyword("translation") >> translation_name;

		BOOST_SPIRIT_DEFINE(drop_translation_statement);

		//-----------------------------------------------------------------------------
		// 11.34 <assertion definition>
		//-----------------------------------------------------------------------------

		const rule<class assertion_check> assertion_check = "assertion check";

		const auto assertion_definition_def =
			keyword("create") >> keyword("assertion") >> constraint_name >> assertion_check >>
			-(constraint_attributes);

		const auto assertion_check_def =
			keyword("check") >> lit('(') >> search_condition >> lit(')');

		BOOST_SPIRIT_DEFINE(assertion_definition, assertion_check);

		//-----------------------------------------------------------------------------
		// 11.35 <drop assertion statement>
		//-----------------------------------------------------------------------------

		const auto drop_assertion_statement_def =
			keyword("drop") >> keyword("assertion") >> constraint_name;

		BOOST_SPIRIT_DEFINE(drop_assertion_statement);

		//-----------------------------------------------------------------------------
		// 11.36 <grant statement>
		//-----------------------------------------------------------------------------

		const auto grant_statement_def =
			keyword("grant") >> privileges >> keyword("on") >> object_name >>
			keyword("to") >> (grantee % lit(',')) >>
			-(keyword("with") >> keyword("grant") >> keyword("option"));

		const auto object_name_def =
			(-(keyword("table")) >> table_name)
			| (keyword("domain") >> domain_name)
			| (keyword("collation") >> collation_name)
			| (keyword("character") >> keyword("set") >> character_set_name)
			| (keyword("translation") >> translation_name);

		BOOST_SPIRIT_DEFINE(grant_statement, object_name);

		//-----------------------------------------------------------------------------
		// 11.37 <revoke statement>
		//-----------------------------------------------------------------------------

		const auto revoke_statement_def =
			keyword("revoke") >> -(keyword("grant") >> keyword("option") >> keyword("for")) >> privileges >>
			keyword("on") >> object_name >>	keyword("from") >> (grantee % lit(',')) >> drop_behavior;

		BOOST_SPIRIT_DEFINE(revoke_statement);

		//-----------------------------------------------------------------------------
		// 12 Module
		//-----------------------------------------------------------------------------

		//-----------------------------------------------------------------------------
		// 12.1 <module>
		//-----------------------------------------------------------------------------

		const rule<class module_contents> module_contents = "module contents";

		const auto module_def =
			module_name_clause >>
			language_clause >>
			module_authorization_clause >>
			*temporary_table_declaration >>
			*module_contents;

		const auto module_authorization_clause_def =
			(keyword("schema") >> schema_name)
			| (keyword("authorization") >> module_authorization_identifier)
			| (keyword("schema") >> schema_name >> keyword("authorization") >> module_authorization_identifier);

		const auto module_authorization_identifier_def =
			authorization_identifier;

		const auto module_contents_def =
			declare_cursor
			| dynamic_declare_cursor
			| procedure;

		BOOST_SPIRIT_DEFINE(module, module_authorization_clause, module_authorization_identifier,
				    module_contents);

		//-----------------------------------------------------------------------------
		// 12.2 <module name clause>
		//-----------------------------------------------------------------------------

		const auto module_name_clause_def =
			keyword("module") >> -(module_name) >>
			-(module_character_set_specification);

		const auto module_character_set_specification_def =
			keyword("names") >> keyword("are") >> character_set_specification;

		BOOST_SPIRIT_DEFINE(module_name_clause, module_character_set_specification);

		//-----------------------------------------------------------------------------
		// 12.3 <procedure>
		//-----------------------------------------------------------------------------

		const rule<class parameter_declaration_list> parameter_declaration_list = "parameter declaration list";
		const rule<class status_parameter> status_parameter = "status parameter";

		const auto procedure_def =
			keyword("procedure") >> procedure_name >> parameter_declaration_list >> lit(';') >>
			SQL_procedure_statement >> lit(';');

		const auto parameter_declaration_list_def =
			(lit('(') >> (parameter_declaration % lit(',')) >> lit(')'))
			| +parameter_declaration;

		const auto parameter_declaration_def =
			(parameter_name >> data_type)
			| status_parameter;

		const auto status_parameter_def =
			keyword("sqlcode") | keyword("sqlstate");

		BOOST_SPIRIT_DEFINE(procedure, parameter_declaration_list, parameter_declaration, status_parameter);

		//-----------------------------------------------------------------------------
		// 12.5 <SQL procedure statement>
		//-----------------------------------------------------------------------------

		const rule<class SQL_schema_definition_statement> SQL_schema_definition_statement = "L schema definition statement";
		const rule<class SQL_schema_manipulation_statement> SQL_schema_manipulation_statement = "L schema manipulation statement";
		const rule<class SQL_data_statement> SQL_data_statement = "L data statement";
		const rule<class SQL_data_change_statement> SQL_data_change_statement = "L data change statement";
		const rule<class SQL_transaction_statement> SQL_transaction_statement = "L transaction statement";
		const rule<class SQL_session_statement> SQL_session_statement = "L session statement";
		const rule<class SQL_dynamic_statement> SQL_dynamic_statement = "L dynamic statement";
		const rule<class system_descriptor_statement> system_descriptor_statement = "system descriptor statement";

		const auto SQL_procedure_statement_def =
			SQL_schema_statement
			| SQL_data_statement
			| SQL_transaction_statement
			| SQL_connection_statement
			| SQL_session_statement
			| SQL_dynamic_statement
			| SQL_diagnostics_statement;

		const auto SQL_schema_statement_def =
			SQL_schema_definition_statement
			| SQL_schema_manipulation_statement;

		const auto SQL_schema_definition_statement_def =
			schema_definition
			| table_definition
			| view_definition
			| grant_statement
			| domain_definition
			| character_set_definition
			| collation_definition
			| translation_definition
			| assertion_definition;

		const auto SQL_schema_manipulation_statement_def =
			drop_schema_statement
			| alter_table_statement
			| drop_table_statement
			| drop_view_statement
			| revoke_statement
			| alter_domain_statement
			| drop_domain_statement
			| drop_character_set_statement
			| drop_collation_statement
			| drop_translation_statement
			| drop_assertion_statement;

		const auto SQL_data_statement_def =
			open_statement
			| fetch_statement
			| close_statement
			| select_statement_single_row
			| SQL_data_change_statement;

		const auto SQL_data_change_statement_def =
			delete_statement_positioned
			| delete_statement_searched
			| insert_statement
			| update_statement_positioned
			| update_statement_searched;

		const auto SQL_transaction_statement_def =
			set_transaction_statement
			| set_constraints_mode_statement
			| commit_statement
			| rollback_statement;

		const auto SQL_connection_statement_def =
			connect_statement
			| set_connection_statement
			| disconnect_statement;

		const auto SQL_session_statement_def =
			set_catalog_statement
			| set_schema_statement
			| set_names_statement
			| set_session_authorization_identifier_statement
			| set_local_time_zone_statement;

		const auto SQL_dynamic_statement_def =
			system_descriptor_statement
			| prepare_statement
			| deallocate_prepared_statement
			| describe_statement
			| execute_statement
			| execute_immediate_statement
			| SQL_dynamic_data_statement;

		const auto SQL_dynamic_data_statement_def =
			allocate_cursor_statement
			| dynamic_open_statement
			| dynamic_fetch_statement
			| dynamic_close_statement
			| dynamic_delete_statement_positioned
			| dynamic_update_statement_positioned;

		const auto system_descriptor_statement_def =
			allocate_descriptor_statement
			| deallocate_descriptor_statement
			| set_descriptor_statement
			| get_descriptor_statement;

		const auto SQL_diagnostics_statement_def =
			get_diagnostics_statement;

		BOOST_SPIRIT_DEFINE(SQL_procedure_statement, SQL_schema_statement,
				    SQL_schema_definition_statement, SQL_schema_manipulation_statement,
				    SQL_data_statement, SQL_data_change_statement,
				    SQL_transaction_statement, SQL_connection_statement,
				    SQL_session_statement, SQL_dynamic_statement,
				    SQL_dynamic_data_statement, system_descriptor_statement,
				    SQL_diagnostics_statement);

		//-----------------------------------------------------------------------------
		// 13 Data manipulation
		//-----------------------------------------------------------------------------

		//-----------------------------------------------------------------------------
		// 13.1 <declare cursor>
		//-----------------------------------------------------------------------------

		const rule<class updatability_clause> updatability_clause = "updatability clause";
		const rule<class sort_specification_list> sort_specification_list = "sort specification list";
		const rule<class sort_specification> sort_specification = "sort specification";
		const rule<class sort_key> sort_key = "sort key";
		const rule<class ordering_specification> ordering_specification = "ordering specification";

		const auto declare_cursor_def =
			keyword("declare") >> cursor_name >> -(keyword("insensitive")) >> -(keyword("scroll")) >> keyword("cursor") >>
			keyword("for") >> cursor_specification;

		const auto cursor_specification_def =
			query_expression >> -(order_by_clause) >> -(updatability_clause);

		const auto updatability_clause_def =
			keyword("for") >> ((keyword("read") >> keyword("only")) | (keyword("update") >> -(keyword("of") >> column_name_list)));

		const auto order_by_clause_def =
			keyword("order") >> keyword("by") >> sort_specification_list;

		const auto sort_specification_list_def =
			sort_specification % lit(',');

		const auto sort_specification_def =
			sort_key >> -(collate_clause) >> -(ordering_specification);

		const auto sort_key_def =
			column_name
			| unsigned_integer;

		const auto ordering_specification_def = keyword("asc") | keyword("desc");

		BOOST_SPIRIT_DEFINE(declare_cursor, cursor_specification, updatability_clause,
				    order_by_clause, sort_specification_list, sort_specification,
				    sort_key, ordering_specification);

		//-----------------------------------------------------------------------------
		// 13.2 <open statement>
		//-----------------------------------------------------------------------------

		const auto open_statement_def =
			keyword("open") >> cursor_name;

		BOOST_SPIRIT_DEFINE(open_statement);

		//-----------------------------------------------------------------------------
		// 13.3 <fetch statement>
		//-----------------------------------------------------------------------------

		const rule<class fetch_orientation> fetch_orientation = "fetch orientation";
		const rule<class fetch_target_list> fetch_target_list = "fetch target list";

		const auto fetch_statement_def =
			keyword("fetch") >> -(-(fetch_orientation) >> keyword("from")) >>
			cursor_name >> keyword("into") >> fetch_target_list;

		const auto fetch_orientation_def =
			keyword("next")
			| keyword("prior")
			| keyword("first")
			| keyword("last")
			| ((keyword("absolute") | keyword("relative")) >> simple_value_specification);

		const auto fetch_target_list_def =
			target_specification % lit(',');

		BOOST_SPIRIT_DEFINE(fetch_statement, fetch_orientation, fetch_target_list);

		//-----------------------------------------------------------------------------
		// 13.4 <close statement>
		//-----------------------------------------------------------------------------

		const auto close_statement_def =
			keyword("close") >> cursor_name;

		BOOST_SPIRIT_DEFINE(close_statement);

		//-----------------------------------------------------------------------------
		// 13.5 <select statement single row>
		//-----------------------------------------------------------------------------

		const rule<class select_target_list> select_target_list = "select target list";

		const auto select_statement_single_row_def =
			keyword("select") >> -(set_quantifier) >> select_list >>
			keyword("into") >> select_target_list >> table_expression;

		const auto select_target_list_def =
			target_specification % lit(',');

		BOOST_SPIRIT_DEFINE(select_statement_single_row, select_target_list);

		//-----------------------------------------------------------------------------
		// 13.6 <delete statement positioned>
		//-----------------------------------------------------------------------------

		const auto delete_statement_positioned_def =
			keyword("delete") >> keyword("from") >> table_name >>
			keyword("where") >> keyword("current") >> keyword("of") >> cursor_name;

		BOOST_SPIRIT_DEFINE(delete_statement_positioned);

		//-----------------------------------------------------------------------------
		// 13.7 <delete statement searched>
		//-----------------------------------------------------------------------------

		const auto delete_statement_searched_def =
			keyword("delete") >> keyword("from") >> table_name >>
			-(keyword("where") >> search_condition);

		BOOST_SPIRIT_DEFINE(delete_statement_searched);

		//-----------------------------------------------------------------------------
		// 13.8 <insert statement>
		//-----------------------------------------------------------------------------

		const rule<class insert_columns_and_source> insert_columns_and_source = "insert columns and source";
		const rule<class set_clause_list> set_clause_list = "set clause list";
		const rule<class set_clause> set_clause = "set clause";

		const auto insert_statement_def =
			keyword("insert") >> keyword("into") >> table_name >>
			insert_columns_and_source;

		const auto insert_columns_and_source_def =
			(-(lit('(') >> insert_column_list >> lit(')')) >>
			 query_expression)
			| (keyword("default") >> keyword("values"));

		const auto insert_column_list_def = column_name_list;

		BOOST_SPIRIT_DEFINE(insert_statement, insert_columns_and_source, insert_column_list);

		//-----------------------------------------------------------------------------
		// 13.9 <update statement: positioned>
		//-----------------------------------------------------------------------------

		const rule<class update_source> update_source = "update source";
		const rule<class object_column> object_column = "object column";

		const auto update_statement_positioned_def =
			keyword("update") >> table_name >>
			keyword("set") >> set_clause_list >>
			keyword("where") >> keyword("current") >> keyword("of") >> cursor_name;

		const auto set_clause_list_def =
			set_clause % lit(',');

		const auto set_clause_def =
			object_column >> lit('=') >> update_source;

		const auto update_source_def =
			value_expression
			| null_specification
			| keyword("default");

		const auto object_column_def = column_name;

		BOOST_SPIRIT_DEFINE(update_statement_positioned, set_clause_list, set_clause, update_source);

		//-----------------------------------------------------------------------------
		// 13.10 <update statement: searched>
		//-----------------------------------------------------------------------------

		const auto update_statement_searched_def =
			keyword("update") >> table_name >>
			keyword("set") >> set_clause_list >>
			-(keyword("where") >> search_condition);

		BOOST_SPIRIT_DEFINE(update_statement_searched);

		//-----------------------------------------------------------------------------
		// 13.11 <temporary table declaration>
		//-----------------------------------------------------------------------------

		const auto temporary_table_declaration_def =
			keyword("declare") >> keyword("local") >> keyword("temporary") >> keyword("table") >>
			qualified_local_table_name >> table_element_list >>
			-(keyword("on") >> keyword("commit") >> (keyword("preserve") | keyword("delete")) >> keyword("rows"));

		BOOST_SPIRIT_DEFINE(temporary_table_declaration);

		//-----------------------------------------------------------------------------
		// 14 Transaction management
		//-----------------------------------------------------------------------------

		//-----------------------------------------------------------------------------
		// 14.1 <set transaction statement>
		//-----------------------------------------------------------------------------

		const rule<class transaction_mode> transaction_mode = "transaction mode";
		const rule<class transaction_access_mode> transaction_access_mode = "transaction access mode";
		const rule<class isolation_level> isolation_level = "isolation level";
		const rule<class level_of_isolation> level_of_isolation = "level of isolation";
		const rule<class diagnostics_size> diagnostics_size = "diagnostics size";
		const rule<class number_of_conditions> number_of_conditions = "number of conditions";

		const auto set_transaction_statement_def =
			keyword("set") >> keyword("transaction") >> (transaction_mode % lit(','));


		const auto transaction_mode_def =
			isolation_level
			| transaction_access_mode
			| diagnostics_size;

		const auto transaction_access_mode_def =
			(keyword("read") >> keyword("only"))
			| (keyword("read") >> keyword("write"));

		const auto isolation_level_def =
			keyword("isolation") >> keyword("level") >> level_of_isolation;

		const auto level_of_isolation_def =
			(keyword("read") >> keyword("uncommitted"))
			| (keyword("read") >> keyword("committed"))
			| (keyword("repeatable") >> keyword("read"))
			| keyword("serializable");

		const auto diagnostics_size_def =
			keyword("diagnostics") >> keyword("size") >> number_of_conditions;

		const auto number_of_conditions_def = simple_value_specification;

		BOOST_SPIRIT_DEFINE(set_transaction_statement, transaction_mode, transaction_access_mode,
				    isolation_level, level_of_isolation, diagnostics_size, number_of_conditions);

		//-----------------------------------------------------------------------------
		// 14.2 <set constraints mode statement>
		//-----------------------------------------------------------------------------

		const rule<class constraint_name_list> constraint_name_list = "constraint name list";

		const auto set_constraints_mode_statement_def =
			keyword("set") >> keyword("constraints") >> constraint_name_list >> (keyword("deferred") | keyword("immediate"));

		const auto constraint_name_list_def =
			keyword("all")
			| (constraint_name % lit(','));

		BOOST_SPIRIT_DEFINE(set_constraints_mode_statement, constraint_name_list);

		//-----------------------------------------------------------------------------
		// 14.3 <commit statement>
		//-----------------------------------------------------------------------------

		const auto commit_statement_def =
			keyword("commit") >> -(keyword("work"));

		BOOST_SPIRIT_DEFINE(commit_statement);

		//-----------------------------------------------------------------------------
		// 14.4 <rollback statement>
		//-----------------------------------------------------------------------------

		const auto rollback_statement_def =
			keyword("rollback") >> -(keyword("work"));

		BOOST_SPIRIT_DEFINE(rollback_statement);

		//-----------------------------------------------------------------------------
		// 15 Connection management
		//-----------------------------------------------------------------------------

		//-----------------------------------------------------------------------------
		// 15.1 <connect statement>
		//-----------------------------------------------------------------------------

		const rule<class connection_target> connection_target = "connection target";

		const auto connect_statement_def =
			keyword("connect") >> keyword("to") >> connection_target;

		const auto connection_target_def =
			(SQL_server_name >>
			 -(keyword("as") >> connection_name) >>
			 -(keyword("user") >> user_name))
			| keyword("default");

		BOOST_SPIRIT_DEFINE(connect_statement, connection_target);

		//-----------------------------------------------------------------------------
		// 15.2 <set connection statement>
		//-----------------------------------------------------------------------------

		const rule<class connection_object> connection_object = "connection object";

		const auto set_connection_statement_def =
			keyword("set") >> keyword("connection") >> connection_object;

		const auto connection_object_def =
			keyword("default")
			| connection_name;

		BOOST_SPIRIT_DEFINE(set_connection_statement,connection_object);

		//-----------------------------------------------------------------------------
		// 15.3 <disconnect statement>
		//-----------------------------------------------------------------------------

		const rule<class disconnect_object> disconnect_object = "disconnect object";

		const auto disconnect_statement_def =
			keyword("disconnect") >> disconnect_object;

		const auto disconnect_object_def =
			connection_object
			| keyword("all")
			| keyword("current");

		BOOST_SPIRIT_DEFINE(disconnect_statement, disconnect_object);

		//-----------------------------------------------------------------------------
		// 16 Session management
		//-----------------------------------------------------------------------------

		//-----------------------------------------------------------------------------
		// 16.1 <set catalog statement>
		//-----------------------------------------------------------------------------

		const auto set_catalog_statement_def =
			keyword("set") >> keyword("catalog") >> value_specification;

		BOOST_SPIRIT_DEFINE(set_catalog_statement);

		//-----------------------------------------------------------------------------
		// 16.2 <set schema statement>
		//-----------------------------------------------------------------------------

		const auto set_schema_statement_def =
			keyword("set") >> keyword("schema") >> value_specification;

		BOOST_SPIRIT_DEFINE(set_schema_statement);

		//-----------------------------------------------------------------------------
		// 16.3 <set names statement>
		//-----------------------------------------------------------------------------

		const auto set_names_statement_def =
			keyword("set") >> keyword("names") >> value_specification;

		BOOST_SPIRIT_DEFINE(set_names_statement);

		//-----------------------------------------------------------------------------
		// 16.4 <set session authorization identifier statement>
		//-----------------------------------------------------------------------------

		const auto set_session_authorization_identifier_statement_def =
			keyword("set") >> keyword("session") >> keyword("authorization") >> value_specification;

		BOOST_SPIRIT_DEFINE(set_session_authorization_identifier_statement);

		//-----------------------------------------------------------------------------
		// 16.5 <set local time zone statement>
		//-----------------------------------------------------------------------------

		const rule<class set_time_zone_value> set_time_zone_value = "set time zone value";

		const auto set_local_time_zone_statement_def =
			keyword("set") >> keyword("time") >> keyword("zone") >> set_time_zone_value;

		const auto set_time_zone_value_def =
			interval_value_expression
			| keyword("local");

		BOOST_SPIRIT_DEFINE(set_local_time_zone_statement,set_time_zone_value);

		//-----------------------------------------------------------------------------
		// 17 Dynamic SQL
		//-----------------------------------------------------------------------------

		//-----------------------------------------------------------------------------
		// 17.2 <allocate descriptor statement>
		//-----------------------------------------------------------------------------

		const rule<class occurrences> occurrences = "occurrences";

		const auto allocate_descriptor_statement_def =
			keyword("allocate") >> keyword("descriptor") >> descriptor_name >>
			-(keyword("with") >> keyword("max") >> occurrences);

		const auto occurrences_def = simple_value_specification;

		BOOST_SPIRIT_DEFINE(allocate_descriptor_statement, occurrences);

		//-----------------------------------------------------------------------------
		// 17.3 <deallocate descriptor statement>
		//-----------------------------------------------------------------------------

		const auto deallocate_descriptor_statement_def =
			keyword("deallocate") >> keyword("descriptor") >> descriptor_name;

		BOOST_SPIRIT_DEFINE(deallocate_descriptor_statement);

		//-----------------------------------------------------------------------------
		// 17.4 <get descriptor statement>
		//-----------------------------------------------------------------------------

		const rule<class get_descriptor_information> get_descriptor_information = "get descriptor information";
		const rule<class get_count> get_count = "get count";
		const rule<class get_item_information> get_item_information = "get item information";
		const rule<class simple_target_specification_1> simple_target_specification_1 = "simple target specification 1";
		const rule<class simple_target_specification_2> simple_target_specification_2 = "simple target specification 2";
		const rule<class descriptor_item_name> descriptor_item_name = "descriptor item name";

		const auto get_descriptor_statement_def =
			keyword("get") >> keyword("descriptor") >> descriptor_name >> get_descriptor_information;

		const auto get_descriptor_information_def =
			get_count
			| (keyword("value") >> item_number >>
			   (get_item_information % lit(',')));

		const auto get_count_def =
			simple_target_specification_1 >> lit('=') >> keyword("count");

		const auto get_item_information_def =
			simple_target_specification_2 >> lit('=') >> descriptor_item_name;

		const auto item_number_def = simple_value_specification;

		const auto simple_target_specification_1_def = simple_target_specification;

		const auto simple_target_specification_2_def = simple_target_specification;

		const auto descriptor_item_name_def =
			keyword("type")
			| keyword("length")
			| keyword("octet_length")
			| keyword("returned_length")
			| keyword("returned_octet_length")
			| keyword("precision")
			| keyword("scale")
			| keyword("datetime_interval_code")
			| keyword("datetime_interval_precision")
			| keyword("nullable")
			| keyword("indicator")
			| keyword("data")
			| keyword("name")
			| keyword("unnamed")
			| keyword("collation_catalog")
			| keyword("collation_schema")
			| keyword("collation_name")
			| keyword("character_set_catalog")
			| keyword("character_set_schema")
			| keyword("character_set_name");

		BOOST_SPIRIT_DEFINE(get_descriptor_statement, get_descriptor_information, get_count,
				    get_item_information, item_number, simple_target_specification_1,
				    simple_target_specification_2, descriptor_item_name);

		//-----------------------------------------------------------------------------
		// 17.5 <set descriptor statement>
		//-----------------------------------------------------------------------------

		const rule<class set_descriptor_information> set_descriptor_information = "set descriptor information";
		const rule<class set_count> set_count = "set count";
		const rule<class set_item_information> set_item_information = "set item information";
		const rule<class simple_value_specification_1> simple_value_specification_1 = "simple value specification 1";
		const rule<class simple_value_specification_2> simple_value_specification_2 = "simple value specification 2";

		const auto set_descriptor_statement_def =
			keyword("set") >> keyword("descriptor") >> descriptor_name >> set_descriptor_information;

		const auto set_descriptor_information_def =
			set_count
			| (keyword("value") >> item_number >>
			   (set_item_information % lit(',')));

		const auto set_count_def =
			keyword("count") >> lit('=') >> simple_value_specification_1;

		const auto set_item_information_def =
			descriptor_item_name >> lit('=') >> simple_value_specification_2;

		const auto simple_value_specification_1_def = simple_value_specification;

		const auto simple_value_specification_2_def = simple_value_specification;

		BOOST_SPIRIT_DEFINE(set_descriptor_statement, set_descriptor_information, set_count,
				    set_item_information, simple_value_specification_1,
				    simple_value_specification_2);

		//-----------------------------------------------------------------------------
		// 17.6 <prepare statement>
		//-----------------------------------------------------------------------------

		const rule<class preparable_SQL_data_statement> preparable_SQL_data_statement = "preparable L data statement";
		const rule<class preparable_SQL_schema_statement> preparable_SQL_schema_statement = "preparable L schema statement";
		const rule<class preparable_SQL_transaction_statement> preparable_SQL_transaction_statement = "preparable L transaction statement";
		const rule<class preparable_SQL_session_statement> preparable_SQL_session_statement = "preparable L session statement";

		const auto prepare_statement_def =
			keyword("prepare") >> SQL_statement_name >> keyword("from") >> SQL_statement_variable;

		const auto SQL_statement_variable_def = simple_value_specification;

		const auto preparable_statement_def =
			preparable_SQL_data_statement
			| preparable_SQL_schema_statement
			| preparable_SQL_transaction_statement
			| preparable_SQL_session_statement
			| preparable_implementation_defined_statement;

		const auto preparable_SQL_data_statement_def =
			delete_statement_searched
			| dynamic_single_row_select_statement
			| insert_statement
			| dynamic_select_statement
			| update_statement_searched
			| preparable_dynamic_delete_statement_positioned
			| preparable_dynamic_update_statement_positioned;

		const auto preparable_SQL_schema_statement_def =
			SQL_schema_statement;

		const auto preparable_SQL_transaction_statement_def =
			SQL_transaction_statement;

		const auto preparable_SQL_session_statement_def =
			SQL_session_statement;

		const auto dynamic_select_statement_def = cursor_specification;

		const auto dynamic_single_row_select_statement_def = query_specification;

		const auto preparable_implementation_defined_statement_def = eps(false);

		BOOST_SPIRIT_DEFINE(prepare_statement, SQL_statement_variable, preparable_statement,
				    preparable_SQL_data_statement, preparable_SQL_schema_statement,
				    preparable_SQL_transaction_statement, preparable_SQL_session_statement,
				    dynamic_select_statement, dynamic_single_row_select_statement,
				    preparable_implementation_defined_statement);

		//-----------------------------------------------------------------------------
		// 17.7 <deallocate prepared statement>
		//-----------------------------------------------------------------------------

		const auto deallocate_prepared_statement_def =
			keyword("deallocate") >> keyword("prepare") >> SQL_statement_name;

		BOOST_SPIRIT_DEFINE(deallocate_prepared_statement);

		//-----------------------------------------------------------------------------
		// 17.8 <describe statement>
		//-----------------------------------------------------------------------------

		const auto describe_statement_def =
			describe_input_statement
			| describe_output_statement;

		const auto describe_input_statement_def =
			keyword("describe") >> keyword("input") >> SQL_statement_name >> using_descriptor;

		const auto describe_output_statement_def =
			keyword("describe") >> -(keyword("output")) >> SQL_statement_name >> using_descriptor;

		BOOST_SPIRIT_DEFINE(describe_statement, describe_input_statement, describe_output_statement);

		//-----------------------------------------------------------------------------
		// 17.9 <using clause>
		//-----------------------------------------------------------------------------

		const rule<class using_arguments> using_arguments = "using arguments";
		const rule<class argument> argument = "argument";

		const auto using_clause_def =
			using_arguments
			| using_descriptor;

		const auto using_arguments_def =
			(keyword("using") | keyword("into")) >> (argument % lit(','));

		const auto argument_def = target_specification;

		const auto using_descriptor_def =
			(keyword("using") | keyword("into")) >> keyword("sql") >> keyword("descriptor") >> descriptor_name;

		BOOST_SPIRIT_DEFINE(using_clause, using_arguments, argument, using_descriptor);

		//-----------------------------------------------------------------------------
		// 17.10 <execute statement>
		//-----------------------------------------------------------------------------

		const auto execute_statement_def =
			keyword("execute") >> SQL_statement_name >>
			-(result_using_clause) >>
			-(parameter_using_clause);

		const auto result_using_clause_def = using_clause;

		const auto parameter_using_clause_def = using_clause;

		BOOST_SPIRIT_DEFINE(execute_statement, result_using_clause, parameter_using_clause);

		//-----------------------------------------------------------------------------
		// 17.11 <execute immediate statement>
		//-----------------------------------------------------------------------------

		const auto execute_immediate_statement_def =
			keyword("execute") >> keyword("immediate") >> SQL_statement_variable;

		BOOST_SPIRIT_DEFINE(execute_immediate_statement);

		//-----------------------------------------------------------------------------
		// 17.12 <dynamic declare cursor>
		//-----------------------------------------------------------------------------

		const auto dynamic_declare_cursor_def =
			keyword("declare") >> cursor_name >> -(keyword("insensitive")) >> -(keyword("scroll")) >>
			keyword("cursor") >> keyword("for") >> statement_name;

		BOOST_SPIRIT_DEFINE(dynamic_declare_cursor);

		//-----------------------------------------------------------------------------
		// 17.13 <allocate cursor statement>
		//-----------------------------------------------------------------------------

		const auto allocate_cursor_statement_def =
			keyword("allocate") >> extended_cursor_name >> -(keyword("insensitive")) >> -(keyword("scroll")) >>
			keyword("cursor") >> keyword("for") >> extended_statement_name;

		BOOST_SPIRIT_DEFINE(allocate_cursor_statement);

		//-----------------------------------------------------------------------------
		// 17.14 <dynamic open statement>
		//-----------------------------------------------------------------------------

		const auto dynamic_open_statement_def =
			keyword("open") >> dynamic_cursor_name >> -(using_clause);

		BOOST_SPIRIT_DEFINE(dynamic_open_statement);

		//-----------------------------------------------------------------------------
		// 17.15 <dynamic fetch statement>
		//-----------------------------------------------------------------------------

		const auto dynamic_fetch_statement_def =
			keyword("fetch") >> -(-(fetch_orientation) >> keyword("from")) >>
			dynamic_cursor_name >> using_clause;

		BOOST_SPIRIT_DEFINE(dynamic_fetch_statement);

		//-----------------------------------------------------------------------------
		// 17.16 <dynamic close statement>
		//-----------------------------------------------------------------------------

		const auto dynamic_close_statement_def =
			keyword("close") >> dynamic_cursor_name;

		BOOST_SPIRIT_DEFINE(dynamic_close_statement);

		//-----------------------------------------------------------------------------
		// 17.17 <dynamic delete statement: positioned>
		//-----------------------------------------------------------------------------

		const auto dynamic_delete_statement_positioned_def =
			keyword("delete") >> keyword("from") >> table_name >>
			keyword("where") >> keyword("current") >> keyword("of") >> dynamic_cursor_name;

		BOOST_SPIRIT_DEFINE(dynamic_delete_statement_positioned);

		//-----------------------------------------------------------------------------
		// 17.18 <dynamic update statement: positioned>
		//-----------------------------------------------------------------------------

		const auto dynamic_update_statement_positioned_def =
			keyword("update") >> table_name >>
			keyword("set") >> (set_clause % lit(',')) >>
			keyword("where") >> keyword("current") >> keyword("of") >> dynamic_cursor_name;

		BOOST_SPIRIT_DEFINE(dynamic_update_statement_positioned);

		//-----------------------------------------------------------------------------
		// 17.19 <preparable dynamic delete statement: positioned>
		//-----------------------------------------------------------------------------

		const auto preparable_dynamic_delete_statement_positioned_def =
			keyword("delete") >> -(keyword("from") >> table_name) >>
			keyword("where") >> keyword("current") >> keyword("of") >> cursor_name;

		BOOST_SPIRIT_DEFINE(preparable_dynamic_delete_statement_positioned);

		//-----------------------------------------------------------------------------
		// 17.20 <preparable dynamic update statement: positioned>
		//-----------------------------------------------------------------------------

		const auto preparable_dynamic_update_statement_positioned_def =
			keyword("update") >> -(table_name) >>
			keyword("set") >> set_clause_list >>
			keyword("where") >> keyword("current") >> keyword("of") >> cursor_name;

		BOOST_SPIRIT_DEFINE(preparable_dynamic_update_statement_positioned);

		//-----------------------------------------------------------------------------
		// 18 Diagnostics management
		//-----------------------------------------------------------------------------

		//-----------------------------------------------------------------------------
		// 18.1 <get diagnostics statement>
		//-----------------------------------------------------------------------------

		const rule<class sql_diagnostics_information> sql_diagnostics_information = "sql diagnostics information";
		const rule<class statement_information> statement_information = "statement information";
		const rule<class statement_information_item> statement_information_item = "statement information item";
		const rule<class statement_information_item_name> statement_information_item_name = "statement information item name";
		const rule<class condition_information> condition_information = "condition information";
		const rule<class condition_information_item> condition_information_item = "condition information item";
		const rule<class condition_information_item_name> condition_information_item_name = "condition information item name";
		const rule<class condition_number> condition_number = "condition number";

		const auto get_diagnostics_statement_def =
			keyword("get") >> keyword("diagnostics") >> sql_diagnostics_information;

		const auto sql_diagnostics_information_def =
			statement_information
			| condition_information;

		const auto statement_information_def =
			statement_information_item % lit(',');


		const auto statement_information_item_def =
			simple_target_specification >> lit('=') >> statement_information_item_name;


		const auto statement_information_item_name_def =
			keyword("number")
			| keyword("more")
			| keyword("command_function")
			| keyword("dynamic_function")
			| keyword("row_count");

		const auto condition_information_def =
			keyword("exception") >> condition_number >>
			(condition_information_item % lit(','));

		const auto condition_information_item_def =
			simple_target_specification >> lit('=') >> condition_information_item_name;


		const auto condition_information_item_name_def =
			keyword("condition_number")
			| keyword("returned_sqlstate")
			| keyword("class_origin")
			| keyword("subclass_origin")
			| keyword("server_name")
			| keyword("connection_name")
			| keyword("constraint_catalog")
			| keyword("constraint_schema")
			| keyword("constraint_name")
			| keyword("catalog_name")
			| keyword("schema_name")
			| keyword("table_name")
			| keyword("column_name")
			| keyword("cursor_name")
			| keyword("message_text")
			| keyword("message_length")
			| keyword("message_octet_length");

		const auto condition_number_def = simple_value_specification;

		BOOST_SPIRIT_DEFINE(get_diagnostics_statement, sql_diagnostics_information,
				    statement_information, statement_information_item,
				    statement_information_item_name, condition_information,
				    condition_information_item, condition_information_item_name);

		//-----------------------------------------------------------------------------
		// 19 Embedded SQL
		//-----------------------------------------------------------------------------

		//-----------------------------------------------------------------------------
		// 19.1 <embedded SQL host program>
		//-----------------------------------------------------------------------------

		const rule<class statement_or_declaration> statement_or_declaration = "statement or declaration";
		const rule<class SQL_prefix> SQL_prefix = "SQL prefix";
		const rule<class SQL_terminator> SQL_terminator = "SQL terminator";
		const rule<class embedded_character_set_declaration> embedded_character_set_declaration = "embedded character set declaration";
		const rule<class embedded_SQL_end_declare> embedded_SQL_end_declare = "embedded SQL end declare";
		const rule<class embedded_SQL_MUMPS_declare> embedded_SQL_MUMPS_declare = "embedded SQL MUMPS declare";
		const rule<class host_variable_definition> host_variable_definition = "host variable definition";
		const rule<class host_identifier> host_identifier = "host identifier";

		const auto embedded_SQL_host_program_def =
			embedded_SQL_Ada_program
			| embedded_SQL_C_program
			| embedded_SQL_COBOL_program
			| embedded_SQL_Fortran_program
			| embedded_SQL_MUMPS_program
			| embedded_SQL_Pascal_program
			| embedded_SQL_PLI_program;

		const auto embedded_SQL_statement_def =
			SQL_prefix >>
			statement_or_declaration >>
			-(SQL_terminator);

		const auto statement_or_declaration_def =
			declare_cursor
			| dynamic_declare_cursor
			| temporary_table_declaration
			| embedded_exception_declaration
			| SQL_procedure_statement;

		const auto SQL_prefix_def =
			(keyword("exec") >> keyword("sql"))
			| (lit('&') >> keyword("sql") >> lit('('));

		const auto SQL_terminator_def =
			keyword("end-exec")
			| lit(';')
			| lit(')');

		const auto embedded_SQL_declare_section_def =
			(embedded_SQL_begin_declare >>
			 -(embedded_character_set_declaration) >>
			 *host_variable_definition >>
			 embedded_SQL_end_declare)
			| embedded_SQL_MUMPS_declare;

		const auto embedded_character_set_declaration_def =
			keyword("sql") >> keyword("names") >> keyword("are") >> character_set_specification;

		const auto embedded_SQL_begin_declare_def =
			SQL_prefix >> keyword("begin") >> keyword("declare") >> keyword("section") >> -(SQL_terminator);

		const auto embedded_SQL_end_declare_def =
			SQL_prefix >> keyword("end") >> keyword("declare") >> keyword("section") >> -(SQL_terminator);

		const auto embedded_SQL_MUMPS_declare_def =
			SQL_prefix >>
			keyword("begin") >> keyword("declare") >> keyword("section") >>
			-(embedded_character_set_declaration) >>
			*host_variable_definition >>
			keyword("end") >> keyword("declare") >> keyword("section") >>
			SQL_terminator;

		const auto host_variable_definition_def =
			Ada_variable_definition
			| C_variable_definition
			| COBOL_variable_definition
			| Fortran_variable_definition
			| MUMPS_variable_definition
			| Pascal_variable_definition
			| PLI_variable_definition;

		const auto embedded_variable_name_def =
			lit(':') >> host_identifier;

		const auto host_identifier_def =
			Ada_host_identifier
			| C_host_identifier
			| COBOL_host_identifier
			| Fortran_host_identifier
			| MUMPS_host_identifier
			| Pascal_host_identifier
			| PLI_host_identifier;

		BOOST_SPIRIT_DEFINE(embedded_SQL_host_program, embedded_SQL_statement,
				    statement_or_declaration, SQL_prefix, SQL_terminator,
				    embedded_SQL_declare_section, embedded_character_set_declaration,
				    embedded_SQL_begin_declare, embedded_SQL_end_declare,
				    embedded_SQL_MUMPS_declare, host_variable_definition,
				    embedded_variable_name, host_identifier);

		//-----------------------------------------------------------------------------
		// 19.2 <embedded exception declaration>
		//-----------------------------------------------------------------------------

		const rule<class condition> condition = "condition";
		const rule<class condition_action> condition_action = "condition action";
		const rule<class go_to> go_to = "go to";
		const rule<class goto_target> goto_target = "goto target";
		const rule<class host_label_identifier> host_label_identifier = "host label identifier";
		const rule<class host_PLI_label_variable> host_PLI_label_variable = "host PL/I label variable";

		const auto embedded_exception_declaration_def =
			keyword("whenever") >> condition >> condition_action;

		const auto condition_def =
			keyword("sqlerror") | (keyword("not") >> keyword("found"));

		const auto condition_action_def =
			keyword("continue") | go_to;

		const auto go_to_def =
			(keyword("goto") | (keyword("go") >> keyword("to"))) >> goto_target;

		const auto goto_target_def =
			host_label_identifier
			| unsigned_integer
			| host_PLI_label_variable;

		const auto host_label_identifier_def = eps(false);

		const auto host_PLI_label_variable_def = eps(false);

		BOOST_SPIRIT_DEFINE(embedded_exception_declaration, condition, condition_action, go_to,
				    goto_target, host_label_identifier, host_PLI_label_variable);

		//-----------------------------------------------------------------------------
		// 19.3 <embedded SQL Ada program>
		//-----------------------------------------------------------------------------

		const rule<class Ada_initial_value> Ada_initial_value = "Ada initial value";
		const rule<class Ada_assignment_operator> Ada_assignment_operator = "Ada assignment operator";
		const rule<class Ada_type_specification> Ada_type_specification = "Ada type specification";
		const rule<class Ada_qualified_type_specification> Ada_qualified_type_specification = "Ada qualified type specification";
		const rule<class Ada_unqualified_type_specification> Ada_unqualified_type_specification = "Ada unqualified type specification";

		const auto embedded_SQL_Ada_program_def = eps(false);

		const auto Ada_variable_definition_def =
			(Ada_host_identifier % lit(',')) >>
			Ada_type_specification >> -(Ada_initial_value);

		const auto Ada_initial_value_def =
			Ada_assignment_operator >> +character_representation;

		const auto Ada_assignment_operator_def = lexeme[lit(":=")];

		const auto Ada_host_identifier_def = eps(false);

		const auto Ada_type_specification_def =
			Ada_qualified_type_specification
			| Ada_unqualified_type_specification;

		const auto Ada_qualified_type_specification_def =
			(keyword("sql_standard.char") >> -(keyword("character") >> keyword("set") >> -(keyword("is")) >> character_set_specification) >>
			 lit('(') >> lit('1') >> lexeme[lit("::")] >> length >> lit(')'))
			| (keyword("sql_standard.bit") >> lit('(') >> lit('1') >> lexeme[lit("::")] >> length >> lit(')'))
			| keyword("sql_standard.smallint")
			| keyword("sql_standard.int")
			| keyword("sql_standard.real")
			| keyword("sql_standard.double_precision")
			| keyword("sql_standard.sqlcode_type")
			| keyword("sql_standard.sqlstate_type")
			| keyword("sql_standard.indicator_type");

		const auto Ada_unqualified_type_specification_def =
			(keyword("char") >> lit('(') >> lit('1') >> lexeme[lit("::")] >> length >> lit(')'))
			| (keyword("bit") >> lit('(') >> lit('1') >> lexeme[lit("::")] >> length >> lit(')'))
			| keyword("smallint")
			| keyword("int")
			| keyword("real")
			| keyword("double_precision")
			| keyword("sqlcode_type")
			| keyword("sqlstate_type")
			| keyword("indicator_type");


		BOOST_SPIRIT_DEFINE(embedded_SQL_Ada_program, Ada_variable_definition, Ada_initial_value,
				    Ada_assignment_operator, Ada_host_identifier, Ada_type_specification,
				    Ada_qualified_type_specification, Ada_unqualified_type_specification);

		//-----------------------------------------------------------------------------
		// 19.4 <embedded SQL C program>
		//-----------------------------------------------------------------------------

		const rule<class C_variable_specification> C_variable_specification = "C variable specification";
		const rule<class C_storage_class> C_storage_class = "C storage class";
		const rule<class C_class_modifier> C_class_modifier = "C class modifier";
		const rule<class C_numeric_variable> C_numeric_variable = "C numeric variable";
		const rule<class C_character_variable> C_character_variable = "C character variable";
		const rule<class C_array_specification> C_array_specification = "C array specification";
		const rule<class C_derived_variable> C_derived_variable = "C derived variable";
		const rule<class C_VARCHAR_variable> C_VARCHAR_variable = "C R variable";
		const rule<class C_bit_variable> C_bit_variable = "C bit variable";
		const rule<class C_initial_value> C_initial_value = "C initial value";

		const auto embedded_SQL_C_program_def = eps(false);

		const auto C_variable_definition_def =
			-(C_storage_class) >>
			-(C_class_modifier) >>
			C_variable_specification >>
			lit(';');

		const auto C_variable_specification_def =
			C_numeric_variable
			| C_character_variable
			| C_derived_variable;

		const auto C_storage_class_def =
			keyword("auto")
			| keyword("extern")
			| keyword("static");

		const auto C_class_modifier_def = keyword("const") | keyword("volatile");

		const auto C_numeric_variable_def =
			(keyword("long") | keyword("short") | keyword("float") | keyword("double")) >>
			C_host_identifier >> -(C_initial_value) >>
			*(lit(',') >> C_host_identifier >> -(C_initial_value));

		const auto C_character_variable_def =
			keyword("char") >> -(keyword("character") >>  keyword("set") >> -(keyword("is")) >> character_set_specification) >>
			C_host_identifier >> C_array_specification >> -(C_initial_value) >>
			*(lit(',') >> C_host_identifier >> C_array_specification >> -(C_initial_value));

		const auto C_array_specification_def =
			lit('[') >> length >> lit(']');

		const auto C_host_identifier_def = eps(false);

		const auto C_derived_variable_def =
			C_VARCHAR_variable
			| C_bit_variable;

		const auto C_VARCHAR_variable_def =
			keyword("varchar") >> -(keyword("character") >> keyword("set") >> -(keyword("is")) >> character_set_specification) >>
			C_host_identifier >> C_array_specification >> -(C_initial_value) >>
			*(lit(',') >> C_host_identifier >> C_array_specification >> -(C_initial_value));

		const auto C_bit_variable_def =
			keyword("bit") >> C_host_identifier >> C_array_specification >> -(C_initial_value) >>
			*(lit(',') >> C_host_identifier >> C_array_specification >> -(C_initial_value));

		const auto C_initial_value_def =
			lit('=') >> +character_representation;

		BOOST_SPIRIT_DEFINE(embedded_SQL_C_program, C_variable_definition, C_variable_specification,
				    C_storage_class, C_class_modifier, C_numeric_variable,
				    C_character_variable, C_array_specification, C_host_identifier,
				    C_derived_variable, C_VARCHAR_variable, C_bit_variable, C_initial_value);

		//-----------------------------------------------------------------------------
		// 19.5 <embedded SQL COBOL program>
		//-----------------------------------------------------------------------------

		const rule<class COBOL_type_specification> COBOL_type_specification = "COBOL type specification";
		const rule<class COBOL_character_type> COBOL_character_type = "COBOL character type";
		const rule<class COBOL_bit_type> COBOL_bit_type = "COBOL bit type";
		const rule<class COBOL_numeric_type> COBOL_numeric_type = "COBOL numeric type";
		const rule<class COBOL_nines_specification> COBOL_nines_specification = "COBOL nines specification";
		const rule<class COBOL_integer_type> COBOL_integer_type = "COBOL integer type";
		const rule<class COBOL_computational_integer> COBOL_computational_integer = "COBOL computational integer";
		const rule<class COBOL_binary_integer> COBOL_binary_integer = "COBOL binary integer";
		const rule<class COBOL_nines> COBOL_nines = "COBOL nines";

		const auto embedded_SQL_COBOL_program_def = eps(false);

		const auto COBOL_variable_definition_def =
			(lexeme[lit("01")] | lexeme[lit("77")]) >> COBOL_host_identifier >> COBOL_type_specification >>
			+character_representation >> lit('.');

		const auto COBOL_host_identifier_def = eps(false);

		const auto COBOL_type_specification_def =
			COBOL_character_type
			| COBOL_bit_type
			| COBOL_numeric_type
			| COBOL_integer_type;

		const auto COBOL_character_type_def =
			-(keyword("character") >> keyword("set") >> -(keyword("is")) >> character_set_specification) >>
			(keyword("pic") | keyword("picture")) >> -(keyword("is")) >> +(no_case[lit('x')] >> -(lit('(') >> length >> lit(')')));

		const auto COBOL_bit_type_def =
			(keyword("pic") | keyword("picture")) >> -(keyword("is")) >> +(no_case[lit('b')] >> -(lit('(') >> length >> lit(')')));

		const auto COBOL_numeric_type_def =
			(keyword("pic") | keyword("picture")) >> -(keyword("is")) >>
			no_case[lit('s')] >> COBOL_nines_specification >>
			-(keyword("usage") >> -(keyword("is"))) >> keyword("display") >> keyword("sign") >> keyword("leading") >> keyword("separate");

		const auto COBOL_nines_specification_def =
			(COBOL_nines >> -(no_case[lit('v')] >> -(COBOL_nines)))
			| (no_case[lit('v')] >> COBOL_nines);

		const auto COBOL_integer_type_def =
			COBOL_computational_integer
			| COBOL_binary_integer;

		const auto COBOL_computational_integer_def =
			(keyword("pic") | keyword("picture")) >> -(keyword("is")) >> no_case[lit('s')] >> COBOL_nines >>
			-(keyword("usage") >> -(keyword("is"))) >> (keyword("comp") | keyword("computational"));

		const auto COBOL_binary_integer_def =
			(keyword("pic") | keyword("picture")) >> -(keyword("is")) >> no_case[lit('s')] >> COBOL_nines >>
			-(keyword("usage") >> -(keyword("is"))) >> keyword("binary");

		const auto COBOL_nines_def = +(lit('9') >> -(lit('(') >> length >> lit(')')));

		BOOST_SPIRIT_DEFINE(embedded_SQL_COBOL_program, COBOL_variable_definition,
				    COBOL_host_identifier, COBOL_type_specification, COBOL_character_type,
				    COBOL_bit_type, COBOL_numeric_type, COBOL_nines_specification,
				    COBOL_integer_type, COBOL_computational_integer, COBOL_binary_integer,
				    COBOL_nines);

		//-----------------------------------------------------------------------------
		// 19.6 <embedded SQL Fortran program>
		//-----------------------------------------------------------------------------

		const rule<class Fortran_type_specification> Fortran_type_specification = "Fortran type specification";

		const auto embedded_SQL_Fortran_program_def = eps(false);

		const auto Fortran_variable_definition_def =
			Fortran_type_specification >>
			(Fortran_host_identifier % lit(','));

		const auto Fortran_host_identifier_def = eps(false);

		const auto Fortran_type_specification_def =
			(keyword("character") >> -(lit('*') >> length) >>
			 -(keyword("character") >> keyword("set") >> -(keyword("is")) >> character_set_specification))
			| (keyword("bit") >> -(lit('*') >> length))
			| keyword("integer")
			| keyword("real")
			| (keyword("double") >> keyword("precision"));

		BOOST_SPIRIT_DEFINE(embedded_SQL_Fortran_program, Fortran_variable_definition,
				    Fortran_host_identifier, Fortran_type_specification);

		//-----------------------------------------------------------------------------
		// 19.7 <embedded SQL MUMPS program>
		//-----------------------------------------------------------------------------

		const rule<class MUMPS_character_variable> MUMPS_character_variable = "S character variable";
		const rule<class MUMPS_length_specification> MUMPS_length_specification = "S length specification";
		const rule<class MUMPS_numeric_variable> MUMPS_numeric_variable = "S numeric variable";
		const rule<class MUMPS_type_specification> MUMPS_type_specification = "S type specification";

		const auto embedded_SQL_MUMPS_program_def = eps(false);

		const auto MUMPS_variable_definition_def =
			(MUMPS_numeric_variable | MUMPS_character_variable) >> lit(';');

		const auto MUMPS_character_variable_def =
			keyword("varchar") >> MUMPS_host_identifier >>
			(MUMPS_length_specification % lit(','));

		const auto MUMPS_host_identifier_def = eps(false);

		const auto MUMPS_length_specification_def =
			lit('(') >> length >> lit(')');

		const auto MUMPS_numeric_variable_def =
			MUMPS_type_specification >>
			(MUMPS_host_identifier % lit(','));

		const auto MUMPS_type_specification_def =
			keyword("int")
			| (keyword("dec") >> -(lit('(') >> precision >> -(lit(',') >> scale) >> lit(')')))
			| keyword("real");

		BOOST_SPIRIT_DEFINE(embedded_SQL_MUMPS_program, MUMPS_variable_definition,
				    MUMPS_character_variable, MUMPS_host_identifier,
				    MUMPS_length_specification, MUMPS_numeric_variable,
				    MUMPS_type_specification);

		//-----------------------------------------------------------------------------
		// 19.8 <embedded SQL Pascal program>
		//-----------------------------------------------------------------------------

		const rule<class Pascal_type_specification> Pascal_type_specification = "Pascal type specification";

		const auto embedded_SQL_Pascal_program_def = eps(false);

		const auto Pascal_variable_definition_def =
			(Pascal_host_identifier % lit(',')) >> lit(':') >>
			Pascal_type_specification >> lit(';');

		const auto Pascal_host_identifier_def = eps(false);

		const auto Pascal_type_specification_def =
			(keyword("packed") >> keyword("array") >> lit('[') >> lit('1') >> lexeme[lit("::")] >> length >> lit(']') >>
			 keyword("of") >> keyword("char") >>
			 -(keyword("character") >> keyword("set") >> -(keyword("is")) >> character_set_specification))
			| (keyword("packed") >> keyword("array") >> lit('[') >> lit('1') >> lexeme[lit("::")] >> length >> lit(']') >>
			   keyword("of") >> keyword("bit"))
			| keyword("integer")
			| keyword("real")
			| (keyword("char") >> -(keyword("character") >> keyword("set") >> -(keyword("is")) >> character_set_specification))
			| keyword("bit");

		BOOST_SPIRIT_DEFINE(embedded_SQL_Pascal_program, Pascal_variable_definition, Pascal_host_identifier, Pascal_type_specification);

		//-----------------------------------------------------------------------------
		// 19.9 <embedded SQL PL/I program>
		//-----------------------------------------------------------------------------

		const rule<class PLI_type_specification> PLI_type_specification = "PL/I type specification";
		const rule<class PLI_type_fixed_decimal> PLI_type_fixed_decimal = "PL/I type fixed decimal";
		const rule<class PLI_type_fixed_binary> PLI_type_fixed_binary = "PL/I type fixed binary";
		const rule<class PLI_type_float_binary> PLI_type_float_binary = "PL/I type float binary";

		const auto embedded_SQL_PLI_program_def = eps(false);

		const auto PLI_variable_definition_def =
			(keyword("dcl") | keyword("declare")) >>
			(PLI_host_identifier
			 | (lit('(') >> (PLI_host_identifier % lit(',')) >> lit(')'))) >>
			PLI_type_specification >> *character_representation >> lit(';');

		const auto PLI_host_identifier_def = eps(false);

		const auto PLI_type_specification_def =
			((keyword("char") | keyword("character")) >> -(keyword("varying")) >> lit('(') >> length >> lit(')') >>
			 -(keyword("character") >> keyword("set") >> -(keyword("is")) >> character_set_specification))
			| (keyword("bit") >> -(keyword("varying")) >> lit('(') >> length >> lit(')'))
			| (PLI_type_fixed_decimal >> lit('(') >> precision >> -(lit(',') >> scale) >> lit(')'))
			| (PLI_type_fixed_binary >> -(lit('(') >> precision >> lit(')')))
			| (PLI_type_float_binary >> -(lit('(') >> precision >> lit(')')));

		const auto PLI_type_fixed_decimal_def =
			((keyword("dec") | keyword("decimal")) >> keyword("fixed"))
			| (keyword("fixed") >> (keyword("dec") | keyword("decimal")));

		const auto PLI_type_fixed_binary_def =
			((keyword("bin") | keyword("binary")) >> keyword("fixed"))
			| (keyword("fixed") | (keyword("bin") | keyword("binary")));

		const auto PLI_type_float_binary_def =
			((keyword("bin") | keyword("binary")) >> keyword("float"))
			| (keyword("float") >> (keyword("bin") | keyword("binary")));

		BOOST_SPIRIT_DEFINE(embedded_SQL_PLI_program, PLI_variable_definition, PLI_host_identifier,
				    PLI_type_specification, PLI_type_fixed_decimal, PLI_type_fixed_binary,
				    PLI_type_float_binary);

		//-----------------------------------------------------------------------------
		// 20 Direct invocation of SQL
		//-----------------------------------------------------------------------------

		//-----------------------------------------------------------------------------
		// 20.1 <direct SQL statement>
		//-----------------------------------------------------------------------------

		const rule<class directly_executable_statement> directly_executable_statement = "directly executable statement";
		const rule<class direct_SQL_data_statement> direct_SQL_data_statement = "direct L data statement";
		const rule<class direct_implementation_defined_statement> direct_implementation_defined_statement = "direct implementation-defined statement";

		const auto direct_SQL_statement_def =
			directly_executable_statement >> lit(';');

		const auto directly_executable_statement_def =
			direct_SQL_data_statement
			| SQL_schema_statement
			| SQL_transaction_statement
			| SQL_connection_statement
			| SQL_session_statement
			| direct_implementation_defined_statement;

		const auto direct_SQL_data_statement_def =
			delete_statement_searched
			| direct_select_statement_multiple_rows
			| insert_statement
			| update_statement_searched
			| temporary_table_declaration;

		const auto direct_implementation_defined_statement_def = eps(false);

		BOOST_SPIRIT_DEFINE(direct_SQL_statement, directly_executable_statement,
				    direct_SQL_data_statement, direct_implementation_defined_statement);

		//-----------------------------------------------------------------------------
		// 20.2 <direct select statement: multiple rows>
		//-----------------------------------------------------------------------------

		const auto direct_select_statement_multiple_rows_def =
			query_expression >> -(order_by_clause);

		BOOST_SPIRIT_DEFINE(direct_select_statement_multiple_rows);

		//------------------------------------------------------------------------------
		// Statement
		//------------------------------------------------------------------------------

		struct endofinput {
			template<typename Context>
			void operator()(Context& ctx) {
				//auto& g(get<parser_tag>(ctx).get());
				//g.finalize();
			}
		};

		const rule<struct file> file = "file";

		const auto file_def = /*subquery*/ lit('(') >> cursor_specification >> lit(')') >> -(keyword("as") >> identifier) >> eoi[endofinput{}];

		BOOST_SPIRIT_DEFINE(file);
	};
};

void CartoSQL::parse_sql(std::istream& is, std::ostream *msg)
{
	if (!is)
		throw std::runtime_error("Cannot read sql");
	namespace x3 = boost::spirit::x3;
	using Iterator = boost::spirit::istream_iterator;
	using PosIterator = boost::spirit::line_pos_iterator<Iterator>;
	std::vector<PosIterator> iters;
	Iterator f(is >> std::noskipws), l;
	PosIterator pf(f), pi(pf), pl;
	cartosql_parser<PosIterator> g(*this, pf, pl, msg);
	try {
		if (!x3::phrase_parse(pi, pl, x3::with<parser::parser_tag>(std::ref(g))[parser::file], skipper::skip))
			parse_error(pf, pi, pl);
	} catch (const x3::expectation_failure<PosIterator>& x) {
		parse_error(pf, x.where(), pl, x.which());
	}
	if (false)
		std::cerr << "finalizing..." << std::endl;
	try {
		g.finalize();
	} catch (const x3::expectation_failure<PosIterator>& x) {
		parse_error(pf, x.where(), pl, x.which());
	}
}

void CartoSQL::parse_sql_value(std::istream& is, std::ostream *msg)
{
	if (!is)
		throw std::runtime_error("Cannot read sql");
	namespace x3 = boost::spirit::x3;
	using Iterator = boost::spirit::istream_iterator;
	using PosIterator = boost::spirit::line_pos_iterator<Iterator>;
	std::vector<PosIterator> iters;
	Iterator f(is >> std::noskipws), l;
	PosIterator pf(f), pi(pf), pl;
	cartosql_parser<PosIterator> g(*this, pf, pl, msg);
	try {
		if (!x3::phrase_parse(pi, pl, x3::with<parser::parser_tag>(std::ref(g))[parser::value_expression >> x3::eoi], skipper::skip))
			parse_error(pf, pi, pl);
	} catch (const x3::expectation_failure<PosIterator>& x) {
		parse_error(pf, x.where(), pl, x.which());
	}
}
