//
// C++ Implementation: cartosqlparser
//
// Description: CartoCSS SQL parser
//
//
// Author: Thomas Sailer <t.sailer@alumni.ethz.ch>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#define BOOST_SPIRIT_X3_DEBUG
#include <boost/config/warning_disable.hpp>
#include <boost/spirit/home/x3.hpp>
#include <boost/fusion/include/vector.hpp>
#include <boost/fusion/include/make_deque.hpp>
#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/fusion/include/io.hpp>
#include <boost/fusion/adapted.hpp>
#include <boost/spirit/include/support_line_pos_iterator.hpp>
#include <boost/spirit/include/support_istream_iterator.hpp>

#include <boost/variant.hpp>
#include <boost/pool/object_pool.hpp>

#include <glibmm.h>
#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>

#include "cartosql.h"
#include "geom.h"

namespace {

	namespace x3 = boost::spirit::x3;
	namespace ascii = boost::spirit::x3::ascii;
	namespace fusion = boost::fusion;

	template<typename Iterator>
	void parse_error(const Iterator& begin, const Iterator& iter, const Iterator& end, const std::string& info = "") {
		std::ostringstream oss;
		oss << "parse error at";
		{
			std::size_t ln(boost::spirit::get_line(iter));
			if (ln != static_cast<std::size_t>(-1))
				oss << " line " << ln;
		}
		std::size_t col(boost::spirit::get_column(begin, iter, 8));
		oss << " column " << col << std::endl
		    << '\'' << boost::spirit::get_current_line(begin, iter, end) << '\'' << std::endl
		    <<std::setw(col) << ' ' << "^- here";
		if (!info.empty())
			oss << std::endl << info;
		throw std::runtime_error(oss.str());
	}

	template<typename Iterator>
	void parse_error(const Iterator& iter, const std::string& info = "") {
		std::ostringstream oss;
		oss << "parse error at";
		{
			std::size_t ln(boost::spirit::get_line(iter));
			if (ln != static_cast<std::size_t>(-1))
				oss << " line " << ln;
		}
		if (!info.empty())
			oss << std::endl << info;
		throw std::runtime_error(oss.str());
	}

	template<typename Iterator>
	struct cartosql_parser {
		cartosql_parser(CartoSQL& sql, std::ostream *msg = nullptr) : m_sql(sql), m_msg(msg) {
		}

		void finalize(void) {
		}

		CartoSQL& m_sql;
		std::ostream *m_msg;

	};

	namespace skipper {
		using x3::rule;
		using x3::lit;
		using x3::eol;
		using x3::eoi;
		using ascii::char_;
		using ascii::space;

		const rule<class single_line_comment> single_line_comment = "single_line_comment";
		const rule<class block_comment> block_comment = "block_comment";
		const rule<class skip> skip = "skip";

		const auto single_line_comment_def = lit("--") >> *(char_ - eol) >> (eol|eoi);
		const auto block_comment_def = (lit("/*") >> *(block_comment | (char_ - lit("*/")))) > lit("*/");
		const auto skip_def = space | single_line_comment | block_comment;

		BOOST_SPIRIT_DEFINE(single_line_comment, block_comment, skip);
	};

	namespace parser {
		using x3::rule;
		using x3::get;
		using x3::no_case;
		using x3::no_skip;
		using x3::lit;
		using x3::string;
		using x3::omit;
		using x3::lexeme;
		using x3::repeat;
		using x3::eps;
		using x3::eol;
		using x3::eoi;
		using x3::attr;
		using x3::with;
		using x3::_val;
		using x3::_attr;
		using x3::_where;
		using x3::_pass;
		using x3::ulong_;
		using x3::long_;
		using x3::double_;
		using ascii::char_;
		using ascii::alnum;
		using ascii::alpha;
		using ascii::digit;
		using ascii::xdigit;
		using ascii::space;
		using fusion::at_c;

		//-----------------------------------------------------------------------------
		// Line
		//-----------------------------------------------------------------------------

		struct parser_tag {};

 		//-----------------------------------------------------------------------------
		// Operators
		//-----------------------------------------------------------------------------

		const struct compoptable_ : x3::symbols<CartoSQL::Expr::compop_t> {
			compoptable_() {
				add
					(">=",			  CartoSQL::Expr::compop_t::greaterthan)
					("<=",			  CartoSQL::Expr::compop_t::lessthan)
					("<>",			  CartoSQL::Expr::compop_t::unequal)
					("=",			   CartoSQL::Expr::compop_t::equal)
					(">",			   CartoSQL::Expr::compop_t::greater)
					("<",			   CartoSQL::Expr::compop_t::less);
			}
		} compoptable;

		//-----------------------------------------------------------------------------
		// Forward Rule Definitions
		//-----------------------------------------------------------------------------

		// 5.4 Names and identifiers
 		const rule<struct character_set_name> character_set_name = "character_set_name";

		// 6.2 <field definition>
		const rule<class field_definition> field_definition = "field_definition";

		// 6.4 <value specification> and <target specification>
		const rule<class unsigned_value_specification> unsigned_value_specification = "unsigned_value_specification";
		const rule<class simple_value_specification> simple_value_specification = "simple_value_specification";

		// 6.6 <identifier chain>
		const rule<class identifier_chain> identifier_chain = "identifier_chain";

		// 6.7 <column reference>
 		const rule<class column_reference> column_reference = "column_reference";

		// 6.9 <set function specification>
		const rule<class set_function_specification> set_function_specification = "set_function_specification";

		// 6.10 <window function>
		const rule<class window_function> window_function = "window_function";

		// 6.11 <case expression>
		const rule<class case_expression> case_expression = "case_expression";

		// 6.12 <cast specification>
		const rule<class cast_specification> cast_specification = "cast_specification";

		// 6.13 <next value expression>
		const rule<class next_value_expression> next_value_expression = "next_value_expression";

		// 6.14 <field reference>
		const rule<class field_reference> field_reference = "field_reference";

		// 6.15 <subtype treatment>
		const rule<class subtype_treatment> subtype_treatment = "subtype_treatment";

		// 6.16 <method invocation>
		const rule<class method_invocation> method_invocation = "method_invocation";

		// 6.17 <static method invocation>
		const rule<class static_method_invocation> static_method_invocation = "static_method_invocation";

		// 6.18 <new specification>
		const rule<class new_specification> new_specification = "new_specification";

		// 6.19 <attribute or method reference>
		const rule<class attribute_or_method_reference> attribute_or_method_reference = "attribute_or_method_reference";

		// 6.22 <reference resolution>
		const rule<class reference_resolution> reference_resolution = "reference_resolution";

		// 6.23 <array element reference>
		const rule<class array_element_reference> array_element_reference = "array_element_reference";

		// 6.24 <multiset element reference>
		const rule<class multiset_element_reference> multiset_element_reference = "multiset_element_reference";

		// 6.25 <value expression>
		const rule<class value_expression> value_expression = "value_expression";
		const rule<class collection_value_constructor> collection_value_constructor = "collection_value_constructor";

		// 6.26 <numeric value expression>
		const rule<class numeric_value_expression> numeric_value_expression = "numeric_value_expression";

		// 6.27 <numeric value function>
		const rule<class numeric_value_function> numeric_value_function = "numeric_value_function";

		// 6.28 <string value expression>
		const rule<class string_value_expression> string_value_expression = "string_value_expression";
		const rule<class character_value_expression> character_value_expression = "character_value_expression";
		const rule<class blob_value_expression> blob_value_expression = "blob_value_expression";

		// 6.29 <string value function>
		const rule<class string_value_function> string_value_function = "string_value_function";

		// 6.30 <datetime value expression>
		const rule<class datetime_value_expression> datetime_value_expression = "datetime_value_expression";

		// 6.31 <datetime value function>
		const rule<class datetime_value_function> datetime_value_function = "datetime_value_function";

		// 6.32 <interval value expression>
		const rule<class interval_value_expression> interval_value_expression = "interval_value_expression";
		const rule<class interval_term> interval_term = "interval_term";
		const rule<class interval_primary> interval_primary = "interval_primary";

		// 6.33 <interval value function>
		const rule<class interval_value_function> interval_value_function = "interval_value_function";

		// 6.34 <boolean value expression>
		const rule<class boolean_value_expression> boolean_value_expression = "boolean_value_expression";

		// 6.35 <array value expression>
		const rule<class array_value_expression> array_value_expression = "array_value_expression";

		// 6.36 <array value constructor>
		const rule<class array_value_constructor> array_value_constructor = "array_value_constructor";

		// 6.37 <multiset value expression>
		const rule<class multiset_value_expression> multiset_value_expression = "multiset_value_expression";

		// 6.38 <multiset value function>
		const rule<class multiset_value_function> multiset_value_function = "multiset_value_function";

		// 6.39 <multiset value constructor>
		const rule<class multiset_value_constructor> multiset_value_constructor = "multiset_value_constructor";

		// 7.2 <row value expression>
		const rule<class row_value_expression> row_value_expression = "row_value_expression";
		const rule<class row_value_predicand> row_value_predicand = "row_value_predicand";

		// 7.5 <from clause>
		const rule<class from_clause> from_clause = "from_clause";

		// 7.6 <table reference>
		const rule<struct table_reference> table_reference = "table_reference";

		// 7.7 <joined table>
		const rule<struct joined_table> joined_table = "joined_table";

		// 7.8 <where clause>
		const rule<class where_clause> where_clause = "where_clause";

		// 7.9 <group by clause>
		const rule<class group_by_clause> group_by_clause = "group_by_clause";

		// 7.10 having_clause
		const rule<class having_clause> having_clause = "having_clause";

		// 7.11 <window clause>
		const rule<class window_clause> window_clause = "window_clause";
		const rule<class window_specification> window_specification = "window_specification";

		// 7.13 <query expression>
		const rule<struct query_expression> query_expression = "query_expression";

		// 7.14 <search or cycle clause>
		const rule<struct search_or_cycle_clause> search_or_cycle_clause = "search_or_cycle_clause";

		// 7.15 <subquery>
		const rule<struct subquery> subquery = "subquery";

		// 8.1 <predicate>
		const rule<struct predicate> predicate = "predicate";

		// 8.2 <comparison predicate>
		const rule<struct comparison_predicate> comparison_predicate = "comparison_predicate";
		const rule<struct comparison_predicate_part_2> comparison_predicate_part_2 = "comparison_predicate_part_2";

		// 8.3 <between predicate>
		const rule<struct between_predicate> between_predicate = "between_predicate";
		const rule<struct between_predicate_part_2> between_predicate_part_2 = "between_predicate_part_2";

		// 8.4 <in predicate>
		const rule<struct in_predicate> in_predicate = "in_predicate";
		const rule<struct in_predicate_part_2> in_predicate_part_2 = "in_predicate_part_2";

		// 8.5 <like predicate>
		const rule<struct like_predicate> like_predicate = "like_predicate";
		const rule<struct character_like_predicate_part_2> character_like_predicate_part_2 = "character_like_predicate_part_2";
		const rule<struct octet_like_predicate_part_2> octet_like_predicate_part_2 = "octet_like_predicate_part_2";

		// 8.6 <similar predicate>
		const rule<struct similar_predicate> similar_predicate = "similar_predicate";
		const rule<struct similar_predicate_part_2> similar_predicate_part_2 = "similar_predicate_part_2";

		// 8.7 <null predicate>
		const rule<struct null_predicate> null_predicate = "null_predicate";
		const rule<struct null_predicate_part_2> null_predicate_part_2 = "null_predicate_part_2";

		// 8.8 <quantified comparison predicate>
		const rule<struct quantified_comparison_predicate> quantified_comparison_predicate = "quantified_comparison_predicate";
		const rule<struct quantified_comparison_predicate_part_2> quantified_comparison_predicate_part_2 = "quantified_comparison_predicate_part_2";

		// 8.9 <exists predicate>
		const rule<struct exists_predicate> exists_predicate = "exists_predicate";

		// 8.10 <unique predicate>
		const rule<struct unique_predicate> unique_predicate = "unique_predicate";

		// 8.11 <normalized predicate>
		const rule<struct normalized_predicate> normalized_predicate = "normalized_predicate";

		// 8.12 <match predicate>
		const rule<struct match_predicate> match_predicate = "match_predicate";
		const rule<struct match_predicate_part_2> match_predicate_part_2 = "match_predicate_part_2";

		// 8.13 <overlaps predicate>
		const rule<struct overlaps_predicate> overlaps_predicate = "overlaps_predicate";
		const rule<struct overlaps_predicate_part_2> overlaps_predicate_part_2 = "overlaps_predicate_part_2";

		// 8.14 <distinct predicate>
		const rule<struct distinct_predicate> distinct_predicate = "distinct_predicate";
		const rule<struct distinct_predicate_part_2> distinct_predicate_part_2 = "distinct_predicate_part_2";

		// 8.15 <member predicate>
		const rule<struct member_predicate> member_predicate = "member_predicate";
		const rule<struct member_predicate_part_2> member_predicate_part_2 = "member_predicate_part_2";

		// 8.16 <submultiset predicate>
		const rule<struct submultiset_predicate> submultiset_predicate = "submultiset_predicate";
		const rule<struct submultiset_predicate_part_2> submultiset_predicate_part_2 = "submultiset_predicate_part_2";

		// 8.17 <set predicate>
		const rule<struct set_predicate> set_predicate = "set_predicate";
		const rule<struct set_predicate_part_2> set_predicate_part_2 = "set_predicate_part_2";

		// 8.18 <type predicate>
		const rule<struct type_predicate> type_predicate = "type_predicate";
		const rule<struct type_predicate_part_2> type_predicate_part_2 = "type_predicate_part_2";

		// 10.1 <interval qualifier>
		const rule<struct interval_qualifier> interval_qualifier = "interval_qualifier";
		const rule<struct primary_datetime_field> primary_datetime_field = "primary_datetime_field";

		// 10.4 <routine invocation>
		const rule<struct routine_invocation> routine_invocation = "routine_invocation";
		const rule<struct SQL_argument_list> SQL_argument_list = "SQL_argument_list";

		// 10.7 <collate clause>
		const rule<struct collate_clause> collate_clause = "collate_clause";

		// 10.9 <aggregate function>
		const rule<struct aggregate_function> aggregate_function = "aggregate_function";
		const rule<struct set_quantifier> set_quantifier = "set_quantifier";

		// 10.10 <sort specification list>
		const rule<struct sort_specification_list> sort_specification_list = "sort_specification_list";

		// 11.4 <column definition>
		const rule<struct reference_scope_check> reference_scope_check = "reference_scope_check";

		// 11.8 <referential constraint definition>
		const rule<struct referential_action> referential_action = "referential_action";

		// 14.1 <declare cursor>
		const rule<class order_by_clause> order_by_clause = "order_by_clause";

		// 20.1 <embedded SQL host program>
		const rule<class embedded_variable_name> embedded_variable_name = "embedded_variable_name";

		// 20.4 <embedded SQL C program>
		const rule<class C_host_identifier> C_host_identifier = "C_host_identifier";

		//-----------------------------------------------------------------------------
		// keywords
		//-----------------------------------------------------------------------------

		inline auto keyword(const char *kwd) {
			return lexeme[no_case[lit(kwd)] >> !char_("0-9a-zA-Z_")];
		};

		//-----------------------------------------------------------------------------
		// SQL terminal character
		//-----------------------------------------------------------------------------

		const rule<struct left_bracket_or_trigraph> left_bracket_or_trigraph = "left_bracket_or_trigraph";
		const rule<struct right_bracket_or_trigraph> right_bracket_or_trigraph = "right_bracket_or_trigraph";
		const rule<struct alnum_underscore, char> alnum_underscore = "alnum_underscore";

//
// <SQL terminal character> ::= <SQL language character>;
//
// <SQL language character> ::= <simple Latin letter> | <digit> | <SQL special character>;
//
// <simple Latin letter> ::= <simple Latin upper case letter> | <simple Latin lower case letter>;
//
// <SQL special character> ::=
// 		<space>
// 	|	lit('"')
// 	|	lit('%')
// 	|	lit('&')
// 	|	lit('\'')
// 	|	lit('(')
// 	|	lit(')')
// 	|	lit('*')
// 	|	lit('+')
// 	|	lit(',')
// 	|	lit('-')
// 	|	lit('.')
// 	|	lit('/')
// 	|	lit(':')
// 	|	lit(';')
// 	|	lit('<')
// 	|	lit('=')
// 	|	lit('>')
// 	|	lit('?')
// 	|	lit('[')
// 	|	lit(']')
// 	|	lit('^')
// 	|	lit('_')
// 	|	lit('|')
// 	|	lit('{')
// 	|	lit('}');
//
//
//
//
//
//

		const auto left_bracket_or_trigraph_def = lit('[') | lexeme[lit("?""?(")];

		const auto right_bracket_or_trigraph_def = lit(']') | lexeme[lit("?""?)")];

		const auto alnum_underscore_def = alnum | char_('_');

		BOOST_SPIRIT_DEFINE(left_bracket_or_trigraph, right_bracket_or_trigraph, alnum_underscore);

		//-----------------------------------------------------------------------------
		// 5.2 <token> and <separator>
		//-----------------------------------------------------------------------------

		struct predicate_regular_identifier {
			template<typename Context>
			bool operator()(const Context& ctx) const {
				std::cerr << "predicate_regular_identifier: " << _val(ctx) << std::endl;
				return true;
			}
		};

		struct action_regular_identifier {
			template<typename Context>
			void operator()(Context& ctx) {
				std::string id(_attr(ctx));
				for (auto& ch : id)
					if (ch >= 'A' && ch <= 'Z')
						ch += 'a' - 'A';
				_val(ctx) = id;
				std::cerr << "regular_identifier: " << id << std::endl;
			}
		};

		static const char * const reserved_words[] = {
			"add", "all", "allocate", "alter", "and",
			"any", "are", "array", "as", "asensitive",
			"asymmetric", "at", "atomic", "authorization",
			"begin", "between", "bigint", "binary",
			"blob", "boolean", "both", "by",
			"call", "called", "cascaded", "case", "cast",
			"char", "character", "check", "clob", "close",
			"collate", "column", "commit", "connect",
			"constraint", "continue", "corresponding", "create",
			"cross", "cube", "current", "current_date",
			"current_default_transform_group", "current_path",
			"current_role", "current_time", "current_timestamp",
			"current_transform_group_for_type", "current_user", "cursor",
			"cycle",
			"date", "day", "deallocate", "dec", "decimal",
			"declare", "default", "delete", "deref", "describe",
			"deterministic", "disconnect", "distinct", "double",
			"drop", "dynamic",
			"each", "element", "else", "end", /*"end-exec",*/
			"escape", "except", "exec", "execute", "exists",
			"external",
			"false", "fetch", "filter", "float", "for",
			"foreign", "free", "from", "full", "function",
			"get", "global", "grant", "group", "grouping",
			"having", "hold", "hour",
			"identity", "immediate", "in", "indicator", "inner",
			"inout", "input", "insensitive", "insert", "int",
			"integer", "intersect", "interval", "into", "is",
			"isolation",
			"join",
			"language", "large", "lateral", "leading", "left",
			"like", "local", "localtime", "localtimestamp",
			"match", "member", "merge", "method", "minute",
			"modifies", "module", "month", "multiset",
			"national", "natural", "nchar", "nclob", "new",
			"no", "none", "not", "null", "numeric",
			"of", "old", "on", "only", "open", "or",
			"order", "out", "outer", "output", "over",
			"overlaps",
			"parameter", "partition", "precision", "prepare",
			"primary", "procedure",
			"range", "reads", "real", "recursive", "ref",
			"references", "referencing", "regr_avgx", "regr_avgy",
			"regr_count", "regr_intercept", "regr_r", "regr_slope",
			"regr_sxx", "regr_sxy", "regr_syy", "release",
			"result", "return", "returns", "revoke", "right",
			"rollback", "rollup", "row", "rows",
			"savepoint", "scroll", "search", "second", "select",
			"sensitive", "session_user", "set", "similar",
			"smallint", "some", "specific", "specifictype", "sql",
			"sqlexception", "sqlstate", "sqlwarning", "start",
			"static", "submultiset", "symmetric", "system",
			"system_user",
			"table", "then", "time", "timestamp", "timezone_hour",
			"timezone_minute", "to", "trailing", "translation",
			"treat", "trigger", "true",
			"uescape", "union", "unique", "unknown", "unnest",
			"update", "upper", "user", "using",
			"value", "values", "var_pop", "var_samp",
			"varchar", "varying",
			"when", "whenever", "where", "width_bucket", "window",
			"with", "within", "without",
			"year"
		};

		struct reserved_words_compare {
			bool operator()(const char *a, const char *b) const {
				if (!b)
					return false;
				if (!a)
					return true;
				return strcmp(a, b) < 0;
			}
		};

		struct regular_identifier_parser : boost::spirit::x3::parser<regular_identifier_parser> {
			typedef std::string attribute_type;
			static constexpr bool has_attribute = true;
			template <typename Iterator, typename Context, typename Attribute>
			bool parse(Iterator& first, Iterator const& last, Context const& context, x3::unused_type, Attribute& attr) const {
				x3::skip_over(first, last, context);
				std::string id;
				Iterator i = first;
				for (; i != last; ++i) {
					char ch(*i);
					if (ch >= 'A' && ch <= 'Z')
						ch += 'a' - 'A';
					if (!((ch >= 'a' && ch <= 'z') ||
					      (!id.empty() && ((ch >= '0' && ch <= '9') || ch == '_'))))
						break;
					id.push_back(ch);
				}
				if (id.empty())
					return false;
				const char * const *reserved_words_end = reserved_words + sizeof(reserved_words)/sizeof(reserved_words[0]);
				const char * const *rw(std::lower_bound(reserved_words, reserved_words_end, id.c_str(), reserved_words_compare()));
				if (rw != reserved_words_end && id == *rw)
					return false;
				if (false)
					x3::traits::move_to(id, attr);
				else
					x3::traits::move_to(first, i, attr);
				first = i;
				std::cerr << "regular_identifier: " << id << std::endl;
				return true;
			}
		};

		const rule<struct regular_identifier_internal, std::string> regular_identifier_internal = "regular_identifier_internal";
		const rule<struct regular_identifier, std::string> regular_identifier = "regular_identifier";
		const rule<struct large_object_length_token> large_object_length_token = "large_object_length_token";
		const rule<struct multiplier> multiplier = "multiplier";
		const rule<struct delimited_identifier_part> delimited_identifier_part = "delimited_identifier_part";
		const rule<struct delimited_identifier> delimited_identifier = "delimited_identifier";
		//const rule<struct >  = "";

// <token> ::= <nondelimiter token> | <delimiter token>;
//
// <nondelimiter token> ::=
// 		<regular identifier>
// 	|	<key word>
// 	|	unsigned_numeric_literal
// 	|	national_character_string_literal
// 	|	<bit string literal>
// 	|	<hex string literal>
// 	|	large_object_length_token
// 	|	<multiplier>;

		const auto regular_identifier_def = regular_identifier_parser{};

		const auto large_object_length_token_def = lexeme[+digit >> multiplier];

		const auto multiplier_def = no_case[lit('k') | lit('m') | lit('g')];

		const auto delimited_identifier_part_def = lexeme[+(lit('"') >> *(char_ - lit('"')) >> lit('"'))];
		const auto delimited_identifier_def = +delimited_identifier_part;


// <Unicode delimited identifier> ::=
// 		Ulit('&')lit('"') <Unicode delimiter body> lit('"')
// 		<Unicode escape specifier>;
//
// <Unicode escape specifier> ::= [ keyword("uescape") lit('\'') <Unicode escape character> lit('\'') ];
//
// <Unicode delimiter body> ::= <Unicode identifier part>...;
//
// <Unicode identifier part> ::= <delimited identifier part> | <Unicode escape value>;
//
// <Unicode escape value> ::=
// 		<Unicode 4 digit escape value>
// 	|	<Unicode 6 digit escape value>
// 	|	<Unicode character escape value>;
//
// //p
// //i
// //small
// Syntax rule 20: <Unicode 4 digit escape value>'<Unicode escape
// character>+xyzw' is equivalent to the Unicode code point specified by
// U+xyzw.
// ///small
// ///i
// ///p
//
// <Unicode 4 digit escape value> ::= <Unicode escape character><xdigit><xdigit><xdigit><xdigit>;
//
// //p
// //i
// //small
// Syntax rule 21: <Unicode 6 digit escape value>'<Unicode escape
// character>+xyzwrs' is equivalent to the Unicode code point specified by
// U+xyzwrs.
// ///small
// ///i
// ///p
//
// //p
// //i
// //small
// NOTE 64: The 6-xdigit notation is derived by taking the UCS-4 notation
// defined by ISO/IEC 10646-1 and removing the leading two xdigits, whose
// values are always 0 (zero).
// ///small
// ///i
// ///p
//
// <Unicode 6 digit escape value> ::=
// 		<Unicode escape character>lit('+')<xdigit><xdigit><xdigit><xdigit><xdigit><xdigit>;
//
// //p
// //i
// //small
// Syntax rule 22: <Unicode character escape value> is equivalent to a
// single instance of <Unicode escape character>.
// ///small
// ///i
// ///p
//
// <Unicode character escape value> ::= <Unicode escape character><Unicode escape character>;
//
// //p
// //i
// //small
// Syntax rule 15: <Unicode escape character> shall be a single character
// from the source language character set other than a <xdigit>, <plus
// sign>, or <white space>.
// ///small
// ///i
// ///p
//
// //p
// //i
// //small
// Syntax rule 16: If the source language character set contains <reverse
// solidus>, then let DEC be <reverse solidus>; otherwise, let DEC be an
// implementation-defined character from the source language character set
// that is not a <xdigit>, lit('+'), lit('"'), or <white space>.
// ///small
// ///i
// ///p
//
// //p
// //i
// //small
// Syntax rule 17: If a <Unicode escape specifier> does not contain
// <Unicode escape character>, then "UESCAPE lit('\'')DEClit('\'')" is
// implicit.
// ///small
// ///i
// ///p
//
// //p
// //i
// //small
// Syntax rule 18: In a <Unicode escape value> there shall be no
// <separator> between the <Unicode escape character> and the first
// <xdigit>, nor between any of the <xdigit>s.
// ///small
// ///i
// ///p
//
// <Unicode escape character> ::= !! See the Syntax Rules (15-18 above).;
//


// <delimiter token> ::=
// 		character_string_literal
// 	|	date_string
// 	|	time_string
// 	|	timestamp_string
// 	|	interval_string
// 	|	delimited_identifier
// 	|	<Unicode delimited identifier>
// 	|	<SQL special character>
// 	|	lexeme[lit("<>")]
// 	|	lexeme[lit(">=")]
// 	|	lexeme[lit("<=")]
// 	|	lexeme[lit("||")]
// 	|	lexeme[lit("->")]
// 	|	lexeme[lit("??(")]
// 	|	lexeme[lit("??)")]
// 	|	lexeme[lit("::")]
// 	|	lexeme[lit("..")];
//

// <key word> ::= reserved_word | <non-reserved word>;
//
// <non-reserved word> ::=
// 		keyword("a")
// 	|	keyword("abs")
// 	|	keyword("absolute")
// 	|	keyword("action")
// 	|	keyword("ada")
// 	|	keyword("admin")
// 	|	keyword("after")
// 	|	keyword("always")
// 	|	keyword("asc")
// 	|	keyword("assertion")
// 	|	keyword("assignment")
// 	|	keyword("attribute")
// 	|	keyword("attributes")
// 	|	keyword("avg")
// 	|	keyword("before")
// 	|	keyword("bernoulli")
// 	|	keyword("breadth")
// 	|	keyword("c")
// 	|	keyword("cardinality")
// 	|	keyword("cascade")
// 	|	keyword("catalog")
// 	|	keyword("catalog_name")
// 	|	keyword("ceil")
// 	|	keyword("ceiling")
// 	|	keyword("chain")
// 	|	keyword("characteristics")
// 	|	keyword("characters")
// 	|	keyword("character_length")
// 	|	keyword("character_set_catalog")
// 	|	keyword("character_set_name")
// 	|	keyword("character_set_schema")
// 	|	keyword("char_length")
// 	|	keyword("checked")
// 	|	keyword("class_origin")
// 	|	keyword("coalesce")
// 	|	keyword("cobol")
// 	|	keyword("code_units")
// 	|	keyword("collation")
// 	|	keyword("collation_catalog")
// 	|	keyword("collation_name")
// 	|	keyword("collation_schema")
// 	|	keyword("collect")
// 	|	keyword("column_name")
// 	|	keyword("command_function")
// 	|	keyword("command_function_code")
// 	|	keyword("committed")
// 	|	keyword("condition")
// 	|	keyword("condition_number")
// 	|	keyword("connection_name")
// 	|	keyword("constraints")
// 	|	keyword("constraint_catalog")
// 	|	keyword("constraint_name")
// 	|	keyword("constraint_schema")
// 	|	keyword("constructors")
// 	|	keyword("contains")
// 	|	keyword("convert")
// 	|	keyword("corr")
// 	|	keyword("count")
// 	|	keyword("covar_pop")
// 	|	keyword("covar_samp")
// 	|	keyword("cume_dist")
// 	|	keyword("current_collation")
// 	|	keyword("cursor_name")
// 	|	keyword("data")
// 	|	keyword("datetime_interval_code")
// 	|	keyword("datetime_interval_precision")
// 	|	keyword("defaults")
// 	|	keyword("deferrable")
// 	|	keyword("deferred")
// 	|	keyword("defined")
// 	|	keyword("definer")
// 	|	keyword("degree")
// 	|	keyword("dense_rank")
// 	|	keyword("depth")
// 	|	keyword("derived")
// 	|	keyword("desc")
// 	|	keyword("descriptor")
// 	|	keyword("diagnostics")
// 	|	keyword("dispatch")
// 	|	keyword("domain")
// 	|	keyword("dynamic_function")
// 	|	keyword("dynamic_function_code")
// 	|	keyword("equals")
// 	|	keyword("every")
// 	|	keyword("exception")
// 	|	keyword("exclude")
// 	|	keyword("excluding")
// 	|	keyword("exp")
// 	|	keyword("extract")
// 	|	keyword("final")
// 	|	keyword("first")
// 	|	keyword("floor")
// 	|	keyword("following")
// 	|	keyword("fortran")
// 	|	keyword("found")
// 	|	keyword("fusion")
// 	|	keyword("g")
// 	|	keyword("general")
// 	|	keyword("go")
// 	|	keyword("goto")
// 	|	keyword("granted")
// 	|	keyword("hierarchy")
// 	|	keyword("implementation")
// 	|	keyword("including")
// 	|	keyword("increment")
// 	|	keyword("initially")
// 	|	keyword("instance")
// 	|	keyword("instantiable")
// 	|	keyword("intersection")
// 	|	keyword("invoker")
// 	|	keyword("isolation")
// 	|	keyword("k")
// 	|	keyword("key")
// 	|	keyword("key_member")
// 	|	keyword("key_type")
// 	|	keyword("last")
// 	|	keyword("length")
// 	|	keyword("level")
// 	|	keyword("ln")
// 	|	keyword("locator")
// 	|	keyword("lower")
// 	|	keyword("m")
// 	|	keyword("map")
// 	|	keyword("matched")
// 	|	keyword("max")
// 	|	keyword("maxvalue")
// 	|	keyword("message_length")
// 	|	keyword("message_octet_length")
// 	|	keyword("message_text")
// 	|	keyword("min")
// 	|	keyword("minvalue")
// 	|	keyword("mod")
// 	|	keyword("more")
// 	|	keyword("mumps")
// 	|	keyword("name")
// 	|	keyword("names")
// 	|	keyword("nesting")
// 	|	keyword("next")
// 	|	keyword("normalize")
// 	|	keyword("normalized")
// 	|	keyword("nullable")
// 	|	keyword("nullif")
// 	|	keyword("nulls")
// 	|	keyword("number")
// 	|	keyword("object")
// 	|	keyword("octets")
// 	|	keyword("octet_length")
// 	|	keyword("option")
// 	|	keyword("options")
// 	|	keyword("ordering")
// 	|	keyword("ordinality")
// 	|	keyword("others")
// 	|	keyword("overlay")
// 	|	keyword("overriding")
// 	|	keyword("pad")
// 	|	keyword("parameter_mode")
// 	|	keyword("parameter_name")
// 	|	keyword("parameter_ordinal_position")
// 	|	keyword("parameter_specific_catalog")
// 	|	keyword("parameter_specific_name")
// 	|	keyword("parameter_specific_schema")
// 	|	keyword("partial")
// 	|	keyword("pascal")
// 	|	keyword("path")
// 	|	keyword("percentile_cont")
// 	|	keyword("percentile_disc")
// 	|	keyword("percent_rank")
// 	|	keyword("placing")
// 	|	keyword("pli")
// 	|	keyword("position")
// 	|	keyword("power")
// 	|	keyword("preceding")
// 	|	keyword("preserve")
// 	|	keyword("prior")
// 	|	keyword("privileges")
// 	|	keyword("public")
// 	|	keyword("rank")
// 	|	keyword("read")
// 	|	keyword("relative")
// 	|	keyword("repeatable")
// 	|	keyword("restart")
// 	|	keyword("returned_cardinality")
// 	|	keyword("returned_length")
// 	|	keyword("returned_octet_length")
// 	|	keyword("returned_sqlstate")
// 	|	keyword("role")
// 	|	keyword("routine")
// 	|	keyword("routine_catalog")
// 	|	keyword("routine_name")
// 	|	keyword("routine_schema")
// 	|	keyword("row_count")
// 	|	keyword("row_number")
// 	|	keyword("scale")
// 	|	keyword("schema")
// 	|	keyword("schema_name")
// 	|	keyword("scope_catalog")
// 	|	keyword("scope_name")
// 	|	keyword("scope_schema")
// 	|	keyword("section")
// 	|	keyword("security")
// 	|	keyword("self")
// 	|	keyword("sequence")
// 	|	keyword("serializable")
// 	|	keyword("server_name")
// 	|	keyword("session")
// 	|	keyword("sets")
// 	|	keyword("simple")
// 	|	keyword("size")
// 	|	keyword("source")
// 	|	keyword("space")
// 	|	keyword("specific_name")
// 	|	keyword("sqrt")
// 	|	keyword("state")
// 	|	keyword("statement")
// 	|	keyword("stddev_pop")
// 	|	keyword("stddev_samp")
// 	|	keyword("structure")
// 	|	keyword("style")
// 	|	keyword("subclass_origin")
// 	|	keyword("substring")
// 	|	keyword("sum")
// 	|	keyword("tablesample")
// 	|	keyword("table_name")
// 	|	keyword("temporary")
// 	|	keyword("ties")
// 	|	keyword("top_level_count")
// 	|	keyword("transaction")
// 	|	keyword("transactions_committed")
// 	|	keyword("transactions_rolled_back")
// 	|	keyword("transaction_active")
// 	|	keyword("transform")
// 	|	keyword("transforms")
// 	|	keyword("translate")
// 	|	keyword("trigger_catalog")
// 	|	keyword("trigger_name")
// 	|	keyword("trigger_schema")
// 	|	keyword("trim")
// 	|	keyword("type")
// 	|	keyword("unbounded")
// 	|	keyword("uncommitted")
// 	|	keyword("under")
// 	|	keyword("unnamed")
// 	|	keyword("usage")
// 	|	keyword("user_defined_type_catalog")
// 	|	keyword("user_defined_type_code")
// 	|	keyword("user_defined_type_name")
// 	|	keyword("user_defined_type_schema")
// 	|	keyword("view")
// 	|	keyword("work")
// 	|	keyword("write")
// 	|	keyword("zone");

		BOOST_SPIRIT_DEFINE(regular_identifier, large_object_length_token, multiplier,
				    delimited_identifier_part, delimited_identifier);

		//-----------------------------------------------------------------------------
		// 5.3 <literal>
		//-----------------------------------------------------------------------------

		const rule<struct literal> literal = "literal";
		const rule<struct unsigned_literal> unsigned_literal = "unsigned_literal";
		const rule<struct general_literal> general_literal = "general_literal";
		const rule<struct character_string_literal> character_string_literal = "character_string_literal";
		const rule<struct national_character_string_literal> national_character_string_literal = "national_character_string_literal";
		const rule<struct binary_string_literal_part> binary_string_literal_part = "binary_string_literal_part";
		const rule<struct binary_string_literal> binary_string_literal = "binary_string_literal";
		const rule<struct signed_numeric_literal> signed_numeric_literal = "signed_numeric_literal";
		const rule<struct unsigned_numeric_literal> unsigned_numeric_literal = "unsigned_numeric_literal";
		const rule<struct exact_numeric_literal> exact_numeric_literal = "exact_numeric_literal";
		const rule<struct sign> sign = "sign";
		const rule<struct approximate_numeric_literal> approximate_numeric_literal = "approximate_numeric_literal";
		const rule<struct signed_integer> signed_integer = "signed_integer";
		const rule<struct unsigned_integer> unsigned_integer = "unsigned_integer";
		const rule<struct datetime_literal> datetime_literal = "datetime_literal";
		const rule<struct date_literal> date_literal = "date_literal";
		const rule<struct time_literal> time_literal = "time_literal";
		const rule<struct timestamp_literal> timestamp_literal = "timestamp_literal";
		const rule<struct date_string> date_string = "date_string";
		const rule<struct time_string> time_string = "time_string";
		const rule<struct timestamp_string> timestamp_string = "timestamp_string";
		const rule<struct time_zone_interval> time_zone_interval = "time_zone_interval";
		const rule<struct date_value> date_value = "date_value";
		const rule<struct time_value> time_value = "time_value";
		const rule<struct interval_literal> interval_literal = "interval_literal";
		const rule<struct interval_string> interval_string = "interval_string";
		const rule<struct unquoted_time_string> unquoted_time_string = "unquoted_time_string";
		const rule<struct unquoted_timestamp_string> unquoted_timestamp_string = "unquoted_timestamp_string";
		const rule<struct unquoted_interval_string> unquoted_interval_string = "unquoted_interval_string";
		const rule<struct year_month_literal> year_month_literal = "year_month_literal";
		const rule<struct day_time_literal> day_time_literal = "day_time_literal";
		const rule<struct day_time_interval> day_time_interval = "day_time_interval";
		const rule<struct time_interval> time_interval = "time_interval";
		const rule<struct seconds_value> seconds_value = "seconds_value";
		const rule<struct boolean_literal> boolean_literal = "boolean_literal";

		const auto literal_def = signed_numeric_literal | general_literal;

		const auto unsigned_literal_def = unsigned_numeric_literal | general_literal;

		const auto general_literal_def = (character_string_literal
						  | national_character_string_literal
						  /*| <Unicode character string literal>*/
						  | binary_string_literal
						  | datetime_literal
						  | interval_literal
						  | boolean_literal);

		const auto character_string_literal_def = -(lit('_') >> omit[character_set_name]) >>
		      +lexeme[+(lit('\'') >> *(char_ - lit('\'')) >> lit('\''))];

		const auto national_character_string_literal_def = lexeme[no_case[lit('n')] >> +(lit('\'') >> +(char_ - lit('\'')) >> lit('\''))];

// <Unicode character string literal> ::=
// 		[ lit('_')character_set_name ]
// 		Ulit('&')lit('\'') [ <Unicode representation>... ] lit('\'')
// 		[ { <separator> lit('\'') [ <Unicode representation>... ] lit('\'') }... ]
// 		[ keyword("escape") character_value_expression ];
//
// <Unicode representation> ::= <character representation> | <Unicode escape value>;

		const auto binary_string_literal_part_def = lit('\'') >> *(repeat(2)[xdigit]) >> lit('\'');

		const auto binary_string_literal_def = lexeme[no_case[lit('x')] >> binary_string_literal_part] >>
			*lexeme[binary_string_literal_part] >>
		       -(keyword("escape") >> character_value_expression);

		const auto signed_numeric_literal_def = -(sign) >> unsigned_numeric_literal;

		const auto unsigned_numeric_literal_def = exact_numeric_literal | approximate_numeric_literal;

		const auto exact_numeric_literal_def =
			lexeme[unsigned_integer >> -(lit('.') >> -(unsigned_integer))]
			| lexeme[lit('.') >> unsigned_integer];

		const auto sign_def = lit('+') | lit('-');

		const auto approximate_numeric_literal_def = lexeme[exact_numeric_literal >> no_case[lit('e')] >> signed_integer];

		const auto signed_integer_def = no_skip[-(sign) >> unsigned_integer];

		const auto unsigned_integer_def = no_skip[+digit];

		const auto datetime_literal_def = date_literal | time_literal | timestamp_literal;

		const auto date_literal_def = keyword("date") >> date_string;

		const auto time_literal_def = keyword("time") >> time_string;

		const auto timestamp_literal_def = keyword("timestamp") >> timestamp_string;

		const auto date_string_def = lexeme[lit('\'') >> date_value > lit('\'')];

		const auto time_string_def = lexeme[lit('\'') >> unquoted_time_string > lit('\'')];

		const auto timestamp_string_def = lexeme[lit('\'') >> unquoted_timestamp_string > lit('\'')];

		const auto time_zone_interval_def = no_skip[sign >> unsigned_integer >> lit(':') >> unsigned_integer];

		const auto date_value_def = no_skip[unsigned_integer >> lit('-') >> unsigned_integer >> lit('-') >> unsigned_integer];

		const auto time_value_def = no_skip[unsigned_integer >> lit(':') >> unsigned_integer >> lit(':') >> seconds_value];

		const auto interval_literal_def = keyword("interval") >> -(sign) >> interval_string >> interval_qualifier;

		const auto interval_string_def = lexeme[lit('\'') >> unquoted_interval_string > lit('\'')];

		const auto unquoted_time_string_def = time_value >> -(time_zone_interval);

		const auto unquoted_timestamp_string_def = date_value >> unquoted_time_string;

		const auto unquoted_interval_string_def = -(sign) >> (year_month_literal | day_time_literal);

		const auto year_month_literal_def = unsigned_integer >> -(lit('-') >> unsigned_integer);

		const auto day_time_literal_def = day_time_interval | time_interval;

		const auto day_time_interval_def =
			lexeme[unsigned_integer] >> -lexeme[unsigned_integer >> -(lit(':') >> unsigned_integer >> -(lit(':') >> seconds_value))];

		const auto time_interval_def =
			lexeme[unsigned_integer >> lit(':') >> unsigned_integer >> lit(':') >> seconds_value]
			| lexeme[unsigned_integer >> lit(':') >> seconds_value]
			| lexeme[seconds_value];

		const auto seconds_value_def = no_skip[+digit >> -(lit('.') >> -(+digit))];

		const auto boolean_literal_def = keyword("true") | keyword("false") | keyword("unknown");

		BOOST_SPIRIT_DEFINE(literal, unsigned_literal, general_literal, character_string_literal, national_character_string_literal,
				    binary_string_literal_part, binary_string_literal, signed_numeric_literal, unsigned_numeric_literal,
				    exact_numeric_literal, sign, approximate_numeric_literal, signed_integer, unsigned_integer,
				    datetime_literal, date_literal, time_literal, timestamp_literal, date_string, time_string,
				    timestamp_string, time_zone_interval, date_value, time_value, interval_literal, interval_string,
				    unquoted_time_string, unquoted_timestamp_string, unquoted_interval_string, year_month_literal,
				    day_time_literal, day_time_interval, time_interval, seconds_value, boolean_literal);

		//-----------------------------------------------------------------------------
		// 5.4 Names and identifiers
		//-----------------------------------------------------------------------------

		const rule<struct identifier> identifier = "identifier";
		const rule<struct SQL_language_identifier, std::string> SQL_language_identifier = "SQL_language_identifier";
		const rule<struct schema_name> schema_name = "schema_name";
		const rule<struct schema_qualified_name> schema_qualified_name = "schema_qualified_name";
		const rule<struct local_or_schema_qualified_name> local_or_schema_qualified_name = "local_or_schema_qualified_name";
		const rule<struct local_or_schema_qualifier> local_or_schema_qualifier = "local_or_schema_qualifier";
		const rule<struct local_qualified_name> local_qualified_name = "local_qualified_name";
		const rule<struct host_parameter_name> host_parameter_name = "host_parameter_name";
		const rule<struct external_routine_name> external_routine_name = "external_routine_name";
		const rule<struct schema_qualified_type_name> schema_qualified_type_name = "schema_qualified_type_name";
		const rule<struct SQL_statement_name> SQL_statement_name = "SQL_statement_name";
		const rule<struct extended_statement_name> extended_statement_name = "extended_statement_name";
		const rule<struct dynamic_cursor_name> dynamic_cursor_name = "dynamic_cursor_name";
		const rule<struct scope_option> scope_option = "scope_option";

		const auto identifier_def = regular_identifier | delimited_identifier;

		const auto SQL_language_identifier_def = alpha | *(char_('_') | alnum);

		const auto schema_name_def = -(identifier >> lit('.')) >> identifier;

		const auto schema_qualified_name_def = -(schema_name >> lit('.')) >> identifier;

		const auto local_or_schema_qualified_name_def = -(local_or_schema_qualifier >> lit('.')) >> identifier;

		const auto local_or_schema_qualifier_def = schema_name | keyword("module");

		const auto local_qualified_name_def = -(keyword("module") >> lit('.')) >> identifier;

		const auto host_parameter_name_def = lit(':') >> identifier;

		const auto external_routine_name_def = identifier | character_string_literal;

		const auto character_set_name_def = -(schema_name >> lit('.')) >> SQL_language_identifier;

		const auto schema_qualified_type_name_def = -(schema_name >> lit('.')) >> identifier;

		const auto SQL_statement_name_def = identifier | extended_statement_name;

		const auto extended_statement_name_def = -(scope_option) >> simple_value_specification;

		const auto dynamic_cursor_name_def = local_qualified_name | extended_statement_name;

		const auto scope_option_def = keyword("global") | keyword("local");


		BOOST_SPIRIT_DEFINE(identifier, SQL_language_identifier, schema_name, schema_qualified_name, local_or_schema_qualified_name,
				    local_or_schema_qualifier, local_qualified_name, host_parameter_name, external_routine_name, character_set_name,
				    schema_qualified_type_name, SQL_statement_name, extended_statement_name, dynamic_cursor_name, scope_option);

		//-----------------------------------------------------------------------------
		// 6 Scalar expressions
		//-----------------------------------------------------------------------------

		//-----------------------------------------------------------------------------
		// 6.1 <data type>
		//-----------------------------------------------------------------------------

		const rule<class data_type> data_type = "data_type";
		const rule<class predefined_type> predefined_type = "predefined_type";
		const rule<class character_string_type> character_string_type = "character_string_type";
		const rule<class national_character_string_type> national_character_string_type = "national_character_string_type";
		const rule<class binary_large_object_string_type> binary_large_object_string_type = "binary_large_object_string_type";
		const rule<class numeric_type> numeric_type = "numeric_type";
		const rule<class exact_numeric_type> exact_numeric_type = "exact_numeric_type";
		const rule<class approximate_numeric_type> approximate_numeric_type = "approximate_numeric_type";
		const rule<class large_object_length> large_object_length = "large_object_length";
		const rule<class char_length_units> char_length_units = "char_length_units";
		const rule<class boolean_type> boolean_type = "boolean_type";
		const rule<class datetime_type> datetime_type = "datetime_type";
		const rule<class with_or_without_time_zone> with_or_without_time_zone = "with_or_without_time_zone";
		const rule<class interval_type> interval_type = "interval_type";
		const rule<class row_type> row_type = "row_type";
		const rule<class row_type_body> row_type_body = "row_type_body";
		const rule<class reference_type> reference_type = "reference_type";
		const rule<class scope_clause> scope_clause = "scope_clause";
		const rule<class collection_type> collection_type = "collection_type";
		const rule<class array_type> array_type = "array_type";
		const rule<class multiset_type> multiset_type = "multiset_type";

		const auto data_type_def = predefined_type
			| row_type
			| schema_qualified_type_name
			| reference_type
			| collection_type;

		const auto predefined_type_def = ((character_string_type >> -(keyword("character") > keyword("set") >> character_set_name) >> -(collate_clause))
						  | (national_character_string_type >> -(collate_clause))
						  | binary_large_object_string_type
						  | numeric_type
						  | boolean_type
						  | datetime_type
						  | interval_type);

		const auto character_string_type_def = ((keyword("character") >> -(lit('(') >> unsigned_integer > lit(')')))
							| (keyword("char") >> -(lit('(') >> unsigned_integer > lit(')')))
							| (keyword("character") >> keyword("varying") >> lit('(') >> unsigned_integer > lit(')'))
							| (keyword("char") >> keyword("varying") >> lit('(') >> unsigned_integer > lit(')'))
							| (keyword("varchar") >> lit('(') >> unsigned_integer > lit(')'))
							| (keyword("character") >> keyword("large") >> keyword("object") >> -(lit('(') >> large_object_length > lit(')')))
							| (keyword("char") >> keyword("large") >> keyword("object") >> -(lit('(') >> large_object_length > lit(')')))
							| (keyword("clob") >> -(lit('(') >> large_object_length > lit(')'))));

		const auto national_character_string_type_def = ((keyword("national") >> keyword("character") >> -(lit('(') >> unsigned_integer > lit(')')))
								 | (keyword("national") >> keyword("char") >> -(lit('(') >> unsigned_integer > lit(')')))
								 | (keyword("nchar") >> -(lit('(') >> unsigned_integer > lit(')')))
								 | (keyword("national") >> keyword("character") >> keyword("varying") >> lit('(') >> unsigned_integer > lit(')'))
								 | (keyword("national") >> keyword("char") >> keyword("varying") >> lit('(') >> unsigned_integer > lit(')'))
								 | (keyword("nchar") >> keyword("varying") >> lit('(') >> unsigned_integer > lit(')'))
								 | (keyword("national") >> keyword("character") >> keyword("large") >> keyword("object") >> -(lit('(') >> large_object_length > lit(')')))
								 | (keyword("nchar") >> keyword("large") >> keyword("object") >> -(lit('(') >> large_object_length > lit(')')))
								 | (keyword("nclob") >> -(lit('(') >> large_object_length > lit(')'))));


		const auto binary_large_object_string_type_def = ((keyword("binary") >> keyword("large") >> keyword("object")) |
								  keyword("blob")) >> -(lit('(') >> large_object_length > lit(')'));


		const auto numeric_type_def = exact_numeric_type | approximate_numeric_type;

		const auto exact_numeric_type_def = ((keyword("numeric") >> -(lit('(') >> unsigned_integer >> -(lit(',') >> unsigned_integer) >> lit(')')))
						     | (keyword("decimal") >> -(lit('(') >> unsigned_integer >> -(lit(',') >> unsigned_integer) >> lit(')')))
						     | (keyword("dec") >> -(lit('(') >> unsigned_integer >> -(lit(',') >> unsigned_integer) >> lit(')')))
						     | keyword("smallint")
						     | keyword("integer")
						     | keyword("int")
						     | keyword("bigint"));

		const auto approximate_numeric_type_def = ((keyword("float") >> -(lit('(') >> unsigned_integer > lit(')')))
							   | keyword("real")
							   | (keyword("double") >> keyword("precision")));

		const auto large_object_length_def =
			((unsigned_integer >> -(multiplier)) | large_object_length_token) >> -(char_length_units);

		const auto char_length_units_def = keyword("characters") | keyword("code_units") | keyword("octets");

		const auto boolean_type_def = keyword("boolean");

		const auto datetime_type_def =
			keyword("date")
			| (keyword("time") >> -(lit('(') >> unsigned_integer >> lit(')')) >> -(with_or_without_time_zone))
			| (keyword("timestamp") >> -(lit('(') >> unsigned_integer >> lit(')')) >> -(with_or_without_time_zone));

		const auto with_or_without_time_zone_def =
			(keyword("with") | keyword("without")) >> keyword("time") >>keyword("zone");

		const auto interval_type_def = keyword("interval") >> interval_qualifier;

		const auto row_type_def = keyword("row") >> row_type_body;

		const auto row_type_body_def = lit('(') >> (field_definition % lit(',')) >> lit(')');

		const auto reference_type_def = keyword("ref") >> lit('(') >> schema_qualified_type_name >> lit(')') >> -(scope_clause);

		const auto scope_clause_def = keyword("scope") >> local_or_schema_qualified_name;

		const auto collection_type_def = array_type | multiset_type;

		const auto array_type_def = data_type >> keyword("array") >> -(left_bracket_or_trigraph >> unsigned_integer >> right_bracket_or_trigraph);

		const auto multiset_type_def = data_type >> keyword("multiset");

		BOOST_SPIRIT_DEFINE(data_type, predefined_type, character_string_type, national_character_string_type,
				    binary_large_object_string_type, numeric_type, exact_numeric_type, approximate_numeric_type,
				    large_object_length, char_length_units, boolean_type, datetime_type, with_or_without_time_zone,
				    interval_type, row_type, row_type_body, reference_type, scope_clause, collection_type, array_type,
				    multiset_type);

		//-----------------------------------------------------------------------------
		// 6.2 <field definition>
		//-----------------------------------------------------------------------------

		const auto field_definition_def = identifier >> data_type >> -(reference_scope_check);

		BOOST_SPIRIT_DEFINE(field_definition);

		//-----------------------------------------------------------------------------
		// 6.3 <value expression primary
		//-----------------------------------------------------------------------------

		const rule<class value_expression_primary> value_expression_primary = "value_expression_primary";
		const rule<class parenthesized_value_expression> parenthesized_value_expression = "parenthesized_value_expression";
		const rule<class nonparenthesized_value_expression_primary> nonparenthesized_value_expression_primary = "nonparenthesized_value_expression_primary";

		const auto value_expression_primary_def = parenthesized_value_expression | nonparenthesized_value_expression_primary;

		const auto parenthesized_value_expression_def = lit('(') >> value_expression >> lit(')');

		const auto nonparenthesized_value_expression_primary_def =
			unsigned_value_specification
			| column_reference
			| set_function_specification
			| window_function
			| subquery
			| case_expression
			| cast_specification
			| (eps(false) >> field_reference) /* generates infinite recursion: <field_reference> <value_expression_primary> <nonparenthesized_value_expression_primary> <field_reference> */
			| subtype_treatment
			| method_invocation
			| static_method_invocation
			| new_specification
			| attribute_or_method_reference
			| reference_resolution
			| collection_value_constructor
			| array_element_reference
			| multiset_element_reference
			| routine_invocation
			| next_value_expression;

		BOOST_SPIRIT_DEFINE(value_expression_primary, parenthesized_value_expression, nonparenthesized_value_expression_primary);

		//-----------------------------------------------------------------------------
		// 6.4 <value specification> and <target specification>
		//-----------------------------------------------------------------------------

		const rule<class value_specification> value_specification = "value_specification";
		const rule<class general_value_specification> general_value_specification = "general_value_specification";
		const rule<class target_specification> target_specification = "target_specification";
		const rule<class simple_target_specification> simple_target_specification = "simple_target_specification";
		const rule<class host_parameter_specification> host_parameter_specification = "host_parameter_specification";
		const rule<class dynamic_parameter_specification> dynamic_parameter_specification = "dynamic_parameter_specification";
		const rule<class embedded_variable_specification> embedded_variable_specification = "embedded_variable_specification";
		const rule<class indicator_variable> indicator_variable = "indicator_variable";
		const rule<class indicator_parameter> indicator_parameter = "indicator_parameter";
		const rule<class target_array_element_specification> target_array_element_specification = "target_array_element_specification";
		const rule<class target_array_reference> target_array_reference = "target_array_reference";
		const rule<class current_collation_specification> current_collation_specification = "current_collation_specification";
		const rule<class mapnik_variable> mapnik_variable = "mapnik_variable";
		const rule<class mapnik_variable_body> mapnik_variable_body = "mapnik_variable_body";

		const auto value_specification_def = literal | general_value_specification;

		const auto unsigned_value_specification_def = unsigned_literal | general_value_specification;

		const auto general_value_specification_def =
			host_parameter_specification
			| (eps(false) >> identifier_chain)
			| dynamic_parameter_specification
			| embedded_variable_specification
			| current_collation_specification
			| keyword("current_default_transform_group")
			| keyword("current_path")
			| keyword("current_role")
			| (keyword("current_transform_group_for_type") >> schema_qualified_type_name)
			| keyword("current_user")
			| keyword("session_user")
			| keyword("system_user")
			| keyword("user")
			| keyword("value")
			| mapnik_variable;

		const auto simple_value_specification_def =
			literal
			| host_parameter_name
			| identifier_chain
			| embedded_variable_name;

		const auto target_specification_def =
			host_parameter_specification
			| identifier_chain
			| column_reference
			| target_array_element_specification
			| dynamic_parameter_specification
			| embedded_variable_specification;

		const auto simple_target_specification_def =
			host_parameter_specification
			| identifier_chain
			| column_reference
			| embedded_variable_name;

		const auto host_parameter_specification_def = host_parameter_name >> -(indicator_parameter);

		const auto dynamic_parameter_specification_def = lit('?');

		const auto embedded_variable_specification_def = embedded_variable_name >> -(indicator_variable);

		const auto indicator_variable_def = -(keyword("indicator")) >> embedded_variable_name;

		const auto indicator_parameter_def = -(keyword("indicator")) >> host_parameter_name;

		const auto target_array_element_specification_def =
			target_array_reference >> left_bracket_or_trigraph >> simple_value_specification >> right_bracket_or_trigraph;

		const auto target_array_reference_def = identifier_chain | column_reference;

		const auto current_collation_specification_def = keyword("current_collation") >> lit('(') >> string_value_expression >> lit(')');

		const auto mapnik_variable_def = lexeme[lit('!') >> mapnik_variable_body >> lit('!')];

		const auto mapnik_variable_body_def =
			no_skip[no_case[lit("scale_denominator")]]
			| no_skip[no_case[lit("pixel_width")]]
			| no_skip[no_case[lit("pixel_height")]];

		BOOST_SPIRIT_DEFINE(value_specification, unsigned_value_specification, general_value_specification, simple_value_specification,
				    target_specification, simple_target_specification, host_parameter_specification, dynamic_parameter_specification,
				    embedded_variable_specification, indicator_variable, indicator_parameter, target_array_element_specification,
				    target_array_reference, current_collation_specification,
				    mapnik_variable, mapnik_variable_body);

		//-----------------------------------------------------------------------------
		// 6.5 <contextually typed value specification>
		//-----------------------------------------------------------------------------

		const rule<class contextually_typed_value_specification> contextually_typed_value_specification = "contextually_typed_value_specification";
		const rule<class implicitly_typed_value_specification> implicitly_typed_value_specification = "implicitly_typed_value_specification";
		const rule<class empty_specification> empty_specification = "empty_specification";

		const auto contextually_typed_value_specification_def =
			implicitly_typed_value_specification | keyword("default");

		const auto implicitly_typed_value_specification_def = keyword("null") | empty_specification;

		const auto empty_specification_def =
			(keyword("array") >> left_bracket_or_trigraph >> right_bracket_or_trigraph)
			| (keyword("multiset") >> left_bracket_or_trigraph >> right_bracket_or_trigraph);

		BOOST_SPIRIT_DEFINE(contextually_typed_value_specification, implicitly_typed_value_specification, empty_specification);

		//-----------------------------------------------------------------------------
		// 6.6 <identifier chain>
		//-----------------------------------------------------------------------------

		const auto identifier_chain_def = identifier % lit('.');

		BOOST_SPIRIT_DEFINE(identifier_chain);

		//-----------------------------------------------------------------------------
		// 6.7 <column reference>
		//-----------------------------------------------------------------------------

		const auto column_reference_def =
			identifier_chain
			| (keyword("module") >> lit('.') >> identifier >> lit('.') >> identifier);

		BOOST_SPIRIT_DEFINE(column_reference);

		//-----------------------------------------------------------------------------
		// 6.9 <set function specification>
		//-----------------------------------------------------------------------------

		const rule<class grouping_operation> grouping_operation = "grouping_operation";

		const auto set_function_specification_def = aggregate_function | grouping_operation;

		const auto grouping_operation_def = keyword("grouping") >> lit('(') >> (column_reference % lit(',')) >> lit(')');

		BOOST_SPIRIT_DEFINE(set_function_specification, grouping_operation);

		//-----------------------------------------------------------------------------
		// 6.10 <window function>
		//-----------------------------------------------------------------------------

		const rule<class window_function_type> window_function_type = "window_function_type";
		const rule<class rank_function_type> rank_function_type = "rank_function_type";
		const rule<class window_name_or_specification> window_name_or_specification = "window_name_or_specification";

		const auto window_function_def = window_function_type >> keyword("over") >> window_name_or_specification;

		const auto window_function_type_def =
			(rank_function_type >> lit('(') >> lit(')'))
			| (keyword("row_number") >> lit('(') >> lit(')'))
			| aggregate_function;

		const auto rank_function_type_def = keyword("rank") | keyword("dense_rank")
			| keyword("percent_rank") | keyword("cume_dist");

		const auto window_name_or_specification_def = identifier | window_specification;

		BOOST_SPIRIT_DEFINE(window_function, window_function_type, rank_function_type, window_name_or_specification);

		//-----------------------------------------------------------------------------
		// 6.11 <case expression>
		//-----------------------------------------------------------------------------

		const rule<class case_abbreviation> case_abbreviation = "case_abbreviation";
		const rule<class case_specification> case_specification = "case_specification";
		const rule<class simple_case> simple_case = "simple_case";
		const rule<class searched_case> searched_case = "searched_case";
		const rule<class simple_when_clause> simple_when_clause = "simple_when_clause";
		const rule<class searched_when_clause> searched_when_clause = "searched_when_clause";
		const rule<class else_clause> else_clause = "else_clause";
		const rule<class case_operand> case_operand = "case_operand";
		const rule<class when_operand> when_operand = "when_operand";
		const rule<class result> result = "result";

		const auto case_expression_def = case_abbreviation | case_specification;

		const auto case_abbreviation_def =
			(keyword("nullif") >> lit('(') >> value_expression >> lit(',') >> value_expression >> lit(')'))
			| (keyword("coalesce") >> lit('(') >> (value_expression % lit(',')) >> lit(')'));

		const auto case_specification_def = simple_case | searched_case;

		const auto simple_case_def = keyword("case") >> case_operand >> *simple_when_clause >> -(else_clause) >> keyword("end");

		const auto searched_case_def = keyword("case") >> *searched_when_clause >> -(else_clause) >> keyword("end");

		const auto simple_when_clause_def = keyword("when") >> when_operand >> keyword("then") >> result;

		const auto searched_when_clause_def = keyword("when") >> boolean_value_expression >> keyword("then") >> result;

		const auto else_clause_def = keyword("else") >> result;

		const auto case_operand_def = row_value_predicand | overlaps_predicate;

		const auto when_operand_def =
			row_value_predicand
			| comparison_predicate_part_2
			| between_predicate_part_2
			| in_predicate_part_2
			| character_like_predicate_part_2
			| octet_like_predicate_part_2
			| similar_predicate_part_2
			| null_predicate_part_2
			| quantified_comparison_predicate_part_2
			| match_predicate_part_2
			| overlaps_predicate_part_2
			| distinct_predicate_part_2
			| member_predicate_part_2
			| submultiset_predicate_part_2
			| set_predicate_part_2
			| type_predicate_part_2;

		const auto result_def = value_expression | keyword("null");

		BOOST_SPIRIT_DEFINE(case_expression, case_abbreviation, case_specification, simple_case, searched_case,
				    simple_when_clause, searched_when_clause, else_clause, case_operand, when_operand, result);

		//-----------------------------------------------------------------------------
		// 6.12 <cast specification>
		//-----------------------------------------------------------------------------

		const rule<class cast_operand> cast_operand = "cast_operand";
		const rule<class cast_target> cast_target = "cast_target";

		const auto cast_specification_def = keyword("cast") >> lit('(') >> cast_operand >> keyword("as") >> cast_target >> lit(')');

		const auto cast_operand_def = value_expression | implicitly_typed_value_specification;

		const auto cast_target_def = schema_qualified_name | data_type;

		BOOST_SPIRIT_DEFINE(cast_specification, cast_operand, cast_target);

		//-----------------------------------------------------------------------------
		// 6.13 <next value expression>
		//-----------------------------------------------------------------------------

		const auto next_value_expression_def = keyword("next") >> keyword("value") >> keyword("for") >> schema_qualified_name;

		BOOST_SPIRIT_DEFINE(next_value_expression);

		//-----------------------------------------------------------------------------
		// 6.14 <field reference>
		//-----------------------------------------------------------------------------

		const auto field_reference_def = value_expression_primary >> lit('.') >> identifier;

		BOOST_SPIRIT_DEFINE(field_reference);

		//-----------------------------------------------------------------------------
		// 6.15 <subtype treatment>
		//-----------------------------------------------------------------------------

		const rule<class target_subtype> target_subtype = "target_subtype";

		const auto subtype_treatment_def =
			keyword("treat") >> lit('(') >> value_expression >> keyword("as") >> target_subtype >> lit(')');

		const auto target_subtype_def =	schema_qualified_type_name | reference_type;

		BOOST_SPIRIT_DEFINE(subtype_treatment, target_subtype);

		//-----------------------------------------------------------------------------
		// 6.16 <method invocation>
		//-----------------------------------------------------------------------------

		const rule<class direct_invocation> direct_invocation = "direct_invocation";
		const rule<class generalized_invocation> generalized_invocation = "generalized_invocation";

		const auto method_invocation_def = direct_invocation | generalized_invocation;

		const auto direct_invocation_def =
			value_expression_primary >> lit('.') >> identifier >> -(SQL_argument_list);

		const auto generalized_invocation_def =
			lit('(') >> value_expression_primary >> keyword("as") >> data_type >> lit(')') >>
			lit('.') >> identifier >> -(SQL_argument_list);

		BOOST_SPIRIT_DEFINE(method_invocation, direct_invocation, generalized_invocation);

		//-----------------------------------------------------------------------------
		// 6.17 <static method invocation>
		//-----------------------------------------------------------------------------

		const auto static_method_invocation_def =
			schema_qualified_type_name >> lexeme[lit("::")] >> identifier >> -(SQL_argument_list);

		BOOST_SPIRIT_DEFINE(static_method_invocation);

		//-----------------------------------------------------------------------------
		// 6.18 <new specification>
		//-----------------------------------------------------------------------------

		const rule<class new_invocation> new_invocation = "new_invocation";

		const auto new_specification_def = keyword("new") >> routine_invocation;

		const auto new_invocation_def = method_invocation | routine_invocation;

		BOOST_SPIRIT_DEFINE(new_specification, new_invocation);

		//-----------------------------------------------------------------------------
		// 6.19 <attribute or method reference>
		//-----------------------------------------------------------------------------

		const auto attribute_or_method_reference_def =
			value_expression_primary >> lexeme[lit("->")] >> identifier >> -(SQL_argument_list);

		BOOST_SPIRIT_DEFINE(attribute_or_method_reference);

		//-----------------------------------------------------------------------------
		// 6.20 <dereference operation>
		//-----------------------------------------------------------------------------

		const rule<class dereference_operation> dereference_operation = "dereference_operation";

		const auto dereference_operation_def = value_expression_primary >> lexeme[lit("->")] >> identifier;

		BOOST_SPIRIT_DEFINE(dereference_operation);

		//-----------------------------------------------------------------------------
		// 6.21 <method reference>
		//-----------------------------------------------------------------------------

		const rule<class method_reference> method_reference = "method_reference";

		const auto method_reference_def =
			value_expression_primary >> lexeme[lit("->")] >> identifier >> SQL_argument_list;

		BOOST_SPIRIT_DEFINE(method_reference);

		//-----------------------------------------------------------------------------
		// 6.22 <reference resolution>
		//-----------------------------------------------------------------------------

		const auto reference_resolution_def = keyword("deref") >> lit('(') >> value_expression_primary >> lit(')');

		BOOST_SPIRIT_DEFINE(reference_resolution);

		//-----------------------------------------------------------------------------
		// 6.23 <array element reference>
		//-----------------------------------------------------------------------------

		const auto array_element_reference_def =
 			array_value_expression >> left_bracket_or_trigraph >> numeric_value_expression >> right_bracket_or_trigraph;

		BOOST_SPIRIT_DEFINE(array_element_reference);

		//-----------------------------------------------------------------------------
		// 6.24 <multiset element reference>
		//-----------------------------------------------------------------------------

		const auto multiset_element_reference_def =
 			keyword("element") >> lit('(') >> multiset_value_expression >> lit(')');

		BOOST_SPIRIT_DEFINE(multiset_element_reference);

		//-----------------------------------------------------------------------------
		// 6.25 <value expression>
		//-----------------------------------------------------------------------------

		const rule<class common_value_expression> common_value_expression = "common_value_expression";
		const rule<class collection_value_expression> collection_value_expression = "collection_value_expression";

		const auto value_expression_def =
			common_value_expression
			| boolean_value_expression
			| row_value_expression;
		const auto common_value_expression_def =
			numeric_value_expression
			| string_value_expression
			| datetime_value_expression
			| interval_value_expression
			| value_expression_primary
			| collection_value_expression;

		const auto collection_value_expression_def = array_value_expression | multiset_value_expression;

		const auto collection_value_constructor_def = array_value_constructor | multiset_value_constructor;

		BOOST_SPIRIT_DEFINE(value_expression, common_value_expression, collection_value_expression, collection_value_constructor);

		//-----------------------------------------------------------------------------
		// 6.26 <numeric value expression>
		//-----------------------------------------------------------------------------

		const rule<class term> term = "term";
		const rule<class factor> factor = "factor";
		const rule<class numeric_primary> numeric_primary = "numeric_primary";

		const auto numeric_value_expression_def =
			term
			| (numeric_value_expression >> lit('+') >> term)
			| (numeric_value_expression >> lit('-') >> term);

		const auto term_def =
			factor
			| (term >> lit('*') >> factor)
			| (term >> lit('/') >> factor);

		const auto factor_def = -(sign) >> numeric_primary;

		const auto numeric_primary_def =
			value_expression_primary | numeric_value_function;

		BOOST_SPIRIT_DEFINE(numeric_value_expression, term, factor, numeric_primary);

		//-----------------------------------------------------------------------------
		// 6.27 <numeric value function>
		//-----------------------------------------------------------------------------

		const rule<class position_expression> position_expression = "position_expression";
		const rule<class string_position_expression> string_position_expression = "string_position_expression";
		const rule<class blob_position_expression> blob_position_expression = "blob_position_expression";
		const rule<class length_expression> length_expression = "length_expression";
		const rule<class char_length_expression> char_length_expression = "char_length_expression";
		const rule<class octet_length_expression> octet_length_expression = "octet_length_expression";
		const rule<class extract_expression> extract_expression = "extract_expression";
		const rule<class extract_field> extract_field = "extract_field";
		const rule<class time_zone_field> time_zone_field = "time_zone_field";
		const rule<class extract_source> extract_source = "extract_source";
		const rule<class cardinality_expression> cardinality_expression = "cardinality_expression";
		const rule<class absolute_value_expression> absolute_value_expression = "absolute_value_expression";
		const rule<class modulus_expression> modulus_expression = "modulus_expression";
		const rule<class natural_logarithm> natural_logarithm = "natural_logarithm";
		const rule<class exponential_function> exponential_function = "exponential_function";
		const rule<class power_function> power_function = "power_function";
		const rule<class square_root> square_root = "square_root";
		const rule<class floor_function> floor_function = "floor_function";
		const rule<class ceiling_function> ceiling_function = "ceiling_function";
		const rule<class width_bucket_function> width_bucket_function = "width_bucket_function";

		const auto numeric_value_function_def =
			position_expression
			| extract_expression
			| length_expression
			| cardinality_expression
			| absolute_value_expression
			| modulus_expression
			| natural_logarithm
			| exponential_function
			| power_function
			| square_root
			| floor_function
			| ceiling_function
			| width_bucket_function;

		const auto position_expression_def =
			string_position_expression
			| blob_position_expression;

		const auto string_position_expression_def =
			keyword("position") >> lit('(') >> string_value_expression >> keyword("in") >> string_value_expression
			>> -(keyword("using") >> char_length_units) >> lit(')');

		const auto blob_position_expression_def =
			keyword("position") >> lit('(') >> blob_value_expression >> keyword("in") >> blob_value_expression >> lit(')');

		const auto length_expression_def = char_length_expression | octet_length_expression;

		const auto char_length_expression_def = (keyword("char_length") | keyword("character_length")) >>
			lit('(') >> string_value_expression >> -(keyword("using") >> char_length_units) >> lit(')');

		const auto octet_length_expression_def = keyword("octet_length") >> lit('(') >> string_value_expression >> lit(')');

		const auto extract_expression_def = keyword("extract") >> lit('(') >> extract_field >> keyword("from") >> extract_source >> lit(')');

		const auto extract_field_def = primary_datetime_field | time_zone_field;

		const auto time_zone_field_def = keyword("timezone_hour") | keyword("timezone_minute");

		const auto extract_source_def = datetime_value_expression | interval_value_expression;

		const auto cardinality_expression_def = keyword("cardinality") >> lit('(') >> collection_value_expression >> lit(')');

		const auto absolute_value_expression_def = keyword("abs") >> lit('(') >> numeric_value_expression >> lit(')');

		const auto modulus_expression_def = keyword("mod") >> lit('(') >> numeric_value_expression >>
			lit(',') >> numeric_value_expression >> lit(')');

		const auto natural_logarithm_def = keyword("ln") >> lit('(') >> numeric_value_expression >> lit(')');

		const auto exponential_function_def = keyword("exp") >> lit('(') >> numeric_value_expression >> lit(')');

		const auto power_function_def = keyword("power") >> lit('(') >> numeric_value_expression >> lit(',') >> numeric_value_expression >> lit(')');

		const auto square_root_def = keyword("sqrt") >> lit('(') >> numeric_value_expression >> lit(')');

		const auto floor_function_def = keyword("floor") >> lit('(') >> numeric_value_expression >> lit(')');

		const auto ceiling_function_def = (keyword("ceil") | keyword("ceiling")) >>
			lit('(') >> numeric_value_expression >> lit(')');

		const auto width_bucket_function_def = keyword("width_bucket") >> lit('(') >> numeric_value_expression >> lit(',') >>
			numeric_value_expression >> lit(',') >> numeric_value_expression >> lit(',') >> numeric_value_expression >> lit(')');

		BOOST_SPIRIT_DEFINE(numeric_value_function, position_expression, string_position_expression, blob_position_expression, length_expression,
				    char_length_expression, octet_length_expression, extract_expression, extract_field, time_zone_field, extract_source,
				    cardinality_expression, absolute_value_expression, modulus_expression, natural_logarithm, exponential_function,
				    power_function, square_root, floor_function, ceiling_function, width_bucket_function);

		//-----------------------------------------------------------------------------
		// 6.28 <string value expression>
		//-----------------------------------------------------------------------------

		const rule<class character_factor> character_factor = "character_factor";
		const rule<class character_primary> character_primary = "character_primary";
		const rule<class blob_primary> blob_primary = "blob_primary";

		const auto string_value_expression_def = character_value_expression | blob_value_expression;

		const auto character_value_expression_def = character_factor % lexeme[lit("||")];

		const auto character_factor_def = character_primary >> -(collate_clause);

		const auto character_primary_def = value_expression_primary | string_value_function;

		const auto blob_value_expression_def = blob_primary % lexeme[lit("||")];

		const auto blob_primary_def = value_expression_primary | string_value_function;

		BOOST_SPIRIT_DEFINE(string_value_expression, character_value_expression, character_factor,
				    character_primary, blob_value_expression, blob_primary);

		//-----------------------------------------------------------------------------
		// 6.29 <string value function>
		//-----------------------------------------------------------------------------

		const rule<class character_value_function> character_value_function = "character_value_function";
		const rule<class character_substring_function> character_substring_function = "character_substring_function";
		const rule<class regular_expression_substring_function> regular_expression_substring_function = "regular_expression_substring_function";
		const rule<class fold> fold = "fold";
		const rule<class transcoding> transcoding = "transcoding";
		const rule<class character_transliteration> character_transliteration = "character_transliteration";
		const rule<class trim_function> trim_function = "trim_function";
		const rule<class trim_operands> trim_operands = "trim_operands";
		const rule<class trim_specification> trim_specification = "trim_specification";
		const rule<class character_overlay_function> character_overlay_function = "character_overlay_function";
		const rule<class normalize_function> normalize_function = "normalize_function";
		const rule<class specific_type_method> specific_type_method = "specific_type_method";
		const rule<class blob_value_function> blob_value_function = "blob_value_function";
		const rule<class blob_substring_function> blob_substring_function = "blob_substring_function";
		const rule<class blob_trim_function> blob_trim_function = "blob_trim_function";
		const rule<class blob_trim_operands> blob_trim_operands = "blob_trim_operands";
		const rule<class blob_overlay_function> blob_overlay_function = "blob_overlay_function";

		const auto string_value_function_def = character_value_function | blob_value_function;

		const auto character_value_function_def =
			character_substring_function
			| regular_expression_substring_function
			| fold
			| transcoding
			| character_transliteration
			| trim_function
			| character_overlay_function
			| normalize_function
			| specific_type_method;

		const auto character_substring_function_def =
			keyword("substring") >> lit('(') >> character_value_expression >> keyword("from") >> numeric_value_expression >>
			-(keyword("for") >> numeric_value_expression) >> -(keyword("using") >> char_length_units) >> lit(')');

		const auto regular_expression_substring_function_def =
			keyword("substring") >> lit('(') >> character_value_expression >>
			keyword("similar") >> character_value_expression >> keyword("escape") >> character_value_expression >> lit(')');

		const auto fold_def = (keyword("upper") | keyword("lower")) >> lit('(') >> character_value_expression >> lit(')');

		const auto transcoding_def = keyword("convert") >> lit('(') >> character_value_expression >>
			keyword("using") >> schema_qualified_name >> lit(')');

		const auto character_transliteration_def = keyword("translate") >> lit('(') >> character_value_expression >>
			keyword("using") >> schema_qualified_name >> lit(')');

		const auto trim_function_def = keyword("trim") >> lit('(') >> trim_operands >> lit(')');

		const auto trim_operands_def = -(-(trim_specification) >> -(character_value_expression) >> keyword("from")) >> character_value_expression;

		const auto trim_specification_def = keyword("leading") | keyword("trailing") | keyword("both");

		const auto character_overlay_function_def =
			keyword("overlay") >> lit('(') >> character_value_expression >> keyword("placing") >> character_value_expression >>
			keyword("from") >> numeric_value_expression >> -(keyword("for") >> numeric_value_expression) >>
			-(keyword("using") >> char_length_units) >> lit(')');

		const auto normalize_function_def = keyword("normalize") >> lit('(') >> character_value_expression >> lit(')');

		const auto specific_type_method_def = value_expression_primary >> lit('.') >> keyword("specifictype");

		const auto blob_value_function_def =
			blob_substring_function
			| blob_trim_function
			| blob_overlay_function;

		const auto blob_substring_function_def =
			keyword("substring") >> lit('(') >> blob_value_expression >> keyword("from") >> numeric_value_expression >>
			-(keyword("for") >> numeric_value_expression) >> lit(')');

		const auto blob_trim_function_def = keyword("trim") >> lit('(') >> blob_trim_operands >> lit(')');

		const auto blob_trim_operands_def = -(-(trim_specification) >> -(blob_value_expression) >> keyword("from")) >> blob_value_expression;

		const auto blob_overlay_function_def =
			keyword("overlay") >> lit('(') >> blob_value_expression >> keyword("placing") >> blob_value_expression >>
			keyword("from") >> numeric_value_expression >> -(keyword("for") >> numeric_value_expression) >> lit(')');

 		BOOST_SPIRIT_DEFINE(string_value_function, character_value_function, character_substring_function, regular_expression_substring_function,
				    fold, transcoding, character_transliteration, trim_function, trim_operands, trim_specification, character_overlay_function,
				    normalize_function, specific_type_method, blob_value_function, blob_substring_function, blob_trim_function, blob_trim_operands,
				    blob_overlay_function);

		//-----------------------------------------------------------------------------
		// 6.30 <datetime value expression>
		//-----------------------------------------------------------------------------

		const rule<class datetime_factor> datetime_factor = "datetime_factor";
		const rule<class datetime_primary> datetime_primary = "datetime_primary";
		const rule<class time_zone> time_zone = "time_zone";
		const rule<class time_zone_specifier> time_zone_specifier = "time_zone_specifier";

		const auto datetime_value_expression_def =
			datetime_factor
			| (interval_value_expression >> lit('+') >> datetime_factor)
			| (datetime_value_expression >> lit('+') >> interval_term)
			| (datetime_value_expression >> lit('-') >> interval_term);

		const auto datetime_factor_def = datetime_primary >> -(time_zone);

		const auto datetime_primary_def = value_expression_primary | datetime_value_function;

		const auto time_zone_def = keyword("at") >> time_zone_specifier;

		const auto time_zone_specifier_def = keyword("local") | (keyword("time") >> keyword("zone") >> interval_primary);

 		BOOST_SPIRIT_DEFINE(datetime_value_expression, datetime_factor, datetime_primary, time_zone, time_zone_specifier);

		//-----------------------------------------------------------------------------
		// 6.31 <datetime value function>
		//-----------------------------------------------------------------------------

		const rule<class current_date_value_function> current_date_value_function = "current_date_value_function";
		const rule<class current_time_value_function> current_time_value_function = "current_time_value_function";
		const rule<class current_local_time_value_function> current_local_time_value_function = "current_local_time_value_function";
		const rule<class current_timestamp_value_function> current_timestamp_value_function = "current_timestamp_value_function";
		const rule<class current_local_timestamp_value_function> current_local_timestamp_value_function = "current_local_timestamp_value_function";

		const auto datetime_value_function_def =
			current_date_value_function
			| current_time_value_function
			| current_timestamp_value_function
			| current_local_time_value_function
			| current_local_timestamp_value_function;

		const auto current_date_value_function_def = keyword("current_date");

		const auto current_time_value_function_def = keyword("current_time") >> -(lit('(') >> unsigned_integer >> lit(')'));

		const auto current_local_time_value_function_def = keyword("localtime") >> -(lit('(') >> unsigned_integer >> lit(')'));

		const auto current_timestamp_value_function_def = keyword("current_timestamp") >> -(lit('(') >> unsigned_integer >> lit(')'));

		const auto current_local_timestamp_value_function_def = keyword("localtimestamp") >> -(lit('(') >> unsigned_integer >> lit(')'));

		BOOST_SPIRIT_DEFINE(datetime_value_function, current_date_value_function, current_time_value_function,
				    current_local_time_value_function, current_timestamp_value_function, current_local_timestamp_value_function);

		//-----------------------------------------------------------------------------
		// 6.32 <interval value expression>
		//-----------------------------------------------------------------------------

		const rule<class interval_factor> interval_factor = "interval_factor";

		const auto interval_value_expression_def =
			interval_term
			| (interval_value_expression >> lit('+') >> interval_term)
			| (interval_value_expression >> lit('-') >> interval_term)
			| (lit('(') >> datetime_value_expression >> lit('-') >> datetime_factor >> lit(')') >> interval_qualifier);

		const auto interval_term_def =
			interval_factor
			| (interval_term >> lit('*') >> factor)
			| (interval_term >> lit('/') >> factor)
			| (term >> lit('*') >> interval_factor);

		const auto interval_factor_def = -(sign) >> interval_primary;

		const auto interval_primary_def =
			(value_expression_primary >> -(interval_qualifier)) | interval_value_function;

		BOOST_SPIRIT_DEFINE(interval_value_expression, interval_term, interval_factor, interval_primary);

		//-----------------------------------------------------------------------------
		//-----------------------------------------------------------------------------


		const auto interval_value_function_def = keyword("abs") >> lit('(') >> interval_value_expression >> lit(')');

		BOOST_SPIRIT_DEFINE(interval_value_function);

		//-----------------------------------------------------------------------------
		// 6.34 <boolean value expression>
		//-----------------------------------------------------------------------------

		const rule<class boolean_term> boolean_term = "boolean_term";
		const rule<class boolean_factor> boolean_factor = "boolean_factor";
		const rule<class boolean_test> boolean_test = "boolean_test";
		const rule<class truth_value> truth_value = "truth_value";
		const rule<class boolean_primary> boolean_primary = "boolean_primary";
		const rule<class boolean_predicand> boolean_predicand = "boolean_predicand";
		const rule<class parenthesized_boolean_value_expression> parenthesized_boolean_value_expression = "parenthesized_boolean_value_expression";

		const auto boolean_value_expression_def =
			boolean_term
			| (boolean_value_expression >> keyword("or") >> boolean_term);

		const auto boolean_term_def =
			boolean_factor
			| (boolean_term >> keyword("and") >> boolean_factor);

		const auto boolean_factor_def = -(keyword("not")) >> boolean_test;

		const auto boolean_test_def = boolean_primary >> -(keyword("is") >> -(keyword("not")) >> truth_value);

		const auto truth_value_def = keyword("true") | keyword("false") | keyword("unknown");

		const auto boolean_primary_def = predicate | boolean_predicand;

		const auto boolean_predicand_def =
			parenthesized_boolean_value_expression | nonparenthesized_value_expression_primary;

		const auto parenthesized_boolean_value_expression_def = lit('(') >> boolean_value_expression >> lit(')');

		BOOST_SPIRIT_DEFINE(boolean_value_expression, boolean_term, boolean_factor, boolean_test, truth_value,
				    boolean_primary, boolean_predicand, parenthesized_boolean_value_expression);

		//-----------------------------------------------------------------------------
		// 6.35 <array value expression>
		//-----------------------------------------------------------------------------

		const auto array_value_expression_def = value_expression_primary % lexeme[lit("||")];

		BOOST_SPIRIT_DEFINE(array_value_expression);

		//-----------------------------------------------------------------------------
		// 6.36 <array value constructor>
		//-----------------------------------------------------------------------------

		const rule<class array_value_constructor_by_enumeration> array_value_constructor_by_enumeration = "array_value_constructor_by_enumeration";
		const rule<class array_element_list> array_element_list = "array_element_list";
		const rule<class array_value_constructor_by_query> array_value_constructor_by_query = "array_value_constructor_by_query";

		const auto array_value_constructor_def =
			array_value_constructor_by_enumeration | array_value_constructor_by_query;

		const auto array_value_constructor_by_enumeration_def =
			keyword("array") >> left_bracket_or_trigraph >> array_element_list >> right_bracket_or_trigraph;

		const auto array_element_list_def = value_expression % lit(',');

		const auto array_value_constructor_by_query_def =
			keyword("array") >> lit('(') >> query_expression >> -(order_by_clause) >> lit(')');

		BOOST_SPIRIT_DEFINE(array_value_constructor, array_value_constructor_by_enumeration,
				    array_element_list, array_value_constructor_by_query);

		//-----------------------------------------------------------------------------
		// 6.37 <multiset value expression>
		//-----------------------------------------------------------------------------

		const rule<class multiset_term> multiset_term = "multiset_term";
		const rule<class multiset_primary> multiset_primary = "multiset_primary";

		const auto multiset_value_expression_def =
			multiset_term
			| (multiset_value_expression >> keyword("multiset") >> keyword("union") >>
			   -(keyword("all") | keyword("distinct")) >> multiset_term)
			| (multiset_value_expression >> keyword("multiset") >> keyword("except") >>
			   -(keyword("all") | keyword("distinct")) >> multiset_term);

		const auto multiset_term_def =
			multiset_primary
			| (multiset_term >> keyword("multiset") >> keyword("intersect") >>
			   -(keyword("all") | keyword("distinct")) >> multiset_primary);

		const auto multiset_primary_def = multiset_value_function | value_expression_primary;

		BOOST_SPIRIT_DEFINE(multiset_value_expression, multiset_term, multiset_primary);

		//-----------------------------------------------------------------------------
		// 6.38 <multiset value function>
		//-----------------------------------------------------------------------------

		const auto multiset_value_function_def = keyword("set") >> lit('(') >> multiset_value_expression >> lit(')');

		BOOST_SPIRIT_DEFINE(multiset_value_function);

		//-----------------------------------------------------------------------------
		// 6.39 <multiset value constructor>
		//-----------------------------------------------------------------------------

		const rule<class multiset_value_constructor_by_enumeration> multiset_value_constructor_by_enumeration = "multiset_value_constructor_by_enumeration";
		const rule<class multiset_element_list> multiset_element_list = "multiset_element_list";
		const rule<class multiset_value_constructor_by_query> multiset_value_constructor_by_query = "multiset_value_constructor_by_query";
		const rule<class table_value_constructor_by_query> table_value_constructor_by_query = "table_value_constructor_by_query";

		const auto multiset_value_constructor_def =
			multiset_value_constructor_by_enumeration
			| multiset_value_constructor_by_query
			| table_value_constructor_by_query;

		const auto multiset_value_constructor_by_enumeration_def = keyword("multiset") >>
			left_bracket_or_trigraph >> multiset_element_list >> right_bracket_or_trigraph;

		const auto multiset_element_list_def = value_expression % lit(',');

		const auto multiset_value_constructor_by_query_def = keyword("multiset") >>
			lit('(') >> query_expression >> lit(')');

		const auto table_value_constructor_by_query_def = keyword("table") >>
			lit('(') >> query_expression >> lit(')');

		BOOST_SPIRIT_DEFINE(multiset_value_constructor, multiset_value_constructor_by_enumeration, multiset_element_list,
				    multiset_value_constructor_by_query, table_value_constructor_by_query);

		//-----------------------------------------------------------------------------
		// 7 Query expressions
		//-----------------------------------------------------------------------------

		//-----------------------------------------------------------------------------
		// 7.1 <row value constructor>
		//-----------------------------------------------------------------------------

		const rule<class row_value_constructor> row_value_constructor = "row_value_constructor";
		const rule<class explicit_row_value_constructor> explicit_row_value_constructor = "explicit_row_value_constructor";
		const rule<class row_value_constructor_element_list> row_value_constructor_element_list = "row_value_constructor_element_list";
		const rule<class contextually_typed_row_value_constructor> contextually_typed_row_value_constructor = "contextually_typed_row_value_constructor";
		const rule<class contextually_typed_row_value_constructor_element_list> contextually_typed_row_value_constructor_element_list = "contextually_typed_row_value_constructor_element_list";
		const rule<class contextually_typed_row_value_constructor_element> contextually_typed_row_value_constructor_element = "contextually_typed_row_value_constructor_element";
		const rule<class row_value_constructor_predicand> row_value_constructor_predicand = "row_value_constructor_predicand";

		const auto row_value_constructor_def =
			common_value_expression
			| boolean_value_expression
			| explicit_row_value_constructor;

		const auto explicit_row_value_constructor_def =
			(lit('(') >> value_expression >> lit(',') >> row_value_constructor_element_list >> lit(')'))
			| (keyword("row") >> lit('(') >> row_value_constructor_element_list >> lit(')'))
			| subquery;

		const auto row_value_constructor_element_list_def = value_expression % lit(',');

		const auto contextually_typed_row_value_constructor_def =
			   common_value_expression
			   | boolean_value_expression
			   | contextually_typed_value_specification
			   | (lit('(') >> contextually_typed_row_value_constructor_element >> lit(',') >>
			      contextually_typed_row_value_constructor_element_list >> lit(')'))
			   | (keyword("row") >> lit('(') >> contextually_typed_row_value_constructor_element_list >> lit(')'));

		const auto contextually_typed_row_value_constructor_element_list_def =
			   contextually_typed_row_value_constructor_element % lit(',');;

		const auto contextually_typed_row_value_constructor_element_def =
			   value_expression | contextually_typed_value_specification;

		const auto row_value_constructor_predicand_def =
			   common_value_expression
			   | boolean_predicand
			   | explicit_row_value_constructor;

		BOOST_SPIRIT_DEFINE(row_value_constructor, explicit_row_value_constructor, row_value_constructor_element_list,
				    contextually_typed_row_value_constructor, contextually_typed_row_value_constructor_element_list,
				    contextually_typed_row_value_constructor_element, row_value_constructor_predicand);

		//-----------------------------------------------------------------------------
		// 7.2 <row value expression>
		//-----------------------------------------------------------------------------

		const rule<class table_row_value_expression> table_row_value_expression = "table_row_value_expression";
		const rule<class contextually_typed_row_value_expression> contextually_typed_row_value_expression = "contextually_typed_row_value_expression";

		const auto row_value_expression_def = nonparenthesized_value_expression_primary | explicit_row_value_constructor;

		const auto table_row_value_expression_def = nonparenthesized_value_expression_primary | row_value_constructor;

		const auto contextually_typed_row_value_expression_def = nonparenthesized_value_expression_primary | contextually_typed_row_value_constructor;

		const auto row_value_predicand_def = nonparenthesized_value_expression_primary | row_value_constructor_predicand;

		BOOST_SPIRIT_DEFINE(row_value_expression, table_row_value_expression, contextually_typed_row_value_expression,
				    row_value_predicand);

		//-----------------------------------------------------------------------------
		// 7.3 <table value constructor>
		//-----------------------------------------------------------------------------

		const rule<class table_value_constructor> table_value_constructor = "table_value_constructor";
		const rule<class row_value_expression_list> row_value_expression_list = "row_value_expression_list";
		const rule<class contextually_typed_table_value_constructor> contextually_typed_table_value_constructor = "contextually_typed_table_value_constructor";
		const rule<class contextually_typed_row_value_expression_list> contextually_typed_row_value_expression_list = "contextually_typed_row_value_expression_list";

		const auto table_value_constructor_def = keyword("values") >> row_value_expression_list;

		const auto row_value_expression_list_def = table_row_value_expression % lit(',');

		const auto contextually_typed_table_value_constructor_def = keyword("values") >> contextually_typed_row_value_expression_list;

		const auto contextually_typed_row_value_expression_list_def = contextually_typed_row_value_expression % lit(',');

		BOOST_SPIRIT_DEFINE(table_value_constructor, row_value_expression_list, contextually_typed_table_value_constructor,
				    contextually_typed_row_value_expression_list);

		//-----------------------------------------------------------------------------
		// 7.4 <table expression>
		//-----------------------------------------------------------------------------

		const rule<class table_expression> table_expression = "table_expression";

		const auto table_expression_def = from_clause >> -(where_clause) >> -(group_by_clause) >>
			-(having_clause) >> -(window_clause);

		BOOST_SPIRIT_DEFINE(table_expression);

		//-----------------------------------------------------------------------------
		// 7.5 <from clause>
		//-----------------------------------------------------------------------------

		const rule<class table_reference_list> table_reference_list = "table_reference_list";

		const auto from_clause_def = keyword("from") > table_reference_list;

		const auto table_reference_list_def = table_reference % lit(',');

		BOOST_SPIRIT_DEFINE(from_clause, table_reference_list);

		//-----------------------------------------------------------------------------
		// 7.6 <table reference>
		//-----------------------------------------------------------------------------

		const rule<struct table_primary_or_joined_table> table_primary_or_joined_table = "table_primary_or_joined_table";
		const rule<struct sample_clause> sample_clause = "sample_clause";
		const rule<struct sample_method> sample_method = "sample_method";
		const rule<struct repeatable_clause> repeatable_clause = "repeatable_clause";
		const rule<struct table_primary> table_primary = "table_primary";
		const rule<struct only_spec> only_spec = "only_spec";
		const rule<struct lateral_derived_table> lateral_derived_table = "lateral_derived_table";
		const rule<struct collection_derived_table> collection_derived_table = "collection_derived_table";
		const rule<struct table_function_derived_table> table_function_derived_table = "table_function_derived_table";
		const rule<struct table_or_query_name> table_or_query_name = "table_or_query_name";
		const rule<struct column_name_list> column_name_list = "column_name_list";

		const auto table_reference_def = table_primary_or_joined_table >> -(sample_clause);

		const auto table_primary_or_joined_table_def = table_primary | joined_table;

		const auto sample_clause_def =
			keyword("tablesample") >> sample_method >> lit('(') >> numeric_value_expression >> lit(')') >> -(repeatable_clause);

		const auto sample_method_def = keyword("bernoulli") | keyword("system");

		const auto repeatable_clause_def = keyword("repeatable") >> lit('(') >> numeric_value_expression >> lit(')');

		const auto table_primary_def =
			(table_or_query_name >> -(-(keyword("as")) >> identifier >> -(lit('(') >> column_name_list >> lit(')'))))
			| (subquery >> -(keyword("as")) >> identifier >> -(lit('(') >> column_name_list >> lit(')')))
			| (lateral_derived_table >> -(keyword("as") >> identifier >> -(lit('(') >> column_name_list >> lit(')'))))
			| (collection_derived_table >> -(keyword("as") >> identifier >> -(lit('(') >> column_name_list >> lit(')'))))
			| (table_function_derived_table >> -(keyword("as") >> identifier >> -(lit('(') >> column_name_list >> lit(')'))))
			| (only_spec >> -(keyword("as") >> identifier >> -(lit('(') >> column_name_list >> lit(')'))))
			| (lit('(') >> joined_table >> lit(')'));

		const auto only_spec_def = keyword("only") >> lit('(') >> table_or_query_name >> lit(')');

		const auto lateral_derived_table_def = keyword("lateral") >> subquery;

		const auto collection_derived_table_def = keyword("unnest") >> lit('(') >> collection_value_expression >> lit(')') >>
			-(keyword("with") >> keyword("ordinality"));

		const auto table_function_derived_table_def = keyword("table") >> lit('(') >> collection_value_expression >> lit(')');

		const auto table_or_query_name_def = local_or_schema_qualified_name | identifier;

		const auto column_name_list_def = identifier % lit(',');

		BOOST_SPIRIT_DEFINE(table_reference, table_primary_or_joined_table, sample_clause, sample_method, repeatable_clause, table_primary,
				    only_spec, lateral_derived_table, collection_derived_table, table_function_derived_table, table_or_query_name,
				    column_name_list);

		//-----------------------------------------------------------------------------
		// 7.7 <joined table>
		//-----------------------------------------------------------------------------

		const rule<struct cross_join> cross_join = "cross_join";
		const rule<struct qualified_join> qualified_join = "qualified_join";
		const rule<struct natural_join> natural_join = "natural_join";
		const rule<struct union_join> union_join = "union_join";
		const rule<struct join_specification> join_specification = "join_specification";
		const rule<struct join_condition> join_condition = "join_condition";
		const rule<struct named_columns_join> named_columns_join = "named_columns_join";
		const rule<struct join_type> join_type = "join_type";
		const rule<struct outer_join_type> outer_join_type = "outer_join_type";

		const auto joined_table_def =
			cross_join
			| qualified_join
			| natural_join
			| union_join;

		const auto cross_join_def = table_reference >> keyword("cross") >> keyword("join") >> table_primary;

		const auto qualified_join_def = table_reference >> -(join_type) >> keyword("join") >> table_reference >> join_specification;

		const auto natural_join_def = table_reference >> keyword("natural") >> -(join_type) >> keyword("join") >> table_primary;

		const auto union_join_def = table_reference >> keyword("union") >> keyword("join") >> table_primary;

		const auto join_specification_def = join_condition | named_columns_join;

		const auto join_condition_def = keyword("on") >> boolean_value_expression;

		const auto named_columns_join_def = keyword("using") >> lit('(') >> column_name_list >> lit(')');

		const auto join_type_def = keyword("inner") | (outer_join_type >> -(keyword("outer")));

		const auto outer_join_type_def = keyword("left") | keyword("right") | keyword("full");


		BOOST_SPIRIT_DEFINE(joined_table, cross_join, qualified_join, natural_join, union_join, join_specification, join_condition,
				    named_columns_join, join_type, outer_join_type);

		//-----------------------------------------------------------------------------
		// 7.8 <where clause>
		//-----------------------------------------------------------------------------

		const auto where_clause_def = keyword("where") > boolean_value_expression;

		BOOST_SPIRIT_DEFINE(where_clause);

		//-----------------------------------------------------------------------------
		// 7.9 <group by clause>
		//-----------------------------------------------------------------------------

		const rule<class grouping_element_list> grouping_element_list = "grouping_element_list";
		const rule<class grouping_element> grouping_element = "grouping_element";
		const rule<class ordinary_grouping_set> ordinary_grouping_set = "ordinary_grouping_set";
		const rule<class grouping_column_reference> grouping_column_reference = "grouping_column_reference";
		const rule<class grouping_column_reference_list> grouping_column_reference_list = "grouping_column_reference_list";
		const rule<class rollup_list> rollup_list = "rollup_list";
		const rule<class ordinary_grouping_set_list> ordinary_grouping_set_list = "ordinary_grouping_set_list";
		const rule<class cube_list> cube_list = "cube_list";
		const rule<class grouping_sets_specification> grouping_sets_specification = "grouping_sets_specification";
		const rule<class grouping_set_list> grouping_set_list = "grouping_set_list";
		const rule<class grouping_set> grouping_set = "grouping_set";

		const auto group_by_clause_def = keyword("group") >> keyword("by") > -(set_quantifier) >> grouping_element_list;

		const auto grouping_element_list_def = grouping_element % lit(',');

		const auto grouping_element_def =
			ordinary_grouping_set
			| rollup_list
			| cube_list
			| grouping_sets_specification
			| (lit('(') >> lit(')'));

		const auto ordinary_grouping_set_def =
			grouping_column_reference
			| (lit('(') >> grouping_column_reference_list >> lit(')'));

		const auto grouping_column_reference_def = column_reference >> -(collate_clause);

		const auto grouping_column_reference_list_def = grouping_column_reference % lit(',');

		const auto rollup_list_def = keyword("rollup") >> lit('(') >> ordinary_grouping_set_list >> lit(')');

		const auto ordinary_grouping_set_list_def = ordinary_grouping_set % lit(',');

		const auto cube_list_def = keyword("cube") >> lit('(') >> ordinary_grouping_set_list >> lit(')');

		const auto grouping_sets_specification_def = keyword("grouping") >> keyword("sets") >> lit('(') >> grouping_set_list >> lit(')');

		const auto grouping_set_list_def = grouping_set % lit(',');

		const auto grouping_set_def =
			ordinary_grouping_set
			| rollup_list
			| cube_list
			| grouping_sets_specification
			| (lit('(') >> lit(')'));

		BOOST_SPIRIT_DEFINE(group_by_clause, grouping_element_list, grouping_element, ordinary_grouping_set, grouping_column_reference,
				    grouping_column_reference_list, rollup_list, ordinary_grouping_set_list, cube_list,
				    grouping_sets_specification, grouping_set_list, grouping_set);

		//-----------------------------------------------------------------------------
		//-----------------------------------------------------------------------------

		const auto having_clause_def = keyword("having") >> boolean_value_expression;

		BOOST_SPIRIT_DEFINE(having_clause);

		//-----------------------------------------------------------------------------
		// 7.11 <window clause>
		//-----------------------------------------------------------------------------

		const rule<class window_definition_list> window_definition_list = "window_definition_list";
		const rule<class window_definition> window_definition = "window_definition";
		const rule<class window_specification_details> window_specification_details = "window_specification_details";
		const rule<class window_partition_clause> window_partition_clause = "window_partition_clause";
		const rule<class window_partition_column_reference_list> window_partition_column_reference_list = "window_partition_column_reference_list";
		const rule<class window_partition_column_reference> window_partition_column_reference = "window_partition_column_reference";
		const rule<class window_order_clause> window_order_clause = "window_order_clause";
		const rule<class window_frame_clause> window_frame_clause = "window_frame_clause";
		const rule<class window_frame_units> window_frame_units = "window_frame_units";
		const rule<class window_frame_extent> window_frame_extent = "window_frame_extent";
		const rule<class window_frame_start> window_frame_start = "window_frame_start";
		const rule<class window_frame_preceding> window_frame_preceding = "window_frame_preceding";
		const rule<class window_frame_between> window_frame_between = "window_frame_between";
		const rule<class window_frame_bound> window_frame_bound = "window_frame_bound";
		const rule<class window_frame_following> window_frame_following = "window_frame_following";
		const rule<class window_frame_exclusion> window_frame_exclusion = "window_frame_exclusion";

		const auto window_clause_def = keyword("window") > window_definition_list;

		const auto window_definition_list_def = window_definition % lit(',');

		const auto window_definition_def = identifier >> keyword("as") >> window_specification;

		const auto window_specification_def = lit('(') >> window_specification_details > lit(')');

		const auto window_specification_details_def =
			-(identifier) >> -(window_partition_clause) >> -(window_order_clause) >> -(window_frame_clause);

		const auto window_partition_clause_def = keyword("partition") > keyword("by") > window_partition_column_reference_list;

		const auto window_partition_column_reference_list_def = window_partition_column_reference % lit(',');

		const auto window_partition_column_reference_def = column_reference >> -(collate_clause);

		const auto window_order_clause_def = keyword("order") >> keyword("by") > sort_specification_list;

		const auto window_frame_clause_def = window_frame_units >> window_frame_extent >> -(window_frame_exclusion);

		const auto window_frame_units_def = keyword("rows") | keyword("range");

		const auto window_frame_extent_def = window_frame_start | window_frame_between;

		const auto window_frame_start_def = (keyword("unbounded") >> keyword("preceding"))
			| window_frame_preceding
			| (keyword("current") >> keyword("row"));

		const auto window_frame_preceding_def = unsigned_value_specification >> keyword("preceding");

		const auto window_frame_between_def = keyword("between") >> window_frame_bound >> keyword("and") >> window_frame_bound;

		const auto window_frame_bound_def =
			window_frame_start
			| (keyword("unbounded") >> keyword("following"))
			| window_frame_following;

		const auto window_frame_following_def = unsigned_value_specification >> keyword("following");

		const auto window_frame_exclusion_def =
			(keyword("exclude") >> keyword("current") >> keyword("row"))
			| (keyword("exclude") >> keyword("group"))
			| (keyword("exclude") >> keyword("ties"))
			| (keyword("exclude") >> keyword("no") >> keyword("others"));

		BOOST_SPIRIT_DEFINE(window_clause, window_definition_list, window_definition, window_specification, window_specification_details,
				    window_partition_clause, window_partition_column_reference_list, window_partition_column_reference,
				    window_order_clause, window_frame_clause, window_frame_units, window_frame_extent, window_frame_start,
				    window_frame_preceding, window_frame_between, window_frame_bound, window_frame_following, window_frame_exclusion);

		//-----------------------------------------------------------------------------
		// 7.12 <query specification>
		//-----------------------------------------------------------------------------

		const rule<struct query_specification> query_specification = "query_specification";
		const rule<struct select_list> select_list = "select_list";
		const rule<struct select_sublist> select_sublist = "select_sublist";
		const rule<struct qualified_asterisk> qualified_asterisk = "qualified_asterisk";
		const rule<struct asterisked_identifier_chain> asterisked_identifier_chain = "asterisked_identifier_chain";
		const rule<struct derived_column> derived_column = "derived_column";
		const rule<struct as_clause> as_clause = "as_clause";
		const rule<struct all_fields_reference> all_fields_reference = "all_fields_reference";

		const auto query_specification_def = keyword("select") > -(set_quantifier) >> select_list >> table_expression;

		const auto select_list_def = lit('*') | (select_sublist % lit(','));

		const auto select_sublist_def = derived_column | qualified_asterisk;

		const auto qualified_asterisk_def =
			       (asterisked_identifier_chain >> lit('.') >> lit('*'))
					| all_fields_reference;

		const auto asterisked_identifier_chain_def = identifier % lit('.');

		const auto derived_column_def = value_expression >> -as_clause;

		const auto as_clause_def = -keyword("as") >> identifier;

		const auto all_fields_reference_def = value_expression_primary >> lit('.') >> lit('*') >> -(keyword("as") > lit('(') > column_name_list > lit(')'));

		BOOST_SPIRIT_DEFINE(query_specification, select_list, select_sublist, qualified_asterisk,
				    asterisked_identifier_chain, derived_column, as_clause, all_fields_reference);

		//-----------------------------------------------------------------------------
		// 7.13 <query expression>
		//-----------------------------------------------------------------------------

		const rule<struct with_clause> with_clause = "with_clause";
		const rule<struct with_list> with_list = "with_list";
		const rule<struct with_list_element> with_list_element = "with_list_element";
		const rule<struct query_expression_body> query_expression_body = "query_expression_body";
		const rule<struct non_join_query_expression> non_join_query_expression = "non_join_query_expression";
		const rule<struct query_term> query_term = "query_term";
		const rule<struct non_join_query_term> non_join_query_term = "non_join_query_term";
		const rule<struct query_primary> query_primary = "query_primary";
		const rule<struct non_join_query_primary> non_join_query_primary = "non_join_query_primary";
		const rule<struct simple_table> simple_table = "simple_table";
		const rule<struct explicit_table> explicit_table = "explicit_table";
		const rule<struct corresponding_spec> corresponding_spec = "corresponding_spec";

		const auto query_expression_def = -(with_clause) >> query_expression_body;

		const auto with_clause_def = keyword("with") >> -(keyword("recursive")) >> with_list;

		const auto with_list_def = with_list_element % lit(',');

		const auto with_list_element_def =
			identifier >> -(lit('(') >> column_name_list >> lit(')')) >>
			keyword("as") >> lit('(') >> query_expression >> lit(')') >> -(search_or_cycle_clause);

		const auto query_expression_body_def = non_join_query_expression | joined_table;

		const auto non_join_query_expression_def =
			non_join_query_term
			| (query_expression_body >> keyword("union") >> -(keyword("all") | keyword("distinct")) >>
			   -(corresponding_spec) >> query_term)
			| (query_expression_body >> keyword("except") >> -(keyword("all") | keyword("distinct")) >>
			   -(corresponding_spec) >> query_term);

		const auto query_term_def = non_join_query_term | joined_table;

		const auto non_join_query_term_def =
			non_join_query_primary
			| (query_term >> keyword("intersect") >> -(keyword("all") | keyword("distinct")) >> -(corresponding_spec) >> query_primary);

		const auto query_primary_def = non_join_query_primary | joined_table;

		const auto non_join_query_primary_def = simple_table | (lit('(') >> non_join_query_expression >> lit(')'));

		const auto simple_table_def =
			query_specification
			| table_value_constructor
			| explicit_table;

		const auto explicit_table_def = keyword("table") >> table_or_query_name;

		const auto corresponding_spec_def = keyword("corresponding") >> -(keyword("by") >> lit('(') >> column_name_list >> lit(')'));

		BOOST_SPIRIT_DEFINE(query_expression, with_clause, with_list, with_list_element, query_expression_body,
				    non_join_query_expression, query_term, non_join_query_term, query_primary, non_join_query_primary,
				    simple_table, explicit_table, corresponding_spec);

		//-----------------------------------------------------------------------------
		// 7.14 <search or cycle clause>
		//-----------------------------------------------------------------------------

		const rule<struct search_clause> search_clause = "search_clause";
		const rule<struct recursive_search_order> recursive_search_order = "recursive_search_order";
		const rule<struct cycle_clause> cycle_clause = "cycle_clause";
		const rule<struct cycle_column_list> cycle_column_list = "cycle_column_list";

		const auto search_or_cycle_clause_def =
			search_clause
			| cycle_clause
			| (search_clause >> cycle_clause);

		const auto search_clause_def = keyword("search") >> recursive_search_order >> keyword("set") >> identifier;

		const auto recursive_search_order_def =
			(keyword("depth") >> keyword("first") >> keyword("by") >> sort_specification_list)
			| (keyword("breadth") >> keyword("first") >> keyword("by") >> sort_specification_list);

		const auto cycle_clause_def =
			keyword("cycle") >> cycle_column_list >>
			keyword("set") >> identifier >> keyword("to") >> value_expression >>
			keyword("default") >> value_expression >>
			keyword("using") >> identifier;

		const auto cycle_column_list_def = identifier % lit(',');

		BOOST_SPIRIT_DEFINE(search_or_cycle_clause, search_clause, recursive_search_order, cycle_clause, cycle_column_list);

		//-----------------------------------------------------------------------------
		// 7.15 <subquery>
		//-----------------------------------------------------------------------------

		const auto subquery_def = lit('(') >> query_expression >> lit(')');

		BOOST_SPIRIT_DEFINE(subquery);

		//-----------------------------------------------------------------------------
		// 8 Predicates
		//-----------------------------------------------------------------------------

		//-----------------------------------------------------------------------------
		// 8.1 <predicate>
		//-----------------------------------------------------------------------------

		const auto predicate_def =
			comparison_predicate
			| between_predicate
			| in_predicate
			| like_predicate
			| similar_predicate
			| null_predicate
			| quantified_comparison_predicate
			| exists_predicate
			| unique_predicate
			| normalized_predicate
			| match_predicate
			| overlaps_predicate
			| distinct_predicate
			| member_predicate
			| submultiset_predicate
			| set_predicate
			| type_predicate;

		BOOST_SPIRIT_DEFINE(predicate);

		//-----------------------------------------------------------------------------
		// 8.2 <comparison predicate>
		//-----------------------------------------------------------------------------

		const rule<struct comp_op, CartoSQL::Expr::compop_t> comp_op = "comp_op";

		const auto comparison_predicate_def = row_value_predicand >> comparison_predicate_part_2;

		const auto comparison_predicate_part_2_def = comp_op >> row_value_predicand;

		const auto comp_op_def = lexeme[compoptable];

		BOOST_SPIRIT_DEFINE(comparison_predicate, comparison_predicate_part_2, comp_op);

		//-----------------------------------------------------------------------------
		// 8.3 <between predicate>
		//-----------------------------------------------------------------------------

		const auto between_predicate_def = row_value_predicand >> between_predicate_part_2;

		const auto between_predicate_part_2_def =
			-(keyword("not")) >> keyword("between") >>
			-(keyword("asymmetric") | keyword("symmetric")) >>
			row_value_predicand >> keyword("and") >> row_value_predicand;

		BOOST_SPIRIT_DEFINE(between_predicate, between_predicate_part_2);

		//-----------------------------------------------------------------------------
		// 8.4 <in predicate>
		//-----------------------------------------------------------------------------

		const rule<struct in_predicate_value> in_predicate_value = "in_predicate_value";
		const rule<struct in_value_list> in_value_list = "in_value_list";

		const auto in_predicate_def = row_value_predicand >> in_predicate_part_2;

		const auto in_predicate_part_2_def = -(keyword("not")) >> keyword("in") >> in_predicate_value;

		const auto in_predicate_value_def = subquery | (lit('(') >> in_value_list >> lit(')'));

		const auto in_value_list_def = row_value_expression % lit(',');

		BOOST_SPIRIT_DEFINE(in_predicate, in_predicate_part_2, in_predicate_value, in_value_list);

		//-----------------------------------------------------------------------------
		// 8.5 <like predicate>
		//-----------------------------------------------------------------------------

		const rule<struct character_like_predicate> character_like_predicate = "character_like_predicate";
		const rule<struct octet_like_predicate> octet_like_predicate = "octet_like_predicate";

		const auto like_predicate_def = character_like_predicate | octet_like_predicate;

		const auto character_like_predicate_def = row_value_predicand >> character_like_predicate_part_2;

		const auto character_like_predicate_part_2_def = -(keyword("not")) >> keyword("like") >>
			character_value_expression >> -(keyword("escape") >> character_value_expression);

		const auto octet_like_predicate_def = row_value_predicand >> octet_like_predicate_part_2;

		const auto octet_like_predicate_part_2_def = -(keyword("not")) >> keyword("like") >>
			blob_value_expression >> -(keyword("escape") >> blob_value_expression);

		BOOST_SPIRIT_DEFINE(like_predicate, character_like_predicate, character_like_predicate_part_2,
				    octet_like_predicate, octet_like_predicate_part_2);

		//-----------------------------------------------------------------------------
		// 8.6 <similar predicate>
		//-----------------------------------------------------------------------------

		const auto similar_predicate_def = row_value_predicand >> similar_predicate_part_2;

		const auto similar_predicate_part_2_def = -(keyword("not")) >> keyword("similar") >> keyword("to") >>
			character_value_expression >> -(keyword("escape") >> character_value_expression);

		BOOST_SPIRIT_DEFINE(similar_predicate, similar_predicate_part_2);

		//-----------------------------------------------------------------------------
		// 8.7 <null predicate>
		//-----------------------------------------------------------------------------

		const auto null_predicate_def = row_value_predicand >> null_predicate_part_2;

		const auto null_predicate_part_2_def = keyword("is") >> -(keyword("not")) >> keyword("null");

		BOOST_SPIRIT_DEFINE(null_predicate, null_predicate_part_2);

		//-----------------------------------------------------------------------------
		// 8.8 <quantified comparison predicate>
		//-----------------------------------------------------------------------------

		const rule<struct quantifier> quantifier = "quantifier";
		const rule<struct all> all = "all";
		const rule<struct some> some = "some";

		const auto quantified_comparison_predicate_def = row_value_predicand >> quantified_comparison_predicate_part_2;

		const auto quantified_comparison_predicate_part_2_def = comp_op >> quantifier >> subquery;

		const auto quantifier_def = all | some;

		const auto all_def = keyword("all");

		const auto some_def = keyword("some") | keyword("any");

		BOOST_SPIRIT_DEFINE(quantified_comparison_predicate, quantified_comparison_predicate_part_2, quantifier, all, some);

		//-----------------------------------------------------------------------------
		// 8.9 <exists predicate>
		//-----------------------------------------------------------------------------

		const auto exists_predicate_def = keyword("exists") >> subquery;

		BOOST_SPIRIT_DEFINE(exists_predicate);

		//-----------------------------------------------------------------------------
		// 8.10 <unique predicate>
		//-----------------------------------------------------------------------------

		const auto unique_predicate_def = keyword("unique") >> subquery;

		BOOST_SPIRIT_DEFINE(unique_predicate);

		//-----------------------------------------------------------------------------
		// 8.11 <normalized predicate>
		//-----------------------------------------------------------------------------

		const auto normalized_predicate_def = string_value_expression >> keyword("is") >> -(keyword("not")) >> keyword("normalized");

		BOOST_SPIRIT_DEFINE(normalized_predicate);

		//-----------------------------------------------------------------------------
		// 8.12 <match predicate>
		//-----------------------------------------------------------------------------

		const auto match_predicate_def = row_value_predicand >> match_predicate_part_2;

		const auto match_predicate_part_2_def = keyword("match") >> -(keyword("unique")) >>
			-(keyword("simple") | keyword("partial") | keyword("full")) >> subquery;

		BOOST_SPIRIT_DEFINE(match_predicate, match_predicate_part_2);

		//-----------------------------------------------------------------------------
		// 8.13 <overlaps predicate>
		//-----------------------------------------------------------------------------

		const auto overlaps_predicate_def = row_value_predicand >> overlaps_predicate_part_2;

		const auto overlaps_predicate_part_2_def = keyword("overlaps") >> row_value_predicand;

		BOOST_SPIRIT_DEFINE(overlaps_predicate, overlaps_predicate_part_2);

		//-----------------------------------------------------------------------------
		// 8.14 <distinct predicate>
		//-----------------------------------------------------------------------------

		const auto distinct_predicate_def = row_value_predicand >> distinct_predicate_part_2;

		const auto distinct_predicate_part_2_def = keyword("is") >> keyword("distinct") >> keyword("from") >> row_value_predicand;

		BOOST_SPIRIT_DEFINE(distinct_predicate, distinct_predicate_part_2);

		//-----------------------------------------------------------------------------
		// 8.15 <member predicate>
		//-----------------------------------------------------------------------------

		const auto member_predicate_def = row_value_predicand >> member_predicate_part_2;

		const auto member_predicate_part_2_def = -(keyword("not")) >> keyword("member") >>
			-(keyword("of")) >> multiset_value_expression;

		BOOST_SPIRIT_DEFINE(member_predicate, member_predicate_part_2);

		//-----------------------------------------------------------------------------
		// 8.16 <submultiset predicate>
		//-----------------------------------------------------------------------------

		const auto submultiset_predicate_def = row_value_predicand >> submultiset_predicate_part_2;

		const auto submultiset_predicate_part_2_def = -(keyword("not")) >> keyword("submultiset") >>
			-(keyword("of")) >> multiset_value_expression;

		BOOST_SPIRIT_DEFINE(submultiset_predicate, submultiset_predicate_part_2);

		//-----------------------------------------------------------------------------
		// 8.17 <set predicate>
		//-----------------------------------------------------------------------------

		const auto set_predicate_def = row_value_predicand >> set_predicate_part_2;

		const auto set_predicate_part_2_def = keyword("is") >> -(keyword("not")) >>
			no_case[lit('a')] >> keyword("set");

		BOOST_SPIRIT_DEFINE(set_predicate, set_predicate_part_2);

		//-----------------------------------------------------------------------------
		// 8.18 <type predicate>
		//-----------------------------------------------------------------------------

		const rule<struct type_list> type_list = "type_list";
		const rule<struct user_defined_type_specification> user_defined_type_specification = "user_defined_type_specification";

		const auto type_predicate_def = row_value_predicand >> type_predicate_part_2;

		const auto type_predicate_part_2_def = keyword("is") >> -(keyword("not")) >>
			keyword("of") >> lit('(') >> type_list >> lit(')');

		const auto type_list_def = user_defined_type_specification % lit(',');

		const auto user_defined_type_specification_def = -(keyword("only")) >> schema_qualified_type_name;

		BOOST_SPIRIT_DEFINE(type_predicate, type_predicate_part_2, type_list, user_defined_type_specification);

		//-----------------------------------------------------------------------------
		// 10 Additional common elements
		//-----------------------------------------------------------------------------

		//-----------------------------------------------------------------------------
		// 10.1 <interval qualifier>
		//-----------------------------------------------------------------------------

		const rule<struct start_field> start_field = "start_field";
		const rule<struct end_field> end_field = "end_field";
		const rule<struct single_datetime_field> single_datetime_field = "single_datetime_field";
		const rule<struct non_second_primary_datetime_field> non_second_primary_datetime_field = "non_second_primary_datetime_field";

		const auto interval_qualifier_def =
			(start_field >> keyword("to") >> end_field)
			| single_datetime_field;

		const auto start_field_def = non_second_primary_datetime_field >> -(lit('(') >> unsigned_integer > lit(')'));

		const auto end_field_def =
			non_second_primary_datetime_field
			| (keyword("second") >> -(lit('(') >> unsigned_integer > lit(')')));

		const auto single_datetime_field_def =
			(non_second_primary_datetime_field >> -(lit('(') >> unsigned_integer > lit(')')))
			| (keyword("second") >> -(lit('(') >> unsigned_integer >> -(lit(',') >> unsigned_integer) >> lit(')')));

		const auto primary_datetime_field_def =
			non_second_primary_datetime_field
			| keyword("second");

		const auto non_second_primary_datetime_field_def = keyword("year") | keyword("month")
			| keyword("day") | keyword("hour") | keyword("minute");

		BOOST_SPIRIT_DEFINE(interval_qualifier, start_field, end_field, single_datetime_field, primary_datetime_field, non_second_primary_datetime_field);

		//-----------------------------------------------------------------------------
		// 10.2 <language clause>
		//-----------------------------------------------------------------------------

// <language clause> ::= keyword("language") <language name>;
//
// <language name> ::= keyword("ada") | C | keyword("cobol") | keyword("fortran") | keyword("mumps") | keyword("pascal") | keyword("pli") | keyword("sql");
//
// //p
// Table 14 // Standard programming languages
// ///p
//
// //## <table border=1>
// //## <tr> <th> Language keyword </th> <th> Relevant standard </th> </tr>
// //## <tr><td>ADA</td><td>ISO/IEC 8652</td></tr>
// //## <tr><td>C</td><td>ISO/IEC 9899</td></tr>
// //## <tr><td>COBOL</td><td>ISO 1989</td></tr>
// //## <tr><td>FORTRAN</td><td>ISO 1539</td></tr>
// //## <tr><td>MUMPS</td><td>ISO/IEC 11756</td></tr>
// //## <tr><td>PASCAL</td><td>ISO/IEC 7185 and ISO/IEC 10206</td></tr>
// //## <tr><td>PLI</td><td>ISO 6160</td></tr>
// //## <tr><td>SQL</td><td>ISO/IEC 9075</td></tr>
// //## </table>
//

		//-----------------------------------------------------------------------------
		// 10.3 <path specification>
		//-----------------------------------------------------------------------------

// <path specification> ::= keyword("path") <schema name list>
//
// <schema name list> ::= schema_name [ { lit(',') schema_name }... ]
//

		//-----------------------------------------------------------------------------
		// 10.4 <routine invocation>
		//-----------------------------------------------------------------------------

		const rule<struct routine_name> routine_name = "routine_name";
		const rule<struct SQL_argument> SQL_argument = "SQL_argument";
		const rule<struct generalized_expression> generalized_expression = "generalized_expression";

		const auto routine_invocation_def = routine_name >> SQL_argument_list;

		const auto routine_name_def = -(schema_name >> lit('.')) >> identifier;

		const auto SQL_argument_list_def = lit('(') >> -(SQL_argument % lit(',')) >> lit(')');

		const auto SQL_argument_def =
			value_expression
			| generalized_expression
			| target_specification;

		const auto generalized_expression_def = value_expression >> keyword("as") >> schema_qualified_type_name;

		BOOST_SPIRIT_DEFINE(routine_invocation, routine_name, SQL_argument_list, SQL_argument, generalized_expression);

		//-----------------------------------------------------------------------------
		// 10.6 <specific routine designator>
		//-----------------------------------------------------------------------------

// <specific routine designator> ::=
// 		keyword("specific") <routine type> schema_qualified_name
// 			|	<routine type> <member name>
// 		[ keyword("for") schema_qualified_type_name ]
//
// <routine type> ::=
// 		keyword("routine")
// 	|	keyword("function")
// 	|	keyword("procedure")
// 	|	[ keyword("instance") | keyword("static") | keyword("constructor") ] keyword("method")
//
// <member name> ::= <member name alternatives> [ <data type list> ]
//
// <member name alternatives> ::= schema_qualified_name | identifier
//
// <data type list> ::= lit('(') [ data_type [ { lit(',') data_type }... ] ] lit(')')
//
		//-----------------------------------------------------------------------------
		// 10.7 <collate clause>
		//-----------------------------------------------------------------------------

		const auto collate_clause_def = keyword("collate") >> schema_qualified_name;

		BOOST_SPIRIT_DEFINE(collate_clause);

		//-----------------------------------------------------------------------------
		// 10.8 <constraint name definition> and <constraint characteristics>
		//-----------------------------------------------------------------------------


// <constraint name definition> ::= keyword("constraint") schema_qualified_name
//
// <constraint characteristics> ::=
// 		<constraint check time> [ [ keyword("not") ] keyword("deferrable") ]
// 	|	[ keyword("not") ] keyword("deferrable") [ <constraint check time> ]
//
// <constraint check time> ::= keyword("initially") keyword("deferred") | keyword("initially") keyword("immediate")

		//-----------------------------------------------------------------------------
		// 10.9 <aggregate function>
		//-----------------------------------------------------------------------------

		const rule<struct general_set_function> general_set_function = "general_set_function";
		const rule<struct computational_operation> computational_operation = "computational_operation";
		const rule<struct filter_clause> filter_clause = "filter_clause";
		const rule<struct binary_set_function> binary_set_function = "binary_set_function";
		const rule<struct binary_set_function_type> binary_set_function_type = "binary_set_function_type";
		const rule<struct ordered_set_function> ordered_set_function = "ordered_set_function";
		const rule<struct hypothetical_set_function> hypothetical_set_function = "hypothetical_set_function";
		const rule<struct within_group_specification> within_group_specification = "within_group_specification";
		const rule<struct hypothetical_set_function_value_expression_list> hypothetical_set_function_value_expression_list = "hypothetical_set_function_value_expression_list";
		const rule<struct inverse_distribution_function> inverse_distribution_function = "inverse_distribution_function";
		const rule<struct inverse_distribution_function_type> inverse_distribution_function_type = "inverse_distribution_function_type";

		const auto aggregate_function_def =
			(keyword("count") >> lit('(') >> lit('*') >> lit(')') >> -(filter_clause))
			| (general_set_function >> -(filter_clause))
			| (binary_set_function >> -(filter_clause))
			| (ordered_set_function >> -(filter_clause));

		const auto general_set_function_def = computational_operation >> lit('(') >> -(set_quantifier) >> value_expression >> lit(')');

		const auto computational_operation_def =
			keyword("avg") | keyword("max") | keyword("min") | keyword("sum")
			| keyword("every") | keyword("any") | keyword("some")
			| keyword("count")
			| keyword("stddev_pop") | keyword("stddev_samp") | keyword("var_samp") | keyword("var_pop")
			| keyword("collect") | keyword("fusion") | keyword("intersection");

		const auto set_quantifier_def = keyword("distinct") | keyword("all");

		const auto filter_clause_def = keyword("filter") >> lit('(') >> keyword("where") >> boolean_value_expression >> lit(')');

		const auto binary_set_function_def = binary_set_function_type >> lit('(') >> numeric_value_expression >> lit(',') >> numeric_value_expression >> lit(')');

		const auto binary_set_function_type_def =
			keyword("covar_pop") | keyword("covar_samp") | keyword("corr") | keyword("regr_slope")
			| keyword("regr_intercept") | keyword("regr_count") | keyword("regr_r")
			| keyword("regr_avgx") | keyword("regr_avgy")
			| keyword("regr_sxx") | keyword("regr_syy") | keyword("regr_sxy");

		const auto ordered_set_function_def = hypothetical_set_function | inverse_distribution_function;

		const auto hypothetical_set_function_def = rank_function_type >> lit('(') >> hypothetical_set_function_value_expression_list >> lit(')') >> within_group_specification;

		const auto within_group_specification_def = keyword("within") >> keyword("group") >>
			lit('(') >> keyword("order") >> keyword("by") >> sort_specification_list >> lit(')');

		const auto hypothetical_set_function_value_expression_list_def = value_expression % lit(',');

		const auto inverse_distribution_function_def = inverse_distribution_function_type >> lit('(') >> numeric_value_expression >> lit(')') >> within_group_specification;

		const auto inverse_distribution_function_type_def = keyword("percentile_cont") | keyword("percentile_disc");

		BOOST_SPIRIT_DEFINE(aggregate_function, general_set_function, computational_operation, set_quantifier,
				    filter_clause, binary_set_function, binary_set_function_type, ordered_set_function,
				    hypothetical_set_function, within_group_specification, hypothetical_set_function_value_expression_list,
				    inverse_distribution_function, inverse_distribution_function_type);

		//-----------------------------------------------------------------------------
		// 10.10 <sort specification list>
		//-----------------------------------------------------------------------------

		const rule<struct sort_specification> sort_specification = "sort_specification";
		const rule<struct ordering_specification> ordering_specification = "ordering_specification";
		const rule<struct null_ordering> null_ordering = "null_ordering";

		const auto sort_specification_list_def = sort_specification % lit(',');

		const auto sort_specification_def = value_expression >> -(ordering_specification) >> -(null_ordering);

		const auto ordering_specification_def = keyword("asc") | keyword("desc");

		const auto null_ordering_def = keyword("nulls") >> (keyword("first") | keyword("last"));

		BOOST_SPIRIT_DEFINE(sort_specification_list, sort_specification, ordering_specification, null_ordering);








// //hr
// //h2 11 Schema definition and manipulation
// ///h2
//
// //h3 11.1 <schema definition> (p517)
// ///h3
//
// //p
// Define a schema.
// ///p
//
// <schema definition> ::= keyword("create") keyword("schema") <schema name clause> [ <schema character set or path> ] [ <schema element>... ]
//
// <schema character set or path> ::=
// 		<schema character set specification>
// 	|	<schema path specification>
// 	|	<schema character set specification> <schema path specification>
// 	|	<schema path specification> <schema character set specification>
//
// <schema name clause> ::=
// 		schema_name
// 	|	keyword("authorization") <schema authorization identifier>
// 	|	schema_name keyword("authorization") <schema authorization identifier>
//
// <schema authorization identifier> ::= identifier
//
// <schema character set specification> ::= keyword("default") keyword("character") keyword("set") character_set_name
//
// <schema path specification> ::= <path specification>
//
// <schema element> ::=
// 		<table definition>
// 	|	<view definition>
// 	|	<domain definition>
// 	|	<character set definition>
// 	|	<collation definition>
// 	|	<transliteration definition>
// 	|	<assertion definition>
// 	|	<trigger definition>
// 	|	<user-defined type definition>
// 	|	<user-defined cast definition>
// 	|	<user-defined ordering definition>
// 	|	<transform definition>
// 	|	<schema routine>
// 	|	<sequence generator definition>
// 	|	<grant statement>
// 	|	<role definition>
//
// //h3 11.2 <drop schema statement> (p520)
// ///h3
//
// //p
// Destroy a schema.
// ///p
//
// <drop schema statement> ::= keyword("drop") keyword("schema") schema_name <drop behavior>
//
// <drop behavior> ::= keyword("cascade") | keyword("restrict")
//
// //h3 11.3 <table definition> (p523)
// ///h3
//
// //p
// Define a persistent base table, a created local temporary table, or a global temporary table.
// ///p
//
// <table definition> ::=
// 		keyword("create") [ <table scope> ] keyword("table") local_or_schema_qualified_name <table contents source>
// 		[ keyword("on") keyword("commit") <table commit action> keyword("rows") ]
//
// <table contents source> ::=
// 		<table element list>
// 	|	keyword("of") schema_qualified_type_name [ <subtable clause> ] [ <table element list> ]
// 	|	<as subquery clause>
//
// <table scope> ::= <global or local> keyword("temporary")
//
// <global or local> ::= keyword("global") | keyword("local")
//
// <table commit action> ::= keyword("preserve") | keyword("delete")
//
// <table element list> ::= lit('(') <table element> [ { lit(',') <table element> }... ] lit(')')
//
// <table element> ::=
// 		<column definition>
// 	|	<table constraint definition>
// 	|	<like clause>
// 	|	<self-referencing column specification>
// 	|	<column options>
//
// <self-referencing column specification> ::= keyword("ref") keyword("is") <self-referencing column name> <reference generation>
//
// <reference generation> ::= keyword("system") keyword("generated") | keyword("user") keyword("generated") | keyword("derived")
//
// <self-referencing column name> ::= identifier
//
// <column options> ::= identifier keyword("with") keyword("options") <column option list>
//
// <column option list> ::= [ scope_clause ] [ <default clause> ] [ <column constraint definition>... ]
//
// <subtable clause> ::= keyword("under") <supertable clause>
//
// <supertable clause> ::= <supertable name>
//
// <supertable name> ::= local_or_schema_qualified_name
//
// <like clause> ::= keyword("like") local_or_schema_qualified_name [ <like options> ]
//
// <like options> ::= <identity option> | <column default option>
//
// <identity option> ::= keyword("including") keyword("identity") | keyword("excluding") keyword("identity")
//
// <column default option> ::= keyword("including") keyword("defaults") | keyword("excluding") keyword("defaults")
//
// <as subquery clause> ::=  -(lit('(') >> column_name_list >> lit(')')) keyword("as") subquery <with or without data>
//
// <with or without data> ::= keyword("with") keyword("no") keyword("data") | keyword("with") keyword("data")
//

		//-----------------------------------------------------------------------------
		// 11.4 <column definition>
		//-----------------------------------------------------------------------------


// <column definition> ::=
// 		identifier [ data_type | schema_qualified_name ] [ reference_scope_check ]
// 		[ <default clause> | <identity column specification> | <generation clause> ]
// 		[ <column constraint definition>... ] [ collate_clause ]
//
// <column constraint definition> ::= [ <constraint name definition> ] <column constraint> [ <constraint characteristics> ]
//
// <column constraint> ::=
// 		keyword("not") keyword("null")
// 	|	<unique specification>
// 	|	<references specification>
// 	|	<check constraint definition>

		const auto reference_scope_check_def = keyword("references") >> keyword("are") >>
			-(keyword("not")) >> keyword("checked") >>
			-(keyword("on") >> keyword("delete") >> referential_action);

// <identity column specification> ::=
// 		keyword("generated") { keyword("always") | keyword("by") keyword("default") } keyword("as") keyword("identity")
// 		[ lit('(') <common sequence generator options> lit(')') ]
//
// <generation clause> ::= <generation rule> keyword("as") <generation expression>
//
// <generation rule> ::= keyword("generated") keyword("always")
//
// <generation expression> ::= lit('(') value_expression lit(')')
//

		BOOST_SPIRIT_DEFINE(reference_scope_check);

		//-----------------------------------------------------------------------------
		// 11.5 <default clause>
		//-----------------------------------------------------------------------------

// //h3 11.5 <default clause> (p539)
// ///h3
//
// //p
// Specify the default for a column, domain, or attribute.
// ///p
//
// <default clause> ::= keyword("default") <default option>
//
// <default option> ::=
// 		literal
// 	|	datetime_value_function
// 	|	keyword("user")
// 	|	keyword("current_user")
// 	|	keyword("current_role")
// 	|	keyword("session_user")
// 	|	keyword("system_user")
// 	|	keyword("current_path")
// 	|	implicitly_typed_value_specification
//

		//-----------------------------------------------------------------------------
		// 11.6 <table constraint definition>
		//-----------------------------------------------------------------------------


// <table constraint definition> ::= [ <constraint name definition> ] <table constraint> [ <constraint characteristics> ]
//
// <table constraint> ::=
// 		<unique constraint definition>
// 	|	<referential constraint definition>
// 	|	<check constraint definition>
//

		//-----------------------------------------------------------------------------
		// 11.7 <unique constraint definition>
		//-----------------------------------------------------------------------------

// <unique constraint definition> ::=
// 		<unique specification> lit('(') <unique column list> lit(')')
// 	|	keyword("unique") ( keyword("value") )
//
// <unique specification> ::= keyword("unique") | keyword("primary") keyword("key")
//
// <unique column list> ::= column_name_list
//

		//-----------------------------------------------------------------------------
		// 11.8 <referential constraint definition>
		//-----------------------------------------------------------------------------


// <referential constraint definition> ::= keyword("foreign") keyword("key") lit('(') <referencing columns> lit(')') <references specification>
//
// <references specification> ::= keyword("references") <referenced table and columns> [ keyword("match") <match type> ] [ <referential triggered action> ]
//
// <match type> ::= keyword("full") | keyword("partial") | keyword("simple")
//
// <referencing columns> ::= <reference column list>
//
// <referenced table and columns> ::= local_or_schema_qualified_name [ lit('(') <reference column list> lit(')') ]
//
// <reference column list> ::= column_name_list
//
// <referential triggered action> ::= <update rule> [ <delete rule> ] | <delete rule> [ <update rule> ]
//
// <update rule> ::= keyword("on") keyword("update") referential_action
//
// <delete rule> ::= keyword("on") keyword("delete") referential_action

		const auto referential_action_def = keyword("cascade")
			| (keyword("set") >> keyword("null"))
			| (keyword("set") >> keyword("default"))
			| keyword("restrict")
			| (keyword("no") >> keyword("action"));

		BOOST_SPIRIT_DEFINE(referential_action);

		//-----------------------------------------------------------------------------
		// 11.9 <check constraint definition>
		//-----------------------------------------------------------------------------

// <check constraint definition> ::= keyword("check") lit('(') boolean_value_expression lit(')')
//
// //h3 11.10 <alter table statement> (p569)
// ///h3
//
// //p
// Change the definition of a table.
// ///p
//
// <alter table statement> ::= keyword("alter") keyword("table") local_or_schema_qualified_name <alter table action>
//
// <alter table action> ::=
// 		<add column definition>
// 	|	<alter column definition>
// 	|	<drop column definition>
// 	|	<add table constraint definition>
// 	|	<drop table constraint definition>
//
// //h3 11.11 <add column definition> (p570)
// ///h3
//
// //p
// Add a column to a table.
// ///p
//
// <add column definition> ::= keyword("add") [ keyword("column") ] <column definition>
//
// //h3 11.12 <alter column definition> (p572)
// ///h3
//
// //p
// Change a column and its definition.
// ///p
//
// <alter column definition> ::= keyword("alter") [ keyword("column") ] identifier <alter column action>
//
// <alter column action> ::=
// 		<set column default clause>
// 	|	<drop column default clause>
// 	|	<add column scope clause>
// 	|	<drop column scope clause>
// 	|	<alter identity column specification>
//
// //h3 11.13 <set column default clause> (p573)
// ///h3
//
// //p
// Set the default clause for a column.
// ///p
//
// <set column default clause> ::= keyword("set") <default clause>
//
// //h3 11.14 <drop column default clause> (p574)
// ///h3
//
// //p
// Drop the default clause from a column.
// ///p
//
// <drop column default clause> ::= keyword("drop") keyword("default")
//
// //h3 11.15 <add column scope clause> (p575)
// ///h3
//
// //p
// Add a non-empty scope for an existing column of data type keyword("ref") in a base table.
// ///p
//
// <add column scope clause> ::= keyword("add") scope_clause
//
// //h3 11.16 <drop column scope clause> (p576)
// ///h3
//
// //p
// Drop the scope from an existing column of data type keyword("ref") in a base table.
// ///p
//
// <drop column scope clause> ::= keyword("drop") keyword("scope") <drop behavior>
//
// //h3 11.17 <alter identity column specification> (p578)
// ///h3
//
// //p
// Change the options specified for an identity column.
// ///p
//
// <alter identity column specification> ::= <alter identity column option>...
//
// <alter identity column option> ::=
// 		<alter sequence generator restart option>
// 	|	keyword("set") <basic sequence generator option>
//
// //h3 11.18 <drop column definition> (p579)
// ///h3
//
// //p
// Destroy a column of a base table.
// ///p
//
// <drop column definition> ::= keyword("drop") [ keyword("column") ] identifier <drop behavior>
//
// //h3 11.19 <add table constraint definition> (p581)
// ///h3
//
// //p
// Add a constraint to a table.
// ///p
//
// <add table constraint definition> ::= keyword("add") <table constraint definition>
//
// //h3 11.20 <drop table constraint definition> (p582)
// ///h3
//
// //p
// Destroy a constraint on a table.
// ///p
//
// <drop table constraint definition> ::= keyword("drop") keyword("constraint") schema_qualified_name <drop behavior>
//
// //h3 11.21 <drop table statement> (p585)
// ///h3
//
// //p
// Destroy a table.
// ///p
//
// <drop table statement> ::= keyword("drop") keyword("table") local_or_schema_qualified_name <drop behavior>
//
// //h3 11.22 <view definition> (p588)
// ///h3
//
// //p
// Define a viewed table.
// ///p
//
// <view definition> ::=
// 		keyword("create") [ keyword("recursive") ] keyword("view") local_or_schema_qualified_name <view specification> keyword("as") query_expression
// 		[ keyword("with") [ <levels clause> ] keyword("check") keyword("option") ]
//
// <view specification> ::= <regular view specification> | <referenceable view specification>
//
// <regular view specification> ::= [ lit('(') <view column list> lit(')') ]
//
// <referenceable view specification> ::= keyword("of") schema_qualified_type_name [ <subview clause> ] [ <view element list> ]
//
// <subview clause> ::= keyword("under") local_or_schema_qualified_name
//
// <view element list> ::= lit('(') <view element> [ { lit(',') <view element> }... ] lit(')')
//
// <view element> ::= <self-referencing column specification> | <view column option>
//
// <view column option> ::= identifier keyword("with") keyword("options") scope_clause
//
// <levels clause> ::= keyword("cascaded") | keyword("local")
//
// <view column list> ::= column_name_list
//
// //h3 11.23 <drop view statement> (p598)
// ///h3
//
// //p
// Destroy a view.
// ///p
//
// <drop view statement> ::= keyword("drop") keyword("view") local_or_schema_qualified_name <drop behavior>
//
// //h3 11.24 <domain definition> (p601)
// ///h3
//
// //p
// Define a domain.
// ///p
//
// <domain definition> ::=
// 		keyword("create") keyword("domain") schema_qualified_name >> -(keyword("as") >> data_type
// 		[ <default clause> ] [ <domain constraint>... ] [ collate_clause ]
//
// <domain constraint> ::= [ <constraint name definition> ] <check constraint definition> [ <constraint characteristics> ]
//
// //h3 11.25 <alter domain statement> (p603)
// ///h3
//
// //p
// Change a domain and its definition.
// ///p
//
// <alter domain statement> ::= keyword("alter") keyword("domain") schema_qualified_name <alter domain action>
//
// <alter domain action> ::=
// 		<set domain default clause>
// 	|	<drop domain default clause>
// 	|	<add domain constraint definition>
// 	|	<drop domain constraint definition>
//
// //h3 11.26 <set domain default clause> (p604)
// ///h3
//
// //p
// Set the default value in a domain.
// ///p
//
// <set domain default clause> ::= keyword("set") <default clause>
//
// //h3 11.27 <drop domain default clause> (p605)
// ///h3
//
// //p
// Remove the default clause of a domain.
// ///p
//
// <drop domain default clause> ::= keyword("drop") keyword("default")
//
// //h3 11.28 <add domain constraint definition> (p606)
// ///h3
//
// //p
// Add a constraint to a domain.
// ///p
//
// <add domain constraint definition> ::= keyword("add") <domain constraint>
//
// //h3 11.29 <drop domain constraint definition> (p607)
// ///h3
//
// //p
// Destroy a constraint on a domain.
// ///p
//
// <drop domain constraint definition> ::= keyword("drop") keyword("constraint") schema_qualified_name
//
// //h3 11.30 <drop domain statement> (p608)
// ///h3
//
// //p
// Destroy a domain.
// ///p
//
// <drop domain statement> ::= keyword("drop") keyword("domain") schema_qualified_name <drop behavior>
//
// //h3 11.31 <character set definition> (p610)
// ///h3
//
// //p
// Define a character set.
// ///p
//
// <character set definition> ::=
// 		keyword("create") keyword("character") keyword("set") character_set_name >> -(keyword("as") >> <character set source> [ collate_clause ]
//
// <character set source> ::= keyword("get") character_set_name
//
// //h3 11.32 <drop character set statement> (p612)
// ///h3
//
// //p
// Destroy a character set.
// ///p
//
// <drop character set statement> ::= keyword("drop") keyword("character") keyword("set") character_set_name
//
// //h3 11.33 <collation definition> (p614)
// ///h3
//
// //p
// Define a collating sequence.
// ///p
//
// <collation definition> ::=
// 		keyword("create") keyword("collation") schema_qualified_name keyword("for") character_set_name
// 		keyword("from") <existing collation name> [ <pad characteristic> ]
//
// <existing collation name> ::= schema_qualified_name
//
// <pad characteristic> ::= keyword("no") keyword("pad") | keyword("pad") keyword("space")
//
// //h3 11.34 <drop collation statement> (p616)
// ///h3
//
// //p
// Destroy a collating sequence.
// ///p
//
// <drop collation statement> ::= keyword("drop") keyword("collation") schema_qualified_name <drop behavior>
//
// //h3 11.35 <transliteration definition> (p618)
// ///h3
//
// //p
// Define a character transliteration.
// ///p
//
// <transliteration definition> ::=
// 		keyword("create") keyword("translation") schema_qualified_name keyword("for") <source character set specification>
// 		keyword("to") <target character set specification> keyword("from") <transliteration source>
//
// <source character set specification> ::= character_set_name
//
// <target character set specification> ::= character_set_name
//
// <transliteration source> ::= <existing transliteration name> | <transliteration routine>
//
// <existing transliteration name> ::= schema_qualified_name
//
// <transliteration routine> ::= <specific routine designator>
//
// //h3 11.36 <drop transliteration statement> (p621)
// ///h3
//
// //p
// Destroy a character transliteration.
// ///p
//
// <drop transliteration statement> ::= keyword("drop") keyword("translation") schema_qualified_name
//
// //h3 11.37 <assertion definition> (p623)
// ///h3
//
// //p
// Specify an integrity constraint.
// ///p
//
// <assertion definition> ::=
// 		keyword("create") keyword("assertion") schema_qualified_name keyword("check") lit('(') boolean_value_expression lit(')') [ <constraint characteristics> ]
//
// //h3 11.38 <drop assertion statement> (p625)
// ///h3
//
// //p
// Destroy an assertion.
// ///p
//
// <drop assertion statement> ::= keyword("drop") keyword("assertion") schema_qualified_name
//
// //h3 11.39 <trigger definition> (p627)
// ///h3
//
// //p
// Define triggered keyword("sql")-statements.
// ///p
//
// <trigger definition> ::=
// 		keyword("create") keyword("trigger") schema_qualified_name <trigger action time> <trigger event>
// 		keyword("on") local_or_schema_qualified_name [ keyword("referencing") <old or new values alias list> ]
// 		<triggered action>
//
// <trigger action time> ::= keyword("before") | keyword("after")
//
// <trigger event> ::= keyword("insert") | keyword("delete") | keyword("update") [ keyword("of") <trigger column list> ]
//
// <trigger column list> ::= column_name_list
//
// <triggered action> ::=
// 		[ keyword("for") keyword("each") { keyword("row") | keyword("statement") } ]
// 		[ keyword("when") lit('(') boolean_value_expression lit(')') ]
// 		<triggered keyword("sql") statement>
//
// <triggered SQL statement> ::=
// 		<SQL procedure statement>
// 	|	keyword("begin") keyword("atomic") { <SQL procedure statement> lit(';') }...  keyword("end")
//
// <old or new values alias list> ::= <old or new values alias>...
//
// <old or new values alias> ::=
// 		keyword("old") [ keyword("row") ] >> -(keyword("as") >> <old values correlation name>
// 	|	keyword("new") [ keyword("row") ] >> -(keyword("as") >> <new values correlation name>
// 	|	keyword("old") keyword("table") >> -(keyword("as") >> <old values table alias>
// 	|	keyword("new") keyword("table") >> -(keyword("as") >> <new values table alias>
//
// <old values table alias> ::= identifier
//
// <new values table alias> ::= identifier
//
// <old values correlation name> ::= identifier
//
// <new values correlation name> ::= identifier
//
// //h3 11.40 <drop trigger statement> (p631)
// ///h3
//
// //p
// Destroy a trigger.
// ///p
//
// <drop trigger statement> ::= keyword("drop") keyword("trigger") schema_qualified_name
//
// //h3 11.41 <user-defined type definition> (p632)
// ///h3
//
// //p
// Define a user-defined type.
// ///p
//
// <user-defined type definition> ::= keyword("create") keyword("type") <user-defined type body>
//
// <user-defined type body> ::=
// 		schema_qualified_type_name [ <subtype clause> ]
// 		[ keyword("as") <representation> ] [ <user-defined type option list> ] [ <method specification list> ]
//
// <user-defined type option list> ::= <user-defined type option> [ <user-defined type option>... ]
//
// <user-defined type option> ::=
// 		<instantiable clause>
// 	|	<finality>
// 	|	<reference type specification>
// 	|	<ref cast option>
// 	|	<cast option>
//
// <subtype clause> ::=
// 		keyword("under") <supertype name>
//
// <supertype name> ::=
// 		schema_qualified_type_name
//
// <representation> ::= predefined_type | <member list>
//
// <member list> ::= lit('(') <member> [ { lit(',') <member> }... ] lit(')')
//
// <member> ::= <attribute definition>
//
// <instantiable clause> ::= keyword("instantiable") | keyword("not") keyword("instantiable")
//
// <finality> ::= keyword("final") | keyword("not") keyword("final")
//
// <reference type specification> ::=
// 		<user-defined representation>
// 	|	<derived representation>
// 	|	<system-generated representation>
//
// <user-defined representation> ::= keyword("ref") keyword("using") predefined_type
//
// <derived representation> ::= keyword("ref") keyword("from") <list of attributes>
//
// <system-generated representation> ::= keyword("ref") keyword("is") keyword("system") keyword("generated")
//
// <ref cast option> ::= [ <cast to ref> ] [ <cast to type> ]
//
// <cast to ref> ::= keyword("cast") lit('(') keyword("source") keyword("as") keyword("ref") lit(')') keyword("with") <cast to ref identifier>
//
// <cast to ref identifier> ::= identifier
//
// <cast to type> ::= keyword("cast") lit('(') keyword("ref") keyword("as") keyword("source") lit(')') keyword("with") <cast to type identifier>
//
// <cast to type identifier> ::= identifier
//
// <list of attributes> ::= lit('(') identifier [ { lit(',') identifier }...] lit(')')
//
// <cast option> ::= [ <cast to distinct> ] [ <cast to source> ]
//
// <cast to distinct> ::=
// 		keyword("cast") lit('(') keyword("source") keyword("as") keyword("distinct") lit(')')
// 		keyword("with") <cast to distinct identifier>
//
// <cast to distinct identifier> ::= identifier
//
// <cast to source> ::=
// 		keyword("cast") lit('(') keyword("distinct") keyword("as") keyword("source") lit(')')
// 		keyword("with") <cast to source identifier>
//
// <cast to source identifier> ::= identifier
//
// <method specification list> ::= <method specification> [ { lit(',') <method specification> }... ]
//
// <method specification> ::= <original method specification> | <overriding method specification>
//
// <original method specification> ::=
// 		<partial method specification> [ keyword("self") keyword("as") keyword("result") ] [ keyword("self") keyword("as") keyword("locator") ] [ <method characteristics> ]
//
// <overriding method specification> ::= keyword("overriding") <partial method specification>
//
// <partial method specification> ::=
// 		[ keyword("instance") | keyword("static") | keyword("constructor") ] keyword("method") identifier <keyword("sql") parameter declaration list>
// 		<returns clause> [ keyword("specific") <specific method name> ]
//
// <specific method name> ::= [ schema_name lit('.') ]identifier
//
// <method characteristics> ::= <method characteristic>...
//
// <method characteristic> ::=
// 		<language clause>
// 	|	<parameter style clause>
// 	|	<deterministic characteristic>
// 	|	<keyword("sql")-data access indication>
// 	|	<null-call clause>
//
// //h3 11.42 <attribute definition> (p648)
// ///h3
//
// //p
// Define an attribute of a structured type.
// ///p
//
// <attribute definition> ::=
// 		identifier data_type [ reference_scope_check ] [ <attribute default> ] [ collate_clause ]
//
// <attribute default> ::= <default clause>
//
// //h3 11.43 <alter type statement> (p650)
// ///h3
//
// //p
// Change the definition of a user-defined type.
// ///p
//
// <alter type statement> ::=
// 	keyword("alter") keyword("type") schema_qualified_type_name <alter type action>
//
// <alter type action> ::=
// 		<add attribute definition>
// 	|	<drop attribute definition>
// 	|	<add original method specification>
// 	|	<add overriding method specification>
// 	|	<drop method specification>
//
// //h3 11.44 <add attribute definition> (p651)
// ///h3
//
// //p
// Add an attribute to a user-defined type.
// ///p
//
// <add attribute definition> ::= keyword("add") keyword("attribute") <attribute definition>
//
// //h3 11.45 <drop attribute definition> (p653)
// ///h3
//
// //p
// Destroy an attribute of a user-defined type.
// ///p
//
// <drop attribute definition> ::= keyword("drop") keyword("attribute") identifier keyword("restrict")
//
// //h3 11.46 <add original method specification> (p655)
// ///h3
//
// //p
// Add an original method specification to a user-defined type.
// ///p
//
// <add original method specification> ::= keyword("add") <original method specification>
//
// //h3 11.47 <add overriding method specification> (p661)
// ///h3
//
// //p
// Add an overriding method specification to a user-defined type.
// ///p
//
// <add overriding method specification> ::= keyword("add") <overriding method specification>
//
// //h3 11.48 <drop method specification> (p666)
// ///h3
//
// //p
// Remove a method specification from a user-defined type.
// ///p
//
// <drop method specification> ::= keyword("drop") <specific method specification designator> keyword("restrict")
//
// <specific method specification designator> ::= [ keyword("instance") | keyword("static") | keyword("constructor") ] keyword("method") identifier <data type list>
//
// //h3 11.49 <drop data type statement> (p670)
// ///h3
//
// //p
// Destroy a user-defined type.
// ///p
//
// <drop data type statement> ::= keyword("drop") keyword("type") schema_qualified_type_name <drop behavior>
//
// //h3 11.50 <keyword("sql")-invoked routine> (p673)
// ///h3
//
// //p
// Define an keyword("sql")-invoked routine.
// ///p
//
// <SQL-invoked routine> ::= <schema routine>
//
// <schema routine> ::= <schema procedure> | <schema function>
//
// <schema procedure> ::= keyword("create") <SQL-invoked procedure>
//
// <schema function> ::= keyword("create") <SQL-invoked function>
//
// <SQL-invoked procedure> ::=
// 		keyword("procedure") schema_qualified_name <SQL parameter declaration list> <routine characteristics> <routine body>
//
// <SQL-invoked function> ::=
// 		{ <function specification> | <method specification designator> } <routine body>
//
// <SQL parameter declaration list> ::=
// 		lit('(') [ <SQL parameter declaration> [ { lit(',') <SQL parameter declaration> }... ] ] lit(')')
//
// <SQL parameter declaration> ::= [ <parameter mode> ] [ identifier ] <parameter type> [ keyword("result") ]
//
// <parameter mode> ::= keyword("in") | keyword("out") | keyword("inout")
//
// <parameter type> ::= data_type [ <locator indication> ]
//
// <locator indication> ::= keyword("as") keyword("locator")
//
// <function specification> ::=
// 		keyword("function") schema_qualified_name
// 		<SQL parameter declaration list> <returns clause> <routine characteristics> [ <dispatch clause> ]
//
// <method specification designator> ::=
// 		keyword("specific") keyword("method") <specific method name>
// 	|	[ keyword("instance") | keyword("static") | keyword("constructor") ] keyword("method") identifier <SQL parameter declaration list>
// 		[ <returns clause> ] keyword("for") schema_qualified_type_name
//
// <routine characteristics> ::= [ <routine characteristic>... ]
//
// <routine characteristic> ::=
// 		<language clause>
// 	|	<parameter style clause>
// 	|	keyword("specific") schema_qualified_name
// 	|	<deterministic characteristic>
// 	|	<SQL-data access indication>
// 	|	<null-call clause>
// 	|	<dynamic result sets characteristic>
// 	|	<savepoint level indication>
//
// <savepoint level indication> ::= keyword("new") keyword("savepoint") keyword("level") | keyword("old") keyword("savepoint") keyword("level")
//
// <dynamic result sets characteristic> ::= keyword("dynamic") keyword("result") keyword("sets") <maximum dynamic result sets>
//
// <parameter style clause> ::= keyword("parameter") keyword("style") <parameter style>
//
// <dispatch clause> ::= keyword("static") keyword("dispatch")
//
// <returns clause> ::= keyword("returns") <returns type>
//
// <returns type> ::=
// 		<returns data type> [ <result cast> ]
// 	|	<returns table type>
//
// <returns table type> ::= keyword("table") <table function column list>
//
// <table function column list> ::=
// 		lit('(')
// 		<table function column list element> [ { lit(',') <table function column list element> }... ]
// 		lit(')')
//
// <table function column list element> ::= identifier data_type
//
// <result cast> ::= keyword("cast") keyword("from") <result cast from type>
//
// <result cast from type> ::= data_type [ <locator indication> ]
//
// <returns data type> ::= data_type [ <locator indication> ]
//
// <routine body> ::=
// 		<SQL routine spec>
// 	|	<external body reference>
//
// <SQL routine spec> ::= [ <rights clause> ] <SQL routine body>
//
// <rights clause> ::= keyword("sql") keyword("security") keyword("invoker") | keyword("sql") keyword("security") keyword("definer")
//
// <SQL routine body> ::= <SQL procedure statement>
//
// <external body reference> ::=
// 		keyword("external") [ keyword("name") external_routine_name ] [ <parameter style clause> ]
// 		[ <transform group specification> ] [ <external security clause> ]
//
// <external security clause> ::=
// 		keyword("external") keyword("security") keyword("definer")
// 	|	keyword("external") keyword("security") keyword("invoker")
// 	|	keyword("external") keyword("security") keyword("implementation") keyword("defined")
//
// <parameter style> ::= keyword("sql") | keyword("general")
//
// <deterministic characteristic> ::= keyword("deterministic") | keyword("not") keyword("deterministic")
//
// <keyword("sql")-data access indication> ::=
// 		keyword("no") keyword("sql")
// 	|	keyword("contains") keyword("sql")
// 	|	keyword("reads") keyword("sql") keyword("data")
// 	|	keyword("modifies") keyword("sql") keyword("data")
//
// <null-call clause> ::=
// 		keyword("returns") keyword("null") keyword("on") keyword("null") keyword("input")
// 	|	keyword("called") keyword("on") keyword("null") keyword("input")
//
// <maximum dynamic result sets> ::= unsigned_integer
//
// <transform group specification> ::= keyword("transform") keyword("group") { <single group specification> | <multiple group specification> }
//
// <single group specification> ::= <group name>
//
// <multiple group specification> ::= <group specification> [ { lit(',') <group specification> }... ]
//
// <group specification> ::= <group name> keyword("for") keyword("type") schema_qualified_type_name
//
// //h3 11.51 <alter routine statement> (p698)
// ///h3
//
// //p
// Alter a characteristic of an SQL-invoked routine.
// ///p
//
// <alter routine statement> ::= keyword("alter") <specific routine designator> <alter routine characteristics> <alter routine behavior>
//
// <alter routine characteristics> ::= <alter routine characteristic>...
//
// <alter routine characteristic> ::=
// 		<language clause>
// 	|	<parameter style clause>
// 	|	<SQL-data access indication>
// 	|	<null-call clause>
// 	|	<dynamic result sets characteristic>
// 	|	keyword("name") external_routine_name
//
// <alter routine behavior> ::= keyword("restrict")
//
// //h3 11.52 <drop routine statement> (p701)
// ///h3
//
// //p
// Destroy an SQL-invoked routine.
// ///p
//
// <drop routine statement> ::= keyword("drop") <specific routine designator> <drop behavior>
//
// //h3 11.53 <user-defined cast definition> (p703)
// ///h3
//
// //p
// Define a user-defined cast.
// ///p
//
// <user-defined cast definition> ::=
// 		keyword("create") keyword("cast") lit('(') <source data type> keyword("as") <target data type> lit(')')
// 		keyword("with") <cast function> [ keyword("as") keyword("assignment") ]
//
// <cast function> ::= <specific routine designator>
//
// <source data type> ::= data_type
//
// <target data type> ::= data_type
//
// //h3 11.54 <drop user-defined cast statement> (p705)
// ///h3
//
// //p
// Destroy a user-defined cast.
// ///p
//
// <drop user-defined cast statement> ::=
// 		keyword("drop") keyword("cast") lit('(') <source data type> keyword("as") <target data type> lit(')') <drop behavior>
//
// //h3 11.55 <user-defined ordering definition> (p707)
// ///h3
//
// //p
// Define a user-defined ordering for a user-defined type.
// ///p
//
// <user-defined ordering definition> ::=
// 		keyword("create") keyword("ordering") keyword("for") schema_qualified_type_name <ordering form>
//
// <ordering form> ::= <equals ordering form> | <full ordering form>
//
// <equals ordering form> ::= keyword("equals") keyword("only") keyword("by") <ordering category>
//
// <full ordering form> ::= keyword("order") keyword("full") keyword("by") <ordering category>
//
// <ordering category> ::= <relative category> | <map category> | <state category>
//
// <relative category> ::= keyword("relative") keyword("with") <relative function specification>
//
// <map category> ::= keyword("map") keyword("with") <map function specification>
//
// <state category> ::= keyword("state") [ schema_qualified_name ]
//
// <relative function specification> ::= <specific routine designator>
//
// <map function specification> ::= <specific routine designator>
//
// //h3 11.56 <drop user-defined ordering statement> (p710)
// ///h3
//
// //p
// Destroy a user-defined ordering method.
// ///p
//
// <drop user-defined ordering statement> ::=
// 		keyword("drop") keyword("ordering") keyword("for") schema_qualified_type_name <drop behavior>
//
// //h3 11.57 <transform definition> (p712)
// ///h3
//
// //p
// Define one or more transform functions for a user-defined type.
// ///p
//
// <transform definition> ::= keyword("create") { keyword("transform") | keyword("transforms") } keyword("for") schema_qualified_type_name <transform group>...
//
// <transform group> ::= <group name> lit('(') <transform element list> lit(')')
//
// <group name> ::= identifier
//
// <transform element list> ::= <transform element> [ lit(',') <transform element> ]
//
// <transform element> ::= <to sql> | <from sql>
//
// <to sql> ::= keyword("to") keyword("sql") keyword("with") <to sql function>
//
// <from sql> ::= keyword("from") keyword("sql") keyword("with") <from sql function>
//
// <to sql function> ::= <specific routine designator>
//
// <from sql function> ::= <specific routine designator>
//
// //h3 11.58 <alter transform statement> (p715)
// ///h3
//
// //p
// Change the definition of one or more transform groups.
// ///p
//
// <alter transform statement> ::=
// 		keyword("alter") { keyword("transform") | keyword("transforms") } keyword("for") schema_qualified_type_name <alter group>...
//
// <alter group> ::= <group name> lit('(') <alter transform action list> lit(')')
//
// <alter transform action list> ::= <alter transform action> [ { lit(',') <alter transform action> }... ]
//
// <alter transform action> ::= <add transform element list> | <drop transform element list>
//
// //h3 11.59 <add transform element list> (p717)
// ///h3
//
// //p
// Add a transform element (<to sql> and/or <from sql>) to an existing transform group.
// ///p
//
// <add transform element list> ::= keyword("add") lit('(') <transform element list> lit(')')
//
// //h3 11.60 <drop transform element list> (p719)
// ///h3
//
// //p
// Remove a transform element (<to sql> and/or <from sql>) from a transform group.
// ///p
//
// <drop transform element list> ::= keyword("drop") lit('(') <transform kind> [ lit(',') <transform kind> ] <drop behavior> lit(')')
//
// <transform kind> ::= keyword("to") keyword("sql") | keyword("from") keyword("sql")
//
// //h3 11.61 <drop transform statement> (p721)
// ///h3
//
// //p
// Remove one or more transform functions associated with a transform.
// ///p
//
// <drop transform statement> ::=
// 		keyword("drop") { keyword("transform") | keyword("transforms") } <transforms to be dropped> keyword("for") schema_qualified_type_name <drop behavior>
//
// <transforms to be dropped> ::= keyword("all") | <transform group element>
//
// <transform group element> ::= <group name>
//
// //h3 11.62 <sequence generator definition> (p724)
// ///h3
//
// //p
// Define an external sequence generator.
// ///p
//
// <sequence generator definition> ::= keyword("create") keyword("sequence") schema_qualified_name [ <sequence generator options> ]
//
// <sequence generator options> ::= <sequence generator option> ...
//
// <sequence generator option> ::= <sequence generator data type option> | <common sequence generator options>
//
// <common sequence generator options> ::= <common sequence generator option> ...
//
// <common sequence generator option> ::= <sequence generator start with option> | <basic sequence generator option>
//
// <basic sequence generator option> ::=
// 		<sequence generator increment by option>
// 	|	<sequence generator maxvalue option>
// 	|	<sequence generator minvalue option>
// 	|	<sequence generator cycle option>
//
// <sequence generator data type option> ::= keyword("as") data_type
//
// <sequence generator start with option> ::= keyword("start") keyword("with") <sequence generator start value>
//
// <sequence generator start value> ::= signed_numeric_literal
//
// <sequence generator increment by option> ::= keyword("increment") keyword("by") <sequence generator increment>
//
// <sequence generator increment> ::= signed_numeric_literal
//
// <sequence generator maxvalue option> ::=
// 		keyword("maxvalue") <sequence generator max value>
// 	|	keyword("no") keyword("maxvalue")
//
// <sequence generator max value> ::= signed_numeric_literal
//
// <sequence generator minvalue option> ::= keyword("minvalue") <sequence generator min value> | keyword("no") keyword("minvalue")
//
// <sequence generator min value> ::= signed_numeric_literal
//
// <sequence generator cycle option> ::= keyword("cycle") | keyword("no") keyword("cycle")
//
// //h3 11.63 <alter sequence generator statement> (p726)
// ///h3
//
// //p
// Change the definition of an external sequence generator.
// ///p
//
// <alter sequence generator statement> ::=
// 		keyword("alter") keyword("sequence") schema_qualified_name <alter sequence generator options>
//
// <alter sequence generator options> ::= <alter sequence generator option>...
//
// <alter sequence generator option> ::=
// 		<alter sequence generator restart option>
// 	|	<basic sequence generator option>
//
// <alter sequence generator restart option> ::= keyword("restart") keyword("with") <sequence generator restart value>
//
// <sequence generator restart value> ::= signed_numeric_literal
//
// //h3 11.64 <drop sequence generator statement> (p727)
// ///h3
//
// //p
// Destroy an external sequence generator.
// ///p
//
// <drop sequence generator statement> ::= keyword("drop") keyword("sequence") schema_qualified_name <drop behavior>
//
// //hr
// //h2 12 Access control
// ///h2
//
// //h3 12.1 <grant statement> (p729)
// ///h3
//
// //p
// Define privileges and role authorizations.
// ///p
//
// <grant statement> ::= <grant privilege statement> | <grant role statement>
//
// //h3 12.2 <grant privilege statement> (p734)
// ///h3
//
// //p
// Define privileges.
// ///p
//
// <grant privilege statement> ::=
// 		keyword("grant") <privileges> keyword("to") <grantee> [ { lit(',') <grantee> }... ]
// 		[ keyword("with") keyword("hierarchy") keyword("option") ] [ keyword("with") keyword("grant") keyword("option") ] [ keyword("granted") keyword("by") <grantor> ]
//
// //h3 12.3 <privileges> (p737)
// ///h3
//
// //p
// Specify privileges.
// ///p
//
// <privileges> ::= <object privileges> keyword("on") <object name>
//
// <object name> ::=
// 		[ keyword("table") ] local_or_schema_qualified_name
// 	|	keyword("domain") schema_qualified_name
// 	|	keyword("collation") schema_qualified_name
// 	|	keyword("character") keyword("set") character_set_name
// 	|	keyword("translation") schema_qualified_name
// 	|	keyword("type") schema_qualified_type_name
// 	|	keyword("sequence") schema_qualified_name
// 	|	<specific routine designator>
//
// <object privileges> ::=
// 		keyword("all") keyword("privileges")
// 	|	<action> [ { lit(',') <action> }... ]
//
// <action> ::=
// 		keyword("select")
// 	|	keyword("select") lit('(') <privilege column list> lit(')')
// 	|	keyword("select") lit('(') <privilege method list> lit(')')
// 	|	keyword("delete")
// 	|	keyword("insert") [ lit('(') <privilege column list> lit(')') ]
// 	|	keyword("update") [ lit('(') <privilege column list> lit(')') ]
// 	|	keyword("references") [ lit('(') <privilege column list> lit(')') ]
// 	|	keyword("usage")
// 	|	keyword("trigger")
// 	|	keyword("under")
// 	|	keyword("execute")
//
// <privilege method list> ::= <specific routine designator> [ { lit(',') <specific routine designator> }... ]
//
// <privilege column list> ::= column_name_list
//
// <grantee> ::= keyword("public") | identifier
//
// <grantor> ::= keyword("current_user") | keyword("current_role")
//
// //h3 12.4 <role definition> (p741)
// ///h3
//
// //p
// Define a role.
// ///p
//
// <role definition> ::= keyword("create") keyword("role") identifier [ keyword("with") keyword("admin") <grantor> ]
//
// //h3 12.5 <grant role statement> (p742)
// ///h3
//
// //p
// Define role authorizations.
// ///p
//
// <grant role statement> ::=
// 		keyword("grant") <role granted> [ { lit(',') <role granted> }... ]
// 		keyword("to") <grantee> [ { lit(',') <grantee> }... ] [ keyword("with") keyword("admin") keyword("option") ] [ keyword("granted") keyword("by") <grantor> ]
//
// <role granted> ::= identifier
//
// //h3 12.6 <drop role statement> (p744)
// ///h3
//
// //p
// Destroy a role.
// ///p
//
// <drop role statement> ::= keyword("drop") keyword("role") identifier
//
// //h3 12.7 <revoke statement> (p745)
// ///h3
//
// //p
// Destroy privileges and role authorizations.
// ///p
//
// <revoke statement> ::=
// 		<revoke privilege statement>
// 	|	<revoke role statement>
//
// <revoke privilege statement> ::=
// 		keyword("revoke") [ <revoke option extension> ] <privileges> keyword("from") <grantee> [ { lit(',') <grantee> }... ]
// 		[ keyword("granted") keyword("by") <grantor> ] <drop behavior>
//
// <revoke option extension> ::= keyword("grant") keyword("option") keyword("for") | keyword("hierarchy") keyword("option") keyword("for")
//
// <revoke role statement> ::=
// 		keyword("revoke") [ keyword("admin") keyword("option") keyword("for") ] <role revoked> [ { lit(',') <role revoked> }... ]
// 		keyword("from") <grantee> [ { lit(',') <grantee> }... ] [ keyword("granted") keyword("by") <grantor> ] <drop behavior>
//
// <role revoked> ::= identifier
//
// //hr
// //h2 13 keyword("sql")-client modules
// ///h2
//
// //h3 13.1 <keyword("sql")-client module definition> (p763)
// ///h3
//
// //p
// Define an keyword("sql")-client module.
// ///p
//
// <SQL-client module definition> ::=
// 		<module name clause> <language clause> <module authorization clause>
// 		[ <module path specification> ]
// 		[ <module transform group specification> ]
// 		[ <module collation> ]
// 		[ <temporary table declaration>... ]
// 		<module contents>...
//
// <module authorization clause> ::=
// 		keyword("schema") schema_name
// 	|	keyword("authorization") <module authorization identifier> [ keyword("for") keyword("static") { keyword("only") | keyword("and") keyword("dynamic") } ]
// 	|	keyword("schema") schema_name keyword("authorization") <module authorization identifier> [ keyword("for") keyword("static") { keyword("only") | keyword("and") keyword("dynamic") } ]
//
// <module authorization identifier> ::= identifier
//
// <module path specification> ::= <path specification>
//
// <module transform group specification> ::= <transform group specification>
//
// <module collation> ::= <module collation specification>...
//
// <module collation specification> ::= keyword("collation") schema_qualified_name [ keyword("for") <character set specification list> ]
//
// //p
// //i
// //small
// There was another definition <character set specification list> in section 18.3.
// That was slightly different in format (simpler) but functionally equivalent.
// It is not clear why it was repeated.
// The alternative definition is now commented out.
// ///small
// ///i
// ///p
//
// <character set specification list> ::= character_set_name [ { lit(',') character_set_name }... ]
//
// <module contents> ::=
// 		<declare cursor>
// 	|	<dynamic declare cursor>
// 	|	<externally-invoked procedure>
//
// //h3 13.2 <module name clause> (p768)
// ///h3
//
// //p
// Name an SQL-client module.
// ///p
//
// <module name clause> ::=
// 		keyword("module") [ identifier ] [ <module character set specification> ]
//
// <module character set specification> ::= keyword("names") keyword("are") character_set_name
//
// //h3 13.3 <externally-invoked procedure> (p769)
// ///h3
//
// //p
// Define an externally-invoked procedure.
// ///p
//
// <externally-invoked procedure> ::=
// 		keyword("procedure") identifier <host parameter declaration list> lit(';')
// 		<SQL procedure statement> lit(';')
//
// <host parameter declaration list> ::=
// 		lit('(') <host parameter declaration> [ { lit(',') <host parameter declaration> }... ] lit(')')
//
// <host parameter declaration> ::=
// 		host_parameter_name <host parameter data type>
// 	|	<status parameter>
//
// <host parameter data type> ::= data_type [ <locator indication> ]
//
// <status parameter> ::= keyword("sqlstate")
//
// //h3 13.4 Calls to an <externally-invoked procedure> (p772)
// ///h3
//
// //h3 13.5 <SQL procedure statement> (p788)
// ///h3
//
// //p
// Define all of the SQL-statements that are <SQL procedure statement>s.
// ///p
//
// <SQL procedure statement> ::= <SQL executable statement>
//
// <SQL executable statement> ::=
// 		<SQL schema statement>
// 	|	<SQL data statement>
// 	|	<SQL control statement>
// 	|	<SQL transaction statement>
// 	|	<SQL connection statement>
// 	|	<SQL session statement>
// 	|	<SQL diagnostics statement>
// 	|	<SQL dynamic statement>
//
// <SQL schema statement> ::=
// 		<SQL schema definition statement>
// 	|	<SQL schema manipulation statement>
//
// <SQL schema definition statement> ::=
// 		<schema definition>
// 	|	<table definition>
// 	|	<view definition>
// 	|	<SQL-invoked routine>
// 	|	<grant statement>
// 	|	<role definition>
// 	|	<domain definition>
// 	|	<character set definition>
// 	|	<collation definition>
// 	|	<transliteration definition>
// 	|	<assertion definition>
// 	|	<trigger definition>
// 	|	<user-defined type definition>
// 	|	<user-defined cast definition>
// 	|	<user-defined ordering definition>
// 	|	<transform definition>
// 	|	<sequence generator definition>
//
// <SQL schema manipulation statement> ::=
// 		<drop schema statement>
// 	|	<alter table statement>
// 	|	<drop table statement>
// 	|	<drop view statement>
// 	|	<alter routine statement>
// 	|	<drop routine statement>
// 	|	<drop user-defined cast statement>
// 	|	<revoke statement>
// 	|	<drop role statement>
// 	|	<alter domain statement>
// 	|	<drop domain statement>
// 	|	<drop character set statement>
// 	|	<drop collation statement>
// 	|	<drop transliteration statement>
// 	|	<drop assertion statement>
// 	|	<drop trigger statement>
// 	|	<alter type statement>
// 	|	<drop data type statement>
// 	|	<drop user-defined ordering statement>
// 	|	<alter transform statement>
// 	|	<drop transform statement> | <alter sequence generator statement>
// 	|	<drop sequence generator statement>
//
// <SQL data statement> ::=
// 		<open statement>
// 	|	<fetch statement>
// 	|	<close statement>
// 	|	<select statement: single row>
// 	|	<free locator statement>
// 	|	<hold locator statement>
// 	|	<SQL data change statement>
//
// <SQL data change statement> ::=
// 		<delete statement: positioned>
// 	|	<delete statement: searched>
// 	|	<insert statement>
// 	|	<update statement: positioned>
// 	|	<update statement: searched>
// 	|	<merge statement>
//
// <SQL control statement> ::=
// 		<call statement>
// 	|	<return statement>
//
// <SQL transaction statement> ::=
// 		<start transaction statement>
// 	|	<set transaction statement>
// 	|	<set constraints mode statement>
// 	|	<savepoint statement>
// 	|	<release savepoint statement>
// 	|	<commit statement>
// 	|	<rollback statement>
//
// <SQL connection statement> ::=
// 		<connect statement>
// 	|	<set connection statement>
// 	|	<disconnect statement>
//
// <SQL session statement> ::=
// 		<set session user identifier statement>
// 	|	<set role statement>
// 	|	<set local time zone statement>
// 	|	<set session characteristics statement>
// 	|	<set catalog statement>
// 	|	<set schema statement>
// 	|	<set names statement>
// 	|	<set path statement>
// 	|	<set transform group statement>
// 	|	<set session collation statement>
//
// <SQL diagnostics statement> ::= <get diagnostics statement>
//
// <SQL dynamic statement> ::=
// 		<system descriptor statement>
// 	|	<prepare statement>
// 	|	<deallocate prepared statement>
// 	|	<describe statement>
// 	|	<execute statement>
// 	|	<execute immediate statement>
// 	|	<SQL dynamic data statement>
//
// <SQL dynamic data statement> ::=
// 		<allocate cursor statement>
// 	|	<dynamic open statement>
// 	|	<dynamic fetch statement>
// 	|	<dynamic close statement>
// 	|	<dynamic delete statement: positioned>
// 	|	<dynamic update statement: positioned>
//
// <system descriptor statement> ::=
// 		<allocate descriptor statement>
// 	|	<deallocate descriptor statement>
// 	|	<set descriptor statement>
// 	|	<get descriptor statement>
//
// //h3 13.6 Data type correspondences (p796)
// ///h3
//
// //p
// Table 16 // Data type correspondences for C
// ///p
//
// //## <table border=1>
// //## <tr><th> SQL Data Type </th><th> C Data Type </th></tr>
//
// //## <tr><td> SQLSTATE </td><td> char, with length 6 </td></tr>
// //## <tr><td> CHARACTER (L)<sup>3</sup> </td><td> char, with length (L+1)*k<sup>1</sup> </td></tr>
// //## <tr><td> CHARACTER VARYING (L)<sup>3</sup> </td><td> char, with length (L+1)*k<sup>1</sup> </td></tr>
// //## <tr><td> CHARACTER LARGE OBJECT(L) </td><td>
// //## <pre>
// //## struct {
// //## long hvn<sup>3</sup>_reserved
// //## unsigned long hvn<sup>2</sup>_length
// //## char<sup>3</sup> hvn<sup>2</sup>_data[L];
// //## } hvn<sup>2</sup>
// //## </pre> </td></tr>
// //## <tr><td> BINARY LARGE OBJECT(L) </td><td>
// //## <pre> struct {
// //## long hvn<sup>2</sup>_reserved
// //## unsigned long hvn<sup>2</sup>_length
// //## char hvn<sup>2</sup>_data[L];
// //## } hvn<sup>2</sup>
// //## </pre> </td></tr>
// //## <tr><td> NUMERIC(P,S) </td><td> None </td></tr>
// //## <tr><td> DECIMAL(P,S) </td><td> None </td></tr>
// //## <tr><td> SMALLINT </td><td> pointer to short </td></tr>
// //## <tr><td> INTEGER </td><td> pointer to long </td></tr>
// //## <tr><td> BIGINT </td><td> pointer to long long </td></tr>
// //## <tr><td> FLOAT(P) </td><td> None </td></tr>
// //## <tr><td> REAL </td><td> pointer to float </td></tr>
// //## <tr><td> DOUBLE PRECISION </td><td> pointer to double </td></tr>
// //## <tr><td> BOOLEAN </td><td> pointer to long </td></tr>
// //## <tr><td> DATE </td><td> None </td></tr>
// //## <tr><td> TIME(T) </td><td> None </td></tr>
// //## <tr><td> TIMESTAMP(T) </td><td> None </td></tr>
// //## <tr><td> INTERVAL(Q) </td><td> None </td></tr>
// //## <tr><td> user-defined type </td><td> None </td></tr>
// //## <tr><td> REF </td><td> char, with length N </td></tr>
// //## <tr><td> ROW </td><td> None </td></tr>
// //## <tr><td> ARRAY </td><td> None </td></tr>
// //## <tr><td> MULTISET </td><td> None </td></tr>
// //## </table>
//
// //p
// //## <sup>1</sup> For character set UTF16, as well as other
// implementation-defined character sets in which a code unit occupies two
// octets, k is the length in units of C unsigned short of the character
// encoded using the greatest number of such units in the character set;
// for character set UTF32, as well as other implementation-defined
// character sets in which a code unit occupies four octets, k is four; for
// other character sets, k is the length in units of C char of the
// character encoded using the greatest number of such units in the
// character set.
// //br
// //## <sup>2</sup> hvn is the name of the host variable defined to correspond
// to the SQL data type
// //br
// //## <sup>3</sup> For character set UTF16, as well as other
// implementation-defined character sets in which a code unit occupies two
// octets, char or unsigned char should be replaced with unsigned short;
// for character set UTF32, as well as other implementation-defined
// character sets in which a code unit occupies four octets, char or
// unsigned char should be replaced with unsigned int.  Otherwise, char or
// unsigned char should be used.


		//-----------------------------------------------------------------------------
		// 14 Data manipulation
		//-----------------------------------------------------------------------------

		//-----------------------------------------------------------------------------
		// 14.1 <declare cursor>
		//-----------------------------------------------------------------------------

		const rule<struct cursor_specification> cursor_specification = "cursor_specification";
		const rule<struct updatability_clause> updatability_clause = "updatability_clause";

// <declare cursor> ::=
// 		keyword("declare") local_qualified_name [ <cursor sensitivity> ] [ <cursor scrollability> ] keyword("cursor")
// 		[ <cursor holdability> ] [ <cursor returnability> ] keyword("for") cursor_specification
//
// <cursor sensitivity> ::= keyword("sensitive") | keyword("insensitive") | keyword("asensitive")
//
// <cursor scrollability> ::= keyword("scroll") | keyword("no") keyword("scroll")
//
// <cursor holdability> ::= keyword("with") keyword("hold") | keyword("without") keyword("hold")
//
// <cursor returnability> ::= keyword("with") keyword("return") | keyword("without") keyword("return")

		const auto cursor_specification_def = query_expression >> -(order_by_clause) >> -(updatability_clause);

		const auto updatability_clause_def = keyword("for") >>
			(keyword("read") >> keyword("only"))
			| (keyword("update") >> -(keyword("of") >> column_name_list));

		const auto order_by_clause_def = keyword("order") >> keyword("by") >> sort_specification_list;

		BOOST_SPIRIT_DEFINE(cursor_specification, updatability_clause, order_by_clause);

		//-----------------------------------------------------------------------------
		// 14.2 <open statement>
		//-----------------------------------------------------------------------------

// <open statement> ::= keyword("open") local_qualified_name
//
// //h3 14.3 <fetch statement> (p815)
// ///h3
//
// //p
// Position a cursor on a specified row of a table and retrieve values from that row.
// ///p
//
// <fetch statement> ::=
// 		keyword("fetch") [ [ <fetch orientation> ] keyword("from") ] local_qualified_name keyword("into") <fetch target list>
//
// <fetch orientation> ::=
// 		keyword("next")
// 	|	keyword("prior")
// 	|	keyword("first")
// 	|	keyword("last")
// 	|	{ keyword("absolute") | keyword("relative") } simple_value_specification
//
// <fetch target list> ::= target_specification [ { lit(',') target_specification }... ]
//
// //h3 14.4 <close statement> (p820)
// ///h3
//
// //p
// Close a cursor.
// ///p
//
// <close statement> ::= keyword("close") local_qualified_name
//
// //h3 14.5 <select statement: single row> (p822)
// ///h3
//
// //p
// Retrieve values from a specified row of a table.
// ///p
//
// <select statement: single row> ::=
// 		keyword("select") [ set_quantifier ] <select list> keyword("into") <select target list> table_expression
//
// <select target list> ::= target_specification [ { lit(',') target_specification }... ]
//
// //h3 14.6 <delete statement: positioned> (p826)
// ///h3
//
// //p
// Delete a row of a table.
// ///p
//
// <delete statement: positioned> ::= keyword("delete") keyword("from") <target table> keyword("where") keyword("current") keyword("of") local_qualified_name
//
// <target table> ::=
// 		local_or_schema_qualified_name
// 	|	keyword("only") lit('(') local_or_schema_qualified_name lit(')')
//
// //h3 14.7 <delete statement: searched> (p829)
// ///h3
//
// //p
// Delete rows of a table.
// ///p
//
// <delete statement: searched> ::= keyword("delete") keyword("from") <target table> [ keyword("where") boolean_value_expression ]
//
// //h3 14.8 <insert statement> (p832)
// ///h3
//
// //p
// Create new rows in a table.
// ///p
//
// <insert statement> ::= keyword("insert") keyword("into") <insertion target> <insert columns and source>
//
// <insertion target> ::= local_or_schema_qualified_name
//
// <insert columns and source> ::=
// 		<from subquery>
// 	|	<from constructor>
// 	|	<from default>
//
// <from subquery> ::= [ lit('(') <insert column list> lit(')') ] [ <override clause> ] query_expression
//
// <from constructor> ::=
// 		[ lit('(') <insert column list> lit(')') ] [ <override clause> ] contextually_typed_table_value_constructor
//
// <override clause> ::= keyword("overriding") keyword("user") keyword("value") | keyword("overriding") keyword("system") keyword("value")
//
// <from default> ::= keyword("default") keyword("values")
//
// <insert column list> ::= column_name_list
//
// //h3 14.9 <merge statement> (p837)
// ///h3
//
// //p
// Conditionally update rows of a table, or insert new rows into a table, or both.
// ///p
//
// <merge statement> ::=
// 		keyword("merge") keyword("into") <target table> [ >> -(keyword("as") >> <merge correlation name> ]
// 		keyword("using") table_reference keyword("on") boolean_value_expression <merge operation specification>
//
// <merge correlation name> ::= identifier
//
// <merge operation specification> ::= <merge when clause>...
//
// <merge when clause> ::= <merge when matched clause> | <merge when not matched clause>
//
// <merge when matched clause> ::= keyword("when") keyword("matched") keyword("then") <merge update specification>
//
// <merge when not matched clause> ::= keyword("when") keyword("not") keyword("matched") keyword("then") <merge insert specification>
//
// <merge update specification> ::= keyword("update") keyword("set") <set clause list>
//
// <merge insert specification> ::=
// 		keyword("insert") [ lit('(') <insert column list> lit(')') ]
// 		[ <override clause> ] keyword("values") <merge insert value list>
//
// <merge insert value list> ::=
// 		lit('(') <merge insert value element> [ { lit(',') <merge insert value element> }... ] lit(')')
//
// <merge insert value element> ::= value_expression | contextually_typed_value_specification
//
// //h3 14.10 <update statement: positioned> (p844)
// ///h3
//
// //p
// Update a row of a table.
// ///p
//
// <update statement: positioned> ::= keyword("update") <target table> keyword("set") <set clause list> keyword("where") keyword("current") keyword("of") local_qualified_name
//
// //h3 14.11 <update statement: searched> (p847)
// ///h3
//
// //p
// Update rows of a table.
// ///p
//
// <update statement: searched> ::= keyword("update") <target table> keyword("set") <set clause list> [ keyword("where") boolean_value_expression ]
//
// //h3 14.12 <set clause list> (p851)
// ///h3
//
// //p
// Specify a list of updates.
// ///p
//
// <set clause list> ::= <set clause> [ { lit(',') <set clause> }... ]
//
// <set clause> ::=
// 		<multiple column assignment>
// 	|	<set target> lit('=') <update source>
//
// <set target> ::= <update target> | <mutated set clause>
//
// <multiple column assignment> ::= <set target list> lit('=') <assigned row>
//
// <set target list> ::= lit('(') <set target> [ { lit(',') <set target> }... ] lit(')')
//
// <assigned row> ::= contextually_typed_row_value_expression
//
// <update target> ::=
// 		<object column>
// 	|	<object column> left_bracket_or_trigraph simple_value_specification right_bracket_or_trigraph
//
// <object column> ::= identifier
//
// <mutated set clause> ::= <mutated target> lit('.') identifier
//
// <mutated target> ::= <object column> | <mutated set clause>
//
// <update source> ::= value_expression | contextually_typed_value_specification
//
// //h3 14.13 <temporary table declaration> (p856)
// ///h3
//
// //p
// Declare a declared local temporary table.
// ///p
//
// <temporary table declaration> ::=
// 		keyword("declare") keyword("local") keyword("temporary") keyword("table") local_or_schema_qualified_name <table element list>
// 		[ keyword("on") keyword("commit") <table commit action> keyword("rows") ]
//
// //h3 14.14 <free locator statement> (p858)
// ///h3
//
// //p
// Remove the association between a locator variable and the value that is represented by that locator.
// ///p
//
// <free locator statement> ::= keyword("free") keyword("locator") <locator reference> [ { lit(',') <locator reference> }... ]
//
// <locator reference> ::= host_parameter_name | embedded_variable_name
//
// //h3 14.15 <hold locator statement> (p859)
// ///h3
//
// //p
// Mark a locator variable as being holdable.
// ///p
//
// <hold locator statement> ::= keyword("hold") keyword("locator") <locator reference> [ { lit(',') <locator reference> }... ]
//
// //h3 14.16 Effect of deleting rows from base tables (p860)
// ///h3
//
// //h3 14.17 Effect of deleting some rows from a derived table (p862)
// ///h3
//
// //h3 14.18 Effect of deleting some rows from a viewed table (p864)
// ///h3
//
// //h3 14.19 Effect of inserting tables into base tables (p865)
// ///h3
//
// //h3 14.20 Effect of inserting a table into a derived table (p867)
// ///h3
//
// //h3 14.21 Effect of inserting a table into a viewed table (p869)
// ///h3
//
// //h3 14.22 Effect of replacing rows in base tables (p871)
// ///h3
//
// //h3 14.23 Effect of replacing some rows in a derived table (p874)
// ///h3
//
// //h3 14.24 Effect of replacing some rows in a viewed table (p877)
// ///h3
//
// //h3 14.25 Execution of keyword("before") triggers (p879)
// ///h3
//
// //h3 14.26 Execution of keyword("after") triggers (p880)
// ///h3
//
// //h3 14.27 Execution of triggers (p881)
// ///h3
//
// //hr
// //h2 15 Control statements
// ///h2
//
// //h3 15.1 <call statement> (p883)
// ///h3
//
// //p
// Invoke an SQL-invoked routine.
// ///p
//
// <call statement> ::= keyword("call") routine_invocation
//
// //h3 15.2 <return statement> (p884)
// ///h3
//
// //p
// Return a value from an SQL function.
// ///p
//
// <return statement> ::= keyword("return") <return value>
//
// <return value> ::= value_expression | keyword("null")
//
// //hr
// //h2 16 Transaction management
// ///h2
//
// //h3 16.1 <start transaction statement> (p885)
// ///h3
//
// //p
// Start an SQL-transaction and set its characteristics.
// ///p
//
// <start transaction statement> ::= keyword("start") keyword("transaction") [ <transaction mode> [ { lit(',') <transaction mode> }...] ]
//
// <transaction mode> ::= <isolation level> | <transaction access mode> | <diagnostics size>
//
// <transaction access mode> ::= keyword("read") keyword("only") | keyword("read") keyword("write")
//
// <isolation level> ::= keyword("isolation") keyword("level") <level of isolation>
//
// <level of isolation> ::= keyword("read") keyword("uncommitted") | keyword("read") keyword("committed") | keyword("repeatable") keyword("read") | keyword("serializable")
//
// <diagnostics size> ::= keyword("diagnostics") keyword("size") <number of conditions>
//
// <number of conditions> ::= simple_value_specification
//
// //h3 16.2 <set transaction statement> (p888)
// ///h3
//
// //p
// Set the characteristics of the next SQL-transaction for the SQL-agent.
// NOTE 402 - This statement has no effect on any SQL-transactions subsequent to the next SQL-transaction.
// ///p
//
// <set transaction statement> ::= keyword("set") [ keyword("local") ] <transaction characteristics>
//
// <transaction characteristics> ::= keyword("transaction") <transaction mode> [ { lit(',') <transaction mode> }... ]
//
// //h3 16.3 <set constraints mode statement> (p890)
// ///h3
//
// //p
// If an SQL-transaction is currently active, then set the constraint mode for that SQL-transaction in
// the current SQL-session. If no SQL-transaction is currently active, then set the constraint mode for
// the next SQL-transaction in the current SQL-session for the SQL-agent.
// NOTE 404: This statement has no effect on any SQL-transactions subsequent to this SQL-transaction.
// ///p
//
// <set constraints mode statement> ::= SET keyword("constraints") <constraint name list> { keyword("deferred") | keyword("immediate") }
//
// <constraint name list> ::= keyword("all") | schema_qualified_name [ { lit(',') schema_qualified_name }... ]
//
// //h3 16.4 <savepoint statement> (p892)
// ///h3
//
// //p
// Establish a savepoint.
// ///p
//
// <savepoint statement> ::= keyword("savepoint") <savepoint specifier>
//
// <savepoint specifier> ::= identifier
//
// //h3 16.5 <release savepoint statement> (p893)
// ///h3
//
// //p
// Destroy a savepoint.
// ///p
//
// <release savepoint statement> ::= keyword("release") keyword("savepoint") <savepoint specifier>
//
// //h3 16.6 <commit statement> (p894)
// ///h3
//
// //p
// Terminate the current keyword("sql")-transaction with commit.
// ///p
//
// <commit statement> ::= keyword("commit") [ keyword("work") ] [ keyword("and") [ keyword("no") ] keyword("chain") ]
//
// //h3 16.7 <rollback statement> (p896)
// ///h3
//
// //p
// Terminate the current SQL-transaction with rollback, or rollback all actions affecting SQL-data
// and/or schemas since the establishment of a savepoint.
// ///p
//
// <rollback statement> ::= keyword("rollback") [ keyword("work") ] [ keyword("and") [ keyword("no") ] keyword("chain") ] [ <savepoint clause> ]
//
// <savepoint clause> ::= keyword("to") keyword("savepoint") <savepoint specifier>
//
// //hr
// //h2 17 Connection management
// ///h2
//
// //h3 17.1 <connect statement> (p899)
// ///h3
//
// //p
// Establish an SQL-session.
// ///p
//
// <connect statement> ::= keyword("connect") keyword("to") <connection target>
//
// <connection target> ::=
// 		simple_value_specification [ AS simple_value_specification ] [ USER simple_value_specification ]
// 	|	DEFAULT
//
// //h3 17.2 <set connection statement> (p902)
// ///h3
//
// //p
// Select an SQL-connection from the available SQL-connections.
// ///p
//
// <set connection statement> ::= SET CONNECTION <connection object>;
//
// <connection object> ::= DEFAULT | simple_value_specification;
//
// //h3 17.3 <disconnect statement> (p904)
// ///h3
//
// //p
// Terminate an SQL-connection.
// ///p
//
// <disconnect statement> ::= DISCONNECT <disconnect object>;
//
// <disconnect object> ::= <connection object> | ALL |	CURRENT;
//
// //hr
// //h2 18 Session management
// ///h2
//
// //h3 18.1 <set session characteristics statement> (p907)
// ///h3
//
// //p
// Set one or more characteristics for the current SQL-session.
// ///p
//
// <set session characteristics statement> ::= SET SESSION CHARACTERISTICS AS <session characteristic list>
//
// <session characteristic list> ::= <session characteristic> [ { lit(',') <session characteristic> }... ]
//
// <session characteristic> ::= <transaction characteristics>
//
// //h3 18.2 <set session user identifier statement> (p908)
// ///h3
//
// //p
// Set the SQL-session user identifier and the current user identifier of the current SQL-session
// context.
// ///p
//
// <set session user identifier statement> ::= SET SESSION AUTHORIZATION value_specification
//
// //h3 18.3 <set role statement> (p909)
// ///h3
//
// //p
// Set the current role name for the current SQL-session context.
// ///p
//
// <set role statement> ::= SET ROLE <role specification>
//
// <role specification> ::= value_specification | NONE
//
// //h3 18.4 <set local time zone statement> (p911)
// ///h3
//
// //p
// Set the default local time zone displacement for the current SQL-session.
// ///p
//
// <set local time zone statement> ::= SET TIME ZONE <set time zone value>
//
// <set time zone value> ::= interval_value_expression | LOCAL
//
// //h3 18.5 <set catalog statement> (p912)
// ///h3
//
// //p
// Set the default catalog name for unqualified schema_names in <preparable statement>s that
// are prepared in the current SQL-session by an <execute immediate statement> or a <prepare
// statement> and in <direct SQL statement>s that are invoked directly.
// ///p
//
// <set catalog statement> ::= SET <catalog name characteristic>
//
// <catalog name characteristic> ::= CATALOG value_specification
//
// //h3 18.6 <set schema statement> (p913)
// ///h3
//
// //p
// Set the default schema name for unqualified schema_qualified_names in <preparable statement>s
// that are prepared in the current SQL-session by an <execute immediate statement> or a <prepare
// statement> and in <direct SQL statement>s that are invoked directly.
// ///p
//
// <set schema statement> ::= SET <schema name characteristic>
//
// <schema name characteristic> ::= SCHEMA value_specification
//
// //h3 18.7 <set names statement> (p915)
// ///h3
//
// //p
// Set the default character set name for character_string_literal s in <preparable statement>s that
// are prepared in the current SQL-session by an <execute immediate statement> or a <prepare
// statement> and in <direct SQL statement>s that are invoked directly.
// ///p
//
// <set names statement> ::= SET <character set name characteristic>
//
// <character set name characteristic> ::= NAMES value_specification
//
// //h3 18.8 <set path statement> (p916)
// ///h3
//
// //p
// Set the SQL-path used to determine the subject routine of routine_invocations with unqualified
// routine_names in <preparable statement>s that are prepared in the current SQL-session by
// an <execute immediate statement> or a <prepare statement> and in <direct SQL statement>s,
// respectively, that are invoked directly. The SQL-path remains the current SQL-path of the SQLsession
// until another SQL-path is successfully set.
// ///p
//
// <set path statement> ::= SET <SQL-path characteristic>
//
// <SQL-path characteristic> ::= PATH value_specification
//
// //h3 18.9 <set transform group statement> (p917)
// ///h3
//
// //p
// Set the group name that identifies the group of transform functions for
// mapping values of userdefined types to predefined data types.
// ///p
//
// <set transform group statement> ::= SET <transform group characteristic>
//
// <transform group characteristic> ::=
// 		DEFAULT TRANSFORM GROUP value_specification
// 	|	TRANSFORM GROUP FOR TYPE schema_qualified_type_name value_specification
//
// //h3 18.10 <set session collation statement> (p918)
// ///h3
//
// //p
// Set the SQL-session collation of the SQL-session for one or more
// character sets.  An SQL-session collation remains effective until
// another SQL-session collation for the same character set is successfully
// set.
// ///p
//
// <set session collation statement> ::=
// 		SET COLLATION <collation specification> [ FOR <character set specification list> ]
// 	|	SET NO COLLATION [ FOR <character set specification list> ]
//
// //@@ This is a second <character set specification list> definition; the first
// //@@ is in section 13.1.
// //@@ It is marginally different in detail from the previous version, but the
// //@@ overall effect is the same &mdash; a comma-separated list of <character
// //@@ set specification> items.
// //@@ It isn't clear why there's a repeat of the rule or the difference in the expansion.
// //@@
// //@@ <character set specification list> ::= character_set_name [ , character_set_name... ]
//
// <collation specification> ::= value_specification
//
// //hr
// //h2 19 Dynamic SQL
//
// //h3 19.1 Description of SQL descriptor areas (p921)
// ///h3
//
// //h3 19.2 <allocate descriptor statement> (p931)
// ///h3
//
// //p
// Allocate an SQL descriptor area.
// ///p
//
// <allocate descriptor statement> ::= ALLOCATE [ SQL ] DESCRIPTOR extended_statement_name [ WITH MAX <occurrences> ]
//
// <occurrences> ::= simple_value_specification
//
// //h3 19.3 <deallocate descriptor statement> (p933)
// ///h3
//
// //p
// Deallocate an SQL descriptor area.
// ///p
//
// <deallocate descriptor statement> ::= DEALLOCATE [ SQL ] DESCRIPTOR extended_statement_name
//
// //h3 19.4 <get descriptor statement> (p934)
// ///h3
//
// //p
// Get information from an SQL descriptor area.
// ///p
//
// <get descriptor statement> ::= GET [ SQL ] DESCRIPTOR extended_statement_name <get descriptor information>
//
// <get descriptor information> ::=
// 		<get header information> [ { lit(',') <get header information> }... ]
// 	|	VALUE <item number> <get item information> [ { lit(',') <get item information> }... ]
//
// <get header information> ::= <simple target specification 1> lit('=') <header item name>
//
// <header item name> ::= COUNT | KEY_TYPE | DYNAMIC_FUNCTION | DYNAMIC_FUNCTION_CODE | TOP_LEVEL_COUNT
//
// <get item information> ::= <simple target specification 2> lit('=') <descriptor item name>
//
// //p
// //i
// //small
// The rule for <item number> was repeated verbatim in section 19.5.
// That rule is now omitted.
// ///small
// ///i
// ///p
//
// <item number> ::= simple_value_specification
//
// <simple target specification 1> ::= simple_target_specification
//
// <simple target specification 2> ::= simple_target_specification
//
// <descriptor item name> ::=
// 		CARDINALITY
// 	|	CHARACTER_SET_CATALOG
// 	|	CHARACTER_SET_NAME
// 	|	CHARACTER_SET_SCHEMA
// 	|	COLLATION_CATALOG
// 	|	COLLATION_NAME
// 	|	COLLATION_SCHEMA
// 	|	DATA
// 	|	DATETIME_INTERVAL_CODE
// 	|	DATETIME_INTERVAL_PRECISION
// 	|	DEGREE
// 	|	INDICATOR
// 	|	KEY_MEMBER
// 	|	LENGTH
// 	|	LEVEL
// 	|	NAME
// 	|	NULLABLE
// 	|	OCTET_LENGTH
// 	|	PARAMETER_MODE
// 	|	PARAMETER_ORDINAL_POSITION
// 	|	PARAMETER_SPECIFIC_CATALOG
// 	|	PARAMETER_SPECIFIC_NAME
// 	|	PARAMETER_SPECIFIC_SCHEMA
// 	|	PRECISION
// 	|	RETURNED_CARDINALITY
// 	|	RETURNED_LENGTH
// 	|	RETURNED_OCTET_LENGTH
// 	|	SCALE
// 	|	SCOPE_CATALOG
// 	|	SCOPE_NAME
// 	|	SCOPE_SCHEMA
// 	|	TYPE
// 	|	UNNAMED
// 	|	USER_DEFINED_TYPE_CATALOG
// 	|	USER_DEFINED_TYPE_NAME
// 	|	USER_DEFINED_TYPE_SCHEMA
// 	|	USER_DEFINED_TYPE_CODE;
//
// //h3 19.5 <set descriptor statement> (p937)
// ///h3
//
// //p
// Set information in an SQL descriptor area.
// ///p
//
// <set descriptor statement> ::= SET [ SQL ] DESCRIPTOR extended_statement_name <set descriptor information>
//
// <set descriptor information> ::=
// 		<set header information> [ { lit(',') <set header information> }... ]
// 	|	VALUE <item number> <set item information> [ { lit(',') <set item information> }... ]
//
// <set header information> ::= <header item name> lit('=') <simple value specification 1>
//
// <set item information> ::= <descriptor item name> lit('=') <simple value specification 2>
//
// <simple value specification 1> ::= simple_value_specification
//
// <simple value specification 2> ::= simple_value_specification
//
// //@@ This <item number> is a repeat of the rule in section 19.4
// //@@ <item number> ::= simple_value_specification
//
// //h3 19.6 <prepare statement> (p941)
// ///h3
//
// //p
// Prepare a statement for execution.
// ///p
//
// <prepare statement> ::=
// 		PREPARE SQL_statement_name [ <attributes specification> ] FROM <SQL statement variable>
//
// <attributes specification> ::= ATTRIBUTES <attributes variable>
//
// <attributes variable> ::= simple_value_specification
//
// <SQL statement variable> ::= simple_value_specification
//
// <preparable statement> ::=
// 		<preparable SQL data statement>
// 	|	<preparable SQL schema statement>
// 	|	<preparable SQL transaction statement>
// 	|	<preparable SQL control statement>
// 	|	<preparable SQL session statement>
// 	|	<preparable implementation-defined statement>
//
// <preparable SQL data statement> ::=
// 		<delete statement: searched>
// 	|	<dynamic single row select statement>
// 	|	<insert statement>
// 	|	<dynamic select statement>
// 	|	<update statement: searched>
// 	|	<merge statement>
// 	|	<preparable dynamic delete statement: positioned>
// 	|	<preparable dynamic update statement: positioned>
//
// <preparable SQL schema statement> ::= <SQL schema statement>
//
// <preparable SQL transaction statement> ::= <SQL transaction statement>
//
// <preparable SQL control statement> ::= <SQL control statement>
//
// <preparable SQL session statement> ::= <SQL session statement>
//
// <dynamic select statement> ::= cursor_specification
//
// <preparable implementation-defined statement> ::= !! See the Syntax Rules.
//
// //h3 19.7 <cursor attributes> (p953)
// ///h3
//
// //p
// Specify a list of cursor attributes.
// ///p
//
// <cursor attributes> ::= <cursor attribute>...
//
// <cursor attribute> ::=
// 		<cursor sensitivity>
// 	|	<cursor scrollability>
// 	|	<cursor holdability>
// 	|	<cursor returnability>
//
// //h3 19.8 <deallocate prepared statement> (p954)
// ///h3
//
// //p
// Deallocate SQL-statements that have been prepared with a <prepare statement>.
// ///p
//
// <deallocate prepared statement> ::= DEALLOCATE PREPARE SQL_statement_name
//
// //h3 19.9 <describe statement> (p955)
// ///h3
//
// //p
// Obtain information about the <select list> columns or dynamic_parameter_specifications contained
// in a prepared statement or about the columns of the result set associated with a cursor.
// ///p
//
// <describe statement> ::= <describe input statement> | <describe output statement>
//
// <describe input statement> ::= DESCRIBE INPUT SQL_statement_name <using descriptor> [ <nesting option> ]
//
// <describe output statement> ::= DESCRIBE [ OUTPUT ] <described object> <using descriptor> [ <nesting option> ]
//
// <nesting option> ::= WITH NESTING | WITHOUT NESTING
//
// <using descriptor> ::= USING [ SQL ] DESCRIPTOR extended_statement_name
//
// <described object> ::=
// 		SQL_statement_name
// 	|	CURSOR extended_statement_name STRUCTURE
//
// //h3 19.10 <input using clause> (p961)
// ///h3
//
// //p
// Supply input values for an <SQL dynamic statement>.
// ///p
//
// <input using clause> ::= <using arguments> | <using input descriptor>
//
// <using arguments> ::= USING <using argument> [ { lit(',') <using argument> }... ]
//
// <using argument> ::= general_value_specification
//
// <using input descriptor> ::= <using descriptor>
//
// //h3 19.11 <output using clause> (p965)
// ///h3
//
// //p
// Supply output variables for an <SQL dynamic statement>.
// ///p
//
// <output using clause> ::= <into arguments> | <into descriptor>
//
// <into arguments> ::= INTO <into argument> [ { lit(',') <into argument> }... ]
//
// <into argument> ::= target_specification
//
// <into descriptor> ::= INTO [ SQL ] DESCRIPTOR extended_statement_name
//
// //h3 19.12 <execute statement> (p970)
// ///h3
//
// //p
// Associate input SQL parameters and output targets with a prepared statement and execute the
// statement.
// ///p
//
// <execute statement> ::= EXECUTE SQL_statement_name [ <result using clause> ] [ <parameter using clause> ]
//
// <result using clause> ::= <output using clause>
//
// <parameter using clause> ::= <input using clause>
//
// //h3 19.13 <execute immediate statement> (p972)
// ///h3
//
// //p
// Dynamically prepare and execute a preparable statement.
// ///p
//
// <execute immediate statement> ::= EXECUTE IMMEDIATE <SQL statement variable>
//
// //h3 19.14 <dynamic declare cursor> (p973)
// ///h3
//
// //p
// Declare a cursor to be associated with a identifier, which may in turn be associated with a
// cursor_specification.
// ///p
//
// <dynamic declare cursor> ::=
// 		DECLARE local_qualified_name [ <cursor sensitivity> ] [ <cursor scrollability> ] CURSOR
// 		[ <cursor holdability> ] [ <cursor returnability> ] FOR identifier
//
// //h3 19.15 <allocate cursor statement> (p974)
// ///h3
//
// //p
// Define a cursor based on a prepared statement for a cursor_specification or assign a cursor to the
// ordered set of result sets returned from an SQL-invoked procedure.
// ///p
//
// <allocate cursor statement> ::= ALLOCATE extended_statement_name <cursor intent>
//
// <cursor intent> ::= <statement cursor> | <result set cursor>
//
// <statement cursor> ::=
// 		[ <cursor sensitivity> ] [ <cursor scrollability> ] CURSOR
// 		[ <cursor holdability> ] [ <cursor returnability> ] FOR extended_statement_name
//
// <result set cursor> ::= FOR PROCEDURE <specific routine designator>
//
// //h3 19.16 <dynamic open statement> (p976)
// ///h3
//
// //p
// Associate input dynamic parameters with a cursor_specification and open the cursor.
// ///p
//
// <dynamic open statement> ::= OPEN dynamic_cursor_name [ <input using clause> ]
//
// //h3 19.17 <dynamic fetch statement> (p977)
// ///h3
//
// //p
// Fetch a row for a cursor declared with a <dynamic declare cursor>.
// ///p
//
// <dynamic fetch statement> ::= FETCH [ [ <fetch orientation> ] FROM ] dynamic_cursor_name <output using clause>
//
// //h3 19.18 <dynamic single row select statement> (p978)
// ///h3
//
// //p
// Retrieve values from a dynamically-specified row of a table.
// ///p
//
// <dynamic single row select statement> ::= query_specification
//
// //h3 19.19 <dynamic close statement> (p979)
// ///h3
//
// //p
// Close a cursor.
// ///p
//
// <dynamic close statement> ::= CLOSE dynamic_cursor_name
//
// //h3 19.20 <dynamic delete statement: positioned> (p980)
// ///h3
//
// //p
// Delete a row of a table.
// ///p
//
// <dynamic delete statement: positioned> ::= DELETE FROM <target table> WHERE CURRENT OF dynamic_cursor_name
//
// //h3 19.21 <dynamic update statement: positioned> (p982)
// ///h3
//
// //p
// Update a row of a table.
// ///p
//
// <dynamic update statement: positioned> ::=
// 		UPDATE <target table> SET <set clause list> WHERE CURRENT OF dynamic_cursor_name
//
// //h3 19.22 <preparable dynamic delete statement: positioned> (p984)
// ///h3
//
// //p
// Delete a row of a table through a dynamic cursor.
// ///p
//
// <preparable dynamic delete statement: positioned> ::=
// 		DELETE [ FROM <target table> ] WHERE CURRENT OF [ scope_option ] local_qualified_name
//
// //h3 19.23 <preparable dynamic update statement: positioned> (p986)
// ///h3
//
// //p
// Update a row of a table through a dynamic cursor.
// ///p
//
// <preparable dynamic update statement: positioned> ::=
// 		UPDATE [ <target table> ] SET <set clause list> WHERE CURRENT OF [ scope_option ] local_qualified_name
//
		//-----------------------------------------------------------------------------
		// 20 Embedded SQL
		//-----------------------------------------------------------------------------

		//-----------------------------------------------------------------------------
		// 20.1 <embedded SQL host program>
		//-----------------------------------------------------------------------------

// <embedded SQL host program> ::=
// 		<embedded SQL Ada program>
// 	|	<embedded SQL C program>
// 	|	<embedded SQL COBOL program>
// 	|	<embedded SQL Fortran program>
// 	|	<embedded SQL MUMPS program>
// 	|	<embedded SQL Pascal program>
// 	|	<embedded SQL PL/I program>;
//
// <embedded SQL statement> ::= <SQL prefix> <statement or declaration> [ <SQL terminator> ];
//
// <statement or declaration> ::=
// 		<declare cursor>
// 	|	<dynamic declare cursor>
// 	|	<temporary table declaration>
// 	|	<embedded authorization declaration>
// 	|	<embedded path specification>
// 	|	<embedded transform group specification>
// 	|	<embedded collation specification>
// 	|	<embedded exception declaration>
// 	|	<handler declaration>
// 	|	<SQL procedure statement>;
//
// <SQL prefix> ::= EXEC SQL | lit('&')SQLlit('(')
//
// <SQL terminator> ::= END-EXEC | lit(';') | lit(')')
//
// <embedded authorization declaration> ::= DECLARE <embedded authorization clause>
//
// <embedded authorization clause> ::=
// 		SCHEMA schema_name
// 	|	AUTHORIZATION <embedded authorization identifier> [ FOR STATIC { ONLY | AND DYNAMIC } ]
// 	|	SCHEMA schema_name AUTHORIZATION <embedded authorization identifier> [ FOR STATIC { ONLY | AND DYNAMIC } ]
//
// <embedded authorization identifier> ::= <module authorization identifier>
//
// <embedded path specification> ::= <path specification>
//
// <embedded transform group specification> ::= <transform group specification>
//
// <embedded collation specification> ::= <module collation>
//
// <embedded SQL declare section> ::=
// 		<embedded SQL begin declare> [ <embedded character set declaration> ] [ <host variable definition>... ] <embedded SQL end declare>
// 	|	<embedded SQL MUMPS declare>
//
// <embedded character set declaration> ::= SQL NAMES ARE character_set_name
//
// <embedded SQL begin declare> ::= <SQL prefix> BEGIN DECLARE SECTION [ <SQL terminator> ]
//
// <embedded SQL end declare> ::= <SQL prefix> END DECLARE SECTION [ <SQL terminator> ]
//
// <embedded SQL MUMPS declare> ::=
// 		<SQL prefix> BEGIN DECLARE SECTION [ <embedded character set declaration> ]
// 		[ <host variable definition>... ] END DECLARE SECTION <SQL terminator>
//
// <host variable definition> ::=
// 		<Ada variable definition>
// 	|	<C variable definition>
// 	|	<COBOL variable definition>
// 	|	<Fortran variable definition>
// 	|	<MUMPS variable definition>
// 	|	<Pascal variable definition>
// 	|	<PL/I variable definition>
//


		const rule<class host_identifier> host_identifier = "host_identifier";

		const auto embedded_variable_name_def = lit(':') >> host_identifier;

		const auto host_identifier_def =
			/* <Ada host identifier>
			   | */C_host_identifier/*
			   | <COBOL host identifier>
			   | <Fortran host identifier>
			   | <MUMPS host identifier>
			   | <Pascal host identifier>
			   | <PL/I host identifier>*/;

		BOOST_SPIRIT_DEFINE(embedded_variable_name, host_identifier);


		//-----------------------------------------------------------------------------
		// 20.2 <embedded exception declaration>
		//-----------------------------------------------------------------------------


// <embedded exception declaration> ::= WHENEVER <condition> <condition action>
//
// <condition> ::= <SQL condition>
//
// <SQL condition> ::=
// 		<major category>
// 	|	SQLSTATE ( <SQLSTATE class value> [ , <SQLSTATE subclass value> ] )
// 	|	CONSTRAINT schema_qualified_name
//
// <major category> ::= SQLEXCEPTION | SQLWARNING | NOT FOUND
//
// <SQLSTATE class value> ::= <SQLSTATE char><SQLSTATE char> !! See the Syntax Rules.
//
// <SQLSTATE subclass value> ::= <SQLSTATE char><SQLSTATE char><SQLSTATE char> !! See the Syntax Rules.
//
// <SQLSTATE char> ::= <simple Latin upper case letter> | <digit>
//
// <condition action> ::= CONTINUE | <go to>
//
// <go to> ::= { GOTO | GO TO } <goto target>
//
// <goto target> ::=
// 		<host label identifier>
// 	|	unsigned_integer
// 	|	<host PL/I label variable>
//
// <host label identifier> ::= !! See the Syntax Rules.
//
// <host PL/I label variable> ::= !! See the Syntax Rules.
//
// //h3 20.3 <embedded SQL Ada program> (p1005)
// ///h3
//
// //p
// Specify an <embedded SQL Ada program>.
// ///p
//
// <embedded SQL Ada program> ::= !! See the Syntax Rules.
//
// <Ada variable definition> ::=
// 		<Ada host identifier> [ { lit(',') <Ada host identifier> }... ] lit(':') <Ada type specification> [ <Ada initial value> ]
//
// <Ada initial value> ::= <Ada assignment operator> <character representation>...
//
// <Ada assignment operator> ::= lit(':')lit('=')
//
// <Ada host identifier> ::= !! See the Syntax Rules.
//
// <Ada type specification> ::=
// 		<Ada qualified type specification>
// 	|	<Ada unqualified type specification>
// 	|	<Ada derived type specification>
//
// <Ada qualified type specification> ::=
// 		Interfaces.SQL lit('.') CHAR [ CHARACTER SET [ IS ] character_set_name ]
// 		lit('(') 1 lexeme[lit("..")] unsigned_integer lit(')')
// 	|	Interfaces.SQL lit('.') SMALLINT
// 	|	Interfaces.SQL lit('.') INT
// 	|	Interfaces.SQL lit('.') BIGINT
// 	|	Interfaces.SQL lit('.') REAL
// 	|	Interfaces.SQL lit('.') DOUBLE_PRECISION
// 	|	Interfaces.SQL lit('.') BOOLEAN
// 	|	Interfaces.SQL lit('.') SQLSTATE_TYPE
// 	|	Interfaces.SQL lit('.') INDICATOR_TYPE
//
// <Ada unqualified type specification> ::=
// 		CHAR lit('(') 1 lexeme[lit("..")] unsigned_integer lit(')')
// 	|	SMALLINT
// 	|	INT
// 	|	BIGINT
// 	|	REAL
// 	|	DOUBLE_PRECISION
// 	|	BOOLEAN
// 	|	SQLSTATE_TYPE
// 	|	INDICATOR_TYPE
//
// <Ada derived type specification> ::=
// 		<Ada CLOB variable>
// 	|	<Ada CLOB locator variable>
// 	|	<Ada BLOB variable>
// 	|	<Ada BLOB locator variable>
// 	|	<Ada user-defined type variable>
// 	|	<Ada user-defined type locator variable>
// 	|	<Ada REF variable>
// 	|	<Ada array locator variable>
// 	|	<Ada multiset locator variable>;
//
// <Ada CLOB variable> ::=
// 		SQL TYPE IS CLOB lit('(') large_object_length lit(')') [ CHARACTER SET [ IS ] character_set_name ]
//
// <Ada CLOB locator variable> ::= SQL TYPE IS CLOB AS LOCATOR
//
// <Ada BLOB variable> ::= SQL TYPE IS BLOB lit('(') large_object_length lit(')')
//
// <Ada BLOB locator variable> ::= SQL TYPE IS BLOB AS LOCATOR
//
// <Ada user-defined type variable> ::= SQL TYPE IS schema_qualified_type_name AS predefined_type
//
// <Ada user-defined type locator variable> ::= SQL TYPE IS schema_qualified_type_name AS LOCATOR
//
// <Ada REF variable> ::= SQL TYPE IS reference_type
//
// <Ada array locator variable> ::= SQL TYPE IS array_type AS LOCATOR
//
// <Ada multiset locator variable> ::= SQL TYPE IS multiset_type AS LOCATOR
//

		//-----------------------------------------------------------------------------
		// 20.4 <embedded SQL C program>
		//-----------------------------------------------------------------------------

// <embedded SQL C program> ::= !! See the Syntax Rules.
//
// <C variable definition> ::= [ <C storage class> ] [ <C class modifier> ] <C variable specification> lit(';')
//
// <C variable specification> ::= <C numeric variable> | <C character variable> | <C derived variable>
//
// <C storage class> ::= auto | extern | static
//
// <C class modifier> ::= const | volatile
//
// //p
// //small
// //i
// It is curious that the C types in SQL 2003 don't include the C keywords int or signed.
// It is slightly less surprising that the grammar doesn't include C99 keywords such as restrict.
// ///i
// ///small
// ///p
//
// <C numeric variable> ::=
// 		{ long long | long | short | float | double } C_host_identifier [ <C initial value> ]
// 		[ { lit(',') C_host_identifier [ <C initial value> ] }... ]
//
// <C character variable> ::=
// 		<C character type> [ CHARACTER SET [ IS ] character_set_name ]
// 		C_host_identifier <C array specification> [ <C initial value> ]
// 		[ { lit(',') C_host_identifier <C array specification> [ <C initial value> ] }... ]
//
// <C character type> ::= char | unsigned char | unsigned short
//
// <C array specification> ::= lit('[') unsigned_integer lit(']')

		const auto C_host_identifier_def = lexeme[alpha >> +alnum];

// <C derived variable> ::=
// 		<C VARCHAR variable>
// 	|	<C NCHAR variable>
// 	|	<C NCHAR VARYING variable>
// 	|	<C CLOB variable>
// 	|	<C NCLOB variable>
// 	|	<C BLOB variable>
// 	|	<C user-defined type variable>
// 	|	<C CLOB locator variable>
// 	|	<C BLOB locator variable>
// 	|	<C array locator variable>
// 	|	<C multiset locator variable>
// 	|	<C user-defined type locator variable>
// 	|	<C REF variable>
//
// <C VARCHAR variable> ::=
// 		VARCHAR [ CHARACTER SET [ IS ] character_set_name ]
// 		C_host_identifier <C array specification> [ <C initial value> ]
// 		[ { lit(',') C_host_identifier <C array specification> [ <C initial value> ] }... ]
//
// <C NCHAR variable> ::=
// 		NCHAR [ CHARACTER SET [ IS ] character_set_name ]
// 		C_host_identifier <C array specification> [ <C initial value> ]
// 		[ { lit(',') C_host_identifier <C array specification> [ <C initial value> ] } ... ]
//
// <C NCHAR VARYING variable> ::=
// 		NCHAR VARYING [ CHARACTER SET [ IS ] character_set_name ]
// 		C_host_identifier <C array specification> [ <C initial value> ]
// 		[ { lit(',') C_host_identifier <C array specification> [ <C initial value> ] } ... ]
//
// <C CLOB variable> ::=
// 		SQL TYPE IS CLOB lit('(') large_object_length lit(')')
// 		[ CHARACTER SET [ IS ] character_set_name ]
// 		C_host_identifier [ <C initial value> ]
// 		[ { lit(',') C_host_identifier [ <C initial value> ] }... ]
//
// <C NCLOB variable> ::=
// 		SQL TYPE IS NCLOB lit('(') large_object_length lit(')')
// 		[ CHARACTER SET [ IS ] character_set_name ]
// 		C_host_identifier [ <C initial value> ]
// 		[ { lit(',') C_host_identifier [ <C initial value> ] }... ]
//
// <C user-defined type variable> ::=
// 		SQL TYPE IS schema_qualified_type_name AS predefined_type
// 		C_host_identifier [ <C initial value> ]
// 		[ { lit(',') C_host_identifier [ <C initial value> ] } ... ]
//
// <C BLOB variable> ::=
// 		SQL TYPE IS BLOB lit('(') large_object_length lit(')')
// 		C_host_identifier [ <C initial value> ]
// 		[ { lit(',') C_host_identifier [ <C initial value> ] } ... ]
//
// <C CLOB locator variable> ::=
// 		SQL TYPE IS CLOB AS LOCATOR
// 		C_host_identifier [ <C initial value> ]
// 		[ { lit(',') C_host_identifier [ <C initial value> ] } ... ]
//
// <C BLOB locator variable> ::=
// 		SQL TYPE IS BLOB AS LOCATOR
// 		C_host_identifier [ <C initial value> ]
// 		[ { lit(',') C_host_identifier [ <C initial value> ] } ... ]
//
// <C array locator variable> ::=
// 		SQL TYPE IS array_type AS LOCATOR
// 		C_host_identifier [ <C initial value> ]
// 		[ { lit(',') C_host_identifier [ <C initial value> ] } ... ]
//
// <C multiset locator variable> ::=
// 		SQL TYPE IS multiset_type AS LOCATOR
// 		C_host_identifier [ <C initial value> ]
// 		[ { lit(',') C_host_identifier [ <C initial value> ] } ... ]
//
// <C user-defined type locator variable> ::=
// 		SQL TYPE IS
// 		schema_qualified_type_name AS LOCATOR
// 		C_host_identifier [ <C initial value> ]
// 		[ { lit(',') C_host_identifier [ <C initial value> ] }... ]
//
// <C REF variable> ::= SQL TYPE IS reference_type
//
// <C initial value> ::= lit('=') <character representation>...

		BOOST_SPIRIT_DEFINE(C_host_identifier);



// //h3 20.5 <embedded SQL COBOL program> (p1019)
// ///h3
//
// //p
// Specify an <embedded SQL COBOL program>.
// ///p
//
// <embedded SQL COBOL program> ::= !! See the Syntax Rules.
//
// <COBOL variable definition> ::=
// 		{ 01 | 77 } <COBOL host identifier> <COBOL type specification>
// 		[ <character representation>... ] lit('.')
//
// <COBOL host identifier> ::= !! See the Syntax Rules.
//
// <COBOL type specification> ::=
// 		<COBOL character type>
// 	|	<COBOL national character type>
// 	|	<COBOL numeric type>
// 	|	<COBOL integer type>
// 	|	<COBOL derived type specification>
//
// <COBOL derived type specification> ::=
// 		<COBOL CLOB variable>
// 	|	<COBOL NCLOB variable>
// 	|	<COBOL BLOB variable>
// 	|	<COBOL user-defined type variable>
// 	|	<COBOL CLOB locator variable>
// 	|	<COBOL BLOB locator variable>
// 	|	<COBOL array locator variable>
// 	|	<COBOL multiset locator variable>
// 	|	<COBOL user-defined type locator variable>
// 	|	<COBOL REF variable>
//
// <COBOL character type> ::=
// 		[ CHARACTER SET [ IS ] character_set_name ]
// 		{ PIC | PICTURE } [ IS ] { X [ lit('(') unsigned_integer lit(')') ] }...
//
// <COBOL national character type> ::=
// 		[ CHARACTER SET [ IS ] character_set_name ]
// 		{ PIC | PICTURE } [ IS ] { N [ lit('(') unsigned_integer lit(')') ] }...
//
// <COBOL CLOB variable> ::=
// 		[ USAGE [ IS ] ]
// 		SQL TYPE IS CLOB lit('(') large_object_length lit(')')
// 		[ CHARACTER SET [ IS ] character_set_name ]
//
// <COBOL NCLOB variable> ::=
// 		[ USAGE [ IS ] ]
// 		SQL TYPE IS NCLOB lit('(') large_object_length lit(')')
// 		[ CHARACTER SET [ IS ] character_set_name ]
//
// <COBOL BLOB variable> ::=
// 		[ USAGE [ IS ] ]
// 		SQL TYPE IS BLOB lit('(') large_object_length lit(')')
//
// <COBOL user-defined type variable> ::=
// 		[ USAGE [ IS ] ] SQL TYPE IS schema_qualified_type_name AS predefined_type
//
// <COBOL CLOB locator variable> ::=
// 		[ USAGE [ IS ] ] SQL TYPE IS CLOB AS LOCATOR
//
// <COBOL BLOB locator variable> ::=
// 		[ USAGE [ IS ] ] SQL TYPE IS BLOB AS LOCATOR
//
// <COBOL array locator variable> ::=
// 	[ USAGE [ IS ] ] SQL TYPE IS array_type AS LOCATOR
//
// <COBOL multiset locator variable> ::=
// 	[ USAGE [ IS ] ] SQL TYPE IS multiset_type AS LOCATOR
//
// <COBOL user-defined type locator variable> ::=
// 	[ USAGE [ IS ] ] SQL TYPE IS schema_qualified_type_name AS LOCATOR
//
// <COBOL REF variable> ::= [ USAGE [ IS ] ] SQL TYPE IS reference_type
//
// <COBOL numeric type> ::=
// 		{ PIC | PICTURE } [ IS ] S <COBOL nines specification> [ USAGE [ IS ] ] DISPLAY SIGN LEADING SEPARATE
//
// <COBOL nines specification> ::=
// 		<COBOL nines> [ V [ <COBOL nines> ] ]
// 	|	V <COBOL nines>
//
// <COBOL integer type> ::= <COBOL binary integer>
//
// <COBOL binary integer> ::= { PIC | PICTURE } [ IS ] S<COBOL nines> [ USAGE [ IS ] ] BINARY
//
// <COBOL nines> ::= { 9 [ lit('(') unsigned_integer lit(')') ] }...
//
// //h3 20.6 <embedded SQL Fortran program> (p1025)
// ///h3
//
// //p
// Specify an <embedded SQL Fortran program>.
// ///p
//
// <embedded SQL Fortran program> ::= !! See the Syntax Rules.
//
// <Fortran variable definition> ::=
// 		<Fortran type specification> <Fortran host identifier> [ { lit(',') <Fortran host identifier> }... ]
//
// <Fortran host identifier> ::= !! See the Syntax Rules.
//
// <Fortran type specification> ::=
// 		CHARACTER [ lit('*') unsigned_integer ] [ CHARACTER SET [ IS ] character_set_name ]
// 	|	CHARACTER KIND = n [ lit('*') unsigned_integer ] [ CHARACTER SET [ IS ] character_set_name ]
// 	|	INTEGER
// 	|	REAL
// 	|	DOUBLE PRECISION
// 	|	LOGICAL
// 	|	<Fortran derived type specification>
//
// <Fortran derived type specification> ::=
// 		<Fortran CLOB variable>
// 	|	<Fortran BLOB variable>
// 	|	<Fortran user-defined type variable>
// 	|	<Fortran CLOB locator variable>
// 	|	<Fortran BLOB locator variable>
// 	|	<Fortran user-defined type locator variable>
// 	|	<Fortran array locator variable>
// 	|	<Fortran multiset locator variable>
// 	|	<Fortran REF variable>
//
// <Fortran CLOB variable> ::=
// 		SQL TYPE IS CLOB lit('(') large_object_length lit(')')
// 		[ CHARACTER SET [ IS ] character_set_name ]
//
// <Fortran BLOB variable> ::=
// 		SQL TYPE IS BLOB lit('(') large_object_length lit(')')
//
// <Fortran user-defined type variable> ::=
// 		SQL TYPE IS schema_qualified_type_name AS predefined_type
//
// <Fortran CLOB locator variable> ::= SQL TYPE IS CLOB AS LOCATOR
//
// <Fortran BLOB locator variable> ::= SQL TYPE IS BLOB AS LOCATOR
//
// <Fortran user-defined type locator variable> ::= SQL TYPE IS schema_qualified_type_name AS LOCATOR
//
// <Fortran array locator variable> ::= SQL TYPE IS array_type AS LOCATOR
//
// <Fortran multiset locator variable> ::= SQL TYPE IS multiset_type AS LOCATOR
//
// <Fortran REF variable> ::= SQL TYPE IS reference_type
//
// //h3 20.7 <embedded SQL MUMPS program> (p1030)
// ///h3
//
// //p
// Specify an <embedded SQL MUMPS program>.
// ///p
//
// <embedded SQL MUMPS program> ::= !! See the Syntax Rules.
//
// <MUMPS variable definition> ::=
// 		<MUMPS numeric variable> lit(';')
// 	|	<MUMPS character variable> lit(';')
// 	|	<MUMPS derived type specification> lit(';')
//
// <MUMPS character variable> ::=
// 		VARCHAR <MUMPS host identifier> <MUMPS length specification>
// 		[ { lit(',') <MUMPS host identifier> <MUMPS length specification> }... ]
//
// <MUMPS host identifier> ::= !! See the Syntax Rules.
//
// <MUMPS length specification> ::= lit('(') unsigned_integer lit(')')
//
// <MUMPS numeric variable> ::= <MUMPS type specification> <MUMPS host identifier> [ { lit(',') <MUMPS host identifier> }... ]
//
// <MUMPS type specification> ::=
// 		INT
// 	|	DEC >> -(lit('(') >> unsigned_integer >> -(lit(',') >> unsigned_integer) >> lit(')'))
// 	|	REAL
//
// <MUMPS derived type specification> ::=
// 		<MUMPS CLOB variable>
// 	|	<MUMPS BLOB variable>
// 	|	<MUMPS user-defined type variable>
// 	|	<MUMPS CLOB locator variable>
// 	|	<MUMPS BLOB locator variable>
// 	|	<MUMPS user-defined type locator variable>
// 	|	<MUMPS array locator variable>
// 	|	<MUMPS multiset locator variable>
// 	|	<MUMPS REF variable>
//
// <MUMPS CLOB variable> ::=
// 		SQL TYPE IS CLOB lit('(') large_object_length lit(')')
// 		[ CHARACTER SET [ IS ] character_set_name ]
//
// <MUMPS BLOB variable> ::= SQL TYPE IS BLOB lit('(') large_object_length lit(')')
//
// <MUMPS user-defined type variable> ::= SQL TYPE IS schema_qualified_type_name AS predefined_type
//
// <MUMPS CLOB locator variable> ::= SQL TYPE IS CLOB AS LOCATOR
//
// <MUMPS BLOB locator variable> ::= SQL TYPE IS BLOB AS LOCATOR
//
// <MUMPS user-defined type locator variable> ::= SQL TYPE IS schema_qualified_type_name AS LOCATOR
//
// <MUMPS array locator variable> ::= SQL TYPE IS array_type AS LOCATOR
//
// <MUMPS multiset locator variable> ::= SQL TYPE IS multiset_type AS LOCATOR
//
// <MUMPS REF variable> ::= SQL TYPE IS reference_type
//
// //h3 20.8 <embedded SQL Pascal program> (p1035)
// ///h3
//
// //p
// Specify an <embedded SQL Pascal program>.
// ///p
//
// <embedded SQL Pascal program> ::= !! See the Syntax Rules.
//
// <Pascal variable definition> ::=
// 		<Pascal host identifier> [ { lit(',') <Pascal host identifier> }... ] lit(':') <Pascal type specification> lit(';')
//
// <Pascal host identifier> ::= !! See the Syntax Rules.
//
// <Pascal type specification> ::=
// 		PACKED ARRAY lit('[') 1 lexeme[lit("..")] unsigned_integer lit(']') OF CHAR
// 		[ CHARACTER SET [ IS ] character_set_name ]
// 	|	INTEGER
// 	|	REAL
// 	|	CHAR [ CHARACTER SET [ IS ] character_set_name ]
// 	|	BOOLEAN
// 	|	<Pascal derived type specification>
//
// <Pascal derived type specification> ::=
// 		<Pascal CLOB variable>
// 	|	<Pascal BLOB variable>
// 	|	<Pascal user-defined type variable>
// 	|	<Pascal CLOB locator variable>
// 	|	<Pascal BLOB locator variable>
// 	|	<Pascal user-defined type locator variable>
// 	|	<Pascal array locator variable>
// 	|	<Pascal multiset locator variable>
// 	|	<Pascal REF variable>
//
// <Pascal CLOB variable> ::=
// 		SQL TYPE IS CLOB lit('(') large_object_length lit(')')
// 		[ CHARACTER SET [ IS ] character_set_name ]
//
// <Pascal BLOB variable> ::= SQL TYPE IS BLOB lit('(') large_object_length lit(')')
//
// <Pascal CLOB locator variable> ::= SQL TYPE IS CLOB AS LOCATOR
//
// <Pascal user-defined type variable> ::= SQL TYPE IS schema_qualified_type_name AS predefined_type
//
// <Pascal BLOB locator variable> ::= SQL TYPE IS BLOB AS LOCATOR
//
// <Pascal user-defined type locator variable> ::= SQL TYPE IS schema_qualified_type_name AS LOCATOR
//
// <Pascal array locator variable> ::= SQL TYPE IS array_type AS LOCATOR
//
// <Pascal multiset locator variable> ::= SQL TYPE IS multiset_type AS LOCATOR
//
// <Pascal REF variable> ::= SQL TYPE IS reference_type
//
// //h3 20.9 <embedded SQL PL/I program> (p1040)
// ///h3
//
// //p
// Specify an <embedded SQL PL/I program>.
// ///p
//
// <embedded SQL PL/I program> ::= !! See the Syntax Rules.
//
// <PL/I variable definition> ::=
// 		{ DCL | DECLARE } { <PL/I host identifier>
// 	|	lit('(') <PL/I host identifier> [ { lit(',') <PL/I host identifier> }... ] lit(')') }
// 		<PL/I type specification> [ <character representation>... ] lit(';')
//
// <PL/I host identifier> ::= !! See the Syntax Rules.
//
// <PL/I type specification> ::=
// 		{ CHAR | CHARACTER } [ VARYING ] lit('(') unsigned_integer lit(')') [ CHARACTER SET [ IS ] character_set_name ]
// 	|	<PL/I type fixed decimal> lit('(') unsigned_integer [ lit(',') unsigned_integer ] lit(')')
// 	|	<PL/I type fixed binary> [ lit('(') unsigned_integer lit(')') ]
// 	|	<PL/I type float binary> lit('(') unsigned_integer lit(')')
// 	|	<PL/I derived type specification>
//
// <PL/I derived type specification> ::=
// 		<PL/I CLOB variable>
// 	|	<PL/I BLOB variable>
// 	|	<PL/I user-defined type variable>
// 	|	<PL/I CLOB locator variable>
// 	|	<PL/I BLOB locator variable>
// 	|	<PL/I user-defined type locator variable>
// 	|	<PL/I array locator variable>
// 	|	<PL/I multiset locator variable>
// 	|	<PL/I REF variable>
//
// <PL/I CLOB variable> ::=
// 		SQL TYPE IS CLOB lit('(') large_object_length lit(')')
// 		[ CHARACTER SET [ IS ] character_set_name ]
//
// <PL/I BLOB variable> ::= SQL TYPE IS BLOB lit('(') large_object_length lit(')')
//
// <PL/I user-defined type variable> ::= SQL TYPE IS schema_qualified_type_name AS predefined_type
//
// <PL/I CLOB locator variable> ::= SQL TYPE IS CLOB AS LOCATOR
//
// <PL/I BLOB locator variable> ::= SQL TYPE IS BLOB AS LOCATOR
//
// <PL/I user-defined type locator variable> ::= SQL TYPE IS schema_qualified_type_name AS LOCATOR
//
// <PL/I array locator variable> ::= SQL TYPE IS array_type AS LOCATOR
//
// <PL/I multiset locator variable> ::= SQL TYPE IS multiset_type AS LOCATOR
//
// <PL/I REF variable> ::= SQL TYPE IS reference_type
//
// <PL/I type fixed decimal> ::=
// 		{ DEC | DECIMAL } FIXED
// 	|	FIXED { DEC | DECIMAL }
//
// <PL/I type fixed binary> ::=
// 		{ BIN | BINARY } FIXED
// 	|	FIXED { BIN | BINARY }
//
// <PL/I type float binary> ::=
// 		{ BIN | BINARY } FLOAT
// 	|	FLOAT { BIN | BINARY }
//
// //hr
// //h2 21 Direct invocation of SQL
// ///h2
//
// //h3 21.1 <direct SQL statement> (p1047)
// ///h3
//
// //p
// Specify direct execution of SQL.
// ///p
//
// <direct SQL statement> ::= <directly executable statement> lit(';')
//
// <directly executable statement> ::=
// 		<direct SQL data statement>
// 	|	<SQL schema statement>
// 	|	<SQL transaction statement>
// 	|	<SQL connection statement>
// 	|	<SQL session statement>
// 	|	<direct implementation-defined statement>
//
// <direct SQL data statement> ::=
// 		<delete statement: searched>
// 	|	<direct select statement: multiple rows>
// 	|	<insert statement>
// 	|	<update statement: searched>
// 	|	<merge statement>
// 	|	<temporary table declaration>
//
// <direct implementation-defined statement> ::= !! See the Syntax Rules.
//
// //h3 21.2 <direct select statement: multiple rows> (p1051)
// ///h3
//
// //p
// Specify a statement to retrieve multiple rows from a specified table.
// ///p
//
// <direct select statement: multiple rows> ::= cursor_specification
//
// //hr
// //h2 22 Diagnostics management
// //h3 22.1 <get diagnostics statement> (p1053)
// ///h3
//
// //p
// Get exception or completion condition information from a diagnostics area.
// ///p
//
// <get diagnostics statement> ::= GET DIAGNOSTICS <SQL diagnostics information>
//
// <SQL diagnostics information> ::= <statement information> | <condition information>
//
// <statement information> ::= <statement information item> [ { lit(',') <statement information item> }... ]
//
// <statement information item> ::= simple_target_specification lit('=') <statement information item name>
//
// <statement information item name> ::=
// 		NUMBER
// 	|	MORE
// 	|	COMMAND_FUNCTION
// 	|	COMMAND_FUNCTION_CODE
// 	|	DYNAMIC_FUNCTION
// 	|	DYNAMIC_FUNCTION_CODE
// 	|	ROW_COUNT
// 	|	TRANSACTIONS_COMMITTED
// 	|	TRANSACTIONS_ROLLED_BACK
// 	|	TRANSACTION_ACTIVE
//
// <condition information> ::=
// 		{ EXCEPTION | CONDITION } <condition number> <condition information item> [ { lit(',') <condition information item> }... ]
//
// <condition information item> ::= simple_target_specification lit('=') <condition information item name>
//
// <condition information item name> ::=
// 		CATALOG_NAME
// 	|	CLASS_ORIGIN
// 	|	COLUMN_NAME
// 	|	CONDITION_NUMBER
// 	|	CONNECTION_NAME
// 	|	CONSTRAINT_CATALOG
// 	|	CONSTRAINT_NAME
// 	|	CONSTRAINT_SCHEMA
// 	|	CURSOR_NAME
// 	|	MESSAGE_LENGTH
// 	|	MESSAGE_OCTET_LENGTH
// 	|	MESSAGE_TEXT
// 	|	PARAMETER_MODE
// 	|	PARAMETER_NAME
// 	|	PARAMETER_ORDINAL_POSITION
// 	|	RETURNED_SQLSTATE
// 	|	ROUTINE_CATALOG
// 	|	ROUTINE_NAME
// 	|	ROUTINE_SCHEMA
// 	|	SCHEMA_NAME
// 	|	SERVER_NAME
// 	|	SPECIFIC_NAME
// 	|	SUBCLASS_ORIGIN
// 	|	TABLE_NAME
// 	|	TRIGGER_CATALOG
// 	|	TRIGGER_NAME
// 	|	TRIGGER_SCHEMA;
//
// <condition number> ::= simple_value_specification
//
// //h3 22.2 Pushing and popping the diagnostics area stack (p1068)
// ///h3




		//------------------------------------------------------------------------------
		// Statement
		//------------------------------------------------------------------------------

		struct endofinput {
			template<typename Context>
			void operator()(Context& ctx) {
				//auto& g(get<parser_tag>(ctx).get());
				//g.finalize();
			}
		};

		const rule<struct file> file = "file";

		const auto file_def = /*subquery*/ lit('(') >> cursor_specification >> lit(')') >> -(keyword("as") >> identifier) >> eoi[endofinput{}];

		BOOST_SPIRIT_DEFINE(file);
	};
};

void CartoSQL::parse_sql(std::istream& is, std::ostream *msg)
{
	if (!is)
		throw std::runtime_error("Cannot read sql");
	namespace x3 = boost::spirit::x3;
	using Iterator = boost::spirit::istream_iterator;
	using PosIterator = boost::spirit::line_pos_iterator<Iterator>;
	cartosql_parser<PosIterator> g(*this, msg);
	std::vector<PosIterator> iters;
	Iterator f(is >> std::noskipws), l;
	PosIterator pf(f), pi(pf), pl;
	try {
		if (!x3::phrase_parse(pi, pl, x3::with<parser::parser_tag>(std::ref(g))[parser::file], skipper::skip))
			parse_error(pf, pi, pl);
	} catch (const x3::expectation_failure<PosIterator>& x) {
		parse_error(pf, x.where(), pl, x.which());
	}
	if (false)
		std::cerr << "finalizing..." << std::endl;
	try {
		g.finalize();
	} catch (const x3::expectation_failure<PosIterator>& x) {
		parse_error(pf, x.where(), pl, x.which());
	}
}
