//
// C++ Implementation: cartosqlparserexpr
//
// Description: CartoCSS SQL parser expressions
//
//
// Author: Thomas Sailer <t.sailer@alumni.ethz.ch>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cartosqlparser.hh"

#undef BOOST_SPIRIT_X3_DEBUG
#include <boost/fusion/include/vector.hpp>
#include <boost/fusion/include/make_deque.hpp>
#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/fusion/include/io.hpp>
#include <boost/fusion/include/for_each.hpp>
#include <boost/fusion/include/pop_front.hpp>
#include <boost/fusion/adapted.hpp>

#include <boost/variant.hpp>

namespace CartoSQLParser {

	namespace ascii = boost::spirit::x3::ascii;
	namespace fusion = boost::fusion;

	namespace parser {
		using x3::get;
		using x3::no_case;
		using x3::no_skip;
		using x3::lit;
		using x3::string;
		using x3::omit;
		using x3::lexeme;
		using x3::repeat;
		using x3::eps;
		using x3::eol;
		using x3::eoi;
		using x3::attr;
		using x3::with;
		using x3::_val;
		using x3::_attr;
		using x3::_where;
		using x3::_pass;
		using x3::ulong_;
		using x3::long_;
		using x3::double_;
		using ascii::char_;
		using ascii::alnum;
		using ascii::alpha;
		using ascii::digit;
		using ascii::xdigit;
		using ascii::space;
		using fusion::at_c;

		//-----------------------------------------------------------------------------
		// Operators
		//-----------------------------------------------------------------------------

		const struct funcaggregate_ : x3::symbols<CartoSQL::ExprFuncAggregate::func_t> {
			funcaggregate_() {
				add
					("count",                       CartoSQL::ExprFuncAggregate::func_t::count)
					("sum",                         CartoSQL::ExprFuncAggregate::func_t::sum)
					("avg",                         CartoSQL::ExprFuncAggregate::func_t::avg)
					("min",                         CartoSQL::ExprFuncAggregate::func_t::min)
					("max",                         CartoSQL::ExprFuncAggregate::func_t::max)
					("bit_and",                     CartoSQL::ExprFuncAggregate::func_t::bit_and)
					("bit_or",                      CartoSQL::ExprFuncAggregate::func_t::bit_or)
					("bool_and",                    CartoSQL::ExprFuncAggregate::func_t::bool_and)
					("bool_or",                     CartoSQL::ExprFuncAggregate::func_t::bool_or)
					("array_agg",                   CartoSQL::ExprFuncAggregate::func_t::array_agg)
					("string_agg",                  CartoSQL::ExprFuncAggregate::func_t::string_agg)
					("every",                       CartoSQL::ExprFuncAggregate::func_t::bool_and);
			}
		} funcaggregate;

		//-----------------------------------------------------------------------------
		// Operators (https://www.postgresql.org/docs/12/functions.html)
		//-----------------------------------------------------------------------------

		const rule<struct op_comp_equal> op_comp_equal = "op_comp_equal";
		const rule<struct op_comp_unequal> op_comp_unequal = "op_comp_unequal";
		const rule<struct op_comp_less> op_comp_less = "op_comp_less";
		const rule<struct op_comp_lessequal> op_comp_lessequal = "op_comp_lessequal";
		const rule<struct op_comp_greater> op_comp_greater = "op_comp_greater";
		const rule<struct op_comp_greaterequal> op_comp_greaterequal = "op_comp_greaterequal";
		const rule<struct op_add> op_add = "op_add";
		const rule<struct op_sub> op_sub = "op_sub";
		const rule<struct op_mul> op_mul = "op_mul";
		const rule<struct op_div> op_div = "op_div";
		const rule<struct op_mod> op_mod = "op_mod";
		const rule<struct op_exp> op_exp = "op_exp";
		const rule<struct op_squareroot> op_squareroot = "op_squareroot";
		const rule<struct op_cuberoot> op_cuberoot = "op_cuberoot";
		const rule<struct op_factorial> op_factorial = "op_factorial";
		const rule<struct op_factorialprefix> op_factorialprefix = "op_factorialprefix";
		const rule<struct op_absval> op_absval = "op_absval";
		const rule<struct op_bitwise_and> op_bitwise_and = "op_bitwise_and";
		const rule<struct op_bitwise_or> op_bitwise_or = "op_bitwise_or";
		const rule<struct op_bitwise_xor> op_bitwise_xor = "op_bitwise_xor";
		const rule<struct op_bitwise_not> op_bitwise_not = "op_bitwise_not";
		const rule<struct op_bitwise_shiftleft> op_bitwise_shiftleft = "op_bitwise_shiftleft";
		const rule<struct op_bitwise_shiftright> op_bitwise_shiftright = "op_bitwise_shiftright";
		const rule<struct op_stringconcat> op_stringconcat = "op_stringconcat";
		const rule<struct op_regex> op_regex = "op_regex";
		const rule<struct op_regexnocase> op_regexnocase = "op_regexnocase";
		const rule<struct op_regexnot> op_regexnot = "op_regexnot";
		const rule<struct op_regexnotnocase> op_regexnotnocase = "op_regexnotnocase";
		const rule<struct op_like> op_like = "op_like";
		const rule<struct op_ilike> op_ilike = "op_ilike";
		const rule<struct op_notlike> op_notlike = "op_notlike";
		const rule<struct op_notilike> op_notilike = "op_notilike";
		const rule<struct op_cast> op_cast = "op_cast";
		const rule<struct op_overlap> op_overlap = "op_overlap";

		const auto op_comp_equal_def = lexeme[lit('=') >> -(lit('='))];
		const auto op_comp_unequal_def = lexeme[(lit("<>") | lit("!="))];
		const auto op_comp_less_def = lexeme[lit('<') >> !char_("<=")];
		const auto op_comp_lessequal_def = lexeme[lit("<=")];
		const auto op_comp_greater_def = lexeme[lit('>') >> !char_(">=")];
		const auto op_comp_greaterequal_def = lexeme[lit(">=")];
		const auto op_add_def = lexeme[lit('+')];
		const auto op_sub_def = lexeme[lit('-') >> !char_('>')];
		const auto op_mul_def = lexeme[lit('*')];
		const auto op_div_def = lexeme[lit('/')];
		const auto op_mod_def = lexeme[lit('%')];
		const auto op_exp_def = lexeme[lit('^')];
		const auto op_squareroot_def = lexeme[lit("|/")];
		const auto op_cuberoot_def = lexeme[lit("||/")];
		const auto op_factorial_def = lexeme[lit('!') >> !char_("=!~")];
		const auto op_factorialprefix_def = lexeme[lit("!!")];
		const auto op_absval_def = lexeme[lit('@') >> !char_('>')];
		const auto op_bitwise_and_def = lexeme[lit('&') >> !char_('&')];
		const auto op_bitwise_or_def = lexeme[lit('|') >> !char_("|/")];
		const auto op_bitwise_xor_def = lexeme[lit('#')];
		const auto op_bitwise_not_def = lexeme[lit('~') >> !char_("~*")];
		const auto op_bitwise_shiftleft_def = lexeme[lit("<<")];
		const auto op_bitwise_shiftright_def = lexeme[lit(">>")];
		const auto op_stringconcat_def = lexeme[lit("||") >> !char_('/')];
		const auto op_regex_def = lexeme[lit('~') >> !char_("~*")];
		const auto op_regexnocase_def = lexeme[lit("~*")];
		const auto op_regexnot_def = lexeme[lit("!~") >> !char_("~*")];
		const auto op_regexnotnocase_def = lexeme[lit("!~*")];
		const auto op_like_def = lexeme[lit("~~") >> !char_('*')];
		const auto op_ilike_def = lexeme[lit("~~*")];
		const auto op_notlike_def = lexeme[lit("!~~") >> !char_('*')];
		const auto op_notilike_def = lexeme[lit("!~~*")];
		const auto op_cast_def = lexeme[lit("::")];
		const auto op_overlap_def = lexeme[lit("&&")];

		BOOST_SPIRIT_DEFINE(op_comp_equal, op_comp_unequal, op_comp_less, op_comp_lessequal,
				    op_comp_greater, op_comp_greaterequal, op_add, op_sub, op_mul, op_div,
				    op_mod, op_exp, op_squareroot, op_cuberoot, op_factorial,
				    op_factorialprefix, op_absval, op_bitwise_and, op_bitwise_or,
				    op_bitwise_xor, op_bitwise_not, op_bitwise_shiftleft,
				    op_bitwise_shiftright, op_stringconcat, op_regex, op_regexnocase,
				    op_regexnot, op_regexnotnocase, op_like, op_ilike, op_notlike, op_notilike,
				    op_cast, op_overlap);

		//-----------------------------------------------------------------------------
		// Expressions (https://www.sqlite.org/lang_expr.html)
		//-----------------------------------------------------------------------------

		struct action_notsupported {
			const char *m_msg;
			action_notsupported(const char *msg) : m_msg(msg) {}
			template<typename Context>
			void operator()(Context& ctx) {
				boost::throw_exception(x3::expectation_failure<iterator_type>(_where(ctx).begin(), m_msg));
			}
		};

		struct action_setfunc {
			ExprFunc::func_t m_func;
			action_setfunc(ExprFunc::func_t func) : m_func(func) {}
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx) = m_func;
			}
		};

		struct action_unaryop {
			template<typename Context>
			void operator()(Context& ctx) {
				typedef std::vector<ExprFunc::func_t> funcs_t;
				const funcs_t& funcs(at_c<0>(_attr(ctx)));
				Expr::const_ptr_t e(at_c<1>(_attr(ctx)));
				for (funcs_t::const_reverse_iterator fi(funcs.rbegin()), fe(funcs.rend()); fi != fe; ++fi) {
					Expr::exprlist_t el;
					el.push_back(e);
					e = Expr::ptr_t(new ExprFunc(*fi, el));
				}
				_val(ctx) = e;
			}
		};

		struct oppostfix {
			oppostfix(void) : m_func(ExprFunc::func_t::invalid) {}
			Expr::exprlist_t m_expr;
			ExprFunc::func_t m_func;
		};

		struct action_postfix_setfunc {
			ExprFunc::func_t m_func;
			action_postfix_setfunc(ExprFunc::func_t func) : m_func(func) {}
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx).m_func = m_func;
			}
		};

		struct action_expr3isnot {
			template<typename Context>
			void operator()(Context& ctx) {
				bool isnot(at_c<0>(_attr(ctx)));
				const oppostfix& postfix(at_c<1>(_attr(ctx)));
				_val(ctx) = postfix;
				if (isnot)
					_val(ctx).m_func = ExprFunc::notfunc(postfix.m_func);
			}
		};

		struct action_postfix_func {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx).m_func = _attr(ctx);
			}
		};

		struct action_postfix_expr {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx).m_expr.push_back(_attr(ctx));
			}
		};

		struct action_postfixop {
			template<typename Context>
			void operator()(Context& ctx) {
				Expr::const_ptr_t e(at_c<0>(_attr(ctx)));
				for (const oppostfix& postfix : at_c<1>(_attr(ctx))) {
					Expr::exprlist_t el;
					el.push_back(e);
					el.insert(el.end(), postfix.m_expr.begin(), postfix.m_expr.end());
					e = Expr::ptr_t(new ExprFunc(postfix.m_func, el));
				}
				_val(ctx) = e;
			}
		};

		struct action_prefixpostfixop {
			template<typename Context>
			void operator()(Context& ctx) {
				typedef std::vector<ExprFunc::func_t> funcs_t;
				const funcs_t& funcs(at_c<0>(_attr(ctx)));
				Expr::const_ptr_t e(at_c<1>(_attr(ctx)));
				for (funcs_t::const_reverse_iterator fi(funcs.rbegin()), fe(funcs.rend()); fi != fe; ++fi) {
					Expr::exprlist_t el;
					el.push_back(e);
					e = Expr::ptr_t(new ExprFunc(*fi, el));
				}
				for (const oppostfix& postfix : at_c<2>(_attr(ctx))) {
					Expr::exprlist_t el;
					el.push_back(e);
					el.insert(el.end(), postfix.m_expr.begin(), postfix.m_expr.end());
					e = Expr::ptr_t(new ExprFunc(postfix.m_func, el));
				}
				_val(ctx) = e;
			}
		};

		struct action_expr5between {
			template<typename Context>
			void operator()(Context& ctx) {
				if (at_c<0>(_attr(ctx))) {
					if (at_c<1>(_attr(ctx))) {
					}
				}
			}
		};

		struct action_expr5in {
			template<typename Context>
			void operator()(Context& ctx) {
				if (at_c<1>(_attr(ctx))) {
					const Expr::exprlist_t& el(*at_c<1>(_attr(ctx)));
					_val(ctx).m_expr.insert(_val(ctx).m_expr.end(), el.begin(), el.end());
				}
				_val(ctx).m_func = (at_c<0>(_attr(ctx))) ? ExprFunc::func_t::notinset : ExprFunc::func_t::inset;
			}
		};

		struct action_expr10cast {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx) = boost::intrusive_ptr<ExprOneChild>(new ExprCast(Expr::ptr_t(), _attr(ctx)));
			}
		};

		struct action_expr10collate {
			template<typename Context>
			void operator()(Context& ctx) {
				// FIXME
				_val(ctx) = boost::intrusive_ptr<ExprOneChild>();
				boost::throw_exception(x3::expectation_failure<iterator_type>(_where(ctx).begin(), "collate not supported"));
			}
		};

		struct action_expr10 {
			template<typename Context>
			void operator()(Context& ctx) {
				Expr::const_ptr_t e(at_c<0>(_attr(ctx)));
				for (const boost::intrusive_ptr<ExprOneChild>& postfix : at_c<1>(_attr(ctx))) {
					if (!postfix)
						continue;
					postfix->set_expr(e);
					e = postfix;
				}
				_val(ctx) = e;
			}
		};

		struct action_expr11_literal {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx) = Expr::ptr_t(new ExprValue(_attr(ctx)));
			}
		};

		struct action_expr11_cast {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx) = Expr::ptr_t(new ExprCast(at_c<0>(_attr(ctx)), at_c<1>(_attr(ctx))));
			}
		};

		struct action_functable_not {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx) = boost::intrusive_ptr<ExprFuncTable>(new ExprFuncTable(ExprFuncTable::func_t::notexists));
			}
		};

		struct action_functable_exists {
			template<typename Context>
			void operator()(Context& ctx) {
				if (!_val(ctx))
					_val(ctx) = boost::intrusive_ptr<ExprFuncTable>(new ExprFuncTable(ExprFuncTable::func_t::exists));
			}
		};

		struct action_functable_stmt {
			template<typename Context>
			void operator()(Context& ctx) {
				if (!_val(ctx))
					_val(ctx) = boost::intrusive_ptr<ExprFuncTable>(new ExprFuncTable(ExprFuncTable::func_t::scalar));
				_val(ctx)->set_expr(_attr(ctx));
			}
		};

		struct action_aggregate_function_invocation_countrows {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx) = boost::intrusive_ptr<ExprFuncAggregate>(new ExprFuncAggregate(ExprFuncAggregate::func_t::countrows));
			}
		};

		struct action_aggregate_function_invocation_func {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx) = boost::intrusive_ptr<ExprFuncAggregate>(new ExprFuncAggregate(_attr(ctx)));
			}
		};

		struct action_aggregate_function_invocation_distinct {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx)->set_distinct(true);
			}
		};

		struct action_aggregate_function_invocation_expr {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx)->set_expr(_attr(ctx));
			}
		};

		struct action_ordering_term_expr {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx).set_expr(_attr(ctx));
			}
		};

		struct action_ordering_term_dir {
			ExprTableSort::Term::direction_t m_dir;
			action_ordering_term_dir(ExprTableSort::Term::direction_t dir) : m_dir(dir) {}
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx).set_direction(m_dir);
			}
		};

		struct action_ordering_term_nulls {
			ExprTableSort::Term::nulls_t m_nulls;
			action_ordering_term_nulls(ExprTableSort::Term::nulls_t nulls) : m_nulls(nulls) {}
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx).set_nulls(m_nulls);
			}
		};

		struct action_ordering_term_finalize {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx).resolve_nulls();
			}
		};

		struct action_mapnik_variable {
			template<typename Context>
			void operator()(Context& ctx) {
				const std::string& s(_attr(ctx));
				_val(ctx) = Expr::const_ptr_t(new ExprVariable(s));
			}
		};

		const rule<struct exprbinaryop, ExprFunc::func_t> exprbinaryop = "exprbinaryop";
		const rule<struct exprpostfix, oppostfix> exprpostfix = "exprpostfix";
		const rule<struct expr1binaryop, ExprFunc::func_t> expr1binaryop = "expr1binaryop";
		const rule<struct expr1postfix, oppostfix> expr1postfix = "expr1postfix";
		const rule<struct expr1, Expr::const_ptr_t> expr1 = "expr1";
		const rule<struct expr2unaryop, ExprFunc::func_t> expr2unaryop = "expr2unaryop";
		const rule<struct expr2, Expr::const_ptr_t> expr2 = "expr2";
		const rule<struct expr_optionalnot, bool> expr_optionalnot = "expr_optionalnot";
		const rule<struct expr3isnot, oppostfix> expr3isnot = "expr3isnot";
		const rule<struct expr3postfix, oppostfix> expr3postfix = "expr3postfix";
		const rule<struct expr3, Expr::const_ptr_t> expr3 = "expr3";
		const rule<struct expr4binaryop, ExprFunc::func_t> expr4binaryop = "expr4binaryop";
		const rule<struct expr4postfix, oppostfix> expr4postfix = "expr4postfix";
		const rule<struct expr4, Expr::const_ptr_t> expr4 = "expr4";
		const rule<struct expr_optionalsymmetric, bool> expr_optionalsymmetric = "expr_optionalsymmetric";
		const rule<struct expr5binaryescop, ExprFunc::func_t> expr5binaryescop = "expr5binaryescop";
		const rule<struct expr5binaryop, ExprFunc::func_t> expr5binaryop = "expr5binaryop";
		const rule<struct expr5postfix, oppostfix> expr5postfix = "expr5postfix";
		const rule<struct expr5, Expr::const_ptr_t> expr5 = "expr5";
		const rule<struct expr6unaryop, ExprFunc::func_t> expr6unaryop = "expr6unaryop";
		const rule<struct expr6unarypostfixop, ExprFunc::func_t> expr6unarypostfixop = "expr6unarypostfixop";
		const rule<struct expr6binaryop, ExprFunc::func_t> expr6binaryop = "expr6binaryop";
		const rule<struct expr6postfix, oppostfix> expr6postfix = "expr5postfix";
		const rule<struct expr6, Expr::const_ptr_t> expr6 = "expr6";
		const rule<struct expr7binaryop, ExprFunc::func_t> expr7binaryop = "expr7binaryop";
		const rule<struct expr7postfix, oppostfix> expr7postfix = "expr7postfix";
		const rule<struct expr7, Expr::const_ptr_t> expr7 = "expr7";
		const rule<struct expr8binaryop, ExprFunc::func_t> expr8binaryop = "expr8binaryop";
		const rule<struct expr8postfix, oppostfix> expr8postfix = "expr8postfix";
		const rule<struct expr8, Expr::const_ptr_t> expr8 = "expr8";
		const rule<struct expr9unaryop, ExprFunc::func_t> expr9unaryop = "expr9unaryop";
		const rule<struct expr9, Expr::const_ptr_t> expr9 = "expr9";
		const rule<struct expr10postfix, boost::intrusive_ptr<ExprOneChild> > expr10postfix = "expr10postfix";
		const rule<struct expr10, Expr::const_ptr_t> expr10 = "expr10";
		const rule<struct expr11, Expr::const_ptr_t> expr11 = "expr11";
		const rule<struct functable, boost::intrusive_ptr<ExprFuncTable> > functable = "functable";
		const rule<struct aggregate_function_invocation, boost::intrusive_ptr<ExprFuncAggregate> > aggregate_function_invocation = "aggregate_function_invocation";
		const rule<struct window_function_invocation> window_function_invocation = "window_function_invocation";
		const rule<struct filter_clause> filter_clause = "filter_clause";
		const rule<struct ordering_options> ordering_options = "ordering_options";
		const rule<struct frame_spec> frame_spec = "frame_spec";
		const rule<struct raise_function> raise_function = "raise_function";
		const rule<struct over_clause> over_clause = "over_clause";
		const rule<struct mapnik_variable, Expr::const_ptr_t> mapnik_variable = "mapnik_variable";
		const rule<struct ordering_term, ExprTableSort::Term> ordering_term = "ordering_term";
		const window_defn_type window_defn INIT_PRIORITY(200) = "window_defn";
		const ordering_terms_type ordering_terms INIT_PRIORITY(200) = "ordering_terms";
		const expr_type expr INIT_PRIORITY(200) = "expr";

		const auto exprbinaryop_def =
			keyword("or")[action_setval<ExprFunc::func_t>{ExprFunc::func_t::logicalor}];

		const auto exprpostfix_def =
			exprbinaryop[action_postfix_func{}] >> expr1[action_postfix_expr{}];

		const auto expr_def =
	        	(expr1 >> *exprpostfix)[action_postfixop{}];

		const auto expr1binaryop_def =
			keyword("and")[action_setval<ExprFunc::func_t>{ExprFunc::func_t::logicaland}];

		const auto expr1postfix_def =
			expr1binaryop[action_postfix_func{}] >> expr2[action_postfix_expr{}];

		const auto expr1_def =
			(expr2 >> *expr1postfix)[action_postfixop{}];

		const auto expr2unaryop_def =
			keyword("not")[action_setval<ExprFunc::func_t>{ExprFunc::func_t::logicalnot}];

		const auto expr2_def =
			(*expr2unaryop >> expr3)[action_unaryop{}];

		const auto expr_optionalnot_def =
			eps[action_setval<bool>{false}] >> -(keyword("not")[action_setval<bool>{true}]);

		const auto expr3isnot_def =
			(keyword("distinct")[action_postfix_setfunc{ExprFunc::func_t::isdistinct}] >
			 keyword("from") >> expr4[action_postfix_expr{}])
			| keyword("null")[action_postfix_setfunc{ExprFunc::func_t::isnull}]
			| keyword("true")[action_postfix_setfunc{ExprFunc::func_t::istrue}]
			| keyword("false")[action_postfix_setfunc{ExprFunc::func_t::isfalse}]
			| keyword("unknown")[action_postfix_setfunc{ExprFunc::func_t::isunknown}];

		const auto expr3postfix_def =
			keyword("isnull")[action_postfix_setfunc{ExprFunc::func_t::isnull}]
			| keyword("notnull")[action_postfix_setfunc{ExprFunc::func_t::isnotnull}]
			| (keyword("is") >> expr_optionalnot >> expr3isnot)[action_expr3isnot{}];

		const auto expr3_def =
			(expr4 >> *expr3postfix)[action_postfixop{}];

		const auto expr4binaryop_def =
			op_comp_equal[action_setval<ExprFunc::func_t>{ExprFunc::func_t::equal}]
			| op_comp_unequal[action_setval<ExprFunc::func_t>{ExprFunc::func_t::unequal}]
			| op_comp_less[action_setval<ExprFunc::func_t>{ExprFunc::func_t::lessthan}]
			| op_comp_lessequal[action_setval<ExprFunc::func_t>{ExprFunc::func_t::lessorequal}]
			| op_comp_greater[action_setval<ExprFunc::func_t>{ExprFunc::func_t::greaterthan}]
			| op_comp_greaterequal[action_setval<ExprFunc::func_t>{ExprFunc::func_t::greaterorequal}];

		const auto expr4postfix_def =
			expr4binaryop[action_postfix_func{}] >> expr5[action_postfix_expr{}];

		const auto expr4_def =
			(expr5 >> *expr4postfix)[action_postfixop{}];

		const auto expr_optionalsymmetric_def =
			eps[action_setval<bool>{false}] >> -(keyword("symmetric")[action_setval<bool>{true}]);

		const auto expr5binaryescop_def =
			keyword("like")[action_setval<ExprFunc::func_t>{ExprFunc::func_t::like}]
			| keyword("ilike")[action_setval<ExprFunc::func_t>{ExprFunc::func_t::ilike}]
			| keyword("similar")[action_setval<ExprFunc::func_t>{ExprFunc::func_t::similar}];

		const auto expr5binaryop_def =
			op_like[action_setval<ExprFunc::func_t>{ExprFunc::func_t::like}]
			| op_ilike[action_setval<ExprFunc::func_t>{ExprFunc::func_t::ilike}]
			| op_notlike[action_setval<ExprFunc::func_t>{ExprFunc::func_t::notlike}]
			| op_notilike[action_setval<ExprFunc::func_t>{ExprFunc::func_t::notilike}];

		const auto expr5postfix_def =
			(expr_optionalnot >> keyword("between") >> expr_optionalsymmetric >> expr6[action_postfix_expr{}] >>
			 keyword("and") >> expr6[action_postfix_expr{}])[action_expr5between{}]
			| (expr5binaryescop[action_postfix_func{}] >> expr6[action_postfix_expr{}] >> -(keyword("escape") >> expr6[action_postfix_expr{}]))
			| (expr5binaryop[action_postfix_func{}] >> expr6[action_postfix_expr{}])
			| (expr_optionalnot >> keyword("in") >> lit('(') >> -(func_exprlist) >> lit(')'))[action_expr5in{}];

		const auto expr5_def =
			(expr6 >> *expr5postfix)[action_postfixop{}];

		const auto expr6unaryop_def =
			op_factorialprefix[action_setval<ExprFunc::func_t>{ExprFunc::func_t::like}]
			| op_absval[action_setval<ExprFunc::func_t>{ExprFunc::func_t::abs}]
			| op_bitwise_not[action_setval<ExprFunc::func_t>{ExprFunc::func_t::bitwisenot}];

		const auto expr6unarypostfixop_def =
			op_factorial[action_setval<ExprFunc::func_t>{ExprFunc::func_t::factorial}];

		const auto expr6binaryop_def =
			op_exp[action_setval<ExprFunc::func_t>{ExprFunc::func_t::pow}]
			| op_squareroot[action_setval<ExprFunc::func_t>{ExprFunc::func_t::sqrt}]
			| op_cuberoot[action_setval<ExprFunc::func_t>{ExprFunc::func_t::cbrt}]
			| op_bitwise_and[action_setval<ExprFunc::func_t>{ExprFunc::func_t::bitwiseand}]
			| op_bitwise_or[action_setval<ExprFunc::func_t>{ExprFunc::func_t::bitwiseor}]
			| op_bitwise_xor[action_setval<ExprFunc::func_t>{ExprFunc::func_t::bitwisexor}]
			| op_bitwise_shiftleft[action_setval<ExprFunc::func_t>{ExprFunc::func_t::bitwiseshiftleft}]
			| op_bitwise_shiftright[action_setval<ExprFunc::func_t>{ExprFunc::func_t::bitwiseshiftright}]
			| op_stringconcat[action_setval<ExprFunc::func_t>{ExprFunc::func_t::stringconcat}]
			| op_regex[action_setval<ExprFunc::func_t>{ExprFunc::func_t::regex}]
			| op_regexnocase[action_setval<ExprFunc::func_t>{ExprFunc::func_t::regexnocase}]
			| op_regexnot[action_setval<ExprFunc::func_t>{ExprFunc::func_t::notregex}]
			| op_regexnotnocase[action_setval<ExprFunc::func_t>{ExprFunc::func_t::notregexnocase}]
			| op_overlap[action_setval<ExprFunc::func_t>{ExprFunc::func_t::overlap}];

		const auto expr6postfix_def =
			(expr6unarypostfixop[action_postfix_func{}])
			| (expr6binaryop[action_postfix_func{}] >> expr7[action_postfix_expr{}]);

		const auto expr6_def =
			(*expr6unaryop >> expr7 >> *expr6postfix)[action_prefixpostfixop{}];

		const auto expr7binaryop_def =
			op_add[action_setval<ExprFunc::func_t>{ExprFunc::func_t::binaryplus}]
			| op_sub[action_setval<ExprFunc::func_t>{ExprFunc::func_t::binaryminus}];

		const auto expr7postfix_def =
			expr7binaryop[action_postfix_func{}] >> expr8[action_postfix_expr{}];

		const auto expr7_def =
			(expr8 >> *expr7postfix)[action_postfixop{}];

		const auto expr8binaryop_def =
			op_mul[action_setval<ExprFunc::func_t>{ExprFunc::func_t::binarymul}]
			| op_div[action_setval<ExprFunc::func_t>{ExprFunc::func_t::binarydiv}]
			| op_mod[action_setval<ExprFunc::func_t>{ExprFunc::func_t::binarymod}];

		const auto expr8postfix_def =
			expr8binaryop[action_postfix_func{}] >> expr9[action_postfix_expr{}];

		const auto expr8_def =
			(expr9 >> *expr8postfix)[action_postfixop{}];

		const auto expr9unaryop_def =
			op_add[action_setval<ExprFunc::func_t>{ExprFunc::func_t::unaryplus}]
			| op_sub[action_setval<ExprFunc::func_t>{ExprFunc::func_t::unaryminus}];

		const auto expr9_def =
			(*expr9unaryop >> expr10)[action_unaryop{}];

		const auto expr10postfix_def =
			(op_cast >> type_name)[action_expr10cast{}]
			| (keyword("collate") > identifier)[action_expr10collate{}];

		const auto expr10_def =
			(expr11 >> *expr10postfix)[action_expr10{}];

		const auto expr11_def =
			literal[action_expr11_literal{}]
			| func_math[action_copy{}]
			| func_string[action_copy{}]
			| func_conditional[action_copy{}]
			| func_array[action_copy{}]
			| func_gis[action_copy{}]
			| aggregate_function_invocation[action_copy{}]
			| window_function_invocation[action_notsupported{"window function not supported"}]
			| (keyword("cast") > lit('(') >> expr > keyword("as") > type_name >> lit(')'))[action_expr11_cast{}]
			| (lit('(') >> expr >> lit(')'))[action_copy{}]
			| functable[action_copy{}]
			| raise_function[action_notsupported{"raise function not supported"}]
			| lexeme[lit('!') >> mapnik_variable >> lit('!')][action_copy{}]
			| column_name[action_copy{}]
			;

		const auto functable_def =
			-(-(keyword("not")[action_functable_not{}]) >> keyword("exists")[action_functable_exists{}]) >>
			lit('(') >> select_stmt[action_functable_stmt{}] >> lit(')');

		const auto aggregate_function_invocation_def =
			(keyword("count") >> lit('(') >> lit('*') > lit(')'))[action_aggregate_function_invocation_countrows{}]
			| (lexeme[no_case[funcaggregate] >> !char_("0-9a-zA-Z_")][action_aggregate_function_invocation_func{}] >> lit('(') >>
			   -(-(keyword("distinct")[action_aggregate_function_invocation_distinct{}]) >>
			     func_exprlist[action_aggregate_function_invocation_expr{}] >> lit(')')) >>
			   -(filter_clause[action_notsupported{"filter clause not supported"}]));

		const auto window_function_invocation_def =
			(keyword("cume_dist") // built in window functions
			 | keyword("dense_rank")
			 | keyword("first_value")
			 | keyword("lag")
			 | keyword("last_value")
			 | keyword("lead")
			 | keyword("nth_value")
			 | keyword("ntile")
			 | keyword("percent_rank")
			 | keyword("rank")
			 | keyword("row_number")
			 // aggregate window functions
			 | keyword("avg")
			 | keyword("count")
			 | keyword("group_concat")
			 | keyword("max")
			 | keyword("min")
			 | keyword("sum")
			 | keyword("total")) > lit('(') >>
			-(lit('*')
			  | func_exprlist) >>
			lit(')') >> -(filter_clause) >> keyword("over") >
			(window_defn | omit[identifier]);

		const auto filter_clause_def =
			keyword("filter") > lit('(') > keyword("where") > expr > lit(')');

		const auto window_defn_def =
			lit('(') >> -(omit[identifier]) >>
			-(keyword("partition") > keyword("by") > func_exprlist) >>
			-(keyword("order") > keyword("by") > omit[ordering_terms]) >>
			-(frame_spec) >> lit(')');

		// const auto ordering_term_def =
		// 	expr >> -(keyword("collate") > omit[identifier]) >>
		// 	-(keyword("asc") | keyword("desc")) >>
		// 	-(keyword("nulls") > (keyword("first") | keyword("last")));

		const auto ordering_options_def =
			(keyword("collate") > omit[identifier])
			| keyword("asc")
			| keyword("desc")
			| (keyword("nulls") > (keyword("first") | keyword("last")));

		const auto ordering_term_def =
			expr[action_ordering_term_expr{}] >>
			*((keyword("collate") > omit[identifier])
			  | keyword("asc")[action_ordering_term_dir{ExprTableSort::Term::direction_t::ascending}]
			  | keyword("desc")[action_ordering_term_dir{ExprTableSort::Term::direction_t::descending}]
			  | (keyword("nulls") >
			     (keyword("first")[action_ordering_term_nulls{ExprTableSort::Term::nulls_t::first}]
			      | keyword("last")[action_ordering_term_nulls{ExprTableSort::Term::nulls_t::last}]))) >>
			eps[action_ordering_term_finalize{}];

		const auto ordering_terms_def =
			ordering_term[action_append{}] % lit(',');

		const auto frame_spec_def =
			(keyword("range")
			 | keyword("rows")
			 | keyword("groups")) >>
			((keyword("unbounded") >> keyword("preceding"))
			 | (keyword("current") >> keyword("row"))
			 | (keyword("between") >>
			    ((keyword("unbounded") >> keyword("preceding"))
			     | (keyword("current") >> keyword("row"))
			     | (expr >> (keyword("preceding") | keyword("following")))) >> keyword("and") >>
			    ((keyword("unbounded") >> keyword("following"))
			     | (keyword("current") >> keyword("row"))
			     | (expr >> (keyword("preceding") | keyword("following")))))
			 | (expr >> keyword("preceding"))) >>
			-(keyword("exclude") >> (keyword("ties")
						 | keyword("group")
						 | (keyword("current") >> keyword("row"))
						 | (keyword("no") >> keyword("others"))));

		const auto raise_function_def =
			keyword("raise") >> lit('(') >>
			(keyword("ignore")
			 | ((keyword("rollback")
			     | keyword("abort")
			     | keyword("fail")) >> lit(',') >> omit[string_literal])) >>
			lit(')');

		const auto over_clause_def =
			keyword("over") >>
			(omit[identifier] | window_defn);

		const auto mapnik_variable_def =
			(+char_("A-Za-z0-9_"))[action_mapnik_variable{}];
			// keyword("scale_denominator")[action_setval<Value>{Value(1.0/0.001/0.28)}]
			// | keyword("pixel_width")[action_setval<Value>{Value(static_cast<int64_t>(1))}]
			// | keyword("pixel_height")[action_setval<Value>{Value(static_cast<int64_t>(1))}]
			// | keyword("bbox")[action_setval<Value>{Value()}];

		BOOST_SPIRIT_DEFINE(exprbinaryop, exprpostfix, expr1binaryop, expr1postfix, expr1,
				    expr2unaryop, expr2, expr_optionalnot, expr3postfix, expr3isnot, expr3,
				    expr4binaryop, expr4postfix, expr4, expr_optionalsymmetric,
				    expr5binaryescop, expr5binaryop, expr5postfix, expr5,
				    expr6unaryop, expr6unarypostfixop, expr6binaryop, expr6postfix, expr6,
				    expr7binaryop, expr7postfix, expr7, expr8binaryop, expr8postfix, expr8,
				    expr9unaryop, expr9, expr10postfix, expr10, expr11, functable,
				    ordering_options, aggregate_function_invocation, window_function_invocation,
				    filter_clause, frame_spec, raise_function, over_clause, mapnik_variable,
				    ordering_term,
				    window_defn, ordering_terms, expr);

		BOOST_SPIRIT_INSTANTIATE(window_defn_type, iterator_type, context_type);
		BOOST_SPIRIT_INSTANTIATE(ordering_terms_type, iterator_type, context_type);
		BOOST_SPIRIT_INSTANTIATE(expr_type, iterator_type, context_type);

#if SPIRIT_X3_VERSION < 0x3006
		template bool parse_rule<iterator_type, context_type, x3::unused_type const>(ordering_terms_type, iterator_type&, iterator_type const&, context_type const&, x3::unused_type const&);
		template bool parse_rule<iterator_type, context_type, x3::unused_type const>(window_defn_type, iterator_type&, iterator_type const&, context_type const&, x3::unused_type const&);
		template bool parse_rule<iterator_type, context_type, x3::unused_type const>(expr_type, iterator_type&, iterator_type const&, context_type const&, x3::unused_type const&);
		template bool parse_rule<iterator_type, context_type, x3::unused_type>(expr_type, iterator_type&, iterator_type const&, context_type const&, x3::unused_type&);
#endif

	};
};
