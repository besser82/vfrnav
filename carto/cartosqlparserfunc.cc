//
// C++ Implementation: cartosqlparserfunc
//
// Description: CartoCSS SQL parser function invocations
//
//
// Author: Thomas Sailer <t.sailer@alumni.ethz.ch>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cartosqlparser.hh"

#undef BOOST_SPIRIT_X3_DEBUG
#include <boost/fusion/include/vector.hpp>
#include <boost/fusion/include/make_deque.hpp>
#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/fusion/include/io.hpp>
#include <boost/fusion/include/for_each.hpp>
#include <boost/fusion/include/pop_front.hpp>
#include <boost/fusion/adapted.hpp>

#include <boost/variant.hpp>

namespace CartoSQLParser {

	namespace ascii = boost::spirit::x3::ascii;
	namespace fusion = boost::fusion;

	namespace parser {
		using x3::get;
		using x3::no_case;
		using x3::no_skip;
		using x3::lit;
		using x3::string;
		using x3::omit;
		using x3::lexeme;
		using x3::repeat;
		using x3::eps;
		using x3::eol;
		using x3::eoi;
		using x3::attr;
		using x3::with;
		using x3::_val;
		using x3::_attr;
		using x3::_where;
		using x3::_pass;
		using x3::ulong_;
		using x3::long_;
		using x3::double_;
		using ascii::char_;
		using ascii::alnum;
		using ascii::alpha;
		using ascii::digit;
		using ascii::xdigit;
		using ascii::space;
		using fusion::at_c;

		//-----------------------------------------------------------------------------
		// Operators
		//-----------------------------------------------------------------------------

		const struct funcmathzeroarg_ : x3::symbols<CartoSQL::ExprFunc::func_t> {
			funcmathzeroarg_() {
				add
					("pi",                          CartoSQL::ExprFunc::func_t::pi);
			}
		} funcmathzeroarg;

		const struct funcmathonearg_ : x3::symbols<CartoSQL::ExprFunc::func_t> {
			funcmathonearg_() {
				add
					("abs",                         CartoSQL::ExprFunc::func_t::abs)
					("cbrt",                        CartoSQL::ExprFunc::func_t::cbrt)
					("ceil",                        CartoSQL::ExprFunc::func_t::ceil)
					("ceiling",                     CartoSQL::ExprFunc::func_t::ceil)
					("degrees",                     CartoSQL::ExprFunc::func_t::degrees)
					("exp",                         CartoSQL::ExprFunc::func_t::exp)
					("floor",                       CartoSQL::ExprFunc::func_t::floor)
					("ln",                          CartoSQL::ExprFunc::func_t::ln)
					("log10",                       CartoSQL::ExprFunc::func_t::log10)
					("radians",                     CartoSQL::ExprFunc::func_t::radians)
					("round",                       CartoSQL::ExprFunc::func_t::round)
					("sign",                        CartoSQL::ExprFunc::func_t::sign)
					("sqrt",                        CartoSQL::ExprFunc::func_t::sqrt)
					("trunc",                       CartoSQL::ExprFunc::func_t::trunc)
					("acos",                        CartoSQL::ExprFunc::func_t::acos)
					("asin",                        CartoSQL::ExprFunc::func_t::asin)
					("atan",                        CartoSQL::ExprFunc::func_t::atan)
					("cos",                         CartoSQL::ExprFunc::func_t::cos)
					("cot",                         CartoSQL::ExprFunc::func_t::cot)
					("sin",                         CartoSQL::ExprFunc::func_t::sin)
					("tan",                         CartoSQL::ExprFunc::func_t::tan)
					("acosd",                       CartoSQL::ExprFunc::func_t::acosd)
					("asind",                       CartoSQL::ExprFunc::func_t::asind)
					("atand",                       CartoSQL::ExprFunc::func_t::atand)
					("cosd",                        CartoSQL::ExprFunc::func_t::cosd)
					("cotd",                        CartoSQL::ExprFunc::func_t::cotd)
					("sind",                        CartoSQL::ExprFunc::func_t::sind)
					("tand",                        CartoSQL::ExprFunc::func_t::tand)
					("sinh",                        CartoSQL::ExprFunc::func_t::sinh)
					("cosh",                        CartoSQL::ExprFunc::func_t::cosh)
					("tanh",                        CartoSQL::ExprFunc::func_t::tanh)
					("asinh",                       CartoSQL::ExprFunc::func_t::asinh)
					("acosh",                       CartoSQL::ExprFunc::func_t::acosh)
					("atanh",                       CartoSQL::ExprFunc::func_t::atanh);
			}
		} funcmathonearg;

		const struct funcmathtwoarg_ : x3::symbols<CartoSQL::ExprFunc::func_t> {
			funcmathtwoarg_() {
				add
					("div",                         CartoSQL::ExprFunc::func_t::div)
					("mod",                         CartoSQL::ExprFunc::func_t::binarymod)
					("pow",                         CartoSQL::ExprFunc::func_t::pow)
					("atan2",                       CartoSQL::ExprFunc::func_t::atan2)
					("atan2d",                      CartoSQL::ExprFunc::func_t::atan2d);
			}
		} funcmathtwoarg;

		const struct funcstringonearg_ : x3::symbols<CartoSQL::ExprFunc::func_t> {
			funcstringonearg_() {
				add
					("ascii",                       CartoSQL::ExprFunc::func_t::ascii)
					("bit_length",                  CartoSQL::ExprFunc::func_t::bit_length)
					("char_length",                 CartoSQL::ExprFunc::func_t::char_length)
					("character_length",            CartoSQL::ExprFunc::func_t::char_length)
					("chr",                         CartoSQL::ExprFunc::func_t::chr)
					("length",                      CartoSQL::ExprFunc::func_t::length)
					("lower",                       CartoSQL::ExprFunc::func_t::lower)
					("md5",                         CartoSQL::ExprFunc::func_t::md5)
					("octet_length",                CartoSQL::ExprFunc::func_t::octet_length)
					("upper",                       CartoSQL::ExprFunc::func_t::upper);
			}
		} funcstringonearg;

		const struct funcblrtrim_ : x3::symbols<CartoSQL::ExprFunc::func_t> {
			funcblrtrim_() {
				add
					("btrim",                       CartoSQL::ExprFunc::func_t::btrim)
					("ltrim",                       CartoSQL::ExprFunc::func_t::ltrim)
					("rtrim",                       CartoSQL::ExprFunc::func_t::rtrim);
			}
		} funcblrtrim;

		const struct funcstringtwoarg_ : x3::symbols<CartoSQL::ExprFunc::func_t> {
			funcstringtwoarg_() {
				add
					("left",                        CartoSQL::ExprFunc::func_t::left)
					("right",                       CartoSQL::ExprFunc::func_t::right)
					("strpos",                      CartoSQL::ExprFunc::func_t::strpos);
			}
		} funcstringtwoarg;

		//-----------------------------------------------------------------------------
		// Mathematical Functions (https://www.postgresql.org/docs/12/functions-math.html)
		//-----------------------------------------------------------------------------

		struct exprlist : public ExprFunc::exprlist_t {
			template<typename T>
			inline void push_back(const T& x) {
				boost::fusion::for_each(x, [this](const auto& x) { this->push_back(x); });
			}
		};

		template<>
		inline void exprlist::push_back(const Expr::const_ptr_t& x) {
			if (x)
				ExprFunc::exprlist_t::push_back(x);
		}

		template<>
		inline void exprlist::push_back(const Expr::exprlist_t& e) {
			for (const Expr::const_ptr_t& x : e)
				push_back(x);
		}

		struct action_func_op {
			ExprFunc::func_t m_func;
			action_func_op(ExprFunc::func_t func) : m_func(func) {}
			template<typename Context>
			void operator()(Context& ctx) {
			        exprlist e;
				e.push_back(_attr(ctx));
				_val(ctx) = Expr::ptr_t(new ExprFunc(m_func, e));
			}
		};

		struct action_func {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx) = func(_attr(ctx));
			}
			template<typename T>
			Expr::const_ptr_t func(const T& x) {
				exprlist e;
				e.push_back(boost::fusion::pop_front(x));
				return Expr::ptr_t(new ExprFunc(at_c<0>(x), e));
			}
		};

		template<>
		Expr::const_ptr_t action_func::func(const ExprFunc::func_t& func) {
			return Expr::ptr_t(new ExprFunc(func, ExprFunc::exprlist_t()));
		}

		struct action_funcmathlog {
			template<typename Context>
			void operator()(Context& ctx) {
				ExprFunc::exprlist_t e;
				ExprFunc::func_t func(ExprFunc::func_t::log10);
				e.push_back(at_c<0>(_attr(ctx)));
				if (at_c<1>(_attr(ctx))) {
					e.push_back(at_c<1>(_attr(ctx)));
					func = ExprFunc::func_t::log;
				}
				_val(ctx) = Expr::ptr_t(new ExprFunc(func, e));
			}
		};

		const rule<struct func_optionalarg, Expr::const_ptr_t> func_optionalarg = "func_optionalarg";
		const func_math_type func_math INIT_PRIORITY(200) = "func_math";

		const auto func_optionalarg_def =
			-(lit(',') > expr[action_copy{}]);

		const auto func_math_def =
			(lexeme[no_case[funcmathzeroarg] >> !char_("0-9a-zA-Z_")] >
			 lit('(') > lit(')'))[action_func{}]
			| (lexeme[no_case[funcmathonearg] >> !char_("0-9a-zA-Z_")] >
			 lit('(') > expr > lit(')'))[action_func{}]
			| (lexeme[no_case[funcmathtwoarg] >> !char_("0-9a-zA-Z_")] >
			 lit('(') > expr > lit(',') > expr > lit(')'))[action_func{}]
			| (keyword("log") > lit('(') > expr > func_optionalarg > lit(')'))[action_funcmathlog{}];

		BOOST_SPIRIT_DEFINE(func_optionalarg, func_math);

		BOOST_SPIRIT_INSTANTIATE(func_math_type, iterator_type, context_type);

		//-----------------------------------------------------------------------------
		// String Functions (https://www.postgresql.org/docs/12/functions-string.html)
		//-----------------------------------------------------------------------------

		struct action_defaultspace {
			template<typename Context>
			void operator()(Context& ctx) {
				if (!_val(ctx))
					_val(ctx) = Expr::ptr_t(new ExprValue(Value(" ")));
			}
		};

		struct action_defaultone {
			template<typename Context>
			void operator()(Context& ctx) {
				if (!_val(ctx))
					_val(ctx) = Expr::ptr_t(new ExprValue(Value(static_cast<int64_t>(1))));
			}
		};

		struct action_appendifnotnull {
			template<typename Context>
			void operator()(Context& ctx) {
				if (_attr(ctx))
					_val(ctx).push_back(_attr(ctx));
			}
		};

		struct action_trimblt_func {
			ExprFunc::func_t m_func;
			action_trimblt_func(ExprFunc::func_t func) : m_func(func) {}
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx) = m_func;
			}
		};

		struct action_functwoarg_reverse {
			template<typename Context>
			void operator()(Context& ctx) {
				ExprFunc::exprlist_t e;
				e.push_back(at_c<2>(_attr(ctx)));
				e.push_back(at_c<1>(_attr(ctx)));
				_val(ctx) = Expr::ptr_t(new ExprFunc(at_c<0>(_attr(ctx)), e));
			}
		};

		struct action_func_overlay {
			template<typename Context>
			void operator()(Context& ctx) {
				ExprFunc::exprlist_t e;
				e.push_back(at_c<0>(_attr(ctx)));
				e.push_back(at_c<1>(_attr(ctx)));
				e.push_back(at_c<2>(_attr(ctx)));
				if (at_c<3>(_attr(ctx)))
					e.push_back(at_c<3>(_attr(ctx)));
				_val(ctx) = Expr::ptr_t(new ExprFunc(ExprFunc::func_t::overlay, e));
			}
		};

		struct action_func_position {
			template<typename Context>
			void operator()(Context& ctx) {
				ExprFunc::exprlist_t e;
				e.push_back(at_c<1>(_attr(ctx)));
				e.push_back(at_c<0>(_attr(ctx)));
				_val(ctx) = Expr::ptr_t(new ExprFunc(ExprFunc::func_t::strpos, e));
			}
		};

		struct action_func_concat_ws {
			template<typename Context>
			void operator()(Context& ctx) {
				ExprFunc::exprlist_t e(at_c<1>(_attr(ctx)));
				e.insert(e.begin(), at_c<0>(_attr(ctx)));
				_val(ctx) = Expr::ptr_t(new ExprFunc(ExprFunc::func_t::concat_ws, e));
			}
		};

		struct action_func_replace {
			template<typename Context>
			void operator()(Context& ctx) {
				ExprFunc::exprlist_t e;
				e.push_back(at_c<0>(_attr(ctx)));
				e.push_back(at_c<1>(_attr(ctx)));
				e.push_back(at_c<2>(_attr(ctx)));
				_val(ctx) = Expr::ptr_t(new ExprFunc(ExprFunc::func_t::replace, e));
			}
		};

		struct action_func_substr {
			template<typename Context>
			void operator()(Context& ctx) {
				ExprFunc::exprlist_t e;
				e.push_back(at_c<0>(_attr(ctx)));
				e.push_back(at_c<1>(_attr(ctx)));
				e.push_back(at_c<2>(_attr(ctx)));
				_val(ctx) = Expr::ptr_t(new ExprFunc(ExprFunc::func_t::substr, e));
			}
		};

		const rule<struct func_blrtrim_char, Expr::const_ptr_t> func_blrtrim_char = "func_blrtrim_char";
		const rule<struct func_trim_char, Expr::const_ptr_t> func_trim_char = "func_trim_char";
		const rule<struct func_trimblt, ExprFunc::func_t> func_trimblt = "func_trimblt";
		const rule<struct func_optionalfor, ExprFunc::const_ptr_t> func_optionalfor = "func_optionalfor";
		const rule<struct func_substring_from, ExprFunc::const_ptr_t> func_substring_from = "func_substring_from";
		const rule<struct func_substring_args, ExprFunc::exprlist_t> func_substring_args = "func_substring_args";
		const func_exprlist_type func_exprlist INIT_PRIORITY(200) = "func_exprlist";
		const func_string_type func_string INIT_PRIORITY(200) = "func_string";

		const auto func_blrtrim_char_def =
			(-(lit(',') > expr[action_copy{}]))[action_defaultspace{}];

		const auto func_trim_char_def =
			(-(expr[action_copy{}]))[action_defaultspace{}];

		const auto func_trimblt_def =
			keyword("trim")[action_trimblt_func{ExprFunc::func_t::btrim}] > lit('(') >
			-(keyword("leading")[action_trimblt_func{ExprFunc::func_t::ltrim}]
			  | keyword("trailing")[action_trimblt_func{ExprFunc::func_t::rtrim}]
			  | keyword("both")[action_trimblt_func{ExprFunc::func_t::btrim}]);

		const auto func_optionalfor_def =
			-(keyword("for") > expr[action_copy{}]);

		const auto func_substring_from_def =
			-(keyword("from") > expr[action_copy{}])[action_defaultone{}];

		const auto func_substring_args_def =
			expr[action_append{}] >
			((lit(',') > expr[action_append{}] > func_optionalarg[action_appendifnotnull{}])
			 | (func_substring_from[action_append{}] > func_optionalfor[action_appendifnotnull{}]));

		const auto func_exprlist_def =
			(expr % lit(','));

		const auto func_string_def =
			(lexeme[no_case[funcstringonearg] >> !char_("0-9a-zA-Z_")] >
			 lit('(') > expr > lit(')'))[action_func{}]
			| (lexeme[no_case[funcblrtrim] >> !char_("0-9a-zA-Z_")] >
			   lit('(') > expr > func_blrtrim_char > lit(')'))[action_func{}]
			| (lexeme[no_case[funcstringtwoarg] >> !char_("0-9a-zA-Z_")] >
			   lit('(') > expr > lit(',') > expr > lit(')'))[action_func{}]
			| (func_trimblt > func_trim_char > keyword("from") > expr > lit(')'))[action_functwoarg_reverse{}]
			| (keyword("overlay") > lit('(') > expr > keyword("placing") > expr >
			   keyword("from") > expr > func_optionalfor > lit(')'))[action_func_overlay{}]
			| (keyword("position") > lit('(') > expr > keyword("in") > expr > lit(')'))[action_func_position{}]
			| (keyword("substring") > lit('(') > func_substring_args > lit(')'))[action_func_op{ExprFunc::func_t::substr}]
			| (keyword("concat") > lit('(') > func_exprlist > lit(')'))[action_func_op{ExprFunc::func_t::concat}]
			| (keyword("concat_ws") > lit('(') > expr > lit(',') > func_exprlist > lit(')'))[action_func_concat_ws{}]
			| (keyword("replace") > lit('(') > expr > lit(',') > expr > lit(',') > expr > lit(')'))[action_func_replace{}]
			| (keyword("substr") > lit('(') > expr > lit(',') > expr > func_optionalarg > lit(')'))[action_func_substr{}];

		BOOST_SPIRIT_DEFINE(func_blrtrim_char, func_trim_char, func_trimblt, func_optionalfor,
				    func_substring_from, func_substring_args, func_exprlist, func_string);

		BOOST_SPIRIT_INSTANTIATE(func_exprlist_type, iterator_type, context_type);
		BOOST_SPIRIT_INSTANTIATE(func_string_type, iterator_type, context_type);

#if SPIRIT_X3_VERSION < 0x3006
		template bool parse_rule<iterator_type, context_type, x3::unused_type const>(func_exprlist_type rule_, iterator_type& first, iterator_type const& last, context_type const& context, x3::unused_type const& attr);
		template bool parse_rule<iterator_type, context_type, x3::unused_type>(func_exprlist_type, iterator_type&, iterator_type const&, context_type const&, x3::unused_type&);
#endif

		//-----------------------------------------------------------------------------
		// Conditional Expressions (https://www.postgresql.org/docs/12/functions-conditional.html)
		//-----------------------------------------------------------------------------

		struct action_func_cond_when_expr {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx).set_expr(_attr(ctx));
			}
		};

		struct action_func_cond_when_value {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx).set_value(_attr(ctx));
			}
		};

		struct action_func_cond_alloc {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx) = new ExprCase();
			}
		};

		struct action_func_cond_expr {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx)->set_expr(_attr(ctx));
			}
		};

		struct action_func_cond_else {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx)->set_else(_attr(ctx));
			}
		};

		struct action_func_cond_when {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx)->append_when(_attr(ctx));
			}
		};

		const rule<struct func_case_when, ExprCase::When> func_case_when = "func_case_when";
		const rule<struct func_case, boost::intrusive_ptr<ExprCase> > func_case = "func_case";
		const func_conditional_type func_conditional = "func_conditional";

		const auto func_case_when_def =
			keyword("when") > expr[action_func_cond_when_expr{}] > keyword("then") > expr[action_func_cond_when_value{}];

		const auto func_case_def =
			keyword("case")[action_func_cond_alloc{}] > -(expr[action_func_cond_expr{}]) >
			+(func_case_when[action_func_cond_when{}]) >
			-(keyword("else") > expr[action_func_cond_else{}]) > keyword("end");

		const auto func_conditional_def =
			func_case[action_copy{}]
			| (keyword("coalesce") > lit('(') > func_exprlist > lit(')'))[action_func_op{ExprFunc::func_t::coalesce}]
			| (keyword("nullif") > lit('(') > expr > lit(',') > expr > lit(')'))[action_func_op{ExprFunc::func_t::nullif}]
			| (keyword("greatest") > lit('(') > func_exprlist > lit(')'))[action_func_op{ExprFunc::func_t::greatest}]
			| (keyword("least") > lit('(') > func_exprlist > lit(')'))[action_func_op{ExprFunc::func_t::least}];

		BOOST_SPIRIT_DEFINE(func_case_when, func_case, func_conditional);

		BOOST_SPIRIT_INSTANTIATE(func_conditional_type, iterator_type, context_type);

		//-----------------------------------------------------------------------------
		// Array Functions (https://www.postgresql.org/docs/12/functions-array.html)
		//-----------------------------------------------------------------------------

		const func_array_type func_array INIT_PRIORITY(200) = "func_array";

		const auto func_array_def =
			(keyword("array") > lit('[') > func_exprlist > lit(']'))[action_func_op{ExprFunc::func_t::array}]
			| (keyword("array_to_string") > lit('(') > expr > lit(',') > expr > func_optionalarg >
			 lit(')'))[action_func_op{ExprFunc::func_t::array_to_string}]
			| (keyword("string_to_array") > lit('(') > expr > lit(',') > expr > func_optionalarg >
			   lit(')'))[action_func_op{ExprFunc::func_t::string_to_array}]
			| (keyword("array_length") > lit('(') > expr > lit(',') > expr >
			   lit(')'))[action_func_op{ExprFunc::func_t::array_length}];

		BOOST_SPIRIT_DEFINE(func_array);

		BOOST_SPIRIT_INSTANTIATE(func_array_type, iterator_type, context_type);

		//-----------------------------------------------------------------------------
		// GIS Functions
		//-----------------------------------------------------------------------------

		const func_gis_type func_gis INIT_PRIORITY(200) = "func_gis";

		const auto func_gis_def =
			(keyword("st_pointonsurface") > lit('(') > expr >
			 lit(')'))[action_func_op{ExprFunc::func_t::st_pointonsurface}]
			| (keyword("st_dwithin") > lit('(') > expr > lit(',') > expr > lit(',') > expr >
			   lit(')'))[action_func_op{ExprFunc::func_t::st_dwithin}];

		BOOST_SPIRIT_DEFINE(func_gis);

		BOOST_SPIRIT_INSTANTIATE(func_gis_type, iterator_type, context_type);

		//-----------------------------------------------------------------------------
		// Operators (https://www.postgresql.org/docs/12/functions.html)
		//-----------------------------------------------------------------------------

		const rule<struct op_tagmember> op_tagmember = "op_tagmember";
		const rule<struct op_tagexists> op_tagexists = "op_tagexists";
		const rule<struct op_tagvalue> op_tagvalue = "op_tagvalue";

		const auto op_tagmember_def = lexeme[lit("->")];
		const auto op_tagexists_def = lexeme[lit('?')];
		const auto op_tagvalue_def = lexeme[lit("@>")];

		BOOST_SPIRIT_DEFINE(op_tagmember, op_tagexists, op_tagvalue);

		//-----------------------------------------------------------------------------
		// Column Name
		//-----------------------------------------------------------------------------

		struct action_column_name_idents_col {
			template<typename Context>
			void operator()(Context& ctx) {
				auto& g(get<parser_tag>(ctx).get());
				boost::intrusive_ptr<ExprColumn> p(new ExprColumn("", "", _attr(ctx)));
				_val(ctx) = p;
				g.add_location(p, _where(ctx).begin());
			}
		};

		struct action_column_name_idents_append {
			template<typename Context>
			void operator()(Context& ctx) {
				boost::intrusive_ptr<ExprColumn>& col(_val(ctx));
				col->set_schema(col->get_table());
				col->set_table(col->get_column());
				col->set_column(_attr(ctx));
			}
		};

		struct column_name_tag {
			std::string m_member;
			std::string m_value;
			ExprColumn::record_t m_recop;
			column_name_tag(void) : m_recop(ExprColumn::record_t::none) {}

			void set_op(ExprColumn::record_t op) {
				m_recop = op;
				m_member.clear();
				m_value.clear();
			}

			void set_lit(const std::string& lit) {
				switch (m_recop) {
				case ExprColumn::record_t::member:
				case ExprColumn::record_t::exists:
					m_member = lit;
					m_value.clear();
					break;

				case ExprColumn::record_t::equalsvalue:
				{
					Glib::ustring litu(lit);
					Glib::ustring::size_type i(litu.find("=>"));
					if (i == Glib::ustring::npos) {
						m_member = lit;
						m_value.clear();
						break;
					}
					m_member = litu.substr(0, i);
					m_value = litu.substr(i + 2);
					break;
				}

				default:
					m_member.clear();
					m_value.clear();
					break;
				}
			}
		};

		struct action_column_name_tagop_rec {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx).set_op(_attr(ctx));
			}
		};

		struct action_column_name_tagop_lit {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx).set_lit(_attr(ctx));
			}
		};

		struct action_column_name {
			template<typename Context>
			void operator()(Context& ctx) {
				boost::intrusive_ptr<ExprColumn>& col(at_c<0>(_attr(ctx)));
				const column_name_tag& tag(at_c<1>(_attr(ctx)));
				col->set_member(tag.m_member);
				col->set_value(tag.m_value);
				col->set_record(tag.m_recop);
				_val(ctx) = col;
			}
		};

		const rule<struct column_name_idents, boost::intrusive_ptr<ExprColumn> > column_name_idents = "column_name_idents";
		const rule<struct column_name_recop, ExprColumn::record_t> column_name_recop = "column_name_recop";
		const rule<struct column_name_tagop, column_name_tag> column_name_tagop = "column_name_tagop";
		const column_name_type column_name INIT_PRIORITY(200) = "column_name";

		const auto column_name_idents_def =
			identifier[action_column_name_idents_col{}] >> (repeat(0, 2)[lit('.') >> identifier[action_column_name_idents_append{}]]);

		const auto column_name_recop_def =
			op_tagmember[action_setval<ExprColumn::record_t>{ExprColumn::record_t::member}]
			| op_tagexists[action_setval<ExprColumn::record_t>{ExprColumn::record_t::exists}]
			| op_tagvalue[action_setval<ExprColumn::record_t>{ExprColumn::record_t::equalsvalue}];

		const auto column_name_tagop_def =
			-(column_name_recop[action_column_name_tagop_rec{}] >> string_literal[action_column_name_tagop_lit{}]);

		const auto column_name_def =
			(column_name_idents >> column_name_tagop)[action_column_name{}];

		BOOST_SPIRIT_DEFINE(column_name_idents, column_name_recop, column_name_tagop, column_name);

		BOOST_SPIRIT_INSTANTIATE(column_name_type, iterator_type, context_type);

#if SPIRIT_X3_VERSION < 0x3006
		template bool parse_rule<iterator_type, context_type, x3::unused_type const>(column_name_type, iterator_type&, iterator_type const&, context_type const&, x3::unused_type const&);
#endif
	};
};
