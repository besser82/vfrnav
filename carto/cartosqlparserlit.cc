//
// C++ Implementation: cartosqlparserlit
//
// Description: CartoCSS SQL parser literals
//
//
// Author: Thomas Sailer <t.sailer@alumni.ethz.ch>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cartosqlparser.hh"

#undef BOOST_SPIRIT_X3_DEBUG
#include <boost/fusion/include/vector.hpp>
#include <boost/fusion/include/make_deque.hpp>
#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/fusion/include/io.hpp>
#include <boost/fusion/include/for_each.hpp>
#include <boost/fusion/include/pop_front.hpp>
#include <boost/fusion/adapted.hpp>

#include <boost/variant.hpp>

namespace CartoSQLParser {

	namespace ascii = boost::spirit::x3::ascii;
	namespace fusion = boost::fusion;

	namespace parser {
		using x3::get;
		using x3::no_case;
		using x3::no_skip;
		using x3::lit;
		using x3::string;
		using x3::omit;
		using x3::lexeme;
		using x3::repeat;
		using x3::eps;
		using x3::eol;
		using x3::eoi;
		using x3::attr;
		using x3::with;
		using x3::_val;
		using x3::_attr;
		using x3::_where;
		using x3::_pass;
		using x3::ulong_;
		using x3::long_;
		using x3::double_;
		using ascii::char_;
		using ascii::alnum;
		using ascii::alpha;
		using ascii::digit;
		using ascii::xdigit;
		using ascii::space;
		using fusion::at_c;

 		//-----------------------------------------------------------------------------
		// 5.2 <token> and <separator>
		//-----------------------------------------------------------------------------

		struct action_delimited_identifier {
			template<typename Context>
			void operator()(Context& ctx) {
				bool delim(false);
				std::string r;
				for (const std::string& s : _attr(ctx)) {
					if (delim)
						r.push_back('"');
					r.append(s);
					delim = true;
				}
				_val(ctx) = r;
			}
		};

		const rule<class regular_identifier, std::string> regular_identifier = "regular_identifier";
		const rule<class delimited_identifier_part, std::string> delimited_identifier_part = "delimited_identifier_part";
		const rule<class delimited_identifier, std::string> delimited_identifier = "delimited_identifier";
		const identifier_type identifier INIT_PRIORITY(200) = "identifier";

		static const char * const reserved_words[] = {
			"all", "analyse", "analyze", "and", "any", "array", "as", "asc", "asymmetric",
			"authorization",
			"binary", "both",
			"case", "cast", "check", "collate", "collation", "column", "concurrently",
			"constraint", "create", "cross", "current_catalog", "current_date",
			"current_role", "current_schema", "current_time", "current_timestamp",
			"current_user",
			"default", "deferrable", "desc", "distinct", "do",
			"else", "end", "except",
			"false", "fetch", "for", "foreign", "freeze", "from", "full",
			"grant", "group",
			"having",
			"ilike", "in", "initially", "inner", "intersect", "into", "is", "isnull",
			"join",
			"lateral", "leading", "left", "like", "limit", "localtime", "localtimestamp",
			"natural", "not", "notnull", "null",
			"offset", "on", "only", "or", "order", "outer", "overlaps",
			"placing", "primary",
			"references", "returning", "right",
			"select", "session_user", "similar", "some", "symmetric",
			"table", "tablesample", "then", "to", "trailing", "true",
			"union", "unique", "user", "using",
			"variadic", "verbose",
			"when", "where", "window", "with"
		};

		struct reserved_words_compare {
			bool operator()(const char *a, const char *b) const {
				if (!b)
					return false;
				if (!a)
					return true;
				return strcmp(a, b) < 0;
			}
		};

		struct regular_identifier_parser : boost::spirit::x3::parser<regular_identifier_parser> {
			typedef std::string attribute_type;
			static constexpr bool has_attribute = true;
			template <typename Iterator, typename Context, typename Attribute>
			bool parse(Iterator& first, Iterator const& last, Context const& context, x3::unused_type, Attribute& attr) const {
				x3::skip_over(first, last, context);
				std::string id;
				Iterator i = first;
				for (; i != last; ++i) {
					char ch(*i);
					if (ch >= 'A' && ch <= 'Z')
						ch += 'a' - 'A';
					if (!(((ch >= 'a' && ch <= 'z') || ch == '_') ||
					      (!id.empty() && (ch >= '0' && ch <= '9'))))
						break;
					id.push_back(ch);
				}
				if (id.empty())
					return false;
				const char * const *reserved_words_end = reserved_words + sizeof(reserved_words)/sizeof(reserved_words[0]);
				const char * const *rw(std::lower_bound(reserved_words, reserved_words_end, id.c_str(), reserved_words_compare()));
				if (rw != reserved_words_end && id == *rw)
					return false;
				if (false)
					x3::traits::move_to(id, attr);
				else
					x3::traits::move_to(first, i, attr);
				first = i;
				if (false)
					std::cerr << "regular_identifier: " << id << std::endl;
				return true;
			}
		};

		const auto regular_identifier_def = regular_identifier_parser{};

		const auto delimited_identifier_part_def = lexeme[+(lit('"') >> *(char_ - lit('"')) >> lit('"'))];
		const auto delimited_identifier_def = (+delimited_identifier_part)[action_delimited_identifier{}];

		const auto identifier_def = regular_identifier | delimited_identifier;

		BOOST_SPIRIT_DEFINE(regular_identifier, delimited_identifier_part, delimited_identifier, identifier);

		BOOST_SPIRIT_INSTANTIATE(identifier_type, iterator_type, context_type);

#if SPIRIT_X3_VERSION < 0x3006
		template bool parse_rule<iterator_type, context_type, x3::unused_type const>(identifier_type rule_, iterator_type& first, iterator_type const& last, context_type const& context, x3::unused_type const& attr);
#endif

		//-----------------------------------------------------------------------------
		// Numeric Literal https://www.sqlite.org/syntax/numeric-literal.html
		//-----------------------------------------------------------------------------

		struct action_numeric_literal {
			template<typename Context>
			void operator()(Context& ctx) {
				const std::string& str(_attr(ctx));
				bool int_(true);
				bool hex(false);
				for (const char ch : str) {
					if (ch == 'x' || ch == 'X') {
						hex = true;
						continue;
					}
					if (!std::isdigit(ch)) {
						int_ = false;
						break;
					}
				}
				if (int_)
					_val(ctx) = Value(static_cast<int64_t>(strtoull(str.c_str(), nullptr, hex ? 0 : 10)));
				else
					_val(ctx) = Value(strtod(str.c_str(), nullptr));
			}
		};

		struct action_unsigned_integer {
			template<typename Context>
			void operator()(Context& ctx) {
				const std::string& str(_attr(ctx));
				_val(ctx) = strtoull(str.c_str(), nullptr, 10);
			}
		};

		struct action_signed_integer {
			template<typename Context>
			void operator()(Context& ctx) {
				const std::string& str(_attr(ctx));
				_val(ctx) = strtoll(str.c_str(), nullptr, 10);
			}
		};

		const rule<struct numeric_literal_string, std::string> numeric_literal_string = "numeric_literal_string";
		const rule<struct numeric_literal_exponent, std::string> numeric_literal_exponent = "numeric_literal_exponent";
		const rule<struct optionalsign, std::string> optionalsign = "optionalsign";
		const rule<struct signed_integer_string, std::string> signed_integer_string = "signed_integer_string";
		const numeric_literal_type numeric_literal INIT_PRIORITY(200) = "numeric_literal";
		const unsigned_integer_type unsigned_integer INIT_PRIORITY(200) = "unsigned_integer";
		const signed_integer_type signed_integer INIT_PRIORITY(200) = "signed_integer";

		const auto numeric_literal_def =
			numeric_literal_string[action_numeric_literal{}];

		const auto numeric_literal_string_def =
			lexeme[(no_case[string("0x")] >> +xdigit)
			       | (char_('.') >> +digit >> numeric_literal_exponent)
			       | (+digit >> -(char_('.') >> *digit) >> numeric_literal_exponent)];

		const auto numeric_literal_exponent_def = -(no_case[char_('e')] >> -(char_('+') | char_('-')) >> +digit);

		const auto unsigned_integer_def = (+digit)[action_unsigned_integer{}];

		const auto optionalsign_def = -(char_("+-")[action_copy{}]);

		const auto signed_integer_string_def = optionalsign >> +digit;

		const auto signed_integer_def = signed_integer_string[action_signed_integer{}];

		BOOST_SPIRIT_DEFINE(numeric_literal_string, numeric_literal_exponent, optionalsign, signed_integer_string,
				    numeric_literal, unsigned_integer, signed_integer);

		BOOST_SPIRIT_INSTANTIATE(numeric_literal_type, iterator_type, context_type);
		BOOST_SPIRIT_INSTANTIATE(unsigned_integer_type, iterator_type, context_type);
		BOOST_SPIRIT_INSTANTIATE(signed_integer_type, iterator_type, context_type);

		//-----------------------------------------------------------------------------
		// String and BLOB literals
		//-----------------------------------------------------------------------------

		static unsigned int convxdigit(char ch) {
			if (ch >= 'a')
				ch -= 'a' - 'A';
			if (ch > '9')
				ch -= 'A' - '0' - 10;
			ch -= '0';
			return ch & 15;
		}

		struct action_regular_string_literal_part {
			template<typename Context>
			void operator()(Context& ctx) {
				bool delim(false);
				std::string r;
				for (const std::string& s : _attr(ctx)) {
					if (delim)
						r.push_back('\'');
					r.append(s);
					delim = true;
				}
				_val(ctx) = r;
			}
		};

		struct action_regular_string_literal {
			template<typename Context>
			void operator()(Context& ctx) {
				std::string r(at_c<0>(_attr(ctx)));
				for (const std::string& s : at_c<1>(_attr(ctx))) {
					r.append(s);
				}
				_val(ctx) = r;
			}
		};

		struct action_unicode_string_literal {
			template<typename Context>
			void operator()(Context& ctx) {
				bool delim(false);
				Glib::ustring r;
				for (const std::string& s : _attr(ctx)) {
					if (delim)
						r.push_back('\'');
					r.append(convert(s));
					delim = true;
				}
				_val(ctx) = r;
			}

			Glib::ustring convert(const std::string& str) {
				Glib::ustring r;
				char uescape('\\');
				for (std::string::const_iterator i(str.begin()), e(str.end()); i != e; ) {
					if (*i != uescape) {
						r.push_back(*i++);
						continue;
					}
					++i;
					if (i == e)
						break;
					int digits(4);
					++i;
					if (i == e)
						break;
					if (*i == '+') {
						digits = 6;
						++i;
					}
					gunichar val(0);
					for (; digits >= 0 && i != e; --digits, ++i) {
						if (!std::isxdigit(*i))
							break;
						val <<= 4;
						val |= convxdigit(*i);
					}
					r.push_back(val);
				}
				return r;
			}
		};

		struct action_escaped_string_literal {
			template<typename Context>
			void operator()(Context& ctx) {
				bool delim(false);
				Glib::ustring r;
				for (const std::string& s : _attr(ctx)) {
					if (delim)
						r.push_back('\'');
					r.append(convert(s));
					delim = true;
				}
				_val(ctx) = r;
			}

			Glib::ustring convert(const std::string& str) {
				Glib::ustring r;
				for (std::string::const_iterator i(str.begin()), e(str.end()); i != e; ) {
					if (*i != '\\') {
						r.push_back(*i++);
						continue;
					}
					++i;
					if (i == e)
						break;
					switch (*i) {
					case 'b':
						r.push_back('\b');
						++i;
						break;

					case 'f':
						r.push_back('\f');
						++i;
						break;

					case 'n':
						r.push_back('\n');
						++i;
						break;

					case 'r':
						r.push_back('\r');
						++i;
						break;

					case 't':
						r.push_back('\t');
						++i;
						break;

					case '0':
					case '1':
					case '2':
					case '3':
					case '4':
					case '5':
					case '6':
					case '7':
					{
						char val(0);
						++i;
						for (int digits(3); digits >= 0 && i != e; --digits, ++i) {
							if (*i < '0' || *i > '7')
								break;
							val <<= 3;
							val |= ((*i) - '0');
						}
						r.push_back(val);
						break;
					}

					case 'x':
					{
						char val(0);
						++i;
						for (int digits(2); digits >= 0 && i != e; --digits, ++i) {
							if (!std::isxdigit(*i))
								break;
							val <<= 4;
							val |= convxdigit(*i);
						}
						r.push_back(val);
						break;
					}

					case 'u':
					{
						gunichar val(0);
						++i;
						for (int digits(4); digits >= 0 && i != e; --digits, ++i) {
							if (!std::isxdigit(*i))
								break;
							val <<= 4;
							val |= convxdigit(*i);
						}
						r.push_back(val);
						break;
					}

					case 'U':
					{
						gunichar val(0);
						++i;
						for (int digits(8); digits >= 0 && i != e; --digits, ++i) {
							if (!std::isxdigit(*i))
								break;
							val <<= 4;
							val |= convxdigit(*i);
						}
						r.push_back(val);
						break;
					}

					default:
						r.push_back(*i);
						++i;
						break;
					}
				}
				return r;
			}
		};

		struct action_bit_string_literal_part {
			template<typename Context>
			void operator()(Context& ctx) {
				bool delim(false);
				std::string r;
				for (const std::string& s : _attr(ctx)) {
					if (delim)
						r.push_back('\'');
					r.append(s);
					delim = true;
				}
				_val(ctx) = r;
			}

			std::string convert(const std::string& str) {
				std::string r;
				char val(0);
				unsigned int digits(0);
				for (std::string::const_iterator i(str.begin()), e(str.end()); i != e; ++i) {
					if (*i < '0' || *i > '1')
						continue;
					val <<= 1;
					val |= (*i) - '0';
					++digits;
					if (digits < 8)
						continue;
					r.push_back(val);
					val = 0;
					digits = 0;
				}
				if (digits)
					r.push_back(val << (8 - digits));
				return r;
			}
		};

		struct action_hex_string_literal_part {
			template<typename Context>
			void operator()(Context& ctx) {
				bool delim(false);
				std::string r;
				for (const std::string& s : _attr(ctx)) {
					if (delim)
						r.push_back('\'');
					r.append(s);
					delim = true;
				}
				_val(ctx) = r;
			}

			std::string convert(const std::string& str) {
				std::string r;
				char val(0);
				unsigned int digits(0);
				for (std::string::const_iterator i(str.begin()), e(str.end()); i != e; ++i) {
					if (!std::isxdigit(*i))
						continue;
					val <<= 4;
					val |= convxdigit(*i);
					digits += 4;
					if (digits < 8)
						continue;
					r.push_back(val);
					val = 0;
					digits = 0;
				}
				if (digits)
					r.push_back(val << (8 - digits));
				return r;
			}
		};

		const rule<struct string_part, std::string> string_part = "string_part";
		const rule<struct regular_string_literal_part1, std::string> regular_string_literal_part1 = "regular_string_literal_part1";
		const rule<struct regular_string_literal_part2, std::string> regular_string_literal_part2 = "regular_string_literal_part2";
		const rule<struct regular_string_literal, std::string> regular_string_literal = "regular_string_literal";
		const rule<struct unicode_string_literal, std::string> unicode_string_literal = "unicode_string_literal";
		const rule<struct escaped_string_literal, std::string> escaped_string_literal = "escaped_string_literal";
		const rule<struct bit_string_literal_part1, std::string> bit_string_literal_part1 = "bit_string_literal_part1";
		const rule<struct bit_string_literal_part2, std::string> bit_string_literal_part2 = "bit_string_literal_part2";
		const rule<struct bit_string_literal, std::string> bit_string_literal = "bit_string_literal";
		const rule<struct hex_string_literal_part1, std::string> hex_string_literal_part1 = "hex_string_literal_part1";
		const rule<struct hex_string_literal_part2, std::string> hex_string_literal_part2 = "hex_string_literal_part2";
		const rule<struct hex_string_literal, std::string> hex_string_literal = "hex_string_literal";
		const string_literal_type string_literal INIT_PRIORITY(200) = "string_literal";

		const auto string_part_def =
			lit('\'') >> *(char_ - lit('\'')) >> lit('\'');

		const auto regular_string_literal_part1_def =
			(+string_part)[action_regular_string_literal_part{}];

		const auto regular_string_literal_part2_def =
			*lit(" \t") >> +(eol >> *lit(" \t")) >> regular_string_literal_part1;

		const auto regular_string_literal_def =
			lexeme[regular_string_literal_part1 >> *(regular_string_literal_part2)][action_regular_string_literal{}];

		const auto unicode_string_literal_def =
			lexeme[no_case[lit("u&")] >> (+string_part)[action_unicode_string_literal{}]];

		const auto escaped_string_literal_def =
			lexeme[no_case[lit('e')] >> (+string_part)[action_escaped_string_literal{}]];

		const auto bit_string_literal_part1_def =
			(+string_part)[action_bit_string_literal_part{}];

		const auto bit_string_literal_part2_def =
			*lit(" \t") >> +(eol >> *lit(" \t")) >> bit_string_literal_part1;

		const auto bit_string_literal_def =
			lexeme[no_case[lit('b')] >> bit_string_literal_part1 >> *(bit_string_literal_part2)][action_regular_string_literal{}];

		const auto hex_string_literal_part1_def =
			(+string_part)[action_hex_string_literal_part{}];

		const auto hex_string_literal_part2_def =
			*lit(" \t") >> +(eol >> *lit(" \t")) >> hex_string_literal_part1;

		const auto hex_string_literal_def =
			lexeme[no_case[lit('x')] >> hex_string_literal_part1 >> *(hex_string_literal_part2)][action_regular_string_literal{}];

		const auto string_literal_def =
			unicode_string_literal
			| escaped_string_literal
			| bit_string_literal
			| hex_string_literal
			| regular_string_literal;

		BOOST_SPIRIT_DEFINE(string_part, regular_string_literal, regular_string_literal_part1,
				    regular_string_literal_part2, unicode_string_literal,
				    escaped_string_literal, bit_string_literal_part1,
				    bit_string_literal_part2, bit_string_literal,
				    hex_string_literal_part1, hex_string_literal_part2,
				    hex_string_literal, string_literal);

		BOOST_SPIRIT_INSTANTIATE(string_literal_type, iterator_type, context_type);

#if SPIRIT_X3_VERSION < 0x3006
		template bool parse_rule<iterator_type, context_type, x3::unused_type const>(string_literal_type rule_, iterator_type& first, iterator_type const& last, context_type const& context, x3::unused_type const& attr);
#endif

		//-----------------------------------------------------------------------------
		// literals
		//-----------------------------------------------------------------------------

		const literal_type literal = "literal";

		const auto literal_def =
			numeric_literal[action_copy{}]
			| string_literal[action_copy{}]
			| keyword("null")[action_setval<Value>{Value()}]
			| keyword("true")[action_setval<bool>{true}]
			| keyword("false")[action_setval<bool>{false}]
			| keyword("current_time")[action_setval<Value>{Value()}]
			| keyword("current_date")[action_setval<Value>{Value()}]
			| keyword("current_timestamp")[action_setval<Value>{Value()}];

		BOOST_SPIRIT_DEFINE(literal);

		BOOST_SPIRIT_INSTANTIATE(literal_type, iterator_type, context_type);

		//-----------------------------------------------------------------------------
		// Type Names (https://www.sqlite.org/datatype3.html)
		//-----------------------------------------------------------------------------

		struct action_typename {
			Value::type_t m_type;
			action_typename(Value::type_t typ) : m_type(typ) {}
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx) = m_type;
			}
		};

		const type_name_type type_name INIT_PRIORITY(200) = "type_name";

		const auto type_name_def =
			((keyword("int")
			  | keyword("integer")
			  | keyword("tinyint")
			  | keyword("smallint")
			  | keyword("mediumint")
			  | keyword("bigint")
			  | (keyword("unsigned") >> keyword("big") >> keyword("int"))
			  | keyword("int2")
			  | keyword("int4")
			  | keyword("int8"))[action_typename{Value::type_t::integer}])
			| (((keyword("character") >> lit('(') >> unsigned_integer >> lit(')'))
			    | (keyword("varchar") >> lit('(') >> unsigned_integer >> lit(')'))
			    | (keyword("varying") >> keyword("character") >> lit('(') >> unsigned_integer >> lit(')'))
			    | (keyword("nchar") >> lit('(') >> unsigned_integer >> lit(')'))
			    | (keyword("native") >> keyword("character") >> lit('(') >> unsigned_integer >> lit(')'))
			    | (keyword("nvarchar") >> lit('(') >> unsigned_integer >> lit(')'))
			    | keyword("text")
			    | keyword("clob")
			    | keyword("blob"))[action_typename{Value::type_t::string}])
			| ((keyword("real")
			    | (keyword("double") >> -(keyword("precision")))
			    | keyword("float")
			    | keyword("numeric")
			    | (keyword("decimal") >> lit('(') >> unsigned_integer >> lit(')')))[action_typename{Value::type_t::floatingpoint}])
			| (keyword("boolean")[action_typename{Value::type_t::boolean}])
			| ((keyword("date")
			    | keyword("datetime"))[action_typename{Value::type_t::null}]);

		BOOST_SPIRIT_DEFINE(type_name);

		BOOST_SPIRIT_INSTANTIATE(type_name_type, iterator_type, context_type);
	};
};
