//
// C++ Implementation: cartosqltbl
//
// Description: CartoSQL Table Expressions
//
//
// Author: Thomas Sailer <t.sailer@alumni.ethz.ch>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <cmath>

#include "geom.h"
#include "cartosql.h"

#include <boost/algorithm/string/predicate.hpp>

int CartoSQL::TableRowValues::compare_value(const TableRowValues& x) const
{
	size_type n0(size()), n1(x.size()), n(std::min(n0, n1));
	for (size_type i(0); i < n; ++i) {
		int c(operator[](i).compare_value(x[i]));
		if (c)
			return c;
	}
	if (n0 < n1)
		return -1;
	if (n1 < n0)
		return 1;
	return 0;
}

int CartoSQL::TableRowValues::compare(const TableRowValues& x) const
{
	size_type n0(size()), n1(x.size()), n(std::min(n0, n1));
	for (size_type i(0); i < n; ++i) {
		int c(operator[](i).compare(x[i]));
		if (c)
			return c;
	}
	if (n0 < n1)
		return -1;
	if (n1 < n0)
		return 1;
	return 0;
}

void CartoSQL::TableRow::swap(TableRow& x)
{
	TableRowValues::swap(x);
	m_osmidset.swap(x.m_osmidset);
}

std::string CartoSQL::TableRow::get_osmidset_string(const osmidset_t& s)
{
	std::ostringstream oss;
	bool subseq(false);
	for (osmid_t oid : s) {
		if (subseq)
			oss << ',';
		subseq = true;
		oss << oid;
	}
	return oss.str();
}

CartoSQL::TableRow CartoSQL::TableRow::get_columnset(const columnset_t& colset) const
{
	TableRow tr;
	tr.set_osmidset(get_osmidset());
	for (unsigned int col : colset) {
		if (col >= size())
			break;
		tr.push_back((*this)[col]);
	}
	return tr;
}

int CartoSQL::Table::ValueColumnSorter::compare(const TableRow& a, const TableRow& b) const
{
	for (unsigned int colidx : m_cols) {
		if (colidx >= a.size())
			return (colidx >= b.size()) ? 0 : -1;
		if (colidx >= b.size())
			return 1;
		int c(a[colidx].compare_value(b[colidx]));
		if (c)
			return c;
	}
	return 0;
}

int CartoSQL::Table::compare_value(const Table& x) const
{
	size_type n0(size()), n1(x.size()), n(std::min(n0, n1));
	for (size_type i(0); i < n; ++i) {
		int c(operator[](i).compare_value(x[i]));
		if (c)
			return c;
	}
	if (n0 < n1)
		return -1;
	if (n1 < n0)
		return 1;
	return 0;
}

int CartoSQL::Table::compare(const Table& x) const
{
	size_type n0(size()), n1(x.size()), n(std::min(n0, n1));
	for (size_type i(0); i < n; ++i) {
		int c(operator[](i).compare(x[i]));
		if (c)
			return c;
	}
	if (n0 < n1)
		return -1;
	if (n1 < n0)
		return 1;
	return 0;
}

void CartoSQL::Table::eliminate_duplicates(void)
{
	std::sort(begin(), end(), ValueSorter());
	for (iterator i(begin()), e(end()); i != e; ) {
		iterator i0(i);
		++i;
		if (i == e)
			break;
		if (i0->compare_value(*i))
			continue;
		i = erase(i0);
		e = end();
	}
}

CartoSQL::Table& CartoSQL::Table::append(const Table& tbl)
{
	size_type n(tbl.size()), p(size());
	resize(p + n);
	for (size_type i(0); i < n; ++i)
		operator[](p + i) = tbl[i];
	return *this;
}

CartoSQL::Table& CartoSQL::Table::append(Table&& tbl)
{
	size_type n(tbl.size()), p(size());
	resize(p + n);
	for (size_type i(0); i < n; ++i)
		operator[](p + i).swap(tbl[i]);
	return *this;
}

CartoSQL::Table CartoSQL::Table::get_columnset(const columnset_t& colset) const
{
	Table t;
	t.reserve(size());
	for (const TableRow& tr : *this)
		t.push_back(tr.get_columnset(colset));
	return t;
}

int CartoSQL::IndexTable::compare_value(const IndexTable& x) const
{
	size_type n0(size()), n1(x.size()), n(std::min(n0, n1));
	for (size_type i(0); i < n; ++i) {
		int c(operator[](i).compare_value(x[i]));
		if (c)
			return c;
	}
	if (n0 < n1)
		return -1;
	if (n1 < n0)
		return 1;
	return 0;
}

int CartoSQL::IndexTable::compare(const IndexTable& x) const
{
	size_type n0(size()), n1(x.size()), n(std::min(n0, n1));
	for (size_type i(0); i < n; ++i) {
		int c(operator[](i).compare(x[i]));
		if (c)
			return c;
	}
	if (n0 < n1)
		return -1;
	if (n1 < n0)
		return 1;
	return 0;
}

template <typename T>
void CartoSQL::IndexTable::permute(T& x) const
{
	size_type n(size());
	if (x.size() != n)
		throw std::runtime_error("permute: size mismatch");
	T z;
	z.resize(n);
	for (size_type i = 0; i < n; ++i) {
		size_type j(operator[](i).get_index());
		if (j >= n)
			throw std::runtime_error("permute: row index out of range");
		z[i].swap(x[j]);
	}
	x.swap(z);
}

template void CartoSQL::IndexTable::permute(Table& x) const;
template void CartoSQL::IndexTable::permute(OSMDB::objects_t& x) const;

void CartoSQL::IndexTableRow::swap(IndexTableRow& x)
{
	TableRowValues::swap(x);
	std::swap(m_index, x.m_index);
}

CartoSQL::IndexTable::indexranges_t CartoSQL::IndexTable::get_equalranges(void) const
{
	indexranges_t r;
	for (size_type i(0), n(size()); i < n; ) {
		size_type j(i + 1);
		for (; j < n; ++j)
			if (operator[](j-1).compare_value(operator[](j)))
				break;
		r.push_back(indexrange_t{i, j});
		i = j;
	}
	return r;
}

CartoSQL::ExprTable::ResolveColRefSubtable::ResolveColRefSubtable(const Expr::subtables_t& subtables)
	: m_subtables(subtables), m_offset(0)
{
	m_enables.resize(m_subtables.size(), true);
}

void CartoSQL::ExprTable::ResolveColRefSubtable::visit(const ExprSelect& e)
{
	m_offset += 2;
	if (m_offset < m_enables.size())
		m_enables[m_offset] = false;
}

void CartoSQL::ExprTable::ResolveColRefSubtable::visit(const ExprOSMSelect& e)
{
	m_offset += 1;
	if (m_offset < m_enables.size())
		m_enables[m_offset] = false;
}

void CartoSQL::ExprTable::ResolveColRefSubtable::visit(const ExprCompound& e)
{
	m_offset += 1;
}

void CartoSQL::ExprTable::ResolveColRefSubtable::visit(const ExprJoin& e)
{
	m_offset += 2;
}

void CartoSQL::ExprTable::ResolveColRefSubtable::visitend(const ExprSelect& e)
{
	m_offset -= 2;
}

void CartoSQL::ExprTable::ResolveColRefSubtable::visitend(const ExprOSMSelect& e)
{
	m_offset -= 1;
}

void CartoSQL::ExprTable::ResolveColRefSubtable::visitend(const ExprCompound& e)
{
	m_offset -= 1;
}

void CartoSQL::ExprTable::ResolveColRefSubtable::visitend(const ExprJoin& e)
{
	m_offset -= 2;
}

void CartoSQL::ExprTable::ResolveColRefSubtable::select_result_table_available(const CartoSQL *csql, const ExprTable& e)
{
	if (m_offset < m_enables.size())
		m_enables[m_offset] = true;
}

CartoSQL::Expr::const_ptr_t CartoSQL::ExprTable::ResolveColRefSubtable::modify(const CartoSQL *csql, const ExprColumn& e)
{
	if (false)
		std::cerr << "ResolveColRefSubtable: " << ColumnName(e.get_table(), e.get_column()).to_str() << std::endl;
	for (Expr::subtables_t::size_type i(0), n(m_subtables.size()); i < n; ++i) {
		if (!m_enables[i])
			continue;
		const ExprTable::const_ptr_t& p(m_subtables[i]);
		if (!p)
			continue;
		const Columns& cols(p->get_columns());
		Columns::size_type idx(cols.find(ColumnName(e.get_table(), e.get_column())));
		if (idx >= cols.size())
			continue;
		Expr::const_ptr_t ptc(new ExprTableColumn(i + m_offset, idx));
		if (e.get_record() == ExprColumn::record_t::none)
			return ptc;
		if (!csql)
			return Expr::ptr_t();
		const OSMDB *osmdb(csql->get_osmdb());
		if (!osmdb)
			return Expr::ptr_t();
		boost::intrusive_ptr<ExprTagMember> ptm(new ExprTagMember(ptc));
		ptm->set_key(e.get_member());
		ptm->set_value(e.get_value());
		ptm->set_tagkey(osmdb->find_tagkey(e.get_member()));
		ptm->set_tagname(osmdb->find_tagname(e.get_value()));
		switch (e.get_record()) {
		case ExprColumn::record_t::member:
	        	ptm->set_tag(ExprTagMember::tag_t::value);
			return ptm;

		case ExprColumn::record_t::exists:
			ptm->set_tag(ExprTagMember::tag_t::exists);
			return ptm;

		case ExprColumn::record_t::equalsvalue:
			ptm->set_tag(ExprTagMember::tag_t::isequal);
			return ptm;

		default:
			return Expr::ptr_t();
		}
	}
	return Expr::ptr_t();
}

std::string CartoSQL::ExprTable::UnresolvedColRef::get_expressionstring(void) const
{
	std::ostringstream oss;
	bool subseq(false);
	for (const auto& p : m_exprs) {
		if (!p.first)
			continue;
		if (subseq)
			oss << ", ";
		subseq = true;
		p.first->print(oss, 0);
	}
	return oss.str();
}

void CartoSQL::ExprTable::UnresolvedColRef::visit(const ExprColumn& e)
{
	m_colref = true;
	m_exprs.insert(exprs_t::value_type(e.get_ptr(), m_tablestack.empty() ? ExprTable::const_ptr_t() : m_tablestack.top()));
}

void CartoSQL::ExprTable::UnresolvedColRef::visit(const ExprTable& e)
{
	m_tablestack.push(e.get_ptr());
}

void CartoSQL::ExprTable::UnresolvedColRef::visitend(const ExprTable& e)
{
	if (!m_tablestack.empty())
		m_tablestack.pop();
}

std::string CartoSQL::ExprTable::UnresolvedSubtableColRef::get_expressionstring(void) const
{
	std::ostringstream oss;
	bool subseq(false);
	for (const auto& p : m_exprs) {
		if (!p.first)
			continue;
		if (subseq)
			oss << ", ";
		subseq = true;
		p.first->print(oss, 0);
	}
	return oss.str();
}

void CartoSQL::ExprTable::UnresolvedSubtableColRef::visit(const ExprTableColumn& e)
{
	m_colref = true;
	m_exprs.insert(exprs_t::value_type(e.get_ptr(), m_tablestack.empty() ? ExprTable::const_ptr_t() : m_tablestack.top()));
}

void CartoSQL::ExprTable::UnresolvedSubtableColRef::visit(const ExprTable& e)
{
	m_tablestack.push(e.get_ptr());
}

void CartoSQL::ExprTable::UnresolvedSubtableColRef::visitend(const ExprTable& e)
{
	if (!m_tablestack.empty())
		m_tablestack.pop();
}

CartoSQL::Expr::const_ptr_t CartoSQL::ExprTable::PushConditionUpResolve::modify(const CartoSQL *csql, const ExprTableColumn& e)
{
	if (e.get_tableindex() || !m_table || e.get_columnindex() >= m_table->get_columns().size()) {
		m_error = true;
		return Expr::const_ptr_t();
	}
	const Column& col(m_table->get_columns()[e.get_columnindex()]);
	if (!col.get_expr()) {
		m_error = true;
		return Expr::const_ptr_t();
	}
	return col.get_expr();
}

CartoSQL::Expr::const_ptr_t CartoSQL::ExprTable::PushConditionUpRename::modify(const CartoSQL *csql, const ExprTableColumn& e)
{
	if (e.get_tableindex() != m_tableindex) {
		m_error = true;
		return Expr::const_ptr_t();
	}
	return Expr::const_ptr_t(new ExprTableColumn(0, e.get_columnindex()));
}

const CartoSQL::columnset_t& CartoSQL::ExprTable::TableColumnSets::get_columnset(unsigned int tblidx) const
{
	columnsets_t::const_iterator i(m_columnsets.find(tblidx));
	if (i == m_columnsets.end()) {
		static const columnset_t empty;
		return empty;
	}
	return i->second;
}

CartoSQL::Expr::const_ptr_t CartoSQL::ExprTable::RenumberTableColumns::modify(const CartoSQL *csql, const ExprTableColumn& e)
{
	if (e.get_tableindex() != m_tableindex)
		return Expr::const_ptr_t();
	unsigned int colidx(0);
	columnset_t::const_iterator ci(m_colsused.begin()), ce(m_colsused.end());
	for (; ci != ce; ++ci, ++colidx)
		if (*ci == e.get_columnindex())
			break;
	if (ci == ce)
		throw Error("RenumberTableColumns: cannot remove in-use column");
	return Expr::const_ptr_t(new ExprTableColumn(m_tableindex, colidx));
}

class CartoSQL::ExprTable::CaseInsensitiveCompare {
public:
	bool operator()(const std::string& a, const std::string& b) const {
		return boost::ilexicographical_compare(a, b);
	}
};

bool CartoSQL::ExprTable::ColumnName::is_match(const ColumnName& x) const
{
	if (x.is_wildcard())
		return false;
	if (get_name().empty() || x.get_name().empty())
		return false;
	if (has_table() && x.has_table() && !boost::iequals(get_table(), x.get_table()))
		return false;
	return is_wildcard() || boost::iequals(get_name(), x.get_name());
}

int CartoSQL::ExprTable::ColumnName::compare(const ColumnName& x) const
{
	int c(m_table.compare(x.m_table));
	if (c)
		return c;
	return m_name.compare(x.m_name);
}

std::string CartoSQL::ExprTable::ColumnName::to_str(void) const
{
	std::string r;
	if (has_table()) {
		r.append(get_table());
		r.push_back('.');
	}
	r.append(get_name());
	return r;
}

template <typename... Args>
bool CartoSQL::ExprTable::Column::simplify_int(const CartoSQL *csql, const Expr::subtables_t& subtables, Args&&... args)
{
	if (!m_expr)
		return false;
	Expr::const_ptr_t p1(m_expr->simplify(csql, std::forward<Args>(args)...));
	if (!p1)
		return false;
	m_expr = p1;
	m_type = m_expr->calculate_type(subtables);
	if (false) {
		PrintContextSQL ctx(csql);
	        PrintContextSubtableVector ctx1(ctx, subtables);
		m_expr->print(std::cerr << "Column: " << to_str() << " recalc type: " << m_type << " expr: ",
			      2, ctx1) << std::endl;
	}
	return true;
}

bool CartoSQL::ExprTable::Column::simplify(const CartoSQL *csql, const Expr::subtables_t& subtables)
{
	return simplify_int(csql, subtables);
}

bool CartoSQL::ExprTable::Column::simplify(const CartoSQL *csql, const Expr::subtables_t& subtables, Visitor& v)
{
	return simplify_int(csql, subtables, v);
}

void CartoSQL::ExprTable::Column::visit(Visitor& v) const
{
	if (m_expr)
		m_expr->visit(v);
}

std::ostream& CartoSQL::ExprTable::Column::print(std::ostream& os, unsigned int indent, const std::string& postfix, const PrintContext& ctx) const
{
	if (is_wildcard() || !get_expr()) {
		os << get_name();
	} else {
		Expr::print_expr(os, get_expr(), indent, ctx);
		if (!get_name().empty())
			os << " as \"" << get_name() << '"';
	}
	os << postfix << " -- type: " << Value::get_sqltypename(get_type());
	if (get_expr() && get_expr()->is_aggregate())
		os << " (aggregate)";
	return os;
}

int CartoSQL::ExprTable::Column::compare(const Column& x) const
{
	int c(ColumnName::compare(x));
	if (c)
		return c;
	return Expr::compare(m_expr, x.m_expr);
}

bool CartoSQL::ExprTable::Columns::is_wildcard(void) const
{
	for (const Column& c : *this)
		if (c.is_wildcard())
			return true;
	return false;
}

CartoSQL::ExprTable::Columns::size_type CartoSQL::ExprTable::Columns::find(const ColumnName& x) const
{
	size_type r(size());
	for (size_type i(0), n(size()); i < n; ++i) {
		const Column& col((*this)[i]);
		if (!x.is_match(col))
			continue;
		// check for duplicate
		if (r < size())
			return size();
		r = i;
	}
	return r;
}

void CartoSQL::ExprTable::Columns::set_table(const std::string& tbl)
{
	for (Column& col : *this)
		col.set_table(tbl);
}

bool CartoSQL::ExprTable::Columns::has_aggregate(void) const
{
	for (const Column& col : *this)
		if (col.get_expr() && col.get_expr()->is_aggregate())
			return true;
	return false;
}

void CartoSQL::ExprTable::Columns::remove_unused_columns(const columnset_t& colsused)
{
	Columns ncol;
	for (unsigned int colnr : colsused) {
		if (colnr >= size())
			throw Error("Columns::remove_unused_columns: invalid column index");
		ncol.push_back((*this)[colnr]);
	}
	swap(ncol);
}

template <typename... Args>
bool CartoSQL::ExprTable::Columns::simplify_int(const CartoSQL *csql, const Expr::subtables_t& subtables, Args&&... args)
{
	bool work(false);
	for (Column& c : *this)
		work = c.simplify(csql, subtables, std::forward<Args>(args)...) || work;
	return work;
}

bool CartoSQL::ExprTable::Columns::simplify(const CartoSQL *csql, const Expr::subtables_t& subtables)
{
	return simplify_int(csql, subtables);
}

bool CartoSQL::ExprTable::Columns::simplify(const CartoSQL *csql, const Expr::subtables_t& subtables, Visitor& v)
{
	return simplify_int(csql, subtables, v);
}

void CartoSQL::ExprTable::Columns::visit(Visitor& v) const
{
	for (const Column& c : *this)
		c.visit(v);
}
std::ostream& CartoSQL::ExprTable::Columns::print(std::ostream& os, unsigned int indent, const PrintContext& ctx) const
{
	for (size_type i(0), n(size()); i < n; ++i) {
		const Column& c((*this)[i]);
		c.print(os << std::endl << std::string(indent, ' '), indent + 2, (i + 1 >= n) ? "" : ",", ctx);
	}
	return os;
}

int CartoSQL::ExprTable::Columns::compare(const Columns& x) const
{
	size_type i(x.size()), n(size());
	if (n < i)
		return -1;
	if (i < n)
		return 1;
	for (i = 0; i < n; ++i) {
		int c(operator[](i).compare(x[i]));
		if (c)
			return c;
	}
	return 0;
}

const std::string& to_str(CartoSQL::ExprTable::type_t t)
{
	switch (t) {
	case CartoSQL::ExprTable::type_t::tablevalue:
	{
		static const std::string r("tablevalue");
		return r;
	}

	case CartoSQL::ExprTable::type_t::osmtable:
	{
		static const std::string r("osmtable");
		return r;
	}

	case CartoSQL::ExprTable::type_t::literaltable:
	{
		static const std::string r("literaltable");
		return r;
	}

	case CartoSQL::ExprTable::type_t::tablefunc:
	{
		static const std::string r("tablefunc");
		return r;
	}

	case CartoSQL::ExprTable::type_t::select:
	{
		static const std::string r("select");
		return r;
	}

	case CartoSQL::ExprTable::type_t::osmselect:
	{
		static const std::string r("osmselect");
		return r;
	}

	case CartoSQL::ExprTable::type_t::compound:
	{
		static const std::string r("compound");
		return r;
	}

	case CartoSQL::ExprTable::type_t::join:
	{
		static const std::string r("join");
		return r;
	}

	case CartoSQL::ExprTable::type_t::tablereference:
	{
		static const std::string r("tablereference");
		return r;
	}

	case CartoSQL::ExprTable::type_t::commontables:
	{
		static const std::string r("commontables");
		return r;
	}

	default:
	{
		static const std::string r("?");
		return r;
	}
	}
}

CartoSQL::ExprTable::ExprTable(void)
	: m_refcount(0)
{
}

CartoSQL::ExprTable::~ExprTable()
{
}

void CartoSQL::ExprTable::copy(ExprTable& x) const
{
	x.set_columns(get_columns());
}

CartoSQL::ExprTable::const_ptr_t CartoSQL::ExprTable::simplify_tail(const CartoSQL *csql, const boost::intrusive_ptr<const ExprTable>& p, bool modif)
{
	{
		Calculate c(csql);
		Table t;
		if (p->calculate(c, t)) {
			boost::intrusive_ptr<ExprTableValue> p1(new ExprTableValue(t));
			p->copy(*p1);
			return p1;
		}
	}
	if (modif)
		return p;
	return const_ptr_t();
}

template <typename... Args>
bool CartoSQL::ExprTable::simplify_int(const CartoSQL *csql, const Expr::subtables_t& subtables, boost::intrusive_ptr<ExprTable> p, Args&&... args)
{
	bool work(false);
	work = p->m_columns.simplify(csql, subtables, std::forward<Args>(args)...) || work;
	return work;
}

bool CartoSQL::ExprTable::calculate(Calc& c, Table& t) const
{
	c.error(*this, "not implemented");
	t.clear();
	return false;
}

int CartoSQL::ExprTable::compare(const ExprTable& x) const
{
	type_t t0(get_type()), t1(x.get_type());
	if (t0 < t1)
		return -1;
	if (t1 < t0)
		return 1;
        return 0;
}

int CartoSQL::ExprTable::compare(const const_ptr_t& p0, const const_ptr_t& p1)
{
	if (p0) {
		if (p1) {
			int c(p0->compare(*p1));
			if (c)
				return c;
		} else {
			return 1;
		}
	} else {
		if (p1)
			return -1;
	}
	return 0;
}

std::ostream& CartoSQL::ExprTable::print_expr(std::ostream& os, const const_ptr_t& e, unsigned int indent, const PrintContext& ctx)
{
	if (!e)
		return os << "?unsupported?";
	return e->print(os << '(', indent, ctx) << ')';
}

CartoSQL::ExprTable::const_ptr_t CartoSQL::ExprTable::replace_variable(Calc& c) const
{
	ReplaceVariable vis(c);
	return simplify(c.get_csql(), vis);
}

boost::intrusive_ptr<CartoSQL::ExprSelect> CartoSQL::ExprTable::select_from(void) const
{
	boost::intrusive_ptr<ExprSelect> p(new ExprSelect());
	p->set_columns(get_columns());
	for (Columns::size_type i(0), n(p->get_columns().size()); i < n; ++i) {
		Column& col(p->get_columns()[i]);
		col.set_expr(new ExprTableColumn(1, i));
	}
	p->set_fromexpr(get_ptr());
	return p;
}

CartoSQL::ExprTable::const_ptr_t CartoSQL::ExprTable::filter(const CartoSQL *csql, const Expr::const_ptr_t& expr) const
{
	if (!csql || !expr)
		return const_ptr_t();
	boost::intrusive_ptr<ExprSelect> p(select_from());
	p->set_whereexpr(expr);
	{
		PushConditionUpResolve resolve(p);
		Expr::const_ptr_t expr1(expr->simplify(csql, resolve));
		if (resolve.is_error())
			return const_ptr_t();
		if (expr1)
			p->set_whereexpr(expr1);
	}
	const_ptr_t p1(p->simplify(csql));
	if (p1)
		return p1;
	return p;
}

bool CartoSQL::ExprTable::is_all_columns(const columnset_t& colsused) const
{
	const unsigned int n(get_columns().size());
	unsigned int i(0);
	columnset_t::const_iterator ci(colsused.begin()), ce(colsused.end());
	for (; ci != ce; ++ci) {
		if (i != *ci)
			break;
		++i;
	}
	return ci == ce && i == n;
}

CartoSQL::ExprTable::const_ptr_t CartoSQL::ExprTable::remove_unused_columns(const CartoSQL *csql, columnset_t& colsused) const
{
	for (unsigned int i(0), n(get_columns().size()); i < n; ++i)
		colsused.insert(i);
	return const_ptr_t();
}

CartoSQL::ExprTable::const_ptr_t CartoSQL::ExprTable::remove_unused_columns(const CartoSQL *csql) const
{
	columnset_t colsused;
	for (unsigned int i(0), n(get_columns().size()); i < n; ++i)
		colsused.insert(i);
	const_ptr_t p(remove_unused_columns(csql, colsused));
	if (!p)
		return const_ptr_t();
	if (is_all_columns(colsused))
		return p;
	throw Error("ExprTable::remove_unused_columns: column error");
}

CartoSQL::ExprTable::const_ptr_t CartoSQL::ExprTable::add_sort_columns(const std::vector<unsigned int>& sortcols) const
{
	boost::intrusive_ptr<CartoSQL::ExprSelect> psel(select_from());
	return psel->add_sort_columns(sortcols);
}

CartoSQL::ExprTable::const_ptr_t CartoSQL::ExprTable::add_all_sort_columns_except(unsigned int colidx) const
{
	std::vector<unsigned int> cols;
	for (unsigned int i(0), n(get_columns().size()); i != n; ++i)
		if (i != colidx)
			cols.push_back(i);
	return add_sort_columns(cols);
}

CartoSQL::ExprTable::const_ptr_t CartoSQL::ExprTable::add_all_sort_columns_except(const std::string& colname) const
{
	unsigned int i(get_columns().size());
	if (!colname.empty()) {
		Columns::size_type j(get_columns().find(ColumnName("", colname)));
		if (j < i)
			i = j;
	}
	return add_all_sort_columns_except(i);
}

void CartoSQL::ExprTableValue::copy(ExprTableValue& x) const
{
	ExprTable::copy(x);
	x.set_table(get_table());
}

std::pair<bool,bool> CartoSQL::ExprTableValue::is_renamed(void) const
{
	std::pair<bool,bool> r(false, false);
	for (Columns::size_type i(0), n(m_columns.size()); i != n; ++i) {
		const Column& col(m_columns[i]);
		r.first = r.first || (col.get_table() != "values");
		if (r.second) {
			if (r.first)
				return r;
			continue;
		}
		std::ostringstream oss;
		oss << "column" << (i + 1);
		r.second = col.get_name() != oss.str();
		if (r.first && r.second)
			return r;
	}
	return r;
}

CartoSQL::ExprTable::const_ptr_t CartoSQL::ExprTableValue::simplify(const CartoSQL *csql) const
{
	const_ptr_t p;
	{
		boost::intrusive_ptr<ExprTableValue> p1(new ExprTableValue());
		copy(*p1);
		p = simplify_tail(csql, p1, simplify_int(csql, get_subtables(), p1));
	}
	return p;
}

CartoSQL::ExprTable::const_ptr_t CartoSQL::ExprTableValue::simplify(const CartoSQL *csql, Visitor& v) const
{
	v.visit(*this);
	const_ptr_t p;
	if (v.descend(*this)) {
		boost::intrusive_ptr<ExprTableValue> p1(new ExprTableValue());
		copy(*p1);
		p = simplify_tail(csql, p1, simplify_int(csql, get_subtables(), p1, v));
	}
	const_ptr_t p1(p ? p->visitor_call_modify(csql, v) : visitor_call_modify(csql, v));
	v.visitend(*this);
	return p1 ? p1 : p;
}

bool CartoSQL::ExprTableValue::calculate(Calc& c, Table& t) const
{
	t = m_table;
	return true;
}

std::ostream& CartoSQL::ExprTableValue::print(std::ostream& os, unsigned int indent, const PrintContext& ctx) const
{
	os << "values ";
	bool subseq(false);
	for (const TableRow& r : m_table) {
		if (subseq)
			os << ", ";
		os << '(';
		subseq = false;
		for (const Value& v : r) {
			if (subseq)
				os << ", ";
			subseq = true;
			os << v.get_sqlstring();
		}
		os << ')';
		subseq = true;
	}
	std::pair<bool,bool> ren(is_renamed());
	if ((!ren.first && !ren.second) || get_columns().empty())
		return os;
	os << " as \"" << get_columns().front().get_table() << '"';
	if (!ren.second)
		return os;
	char sep('(');
	for (const Column& col : get_columns()) {
		os << sep << '"' << col.get_name() << '"';
		sep = ',';
	}
	if (sep != '(')
		os << ')';
	return os;
}

int CartoSQL::ExprTableValue::compare(const ExprTable& x) const
{
	int c(ExprTable::compare(x));
	if (c)
		return c;
	const ExprTableValue& xx(static_cast<const ExprTableValue&>(x));
	return m_table.compare(xx.m_table);
}

CartoSQL::ExprTable::const_ptr_t CartoSQL::ExprTableValue::push_filter_up(const CartoSQL *csql, const Expr::const_ptr_t& expr) const
{
	if (!csql || !expr || expr->is_true())
		return const_ptr_t();
	return filter(csql, expr);
}

CartoSQL::ExprTable::const_ptr_t CartoSQL::ExprTableValue::remove_unused_columns(const CartoSQL *csql, columnset_t& colsused) const
{
	if (is_all_columns(colsused))
		return ExprTable::const_ptr_t();
	boost::intrusive_ptr<ExprTableValue> p1(new ExprTableValue());
	copy(*p1);
	p1->get_columns().remove_unused_columns(colsused);
	p1->m_table = m_table.get_columnset(colsused);
	return p1;
}

const std::string& to_str(CartoSQL::ExprTableSort::Term::direction_t x)
{
	switch (x) {
	case CartoSQL::ExprTableSort::Term::direction_t::ascending:
	{
		static const std::string r("ascending");
		return r;
	}

	case CartoSQL::ExprTableSort::Term::direction_t::descending:
	{
		static const std::string r("descending");
		return r;
	}

	default:
	{
		static const std::string r("?");
		return r;
	}
	}
}

const std::string& to_str(CartoSQL::ExprTableSort::Term::nulls_t x)
{
	switch (x) {
	case CartoSQL::ExprTableSort::Term::nulls_t::default_:
	{
		static const std::string r("default");
		return r;
	}

	case CartoSQL::ExprTableSort::Term::nulls_t::first:
	{
		static const std::string r("first");
		return r;
	}

	case CartoSQL::ExprTableSort::Term::nulls_t::last:
	{
		static const std::string r("last");
		return r;
	}

	default:
	{
		static const std::string r("?");
		return r;
	}
	}
}

void CartoSQL::ExprTableSort::Term::resolve_nulls(void)
{
	if (m_nulls != nulls_t::default_)
		return;
	switch (m_direction) {
	case direction_t::ascending:
		m_nulls = nulls_t::last;
		break;

	case direction_t::descending:
		m_nulls = nulls_t::first;
		break;

	default:
		break;
	}
}

template <typename... Args>
bool CartoSQL::ExprTableSort::Term::simplify_int(const CartoSQL *csql, Args&&... args)
{
	if (!m_expr)
		return false;
	Expr::const_ptr_t p1(m_expr->simplify(csql, std::forward<Args>(args)...));
	if (!p1)
		return false;
	m_expr = p1;
	return true;
}

bool CartoSQL::ExprTableSort::Term::simplify(const CartoSQL *csql)
{
	return simplify_int(csql);
}

bool CartoSQL::ExprTableSort::Term::simplify(const CartoSQL *csql, Visitor& v)
{
	return simplify_int(csql, v);
}

void CartoSQL::ExprTableSort::Term::visit(Visitor& v) const
{
	if (m_expr)
		m_expr->visit(v);
}

std::ostream& CartoSQL::ExprTableSort::Term::print(std::ostream& os, unsigned int indent, const PrintContext& ctx) const
{
	Expr::print_expr(os, m_expr, indent, ctx);
	switch (m_direction) {
	case direction_t::ascending:
		os << " asc";
		break;

	case direction_t::descending:
		os << " desc";
		break;

	default:
		break;
	}
	if ((m_direction == direction_t::ascending && m_nulls == nulls_t::first) ||
	    (m_direction == direction_t::descending && m_nulls == nulls_t::last))
		os << " nulls " << m_nulls;
	return os;
}

int CartoSQL::ExprTableSort::Term::compare(const Term& x) const
{
	int c(Expr::compare(m_expr, x.m_expr));
	if (c)
		return c;
	if (m_direction < x.m_direction)
		return -1;
	if (x.m_direction < m_direction)
		return 1;
	if (m_nulls < x.m_nulls)
		return -1;
	if (x.m_nulls < m_nulls)
		return 1;
	return 0;
}

template <typename... Args>
bool CartoSQL::ExprTableSort::Terms::simplify_int(const CartoSQL *csql, Args&&... args)
{
	bool work(false);
	for (Term& t : *this)
		work = t.simplify(csql, std::forward<Args>(args)...) || work;
	return work;
}

bool CartoSQL::ExprTableSort::Terms::simplify(const CartoSQL *csql)
{
	return simplify_int(csql);
}

bool CartoSQL::ExprTableSort::Terms::simplify(const CartoSQL *csql, Visitor& v)
{
	return simplify_int(csql, v);
}

void CartoSQL::ExprTableSort::Terms::visit(Visitor& v) const
{
	for (const Term& t : *this)
		t.visit(v);
}

std::ostream& CartoSQL::ExprTableSort::Terms::print(std::ostream& os, unsigned int indent, const PrintContext& ctx) const
{
	bool subseq(false);
	for (const Term& t : *this) {
		if (subseq)
			os << ",";
		subseq = true;
		t.print(os << std::endl << std::string(indent, ' '), indent + 2, ctx);
	}
	return os;
}

int CartoSQL::ExprTableSort::Terms::compare(const Terms& x) const
{
	size_type i(x.size()), n(size());
	if (n < i)
		return -1;
	if (i < n)
		return 1;
	for (i = 0; i < n; ++i) {
		int c(operator[](i).compare(x[i]));
		if (c)
			return c;
	}
	return 0;
}

int CartoSQL::ExprTableSort::TermsSorter::compare(const TableRowValues& a, const TableRowValues& b) const
{
	Terms::size_type n(std::min(std::min(a.size(), b.size()), m_terms.size()));
	for (Terms::size_type i(0); i < n; ++i) {
		const Value& ai(a[i]), bi(b[i]);
		const Term& t(m_terms[i]);
		if (ai.is_null()) {
			if (bi.is_null())
				return 0;
			return (t.get_nulls() == Term::nulls_t::first) ? -1 : 1;
		}
		if (bi.is_null())
			return (t.get_nulls() == Term::nulls_t::first) ? 1 : -1;
		int c(ai.compare_value(bi));
		if (!c)
			continue;
		if (t.get_direction() == Term::direction_t::descending)
			return -c;
		return c;
	}
	return 0;
}

void CartoSQL::ExprTableSort::copy(ExprTableSort& x) const
{
	ExprTable::copy(x);
	x.set_terms(get_terms());
	x.set_limit(get_limit());
	x.set_offset(get_offset());
}

template <typename... Args>
bool CartoSQL::ExprTableSort::simplify_order(const CartoSQL *csql, const Expr::subtables_t& subtables, boost::intrusive_ptr<ExprTableSort> p, Args&&... args)
{
	bool work(p->get_terms().simplify(csql, std::forward<Args>(args)...));
	if (p->get_limit()) {
		Expr::const_ptr_t p1(p->get_limit()->simplify(csql, std::forward<Args>(args)...));
		if (p1) {
			work = true;
			p->set_limit(p1);
		}
	}
	if (p->get_offset()) {
		Expr::const_ptr_t p1(p->get_offset()->simplify(csql, std::forward<Args>(args)...));
		if (p1) {
			work = true;
			p->set_offset(p1);
		}
	}
	return work;
}

template <typename... Args>
bool CartoSQL::ExprTableSort::simplify_int(const CartoSQL *csql, const Expr::subtables_t& subtables, boost::intrusive_ptr<ExprTableSort> p, Args&&... args)
{
	bool work(ExprTable::simplify_int(csql, subtables, p, std::forward<Args>(args)...));
	return simplify_order(csql, subtables, p, std::forward<Args>(args)...) || work;
}

void CartoSQL::ExprTableSort::visit(Visitor& v) const
{
	if (m_subtable)
		m_subtable->visit(v);
	m_terms.visit(v);
	if (m_limit)
		m_limit->visit(v);
	if (m_offset)
		m_offset->visit(v);
}

std::ostream& CartoSQL::ExprTableSort::print(std::ostream& os, unsigned int indent, const PrintContext& ctx) const
{
	if (!m_terms.empty())
		m_terms.print(os << std::endl << std::string(indent + 2, ' ') << "order by ", indent + 4, ctx);
	if (m_limit)
		Expr::print_expr(os << std::endl << std::string(indent + 2, ' ') << "limit ", m_limit, indent + 2, ctx);
	if (m_offset)
		Expr::print_expr(os << " offset ", m_offset, indent + 2, ctx);
	return os;
}

int CartoSQL::ExprTableSort::compare(const ExprTable& x) const
{
	int c(ExprTable::compare(x));
	if (c)
		return c;
	const ExprTableSort& xx(static_cast<const ExprTableSort&>(x));
	c = m_terms.compare(xx.m_terms);
	if (c)
		return c;
	c = Expr::compare(m_limit, xx.m_limit);
	if (c)
		return c;
	c = Expr::compare(m_offset, xx.m_offset);
	if (c)
		return c;
	return 0;
}

CartoSQL::ExprTable::const_ptr_t CartoSQL::ExprTableSort::add_sort_columns(const std::vector<unsigned int>& sortcols) const
{
	boost::intrusive_ptr<ExprTableSort> p1(boost::dynamic_pointer_cast<ExprTableSort>(clone()));
	if (!p1)
		throw Error("ExprTableSort: cannot clone");
	std::set<unsigned int> alreadysorted;
	for (const Term& term : p1->get_terms()) {
		if (!term.get_expr() || term.get_expr()->get_type() != Expr::type_t::tablecolumn)
			continue;
		const ExprTableColumn& e(static_cast<const ExprTableColumn&>(*term.get_expr()));
		if (e.get_tableindex())
			continue;
		alreadysorted.insert(e.get_columnindex());
	}
	for (unsigned int colnr : sortcols) {
		if (colnr >= p1->get_columns().size() || alreadysorted.find(colnr) != alreadysorted.end())
			continue;
		Term term(Expr::ptr_t(new ExprTableColumn(0, colnr)), Term::direction_t::ascending, Term::nulls_t::default_);
		term.resolve_nulls();
		p1->get_terms().push_back(term);
		alreadysorted.insert(colnr);
	}
	return p1;
}

bool CartoSQL::ExprTableSort::apply_offset_limit(Calc& c, Table& t) const
{
	if (get_offset()) {
		Value v;
		if (!get_offset()->calculate(c, v)) {
			c.error(*this, "offset");
			return false;
		}
		if (!v.is_integer()) {
			c.error(*this, v, "offset must be integer");
			return false;
		}
		if (v.get_integer() < 0) {
			c.error(*this, v, "offset must be positive");
			return false;
		}
		if (static_cast<Table::size_type>(v.get_integer()) >= t.size())
			t.clear();
		else
			t.erase(t.begin(), t.begin() + v.get_integer());
	}
	if (get_limit()) {
		Value v;
		if (!get_limit()->calculate(c, v)) {
			c.error(*this, "limit");
			return false;
		}
		if (!v.is_integer()) {
			c.error(*this, v, "limit must be integer");
			return false;
		}
		if (v.get_integer() < 0) {
			c.error(*this, v, "limit must be positive");
			return false;
		}
		if (static_cast<Table::size_type>(v.get_integer()) < t.size())
			t.resize(v.get_integer());
	}
	return true;
}

void CartoSQL::ExprOSMTable::copy(ExprOSMTable& x) const
{
	ExprTable::copy(x);
	x.set_table(get_table());
}

void CartoSQL::ExprOSMTable::default_columns(Columns& cols, const CartoSQL& csql, table_t tbl) const
{
	const std::string& tblname(get_tablename());
	std::set<std::string, CaseInsensitiveCompare> colnames;
	cols.clear();
	for (ExprOSMSpecial::field_t f = ExprOSMSpecial::field_t::first; f <= ExprOSMSpecial::field_t::last;
	     f = static_cast<ExprOSMSpecial::field_t>(static_cast<std::underlying_type<ExprOSMSpecial::field_t>::type>(f) + 1)) {
		const std::string& fld(to_str(f));
		Expr::ptr_t expr(new ExprOSMSpecial(f));
        	cols.push_back(Column(tblname, fld, expr, ExprOSMSpecial::get_valuetype(f, tbl)));
		colnames.insert(fld);
	}
	for (const auto& schema : csql.get_osmschema()) {
		if (colnames.find(schema.first) != colnames.end())
			continue;
		Expr::ptr_t expr;
		{
			ExprOSMTag::tagkey_t key(ExprOSMTag::tagkey_t::invalid);
			const OSMDB *osmdb(csql.get_osmdb());
			if (osmdb)
				key = osmdb->find_tagkey(schema.first);
			if (key == ExprOSMTag::tagkey_t::invalid) {
				expr = Expr::ptr_t(new ExprValue(Value()));
			} else {
				expr = Expr::ptr_t(new ExprOSMTag(ExprOSMTag::tag_t::value, key));
				if (schema.second != Value::type_t::string)
					expr = Expr::ptr_t(new ExprCast(expr, schema.second));
			}
		}
		cols.push_back(Column(tblname, schema.first, expr, Value::type_t::string));
		colnames.insert(schema.first);
	}
}

void CartoSQL::ExprOSMTable::compute_columns(const CartoSQL& csql)
{
	default_columns(m_columns, csql, m_table);
}

std::pair<bool,bool> CartoSQL::ExprOSMTable::is_renamed(const CartoSQL *csql) const
{
	if (!csql)
		return std::pair<bool,bool>(true, true);
	Columns cols;
	default_columns(cols, *csql, m_table);
	std::pair<bool,bool> ret(false, false);
	for (Columns::size_type i(0), n(m_columns.size()), j(0), m(cols.size()); i < n; ++i) {
		const Column& col(m_columns[i]);
		if (!col.get_expr())
			throw Error("Internal error: invalid OSM table column");
		for (; j < m; ++j)
			if (cols[j].get_expr() && !col.get_expr()->compare(*cols[j].get_expr()))
				break;
		if (j >= m)
			throw Error("Internal error: invalid OSM table column");
		const Column& cold(cols[j]);
		ret.first = ret.first || (cold.get_table() != col.get_table());
		ret.second = ret.second || (cold.get_name() != col.get_name());
		if (ret.first && ret.second)
			return ret;
	}
	return ret;
}

CartoSQL::ExprTable::const_ptr_t CartoSQL::ExprOSMTable::simplify(const CartoSQL *csql) const
{
	const_ptr_t p;
	{
		boost::intrusive_ptr<ExprOSMTable> p1(new ExprOSMTable());
		copy(*p1);
		p = simplify_tail(csql, p1, simplify_int(csql, get_subtables(), p1));
	}
	return p;
}

CartoSQL::ExprTable::const_ptr_t CartoSQL::ExprOSMTable::simplify(const CartoSQL *csql, Visitor& v) const
{
	v.visit(*this);
	const_ptr_t p;
	if (v.descend(*this)) {
		boost::intrusive_ptr<ExprOSMTable> p1(new ExprOSMTable());
		copy(*p1);
		p = simplify_tail(csql, p1, simplify_int(csql, get_subtables(), p1, v));
	}
	const_ptr_t p1(p ? p->visitor_call_modify(csql, v) : visitor_call_modify(csql, v));
	v.visitend(*this);
	return p1 ? p1 : p;
}

class CartoSQL::ExprOSMTable::OSMCalc : public CalcOverride {
public:
	OSMCalc(Calc& c) : CalcOverride(c), m_osmobj(nullptr) {}
	virtual const TableRow *get_table_row(unsigned int tableindex) { return nullptr; }
	virtual const OSMDB::Object *get_osmobject(void) { return m_osmobj; }
	virtual grouprow_t get_group_rows(void) { return 1; }
	virtual void set_group_row(grouprow_t x) { }
	void set_osmobject(const OSMDB::Object::const_ptr_t& p) { m_osmobj = p.get(); }
	void set_osmobject(const OSMDB::Object *p = nullptr) { m_osmobj = p; }

protected:
	const OSMDB::Object *m_osmobj;
};

bool CartoSQL::ExprOSMTable::get_dbobjs(OSMDB::objects_t& objs, Calc& c, table_t tbl, const ExprTable *expr)
{
	if (!c.get_csql()) {
		if (expr)
			c.error(*expr, "CartoSQL pointer null");
		return false;
	}
	const OSMDB *osmdb(c.get_csql()->get_osmdb());
	if (!osmdb) {
		if (expr)
			c.error(*expr, "no underlying OSM database");
		return false;
	}
	Rect bbox;
	{
		Value v;
		if (!c.get_variable(v, "bbox")) {
			if (expr)
				c.error(*expr, "no bbox");
			return false;
		}
		if (!v.is_rectangle()) {
			if (expr)
				c.error(*expr, v, "bbox must be a rectangle");
			return false;
		}
		bbox = v.get_rectangle();
		if (bbox.is_invalid()) {
			if (expr)
				c.error(*expr, v, "bbox is invalid");
			return false;
		}
	}
	OSMDB::typemask_t mask(static_cast<OSMDB::typemask_t>(1 << static_cast<unsigned int>(tbl)));
	if ((mask & OSMDB::typemask_t::line) != OSMDB::typemask_t::none)
		mask |= OSMDB::typemask_t::road;
	objs = osmdb->find(bbox, mask);
        return true;
}

bool CartoSQL::ExprOSMTable::calculate(Calc& c, Table& t) const
{
	t.clear();
	OSMDB::objects_t objs;
	if (!get_dbobjs(objs, c))
		return false;
	OSMCalc calc(c);
	for (const OSMDB::Object::const_ptr_t& p : objs) {
		if (!p)
			continue;
		TableRow tr;
		tr.reserve(m_columns.size());
		calc.set_osmobject(p);
		for (const Column& col : m_columns) {
			tr.push_back(Value());
			if (!col.get_expr())
				continue;
			if (!col.get_expr()->calculate(calc, tr.back())) {
				c.error(*this, "column expression error");
				t.clear();
				return false;
			}
		}
		tr.add_osmidset(p->get_id());
		t.push_back(tr);
	}
	return true;
}

bool CartoSQL::ExprOSMTable::parse_tablename(table_t& t, const std::string& str)
{
	for (table_t tbl(table_t::point); tbl <= table_t::area;
	     tbl = static_cast<table_t>(static_cast<std::underlying_type<table_t>::type>(tbl) + 1)) {
		if (!boost::iequals(get_tablename(tbl), str))
			continue;
		t = tbl;
		return true;
	}
	return false;
}

const std::string& CartoSQL::ExprOSMTable::get_tablename(table_t t)
{
	switch (t) {
	case table_t::point:
	{
		static const std::string r("planet_osm_point");
		return r;
	}

	case table_t::line:
	{
		static const std::string r("planet_osm_line");
		return r;
	}

	case table_t::road:
	{
		static const std::string r("planet_osm_roads");
		return r;
	}

	case table_t::area:
	{
		static const std::string r("planet_osm_polygon");
		return r;
	}

	default:
	{
		static const std::string r("?planet_osm?");
		return r;
	}
	}
}

std::ostream& CartoSQL::ExprOSMTable::print(std::ostream& os, unsigned int indent, const PrintContext& ctx) const
{
	os << get_tablename();
	std::pair<bool,bool> ren(is_renamed(ctx.get_csql()));
	if ((!ren.first && !ren.second) || get_columns().empty())
		return os;
	os << " as \"" << get_columns().front().get_table() << '"';
	if (!ren.second)
		return os;
	char sep('(');
	for (const Column& col : get_columns()) {
		os << sep << '"' << col.get_name() << '"';
		sep = ',';
	}
	if (sep != '(')
		os << ')';
	return os;
}

int CartoSQL::ExprOSMTable::compare(const ExprTable& x) const
{
	int c(ExprTable::compare(x));
	if (c)
		return c;
	const ExprOSMTable& xx(static_cast<const ExprOSMTable&>(x));
	if (m_table < xx.m_table)
		return -1;
	if (xx.m_table < m_table)
		return 1;
	return 0;
}

CartoSQL::ExprTable::const_ptr_t CartoSQL::ExprOSMTable::push_filter_up(const CartoSQL *csql, const Expr::const_ptr_t& expr) const
{
	static constexpr bool debug = false;
	if (!csql || !expr || expr->is_true())
		return const_ptr_t();
	boost::intrusive_ptr<ExprOSMSelect> p(new ExprOSMSelect());
	p->set_columns(get_columns());
	p->set_from(get_table());
	{
		PushConditionUpResolve resolve(p);
		Expr::const_ptr_t expr1(expr->simplify(csql, resolve));
		if (resolve.is_error())
			return const_ptr_t();
		if (expr1)
			p->set_whereexpr(expr1);
		else
			p->set_whereexpr(expr);
	}
	if (debug) {
		csql->print_expr(std::cerr << "ExprOSMTable::push_filter_up: ", expr, 4) << std::endl;
		csql->print_expr(std::cerr << "  Old: ", get_ptr(), 4) << std::endl;
		csql->print_expr(std::cerr << "  new where: ", p->get_whereexpr(), 4) << std::endl;
	}
	const_ptr_t p1(p->simplify(csql));
	if (p1)
		return p1;
	return p;
}

CartoSQL::ExprTable::const_ptr_t CartoSQL::ExprOSMTable::remove_unused_columns(const CartoSQL *csql, columnset_t& colsused) const
{
	if (is_all_columns(colsused))
		return ExprTable::const_ptr_t();
	boost::intrusive_ptr<ExprOSMTable> p1(new ExprOSMTable());
	copy(*p1);
	p1->get_columns().remove_unused_columns(colsused);
	return p1;
}

void CartoSQL::ExprLiteralTable::copy(ExprLiteralTable& x) const
{
	ExprTableSort::copy(x);
	x.set_data(get_data());
}

void CartoSQL::ExprLiteralTable::compute_columns(void)
{
	exprlist_t::size_type ncol(0);
	std::vector<Value::type_t> types;
	std::vector<bool> typeset;
	for (const exprlist_t& e : m_data) {
		ncol = std::max(ncol, e.size());
		types.resize(ncol, Value::type_t::null);
		typeset.resize(ncol, false);
		for (exprlist_t::size_type i = 0, n = e.size(); i < n; ++i) {
			if (!typeset[i]) {
				typeset[i] = true;
				if (e[i])
					types[i] = e[i]->calculate_type();
				continue;
			}
			if (types[i] == Value::type_t::null || !e[i])
				continue;
			Value::type_t t(e[i]->calculate_type());
			if (t == Value::type_t::null || (t == Value::type_t::floatingpoint && types[i] == Value::type_t::integer)) {
				types[i] = t;
				continue;
			}
			if (types[i] == Value::type_t::floatingpoint && t == Value::type_t::integer)
				continue;
			if (types[i] == t)
				continue;
			types[i] = Value::type_t::null;
		}
	}
	m_columns.clear();
	m_columns.resize(ncol);
	for (exprlist_t::size_type i = 0; i < ncol; ++i) {
		Column& col(m_columns[i]);
		std::ostringstream oss;
		oss << "column" << (i + 1);
		col.set_name(oss.str());
		col.set_table("values");
		col.set_type(types[i]);
	}
}

std::pair<bool,bool> CartoSQL::ExprLiteralTable::is_renamed(void) const
{
	std::pair<bool,bool> r(false, false);
	for (Columns::size_type i(0), n(m_columns.size()); i != n; ++i) {
		const Column& col(m_columns[i]);
		r.first = r.first || (col.get_table() != "values");
		if (r.second) {
			if (r.first)
				return r;
			continue;
		}
		std::ostringstream oss;
		oss << "column" << (i + 1);
		r.second = col.get_name() != oss.str();
		if (r.first && r.second)
			return r;
	}
	return r;
}

template <typename... Args>
bool CartoSQL::ExprLiteralTable::simplify_int(const CartoSQL *csql, const Expr::subtables_t& subtables, boost::intrusive_ptr<ExprLiteralTable> p, Args&&... args)
{
	bool work(ExprTableSort::simplify_int(csql, subtables, p, std::forward<Args>(args)...));
	for (exprlist_t& e : p->m_data) {
		for (Expr::const_ptr_t& p : e) {
			Expr::const_ptr_t p1(p->simplify(csql, std::forward<Args>(args)...));
			if (!p1)
				continue;
			p = p1;
			work = true;
		}
	}
	return work;
}

CartoSQL::ExprTable::const_ptr_t CartoSQL::ExprLiteralTable::simplify(const CartoSQL *csql) const
{
	const_ptr_t p;
	{
		boost::intrusive_ptr<ExprLiteralTable> p1(new ExprLiteralTable());
		copy(*p1);
		p = simplify_tail(csql, p1, simplify_int(csql, get_subtables(), p1));
	}
	return p;
}

CartoSQL::ExprTable::const_ptr_t CartoSQL::ExprLiteralTable::simplify(const CartoSQL *csql, Visitor& v) const
{
	v.visit(*this);
	const_ptr_t p;
	{
		boost::intrusive_ptr<ExprLiteralTable> p1(new ExprLiteralTable());
		copy(*p1);
		p = simplify_tail(csql, p1, simplify_int(csql, get_subtables(), p1, v));
	}
	const_ptr_t p1(p ? p->visitor_call_modify(csql, v) : visitor_call_modify(csql, v));
	v.visitend(*this);
	return p1 ? p1 : p;
}

void CartoSQL::ExprLiteralTable::visit(Visitor& v) const
{
	v.visit(*this);
	for (const exprlist_t& e : m_data)
		for (const Expr::const_ptr_t& p : e)
			if (p)
				p->visit(v);
	ExprTableSort::visit(v);
	v.visitend(*this);
}

bool CartoSQL::ExprLiteralTable::calculate(Calc& c, Table& t) const
{
	t.clear();
	for (const exprlist_t& e : m_data) {
		TableRow row;
		for (const Expr::const_ptr_t& p : e) {
			if (!p)
				row.push_back(Value());
			Value v1;
			if (!p->calculate(c, v1)) {
				c.error(*this, "argument error");
				return false;
			}
			row.push_back(v1);
		}
		t.push_back(row);
	}
	// ORDER BY
	if (!get_terms().empty()) {
		CalcStaticRows calc(c);
		IndexTable order;
		{
			IndexTable::size_type n(t.size());
			for (IndexTable::size_type i(0); i < n; ++i) {
				calc.set_table_row(0, &t[i]);
				order.push_back(IndexTableRow());
				order.back().set_index(i);
				order.back().reserve(get_terms().size());
				for (const Term& trm : get_terms()) {
					order.back().push_back(Value());
					if (!trm.get_expr())
						continue;
					if (!trm.get_expr()->calculate(calc, order.back().back())) {
						c.error(*this, "order by expression");
						return false;
					}
				}
			}
		}
		{
			TermsSorter sorter(get_terms());
			std::stable_sort(order.begin(), order.end(), sorter);
		}
		order.permute(t);
	}
	return apply_offset_limit(c, t);
}

std::ostream& CartoSQL::ExprLiteralTable::print(std::ostream& os, unsigned int indent, const PrintContext& ctx) const
{
	os << "values ";
	bool subseq(false);
	for (const exprlist_t& e : m_data) {
		if (subseq)
			os << ", ";
		os << '(';
		subseq = false;
		for (const Expr::const_ptr_t& p : e) {
			if (subseq)
				os << ", ";
			subseq = true;
			Expr::print_expr(os, p, indent + 2, ctx);
		}
		os << ')';
		subseq = true;
	}
	ExprTableSort::print(os, indent, ctx);
	std::pair<bool,bool> ren(is_renamed());
	if ((!ren.first && !ren.second) || get_columns().empty())
		return os;
	os << " as \"" << get_columns().front().get_table() << '"';
	if (!ren.second)
		return os;
	char sep('(');
	for (const Column& col : get_columns()) {
		os << sep << '"' << col.get_name() << '"';
		sep = ',';
	}
	if (sep != '(')
		os << ')';
	return os;
}

int CartoSQL::ExprLiteralTable::compare(const ExprTable& x) const
{
	int c(ExprTableSort::compare(x));
	if (c)
		return c;
	const ExprLiteralTable& xx(static_cast<const ExprLiteralTable&>(x));
	{
		literaltable_t::size_type i(xx.m_data.size()), n(m_data.size());
		if (n < i)
			return -1;
		if (i < n)
			return 1;
		for (i = 0; i < n; ++i) {
			int c(Expr::compare(m_data[i], xx.m_data[i]));
			if (c)
				return c;
		}
	}
	return 0;
}

CartoSQL::ExprTable::const_ptr_t CartoSQL::ExprLiteralTable::push_filter_up(const CartoSQL *csql, const Expr::const_ptr_t& expr) const
{
	if (!csql || !expr || expr->is_true())
		return const_ptr_t();
	return filter(csql, expr);
}

CartoSQL::ExprTable::const_ptr_t CartoSQL::ExprLiteralTable::remove_unused_columns(const CartoSQL *csql, columnset_t& colsused) const
{
	// add those columns needed by ordering etc.
	{
		TableColumnSets colset;
		visit(colset);
		const columnset_t& cs(colset.get_columnset(0));
		colsused.insert(cs.begin(), cs.end());
	}
	if (is_all_columns(colsused))
		return ExprTable::const_ptr_t();
	boost::intrusive_ptr<ExprLiteralTable> p1(new ExprLiteralTable());
	copy(*p1);
	p1->get_columns().remove_unused_columns(colsused);
	{
		RenumberTableColumns ren(0, colsused);
		ExprTable::const_ptr_t p2(p1->simplify(csql, ren));
		if (p2) {
			if (p2->get_type() != type_t::select)
				throw Error("ExprLiteralTable::remove_unused_columns: renumber changed type");
			p1 = boost::static_pointer_cast<ExprLiteralTable>(boost::const_pointer_cast<ExprTable>(p2));
		}
	}
	literaltable_t data;
	data.reserve(m_data.size());
	for (const exprlist_t& el : m_data) {
		data.push_back(exprlist_t());
		for (unsigned int colnr : colsused) {
			if (colnr >= el.size())
				break;
			data.back().push_back(el[colnr]);
		}
	}
	p1->set_data(std::move(data));
	return p1;
}

const std::string& to_str(CartoSQL::ExprTableFunc::func_t t)
{
	switch (t) {
	// table
	case CartoSQL::ExprTableFunc::func_t::unnest:
	{
		static const std::string r("unnest");
		return r;
	}

	case CartoSQL::ExprTableFunc::func_t::generate_series:
	{
		static const std::string r("generate_series");
		return r;
	}

	default:
	{
		static const std::string r("?");
		return r;
	}
	}
}

void CartoSQL::ExprTableFunc::copy(ExprTableFunc& x) const
{
	ExprTable::copy(x);
	x.set_func(get_func());
	x.set_expr(get_expr());
}

void CartoSQL::ExprTableFunc::compute_columns(void)
{
	const std::string& tblname(to_str(m_func));
	m_columns.clear();
	m_columns.resize(1);
	Column& col(m_columns[0]);
	col.set_name(tblname);
	col.set_table(tblname);
	col.set_type(m_func == func_t::generate_series ? Value::type_t::floatingpoint : Value::type_t::null);
}

std::pair<bool,bool> CartoSQL::ExprTableFunc::is_renamed(void) const
{
	std::pair<bool,bool> r(false, false);
	const std::string& tblname(to_str(m_func));
	for (const Column& col : m_columns) {
		r.first = r.first || (col.get_table() != tblname);
		r.second = r.second || (col.get_name() != tblname);
		if (r.first && r.second)
			return r;
	}
	return r;
}

template <typename... Args>
bool CartoSQL::ExprTableFunc::simplify_int(const CartoSQL *csql, const Expr::subtables_t& subtables, boost::intrusive_ptr<ExprTableFunc> p, Args&&... args)
{
	bool work(ExprTable::simplify_int(csql, subtables, p, std::forward<Args>(args)...));
	for (Expr::const_ptr_t& p : p->get_expr()) {
		Expr::const_ptr_t p1(p->simplify(csql, std::forward<Args>(args)...));
		if (!p1)
			continue;
		p = p1;
		work = true;
	}
	return work;
}

CartoSQL::ExprTable::const_ptr_t CartoSQL::ExprTableFunc::simplify(const CartoSQL *csql) const
{
	const_ptr_t p;
	{
		boost::intrusive_ptr<ExprTableFunc> p1(new ExprTableFunc());
		copy(*p1);
		p = simplify_tail(csql, p1, simplify_int(csql, get_subtables(), p1));
	}
	return p;
}

CartoSQL::ExprTable::const_ptr_t CartoSQL::ExprTableFunc::simplify(const CartoSQL *csql, Visitor& v) const
{
	v.visit(*this);
	const_ptr_t p;
	if (v.descend(*this)) {
		boost::intrusive_ptr<ExprTableFunc> p1(new ExprTableFunc());
		copy(*p1);
		p = simplify_tail(csql, p1, simplify_int(csql, get_subtables(), p1, v));
	}
	const_ptr_t p1(p ? p->visitor_call_modify(csql, v) : visitor_call_modify(csql, v));
	v.visitend(*this);
	return p1 ? p1 : p;
}

void CartoSQL::ExprTableFunc::visit(Visitor& v) const
{
	v.visit(*this);
	if (v.descend(*this))
		for (const Expr::const_ptr_t& p : m_expr)
			if (p)
				p->visit(v);
	v.visitend(*this);
}

std::ostream& CartoSQL::ExprTableFunc::print(std::ostream& os, unsigned int indent, const PrintContext& ctx) const
{
	os << m_func << '(';
	bool subseq(false);
	for (const Expr::const_ptr_t& e : m_expr) {
		if (subseq)
			os << ", ";
		subseq = true;
		Expr::print_expr(os, e, indent + 2, ctx);
	}
	os << ')';
	std::pair<bool,bool> ren(is_renamed());
	if ((!ren.first && !ren.second) || get_columns().empty())
		return os;
	os << " as \"" << get_columns().front().get_table() << '"';
	if (!ren.second)
		return os;
	char sep('(');
	for (const Column& col : get_columns()) {
		os << sep << '"' << col.get_name() << '"';
		sep = ',';
	}
	if (sep != '(')
		os << ')';
	return os;
}

int CartoSQL::ExprTableFunc::compare(const ExprTable& x) const
{
	int c(ExprTable::compare(x));
	if (c)
		return c;
	const ExprTableFunc& xx(static_cast<const ExprTableFunc&>(x));
	if (m_func < xx.m_func)
		return -1;
	if (xx.m_func < m_func)
		return 1;
	exprlist_t::size_type i(xx.m_expr.size()), n(m_expr.size());
	if (n < i)
		return -1;
	if (i < n)
		return 1;
	for (i = 0; i < n; ++i) {
		int c(Expr::compare(m_expr[i], xx.m_expr[i]));
		if (c)
			return c;
	}
	return 0;
}

bool CartoSQL::ExprTableFunc::calculate(Calc& c, Table& t) const
{
	t.clear();
	switch (get_func()) {
	case func_t::unnest:
	{
		if (m_expr.size() != 1 || !m_expr[0]) {
			c.error(*this, "one argument expected");
			return false;
		}
		Value v;
		if (!m_expr[0]->calculate(c, v)) {
			c.error(*this, "argument");
			return false;
		}
		if (v.is_null())
			return true;
		if (!v.is_array()) {
			c.error(*this, v, "array expected");
			return false;
		}
		t.reserve(v.get_array().size());
		for (const Value& val : v.get_array()) {
			TableRow tr;
			tr.push_back(val);
			t.push_back(tr);
		}
		return true;
	}

	case func_t::generate_series:
	{
		if (m_expr.size() < 2 || m_expr.size() > 3 || !m_expr[0] || !m_expr[1]) {
			c.error(*this, "two or three arguments expected");
			return false;
		}
		ExprFunc::valuelist_t vl;
		vl.resize(2);
		vl[1] = static_cast<int64_t>(1);
		Value vend;
		if (!m_expr[0]->calculate(c, vl[0]) ||
		    !m_expr[1]->calculate(c, vend)) {
			c.error(*this, "argument");
			return false;
		}
		if (!vl[0].is_numeric()) {
			c.error(*this, vl[0], "numeric start argument required");
			return false;
		}
		if (!vend.is_numeric()) {
			c.error(*this, vend, "numeric end argument required");
			return false;
		}
		if (m_expr.size() >= 3 && m_expr[2]) {
			if (!m_expr[2]->calculate(c, vl[1])) {
				c.error(*this, "argument");
				return false;
			}
			if (vl[1].is_null()) {
				vl[1] = static_cast<int64_t>(1);
			} else if (!vl[1].is_numeric()) {
				c.error(*this, vl[1], "numeric increment argument required");
				return false;
			}
		}
		ExprFunc::func_t cmpop(ExprFunc::func_t::equal);
		{
			Value v;
			if (!ExprFunc::calculate_comparison(v, ExprFunc::func_t::lessthan, vl[1], Value(static_cast<int64_t>(0)))) {
				c.error(*this, "comparison error");
				return false;
			}
			if (!v.is_boolean()) {
				c.error(*this, v, "comparison not boolean");
				return false;
			}
			if (v.get_boolean())
				cmpop = ExprFunc::func_t::greaterorequal;
		}
		if (cmpop == ExprFunc::func_t::equal) {
			Value v;
			if (!ExprFunc::calculate_comparison(v, ExprFunc::func_t::greaterthan, vl[1], Value(static_cast<int64_t>(0)))) {
				c.error(*this, "comparison error");
				return false;
			}
			if (!v.is_boolean()) {
				c.error(*this, v, "comparison not boolean");
				return false;
			}
			if (v.get_boolean())
				cmpop = ExprFunc::func_t::lessorequal;
		}
		if (cmpop == ExprFunc::func_t::equal) {
			c.error(*this, "increment cannot be zero");
			return false;
		}
		for (;;) {
			{
				Value v;
				if (!ExprFunc::calculate_comparison(v, cmpop, vl[0], vend)) {
					t.clear();
					c.error(*this, "comparison error");
					return false;
				}
				if (!v.is_boolean()) {
					t.clear();
					c.error(*this, v, "comparison not boolean");
					return false;
				}
				if (!v.get_boolean())
					break;
			}
			{
				TableRow tr;
				tr.push_back(vl[0]);
				t.push_back(tr);
			}
			{
				Value v;
				if (!ExprFunc::calculate(v, ExprFunc::func_t::binaryplus, vl)) {
					t.clear();
					c.error(*this, "add error");
					return false;
				}
				vl[0] = v;
			}
		}
		return true;
	}

	default:
		c.error(*this, "not implemented");
		return false;
	}
}

const std::string& to_str(CartoSQL::ExprSelectBase::distinct_t x)
{
	switch (x) {
	case CartoSQL::ExprSelectBase::distinct_t::all:
	{
		static const std::string r("all");
		return r;
	}

	case CartoSQL::ExprSelectBase::distinct_t::distinctrows:
	{
		static const std::string r("distinctrows");
		return r;
	}

	case CartoSQL::ExprSelectBase::distinct_t::distinctexpr:
	{
		static const std::string r("distinctexpr");
		return r;
	}

	default:
	{
		static const std::string r("?");
		return r;
	}
	}
}

constexpr char CartoSQL::ExprSelectBase::default_tablename[];

CartoSQL::ExprSelectBase::ExprSelectBase(void)
	: m_distinct(distinct_t::all)
{
}

void CartoSQL::ExprSelectBase::copy(ExprSelectBase& x) const
{
	ExprTableSort::copy(x);
	x.set_distinctexpr(get_distinctexpr());
	x.set_distinct(get_distinct());
	x.set_whereexpr(get_whereexpr());
	x.set_groupexpr(get_groupexpr());
	x.set_havingexpr(get_havingexpr());
}

std::string CartoSQL::ExprSelectBase::default_column_name(const Expr::const_ptr_t& expr, const ExprTable::const_ptr_t& fromexpr)
{
	if (!expr)
		return std::string();
	switch (expr->get_type()) {
	case Expr::type_t::column:
	{
		boost::intrusive_ptr<const ExprColumn> p(boost::static_pointer_cast<const ExprColumn>(expr));
		return p->get_column();
	}

	case Expr::type_t::tablecolumn:
	{
		if (!fromexpr)
			return std::string();
		boost::intrusive_ptr<const ExprTableColumn> p(boost::static_pointer_cast<const ExprTableColumn>(expr));
		if (!p->get_tableindex() && p->get_columnindex() < fromexpr->get_columns().size())
			return fromexpr->get_columns()[p->get_columnindex()].get_name();
		return std::string();
	}

	case Expr::type_t::osmspecial:
	{
		boost::intrusive_ptr<const ExprOSMSpecial> p(boost::static_pointer_cast<const ExprOSMSpecial>(expr));
		return to_str(p->get_field());
	}

	case Expr::type_t::osmtag:
	{
		return to_str(ExprOSMSpecial::field_t::tags);
	}

	case Expr::type_t::function:
	{
		boost::intrusive_ptr<const ExprFunc> p(boost::static_pointer_cast<const ExprFunc>(expr));
		if (p->get_func() > ExprFunc::func_t::postfixoperatorend)
			return to_str(p->get_func());
		return std::string();
	}

	case Expr::type_t::case_:
	{
		boost::intrusive_ptr<const ExprCase> p(boost::static_pointer_cast<const ExprCase>(expr));
		std::string name(default_column_name(p->get_else(), fromexpr));
		if (!name.empty())
			return name;
		for (const ExprCase::When& when : p->get_when()) {
			name = default_column_name(when.get_value(), fromexpr);
			if (!name.empty())
				return name;
		}
		return std::string();
	}

	default:
		return std::string();
	}
}

void CartoSQL::ExprSelectBase::compute_columns(const ExprTable::const_ptr_t& fromexpr)
{
	if (!fromexpr) {
		for (Columns::size_type i(0), n(m_columns.size()); i < n; ++i) {
			Column& col(m_columns[i]);
			if (col.is_wildcard())
				throw Error("wildcard column " + col.to_str() + " but no from clause");
			if (!col.get_name().empty())
				continue;
			if (col.get_name().empty()) {
				std::string name(default_column_name(col.get_expr(), fromexpr));
				if (name.empty())
					name = "?column?";
				col.set_name(name);
			}
		}
		m_columns.set_table(default_tablename);
		return;
	}
	Columns cols;
	for (Columns::size_type i(0), n(m_columns.size()); i < n; ++i) {
		const Column& colm(m_columns[i]);
		if (colm.is_wildcard()) {
			bool found(false);
			const Columns& xcols(fromexpr->get_columns());
			for (Columns::size_type i(0), n(xcols.size()); i < n; ++i) {
				const Column& col(xcols[i]);
				if (!colm.is_match(col))
					continue;
				Column col1(col);
				col1.set_expr(new ExprTableColumn(1, i));
				cols.push_back(col1);
				found = true;
			}
			if (!found)
				throw Error("column " + colm.to_str() + " not found");
			continue;
		}
		Column col(colm);
		if (col.get_expr()) {
			Expr::subtables_t subtables;
			subtables.push_back(fromexpr);
			col.set_type(col.get_expr()->calculate_type(subtables));
			if (false) {
				std::cerr << "ExprSelectBase::compute_columns: type " << Value::get_sqltypename(col.get_type());
				col.get_expr()->print(std::cerr << " expr:" << std::endl << "  ", 2, PrintContextSQL()) << std::endl;
			}
		}
		if (col.get_name().empty()) {
			std::string name(default_column_name(col.get_expr(), fromexpr));
			if (name.empty())
				name = "?column?";
			col.set_name(name);
		}
		cols.push_back(col);
	}
	cols.set_table(default_tablename);
	m_columns.swap(cols);
}

template <typename... Args>
bool CartoSQL::ExprSelectBase::simplify_preresult(const CartoSQL *csql, const Expr::subtables_t& subtables, boost::intrusive_ptr<ExprSelectBase> p, Args&&... args)
{
	bool work(false);
	// WHERE
	if (p->get_whereexpr()) {
		Expr::const_ptr_t p1(p->get_whereexpr()->simplify(csql, std::forward<Args>(args)...));
		if (p1) {
			work = true;
			p->set_whereexpr(p1);
		}
	}
	// GROUP BY
	for (Expr::const_ptr_t& p : p->get_groupexpr()) {
		Expr::const_ptr_t p1(p->simplify(csql, std::forward<Args>(args)...));
		if (!p1)
			continue;
		p = p1;
		work = true;
	}
	// HAVING
	if (p->get_havingexpr()) {
		Expr::const_ptr_t p1(p->get_havingexpr()->simplify(csql, std::forward<Args>(args)...));
		if (p1) {
			work = true;
			p->set_havingexpr(p1);
		}
	}
	// SELECT
	return ExprTable::simplify_int(csql, subtables, p, std::forward<Args>(args)...) || work;
}

template <typename... Args>
bool CartoSQL::ExprSelectBase::simplify_postresult(const CartoSQL *csql, const Expr::subtables_t& subtables, boost::intrusive_ptr<ExprSelectBase> p, Args&&... args)
{
	bool work(false);
	// DISTINCT
	for (Expr::const_ptr_t& p : p->get_distinctexpr()) {
		Expr::const_ptr_t p1(p->simplify(csql, std::forward<Args>(args)...));
		if (!p1)
			continue;
		p = p1;
		work = true;
	}
	// ORDER BY, LIMIT / OFFSET
	return simplify_order(csql, subtables, p, std::forward<Args>(args)...) || work;
}

template <typename... Args>
bool CartoSQL::ExprSelectBase::simplify_int(const CartoSQL *csql, const Expr::subtables_t& subtables, boost::intrusive_ptr<ExprSelectBase> p, Args&&... args)
{
	bool work(simplify_preresult(csql, subtables, p, std::forward<Args>(args)...));
	return simplify_postresult(csql, subtables, p, std::forward<Args>(args)...) || work;
}

void CartoSQL::ExprSelectBase::visit(Visitor& v) const
{
	m_columns.visit(v);
	for (const Expr::const_ptr_t& p : m_distinctexpr)
		p->visit(v);
	if (m_whereexpr)
		m_whereexpr->visit(v);
	for (const Expr::const_ptr_t& p : m_groupexpr)
		p->visit(v);
	if (m_havingexpr)
		m_havingexpr->visit(v);
	ExprTableSort::visit(v);
}

std::ostream& CartoSQL::ExprSelectBase::print1(std::ostream& os, unsigned int indent, const PrintContext& ctx) const
{
	os << std::endl << std::string(indent, ' ') << "select";
	switch (m_distinct) {
	case distinct_t::all:
		os << " all";
		break;

	case distinct_t::distinctrows:
		os << " distinct";
		break;

	case distinct_t::distinctexpr:
		os << " distinct on (";
		{
			bool subseq(false);
			for (const Expr::const_ptr_t& e : m_distinctexpr) {
				if (subseq)
					os << ", ";
				subseq = true;
				Expr::print_expr(os, e, indent + 2, ctx);
			}
		}
		os << ')';
		break;

	default:
		break;
	}
	PrintContextHideFirstTable ctx1(ctx);
	return m_columns.print(os, indent + 4, ctx1);
}

std::ostream& CartoSQL::ExprSelectBase::print2(std::ostream& os, unsigned int indent, const PrintContext& ctx) const
{
	PrintContextHideFirstTable ctx1(ctx);
	if (m_whereexpr)
		Expr::print_expr(os << std::endl << std::string(indent + 2, ' ') << "where ", m_whereexpr, indent + 2, ctx1);
	if (!m_groupexpr.empty())
		Expr::print_expr(os << std::endl << std::string(indent + 2, ' ') << "group by ", m_groupexpr, indent + 2, ctx1);
	if (m_havingexpr)
		Expr::print_expr(os << std::endl << std::string(indent + 2, ' ') << "having ", m_havingexpr, indent + 2, ctx1);
	return ExprTableSort::print(os, indent, ctx);
}

int CartoSQL::ExprSelectBase::compare(const ExprTable& x) const
{
	int c(ExprTableSort::compare(x));
	if (c)
		return c;
	const ExprSelectBase& xx(static_cast<const ExprSelectBase&>(x));
	c = m_columns.compare(xx.m_columns);
	if (c)
		return c;
	c = Expr::compare(m_distinctexpr, xx.m_distinctexpr);
	if (c)
		return c;
	c = Expr::compare(m_whereexpr, xx.m_whereexpr);
	if (c)
		return c;
	c = Expr::compare(m_groupexpr, xx.m_groupexpr);
	if (c)
		return c;
	c = Expr::compare(m_havingexpr, xx.m_havingexpr);
	if (c)
		return c;
	if (m_distinct < xx.m_distinct)
		return -1;
	if (xx.m_distinct < m_distinct)
		return 1;
	return 0;
}

CartoSQL::ExprSelect::ExprSelect(void)
{
}

void CartoSQL::ExprSelect::copy(ExprSelect& x) const
{
	ExprSelectBase::copy(x);
	x.set_fromexpr(get_fromexpr());
}

void CartoSQL::ExprSelect::compute_columns(void)
{
	ExprSelectBase::compute_columns(get_fromexpr());
}

class CartoSQL::ExprSelect::ConvertOSM : public Visitor {
public:
	ConvertOSM(const ExprTable::const_ptr_t& from) : m_fromexpr(from) {}
	virtual Expr::const_ptr_t modify(const CartoSQL *csql, const ExprTableColumn& e);

protected:
	ExprTable::const_ptr_t m_fromexpr;
};

CartoSQL::Expr::const_ptr_t CartoSQL::ExprSelect::ConvertOSM::modify(const CartoSQL *csql, const ExprTableColumn& e)
{
	if (e.get_tableindex() >= 2)
		return Expr::ptr_t(new ExprTableColumn(e.get_tableindex() - 1, e.get_columnindex()));
	if (!e.get_tableindex())
		return Expr::const_ptr_t();
	if (!m_fromexpr)
		throw Error("null from expression when converting to OSM");
	const Columns& cols(m_fromexpr->get_columns());
	if (e.get_columnindex() >= cols.size())
		throw Error("column index out of range when converting to OSM");
	const Column& col(cols[e.get_columnindex()]);
	return col.get_expr();
}

boost::intrusive_ptr<CartoSQL::ExprOSMSelect> CartoSQL::ExprSelect::convert_osmselect(const CartoSQL *csql) const
{
	if (!csql || !get_fromexpr() || get_fromexpr()->get_type() != type_t::osmtable)
		return boost::intrusive_ptr<ExprOSMSelect>();
	boost::intrusive_ptr<const ExprOSMTable> posm(boost::static_pointer_cast<const ExprOSMTable>(get_fromexpr()));
	boost::intrusive_ptr<ExprOSMSelect> p(new ExprOSMSelect());
	ExprSelectBase::copy(*p);
	p->set_from(posm->get_table());
	{
		ConvertOSM convosm(posm);
		ExprTable::const_ptr_t p1(p->simplify(csql, convosm));
		if (p1 && p1->get_type() == type_t::osmselect)
			p = boost::static_pointer_cast<ExprOSMSelect>(boost::const_pointer_cast<ExprTable>(p1));
	}
	// fixup way column
	for (Column& col : p->get_columns()) {
		if (!col.get_expr() || col.get_expr()->get_type() != Expr::type_t::osmspecial)
			continue;
		const ExprOSMSpecial& e(*boost::static_pointer_cast<const ExprOSMSpecial>(col.get_expr()));
		col.set_type(e.get_valuetype(p->get_from()));
	}
	if (false) {
		std::cerr << "convert_osmselect: select columns:" << std::endl;
		for (const Column& col : get_columns())
			std::cerr << "  " << col.to_str() << " type " << Value::get_sqltypename(col.get_type()) << std::endl;
		std::cerr << "convert_osmselect: from columns:" << std::endl;
		for (const Column& col : get_fromexpr()->get_columns())
			std::cerr << "  " << col.to_str() << " type " << Value::get_sqltypename(col.get_type()) << std::endl;
	}
	{
		UnresolvedSubtableColRef unresolved;
		p->visit(unresolved);
		typedef std::map<boost::intrusive_ptr<const ExprTableColumn>,ExprTable::const_ptr_t> exprs_t;
		exprs_t exprs;
		for (const auto& x : unresolved.get_expressions()) {
			if (!x.first || x.first->get_type() != Expr::type_t::tablecolumn)
				continue;
			boost::intrusive_ptr<const ExprTableColumn> p(boost::static_pointer_cast<const ExprTableColumn>(x.first));
			if (!p->get_tableindex())
				continue;
			exprs.insert(exprs_t::value_type(p, x.second));
		}
		if (!exprs.empty()) {
			std::ostringstream oss;
			oss << "Unresolved TableColumn while converting select to osmselect: ";
			bool subseq(false);
			for (const auto& p : exprs) {
				if (!p.first)
					continue;
				if (subseq)
					oss << ", ";
				subseq = true;
				p.first->print(oss, 0);
			}
			throw Error(oss.str());
		}
	}
	return p;
}

template <typename... Args>
bool CartoSQL::ExprSelect::simplify_preresult(const CartoSQL *csql, const Expr::subtables_t& subtables, boost::intrusive_ptr<ExprSelect> p, Args&&... args)
{
	bool work(false);
	// FROM
	if (p->get_fromexpr()) {
		ExprTable::const_ptr_t p1(p->get_fromexpr()->simplify(csql, std::forward<Args>(args)...));
		if (p1) {
			work = true;
			p->set_fromexpr(p1);
		}
	}
	return ExprSelectBase::simplify_preresult(csql, subtables, p, std::forward<Args>(args)...) || work;
}

template <typename... Args>
bool CartoSQL::ExprSelect::simplify_int(const CartoSQL *csql, const Expr::subtables_t& subtables, boost::intrusive_ptr<ExprSelect> p, Args&&... args)
{
	bool work(simplify_preresult(csql, subtables, p, std::forward<Args>(args)...));
	return simplify_postresult(csql, subtables, p, std::forward<Args>(args)...) || work;
}

CartoSQL::ExprTable::const_ptr_t CartoSQL::ExprSelect::simplify(const CartoSQL *csql) const
{
	const_ptr_t p;
	{
		boost::intrusive_ptr<ExprSelect> p1(new ExprSelect());
		copy(*p1);
		p = simplify_tail(csql, p1, simplify_int(csql, get_subtables(), p1));
	}
	return p;
}

CartoSQL::ExprTable::const_ptr_t CartoSQL::ExprSelect::simplify(const CartoSQL *csql, Visitor& v) const
{
	v.visit(*this);
	const_ptr_t p;
	if (v.descend(*this)) {
		boost::intrusive_ptr<ExprSelect> p1(new ExprSelect());
		copy(*p1);
		bool work(simplify_preresult(csql, get_subtables(), p1, v));
		v.select_result_table_available(csql, *this);
		p = simplify_tail(csql, p1, simplify_postresult(csql, get_subtables(), p1, v) || work);
	}
	const_ptr_t p1(p ? p->visitor_call_modify(csql, v) : visitor_call_modify(csql, v));
	v.visitend(*this);
	return p1 ? p1 : p;
}

void CartoSQL::ExprSelect::visit(Visitor& v) const
{
	v.visit(*this);
	if (v.descend(*this)) {
		ExprSelectBase::visit(v);
		if (m_fromexpr)
			m_fromexpr->visit(v);
	}
	v.visitend(*this);
}

CartoSQL::Expr::subtables_t CartoSQL::ExprSelect::get_subtables(void) const
{
	Expr::subtables_t subtbl;
	subtbl.push_back(get_ptr());
	if (get_fromexpr()) {
		subtbl.push_back(get_fromexpr());
		Expr::subtables_t subtbl1(get_fromexpr()->get_subtables());
		subtbl.insert(subtbl.end(), subtbl1.begin(), subtbl1.end());
	}
	return subtbl;
}

class CartoSQL::ExprSelect::SelectCalc : public CalcOverride {
public:
	SelectCalc(Calc& c) : CalcOverride(c), m_groupbegin(0), m_groupend(0), m_index(0) {}
	virtual const TableRow *get_table_row(unsigned int tableindex) {
		switch (tableindex) {
		case 0:
			if (m_index >= m_table.size())
				return nullptr;
			return &m_table[m_index];

		case 1:
			if (m_index >= m_from.size())
				return nullptr;
			return &m_from[m_index];

		default:
			return CalcOverride::get_table_row(tableindex - 2);
		}
	}
	virtual const OSMDB::Object *get_osmobject(void) { return nullptr; }
	virtual grouprow_t get_group_rows(void) { return m_groupend - m_groupbegin; }
	virtual void set_group_row(grouprow_t x) {
		if (m_groupend <= m_groupbegin + 1)
			return;
		m_index = m_groupbegin + x;
		if (m_index > m_groupend)
			m_index = m_groupend;
	}
	const Table& get_from(void) const { return m_from; }
	Table& get_from(void) { return m_from; }
	void set_from(const Table& tbl) { m_from = tbl; }
	void set_from(Table&& tbl) { m_from.swap(tbl); }
	const Table& get_table(void) const { return m_table; }
	Table& get_table(void) { return m_table; }
	void set_table(const Table& tbl) { m_table = tbl; }
	void set_table(Table&& tbl) { m_table.swap(tbl); }
	grouprow_t get_groupbegin(void) const { return m_groupbegin; }
	grouprow_t get_groupend(void) const { return m_groupend; }
	void set_group(grouprow_t beg, grouprow_t end) { m_index = m_groupbegin = beg; m_groupend = end; }
	typedef std::tuple<grouprow_t,grouprow_t> group_t;
	group_t get_group(void) const { return std::make_tuple(get_groupbegin(), get_groupend()); }
	void set_group(const group_t& grp) { m_index = m_groupbegin = std::get<0>(grp); m_groupend = std::get<1>(grp); }
	grouprow_t get_index(void) const { return m_index; }
	void set_index(grouprow_t idx) { m_index = idx; }

protected:
	Table m_from;
	Table m_table;
	grouprow_t m_groupbegin;
	grouprow_t m_groupend;
	grouprow_t m_index;
};

bool CartoSQL::ExprSelect::calculate(Calc& c, Table& t) const
{
	t.clear();
	if (get_whereexpr() && get_whereexpr()->is_false())
		return true;
	// single row select
	if (!get_fromexpr()) {
		TableRow tr;
		tr.reserve(m_columns.size());
		for (const Column& col : m_columns) {
			tr.push_back(Value());
			if (!col.get_expr())
				continue;
			if (!col.get_expr()->calculate(c, tr.back())) {
				c.error(*this, "column expression");
				return false;
			}
		}
		t.push_back(std::move(tr));
		return true;
	}
	SelectCalc calc(c);
	// FROM
	if (!get_fromexpr()->calculate(calc, calc.get_from())) {
		c.error(*this, "from expression");
		return false;
	}
	// WHERE
	if (get_whereexpr()) {
		Table from;
		from.reserve(calc.get_from().size());
		calc.set_index(0);
		for (SelectCalc::grouprow_t n(calc.get_from().size()); calc.get_index() < n; calc.set_index(calc.get_index() + 1)) {
			Value val;
			if (!get_whereexpr()->calculate(calc, val)) {
				c.error(*this, "where expression");
				return false;
			}
			if (val.is_null())
				continue;
			if (!val.is_boolean()) {
				c.error(*this, val, "where result is not boolean");
				return false;
			}
			if (!val.get_boolean())
				continue;
			from.push_back(TableRow());
			from.back().swap(calc.get_from()[calc.get_index()]);
		}
		calc.set_from(std::move(from));
	}
	// grouped select
	if (!get_groupexpr().empty() || get_havingexpr() || m_columns.has_aggregate()) {
		typedef SelectCalc::group_t group_t;
		typedef std::vector<group_t> groups_t;
		groups_t groups;
		groups.push_back(group_t{ 0, calc.get_from().size() });
		// GROUP BY
		if (!get_groupexpr().empty()) {
			IndexTable grp;
			grp.reserve(calc.get_from().size());
			calc.set_index(0);
			for (SelectCalc::grouprow_t n(calc.get_from().size()); calc.get_index() < n; calc.set_index(calc.get_index() + 1)) {
				IndexTableRow tr;
				tr.reserve(get_groupexpr().size());
				tr.set_index(calc.get_index());
				for (const Expr::const_ptr_t& expr : get_groupexpr()) {
					tr.push_back(Value());
					if (expr && !expr->calculate(calc, tr.back())) {
						c.error(*this, "group by expression");
						return false;
					}
				}
				grp.push_back(tr);
			}
			std::stable_sort(grp.begin(), grp.end(), IndexTable::ValueSorter());
			grp.permute(calc.get_from());
			groups = grp.get_equalranges();
		}
		Table from;
		calc.get_table().reserve(groups.size());
		for (const group_t& grp : groups) {
			calc.set_group(grp);
			if (!calc.get_group_rows())
				continue;
			// HAVING
			if (get_havingexpr()) {
				Value v;
				if (!get_havingexpr()->calculate(calc, v)) {
					c.error(*this, "having expression");
					return false;
				}
				if (!v.is_boolean()) {
					c.error(*this, v, "having expression does not yield a boolean");
					return false;
				}
				if (!v.get_boolean())
					continue;
			}
			// construct a row
			TableRow tr;
			{
				const TableRow *trf(calc.get_table_row(0));
				if (trf) {
					tr.set_osmidset(trf->get_osmidset());
				}
			}
			tr.reserve(m_columns.size());
			for (const Column& col : m_columns) {
				tr.push_back(Value());
				if (!col.get_expr())
					continue;
				if (!col.get_expr()->calculate(calc, tr.back())) {
					c.error(*this, "column expression");
					return false;
				}
			}
			calc.get_table().push_back(tr);
			from.push_back(TableRow());
			from.back().swap(calc.get_from()[calc.get_index()]);
		}
		calc.set_from(std::move(from));
	} else {
		SelectCalc::grouprow_t n(calc.get_from().size());
		calc.get_table().reserve(n);
		for (calc.set_index(0); calc.get_index() < n; calc.set_index(calc.get_index() + 1)) {
			// construct a row
			TableRow tr;
			{
				const TableRow *trf(calc.get_table_row(1));
				if (trf) {
					tr.set_osmidset(trf->get_osmidset());
				}
			}
			tr.reserve(m_columns.size());
			for (const Column& col : m_columns) {
				tr.push_back(Value());
				if (!col.get_expr())
					continue;
				if (!col.get_expr()->calculate(calc, tr.back())) {
					c.error(*this, "column expression");
					return false;
				}
			}
			calc.get_table().push_back(tr);
		}
	}
	calc.set_group(0, 0);
	// DISTINCT
	switch (get_distinct()) {
	default:
	case distinct_t::all:
		break;

	case distinct_t::distinctrows:
	{
		{
			IndexTable dist;
			IndexTable::size_type n(calc.get_table().size());
			dist.resize(n);
			for (IndexTable::size_type i(0); i < n; ++i) {
				dist[i].TableRowValues::swap(calc.get_table()[i]);
				dist[i].set_index(i);
			}
			std::stable_sort(dist.begin(), dist.end(), IndexTable::ValueSorter());
			dist.permute(calc.get_from());
			dist.permute(calc.get_table());
			for (IndexTable::size_type i(0); i < n; ++i) 
				calc.get_table()[i].TableRowValues::swap(dist[i]);
		}
		Table from;
		from.reserve(calc.get_table().size());
		Table tbl;
		tbl.reserve(calc.get_table().size());
		for (IndexTable::size_type i(0), n(calc.get_table().size()); i < n; ++i) {
			if (i && !tbl.back().compare_value(calc.get_table()[i]))
				continue;
			tbl.push_back(TableRow());
			tbl.back().swap(calc.get_table()[i]);
			from.push_back(TableRow());
			from.back().swap(calc.get_from()[i]);
		}
		calc.set_from(std::move(from));
		calc.set_table(std::move(tbl));
		break;
	}

	case distinct_t::distinctexpr:
	{
		if (get_distinctexpr().empty())
			break;
		IndexTable dist;
		{
			IndexTable::size_type n(calc.get_table().size());
			dist.reserve(n);
			for (IndexTable::size_type i(0); i < n; ++i) {
				dist.push_back(IndexTableRow());
				dist.back().set_index(i);
				dist.back().reserve(get_distinctexpr().size());
				for (const Expr::const_ptr_t& p : get_distinctexpr()) {
					dist.back().push_back(Value());
					if (!p)
						continue;
					if (!p->calculate(calc, dist.back().back())) {
						c.error(*this, "distinct expression");
						return false;
					}
				}
			}
		}
		std::stable_sort(dist.begin(), dist.end(), IndexTable::ValueSorter());
		Table from;
		Table tbl;
		tbl.reserve(dist.size());
		from.reserve(dist.size());
		for (IndexTable::size_type i(0), n(dist.size()); i < n; ++i) {
			if (i && !dist[i].compare_value(dist[i-1]))
				continue;
			IndexTableRow::index_t idx(dist[i].get_index());
			tbl.push_back(TableRow());
			tbl.back().swap(calc.get_table()[idx]);
			from.push_back(TableRow());
			from.back().swap(calc.get_from()[idx]);
		}
		calc.set_from(std::move(from));
		calc.set_table(std::move(tbl));
		break;
	}
	}
	// ORDER BY
	if (!get_terms().empty()) {
		IndexTable order;
		{
			IndexTable::size_type n(calc.get_table().size());
			order.reserve(n);
			for (calc.set_index(0); calc.get_index() < n; calc.set_index(calc.get_index() + 1)) {
				order.push_back(IndexTableRow());
				order.back().set_index(calc.get_index());
				order.back().reserve(get_terms().size());
				for (const Term& trm : get_terms()) {
					order.back().push_back(Value());
					if (!trm.get_expr())
						continue;
					if (!trm.get_expr()->calculate(calc, order.back().back())) {
						c.error(*this, "order by expression");
						return false;
					}
				}
			}
		}
		if (false)
			CartoSQL::print_table(std::cerr << "Sort Terms Table before sorting" << std::endl,
					      order, 2, " | ", Value::sqlstringflags_t::none) << std::endl;
		{
			TermsSorter sorter(get_terms());
			std::stable_sort(order.begin(), order.end(), sorter);
		}
		if (false)
			CartoSQL::print_table(std::cerr << "Sort Terms Table after sorting" << std::endl,
					      order, 2, " | ", Value::sqlstringflags_t::none) << std::endl;
		order.permute(calc.get_table());
	}
	t.swap(calc.get_table());
	return apply_offset_limit(c, t);
}

std::ostream& CartoSQL::ExprSelect::print(std::ostream& os, unsigned int indent, const PrintContext& ctx) const
{
	ExprTableValue resultcol;
	resultcol.set_columns(get_columns());
	resultcol.get_columns().set_table("");
	ExprTable const * const subtables[] = { &resultcol, m_fromexpr.get(), nullptr };
	PrintContextStackSubtables ctx1(ctx, subtables);
	bool as(is_renamed());
	if (as)
		os << '(';
	print1(os, indent, ctx1);
	if (m_fromexpr)
		print_expr(os << std::endl << std::string(indent + 2, ' ') << "from ", m_fromexpr, indent + 2, ctx1);
	print2(os, indent, ctx1);
	if (as)
		os << ") as \"" << get_columns().front().get_table() << '"';
	return os;
}

int CartoSQL::ExprSelect::compare(const ExprTable& x) const
{
	int c(ExprSelectBase::compare(x));
	if (c)
		return c;
	const ExprSelect& xx(static_cast<const ExprSelect&>(x));
	return ExprTable::compare(m_fromexpr, xx.m_fromexpr);
}

CartoSQL::ExprTable::const_ptr_t CartoSQL::ExprSelect::push_filter_up(const CartoSQL *csql, const Expr::const_ptr_t& expr) const
{
	static constexpr bool debug = false;
	if (!csql)
		return const_ptr_t();
	boost::intrusive_ptr<ExprSelect> p(new ExprSelect());
	copy(*p);
	bool work(false);
	if (expr && !expr->is_true()) {
		Expr::exprlist_t el;
		el.push_back(get_whereexpr());
		{
			PushConditionUpResolve resolve(get_ptr());
			Expr::const_ptr_t expr1(expr->simplify(csql, resolve));
			if (resolve.is_error())
				return const_ptr_t();
			if (expr1)
				el.push_back(expr1);
			else
				el.push_back(expr);
		}
		p->set_whereexpr(Expr::combine_and_terms(el));
		work = true;
	}
	if (debug) {
		csql->print_expr(std::cerr << "ExprSelect::push_filter_up: ", expr, 4) << std::endl;
		csql->print_expr(std::cerr << "  Old: ", get_ptr(), 4) << std::endl;
		csql->print_expr(std::cerr << "  new where: ", p->get_whereexpr(), 4) << std::endl;
	}
	if (!p->get_whereexpr())
		return const_ptr_t();
	{
		Expr::const_ptr_t expr(p->get_whereexpr()->simplify(csql));
		if (expr) {
			p->set_whereexpr(expr);
			work = true;
		}
	}
	if (!get_fromexpr()) {
		if (work)
			return p;
		return ExprTable::const_ptr_t();
	}
	{
		Expr::const_ptr_t expr1;
		{
			PushConditionUpRename rename(1);
			expr1 = p->get_whereexpr()->simplify(csql, rename);
			if (rename.is_error())
				return p;
			if (!expr1)
				expr1 = p->get_whereexpr();
		}
		ExprTable::const_ptr_t pfrom(get_fromexpr()->push_filter_up(csql, expr1));
		if (pfrom) {
			p->set_fromexpr(pfrom);
			work = true;
		}
	}
	if (!work)
		return ExprTable::const_ptr_t();
	if (debug)
		csql->print_expr(std::cerr << "  New: ", p, 4) << std::endl;
	return p;
}

CartoSQL::ExprTable::const_ptr_t CartoSQL::ExprSelect::remove_unused_columns(const CartoSQL *csql, columnset_t& colsused) const
{
	boost::intrusive_ptr<ExprSelect> p1(new ExprSelect());
	copy(*p1);
	bool work(false);
	// add those columns needed by ordering etc.
	columnset_t fromcolset;
	{
		p1->get_columns().remove_unused_columns(colsused);
		TableColumnSets colset;
		p1->visit(colset);
		const columnset_t& cs(colset.get_columnset(0));
		colsused.insert(cs.begin(), cs.end());
		fromcolset = colset.get_columnset(1);
		p1->set_columns(get_columns());
	}
	if (!is_all_columns(colsused)) {
		work = true;
		p1->get_columns().remove_unused_columns(colsused);
		RenumberTableColumns ren(0, colsused);
		ExprTable::const_ptr_t p2(p1->simplify(csql, ren));
		if (p2) {
			if (p2->get_type() != type_t::select)
				throw Error("ExprSelect::remove_unused_columns: renumber changed type");
			p1 = boost::static_pointer_cast<ExprSelect>(boost::const_pointer_cast<ExprTable>(p2));
		}
	}
	// remove columns from downstream tables
	if (p1->get_fromexpr()) {
		ExprTable::const_ptr_t p2(p1->get_fromexpr()->remove_unused_columns(csql, fromcolset));
		if (p2) {
			work = true;
			p1->set_fromexpr(p2);
			RenumberTableColumns ren(1, fromcolset);
			p2 = p1->simplify(csql, ren);
			if (p2) {
				if (p2->get_type() != type_t::select)
					throw Error("ExprSelect::remove_unused_columns: renumber changed type");
				p1 = boost::static_pointer_cast<ExprSelect>(boost::const_pointer_cast<ExprTable>(p2));
			}
		}
	}
	if (work)
		return p1;
	return const_ptr_t();
}

CartoSQL::ExprOSMSelect::ExprOSMSelect(void)
	: m_from(table_t::point)
{
}

void CartoSQL::ExprOSMSelect::copy(ExprOSMSelect& x) const
{
	ExprSelectBase::copy(x);
	x.set_from(get_from());
}

CartoSQL::ExprTable::const_ptr_t CartoSQL::ExprOSMSelect::simplify(const CartoSQL *csql) const
{
	const_ptr_t p;
	{
		boost::intrusive_ptr<ExprOSMSelect> p1(new ExprOSMSelect());
		copy(*p1);
		p = simplify_tail(csql, p1, simplify_int(csql, get_subtables(), p1));
	}
	return p;
}

CartoSQL::ExprTable::const_ptr_t CartoSQL::ExprOSMSelect::simplify(const CartoSQL *csql, Visitor& v) const
{
	v.visit(*this);
	const_ptr_t p;
	if (v.descend(*this)) {
		boost::intrusive_ptr<ExprOSMSelect> p1(new ExprOSMSelect());
		copy(*p1);
		bool work(simplify_preresult(csql, get_subtables(), p1, v));
		v.select_result_table_available(csql, *this);
		p = simplify_tail(csql, p1, simplify_postresult(csql, get_subtables(), p1, v) || work);
	}
	const_ptr_t p1(p ? p->visitor_call_modify(csql, v) : visitor_call_modify(csql, v));
	v.visitend(*this);
	return p1 ? p1 : p;
}

void CartoSQL::ExprOSMSelect::visit(Visitor& v) const
{
	v.visit(*this);
	if (v.descend(*this))
		ExprSelectBase::visit(v);
	v.visitend(*this);
}

CartoSQL::Expr::subtables_t CartoSQL::ExprOSMSelect::get_subtables(void) const
{
	Expr::subtables_t subtbl;
	subtbl.push_back(get_ptr());
	return subtbl;
}

class CartoSQL::ExprOSMSelect::OSMSelectCalc : public CalcOverride {
public:
	OSMSelectCalc(Calc& c) : CalcOverride(c), m_groupbegin(0), m_groupend(0), m_index(0) {}
	virtual const TableRow *get_table_row(unsigned int tableindex) {
		if (tableindex > 0)
			return CalcOverride::get_table_row(tableindex - 1);
		if (m_index >= m_table.size())
			return nullptr;
		return &m_table[m_index];
	}
	virtual const OSMDB::Object *get_osmobject(void) {
		if (m_index >= m_objs.size())
			return nullptr;
		return m_objs[m_index].get();
	}
	virtual grouprow_t get_group_rows(void) { return m_groupend - m_groupbegin; }
	virtual void set_group_row(grouprow_t x) {
		if (m_groupend <= m_groupbegin + 1)
			return;
		m_index = m_groupbegin + x;
		if (m_index > m_groupend)
			m_index = m_groupend;
	}
	const OSMDB::objects_t& get_objs(void) const { return m_objs; }
	OSMDB::objects_t& get_objs(void) { return m_objs; }
	void set_objs(const OSMDB::objects_t& objs) { m_objs = objs; }
	void set_objs(OSMDB::objects_t&& objs) { m_objs.swap(objs); }
	const Table& get_table(void) const { return m_table; }
	Table& get_table(void) { return m_table; }
	void set_table(const Table& tbl) { m_table = tbl; }
	void set_table(Table&& tbl) { m_table.swap(tbl); }
	grouprow_t get_groupbegin(void) const { return m_groupbegin; }
	grouprow_t get_groupend(void) const { return m_groupend; }
	void set_group(grouprow_t beg, grouprow_t end) { m_index = m_groupbegin = beg; m_groupend = end; }
	typedef std::tuple<grouprow_t,grouprow_t> group_t;
	group_t get_group(void) const { return std::make_tuple(get_groupbegin(), get_groupend()); }
	void set_group(const group_t& grp) { m_index = m_groupbegin = std::get<0>(grp); m_groupend = std::get<1>(grp); }
	grouprow_t get_index(void) const { return m_index; }
	void set_index(grouprow_t idx) { m_index = idx; }

protected:
	OSMDB::objects_t m_objs;
	Table m_table;
	grouprow_t m_groupbegin;
	grouprow_t m_groupend;
	grouprow_t m_index;
};

bool CartoSQL::ExprOSMSelect::calculate(Calc& c, Table& t) const
{
	t.clear();
	if (get_whereexpr() && get_whereexpr()->is_false())
		return true;
	OSMSelectCalc calc(c);
	// FROM
	if (!ExprOSMTable::get_dbobjs(calc.get_objs(), c, get_from(), this))
		return false;
	// WHERE
	if (get_whereexpr()) {
		OSMDB::objects_t objs;
		objs.reserve(calc.get_objs().size());
		calc.set_index(0);
		for (OSMSelectCalc::grouprow_t n(calc.get_objs().size()); calc.get_index() < n; calc.set_index(calc.get_index() + 1)) {
			Value val;
			if (!get_whereexpr()->calculate(calc, val)) {
				c.error(*this, "where expression");
				return false;
			}
			if (val.is_null())
				continue;
			if (!val.is_boolean()) {
				c.error(*this, val, "where result is not boolean");
				return false;
			}
			if (!val.get_boolean())
				continue;
			objs.push_back(OSMDB::Object::const_ptr_t());
			objs.back().swap(calc.get_objs()[calc.get_index()]);
		}
		calc.set_objs(std::move(objs));
	}
	// grouped select
	if (!get_groupexpr().empty() || get_havingexpr() || m_columns.has_aggregate()) {
		typedef OSMSelectCalc::group_t group_t;
		typedef std::vector<group_t> groups_t;
		groups_t groups;
		groups.push_back(group_t{ 0, calc.get_objs().size() });
		// GROUP BY
		if (!get_groupexpr().empty()) {
			IndexTable grp;
			grp.reserve(calc.get_objs().size());
			calc.set_index(0);
			for (OSMSelectCalc::grouprow_t n(calc.get_objs().size()); calc.get_index() < n; calc.set_index(calc.get_index() + 1)) {
				IndexTableRow tr;
				tr.reserve(get_groupexpr().size());
				tr.set_index(calc.get_index());
				for (const Expr::const_ptr_t& expr : get_groupexpr()) {
					tr.push_back(Value());
					if (expr && !expr->calculate(calc, tr.back())) {
						c.error(*this, "group by expression");
						return false;
					}
				}
				grp.push_back(tr);
			}
			std::stable_sort(grp.begin(), grp.end(), IndexTable::ValueSorter());
			grp.permute(calc.get_objs());
			groups = grp.get_equalranges();
		}
		OSMDB::objects_t objs;
		calc.get_table().reserve(groups.size());
		for (const group_t& grp : groups) {
			calc.set_group(grp);
			if (!calc.get_group_rows())
				continue;
			// HAVING
			if (get_havingexpr()) {
				Value v;
				if (!get_havingexpr()->calculate(calc, v)) {
					c.error(*this, "having expression");
					return false;
				}
				if (!v.is_boolean()) {
					c.error(*this, v, "having expression does not yield a boolean");
					return false;
				}
				if (!v.get_boolean())
					continue;
			}
			// construct a row
			calc.get_table().push_back(TableRow());
			TableRow& tr(calc.get_table().back());
			{
				const OSMDB::Object *osmobj(calc.get_osmobject());
				if (!osmobj) {
					tr.add_osmidset(osmobj->get_id());
				}
			}
			tr.reserve(m_columns.size());
			for (const Column& col : m_columns) {
				tr.push_back(Value());
				if (!col.get_expr())
					continue;
				if (!col.get_expr()->calculate(calc, tr.back())) {
					c.error(*this, "column expression");
					return false;
				}
			}
			objs.push_back(OSMDB::Object::const_ptr_t());
			objs.back().swap(calc.get_objs()[calc.get_index()]);
		}
		calc.set_objs(std::move(objs));
	} else {
		OSMSelectCalc::grouprow_t n(calc.get_objs().size());
		calc.get_table().reserve(n);
		for (calc.set_index(0); calc.get_index() < n; calc.set_index(calc.get_index() + 1)) {
			// construct a row
			calc.get_table().push_back(TableRow());
			TableRow& tr(calc.get_table().back());
			{
				const OSMDB::Object *osmobj(calc.get_osmobject());
				if (osmobj) {
					tr.add_osmidset(osmobj->get_id());
				}
			}
			tr.reserve(m_columns.size());
			for (const Column& col : m_columns) {
				tr.push_back(Value());
				if (!col.get_expr())
					continue;
				if (!col.get_expr()->calculate(calc, tr.back())) {
					c.error(*this, "column expression");
					return false;
				}
			}
		}
	}
	calc.set_group(0, 0);
	// DISTINCT
	switch (get_distinct()) {
	default:
	case distinct_t::all:
		break;

	case distinct_t::distinctrows:
	{
		{
			IndexTable dist;
			IndexTable::size_type n(calc.get_table().size());
			dist.resize(n);
			for (IndexTable::size_type i(0); i < n; ++i) {
				dist[i].TableRowValues::swap(calc.get_table()[i]);
				dist[i].set_index(i);
			}
			std::stable_sort(dist.begin(), dist.end(), IndexTable::ValueSorter());
			dist.permute(calc.get_objs());
			dist.permute(calc.get_table());
			for (IndexTable::size_type i(0); i < n; ++i) 
				calc.get_table()[i].TableRowValues::swap(dist[i]);
		}
		OSMDB::objects_t objs;
		objs.reserve(calc.get_table().size());
		Table tbl;
		tbl.reserve(calc.get_table().size());
		for (IndexTable::size_type i(0), n(calc.get_table().size()); i < n; ++i) {
			if (i && !tbl.back().compare_value(calc.get_table()[i]))
				continue;
			tbl.push_back(TableRow());
			tbl.back().swap(calc.get_table()[i]);
			objs.push_back(OSMDB::Object::const_ptr_t());
			objs.back().swap(calc.get_objs()[i]);
		}
		calc.set_objs(std::move(objs));
		calc.set_table(std::move(tbl));
		break;
	}

	case distinct_t::distinctexpr:
	{
		if (get_distinctexpr().empty())
			break;
		IndexTable dist;
		{
			IndexTable::size_type n(calc.get_table().size());
			dist.reserve(n);
			for (IndexTable::size_type i(0); i < n; ++i) {
				dist.push_back(IndexTableRow());
				dist.back().set_index(i);
				dist.back().reserve(get_distinctexpr().size());
				for (const Expr::const_ptr_t& p : get_distinctexpr()) {
					dist.back().push_back(Value());
					if (!p)
						continue;
					if (!p->calculate(calc, dist.back().back())) {
						c.error(*this, "distinct expression");
						return false;
					}
				}
			}
		}
		std::stable_sort(dist.begin(), dist.end(), IndexTable::ValueSorter());
		OSMDB::objects_t objs;
		Table tbl;
		tbl.reserve(dist.size());
		objs.reserve(dist.size());
		for (IndexTable::size_type i(0), n(dist.size()); i < n; ++i) {
			if (i && !dist[i].compare_value(dist[i-1]))
				continue;
			IndexTableRow::index_t idx(dist[i].get_index());
			tbl.push_back(TableRow());
			tbl.back().swap(calc.get_table()[idx]);
			objs.push_back(OSMDB::Object::const_ptr_t());
			objs.back().swap(calc.get_objs()[idx]);
		}
		calc.set_objs(std::move(objs));
		calc.set_table(std::move(tbl));
		break;
	}
	}
	// ORDER BY
	if (!get_terms().empty()) {
		IndexTable order;
		{
			IndexTable::size_type n(calc.get_table().size());
			order.reserve(n);
			for (calc.set_index(0); calc.get_index() < n; calc.set_index(calc.get_index() + 1)) {
				order.push_back(IndexTableRow());
				order.back().set_index(calc.get_index());
				order.back().reserve(get_terms().size());
				for (const Term& trm : get_terms()) {
					order.back().push_back(Value());
					if (!trm.get_expr())
						continue;
					if (!trm.get_expr()->calculate(calc, order.back().back())) {
						c.error(*this, "order by expression");
						return false;
					}
				}
			}
		}
		{
			TermsSorter sorter(get_terms());
			std::stable_sort(order.begin(), order.end(), sorter);
		}
		order.permute(calc.get_table());
	}
	t.swap(calc.get_table());
	return apply_offset_limit(c, t);
}

std::ostream& CartoSQL::ExprOSMSelect::print(std::ostream& os, unsigned int indent, const PrintContext& ctx) const
{
	ExprTableValue resultcol;
	resultcol.set_columns(get_columns());
	resultcol.get_columns().set_table("");
	ExprTable const * const subtables[] = { &resultcol, nullptr };
	PrintContextStackSubtables ctx1(ctx, subtables);
	bool as(is_renamed());
	if (as)
		os << '(';
	print1(os, indent, ctx1);
	os << std::endl << std::string(indent + 2, ' ') << "from " << ExprOSMTable::get_tablename(m_from);
	print2(os, indent, ctx1);
	if (as)
		os << ") as \"" << get_columns().front().get_table() << '"';
	return os;
}

int CartoSQL::ExprOSMSelect::compare(const ExprTable& x) const
{
	int c(ExprSelectBase::compare(x));
	if (c)
		return c;
	const ExprOSMSelect& xx(static_cast<const ExprOSMSelect&>(x));
	if (m_from < xx.m_from)
		return -1;
	if (xx.m_from < m_from)
		return 1;
	return 0;
}

CartoSQL::ExprTable::const_ptr_t CartoSQL::ExprOSMSelect::push_filter_up(const CartoSQL *csql, const Expr::const_ptr_t& expr) const
{
	static constexpr bool debug = false;
	if (!csql || !expr || expr->is_true())
		return const_ptr_t();
	boost::intrusive_ptr<ExprOSMSelect> p(new ExprOSMSelect());
	copy(*p);
	{
		Expr::exprlist_t el;
		el.push_back(get_whereexpr());
		{
			PushConditionUpResolve resolve(p);
			Expr::const_ptr_t expr1(expr->simplify(csql, resolve));
			if (resolve.is_error())
				return const_ptr_t();
			if (expr1)
				el.push_back(expr1);
			else
				el.push_back(expr);
		}
		p->set_whereexpr(Expr::combine_and_terms(el));
	}
	if (debug) {
		csql->print_expr(std::cerr << "ExprOSMSelect::push_filter_up: ", expr, 4) << std::endl;
		csql->print_expr(std::cerr << "  Old: ", get_ptr(), 4) << std::endl;
		csql->print_expr(std::cerr << "  new where: ", p->get_whereexpr(), 4) << std::endl;
	}
	if (!p->get_whereexpr())
		return const_ptr_t();
	{
		Expr::const_ptr_t expr(p->get_whereexpr()->simplify(csql));
		if (expr)
			p->set_whereexpr(expr);
	}
	if (debug)
		csql->print_expr(std::cerr << "  New: ", p, 4) << std::endl;
	return p;
}

CartoSQL::ExprTable::const_ptr_t CartoSQL::ExprOSMSelect::remove_unused_columns(const CartoSQL *csql, columnset_t& colsused) const
{
	if (is_all_columns(colsused))
		return ExprTable::const_ptr_t();
	boost::intrusive_ptr<ExprOSMSelect> p1(new ExprOSMSelect());
	copy(*p1);
	p1->get_columns().remove_unused_columns(colsused);
	RenumberTableColumns renum(0, colsused);
	const_ptr_t p2(p1->simplify(csql, renum));
	if (p2)
		return p2;
	return p1;
}

const std::string& to_str(CartoSQL::ExprCompound::operator_t x)
{
	switch (x) {
	case CartoSQL::ExprCompound::operator_t::union_:
	{
		static const std::string r("union");
		return r;
	}

	case CartoSQL::ExprCompound::operator_t::intersect:
	{
		static const std::string r("intersect");
		return r;
	}

	case CartoSQL::ExprCompound::operator_t::except:
	{
		static const std::string r("except");
		return r;
	}

	default:
	{
		static const std::string r("?");
		return r;
	}
	}
}

const std::string& to_str(CartoSQL::ExprCompound::distinct_t x)
{
	switch (x) {
	case CartoSQL::ExprCompound::distinct_t::distinct:
	{
		static const std::string r("distinct");
		return r;
	}

	case CartoSQL::ExprCompound::distinct_t::all:
	{
		static const std::string r("all");
		return r;
	}

	default:
	{
		static const std::string r("?");
		return r;
	}
	}
}

void CartoSQL::ExprCompound::copy(ExprCompound& x) const
{
	ExprTableSort::copy(x);
	for (unsigned int i = 0; i < 2; ++i)
		x.set_subtable(i, get_subtable(i));
	x.set_operator(get_operator());
	x.set_distinct(get_distinct());
}

bool CartoSQL::ExprCompound::types_compound_compatible(Value::type_t t0, Value::type_t t1)
{
	switch (t0) {
	case Value::type_t::null:
		return true;

	case Value::type_t::floatingpoint:
	case Value::type_t::integer:
		switch (t1) {
		case Value::type_t::null:
		case Value::type_t::floatingpoint:
		case Value::type_t::integer:
			return true;

		default:
			return false;
		}

	case Value::type_t::tags:
		switch (t1) {
		case Value::type_t::tags:
		case Value::type_t::osmdbobject:
			return true;

		default:
			return false;
		}

	case Value::type_t::points:
	case Value::type_t::lines:
	case Value::type_t::polygons:
	case Value::type_t::rectangle:
		switch (t1) {
		case Value::type_t::null:
		case Value::type_t::points:
		case Value::type_t::lines:
		case Value::type_t::polygons:
		case Value::type_t::rectangle:
		case Value::type_t::osmdbobject:
			return true;

		default:
			return false;
		}

	case Value::type_t::osmdbobject:
		switch (t1) {
		case Value::type_t::null:
		case Value::type_t::tags:
		case Value::type_t::points:
		case Value::type_t::lines:
		case Value::type_t::polygons:
		case Value::type_t::rectangle:
		case Value::type_t::osmdbobject:
			return true;

		default:
			return false;
		}

	default:
		return t0 == t1;
	}
}

void CartoSQL::ExprCompound::default_columns(Columns& cols) const
{
	if (!m_subtable[0])
		throw Error("first subtable not available");
	if (!m_subtable[1])
		throw Error("second subtable not available");
	const Columns& scols0(m_subtable[0]->get_columns());
	const Columns& scols1(m_subtable[1]->get_columns());
	if (scols0.size() != scols1.size())
		throw Error("tables not compound compatible (degree mismatch)");
	cols.clear();
	cols.resize(scols0.size());
	for (Columns::size_type i(0), n(cols.size()); i < n; ++i) {
		Column& col(cols[i]);
		const Column& col0(scols0[i]);
		const Column& col1(scols1[i]);
		col.set_name("?column?");
		if (!col0.get_name().empty() && col0.get_name() != "?column?" &&
		    (col0.get_name() == col1.get_name() || col1.get_name().empty() || col1.get_name() == "?column?")) {
			col.set_name(col0.get_name());
		} else if (!col1.get_name().empty() && col1.get_name() != "?column?" &&
			   (col0.get_name().empty() || col0.get_name() == "?column?")) {
			col.set_name(col1.get_name());
		} else if (false) {
			std::cerr << "Column name clash: \"" << col0.get_name() << "\", \"" << col1.get_name() << "\"" << std::endl;
		}
		col.set_type(col0.get_type());
		col.set_expr(Expr::ptr_t(new ExprTableColumn(0, i)));
		if (true && !types_compound_compatible(col0.get_type(), col1.get_type())) {
			if (!false) {
				std::cerr << "Composite (" << get_operator() << ") Table 0: " << m_subtable[0]->get_type() << std::endl;
				for (const Column& col : scols0)
					std::cerr << "  " << col.to_str() << " type " << Value::get_sqltypename(col.get_type()) << std::endl;
				std::cerr << "Table 1: " << m_subtable[1]->get_type() << std::endl;
				for (const Column& col : scols1)
					std::cerr << "  " << col.to_str() << " type " << Value::get_sqltypename(col.get_type()) << std::endl;
			}
			std::ostringstream oss;
			oss << "tables not compound compatible (type mismatch column " << (i + 1)
			    << ": first table " << Value::get_sqltypename(col0.get_type())
			    << " second table " << Value::get_sqltypename(col1.get_type()) << ')';
			throw Error(oss.str());
		}
	}
	cols.set_table(to_str(get_operator()));
}

void CartoSQL::ExprCompound::compute_columns(void)
{
	default_columns(m_columns);
}

std::pair<bool,bool> CartoSQL::ExprCompound::is_renamed(void) const
{
	Columns cols;
	default_columns(cols);
	if (cols.size() != m_columns.size())
		throw Error("Internal Error: column degree mismatch");
	std::pair<bool,bool> ret(false, false);
	for (Columns::size_type i(0), n(cols.size()); i < n; ++i) {
		const Column& cold(cols[i]);
		const Column& col(m_columns[i]);
		ret.first = ret.first || (cold.get_table() != col.get_table());
		ret.second = ret.second || (cold.get_name() != col.get_name());
		if (ret.first && ret.second)
			return ret;
	}
	return ret;
}

template <typename... Args>
bool CartoSQL::ExprCompound::simplify_int(const CartoSQL *csql, const Expr::subtables_t& subtables, boost::intrusive_ptr<ExprCompound> p, Args&&... args)
{
	bool work(ExprTableSort::simplify_int(csql, subtables, p, std::forward<Args>(args)...));
	for (unsigned int i = 0; i < 2; ++i) {
		if (!p->get_subtable(i))
			continue;
		const_ptr_t p1(p->get_subtable(i)->simplify(csql, std::forward<Args>(args)...));
		if (p1) {
			work = true;
			p->set_subtable(i, p1);
		}
	}
	return work;
}

CartoSQL::ExprTable::const_ptr_t CartoSQL::ExprCompound::simplify(const CartoSQL *csql) const
{
	const_ptr_t p;
	{
		boost::intrusive_ptr<ExprCompound> p1(new ExprCompound());
		copy(*p1);
		p = simplify_tail(csql, p1, simplify_int(csql, get_subtables(), p1));
	}
	return p;
}

CartoSQL::ExprTable::const_ptr_t CartoSQL::ExprCompound::simplify(const CartoSQL *csql, Visitor& v) const
{
	v.visit(*this);
	const_ptr_t p;
	if (v.descend(*this)) {
		boost::intrusive_ptr<ExprCompound> p1(new ExprCompound());
		copy(*p1);
		p = simplify_tail(csql, p1, simplify_int(csql, get_subtables(), p1, v));
	}
	const_ptr_t p1(p ? p->visitor_call_modify(csql, v) : visitor_call_modify(csql, v));
	v.visitend(*this);
	return p1 ? p1 : p;
}

void CartoSQL::ExprCompound::visit(Visitor& v) const
{
	v.visit(*this);
	if (v.descend(*this)) {
		for (unsigned int i = 0; i < 2; ++i)
			if (m_subtable[i])
				m_subtable[i]->visit(v);
		ExprTableSort::visit(v);
	}
	v.visitend(*this);
}

CartoSQL::Expr::subtables_t CartoSQL::ExprCompound::get_subtables(void) const
{
	Expr::subtables_t subtbl;
	subtbl.push_back(get_ptr());
	return subtbl;
}

class CartoSQL::ExprCompound::CompoundCalc : public CalcOverride {
public:
	CompoundCalc(Calc& c) : CalcOverride(c), m_index(0) { }
	virtual const TableRow *get_table_row(unsigned int tableindex) {
		if (tableindex > 0)
			return CalcOverride::get_table_row(tableindex - 1);
		if (m_index >= m_table.size())
			return nullptr;
		return &m_table[m_index];
	}
	virtual const OSMDB::Object *get_osmobject(void) { return nullptr; }
	const Table& get_table(void) const { return m_table; }
	Table& get_table(void) { return m_table; }
	void set_table(const Table& tbl) { m_table = tbl; }
	void set_table(Table&& tbl) { m_table.swap(tbl); }
	virtual grouprow_t get_group_rows(void) { return 1; }
	virtual void set_group_row(grouprow_t x) { }
	grouprow_t get_index(void) const { return m_index; }
	void set_index(grouprow_t idx) { m_index = idx; }

protected:
	grouprow_t m_index;
	Table m_table;
};

bool CartoSQL::ExprCompound::calculate(Calc& c, Table& t) const
{
	t.clear();
	CompoundCalc calc(c);
	{
		Table left, right;
		if (m_subtable[0] && !m_subtable[0]->calculate(calc, left)) {
			c.error(*this, "left table");
			return false;
		}
		if (m_subtable[1] && !m_subtable[1]->calculate(calc, right)) {
			c.error(*this, "right table");
			return false;
		}
		if (get_operator() == operator_t::union_) {
			calc.set_table(std::move(left));
			calc.get_table().append(std::move(right));
			if (get_distinct() == distinct_t::distinct)
				calc.get_table().eliminate_duplicates();
		} else {
			std::sort(left.begin(), left.end(), Table::ValueSorter());
			std::sort(right.begin(), right.end(), Table::ValueSorter());
			switch (get_operator()) {
			case operator_t::intersect:
				for (Table::iterator i0(left.begin()), e0(left.end()), i1(right.begin()), e1(right.end()); i0 != e0 && i1 != e1; ) {
					int c(i0->compare_value(*i1));
					if (c < 0) {
						++i0;
						continue;
					}
					if (c > 0) {
						++i1;
						continue;
					}
					calc.get_table().push_back(TableRow());
					calc.get_table().back().swap(*i0);
					calc.get_table().back().add_osmidset(i1->get_osmidset());
					++i0;
					++i1;
				}
				if (get_distinct() == distinct_t::distinct)
					calc.get_table().eliminate_duplicates();
				break;

			case operator_t::except:
				for (Table::iterator i0(left.begin()), e0(left.end()), i1(right.begin()), e1(right.end()); i0 != e0 && i1 != e1; ) {
					int c(i0->compare_value(*i1));
					if (c < 0) {
						calc.get_table().push_back(TableRow());
						calc.get_table().back().swap(*i0);
						++i0;
						continue;
					}
					if (c > 0) {
						++i1;
						continue;
					}
					++i0;
					++i1;
				}
				if (get_distinct() == distinct_t::distinct)
					calc.get_table().eliminate_duplicates();
				break;

			default:
				c.error(*this, "not implemented");
				return false;
			}
		}
	}
	// ORDER BY
	if (!get_terms().empty()) {
		// FIXME
		IndexTable order;
		{
			IndexTable::size_type n(calc.get_table().size());
			order.reserve(n);
			for (calc.set_index(0); calc.get_index() < n; calc.set_index(calc.get_index() + 1)) {
				order.push_back(IndexTableRow());
				order.back().set_index(calc.get_index());
				order.back().reserve(get_terms().size());
				for (const Term& trm : get_terms()) {
					order.back().push_back(Value());
					if (!trm.get_expr())
						continue;
					if (!trm.get_expr()->calculate(calc, order.back().back())) {
						c.error(*this, "order by expression");
						return false;
					}
				}
			}
		}
		{
			TermsSorter sorter(get_terms());
			std::stable_sort(order.begin(), order.end(), sorter);
		}
		order.permute(calc.get_table());
	}
	t.swap(calc.get_table());
	return apply_offset_limit(c, t);
}

std::ostream& CartoSQL::ExprCompound::print(std::ostream& os, unsigned int indent, const PrintContext& ctx) const
{
	ExprTableValue resultcol;
	resultcol.set_columns(get_columns());
	resultcol.get_columns().set_table("");
	ExprTable const * const subtables[] = { &resultcol, nullptr };
	PrintContextSubtables ctx1(ctx, subtables);
	os << '(';
	if (!false) {
		for (const Column& col : get_columns())
			os << std::endl << std::string(indent + 2, ' ') << "-- " << col.to_str() << " type: " << Value::get_sqltypename(col.get_type());
		os << std::endl << std::string(indent + 2, ' ');
	}
	print_expr(os, m_subtable[0], indent + 2, ctx1);
	os << ' ' << m_operator << ' ';
	if (m_distinct != distinct_t::distinct)
		os << m_distinct << ' ';
	print_expr(os, m_subtable[1], indent + 2, ctx1);
	ExprTableSort::print(os, indent, ctx1);
	std::pair<bool,bool> ren(is_renamed());
	if ((!ren.first && !ren.second) || get_columns().empty())
		return os;
	os << " as \"" << get_columns().front().get_table() << '"';
	if (!ren.second)
		return os;
	char sep('(');
	for (const Column& col : get_columns()) {
		os << sep << '"' << col.get_name() << '"';
		sep = ',';
	}
	if (sep != '(')
		os << ')';
	return os;
}

int CartoSQL::ExprCompound::compare(const ExprTable& x) const
{
	int c(ExprTableSort::compare(x));
	if (c)
		return c;
	const ExprCompound& xx(static_cast<const ExprCompound&>(x));
	for (unsigned int i = 0; i < 2; ++i) {
		c = ExprTable::compare(m_subtable[i], xx.m_subtable[i]);
		if (c)
			return c;
	}
	if (m_operator < xx.m_operator)
		return -1;
	if (xx.m_operator < m_operator)
		return 1;
	if (m_distinct < xx.m_distinct)
		return -1;
	if (xx.m_distinct < m_distinct)
		return 1;
	return 0;
}

CartoSQL::ExprTable::const_ptr_t CartoSQL::ExprCompound::push_filter_up(const CartoSQL *csql, const Expr::const_ptr_t& expr) const
{
	if (!csql || !expr || expr->is_true())
		return const_ptr_t();
	const_ptr_t p[2];
	bool work(false);
	for (unsigned int i = 0; i < 2; ++i) {
		if (!m_subtable[i])
			continue;
		p[i] = m_subtable[i]->push_filter_up(csql, expr);
		if (p[i])
			work = true;
	}
	if (work)
		return const_ptr_t();
	boost::intrusive_ptr<ExprCompound> p1(new ExprCompound());
	copy(*p1);
	for (unsigned int i = 0; i < 2; ++i)
		if (p[i])
			p1->set_subtable(i, p[i]);
	return p1;
}

CartoSQL::ExprTable::const_ptr_t CartoSQL::ExprCompound::remove_unused_columns(const CartoSQL *csql, columnset_t& colsused) const
{
	boost::intrusive_ptr<ExprCompound> p1(new ExprCompound());
	copy(*p1);
	bool work(false);
	if (p1->m_subtable[0])
		p1->m_subtable[0]->remove_unused_columns(csql, colsused);
	if (p1->m_subtable[1]) {
		const_ptr_t p2(p1->m_subtable[1]->remove_unused_columns(csql, colsused));
		if (p2) {
			work = true;
			p1->m_subtable[1] = p2;
		}
	}
	if (p1->m_subtable[0]) {
		columnset_t colsused1(colsused);
		const_ptr_t p2(p1->m_subtable[0]->remove_unused_columns(csql, colsused));
		if (p2) {
			work = true;
			p1->m_subtable[0] = p2;
		}
		if (colsused != colsused1)
			throw Error("ExprCompound::remove_unused_columns: inconsistent column sets");
	}
	if (!is_all_columns(colsused)) {
		work = true;
		p1->get_columns().remove_unused_columns(colsused);
		RenumberTableColumns ren(0, colsused);
		ExprTable::const_ptr_t p2(p1->simplify(csql, ren));
		if (p2) {
			if (p2->get_type() != type_t::compound)
				throw Error("ExprCompound::remove_unused_columns: renumber changed type");
			p1 = boost::static_pointer_cast<ExprCompound>(boost::const_pointer_cast<ExprTable>(p2));
		}
	}
	if (work)
		return p1;
	return const_ptr_t();
}

int CartoSQL::JoinColumn::compare(const JoinColumn& x) const
{
	for (unsigned int i = 0; i < 2; ++i) {
		if (m_idx[i] < x.m_idx[i])
			return -1;
		if (x.m_idx[i] < m_idx[i])
			return 1;
	}
	return 0;
}

void CartoSQL::JoinColumn::get_columns_used(columnset_t colsets[2]) const
{
	for (unsigned int i = 0; i < 2; ++i)
		colsets[i].insert(m_idx[i]);
}

void CartoSQL::JoinColumn::renumber_columns(const columnset_t colsused[2])
{
	for (unsigned int i = 0; i < 2; ++i) {
		unsigned int j(0);
		columnset_t::const_iterator ci(colsused[i].begin()), ce(colsused[i].end());
		for (; ci != ce; ++ci, ++j)
			if (*ci == m_idx[i])
				break;
		if (ci == ce)
			throw Error("JoinColumn::renumber_columns: column not found in used set");
		m_idx[i] = j;
	}
}

int CartoSQL::JoinColumns::compare(const JoinColumns& x) const
{
	size_type i(x.size()), n(size());
	if (n < i)
		return -1;
	if (i < n)
		return 1;
	for (i = 0; i < n; ++i) {
		int c((*this)[i].compare(x[i]));
		if (c)
			return c;
	}
	return 0;
}

void CartoSQL::JoinColumns::get_columns_used(columnset_t colsets[2]) const
{
	for (const JoinColumn& jc : *this)
		jc.get_columns_used(colsets);
}

void CartoSQL::JoinColumns::renumber_columns(const columnset_t colsused[2])
{
	for (JoinColumn& jc : *this)
	        jc.renumber_columns(colsused);
}

CartoSQL::JoinColumns::joincols_t CartoSQL::JoinColumns::get_joincols(unsigned int idx) const
{
	joincols_t r;
	for (const JoinColumn& jc : *this)
		r.insert(jc[idx]);
	return r;
}

const std::string& to_str(CartoSQL::ExprJoin::jointype_t x)
{
	switch (x) {
	case CartoSQL::ExprJoin::jointype_t::inner:
	{
		static const std::string r("inner");
		return r;
	}

	case CartoSQL::ExprJoin::jointype_t::leftouter:
	{
		static const std::string r("left outer");
		return r;
	}

	case CartoSQL::ExprJoin::jointype_t::rightouter:
	{
		static const std::string r("right outer");
		return r;
	}

	case CartoSQL::ExprJoin::jointype_t::fullouter:
	{
		static const std::string r("full outer");
		return r;
	}

	default:
	{
		static const std::string r("?");
		return r;
	}
	}
}

const std::string& to_str(CartoSQL::ExprJoin::constraint_t x)
{
	switch (x) {
	case CartoSQL::ExprJoin::constraint_t::none:
	{
		static const std::string r("none");
		return r;
	}

	case CartoSQL::ExprJoin::constraint_t::column:
	{
		static const std::string r("column");
		return r;
	}

	case CartoSQL::ExprJoin::constraint_t::expr:
	{
		static const std::string r("expr");
		return r;
	}

	default:
	{
		static const std::string r("?");
		return r;
	}
	}
}

CartoSQL::ExprJoin::ExprJoin(const ExprTable::const_ptr_t& se0, const ExprTable::const_ptr_t& se1, jointype_t jointype)
	: m_jointype(jointype), m_constraint(constraint_t::none)
{
	m_subtable[0] = se0;
	m_subtable[1] = se1;
}

CartoSQL::ExprJoin::~ExprJoin()
{
	set_constraint_none();
}

void CartoSQL::ExprJoin::set_constraint_none(void)
{
	switch (m_constraint) {
	case constraint_t::column:
		m_joincolumns.~JoinColumns();
		break;

	case constraint_t::expr:
		m_joinexpr.~joinexpr_t();
		break;

	default:
		break;
	}
	m_constraint = constraint_t::none;
}

const CartoSQL::Expr::const_ptr_t& CartoSQL::ExprJoin::get_joinexpr(void) const
{
	if (m_constraint == constraint_t::expr)
		return m_joinexpr;
	static const Expr::const_ptr_t nullexpr;
	return nullexpr;
}

const CartoSQL::JoinColumns& CartoSQL::ExprJoin::get_joincolumns(void) const
{
	if (m_constraint == constraint_t::column)
		return m_joincolumns;
	static const JoinColumns nullcols;
	return nullcols;
}

void CartoSQL::ExprJoin::set_joinexpr(const Expr::const_ptr_t& x)
{
	if (!x)
		return;
	set_constraint_none();
	::new(&m_joinexpr) joinexpr_t(x);
	m_constraint = constraint_t::expr;
}

void CartoSQL::ExprJoin::set_joincolumns(const JoinColumns& x)
{
	if (x.empty())
		return;
	set_constraint_none();
	::new(&m_joincolumns) JoinColumns(x);
	m_constraint = constraint_t::column;
}

void CartoSQL::ExprJoin::set_joincolumns(JoinColumns&& x)
{
	if (x.empty())
		return;
	set_constraint_none();
	::new(&m_joincolumns) JoinColumns();
	m_constraint = constraint_t::column;
	m_joincolumns.swap(x);
}

void CartoSQL::ExprJoin::copy(ExprJoin& x) const
{
	ExprTable::copy(x);
	for (unsigned int i = 0; i < 2; ++i)
		x.set_subtable(i, get_subtable(i));
	x.set_jointype(get_jointype());
	switch (get_constraint()) {
	case constraint_t::expr:
		x.set_joinexpr(get_joinexpr());
		break;

	case constraint_t::column:
		x.set_joincolumns(get_joincolumns());
		break;

	default:
		x.set_constraint_none();
		break;
	}
}

void CartoSQL::ExprJoin::default_columns(Columns& cols) const
{
	static constexpr unsigned int jointindex = 0;
	if (!m_subtable[0])
		throw Error("first subtable not available");
	if (!m_subtable[1])
		throw Error("second subtable not available");
	const Columns *scols[2] = { &m_subtable[0]->get_columns(), &m_subtable[1]->get_columns() };
	cols.clear();
	{
		const Columns& scol(*scols[jointindex]);
		for (const JoinColumn& jc : get_joincolumns()) {
			if (jc[jointindex] >= scol.size())
				throw Error("join: inconsistent join column");
			cols.push_back(Column("", scol[jc[jointindex]].get_name(), new ExprTableColumn(jointindex, jc[jointindex]), scol[jc[jointindex]].get_type()));
		}
	}
	for (unsigned int idx = 0; idx < 2; ++idx) {
		const Columns& scol(*scols[idx]);
		JoinColumns::joincols_t joincols(get_joincolumns().get_joincols(idx));
		for (Columns::size_type i(0), n(scol.size()); i < n; ++i) {
			if (joincols.find(i) != joincols.end())
				continue;
			const Column& col(scol[i]);
			cols.push_back(Column(col.get_table(), col.get_name(), new ExprTableColumn(idx, i), col.get_type()));
		}
	}
}

void CartoSQL::ExprJoin::compute_columns(void)
{
	default_columns(m_columns);
}

std::pair<bool,bool> CartoSQL::ExprJoin::is_renamed(void) const
{
	Columns cols;
	default_columns(cols);
	if (cols.size() != m_columns.size())
		throw Error("Internal Error: column degree mismatch");
	std::pair<bool,bool> ret(false, false);
	for (Columns::size_type i(0), n(cols.size()); i < n; ++i) {
		const Column& cold(cols[i]);
		const Column& col(m_columns[i]);
		ret.first = ret.first || (cold.get_table() != col.get_table());
		ret.second = ret.second || (cold.get_name() != col.get_name());
		if (ret.first && ret.second)
			return ret;
	}
	return ret;
}

void CartoSQL::ExprJoin::compute_joincolumns(const std::vector<std::string>& joincols)
{
	if (!m_subtable[0])
		throw Error("first subtable not available");
	if (!m_subtable[1])
		throw Error("second subtable not available");
	JoinColumns jcs;
	jcs.reserve(joincols.size());
	const Columns *scols[2] = { &m_subtable[0]->get_columns(), &m_subtable[1]->get_columns() };
	std::set<JoinColumn::size_type> dupl;
	for (const std::string& joincol : joincols) {
		JoinColumn::size_type i[2];
		for (unsigned int idx = 0; idx < 2; ++idx) {
			const Columns& cols(*scols[idx]);
			i[idx] = cols.find(ColumnName("", joincol));
			if (i[idx] >= cols.size())
				throw Error("join: join column " + joincol + " not found in " + (idx ? "second" : "first") + " table");
		}
		if (!dupl.insert(i[0]).second)
			throw Error("join: duplicate join column " + joincol);
		jcs.push_back(JoinColumn(i[0], i[1]));
	}
	set_joincolumns(std::move(jcs));
}

void CartoSQL::ExprJoin::compute_joincolumns_natural(void)
{
	if (!m_subtable[0])
		throw Error("first subtable not available");
	if (!m_subtable[1])
		throw Error("second subtable not available");
	JoinColumns jcs;
	const Columns *scols[2] = { &m_subtable[0]->get_columns(), &m_subtable[1]->get_columns() };
	std::set<JoinColumn::size_type> dupl;
	for (const Column& col : *scols[0]) {
		if (col.is_wildcard())
			continue;
		JoinColumn::size_type i[2];
		{
			unsigned int idx(0);
			for (; idx < 2; ++idx) {
				const Columns& cols(*scols[idx]);
				i[idx] = cols.find(ColumnName("", col.get_name()));
				if (i[idx] >= cols.size())
					break;
			}
			if (idx < 2)
				continue;
		}
		if (!dupl.insert(i[0]).second)
			continue;
	        jcs.push_back(JoinColumn(i[0], i[1]));
	}
	set_joincolumns(std::move(jcs));
}

std::vector<std::string> CartoSQL::ExprJoin::get_joincolumns_string(void) const
{
	for (unsigned int idx = 0; idx < 2; ++idx) {
		if (!get_subtable(idx))
			continue;
		const Columns& cols(get_subtable(idx)->get_columns());
		JoinColumn::size_type n(cols.size());
		std::vector<std::string> r;
		for (const JoinColumn& jc : get_joincolumns()) {
			JoinColumn::size_type i(jc[idx]);
			if (i >= n)
				throw Error("join: columns inconsistent");
			r.push_back(cols[i].get_name());
		}
		return r;
	}
	throw Error("join: no subtables");
}

template <typename... Args>
bool CartoSQL::ExprJoin::simplify_int(const CartoSQL *csql, const Expr::subtables_t& subtables, boost::intrusive_ptr<ExprJoin> p, Args&&... args)
{
	bool work(ExprTable::simplify_int(csql, subtables, p, std::forward<Args>(args)...));
	for (unsigned int i = 0; i < 2; ++i) {
		if (!p->get_subtable(i))
			continue;
		const_ptr_t p1(p->get_subtable(i)->simplify(csql, std::forward<Args>(args)...));
		if (p1) {
			work = true;
			p->set_subtable(i, p1);
		}
	}
	if (p->get_joinexpr()) {
		Expr::const_ptr_t p1(p->get_joinexpr()->simplify(csql, std::forward<Args>(args)...));
		if (p1) {
			work = true;
			p->set_joinexpr(p1);
		}
	}
	return work;
}

CartoSQL::ExprTable::const_ptr_t CartoSQL::ExprJoin::simplify(const CartoSQL *csql) const
{
	const_ptr_t p;
	{
		boost::intrusive_ptr<ExprJoin> p1(new ExprJoin());
		copy(*p1);
		p = simplify_tail(csql, p1, simplify_int(csql, get_subtables(), p1));
	}
	return p;
}

CartoSQL::ExprTable::const_ptr_t CartoSQL::ExprJoin::simplify(const CartoSQL *csql, Visitor& v) const
{
	v.visit(*this);
	const_ptr_t p;
	if (v.descend(*this)) {
		boost::intrusive_ptr<ExprJoin> p1(new ExprJoin());
		copy(*p1);
        	p = simplify_tail(csql, p1, simplify_int(csql, get_subtables(), p1, v));
	}
	const_ptr_t p1(p ? p->visitor_call_modify(csql, v) : visitor_call_modify(csql, v));
	v.visitend(*this);
	return p1 ? p1 : p;
}


void CartoSQL::ExprJoin::visit(Visitor& v) const
{
	v.visit(*this);
	if (v.descend(*this)) {
		for (unsigned int i = 0; i < 2; ++i)
			if (m_subtable[i])
				m_subtable[i]->visit(v);
		if (m_constraint == constraint_t::expr)
			m_joinexpr->visit(v);
	}
	v.visitend(*this);
}

CartoSQL::Expr::subtables_t CartoSQL::ExprJoin::get_subtables(void) const
{
	Expr::subtables_t subtbl;
	subtbl.push_back(get_subtable(0));
	subtbl.push_back(get_subtable(1));
	return subtbl;
}

class CartoSQL::ExprJoin::JoinCalc : public CalcOverride {
public:
	JoinCalc(Calc& c) : CalcOverride(c) { m_row[0] = m_row[1] = nullptr; }
	virtual const TableRow *get_table_row(unsigned int tableindex) {
		if (tableindex < 2)
			return m_row[tableindex];
		return CalcOverride::get_table_row(tableindex - 2);
	}
	virtual const OSMDB::Object *get_osmobject(void) { return nullptr; }
	virtual grouprow_t get_group_rows(void) { return 1; }
	virtual void set_group_row(grouprow_t x) { }
	void set_row(unsigned int tableindex, const TableRow *row = nullptr) {
		if (tableindex < 2)
			m_row[tableindex] = row;
	}

protected:
	const TableRow *m_row[2];
};

class CartoSQL::ExprJoin::JoinColSorter {
public:
	JoinColSorter(const JoinColumns& joincol = JoinColumns()) : m_joincol(joincol) {}
	const JoinColumns& get_joincol(void) const { return m_joincol; }
	JoinColumns& get_joincol(void) { return m_joincol; }

	template <int tblidx> struct TableRow : public CartoSQL::TableRow {
		static constexpr int tableindex = tblidx;
	};

	template <int tblidx> struct iterator : public CartoSQL::Table::iterator {
		static constexpr int tableindex = tblidx;
		using iterator_category = CartoSQL::Table::iterator::iterator_category;
		using difference_type   = CartoSQL::Table::iterator::difference_type;
		typedef TableRow<tblidx> value_type;
		typedef TableRow<tblidx>* pointer;
		typedef TableRow<tblidx>& reference;
		iterator(const CartoSQL::Table::iterator& i) : CartoSQL::Table::iterator(i) {}
		reference operator*(void) const noexcept { return static_cast<reference>(Table::iterator::operator*()); }
		pointer operator->(void) const noexcept { return static_cast<pointer>(Table::iterator::operator->()); }
		iterator& operator++(void) noexcept { return static_cast<iterator&>(Table::iterator::operator++()); }
		iterator operator++(int x) noexcept { return static_cast<iterator>(Table::iterator::operator++(x)); }
		iterator& operator--(void) noexcept { return static_cast<iterator&>(Table::iterator::operator--()); }
		iterator operator--(int x) noexcept { return static_cast<iterator&>(Table::iterator::operator--(x)); }
		reference operator[](difference_type x) const noexcept { return static_cast<reference>(Table::iterator::operator[](x)); }
		iterator& operator+=(difference_type x) noexcept { return static_cast<iterator&>(Table::iterator::operator+=(x)); }
		iterator operator+(difference_type x) const noexcept { return static_cast<iterator>(Table::iterator::operator+(x)); }
		iterator& operator-=(difference_type x) noexcept { return static_cast<iterator&>(Table::iterator::operator-=(x)); }
		iterator operator-(difference_type x) const noexcept { return static_cast<iterator>(Table::iterator::operator-(x)); }
	};

	template <int tblidx> struct const_iterator : public CartoSQL::Table::const_iterator {
		static constexpr int tableindex = tblidx;
		using iterator_category = CartoSQL::Table::const_iterator::iterator_category;
		using difference_type   = CartoSQL::Table::const_iterator::difference_type;
		typedef TableRow<tblidx> value_type;
		typedef const TableRow<tblidx>* pointer;
		typedef const TableRow<tblidx>& reference;
		const_iterator(const CartoSQL::Table::const_iterator& i) : CartoSQL::Table::const_iterator(i) {}
		reference operator*(void) const { return static_cast<reference>(Table::const_iterator::operator*()); }
		pointer operator->(void) const noexcept { return static_cast<pointer>(Table::const_iterator::operator->()); }
		const_iterator& operator++(void) noexcept { return static_cast<const_iterator&>(Table::const_iterator::operator++()); }
		const_iterator operator++(int x) noexcept { return static_cast<const_iterator>(Table::const_iterator::operator++(x)); }
		const_iterator& operator--(void) noexcept { return static_cast<const_iterator&>(Table::const_iterator::operator--()); }
		const_iterator operator--(int x) noexcept { return static_cast<const_iterator&>(Table::const_iterator::operator--(x)); }
		reference operator[](difference_type x) const noexcept { return static_cast<reference>(Table::const_iterator::operator[](x)); }
		const_iterator& operator+=(difference_type x) noexcept { return static_cast<const_iterator&>(Table::const_iterator::operator+=(x)); }
		const_iterator operator+(difference_type x) const noexcept { return static_cast<const_iterator>(Table::const_iterator::operator+(x)); }
		const_iterator& operator-=(difference_type x) noexcept { return static_cast<const_iterator&>(Table::const_iterator::operator-=(x)); }
		const_iterator operator-(difference_type x) const noexcept { return static_cast<const_iterator>(Table::const_iterator::operator-(x)); }
	};

	template <typename TA, typename TB>
	int compare(const TA& a, const TB& b) const {
		for (const JoinColumn& jc : m_joincol) {
			JoinColumn::size_type ia(jc[TA::tableindex]);
			JoinColumn::size_type ib(jc[TB::tableindex]);
			if (ia >= a.size())
				return (ib >= b.size()) ? 0 : -1;
			if (ib >= b.size())
				return 1;
			int c(a[ia].compare_value(b[ib]));
			if (c)
				return c;
		}
		return 0;
	}

	template <typename TA, typename TB>
	bool operator()(const TA& a, const TB& b) const {
		return compare(a, b) < 0;
	}

protected:
	JoinColumns m_joincol;
};

bool CartoSQL::ExprJoin::calculate(Calc& c, Table& t) const
{
	t.clear();
	JoinCalc calc(c);
	Table tbl[2];
	if (m_subtable[0] && !m_subtable[0]->calculate(calc, tbl[0])) {
		c.error(*this, "left table");
		return false;
	}
	if (m_subtable[1] && !m_subtable[1]->calculate(calc, tbl[1])) {
		c.error(*this, "right table");
		return false;
	}
	JoinColSorter jcsort(get_joincolumns());
	if (get_joinexpr())
		get_joinexpr()->extract_joincolumns(jcsort.get_joincol());
	if (!jcsort.get_joincol().empty()) {
		// pre-sorted tables for column joins
		std::stable_sort(static_cast<JoinColSorter::iterator<0> >(tbl[0].begin()),
				 static_cast<JoinColSorter::iterator<0> >(tbl[0].end()), jcsort);
		std::stable_sort(static_cast<JoinColSorter::iterator<1> >(tbl[1].begin()),
				 static_cast<JoinColSorter::iterator<1> >(tbl[1].end()), jcsort);
	}
	Table::size_type n0(tbl[0].size()), n1(tbl[1].size()), b1(0);
	t.reserve(n0 * n1);
	std::vector<bool> outer[2];
	outer[0].resize(n0, true);
	outer[1].resize(n1, true);
	for (Table::size_type i0(0); i0 < n0; ++i0) {
		const TableRow& row0(tbl[0][i0]);
		calc.set_row(0, &row0);
		Table::size_type e1(n1);
		if (!jcsort.get_joincol().empty()) {
			JoinColSorter::const_iterator<1> ib(static_cast<JoinColSorter::const_iterator<1> >(tbl[1].begin() + b1));
			JoinColSorter::const_iterator<1> ie(static_cast<JoinColSorter::const_iterator<1> >(tbl[1].begin() + n1));
			std::tie(ib, ie) = std::equal_range(ib, ie, static_cast<JoinColSorter::const_iterator<1>::reference>(row0));
			b1 = ib - tbl[1].begin();
			e1 = ie - tbl[1].begin();
		}
		for (Table::size_type i1(b1); i1 < e1; ++i1) {
			const TableRow& row1(tbl[1][i1]);
			if (get_joinexpr()) {
				calc.set_row(1, &row1);
				Value v;
				if (!get_joinexpr()->calculate(calc, v)) {
					c.error(*this, "join expression");
					return false;
				}
				if (!v.is_boolean()) {
					c.error(*this, v, "join expression does not yield a boolean");
					return false;
				}
				if (!v.get_boolean())
					continue;
			}
			t.push_back(TableRow());
			t.back().reserve(row0.size() + row1.size());
			t.back().insert(t.back().end(), row0.begin(), row0.end());
			t.back().insert(t.back().end(), row1.begin(), row1.end());
			t.back().set_osmidset(row0.get_osmidset());
			t.back().add_osmidset(row1.get_osmidset());
			outer[0][i0] = false;
			outer[1][i1] = false;
		}
	}
	if (get_jointype() == jointype_t::leftouter || get_jointype() == jointype_t::fullouter) {
		for (Table::size_type i0(0); i0 < n0; ++i0) {
			if (!outer[0][i0])
				continue;
			const TableRow& row0(tbl[0][i0]);
			t.push_back(TableRow());
			t.back().reserve(m_columns.size());
			t.back().insert(t.back().end(), row0.begin(), row0.end());
			t.back().resize(m_columns.size(), Value());
		}
	}
	if (get_jointype() == jointype_t::rightouter || get_jointype() == jointype_t::fullouter) {
		for (Table::size_type i1(0); i1 < n1; ++i1) {
			if (!outer[1][i1])
				continue;
			const TableRow& row1(tbl[1][i1]);
			t.push_back(TableRow());
			t.back().resize(m_columns.size() - row1.size(), Value());
			t.back().insert(t.back().end(), row1.begin(), row1.end());
		}
	}
	return true;
}

std::ostream& CartoSQL::ExprJoin::print(std::ostream& os, unsigned int indent, const PrintContext& ctx) const
{
	ExprTable const * const subtables[] = { &*m_subtable[0], &*m_subtable[1], nullptr };
	PrintContextSubtables ctx1(ctx, subtables);
	os << '(';
	if (!false) {
		for (const Column& col : get_columns())
			os << std::endl << std::string(indent + 2, ' ') << "-- " << col.to_str() << " type: " << Value::get_sqltypename(col.get_type());
		os << std::endl << std::string(indent + 2, ' ');
	}
	print_expr(os, m_subtable[0], indent + 2, ctx1);
	os << ' ';
	if (m_jointype == jointype_t::inner && false)
		os << "cross";
	else
		os << m_jointype;
	os << " join ";
	print_expr(os, m_subtable[1], indent + 2, ctx1);
	switch (m_constraint) {
	case constraint_t::column:
		os << " using (";
		{
			bool subseq(false);
			for (const std::string& col : get_joincolumns_string()) {
				if (subseq)
					os << ',';
				subseq = true;
				os << col;
			}
		}
		os << ')';
		break;

	case constraint_t::expr:
		Expr::print_expr(os << " on (", m_joinexpr, indent + 2, ctx1) << ')';
		break;

	default:
		break;
	}
	std::pair<bool,bool> ren(is_renamed());
	if ((!ren.first && !ren.second) || get_columns().empty())
		return os;
	os << " as \"" << get_columns().front().get_table() << '"';
	if (!ren.second)
		return os;
	char sep('(');
	for (const Column& col : get_columns()) {
		os << sep << '"' << col.get_name() << '"';
		sep = ',';
	}
	if (sep != '(')
		os << ')';
	return os;
}

int CartoSQL::ExprJoin::compare(const ExprTable& x) const
{
	int c(ExprTable::compare(x));
	if (c)
		return c;
	const ExprJoin& xx(static_cast<const ExprJoin&>(x));
	for (unsigned int i = 0; i < 2; ++i) {
		c = ExprTable::compare(m_subtable[i], xx.m_subtable[i]);
		if (c)
			return c;
	}
	if (m_constraint < xx.m_constraint)
		return -1;
	if (xx.m_constraint < m_constraint)
		return 1;
	switch (m_constraint) {
	case constraint_t::expr:
		c = Expr::compare(m_joinexpr, xx.m_joinexpr);
		if (c)
			return c;
		break;

	case constraint_t::column:
		c = m_joincolumns.compare(xx.m_joincolumns);
		if (c)
			return c;
		break;

	default:
		break;
	}
	if (m_jointype < xx.m_jointype)
		return -1;
	if (xx.m_jointype < m_jointype)
		return 1;
	return 0;
}

CartoSQL::ExprTable::const_ptr_t CartoSQL::ExprJoin::push_filter_up(const CartoSQL *csql, const Expr::const_ptr_t& expr) const
{
	static constexpr bool debug = false;
	if (!csql || !expr || expr->is_true())
		return const_ptr_t();
	Expr::const_ptr_t filter[2];
	{
		Expr::exprlist_t andterms;
		{
			PushConditionUpResolve resolve(get_ptr());
			Expr::const_ptr_t exprr(expr->simplify(csql, resolve)), exprs;
			if (resolve.is_error())
				return const_ptr_t();
			if (exprr)
				exprs = exprr->simplify(csql);
			else
				exprs = expr->simplify(csql);
			if (exprs)
				exprs->split_and_terms(andterms);
			else if (exprr)
				exprr->split_and_terms(andterms);
			else
				expr->split_and_terms(andterms);
		}
		if (andterms.empty())
			return const_ptr_t();
		for (unsigned int i = 0; i < 2; ++i) {
			Expr::exprlist_t aterms;
			for (const Expr::const_ptr_t& andterm : andterms) {
				Expr::const_ptr_t aterm;
				{
					PushConditionUpRename ren(i);
					aterm = andterm->simplify(csql, ren);
					if (ren.is_error())
						continue;
				}
				if (aterm)
					aterms.push_back(aterm);
				else
					aterms.push_back(andterm);
			}
			filter[i] = Expr::combine_and_terms(aterms);
		}
	}
	bool work(false);
	ExprTable::const_ptr_t subtbl[2];
	for (unsigned int i = 0; i < 2; ++i) {
		if (!get_subtable(i) || !filter[i] || filter[i]->is_true())
			continue;
		ExprTable::const_ptr_t p(get_subtable(i));
		if (p->get_type() == type_t::join)
			p = p->select_from();
		p = p->push_filter_up(csql, filter[i]);
		if (!p)
			continue;
		if (p->get_type() == type_t::select) {
			const ExprSelect& sel(static_cast<const ExprSelect&>(*p));
			if (!sel.get_whereexpr()) {
				p = sel.get_fromexpr();
				if (!p || p == get_subtable(i))
					continue;
			}
		}
		work = true;
		subtbl[i] = p;
	}
	if (debug) {
		csql->print_expr(std::cerr << "ExprJoin::push_filter_up: ", expr, 4) << std::endl;
		for (unsigned int i = 0; i < 2; ++i)
			csql->print_expr(std::cerr << "  filter" << i << ": ", filter[i], 4) << std::endl;
		csql->print_expr(std::cerr << "  Old: ", get_ptr(), 4) << std::endl;
		for (unsigned int i = 0; i < 2; ++i)
			csql->print_expr(std::cerr << "  subtable" << i << ": ", subtbl[i], 4) << std::endl;
	}
	if (!work)
		return ExprTable::const_ptr_t();
	boost::intrusive_ptr<ExprJoin> p1(new ExprJoin());
	copy(*p1);
	for (unsigned int i = 0; i < 2; ++i)
		if (subtbl[i])
			p1->set_subtable(i, subtbl[i]);
	return p1;
}

CartoSQL::ExprTable::const_ptr_t CartoSQL::ExprJoin::remove_unused_columns(const CartoSQL *csql, columnset_t& colsused) const
{
	// find join condition columns
	columnset_t joincolsused[2];
	switch (m_constraint) {
	case constraint_t::column:
		m_joincolumns.get_columns_used(joincolsused);
		break;

	case constraint_t::expr:
	{
		TableColumnSets cs;
		m_joinexpr->visit(cs);
		for (unsigned int i = 0; i < 2; ++i)
			joincolsused[i] = cs.get_columnset(i);
		break;
	}

	default:
		break;
	}
	for (unsigned int colnr : colsused) {
		if (colnr >= m_columns.size())
			throw Error("ExprJoin::remove_unused_columns: used column out of range");
		const Column& col(m_columns[colnr]);
		if (!col.get_expr())
			throw Error("ExprJoin::remove_unused_columns: no column expression");
		if (col.get_expr()->get_type() != Expr::type_t::tablecolumn)
			throw Error("ExprJoin::remove_unused_columns: invalid column expression");
		const ExprTableColumn& tc(static_cast<const ExprTableColumn&>(*col.get_expr()));
		if (tc.get_tableindex() >= 2)
			throw Error("ExprJoin::remove_unused_columns: invalid column expression");
		joincolsused[tc.get_tableindex()].insert(tc.get_columnindex());
	}
	boost::intrusive_ptr<ExprJoin> p1(new ExprJoin());
	copy(*p1);
	bool work(false);
	for (unsigned int i = 0; i < 2; ++i) {
		if (!p1->m_subtable[i])
			continue;
		const_ptr_t p(p1->m_subtable[i]->remove_unused_columns(csql, joincolsused[i]));
		if (!p)
			continue;
		p1->m_subtable[i] = p;
		work = true;
	}
	for (unsigned int i(0), n(p1->get_columns().size()); i < n; ++i) {
		const Column& col(p1->get_columns()[i]);
		if (!col.get_expr())
			throw Error("ExprJoin::remove_unused_columns: no column expression");
		if (col.get_expr()->get_type() != Expr::type_t::tablecolumn)
			throw Error("ExprJoin::remove_unused_columns: invalid column expression");
		const ExprTableColumn& tc(static_cast<const ExprTableColumn&>(*col.get_expr()));
		if (tc.get_tableindex() >= 2)
			throw Error("ExprJoin::remove_unused_columns: invalid column expression");
		const columnset_t& cs(joincolsused[tc.get_tableindex()]);
		if (cs.find(tc.get_columnindex()) == cs.end())
			continue;
		colsused.insert(i);
	}
	if (!is_all_columns(colsused)) {
		work = true;
		p1->get_columns().remove_unused_columns(colsused);
	}
	for (unsigned int i = 0; i < 2; ++i) {
      		RenumberTableColumns ren(i, joincolsused[i]);
		ExprTable::const_ptr_t p2(p1->simplify(csql, ren));
		if (p2) {
			if (p2->get_type() != type_t::join)
				throw Error("ExprJoin::remove_unused_columns: renumber changed type");
			p1 = boost::static_pointer_cast<ExprJoin>(boost::const_pointer_cast<ExprTable>(p2));
		}
	}
	if (work)
		return p1;
	return const_ptr_t();
}

void CartoSQL::ExprTableReference::copy(ExprTableReference& x) const
{
	ExprTable::copy(x);
	x.set_name(get_name());
}

CartoSQL::ExprTable::const_ptr_t CartoSQL::ExprTableReference::simplify(const CartoSQL *csql) const
{
	const_ptr_t p;
	{
		boost::intrusive_ptr<ExprTableReference> p1(new ExprTableReference());
		copy(*p1);
		p = simplify_tail(csql, p1, simplify_int(csql, get_subtables(), p1));
	}
	return p;
}

CartoSQL::ExprTable::const_ptr_t CartoSQL::ExprTableReference::simplify(const CartoSQL *csql, Visitor& v) const
{
	v.visit(*this);
	const_ptr_t p;
	{
		boost::intrusive_ptr<ExprTableReference> p1(new ExprTableReference());
		copy(*p1);
		p = simplify_tail(csql, p1, simplify_int(csql, get_subtables(), p1, v));
	}
	const_ptr_t p1(p ? p->visitor_call_modify(csql, v) : visitor_call_modify(csql, v));
	v.visitend(*this);
	return p1 ? p1 : p;
}

bool CartoSQL::ExprTableReference::calculate(Calc& c, Table& t) const
{
	t.clear();
	const Table *tbl(c.get_referenced_table(get_name()));
	if (!tbl) {
		c.error(*this, "referenced table not found");
		return false;
	}
	t = *tbl;
	return true;
}

std::ostream& CartoSQL::ExprTableReference::print(std::ostream& os, unsigned int indent, const PrintContext& ctx) const
{
	return os << get_name();
}

int CartoSQL::ExprTableReference::compare(const ExprTable& x) const
{
	int c(ExprTable::compare(x));
	if (c)
		return c;
	const ExprTableReference& xx(static_cast<const ExprTableReference&>(x));
	return m_name.compare(xx.m_name);
}

CartoSQL::ExprCommonTables::ExprCommonTables(const const_ptr_t& subtbl, const namedtables_t& tbls)
	: m_subtable(subtbl), m_tables(tbls)
{
	if (m_subtable)
		m_columns = m_subtable->get_columns();
	sort_tables();
}

void CartoSQL::ExprCommonTables::set_subtable(const const_ptr_t& t)
{
	m_subtable = t;
	m_columns.clear();
	if (m_subtable)
		m_columns = m_subtable->get_columns();
}

void CartoSQL::ExprCommonTables::set_subtable(const_ptr_t&& t)
{
	m_subtable.swap(t);
	m_columns.clear();
	if (m_subtable)
		m_columns = m_subtable->get_columns();
}

void CartoSQL::ExprCommonTables::set_tables(const namedtables_t& t)
{
	m_tables = t;
	sort_tables();
}

void CartoSQL::ExprCommonTables::set_tables(namedtables_t&& t)
{
	m_tables.swap(t);
	sort_tables();
}

void CartoSQL::ExprCommonTables::copy(ExprCommonTables& x) const
{
	ExprTable::copy(x);
	x.set_subtable(get_subtable());
	x.set_tables(get_tables());
}

template <typename... Args>
bool CartoSQL::ExprCommonTables::simplify_int(const CartoSQL *csql, const Expr::subtables_t& subtables, boost::intrusive_ptr<ExprCommonTables> p, Args&&... args)
{
	bool work(ExprTable::simplify_int(csql, subtables, p, std::forward<Args>(args)...));
	if (p->get_subtable()) {
		const_ptr_t p1(p->simplify(csql, std::forward<Args>(args)...));
		if (p1) {
			p->set_subtable(p1);
			work = true;
		}
	}
	{
		bool work1(false);
		for (const_ptr_t& p : p->m_tables) {
			const_ptr_t p1(p->simplify(csql, std::forward<Args>(args)...));
			if (!p1)
				continue;
			p = p1;
			work1 = true;
		}
		if (work1) {
			p->sort_tables();
			work = true;
		}
	}
	return work;
}

CartoSQL::ExprTable::const_ptr_t CartoSQL::ExprCommonTables::simplify(const CartoSQL *csql) const
{
	const_ptr_t p;
	{
		boost::intrusive_ptr<ExprCommonTables> p1(new ExprCommonTables());
		copy(*p1);
		p = simplify_tail(csql, p1, simplify_int(csql, get_subtables(), p1));
	}
	return p;
}

CartoSQL::ExprTable::const_ptr_t CartoSQL::ExprCommonTables::simplify(const CartoSQL *csql, Visitor& v) const
{
	v.visit(*this);
	const_ptr_t p;
	if (v.descend(*this)) {
		boost::intrusive_ptr<ExprCommonTables> p1(new ExprCommonTables());
		copy(*p1);
		p = simplify_tail(csql, p1, simplify_int(csql, get_subtables(), p1, v));
	}
	const_ptr_t p1(p ? p->visitor_call_modify(csql, v) : visitor_call_modify(csql, v));
	v.visitend(*this);
	return p1 ? p1 : p;
}

void CartoSQL::ExprCommonTables::visit(Visitor& v) const
{
	v.visit(*this);
	if (v.descend(*this)) {
		if (get_subtable())
			get_subtable()->visit(v);
		for (const const_ptr_t& p : get_tables())
			if (p)
				p->visit(v);
	}
	v.visitend(*this);
}

CartoSQL::Expr::subtables_t CartoSQL::ExprCommonTables::get_subtables(void) const
{
	Expr::subtables_t subtbl;
	subtbl.push_back(get_subtable());
	return subtbl;
}

class CartoSQL::ExprCommonTables::NameSorter {
public:
	static bool is_ok(const ExprTable::const_ptr_t& p) {
		return p && !p->get_columns().empty() && !p->get_columns().front().get_table().empty();
	}

	static const std::string& get_table(const ExprTable::const_ptr_t& p) {
		if (is_ok)
			return p->get_columns().front().get_table();
		static const std::string empty;
		return empty;
	}

	bool operator()(const ExprTable::const_ptr_t& a, const ExprTable::const_ptr_t& b) const {
		if (!is_ok(a))
			return false;
		if (!is_ok(b))
			return true;
		return operator()(get_table(a), get_table(b));
	}

	bool operator()(const ExprTable::const_ptr_t& a, const std::string& b) const {
		if (!is_ok(a))
			return false;
		return operator()(get_table(a), b);
	}

	bool operator()(const std::string& a, const ExprTable::const_ptr_t& b) const {
		if (!is_ok(b))
			return true;
		return operator()(a, get_table(b));
	}

	bool operator()(const std::string& a, const std::string& b) const {
		return boost::ilexicographical_compare(a, b);
	}
};

std::ostream& CartoSQL::ExprCommonTables::print(std::ostream& os, unsigned int indent, const PrintContext& ctx) const
{
	os << "with";
	bool subseq(false);
	for (const const_ptr_t& p : get_tables()) {
		if (!NameSorter::is_ok(p))
			continue;
		if (subseq)
			os << ',';
		subseq = true;
		p->print(os << std::endl << std::string(indent, ' ') << NameSorter::get_table(p)
			 << " as (", indent + 2, ctx) << ')';
	}
	if (get_subtable())
		get_subtable()->print(os << ' ', indent, ctx);
	return os;
}

int CartoSQL::ExprCommonTables::compare(const ExprTable& x) const
{
	int c(ExprTable::compare(x));
	if (c)
		return c;
	const ExprCommonTables& xx(static_cast<const ExprCommonTables&>(x));
	c = ExprTable::compare(get_subtable(), xx.get_subtable());
	if (c)
		return c;
	exprlist_t::size_type i(xx.get_tables().size()), n(get_tables().size());
	if (n < i)
		return -1;
	if (i < n)
		return 1;
	for (i = 0; i < n; ++i) {
		int c(ExprTable::compare(get_tables()[i], xx.get_tables()[i]));
		if (c)
			return c;
	}
	return 0;
}

class CartoSQL::ExprCommonTables::CommonTablesCalc : public CalcOverride {
public:
	CommonTablesCalc(Calc& c) : CalcOverride(c) {}

	virtual const Table *get_referenced_table(const std::string& name) {
		tables_t::const_iterator i(m_table.find(name));
		if (i == m_table.end())
			return nullptr;
		return &i->second;
	}

	void add_table(const std::string& name, const Table& tbl) { m_table[name] = tbl; }
	void add_table(const std::string& name, Table&& tbl) { m_table[name].swap(tbl); }

protected:
	typedef std::map<std::string,Table> tables_t;
	tables_t m_table;
};

bool CartoSQL::ExprCommonTables::calculate(Calc& c, Table& t) const
{
	t.clear();
	CommonTablesCalc calc(c);
	for (const const_ptr_t& p : get_tables()) {
		if (!NameSorter::is_ok(p))
			continue;
		Table tbl;
		if (!p->calculate(c, tbl)) {
			c.error(*this, "named table");
			return false;
		}
		calc.add_table(NameSorter::get_table(p), std::move(tbl));
	}
	if (!get_subtable() || !get_subtable()->calculate(calc, t)) {
		c.error(*this, "subtable");
		return false;
	}
	return true;
}

const CartoSQL::ExprTable::const_ptr_t& CartoSQL::ExprCommonTables::find_table(const std::string& n) const
{
	static const const_ptr_t nulltable;
	namedtables_t::const_iterator i(std::lower_bound(m_tables.begin(), m_tables.end(), n, NameSorter()));
	if (i == m_tables.end())
		return nulltable;
	if (NameSorter()(n, *i))
		return nulltable;
	return *i;
}

void CartoSQL::ExprCommonTables::sort_tables(void)
{
	std::sort(m_tables.begin(), m_tables.end(), NameSorter());
	while (!m_tables.empty() && !NameSorter::is_ok(m_tables.back()))
		m_tables.pop_back();
}

CartoSQL::ExprTable::const_ptr_t CartoSQL::ExprCommonTables::push_filter_up(const CartoSQL *csql, const Expr::const_ptr_t& expr) const
{
	if (!csql || !expr || expr->is_true() || !get_subtable())
		return const_ptr_t();
	const_ptr_t p(get_subtable()->push_filter_up(csql, expr));
	if (!p)
		return const_ptr_t();
	boost::intrusive_ptr<ExprCommonTables> p1(new ExprCommonTables());
	copy(*p1);
	p1->set_subtable(p);
	return p1;
}
