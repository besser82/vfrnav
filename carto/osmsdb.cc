/*****************************************************************************/

/*
 *      osmsdb.cc  --  OpenStreetMap proprietary database for static data.
 *
 *      Copyright (C) 2020  Thomas Sailer (t.sailer@alumni.ethz.ch)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

/*****************************************************************************/

#include <iomanip>
#include <fstream>

#include "osmsdb.h"
#include "hibernate.h"

template<int sz> const unsigned int OSMStaticDB::BinFileEntry<sz>::size;

template<int sz> OSMStaticDB::BinFileEntry<sz>::BinFileEntry(void)
{
	memset(m_data, 0, sz);
}

template<int sz> uint8_t OSMStaticDB::BinFileEntry<sz>::readu8(unsigned int idx) const
{
	if (idx >= size)
		return 0;
	return m_data[idx];
}

template<int sz> uint16_t OSMStaticDB::BinFileEntry<sz>::readu16(unsigned int idx) const
{
	if (idx + 1U >= size)
		return 0;
	return m_data[idx] | (m_data[idx + 1U] << 8);
}

template<int sz> uint32_t OSMStaticDB::BinFileEntry<sz>::readu24(unsigned int idx) const
{
	if (idx + 2U >= size)
		return 0;
	return m_data[idx] | (m_data[idx + 1U] << 8) | (m_data[idx + 2U] << 16);
}

template<int sz> uint32_t OSMStaticDB::BinFileEntry<sz>::readu32(unsigned int idx) const
{
	if (idx + 3U >= size)
		return 0;
	return m_data[idx] | (m_data[idx + 1U] << 8) | (m_data[idx + 2U] << 16) | (m_data[idx + 3U] << 24);
}

template<int sz> uint64_t OSMStaticDB::BinFileEntry<sz>::readu40(unsigned int idx) const
{
	if (idx + 4U >= size)
		return 0;
	uint32_t r0(m_data[idx] | (m_data[idx + 1U] << 8) | (m_data[idx + 2U] << 16) | (m_data[idx + 3U] << 24));
	uint32_t r1(m_data[idx + 4U]);
	uint64_t r(r1);
	r <<= 32;
	r |= r0;
	return r;
}

template<int sz> uint64_t OSMStaticDB::BinFileEntry<sz>::readu48(unsigned int idx) const
{
	if (idx + 5U >= size)
		return 0;
	uint32_t r0(m_data[idx] | (m_data[idx + 1U] << 8) | (m_data[idx + 2U] << 16) | (m_data[idx + 3U] << 24));
	uint32_t r1(m_data[idx + 4U] | (m_data[idx + 5U] << 8));
	uint64_t r(r1);
	r <<= 32;
	r |= r0;
	return r;
}

template<int sz> uint64_t OSMStaticDB::BinFileEntry<sz>::readu64(unsigned int idx) const
{
	if (idx + 7U >= size)
		return 0;
	uint32_t r0(m_data[idx] | (m_data[idx + 1U] << 8) | (m_data[idx + 2U] << 16) | (m_data[idx + 3U] << 24));
	uint32_t r1(m_data[idx + 4U] | (m_data[idx + 5U] << 8) | (m_data[idx + 6U] << 16) | (m_data[idx + 7U] << 24));
	uint64_t r(r1);
	r <<= 32;
	r |= r0;
	return r;
}

template<int sz> void OSMStaticDB::BinFileEntry<sz>::writeu8(unsigned int idx, uint8_t v)
{
	if (idx >= size)
		return;
	m_data[idx] = v;
}

template<int sz> void OSMStaticDB::BinFileEntry<sz>::writeu16(unsigned int idx, uint16_t v)
{
	if (idx + 1U >= size)
		return;
	m_data[idx] = v;
	m_data[idx + 1U] = v >> 8;
}

template<int sz> void OSMStaticDB::BinFileEntry<sz>::writeu24(unsigned int idx, uint32_t v)
{
	if (idx + 2U >= size)
		return;
	m_data[idx] = v;
	m_data[idx + 1U] = v >> 8;
	m_data[idx + 2U] = v >> 16;
}

template<int sz> void OSMStaticDB::BinFileEntry<sz>::writeu32(unsigned int idx, uint32_t v)
{
	if (idx + 3U >= size)
		return;
	m_data[idx] = v;
	m_data[idx + 1U] = v >> 8;
	m_data[idx + 2U] = v >> 16;
	m_data[idx + 3U] = v >> 24;
}

template<int sz> void OSMStaticDB::BinFileEntry<sz>::writeu40(unsigned int idx, uint64_t v)
{
	if (idx + 4U >= size)
		return;
	m_data[idx] = v;
	m_data[idx + 1U] = v >> 8;
	m_data[idx + 2U] = v >> 16;
	m_data[idx + 3U] = v >> 24;
	m_data[idx + 4U] = v >> 32;
}

template<int sz> void OSMStaticDB::BinFileEntry<sz>::writeu48(unsigned int idx, uint64_t v)
{
	if (idx + 5U >= size)
		return;
	m_data[idx] = v;
	m_data[idx + 1U] = v >> 8;
	m_data[idx + 2U] = v >> 16;
	m_data[idx + 3U] = v >> 24;
	m_data[idx + 4U] = v >> 32;
	m_data[idx + 5U] = v >> 40;
}

template<int sz> void OSMStaticDB::BinFileEntry<sz>::writeu64(unsigned int idx, uint64_t v)
{
	if (idx + 7U >= size)
		return;
	m_data[idx] = v;
	m_data[idx + 1U] = v >> 8;
	m_data[idx + 2U] = v >> 16;
	m_data[idx + 3U] = v >> 24;
	m_data[idx + 4U] = v >> 32;
	m_data[idx + 5U] = v >> 40;
	m_data[idx + 6U] = v >> 48;
	m_data[idx + 7U] = v >> 56;
}

const char OSMStaticDB::BinFileHeader::signature[] = "vfrnav OSM Static Database V1\n";

bool OSMStaticDB::BinFileHeader::check_signature(void) const
{
	return !memcmp(m_data, signature, sizeof(signature));
}

void OSMStaticDB::BinFileHeader::set_signature(void)
{
	memset(m_data, 0, 32);
	memset(m_data + 72, 0, 24);
	memset(m_data + 116, 0, 12);
	memset(m_data + 168, 0, 24);
	memset(m_data + 216, 0, 40);
	memcpy(m_data, signature, sizeof(signature));
}

uint64_t OSMStaticDB::BinFileHeader::get_objdiroffs(layer_t layer) const
{
	if (layer > layer_t::last)
		return 0;
	return readu64(32 + 8 * static_cast<unsigned int>(layer));
}

void OSMStaticDB::BinFileHeader::set_objdiroffs(layer_t layer, uint64_t offs)
{
	if (layer > layer_t::last)
		return;
	writeu64(32 + 8 * static_cast<unsigned int>(layer), offs);
}

uint32_t OSMStaticDB::BinFileHeader::get_objdirentries(layer_t layer) const
{
	if (layer > layer_t::last)
		return 0;
	return readu32(96 + 4 * static_cast<unsigned int>(layer));
}

void OSMStaticDB::BinFileHeader::set_objdirentries(layer_t layer, uint32_t n)
{
	if (layer > layer_t::last)
		return;
	writeu32(96 + 4 * static_cast<unsigned int>(layer), n);
}

uint64_t OSMStaticDB::BinFileHeader::get_rtreeoffs(layer_t layer) const
{
	if (layer > layer_t::last)
		return 0;
	return readu64(128 + 8 * static_cast<unsigned int>(layer));
}

void OSMStaticDB::BinFileHeader::set_rtreeoffs(layer_t layer, uint64_t offs)
{
	if (layer > layer_t::last)
		return;
	writeu64(128 + 8 * static_cast<unsigned int>(layer), offs);
}

uint64_t OSMStaticDB::BinFileHeader::get_tagkeyoffs(void) const
{
	return readu64(192);
}

void OSMStaticDB::BinFileHeader::set_tagkeyoffs(uint64_t offs)
{
	writeu64(192, offs);
}

uint32_t OSMStaticDB::BinFileHeader::get_tagkeyentries(void) const
{
	return readu32(208);
}

void OSMStaticDB::BinFileHeader::set_tagkeyentries(uint32_t n)
{
	writeu32(208, n);
}

uint64_t OSMStaticDB::BinFileHeader::get_tagnameoffs(void) const
{
	return readu64(200);
}

void OSMStaticDB::BinFileHeader::set_tagnameoffs(uint64_t offs)
{
	writeu64(200, offs);
}

uint32_t OSMStaticDB::BinFileHeader::get_tagnameentries(void) const
{
	return readu32(212);
}

void OSMStaticDB::BinFileHeader::set_tagnameentries(uint32_t n)
{
	writeu32(212, n);
}

template<OSMStaticDB::layer_t layer>
typename OSMStaticDB::layer_types<layer>::IndexRangeType OSMStaticDB::BinFileHeader::get_objdir_range(void) const
{
	typedef typename layer_types<layer>::IndexType IndexType;
	uint64_t offs(get_objdiroffs(layer));
	if (!offs)
		return std::make_pair(nullptr, nullptr);
	const IndexType *p(reinterpret_cast<const IndexType *>(reinterpret_cast<const uint8_t *>(this) + offs));
	return std::make_pair(p, p + get_objdirentries(layer));
}

const OSMStaticDB::BinFileRtreeInternalNode *OSMStaticDB::BinFileHeader::get_rtree_root(layer_t layer) const
{
	uint64_t offs(get_rtreeoffs(layer));
	if (!offs)
		return nullptr;
	return reinterpret_cast<const BinFileRtreeInternalNode *>(reinterpret_cast<const uint8_t *>(this) + offs);
}

const OSMStaticDB::BinFileTagKeyEntry *OSMStaticDB::BinFileHeader::get_tagkey_begin(void) const
{
	uint64_t offs(get_tagkeyoffs());
	if (!offs)
		return nullptr;
	return reinterpret_cast<const BinFileTagKeyEntry *>(reinterpret_cast<const uint8_t *>(this) + offs);
}

const OSMStaticDB::BinFileTagKeyEntry *OSMStaticDB::BinFileHeader::get_tagkey_end(void) const
{
	const BinFileTagKeyEntry *p(get_tagkey_begin());
	if (p)
		p += get_tagkeyentries();
	return p;
	
}

const OSMStaticDB::BinFileTagNameEntry *OSMStaticDB::BinFileHeader::get_tagname_begin(void) const
{
	uint64_t offs(get_tagnameoffs());
	if (!offs)
		return nullptr;
	return reinterpret_cast<const BinFileTagNameEntry *>(reinterpret_cast<const uint8_t *>(this) + offs);
}

const OSMStaticDB::BinFileTagNameEntry *OSMStaticDB::BinFileHeader::get_tagname_end(void) const
{
	const BinFileTagNameEntry *p(get_tagname_begin());
	if (p)
		p += get_tagnameentries();
	return p;
	
}

OSMStaticDB::Object::id_t OSMStaticDB::BinFilePointEntry::get_id(void) const
{
	return reads64(0);
}

void OSMStaticDB::BinFilePointEntry::set_id(Object::id_t id)
{
	writes64(0, id);
}

Point OSMStaticDB::BinFilePointEntry::get_location(void) const
{
	return Point(reads32(8), reads32(12));
}

void OSMStaticDB::BinFilePointEntry::set_location(const Point& location)
{
	writes32(8, location.get_lon());
	writes32(12, location.get_lat());
}

uint64_t OSMStaticDB::BinFilePointEntry::get_dataoffs(void) const
{
	return readu40(16);
}

void OSMStaticDB::BinFilePointEntry::set_dataoffs(uint64_t offs)
{
	writeu40(16, offs);
}

const uint8_t *OSMStaticDB::BinFilePointEntry::get_dataptr(void) const
{
	return reinterpret_cast<const uint8_t *>(this) + get_dataoffs();
}

uint32_t OSMStaticDB::BinFilePointEntry::get_datasize(void) const
{
	return readu24(21);
}

void OSMStaticDB::BinFilePointEntry::set_datasize(uint32_t sz)
{
	writeu24(21, sz);
}

OSMStaticDB::Object::id_t OSMStaticDB::BinFileLineAreaEntry::get_id(void) const
{
	return reads64(0);
}

void OSMStaticDB::BinFileLineAreaEntry::set_id(Object::id_t id)
{
	writes64(0, id);
}

Rect OSMStaticDB::BinFileLineAreaEntry::get_bbox(void) const
{
	return Rect(Point(reads32(8), reads32(12)), Point(reads32(16), reads32(20)));
}

void OSMStaticDB::BinFileLineAreaEntry::set_bbox(const Rect& bbox)
{
	writes32(8, bbox.get_southwest().get_lon());
	writes32(12, bbox.get_southwest().get_lat());
	writes32(16, bbox.get_northeast().get_lon());
	writes32(20, bbox.get_northeast().get_lat());
}

uint64_t OSMStaticDB::BinFileLineAreaEntry::get_dataoffs(void) const
{
	return readu40(24);
}

void OSMStaticDB::BinFileLineAreaEntry::set_dataoffs(uint64_t offs)
{
	writeu40(24, offs);
}

const uint8_t *OSMStaticDB::BinFileLineAreaEntry::get_dataptr(void) const
{
	return reinterpret_cast<const uint8_t *>(this) + get_dataoffs();
}

uint32_t OSMStaticDB::BinFileLineAreaEntry::get_datasize(void) const
{
	return readu24(29);
}

void OSMStaticDB::BinFileLineAreaEntry::set_datasize(uint32_t sz)
{
	writeu24(29, sz);
}

uint64_t OSMStaticDB::BinFileTagKeyEntry::get_stroffs(void) const
{
	return readu32(0);
}

void OSMStaticDB::BinFileTagKeyEntry::set_stroffs(uint64_t offs)
{
	writeu32(0, offs);
}

const char *OSMStaticDB::BinFileTagKeyEntry::get_strptr(void) const
{
	return reinterpret_cast<const char *>(this) + get_stroffs();
}

bool OSMStaticDB::BinFileTagKeySorter::operator()(const char *a, const char *b) const
{
	if (!a)
		return !!b;
	if (!b)
		return false;
	return strcmp(a, b) < 0;
}

uint64_t OSMStaticDB::BinFileTagNameEntry::get_stroffs(void) const
{
	return readu32(0);
}

void OSMStaticDB::BinFileTagNameEntry::set_stroffs(uint64_t offs)
{
	writeu32(0, offs);
}

const char *OSMStaticDB::BinFileTagNameEntry::get_strptr(void) const
{
	return reinterpret_cast<const char *>(this) + get_stroffs();
}

bool OSMStaticDB::BinFileTagNameSorter::operator()(const char *a, const char *b) const
{
	if (!a)
		return !!b;
	if (!b)
		return false;
	return strcmp(a, b) < 0;
}

uint16_t OSMStaticDB::BinFileRtreeLeafNode::get_nodes(void) const
{
	return readu16(0);
}

void OSMStaticDB::BinFileRtreeLeafNode::set_nodes(uint16_t n)
{
	writeu16(0, std::min(n, static_cast<uint16_t>(rtreemaxelperpage)));
}

std::size_t OSMStaticDB::BinFileRtreeLeafNode::get_size(void) const
{
	return get_nodes() * 4 + 2;
}

OSMStaticDB::BinFileRtreeLeafNode::index_t OSMStaticDB::BinFileRtreeLeafNode::get_index(uint16_t n) const
{
	if (n >= static_cast<uint16_t>(rtreemaxelperpage))
		return 0;
	return readu32(n * 4 + 2);
}

void OSMStaticDB::BinFileRtreeLeafNode::set_index(uint16_t n, index_t index)
{
	if (n >= static_cast<uint16_t>(rtreemaxelperpage))
		return;
	writeu32(n * 4 + 2, index);
}

uint16_t OSMStaticDB::BinFileRtreeInternalNode::get_nodes(void) const
{
	return readu16(0) & 0x7fff;
}

void OSMStaticDB::BinFileRtreeInternalNode::set_nodes(uint16_t n)
{
	uint16_t x(readu16(0));
	x ^= (x ^ std::min(n, static_cast<uint16_t>(rtreemaxelperpage))) & 0x7fff;
	writeu16(0, x);
}

bool OSMStaticDB::BinFileRtreeInternalNode::is_pointstoleaf(void) const
{
	return !!(readu16(0) & 0x8000);
}

void OSMStaticDB::BinFileRtreeInternalNode::set_pointstoleaf(bool l)
{
	uint16_t x(readu16(0));
	x ^= (x ^ -!!l) & 0x8000;
	writeu16(0, x);
}

std::size_t OSMStaticDB::BinFileRtreeInternalNode::get_size(void) const
{
	return get_nodes() * 24 + 2;
}

int64_t OSMStaticDB::BinFileRtreeInternalNode::get_addroffs(uint16_t n) const
{
	if (n >= static_cast<uint16_t>(rtreemaxelperpage))
		return 0;
	return reads64(n * 24 + 2);
}

void OSMStaticDB::BinFileRtreeInternalNode::set_addroffs(uint16_t n, int64_t addr)
{
	if (n >= static_cast<uint16_t>(rtreemaxelperpage))
		return;
	writes64(n * 24 + 2, addr);
}

const OSMStaticDB::BinFileRtreeLeafNode *OSMStaticDB::BinFileRtreeInternalNode::get_leafnode(uint16_t n) const
{
	return reinterpret_cast<const BinFileRtreeLeafNode *>(reinterpret_cast<const uint8_t *>(this) + get_addroffs(n));
}

const OSMStaticDB::BinFileRtreeInternalNode *OSMStaticDB::BinFileRtreeInternalNode::get_internalnode(uint16_t n) const
{
	return reinterpret_cast<const BinFileRtreeInternalNode *>(reinterpret_cast<const uint8_t *>(this) + get_addroffs(n));
}

Rect OSMStaticDB::BinFileRtreeInternalNode::get_bbox(uint16_t n) const
{
	if (n >= static_cast<uint16_t>(rtreemaxelperpage))
		return Rect::invalid;
	return Rect(Point(reads32(n * 24 + 10), reads32(n * 24 + 14)), Point(reads32(n * 24 + 18), reads32(n * 24 + 22)));
}

void OSMStaticDB::BinFileRtreeInternalNode::set_bbox(uint16_t n, const Rect& bbox)
{
	if (n >= static_cast<uint16_t>(rtreemaxelperpage))
		return;
	writes32(n * 24 + 10, bbox.get_southwest().get_lon());
	writes32(n * 24 + 14, bbox.get_southwest().get_lat());
	writes32(n * 24 + 18, bbox.get_northeast().get_lon());
	writes32(n * 24 + 22, bbox.get_northeast().get_lat());
}

const std::string& to_str(OSMStaticDB::Object::type_t t)
{
	switch (t) {
	case OSMStaticDB::Object::type_t::point:
		return OSMStaticDB::ObjPoint::object_name;

	case OSMStaticDB::Object::type_t::line:
		return OSMStaticDB::ObjLine::object_name;

	case OSMStaticDB::Object::type_t::area:
		return OSMStaticDB::ObjArea::object_name;

	default:
	{
		static const std::string r("?");
		return r;
	}
	}
}

const std::string& to_str(OSMStaticDB::layer_t l)
{
	switch (l) {
	case OSMStaticDB::layer_t::boundarylinesland:
		return OSMStaticDB::layer_data_type<OSMStaticDB::layer_t::boundarylinesland>::name;

	case OSMStaticDB::layer_t::icesheetpolygons:
		return OSMStaticDB::layer_data_type<OSMStaticDB::layer_t::icesheetpolygons>::name;

	case OSMStaticDB::layer_t::icesheetoutlines:
		return OSMStaticDB::layer_data_type<OSMStaticDB::layer_t::icesheetoutlines>::name;

	case OSMStaticDB::layer_t::waterpolygons:
		return OSMStaticDB::layer_data_type<OSMStaticDB::layer_t::waterpolygons>::name;

	case OSMStaticDB::layer_t::simplifiedwaterpolygons:
		return OSMStaticDB::layer_data_type<OSMStaticDB::layer_t::simplifiedwaterpolygons>::name;

	default:
	{
		static const std::string r("?");
		return r;
	}
	}
}

OSMStaticDB::Object::Object(id_t id, const tags_t& tags)
	: m_tags(tags), m_id(id), m_refcount(0)
{
}

OSMStaticDB::Object::~Object()
{
}

OSMStaticDB::Object::tagname_t OSMStaticDB::Object::find_tag(tagkey_t key) const
{
	if (key == tagkey_t::invalid)
		return tagname_t::invalid;
	tags_t::const_iterator i(std::lower_bound(m_tags.begin(), m_tags.end(), key, TagSorter()));
	if (i == m_tags.end() || std::get<tagkey_t>(*i) != key)
		return tagname_t::invalid;
	return std::get<tagname_t>(*i);
}

OSMStaticDB::Object::ptr_t OSMStaticDB::Object::create(type_t t)
{
	switch (t) {
	case type_t::point:
		return ptr_t(new ObjPoint());

	case type_t::line:
		return ptr_t(new ObjLine());

	case type_t::area:
		return ptr_t(new ObjArea());

	default:
		return ptr_t();
	}
}

std::ostream& OSMStaticDB::Object::print(std::ostream& os, const OSMStaticDB& db, printflags_t flags) const
{
	static const char * const pfx[4] = { "Point", "Line", "Area" };
	os << pfx[static_cast<unsigned int>(get_type())] << " ID " << get_id();
	{
		Rect bbox(get_bbox());
		if (!bbox.is_invalid())
			os << ' ' << bbox.get_southwest().get_lat_str2()
			   << ' ' << bbox.get_southwest().get_lon_str2()
			   << ' ' << bbox.get_northeast().get_lat_str2()
			   << ' ' << bbox.get_northeast().get_lon_str2();
	}
	os << std::endl;
	if ((flags & printflags_t::tags) != printflags_t::none) {
		bool subseq(false);
		for (const auto& t : get_tags()) {
			os << (subseq ? ", " : "  Tags: ");
			subseq = true;
			const char *tk(db.find_tagkey(std::get<OSMStaticDB::Object::tagkey_t>(t)));
			if (tk)
				os << '"' << tk << '"';
			else
				os << static_cast<std::underlying_type<tagkey_t>::type>(std::get<OSMStaticDB::Object::tagkey_t>(t));
			os << " = ";
			const char *tn(db.find_tagname(std::get<OSMStaticDB::Object::tagname_t>(t)));
			if (tn)
				os << '"' << tn << '"';
			else
				os << static_cast<std::underlying_type<tagname_t>::type>(std::get<OSMStaticDB::Object::tagname_t>(t));
		}
		if (subseq)
			os << std::endl;
	}
	return os;
}

constexpr OSMStaticDB::ObjPoint::type_t OSMStaticDB::ObjPoint::object_type;
const std::string OSMStaticDB::ObjPoint::object_name("point");

OSMStaticDB::ObjPoint::ObjPoint(id_t id, const tags_t& tags, const Point& loc)
	: Object(id, tags), m_location(loc)
{
}

std::ostream& OSMStaticDB::ObjPoint::print(std::ostream& os, const OSMStaticDB& db, printflags_t flags) const
{
	Object::print(os, db, flags);
	if ((flags & printflags_t::coords) != printflags_t::none)
		get_location().print(os << "  POINT: ") << std::endl;
	return os;
}

void OSMStaticDB::ObjPoint::load(HibernateReadStream& ar)
{
	hibernate(ar);
}

void OSMStaticDB::ObjPoint::load(HibernateReadBuffer& ar)
{
	hibernate(ar);
}

void OSMStaticDB::ObjPoint::save(HibernateWriteStream& ar) const
{
	(const_cast<ObjPoint *>(this))->hibernate(ar);
}

void OSMStaticDB::ObjPoint::loaddb(HibernateReadStream& ar)
{
	Object::hibernate_noid(ar);
}

void OSMStaticDB::ObjPoint::loaddb(HibernateReadBuffer& ar)
{
	Object::hibernate_noid(ar);
}

void OSMStaticDB::ObjPoint::savedb(HibernateWriteStream& ar) const
{
	(const_cast<ObjPoint *>(this))->Object::hibernate_noid(ar);
}

constexpr OSMStaticDB::ObjLine::type_t OSMStaticDB::ObjLine::object_type;
const std::string OSMStaticDB::ObjLine::object_name("line");

OSMStaticDB::ObjLine::ObjLine(id_t id, const tags_t& tags, const MultiLineString& ln)
	: Object(id, tags), m_line(ln)
{
}

std::ostream& OSMStaticDB::ObjLine::print(std::ostream& os, const OSMStaticDB& db, printflags_t flags) const
{
	Object::print(os, db, flags);
	if ((flags & printflags_t::coords) != printflags_t::none)
		get_line().print(os << "  LINE: ") << std::endl;
	if ((flags & printflags_t::skyvector) != printflags_t::none) {
		for (const auto& ls : get_line())
			os << "  " << ls.to_skyvector() << std::endl;
	}
	return os;
}

void OSMStaticDB::ObjLine::load(HibernateReadStream& ar)
{
	hibernate(ar);
}

void OSMStaticDB::ObjLine::load(HibernateReadBuffer& ar)
{
	hibernate(ar);
}

void OSMStaticDB::ObjLine::save(HibernateWriteStream& ar) const
{
	(const_cast<ObjLine *>(this))->hibernate(ar);
}

void OSMStaticDB::ObjLine::loaddb(HibernateReadStream& ar)
{
	hibernate_noid(ar);
}

void OSMStaticDB::ObjLine::loaddb(HibernateReadBuffer& ar)
{
	hibernate_noid(ar);
}

void OSMStaticDB::ObjLine::savedb(HibernateWriteStream& ar) const
{
	(const_cast<ObjLine *>(this))->hibernate_noid(ar);
}

constexpr OSMStaticDB::ObjArea::type_t OSMStaticDB::ObjArea::object_type;
const std::string OSMStaticDB::ObjArea::object_name("area");

OSMStaticDB::ObjArea::ObjArea(id_t id, const tags_t& tags, const MultiPolygonHole& a)
	: Object(id, tags), m_area(a)
{
}

std::ostream& OSMStaticDB::ObjArea::print(std::ostream& os, const OSMStaticDB& db, printflags_t flags) const
{
	Object::print(os, db, flags);
	if ((flags & printflags_t::coords) != printflags_t::none)
		get_area().print(os << "  AREA: ") << std::endl;
	if ((flags & printflags_t::skyvector) != printflags_t::none) {
		for (const auto& ph : get_area()) {
			os << "  OUTER: " << ph.get_exterior().to_skyvector() << std::endl;
			for (unsigned int i(0), n(ph.get_nrinterior()); i < n; ++i)
				os << "    INNER: " << ph[i].to_skyvector() << std::endl;
		}
	}
	return os;
}

void OSMStaticDB::ObjArea::load(HibernateReadStream& ar)
{
	hibernate(ar);
}

void OSMStaticDB::ObjArea::load(HibernateReadBuffer& ar)
{
	hibernate(ar);
}

void OSMStaticDB::ObjArea::save(HibernateWriteStream& ar) const
{
	(const_cast<ObjArea *>(this))->hibernate(ar);
}

void OSMStaticDB::ObjArea::loaddb(HibernateReadStream& ar)
{
	hibernate_noid(ar);
}

void OSMStaticDB::ObjArea::loaddb(HibernateReadBuffer& ar)
{
	hibernate_noid(ar);
}

void OSMStaticDB::ObjArea::savedb(HibernateWriteStream& ar) const
{
	(const_cast<ObjArea *>(this))->hibernate_noid(ar);
}

OSMStaticDB::Statistics::RTree::RTree(void)
	: m_bytes(0), m_objects(0), m_pages(0), m_depth(0)
{
}

OSMStaticDB::Statistics::RTree::RTree(const BinFileRtreeInternalNode *node)
	: RTree()
{
	depth_first_visit(node, 1);
}

void OSMStaticDB::Statistics::RTree::depth_first_visit(const BinFileRtreeInternalNode *node, unsigned int depth)
{
	if (!node)
		return;
	m_depth = std::max(m_depth, depth);
	++m_pages;
	m_bytes += node->get_size();
	uint16_t n(node->get_nodes());
	++depth;
	if (node->is_pointstoleaf()) {
		for (uint16_t i(0); i < n; ++i)
			depth_first_visit(node->get_leafnode(i), depth);
	} else {
		for (uint16_t i(0); i < n; ++i)
			depth_first_visit(node->get_internalnode(i), depth);
	}
}

void OSMStaticDB::Statistics::RTree::depth_first_visit(const BinFileRtreeLeafNode *node, unsigned int depth)
{
	if (!node)
		return;
	m_depth = std::max(m_depth, depth);
	++m_pages;
	m_bytes += node->get_size();
	m_objects += node->get_nodes();
}

bool OSMStaticDB::Statistics::RTree::is_valid(void) const
{
	return !(!get_objects() && !get_pages() && !get_bytes() && !get_depth());
}

std::string OSMStaticDB::Statistics::RTree::to_str(void) const
{
	std::ostringstream oss;
	oss << get_objects() << " objects, " << get_pages() << " pages " << get_bytes() << " bytes " << get_depth() << " depth";
	return oss.str();
}

OSMStaticDB::Statistics::Statistics(void)
	: m_tagkeys(0), m_tagnames(0)
{
	for (layer_t l(layer_t::first); l <= layer_t::last; l = static_cast<layer_t>(static_cast<unsigned int>(l) + 1))
		m_objects[static_cast<unsigned int>(l)] = 0;
}

OSMStaticDB::Statistics::Statistics(const BinFileHeader& hdr, bool dortree)
{
	m_tagkeys = hdr.get_tagkeyentries();
	m_tagnames = hdr.get_tagnameentries();
	for (layer_t l(layer_t::first); l <= layer_t::last; l = static_cast<layer_t>(static_cast<unsigned int>(l) + 1)) {
		m_objects[static_cast<unsigned int>(l)] = hdr.get_objdirentries(l);
		if (dortree)
			m_rtrees[static_cast<unsigned int>(l)] = RTree(hdr.get_rtree_root(l));
	}
}

unsigned int OSMStaticDB::Statistics::get_objects(layer_t layer) const
{
	if (layer > layer_t::last)
		return 0;
	return m_objects[static_cast<unsigned int>(layer)];
}

const OSMStaticDB::Statistics::RTree& OSMStaticDB::Statistics::get_rtree(layer_t layer) const
{
	static const RTree empty;
	if (layer > layer_t::last)
		return empty;
	return m_rtrees[static_cast<unsigned int>(layer)];
}

std::string OSMStaticDB::Statistics::to_str(void) const
{
	std::ostringstream oss;
	for (layer_t l(layer_t::first); l <= layer_t::last; l = static_cast<layer_t>(static_cast<unsigned int>(l) + 1))
		oss << get_objects(l) << ' ' << l << "s, ";
	oss << "tags " << get_tagkeys() << " keys " << get_tagnames() << " names";
	for (layer_t l(layer_t::first); l <= layer_t::last; l = static_cast<layer_t>(static_cast<unsigned int>(l) + 1)) {
		const RTree& rt(get_rtree(l));
		if (!rt.is_valid())
			continue;
		oss << ", " << l << " rtree " << rt.to_str();
	}
	return oss.str();
}

const std::string OSMStaticDB::layer_data_type<OSMStaticDB::layer_t::boundarylinesland>::name("boundarylinesland");
const std::string OSMStaticDB::layer_data_type<OSMStaticDB::layer_t::icesheetpolygons>::name("icesheetpolygons");
const std::string OSMStaticDB::layer_data_type<OSMStaticDB::layer_t::icesheetoutlines>::name("icesheetoutlines");
const std::string OSMStaticDB::layer_data_type<OSMStaticDB::layer_t::waterpolygons>::name("waterpolygons");
const std::string OSMStaticDB::layer_data_type<OSMStaticDB::layer_t::simplifiedwaterpolygons>::name("simplifiedwaterpolygons");

constexpr unsigned int OSMStaticDB::rtreemaxelperpage;
constexpr unsigned int OSMStaticDB::rtreedefaultmaxelperpage;
constexpr uint64_t OSMStaticDB::aligninc;
constexpr uint64_t OSMStaticDB::alignmask;

OSMStaticDB::OSMStaticDB(const std::string& path)
	: m_path(path)
{
}

OSMStaticDB::~OSMStaticDB()
{
	close();
}

void OSMStaticDB::set_path(const std::string& path)
{
	if (path == m_path)
		return;
	close();
	m_path = path;
}

void OSMStaticDB::open(void)
{
	static constexpr bool errmsg = true;
	if (m_bin.is_open())
		return;
	try {
		m_bin.open(m_path.c_str(), false, "osm database");
	} catch (const std::exception& e) {
		if (errmsg)
			std::cerr << "Cannot open file " << m_path << ": " << e.what() << std::endl;
		return;
	}
	const BinFileHeader& hdr(*m_bin.begin_as<BinFileHeader>());
	if (!hdr.check_signature()) {
		if (errmsg)
			std::cerr << "Invalid signature on file " << m_path << std::endl;
		close();
		return;
	}
	if (errmsg) {
		std::cerr << "Using file " << m_path << ": ";
		for (layer_t l(layer_t::first); l <= layer_t::last; l = static_cast<layer_t>(static_cast<unsigned int>(l) + 1)) {
			std::cerr << hdr.get_objdirentries(l) << ' ' << l << " objects";
			if (hdr.get_rtreeoffs(l))
				std::cerr << " (rtree)";
			std::cerr << ", ";
		}
		std::cerr << hdr.get_tagkeyentries() << " tags" << std::endl;
	}
}

void OSMStaticDB::close(void)
{
	m_bin.close();
}

template<OSMStaticDB::Object::type_t typ>
typename OSMStaticDB::obj_types<typ>::ObjType::const_ptr_t OSMStaticDB::load_object(const typename obj_types<typ>::IndexType *pe) const {
	typedef typename obj_types<typ>::ObjType ObjType;
	if (!pe)
		return typename ObjType::const_ptr_t();
	unsigned int sz(pe->get_datasize());
	if (!sz)
		return typename ObjType::const_ptr_t();
	const uint8_t *data(pe->get_dataptr());
	HibernateReadBuffer ar(data, data + sz);
	typename ObjType::ptr_t p(new ObjType());
	p->hibernate_noid(ar);
	p->set_id(pe->get_id());
	return p;
}

template<>
typename OSMStaticDB::obj_types<OSMStaticDB::Object::type_t::point>::ObjType::const_ptr_t OSMStaticDB::load_object<OSMStaticDB::Object::type_t::point>(const typename obj_types<Object::type_t::point>::IndexType *pe) const {
	typedef typename obj_types<Object::type_t::point>::ObjType ObjType;
	if (!pe)
		return typename ObjType::const_ptr_t();
	unsigned int sz(pe->get_datasize());
	if (!sz)
		return typename ObjType::const_ptr_t();
	const uint8_t *data(pe->get_dataptr());
	HibernateReadBuffer ar(data, data + sz);
	typename ObjType::ptr_t p(new ObjType());
	p->Object::hibernate_noid(ar);
	p->set_id(pe->get_id());
	p->set_location(pe->get_location());
	return p;
}

template<OSMStaticDB::layer_t layer>
void OSMStaticDB::find_object(objects_t& obj) const
{
	typedef typename layer_types<layer>::ObjType ObjType;
	typedef typename layer_types<layer>::IndexType IndexType;
	typedef typename layer_types<layer>::IndexRangeType IndexRangeType;
	if (!m_bin.is_open())
		return;
	const BinFileHeader& hdr(*m_bin.begin_as<BinFileHeader>());
	IndexRangeType index(hdr.get_objdir_range<layer>());
	for (const IndexType *oi(index.first); oi != index.second; ++oi) {
		typename ObjType::const_ptr_t p(load_object<layer_types<layer>::objtype>(oi));
		if (!p)
			continue;
		obj.push_back(p);
	}
}

template<OSMStaticDB::layer_t layer>
void OSMStaticDB::find_object(objects_t& obj, Object::id_t id) const
{
	typedef typename layer_types<layer>::ObjType ObjType;
	typedef typename layer_types<layer>::IndexType IndexType;
	typedef typename layer_types<layer>::IndexRangeType IndexRangeType;
	if (!m_bin.is_open())
		return;
	const BinFileHeader& hdr(*m_bin.begin_as<BinFileHeader>());
	IndexRangeType index(hdr.get_objdir_range<layer>());
	for (const IndexType *oi(std::lower_bound(index.first, index.second, id, BinFileIDSorter())); oi != index.second && oi->get_id() == id; ++oi) {
		typename ObjType::const_ptr_t p(load_object<layer_types<layer>::objtype>(oi));
		if (!p)
			continue;
		obj.push_back(p);
	}
}

OSMStaticDB::objects_t OSMStaticDB::find(layer_t layer) const
{
	objects_t r;
	switch (layer) {
	case layer_t::boundarylinesland:
		find_object<layer_t::boundarylinesland>(r);
		break;

	case layer_t::icesheetpolygons:
		find_object<layer_t::icesheetpolygons>(r);
		break;

	case layer_t::icesheetoutlines:
		find_object<layer_t::icesheetoutlines>(r);
		break;

	case layer_t::waterpolygons:
		find_object<layer_t::waterpolygons>(r);
		break;

	case layer_t::simplifiedwaterpolygons:
		find_object<layer_t::simplifiedwaterpolygons>(r);
		break;

	default:
		break;
	}
	return r;
}

OSMStaticDB::objects_t OSMStaticDB::find(Object::id_t id, layer_t layer) const
{
	objects_t r;
	switch (layer) {
	case layer_t::boundarylinesland:
		find_object<layer_t::boundarylinesland>(r, id);
		break;

	case layer_t::icesheetpolygons:
		find_object<layer_t::icesheetpolygons>(r, id);
		break;

	case layer_t::icesheetoutlines:
		find_object<layer_t::icesheetoutlines>(r, id);
		break;

	case layer_t::waterpolygons:
		find_object<layer_t::waterpolygons>(r, id);
		break;

	case layer_t::simplifiedwaterpolygons:
		find_object<layer_t::simplifiedwaterpolygons>(r, id);
		break;

	default:
		break;
	}
	return r;
}

void OSMStaticDB::rtree_visitor(rtree_ids_t& ids, const Rect& bbox, const BinFileRtreeInternalNode *node)
{
	if (!node)
		return;
	uint16_t n(node->get_nodes());
	if (node->is_pointstoleaf()) {
		for (uint16_t i(0); i < n; ++i)
			if (bbox.is_intersect(node->get_bbox(i)))
				rtree_visitor(ids, node->get_leafnode(i));
	} else {
		for (uint16_t i(0); i < n; ++i)
			if (bbox.is_intersect(node->get_bbox(i)))
				rtree_visitor(ids, bbox, node->get_internalnode(i));
	}
}

void OSMStaticDB::rtree_visitor(rtree_ids_t& ids, const BinFileRtreeLeafNode *node)
{
	if (!node)
		return;
	uint16_t n(node->get_nodes());
	for (uint16_t i(0); i < n; ++i)
		ids.push_back(node->get_index(i));
}

template<OSMStaticDB::layer_t layer>
void OSMStaticDB::find_objects(objects_t& obj, const Rect& bbox) const
{
	typedef typename layer_types<layer>::ObjType ObjType;
	typedef typename layer_types<layer>::IndexType IndexType;
	typedef typename layer_types<layer>::IndexRangeType IndexRangeType;
	if (!m_bin.is_open())
		return;
	const BinFileHeader& hdr(*m_bin.begin_as<BinFileHeader>());
	IndexRangeType index(hdr.get_objdir_range<layer>());
	const BinFileRtreeInternalNode *rtree(hdr.get_rtree_root(layer));
	if (!rtree) {
		// if the database has no spatial index, go through all objects
		for (const IndexType *oi(index.first); oi != index.second; ++oi) {
			if (!(oi->is_match(bbox)))
				continue;
			typename ObjType::const_ptr_t p(load_object<layer_types<layer>::objtype>(oi));
			if (!p)
				continue;
			obj.push_back(p);
		}
		return;
	}
	rtree_ids_t ids;
	rtree_visitor(ids, bbox, rtree);
	if (ids.empty())
		return;
	std::sort(ids.begin(), ids.end());
	BinFileRtreeLeafNode::index_t lastid(*ids.begin() - 1);
	for (const auto id : ids) {
		if (id == lastid)
			continue;
		lastid = id;
		const IndexType *oi(index.first + id);
		if (oi >= index.second || !(oi->is_match(bbox)))
			continue;
		typename ObjType::const_ptr_t p(load_object<layer_types<layer>::objtype>(oi));
		if (!p)
			continue;
		obj.push_back(p);
	}
}

OSMStaticDB::objects_t OSMStaticDB::find(const Rect& bbox, layer_t layer) const
{
	objects_t r;
	switch (layer) {
	case layer_t::boundarylinesland:
		find_objects<layer_t::boundarylinesland>(r, bbox);
		break;

	case layer_t::icesheetpolygons:
		find_objects<layer_t::icesheetpolygons>(r, bbox);
		break;

	case layer_t::icesheetoutlines:
		find_objects<layer_t::icesheetoutlines>(r, bbox);
		break;

	case layer_t::waterpolygons:
		find_objects<layer_t::waterpolygons>(r, bbox);
		break;

	case layer_t::simplifiedwaterpolygons:
		find_objects<layer_t::simplifiedwaterpolygons>(r, bbox);
		break;

	default:
		break;
	}
	return r;
}

OSMStaticDB::Object::const_ptr_t OSMStaticDB::get(uint32_t index, layer_t layer) const
{
	if (!m_bin.is_open())
		return Object::const_ptr_t();
	const BinFileHeader& hdr(*m_bin.begin_as<BinFileHeader>());
	switch (layer) {
	case layer_t::boundarylinesland:
	{
		layer_types<layer_t::boundarylinesland>::IndexRangeType idx(hdr.get_objdir_range<layer_t::boundarylinesland>());
		const layer_types<layer_t::boundarylinesland>::IndexType *oi(idx.first + index);
		if (oi >= idx.second)
			return Object::const_ptr_t();
		return load_object<layer_types<layer_t::boundarylinesland>::objtype>(oi);
	}

	case layer_t::icesheetpolygons:
	{
		layer_types<layer_t::icesheetpolygons>::IndexRangeType idx(hdr.get_objdir_range<layer_t::icesheetpolygons>());
		const layer_types<layer_t::icesheetpolygons>::IndexType *oi(idx.first + index);
		if (oi >= idx.second)
			return Object::const_ptr_t();
		return load_object<layer_types<layer_t::icesheetpolygons>::objtype>(oi);
	}

	case layer_t::icesheetoutlines:
	{
		layer_types<layer_t::icesheetoutlines>::IndexRangeType idx(hdr.get_objdir_range<layer_t::icesheetoutlines>());
		const layer_types<layer_t::icesheetoutlines>::IndexType *oi(idx.first + index);
		if (oi >= idx.second)
			return Object::const_ptr_t();
		return load_object<layer_types<layer_t::icesheetoutlines>::objtype>(oi);
	}

	case layer_t::waterpolygons:
	{
		layer_types<layer_t::waterpolygons>::IndexRangeType idx(hdr.get_objdir_range<layer_t::waterpolygons>());
		const layer_types<layer_t::waterpolygons>::IndexType *oi(idx.first + index);
		if (oi >= idx.second)
			return Object::const_ptr_t();
		return load_object<layer_types<layer_t::waterpolygons>::objtype>(oi);
	}

	case layer_t::simplifiedwaterpolygons:
	{
		layer_types<layer_t::simplifiedwaterpolygons>::IndexRangeType idx(hdr.get_objdir_range<layer_t::simplifiedwaterpolygons>());
		const layer_types<layer_t::simplifiedwaterpolygons>::IndexType *oi(idx.first + index);
		if (oi >= idx.second)
			return Object::const_ptr_t();
		return load_object<layer_types<layer_t::simplifiedwaterpolygons>::objtype>(oi);
	}

	default:
		return Object::const_ptr_t();
	}
}

const char *OSMStaticDB::find_tagkey(Object::tagkey_t key) const
{
	if (!m_bin.is_open())
		return nullptr;
	const BinFileHeader& hdr(*m_bin.begin_as<BinFileHeader>());
	const BinFileTagKeyEntry *oi(hdr.get_tagkey_begin());
	if (!oi)
		return nullptr;
	const BinFileTagKeyEntry *oe(hdr.get_tagkey_end());
	const BinFileTagKeyEntry *ox(oi + static_cast<std::underlying_type<Object::tagkey_t>::type>(key));
	if (ox >= oe)
		return nullptr;
	return ox->get_strptr();
}

OSMStaticDB::Object::tagkey_t OSMStaticDB::find_tagkey(const std::string& tag) const
{
	if (!m_bin.is_open())
		return Object::tagkey_t::invalid;
	const BinFileHeader& hdr(*m_bin.begin_as<BinFileHeader>());
	const BinFileTagKeyEntry *oi(hdr.get_tagkey_begin());
	const BinFileTagKeyEntry *oe(hdr.get_tagkey_end());
	const BinFileTagKeyEntry *ox(std::lower_bound(oi, oe, tag.c_str(), BinFileTagKeySorter()));
	if (ox == oe || BinFileTagKeySorter()(tag.c_str(), *ox))
		return Object::tagkey_t::invalid;
	return static_cast<Object::tagkey_t>(ox - oi);
}

bool OSMStaticDB::set_tagkey(std::string& v, Object::tagkey_t key) const
{
	const char *cp(find_tagkey(key));
	if (cp) {
		v = cp;
		return true;
	}
	v.clear();
	return false;
}

const char *OSMStaticDB::find_tagname(Object::tagname_t key) const
{
	if (!m_bin.is_open())
		return nullptr;
	const BinFileHeader& hdr(*m_bin.begin_as<BinFileHeader>());
	const BinFileTagNameEntry *oi(hdr.get_tagname_begin());
	if (!oi)
		return nullptr;
	const BinFileTagNameEntry *oe(hdr.get_tagname_end());
	const BinFileTagNameEntry *ox(oi + static_cast<std::underlying_type<Object::tagname_t>::type>(key));
	if (ox >= oe)
		return nullptr;
	return ox->get_strptr();
}

OSMStaticDB::Object::tagname_t OSMStaticDB::find_tagname(const std::string& tag) const
{
	if (!m_bin.is_open())
		return Object::tagname_t::invalid;
	const BinFileHeader& hdr(*m_bin.begin_as<BinFileHeader>());
	const BinFileTagNameEntry *oi(hdr.get_tagname_begin());
	const BinFileTagNameEntry *oe(hdr.get_tagname_end());
	const BinFileTagNameEntry *ox(std::lower_bound(oi, oe, tag.c_str(), BinFileTagNameSorter()));
	if (ox == oe || BinFileTagNameSorter()(tag.c_str(), *ox))
		return Object::tagname_t::invalid;
	return static_cast<Object::tagname_t>(ox - oi);
}

bool OSMStaticDB::set_tagname(std::string& v, Object::tagname_t key) const
{
	const char *cp(find_tagname(key));
	if (cp) {
		v = cp;
		return true;
	}
	v.clear();
	return false;
}

OSMStaticDB::TagNameSet OSMStaticDB::create_tagnameset(const std::vector<std::string>& x) const
{
	TagNameSet r;
	for (const std::string& s : x) {
		Object::tagname_t t(find_tagname(s));
		if (t != Object::tagname_t::invalid)
			r.insert(t);
	}
	return r;
}

OSMStaticDB::TagNameSet OSMStaticDB::create_tagnameset(const char * const *x) const
{
	if (!x)
		return TagNameSet();
	TagNameSet r;
	for (; *x; ++x) {
		Object::tagname_t t(find_tagname(*x));
		if (t != Object::tagname_t::invalid)
			r.insert(t);
	}
	return r;
}

OSMStaticDB::TagNameSet OSMStaticDB::create_tagnameset(std::initializer_list<const char *> x) const
{
	TagNameSet r;
	for (const auto v : x) {
		Object::tagname_t t(find_tagname(v));
		if (t != Object::tagname_t::invalid)
			r.insert(t);
	}
	return r;
}

OSMStaticDB::Statistics OSMStaticDB::get_statistics(bool dortree) const
{
	if (!m_bin.is_open())
		return Statistics();
	const BinFileHeader& hdr(*m_bin.begin_as<BinFileHeader>());
	return Statistics(hdr, dortree);
}

OSMStaticDB::Object::type_t OSMStaticDB::get_objtype(layer_t layer)
{
	switch (layer) {
	case layer_t::boundarylinesland:
		return layer_types<layer_t::boundarylinesland>::objtype;

	case layer_t::icesheetpolygons:
		return layer_types<layer_t::icesheetpolygons>::objtype;

	case layer_t::icesheetoutlines:
		return layer_types<layer_t::icesheetoutlines>::objtype;

	case layer_t::waterpolygons:
		return layer_types<layer_t::waterpolygons>::objtype;

	case layer_t::simplifiedwaterpolygons:
		return layer_types<layer_t::simplifiedwaterpolygons>::objtype;

	default:
		return Object::type_t::point;
	}
}

template class OSMStaticDB::BinFileEntry<4>;
template class OSMStaticDB::BinFileEntry<16>;
template class OSMStaticDB::BinFileEntry<20>;
template class OSMStaticDB::BinFileEntry<24>;
template class OSMStaticDB::BinFileEntry<32>;
template class OSMStaticDB::BinFileEntry<256>;
template class OSMStaticDB::BinFileEntry<2+OSMStaticDB::rtreemaxelperpage*4>;
template class OSMStaticDB::BinFileEntry<2+OSMStaticDB::rtreemaxelperpage*24>;
template typename OSMStaticDB::layer_types<OSMStaticDB::layer_t::boundarylinesland>::IndexRangeType OSMStaticDB::BinFileHeader::get_objdir_range<OSMStaticDB::layer_t::boundarylinesland>(void) const;
template typename OSMStaticDB::layer_types<OSMStaticDB::layer_t::icesheetpolygons>::IndexRangeType OSMStaticDB::BinFileHeader::get_objdir_range<OSMStaticDB::layer_t::icesheetpolygons>(void) const;
template typename OSMStaticDB::layer_types<OSMStaticDB::layer_t::icesheetoutlines>::IndexRangeType OSMStaticDB::BinFileHeader::get_objdir_range<OSMStaticDB::layer_t::icesheetoutlines>(void) const;
template typename OSMStaticDB::layer_types<OSMStaticDB::layer_t::waterpolygons>::IndexRangeType OSMStaticDB::BinFileHeader::get_objdir_range<OSMStaticDB::layer_t::waterpolygons>(void) const;
template typename OSMStaticDB::layer_types<OSMStaticDB::layer_t::simplifiedwaterpolygons>::IndexRangeType OSMStaticDB::BinFileHeader::get_objdir_range<OSMStaticDB::layer_t::simplifiedwaterpolygons>(void) const;
