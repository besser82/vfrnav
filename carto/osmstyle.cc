#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#undef BOOST_SPIRIT_X3_DEBUG
#include <boost/config/warning_disable.hpp>
#include <boost/spirit/home/x3.hpp>
#include <boost/fusion/include/vector.hpp>
#include <boost/fusion/include/make_deque.hpp>
#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/fusion/include/io.hpp>
#include <boost/fusion/adapted.hpp>
#include <boost/spirit/include/support_line_pos_iterator.hpp>
#include <boost/spirit/include/support_istream_iterator.hpp>

#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>

#include "osmstyle.h"


OsmStyleEntry::OsmStyleEntry(entity_t osmtyp, const std::string& tag, datatype_t dt, flags_t flg)
	: m_tag(tag), m_osmtype(osmtyp), m_datatype(dt), m_flags(flg)
{
}

int OsmStyleEntry::tag_compare(const std::string& tag) const
{
	if (!get_tag().empty() && get_tag().back() == '*')
		return get_tag().compare(0, get_tag().size() - 1, tag, 0, std::min(get_tag().size() - 1, tag.size()));
	return get_tag().compare(tag);
}

bool OsmStyleEntry::tag_match(const std::string& tag) const
{
	return !tag_compare(tag);
}

const OsmStyleEntry *OsmStyle::find(const std::string& tag) const
{
	const_iterator i(lower_bound(OsmStyleEntry(OsmStyleEntry::entity_t::none, tag)));
	if (i != end()) {
		if (i->tag_match(tag))
			return &*i;
	}
	while (i != begin()) {
		--i;
		int c(i->tag_compare(tag));
		if (!c)
			return &*i;
		if (c < 0)
			break;
	}
	if (!false) {
		for (const_iterator i(begin()), e(end()); i != e; ++i)
			if (i->tag_match(tag)) {
				std::cerr << "OsmStyle::find: missed " << i->get_tag() << " for " << tag << std::endl;
				return &*i;
			}
	}
	return 0;
}

bool OsmStyle::has_relation(void) const
{
	for (const auto& ent : *this)
		if ((ent.get_osmtype() & OsmStyleEntry::entity_t::relation) != OsmStyleEntry::entity_t::none)
			return true;
	return false;
}

void OsmStyle::fix_relation(void)
{
	if (has_relation())
		return;
	for (auto& ent : *this)
		if ((ent.get_osmtype() & OsmStyleEntry::entity_t::way) != OsmStyleEntry::entity_t::none)
			const_cast<OsmStyleEntry&>(ent).set_osmtype(ent.get_osmtype() | OsmStyleEntry::entity_t::relation);
}

namespace {

	namespace x3 = boost::spirit::x3;
	namespace ascii = boost::spirit::x3::ascii;
	namespace fusion = boost::fusion;

	template<typename Iterator>
	void parse_error(const Iterator& begin, const Iterator& iter, const Iterator& end, const std::string& info = "") {
		std::ostringstream oss;
		oss << "parse error at";
		{
			std::size_t ln(boost::spirit::get_line(iter));
			if (ln != static_cast<std::size_t>(-1))
				oss << " line " << ln;
		}
		std::size_t col(boost::spirit::get_column(begin, iter, 8));
		oss << " column " << col << std::endl
		    << '\'' << boost::spirit::get_current_line(begin, iter, end) << '\'' << std::endl
		    <<std::setw(col) << ' ' << "^- here";
		if (!info.empty())
			oss << std::endl << info;
		throw std::runtime_error(oss.str());
	}

	struct osmstyle_parser {

		osmstyle_parser(OsmStyle& style) : m_style(style) {
		}

		OsmStyle& m_style;
	};

	namespace skipper {
		using x3::rule;
		using x3::lit;
		using x3::eol;
		using x3::eoi;
		using ascii::char_;
		using ascii::space;

		const rule<class comment> comment = "comment";
	        const rule<class skip> skip = "skip";

		const auto comment_def = lit("#") >> *(char_ - eol);
		const auto skip_def = (space - eol) | comment;

		BOOST_SPIRIT_DEFINE(comment, skip);
	};


	namespace parser {
		using x3::rule;
		using x3::get;
		using x3::no_case;
		using x3::no_skip;
		using x3::lit;
		using x3::omit;
		using x3::lexeme;
		using x3::repeat;
		using x3::eps;
		using x3::eol;
		using x3::eoi;
		using x3::attr;
		using x3::with;
		using x3::_val;
		using x3::_attr;
		using x3::_where;
		using x3::_pass;
		using x3::ulong_;
		using x3::long_;
		using x3::double_;
		using ascii::char_;
		using ascii::alnum;
		using ascii::alpha;
		using ascii::digit;
		using ascii::xdigit;
		using ascii::space;

		const struct osmobjtype_ : x3::symbols<OsmStyleEntry::entity_t> {
			osmobjtype_() {
				add
					("node",       OsmStyleEntry::entity_t::node)
					("way",        OsmStyleEntry::entity_t::way)
					("relation",   OsmStyleEntry::entity_t::relation);
			}
		} osmobjtype;

		const struct flagstype_ : x3::symbols<OsmStyleEntry::flags_t> {
			flagstype_() {
				add
					("polygon",    OsmStyleEntry::flags_t::polygon)
					("linear",     OsmStyleEntry::flags_t::linear)
					("nocolumn",   OsmStyleEntry::flags_t::nocolumn)
					("phstore",    OsmStyleEntry::flags_t::polygon | OsmStyleEntry::flags_t::nocolumn)
					("delete",     OsmStyleEntry::flags_t::delete_)
					("nocache",    OsmStyleEntry::flags_t::none);
			}
		} flagstype;

		const struct datatype_ : x3::symbols<OsmStyleEntry::datatype_t> {
			datatype_() {
				add
					("text",       OsmStyleEntry::datatype_t::text)
					("real",       OsmStyleEntry::datatype_t::real)
					("int4",       OsmStyleEntry::datatype_t::int4);
			}
		} datatype;

		//-----------------------------------------------------------------------------
		// Parsers
		//-----------------------------------------------------------------------------

		const rule<struct colosmobjtyp, OsmStyleEntry::entity_t> colosmobjtyp = "colosmobjtyp";
		const rule<struct coltag, std::string> coltag = "coltag";
		const rule<struct coldatatype, OsmStyleEntry::datatype_t> coldatatype = "coldatatype";
		const rule<struct colflags, OsmStyleEntry::flags_t> colflags = "colflags";

		const auto colosmobjtyp_def = lexeme[osmobjtype[([](auto& ctx) { _val(ctx) = _attr(ctx); })] >>
						     *(lit(',') >> osmobjtype[([](auto& ctx) { _val(ctx) |= _attr(ctx); })])];

		const auto coltag_def = lexeme[char_("0-9a-zA-Z_") >> *char_("0-9a-zA-Z_:*")];

		const auto coldatatype_def = lexeme[datatype];

		const auto colflags_def = lexeme[flagstype[([](auto& ctx) { _val(ctx) = _attr(ctx); })] >>
						 *(lit(',') >> flagstype[([](auto& ctx) { _val(ctx) |= _attr(ctx); })])];

		BOOST_SPIRIT_DEFINE(colosmobjtyp,coltag,coldatatype,colflags);

		//-----------------------------------------------------------------------------
		// Line
		//-----------------------------------------------------------------------------

		struct parser_tag {};

		struct addelement {
			template<typename Context>
			void operator()(Context& ctx) {
				osmstyle_parser& g(get<parser_tag>(ctx).get());
				g.m_style.insert(_attr(ctx));
				if (false)
					std::cerr << "adding style " << _attr(ctx).get_tag() << " flags "
						  << static_cast<unsigned int>(_attr(ctx).get_flags()) << std::endl;
			}
		};

		struct endofinput {
			template<typename Context>
			void operator()(Context& ctx) {
				osmstyle_parser& g(get<parser_tag>(ctx).get());
				g.m_style.fix_relation();
			}
		};

		const rule<struct entry, OsmStyleEntry> entry = "entry";
		const rule<struct line> line = "line";
		const rule<struct file> file = "file";

		const auto entry_def = (colosmobjtyp[([](auto& ctx) { _val(ctx).set_osmtype(_attr(ctx)); })] >
					coltag[([](auto& ctx) { _val(ctx).set_tag(_attr(ctx)); })] >
					coldatatype[([](auto& ctx) { _val(ctx).set_datatype(_attr(ctx)); })] >
					colflags[([](auto& ctx) { _val(ctx).set_flags(_attr(ctx)); })]);

		const auto line_def = -entry[addelement{}];

		const auto file_def = -(line > *(eol > line)) >> -eol >> eoi[endofinput{}];

		BOOST_SPIRIT_DEFINE(entry,line,file);


	};
};

void OsmStyle::parse(std::istream& is)
{
	namespace x3 = boost::spirit::x3;
	using Iterator = boost::spirit::istream_iterator;
	Iterator f(is >> std::noskipws), l;
	using PosIterator = boost::spirit::line_pos_iterator<Iterator>;
	PosIterator pf(f), pi(pf), pl;
	osmstyle_parser g(*this);
	try {
		if (!x3::phrase_parse(pi, pl, x3::with<parser::parser_tag>(std::ref(g))[parser::file], skipper::skip))
			parse_error(pf, pi, pl);
	} catch (const x3::expectation_failure<PosIterator>& x) {
		parse_error(pf, x.where(), pl, x.which());
	}
}

void OsmStyle::parse(const std::string& fn)
{
	std::ifstream is(fn.c_str());
	if (!is.is_open())
		throw std::runtime_error("Cannot open file " + fn);
	parse(is);
}
