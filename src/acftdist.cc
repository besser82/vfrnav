/*****************************************************************************/

/*
 *      acftdist.cc  --  Aircraft Model Takeoff/Landing Distances.
 *
 *      Copyright (C) 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020
 *        Thomas Sailer (t.sailer@alumni.ethz.ch)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

/*****************************************************************************/

#include "aircraft.h"

#ifdef HAVE_EIGEN3
#include <Eigen/Dense>
#include <Eigen/Cholesky>
#include <unsupported/Eigen/Polynomials>
#endif

#ifdef HAVE_EIGEN3

namespace {

#ifdef __WIN32__
__attribute__((force_align_arg_pointer))
#endif

Eigen::VectorXd least_squares_solution(const Eigen::MatrixXd& A, const Eigen::VectorXd& b)
{
	if (false) {
		// traditional
		return (A.transpose() * A).inverse() * A.transpose() * b;
	} else if (true) {
		// cholesky
		return (A.transpose() * A).ldlt().solve(A.transpose() * b);
	} else if (false) {
		// householder QR
		return A.colPivHouseholderQr().solve(b);
	} else if (false) {
		// jacobi SVD
		return A.jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(b);
	} else {
		typedef Eigen::JacobiSVD<Eigen::MatrixXd, Eigen::FullPivHouseholderQRPreconditioner> jacobi_t;
		jacobi_t jacobi(A, Eigen::ComputeFullU | Eigen::ComputeFullV);
		return jacobi.solve(b);
	}
}

};

#endif

Aircraft::Distances::Distance::Point::Point(double da, double pa, double temp, double gnddist, double obstdist)
	: m_da(da), m_pa(pa), m_temp(temp), m_gnddist(gnddist), m_obstdist(obstdist)
{
}

void Aircraft::Distances::Distance::Point::load_xml(const xmlpp::Element *el, double altfactor, double tempfactor, double tempoffset, double distfactor)
{
	if (!el)
		return;
	m_da = m_pa = m_temp = m_gnddist = m_obstdist = std::numeric_limits<double>::quiet_NaN();
	xmlpp::Attribute *attr;
	if ((attr = el->get_attribute("da")) && attr->get_value() != "")
		m_da = Glib::Ascii::strtod(attr->get_value()) * altfactor;
	if ((attr = el->get_attribute("pa")) && attr->get_value() != "")
		m_pa = Glib::Ascii::strtod(attr->get_value()) * altfactor;
	if ((attr = el->get_attribute("temp")) && attr->get_value() != "")
		m_temp = Glib::Ascii::strtod(attr->get_value()) * tempfactor + tempoffset;
	if ((attr = el->get_attribute("gnddist")) && attr->get_value() != "")
		m_gnddist = Glib::Ascii::strtod(attr->get_value()) * distfactor;
	if ((attr = el->get_attribute("obstdist")) && attr->get_value() != "")
		m_obstdist = Glib::Ascii::strtod(attr->get_value()) * distfactor;

}

void Aircraft::Distances::Distance::Point::save_xml(xmlpp::Element *el, double altfactor, double tempfactor, double tempoffset, double distfactor) const
{
	if (!el)
		return;
	if (std::isnan(m_da) || altfactor == 0) {
		el->remove_attribute("da");
	} else {
		std::ostringstream oss;
		oss << (m_da / altfactor);
		el->set_attribute("da", oss.str());
	}
	if (std::isnan(m_pa) || altfactor == 0) {
		el->remove_attribute("pa");
	} else {
		std::ostringstream oss;
		oss << (m_pa / altfactor);
		el->set_attribute("pa", oss.str());
	}
	if (std::isnan(m_temp) || tempfactor == 0) {
		el->remove_attribute("temp");
	} else {
		std::ostringstream oss;
		oss << ((m_temp - tempoffset) / tempfactor);
		el->set_attribute("temp", oss.str());
	}
	if (std::isnan(m_gnddist) || distfactor == 0) {
		el->remove_attribute("gnddist");
	} else {
		std::ostringstream oss;
		oss << (m_gnddist / distfactor);
		el->set_attribute("gnddist", oss.str());
	}
	if (std::isnan(m_obstdist) || distfactor == 0) {
		el->remove_attribute("obstdist");
	} else {
		std::ostringstream oss;
		oss << (m_obstdist / distfactor);
		el->set_attribute("obstdist", oss.str());
	}
}

double Aircraft::Distances::Distance::Point::get_densityalt(void) const
{
	if (!std::isnan(m_da))
		return m_da;
	if (std::isnan(m_pa))
		return std::numeric_limits<double>::quiet_NaN();
	float temp, press;
	IcaoAtmosphere<float>::std_altitude_to_pressure(&press, &temp, m_pa);
	if (!std::isnan(m_temp))
		temp = m_temp;
	return IcaoAtmosphere<float>::std_density_to_altitude(IcaoAtmosphere<float>::pressure_to_density(press, temp));
}

const std::string& to_str(Aircraft::Distances::Distance::surface_t sfc)
{
	switch (sfc) {
	case Aircraft::Distances::Distance::surface_invalid:
	{
		static const std::string r("invalid");
		return r;
	}

	case Aircraft::Distances::Distance::surface_hard:
	{
		static const std::string r("hard");
		return r;
	}

	case Aircraft::Distances::Distance::surface_soft:
	{
		static const std::string r("soft");
		return r;
	}

	case Aircraft::Distances::Distance::surface_both:
	{
		static const std::string r("both");
		return r;
	}

	default:
	{
		static const std::string r("?");
		return r;
	}
	}
}

Aircraft::Distances::Distance::Distance(const Glib::ustring& name, double vrotate, double mass)
	: m_name(name), m_vrotate(vrotate), m_mass(mass), m_softfactor(1.2), m_softwetfactor(std::numeric_limits<double>::quiet_NaN()), m_surface(surface_both)
{
}

#ifdef __WIN32__
__attribute__((force_align_arg_pointer))
#endif
bool Aircraft::Distances::Distance::recalculatepoly(bool force)
{
#ifdef HAVE_EIGEN3
	if (!force && !m_gndpoly.empty() && !m_obstpoly.empty())
		return false;
	if (m_points.empty()) {
		m_gndpoly.clear();
		m_obstpoly.clear();
		return false;
	}
	unsigned int polyorder(4);
	unsigned int ptgnd(0);
	unsigned int ptobst(0);
	Eigen::MatrixXd mg(m_points.size(), polyorder);
	Eigen::MatrixXd mo(m_points.size(), polyorder);
	Eigen::VectorXd vg(m_points.size());
	Eigen::VectorXd vo(m_points.size());
	for (points_t::const_iterator pi(m_points.begin()), pe(m_points.end()); pi != pe; ++pi) {
		double da(pi->get_densityalt());
		if (std::isnan(da))
			continue;
		double d(pi->get_gnddist());
		if (!std::isnan(d)) {
			double da1(1);
			for (unsigned int i = 0; i < polyorder; ++i, da1 *= da)
				mg(ptgnd, i) = da1;
			vg(ptgnd) = d;
			++ptgnd;
		}
		d = pi->get_obstdist();
		if (!std::isnan(d)) {
			double da1(1);
			for (unsigned int i = 0; i < polyorder; ++i, da1 *= da)
				mo(ptobst, i) = da1;
			vo(ptobst) = d;
			++ptobst;
		}
	}
	bool ret(false);
	if (force || m_gndpoly.empty()) {
		m_gndpoly.clear();
		if (ptgnd) {
			ret = true;
			mg.conservativeResize(ptgnd, std::min(ptgnd, polyorder));
			vg.conservativeResize(ptgnd);
			Eigen::VectorXd x(least_squares_solution(mg, vg));
			poly_t p(x.data(), x.data() + x.size());
			m_gndpoly.swap(p);
		}
	}
	if (force || m_obstpoly.empty()) {
		m_obstpoly.clear();
		if (ptobst) {
			ret = true;
			mo.conservativeResize(ptobst, std::min(ptobst, polyorder));
			vo.conservativeResize(ptobst);
			Eigen::VectorXd x(least_squares_solution(mo, vo));
			poly_t p(x.data(), x.data() + x.size());
			m_obstpoly.swap(p);
		}
	}
	return ret;
#else
	return false;
#endif
}

void Aircraft::Distances::Distance::add_point_da(double gnddist, double obstdist, double da)
{
	m_points.push_back(Point(da, std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), gnddist, obstdist));
}

void Aircraft::Distances::Distance::add_point_pa(double gnddist, double obstdist, double pa, double temp)
{
	m_points.push_back(Point(std::numeric_limits<double>::quiet_NaN(), pa, temp, gnddist, obstdist));
}

void Aircraft::Distances::Distance::load_xml(const xmlpp::Element *el, double altfactor, double tempfactor, double tempoffset, double distfactor)
{
	if (!el)
		return;
	m_name.clear();
	m_vrotate = 0;
	m_mass = 0;
	m_softfactor = 1.2;
	m_softwetfactor = std::numeric_limits<double>::quiet_NaN();
	m_surface = surface_both;
	m_gndpoly.clear();
	m_obstpoly.clear();
	m_points.clear();
	xmlpp::Attribute *attr;
	if ((attr = el->get_attribute("name")))
		m_name = attr->get_value();
	if ((attr = el->get_attribute("vrotate")) && attr->get_value() != "")
		m_vrotate = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("mass")) && attr->get_value() != "")
		m_mass = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("softfactor"))) {
		if (attr->get_value().empty())
			m_softfactor = std::numeric_limits<double>::quiet_NaN();
		else
			m_softfactor = Glib::Ascii::strtod(attr->get_value());
	}
	if ((attr = el->get_attribute("softwetfactor"))) {
		if (attr->get_value().empty())
			m_softwetfactor = std::numeric_limits<double>::quiet_NaN();
		else
			m_softwetfactor = Glib::Ascii::strtod(attr->get_value());
	}
	if ((attr = el->get_attribute("surface"))) {
		for (surface_t sfc(surface_hard); sfc <= surface_both; sfc = (surface_t)(sfc + 1)) {
			if (to_str(sfc) != attr->get_value())
				continue;
			m_surface = sfc;
			break;
		}
	}
	{
		xmlpp::Node::NodeList nl(el->get_children("point"));
		for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni) {
			m_points.push_back(Point());
			m_points.back().load_xml(static_cast<xmlpp::Element *>(*ni), altfactor, tempfactor, tempoffset, distfactor);
		}
	}
	{
		xmlpp::Node::NodeList nl(el->get_children("gndpoly"));
		for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni)
			m_gndpoly.load_xml(static_cast<xmlpp::Element *>(*ni));
 	}
	{
		xmlpp::Node::NodeList nl(el->get_children("obstpoly"));
		for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni)
			m_obstpoly.load_xml(static_cast<xmlpp::Element *>(*ni));
	}
}

void Aircraft::Distances::Distance::save_xml(xmlpp::Element *el, double altfactor, double tempfactor, double tempoffset, double distfactor) const
{
	if (!el)
		return;
	if (m_name.empty())
		el->remove_attribute("name");
	else
		el->set_attribute("name", m_name);
	if (m_vrotate == 0 || std::isnan(m_vrotate)) {
		el->remove_attribute("vrotate");
	} else {
		std::ostringstream oss;
		oss << m_vrotate;
		el->set_attribute("vrotate", oss.str());
	}
	if (m_mass == 0 || std::isnan(m_mass)) {
		el->remove_attribute("mass");
	} else {
		std::ostringstream oss;
		oss << m_mass;
		el->set_attribute("mass", oss.str());
	}
	{
		std::ostringstream oss;
		if (!std::isnan(m_softfactor))
			oss << m_softfactor;
		el->set_attribute("softfactor", oss.str());
	}
	{
		std::ostringstream oss;
		if (!std::isnan(m_softwetfactor))
			oss << m_softwetfactor;
		el->set_attribute("softwetfactor", oss.str());
	}
	el->set_attribute("surface", to_str(m_surface));
	for (points_t::const_iterator di(m_points.begin()), de(m_points.end()); di != de; ++di)
		di->save_xml(el->add_child("point"), altfactor, tempfactor, tempoffset, distfactor);
	{
		xmlpp::Node::NodeList nl(el->get_children("gndpoly"));
		for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni)
			el->remove_child(*ni);
	}
	if (!m_gndpoly.empty())
		m_gndpoly.save_xml(el->add_child("gndpoly"));
 	{
		xmlpp::Node::NodeList nl(el->get_children("obstpoly"));
		for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni)
			el->remove_child(*ni);
	}
	if (!m_obstpoly.empty())
		m_obstpoly.save_xml(el->add_child("obstpoly"));
}

bool Aircraft::Distances::Distance::is_compatible(Distances::surface_t sfc) const
{
	if (!(get_surface() & (sfc == Distances::surface_hard ? surface_hard : surface_soft)))
		return false;
	if (sfc != Distances::surface_softwet)
		return true;
	return !std::isnan(get_softwetfactor()) && get_softwetfactor() > 0;
}

double Aircraft::Distances::Distance::get_factor(Distances::surface_t sfc) const
{
	switch (sfc) {
	default:
		return 1;

	case Distances::surface_soft:
		if (std::isnan(get_softfactor()))
			return 1;
		return get_softfactor();

	case Distances::surface_softwet:
		if (std::isnan(get_softwetfactor()))
			return 1;
		return get_softwetfactor();
	}
}

double Aircraft::Distances::Distance::calculate_gnddist(double da) const
{
	return m_gndpoly.eval(da);
}

double Aircraft::Distances::Distance::calculate_obstdist(double da) const
{
	return m_obstpoly.eval(da);
}

void Aircraft::Distances::Distance::calculate(double& gnddist, double& obstdist, double da, double headwind) const
{
	calculate(gnddist, obstdist, da, headwind, get_mass());
}

void Aircraft::Distances::Distance::calculate(double& gnddist, double& obstdist, double da, double headwind, double mass) const
{
	double mmul(1);
	if (!std::isnan(get_mass()) && get_mass() > 0 && !std::isnan(mass) && mass > 0)
		mmul = std::max(0.1, std::min(10.0, mass / get_mass()));
	double mul(1);
	if (!std::isnan(get_vrotate()) && get_vrotate() > 0 && !std::isnan(headwind) && fabs(headwind) <= 0.5 * get_vrotate())
		mul = std::max(0.5, (get_vrotate() - headwind) / get_vrotate());
	double mul2(mul * mul);
	double gd(calculate_gnddist(da));
	double od(calculate_obstdist(da));
	if (std::isnan(gd)) {
		gnddist = gd;
		obstdist = od;
		return;
	}
	if (false)
		std::cerr << "Aircraft: Distance: " << m_name << ": da " << da << " MTOM gnd: " << gd << " obst " << od << std::endl;
	od -= gd;
	od *= mul;
	gd *= mul2 * mmul;
	od += gd;
	gnddist = gd;
	obstdist = od;
	if (false)
		std::cerr << "Aircraft: Distance: " << m_name << ": da " << da << " mass: " << mass << " headwind: " << headwind
			  << " mmul: " << mmul << " mul: " << mul << " gnd: " << gd << " obst " << od << std::endl;
}

std::pair<double,double> Aircraft::Distances::Distance::get_minmax_densityalt(void) const
{
	std::pair<double,double> r(std::numeric_limits<double>::max(), std::numeric_limits<double>::lowest());
	for (points_t::const_iterator pi(m_points.begin()), pe(m_points.end()); pi != pe; ++pi) {
		double da(pi->get_densityalt());
		if (std::isnan(da))
			continue;
		r.first = std::min(r.first, da);
		r.second = std::max(r.second, da);
	}
	return r;
}

const std::string& to_str(Aircraft::Distances::surface_t sfc)
{
	switch (sfc) {
	case Aircraft::Distances::surface_hard:
	{
		static const std::string r("hard");
		return r;
	}

	case Aircraft::Distances::surface_soft:
	{
		static const std::string r("soft");
		return r;
	}

	case Aircraft::Distances::surface_softwet:
	{
		static const std::string r("softwet");
		return r;
	}

	default:
	{
		static const std::string r("?");
		return r;
	}
	}
}

Aircraft::Distances::Distances(unit_t distunit, double altfactor, double tempfactor, double tempoffset, double distfactor)
	: m_altfactor(altfactor), m_tempfactor(tempfactor), m_tempoffset(tempoffset), m_distfactor(distfactor), m_distunit(distunit)
{
	// Default: P28R-200
	static const int16_t p28r200_tkoffdist[][4] = {
		{  756, 1022, 1578, 1800 },
		{  867, 1156, 1800, 2022 },
		{  956, 1289, 2000, 2267 },
		{ 1067, 1422, 2244, 2533 },
		{ 1200, 1600, 2533, 2867 },
		{ 1311, 1755, 2889, 3244 },
		{ 1489, 1978, 3311, 3689 },
		{ 1667, 2200, 3867, 4244 },
		{ 1867, 2467, 4556, 5000 }
	};
	static const int16_t p28r200_ldgdist[][2] = {
		{ 773, 1351 },
		{ 800, 1369 },
		{ 827, 1396 },
		{ 853, 1431 },
		{ 880, 1449 },
		{ 898, 1476 },
		{ 924, 1502 },
		{ 951, 1520 }
	};
	if (!((1 << m_distunit) & unitmask_length))
		m_distunit = unit_m;
	m_takeoffdist.push_back(Distance("Flaps 25", 56, 2600));
	for (unsigned int i = 0; i < sizeof(p28r200_tkoffdist)/sizeof(p28r200_tkoffdist[0]); ++i)
		m_takeoffdist.back().add_point_da(convert(unit_ft, m_distunit, p28r200_tkoffdist[i][0]), convert(unit_ft, m_distunit, p28r200_tkoffdist[i][2]), i * 1000);
	m_takeoffdist.push_back(Distance("Flaps 0", 56, 2600));
	for (unsigned int i = 0; i < sizeof(p28r200_tkoffdist)/sizeof(p28r200_tkoffdist[0]); ++i)
		m_takeoffdist.back().add_point_da(convert(unit_ft, m_distunit, p28r200_tkoffdist[i][1]), convert(unit_ft, m_distunit, p28r200_tkoffdist[i][3]), i * 1000);
	m_ldgdist.push_back(Distance("Flaps 40", 56, 2600));
	for (unsigned int i = 0; i < sizeof(p28r200_ldgdist)/sizeof(p28r200_ldgdist[0]); ++i)
		m_ldgdist.back().add_point_da(convert(unit_ft, m_distunit, p28r200_ldgdist[i][0]), convert(unit_ft, m_distunit, p28r200_ldgdist[i][1]), i * 1000);
	recalculatepoly(true);
}

bool Aircraft::Distances::recalculatepoly(bool force)
{
	bool ret(false);
	for (distances_t::iterator di(m_takeoffdist.begin()), de(m_takeoffdist.end()); di != de; ++di)
		ret = di->recalculatepoly(force) || ret;
	for (distances_t::iterator di(m_ldgdist.begin()), de(m_ldgdist.end()); di != de; ++di)
		ret = di->recalculatepoly(force) || ret;
	return ret;
}

void Aircraft::Distances::clear_takeoffdists(void)
{
	m_takeoffdist.clear();
}

void Aircraft::Distances::add_takeoffdist(const Distance& d)
{
	m_takeoffdist.push_back(d);
}

void Aircraft::Distances::clear_ldgdists(void)
{
	m_ldgdist.clear();
}

void Aircraft::Distances::add_ldgdist(const Distance& d)
{
	m_ldgdist.push_back(d);
}

void Aircraft::Distances::load_xml(const xmlpp::Element *el)
{
	if (!el)
		return;
	m_takeoffdist.clear();
	m_ldgdist.clear();
	m_remark.clear();
	m_altfactor = 1;
	m_tempfactor = 1;
	m_tempoffset = 273.15;
	m_distfactor = std::numeric_limits<double>::quiet_NaN();
	m_distunit = unit_m;
	xmlpp::Attribute *attr;
 	if ((attr = el->get_attribute("altfactor")) && attr->get_value() != "")
		m_altfactor = Glib::Ascii::strtod(attr->get_value());
 	if ((attr = el->get_attribute("tempfactor")) && attr->get_value() != "")
		m_tempfactor = Glib::Ascii::strtod(attr->get_value());
 	if ((attr = el->get_attribute("tempoffset")) && attr->get_value() != "")
		m_tempoffset = Glib::Ascii::strtod(attr->get_value());
 	if ((attr = el->get_attribute("distfactor")) && attr->get_value() != "")
		m_distfactor = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("unitname")))
		m_distunit = parse_unit(attr->get_value());
	if (!((1 << m_distunit) & unitmask_length))
		m_distunit = unit_m;
	if (std::isnan(m_distfactor))
		m_distfactor = convert(m_distunit, unit_m, 1.0);
	if ((attr = el->get_attribute("remark")))
		m_remark = attr->get_value();
	{
		xmlpp::Node::NodeList nl(el->get_children("tkoffdist"));
		for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni) {
			m_takeoffdist.push_back(Distance());
			m_takeoffdist.back().load_xml(static_cast<xmlpp::Element *>(*ni), m_altfactor, m_tempfactor, m_tempoffset, m_distfactor);
		}
	}
	{
		xmlpp::Node::NodeList nl(el->get_children("ldgdist"));
		for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni) {
			m_ldgdist.push_back(Distance());
			m_ldgdist.back().load_xml(static_cast<xmlpp::Element *>(*ni), m_altfactor, m_tempfactor, m_tempoffset, m_distfactor);
		}
	}
	if ((attr = el->get_attribute("vrotate")) && attr->get_value() != "") {
		double vrot(Glib::Ascii::strtod(attr->get_value()));
		if (!std::isnan(vrot) && vrot > 0) {
			for (distances_t::iterator i(m_takeoffdist.begin()), e(m_takeoffdist.end()); i != e; ++i)
				if (std::isnan(i->get_vrotate()) || i->get_vrotate() <= 0)
					i->set_vrotate(vrot);
			for (distances_t::iterator i(m_ldgdist.begin()), e(m_ldgdist.end()); i != e; ++i)
				if (std::isnan(i->get_vrotate()) || i->get_vrotate() <= 0)
					i->set_vrotate(vrot);
		}
	}
	if ((attr = el->get_attribute("mass")) && attr->get_value() != "") {
		double m(Glib::Ascii::strtod(attr->get_value()));
		if (!std::isnan(m) && m > 0) {
			for (distances_t::iterator i(m_takeoffdist.begin()), e(m_takeoffdist.end()); i != e; ++i)
				if (std::isnan(i->get_mass()) || i->get_mass() <= 0)
					i->set_mass(m);
			for (distances_t::iterator i(m_ldgdist.begin()), e(m_ldgdist.end()); i != e; ++i)
				if (std::isnan(i->get_mass()) || i->get_mass() <= 0)
					i->set_mass(m);
		}
	}
}

void Aircraft::Distances::save_xml(xmlpp::Element *el) const
{
	if (!el)
		return;
	if (m_altfactor == 1) {
		el->remove_attribute("altfactor");
	} else {
		std::ostringstream oss;
		oss << m_altfactor;
		el->set_attribute("altfactor", oss.str());
	}
	if (m_tempfactor == 1) {
		el->remove_attribute("tempfactor");
	} else {
		std::ostringstream oss;
		oss << m_tempfactor;
		el->set_attribute("tempfactor", oss.str());
	}
	if (m_tempoffset == 273.15) {
		el->remove_attribute("tempoffset");
	} else {
		std::ostringstream oss;
		oss << m_tempoffset;
		el->set_attribute("tempoffset", oss.str());
	}
	{
		bool dfltdistf(m_distfactor == 1);
		if (m_distunit != unit_invalid) {
			double distf(convert(m_distunit, unit_m, 1.0));
			dfltdistf = fabs(m_distfactor - distf) < 1e-6 * distf;
		}
		if (dfltdistf) {
			el->remove_attribute("distfactor");
		} else {
			std::ostringstream oss;
			oss << m_distfactor;
			el->set_attribute("distfactor", oss.str());
		}
	}
	if (m_distunit == unit_invalid)
		el->remove_attribute("unitname");
	else
		el->set_attribute("unitname", to_str(m_distunit));
	if (m_remark.empty())
		el->remove_attribute("remark");
	else
		el->set_attribute("remark", m_remark);
	for (distances_t::const_iterator di(m_takeoffdist.begin()), de(m_takeoffdist.end()); di != de; ++di)
		di->save_xml(el->add_child("tkoffdist"), m_altfactor, m_tempfactor, m_tempoffset, m_distfactor);
	for (distances_t::const_iterator di(m_ldgdist.begin()), de(m_ldgdist.end()); di != de; ++di)
		di->save_xml(el->add_child("ldgdist"), m_altfactor, m_tempfactor, m_tempoffset, m_distfactor);
}

Aircraft::Distances::names_t Aircraft::Distances::get_takeoffnames(void) const
{
	names_t n;
	for (distances_t::const_iterator di(m_takeoffdist.begin()), de(m_takeoffdist.end()); di != de; ++di)
		n.insert(di->get_name());
	return n;
}

Aircraft::Distances::names_t Aircraft::Distances::get_ldgnames(void) const
{
	names_t n;
	for (distances_t::const_iterator di(m_ldgdist.begin()), de(m_ldgdist.end()); di != de; ++di)
		n.insert(di->get_name());
	return n;
}

void Aircraft::Distances::calculate_takeoff(double& gnddist, double& obstdist, double& vrot, surface_t sfc, const std::string& name, double da, double headwind, double mass) const
{
	std::pair<double,double> dalim(std::numeric_limits<double>::max(), std::numeric_limits<double>::lowest());
	masses_t masses;
	for (distances_t::const_iterator di(m_takeoffdist.begin()), de(m_takeoffdist.end()); di != de; ++di) {
		const Distance& dist(*di);
		if (dist.get_name() != name || !dist.is_compatible(sfc) || std::isnan(dist.get_mass()))
			continue;
		masses.insert(&dist);
		{
			std::pair<double,double> d(dist.get_minmax_densityalt());
			dalim.first = std::min(dalim.first, d.first);
			dalim.second = std::max(dalim.second, d.second);
		}
	}
	if (dalim.first <= dalim.second)
		da = std::max(std::min(da, dalim.second), dalim.first);
	calculate(gnddist, obstdist, vrot, masses, sfc, da, headwind, mass);
}

void Aircraft::Distances::calculate_ldg(double& gnddist, double& obstdist, double& vrot, surface_t sfc, const std::string& name, double da, double headwind, double mass) const
{
	std::pair<double,double> dalim(std::numeric_limits<double>::max(), std::numeric_limits<double>::lowest());
	masses_t masses;
	for (distances_t::const_iterator di(m_ldgdist.begin()), de(m_ldgdist.end()); di != de; ++di) {
		const Distance& dist(*di);
		if (dist.get_name() != name || !dist.is_compatible(sfc) || std::isnan(dist.get_mass()))
			continue;
		masses.insert(&dist);
		{
			std::pair<double,double> d(dist.get_minmax_densityalt());
			dalim.first = std::min(dalim.first, d.first);
			dalim.second = std::max(dalim.second, d.second);
		}
	}
	if (dalim.first <= dalim.second)
		da = std::max(std::min(da, dalim.second), dalim.first);
	calculate(gnddist, obstdist, vrot, masses, sfc, da, headwind, mass);
}

void Aircraft::Distances::calculate(double& gnddist, double& obstdist, double& vrot, const masses_t& masses, surface_t sfc, double da, double headwind, double mass) const
{
	gnddist = obstdist = vrot = std::numeric_limits<double>::quiet_NaN();
	if (masses.empty())
		return;
	if (std::isnan(mass) || std::isnan(headwind) || std::isnan(da))
		return;
	mass = std::max(mass, 0.);
	for (masses_t::const_iterator i(masses.begin()), e(masses.end()); i != e; ) {
		const Distance& dist0(**i);
		if (mass < dist0.get_mass()) {
			double gnddist1, obstdist1;
			dist0.calculate(gnddist1, obstdist1, da, headwind);
			double factor(dist0.get_factor(sfc));
			gnddist = gnddist1 * factor;
			obstdist = gnddist1 * (factor - 1) + obstdist1;
			vrot = dist0.get_vrotate();
			return;
		}
		++i;
		if (i == e) {
			double gnddist1, obstdist1;
			dist0.calculate(gnddist1, obstdist1, da, headwind, mass);
			double factor(dist0.get_factor(sfc));
			gnddist = gnddist1 * factor;
			obstdist = gnddist1 * (factor - 1) + obstdist1;
			vrot = dist0.get_vrotate();
			return;
		}
		const Distance& dist1(**i);
		if (mass > dist1.get_mass())
			continue;
		double gnddist0, obstdist0, gnddist1, obstdist1;
		dist0.calculate(gnddist0, obstdist0, da, headwind);
		dist1.calculate(gnddist1, obstdist1, da, headwind);
		double factor0(dist0.get_factor(sfc));
		double factor1(dist0.get_factor(sfc));
		obstdist0 += gnddist0 * (factor0 - 1);
		gnddist0 *= factor0;
		obstdist1 += gnddist1 * (factor1 - 1);
		gnddist1 *= factor1;
		double mf((mass - dist0.get_mass()) / (dist1.get_mass() - dist0.get_mass()));
		gnddist = gnddist0 * mf + gnddist1 * (1 - mf);
		obstdist = obstdist0 * mf + obstdist1 * (1 - mf);
		vrot = dist0.get_vrotate() * mf + dist1.get_vrotate() * (1 - mf);
		return;
	}
}
