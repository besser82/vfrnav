/*****************************************************************************/

/*
 *      acftglide.cc  --  Aircraft Model Glide.
 *
 *      Copyright (C) 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020
 *        Thomas Sailer (t.sailer@alumni.ethz.ch)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

/*****************************************************************************/

#include "aircraft.h"

Aircraft::Glide::Glide(double altfactor, double ratefactor, double casfactor, double timefactor, double distfactor)
	: Climb(altfactor, ratefactor, std::numeric_limits<double>::quiet_NaN(), casfactor,
		timefactor, std::numeric_limits<double>::quiet_NaN(), distfactor)
{
	set_default(2600, 17000, 105 * 0.86897624, 8.1086);
}

void Aircraft::Glide::load_xml(const xmlpp::Element *el, double ceiling)
{
	if (!el)
		return;
	m_curves.clear();
	m_remark.clear();
	m_altfactor = 1;
	m_ratefactor = 1;
	m_fuelflowfactor = std::numeric_limits<double>::quiet_NaN();
	m_casfactor = 1;
	m_timefactor = 1;
	m_fuelfactor = std::numeric_limits<double>::quiet_NaN();
	m_distfactor = 1;
	xmlpp::Attribute *attr;
	if ((attr = el->get_attribute("altfactor")) && attr->get_value() != "")
		m_altfactor = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("ratefactor")) && attr->get_value() != "")
		m_ratefactor = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("casfactor")) && attr->get_value() != "")
		m_casfactor = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("timefactor")) && attr->get_value() != "")
		m_timefactor = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("distfactor")) && attr->get_value() != "")
		m_distfactor = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("remark")))
		m_remark = attr->get_value();
	{
		xmlpp::Node::NodeList nl(el->get_children("curve"));
		for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni) {
			xmlpp::Element *el(static_cast<xmlpp::Element *>(*ni));
			ClimbDescent cd;
			cd.load_xml(el, m_altfactor, m_ratefactor, m_fuelflowfactor, m_casfactor,
				    m_timefactor, m_fuelfactor, m_distfactor);
			{
				double vbestglide(std::numeric_limits<double>::quiet_NaN());
				double glideslope(std::numeric_limits<double>::quiet_NaN());
				if ((attr = el->get_attribute("vbestglide")) && attr->get_value() != "")
					vbestglide = Glib::Ascii::strtod(attr->get_value());
				if ((attr = el->get_attribute("glideslope")) && attr->get_value() != "")
					glideslope = Glib::Ascii::strtod(attr->get_value());
				set_default(cd, ceiling, vbestglide, glideslope);
			}
			add(cd);
		}
		if (nl.empty()) {
			ClimbDescent cd;
			cd.load_xml(el, m_altfactor, m_ratefactor, m_fuelflowfactor, m_casfactor,
				    m_timefactor, m_fuelfactor, m_distfactor);
			{
				double vbestglide(std::numeric_limits<double>::quiet_NaN());
				double glideslope(std::numeric_limits<double>::quiet_NaN());
				if ((attr = el->get_attribute("vbestglide")) && attr->get_value() != "")
					vbestglide = Glib::Ascii::strtod(attr->get_value());
				if ((attr = el->get_attribute("glideslope")) && attr->get_value() != "")
					glideslope = Glib::Ascii::strtod(attr->get_value());
				set_default(cd, ceiling, vbestglide, glideslope);
			}
			if (cd.has_points()) {
				add(cd);
			}
		}
	}
}

void Aircraft::Glide::save_xml(xmlpp::Element *el) const
{
	if (m_altfactor == 1) {
		el->remove_attribute("altfactor");
	} else {
		std::ostringstream oss;
		oss << m_altfactor;
		el->set_attribute("altfactor", oss.str());
	}
	if (m_ratefactor == 1) {
		el->remove_attribute("ratefactor");
	} else {
		std::ostringstream oss;
		oss << m_ratefactor;
		el->set_attribute("ratefactor", oss.str());
	}
	if (m_casfactor == 1) {
		el->remove_attribute("casfactor");
	} else {
		std::ostringstream oss;
		oss << m_casfactor;
		el->set_attribute("casfactor", oss.str());
	}
	if (m_timefactor == 1) {
		el->remove_attribute("timefactor");
	} else {
		std::ostringstream oss;
		oss << m_timefactor;
		el->set_attribute("timefactor", oss.str());
	}
	if (m_distfactor == 1) {
		el->remove_attribute("distfactor");
	} else {
		std::ostringstream oss;
		oss << m_distfactor;
		el->set_attribute("distfactor", oss.str());
	}
	if (m_remark.empty())
		el->remove_attribute("remark");
	else
		el->set_attribute("remark", m_remark);
	for (curves_t::const_iterator ci(m_curves.begin()), ce(m_curves.end()); ci != ce; ++ci)
		for (massmap_t::const_iterator mi(ci->second.begin()), me(ci->second.end()); mi != me; ++mi)
			for (isamap_t::const_iterator ii(mi->second.begin()), ie(mi->second.end()); ii != ie; ++ii) {
				xmlpp::Element *el2(el->add_child("curve"));
				ii->second.save_xml(el2, m_altfactor, m_ratefactor, m_fuelflowfactor, m_casfactor,
						    m_timefactor, m_fuelfactor, m_distfactor);
				{
					double vbestglide(ii->second.get_constant_cas());
					if (std::isnan(vbestglide)) {
						el2->remove_attribute("vbestglide");
					} else {
						std::ostringstream oss;
						oss << vbestglide;
						el2->set_attribute("vbestglide", oss.str());
					}
				}
				{
					double glideslope(ii->second.get_constant_slope());
					if (std::isnan(glideslope)) {
						el2->remove_attribute("glideslope");
					} else {
						std::ostringstream oss;
						oss << glideslope;
						el2->set_attribute("glideslope", oss.str());
					}
				}
			}
}

bool Aircraft::Glide::set_default(ClimbDescent& cd, double ceiling, double vbg, double glideslope)
{
	if (std::isnan(ceiling) || std::isnan(vbg) || std::isnan(glideslope) ||
	    ceiling <= 0 || vbg <= 0 || glideslope <= 0)
		return false;
	if (cd.has_points() || std::isnan(cd.get_mass()) || cd.get_mass() <= 0 || std::isnan(cd.get_isaoffset()))
		return false;
	cd.set_ceiling(ceiling);
	for (double alt = 0;; alt += 1000) {
		const double knots_to_ftpm = 1.e3 / Point::km_to_nmi_dbl * Point::m_to_ft_dbl / 60.0;
		bool last(alt >= ceiling);
		if (last)
			alt = ceiling;
		AirData<double> ad;
		ad.set_qnh_tempoffs(IcaoAtmosphere<double>::std_sealevel_pressure, cd.get_isaoffset());
		ad.set_pressure_altitude(alt);
		ad.set_cas(vbg);
		double tas(ad.get_tas());
		cd.add_point(ClimbDescent::Point(alt, tas * knots_to_ftpm / glideslope, 0, vbg));
		if (false)
			std::cerr << "Glide Point: alt " << alt << " rod " << (tas * knots_to_ftpm / get_glideslope())
				  << " cas " << vbg << std::endl;
		if (last)
			break;
	}
	cd.recalculatepoly(true);
	return true;
}

void Aircraft::Glide::set_default(double mass, double ceiling, double vbg, double glideslope)
{
	if (std::isnan(ceiling) || std::isnan(vbg) || std::isnan(glideslope) ||
	    ceiling <= 0 || vbg <= 0 || glideslope <= 0)
		return;
	m_curves.clear();
	{
		ClimbDescent cd("", mass);
		cd.clear_points();
		add(cd);
	}
	for (curves_t::iterator ci(m_curves.begin()), ce(m_curves.end()); ci != ce; ++ci)
		for (massmap_t::iterator mi(ci->second.begin()), me(ci->second.end()); mi != me; ++mi)
			for (isamap_t::iterator ii(mi->second.begin()), ie(mi->second.end()); ii != ie; ++ii)
				set_default(ii->second, ceiling, vbg, glideslope);
}

double Aircraft::Glide::get_vbestglide(void) const
{
	const double tolerance = 0.05;
	std::vector<double> x;
	for (curves_t::const_iterator ci(m_curves.begin()), ce(m_curves.end()); ci != ce; ++ci)
		for (massmap_t::const_iterator mi(ci->second.begin()), me(ci->second.end()); mi != me; ++mi)
			for (isamap_t::const_iterator ii(mi->second.begin()), ie(mi->second.end()); ii != ie; ++ii) {
				double xx(ii->second.get_constant_cas());
				if (std::isnan(xx) || xx <= 0)
					return std::numeric_limits<double>::quiet_NaN();
				x.push_back(xx);
			}
	if (x.size() < 1)
		return std::numeric_limits<double>::quiet_NaN();
	std::sort(x.begin(), x.end());
	double s(x[x.size() / 2]);
	if (!(x.size() & 1)) {
		s += x[x.size() / 2 - 1];
		s *= 0.5;
	}
	if (x[0] < s * (1 - tolerance) || x[x.size() - 1] > s * (1 + tolerance))
		return std::numeric_limits<double>::quiet_NaN();
	return s;
}

double Aircraft::Glide::get_glideslope(void) const
{
	const double tolerance = 0.05;
	std::vector<double> x;
	for (curves_t::const_iterator ci(m_curves.begin()), ce(m_curves.end()); ci != ce; ++ci)
		for (massmap_t::const_iterator mi(ci->second.begin()), me(ci->second.end()); mi != me; ++mi)
			for (isamap_t::const_iterator ii(mi->second.begin()), ie(mi->second.end()); ii != ie; ++ii) {
				double xx(ii->second.get_constant_slope());
				if (std::isnan(xx) || xx <= 0)
					return std::numeric_limits<double>::quiet_NaN();
				x.push_back(xx);
			}
	if (x.size() < 1)
		return std::numeric_limits<double>::quiet_NaN();
	std::sort(x.begin(), x.end());
	double s(x[x.size() / 2]);
	if (!(x.size() & 1)) {
		s += x[x.size() / 2 - 1];
		s *= 0.5;
	}
	if (x[0] < s * (1 - tolerance) || x[x.size() - 1] > s * (1 + tolerance))
		return std::numeric_limits<double>::quiet_NaN();
	return s;
}
