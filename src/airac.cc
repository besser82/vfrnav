/*****************************************************************************/

/*
 *      airac.cc  --  AIRAC date calculations
 *
 *      Copyright (C) 2019  Thomas Sailer (t.sailer@alumni.ethz.ch)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

/*****************************************************************************/

#include <sstream>
#include <iomanip>

#include "airac.h"

bool AIRAC::set_cycle(const std::string& s)
{
	clear();
	if (s.size() != 4 && s.size() != 6)
		return false;
	guint32 y(0);
	for (std::string::const_iterator si(s.begin()), se(s.end()); si != se; ++si) {
		if (*si < '0' || *si > '9')
			return false;
		y *= 10;
		y += *si - '0';
	}
	Seq seq(y % 100);
	y /= 100;
	if (s.size() == 4) {
		if (y < 64)
			y += 100;
		y += 1900;
	}
	return set_cycle(y, seq);
}

bool AIRAC::set_cycle(Year y, Seq s)
{
	clear();
	if (s < 1)
		return false;
	guint32 j(get_first_of_year(y));
	if (!j)
		return false;
	j += (s - 1) * period;
	set_julian(j);
	if (get_year() == y)
		return true;
	clear();
	return false;
}

bool AIRAC::get_cycle(Year& y, Seq& s) const
{
	y = 0;
	s = 0;
	guint32 j(get_julian());
	if (j < offset)
		return false;
	guint32 jb(get_first_of_year(get_year()));
	if (!jb)
		return false;
	if (j >= jb) {
		y = get_year();
	} else {
		jb = get_first_of_year(get_year() - 1);
		if (!jb)
			return false;
		y = get_year() - 1;
	}
	s = (j - jb) / period + 1;
	return true;
}

guint32 AIRAC::get_first_of_year(Year y)
{
	Glib::Date dt(1, Glib::Date::JANUARY, y);
	if (!dt.valid())
		return 0;
	guint32 jb(dt.get_julian());
	guint32 j(jb + (period - offset));
	j /= period;
	j *= period;
	j += offset;
	if (j - jb >= period)
		j -= period;
	return j;
}

bool AIRAC::set_iso8601(const std::string& s)
{
	clear();
	if (s.size() != 10 || s[4] != '-' || s[7] != '-')
		return false;
	Year y(0);
	guint8 m(0);
	Day d(0);
	for (guint i(0); i < 4; ++i) {
		if (s[i] < '0' || s[i] > '9')
			return false;
		y *= 10;
		y += s[i] - '0';
	}
	for (guint i(5); i < 7; ++i) {
		if (s[i] < '0' || s[i] > '9')
			return false;
		m *= 10;
		m += s[i] - '0';
	}
	for (guint i(8); i < 10; ++i) {
		if (s[i] < '0' || s[i] > '9')
			return false;
		d *= 10;
		d += s[i] - '0';
	}
	if (m < 1 || m > 12)
		return false;
	set_dmy(d, static_cast<Month>(m), y);
	if (valid())
		return true;
	clear();
	return false;
}

void AIRAC::prev_cycle(void)
{
	guint32 j(get_julian());
	if (j < period) {
		clear();
		return;
	}
	j -= period;
	set_julian(j);
}

void AIRAC::next_cycle(void)
{
	guint32 j(get_julian());
	j += period;
	if (j < period) {
		clear();
		return;
	}
	set_julian(j);
}

Glib::DateTime AIRAC::get_datetime(void) const
{
	if (!valid())
		return Glib::DateTime();
	return Glib::DateTime(Glib::DateTime::create_utc(get_year(), static_cast<int>(get_month()), get_day(), 0, 0, 0));
}

gint64 AIRAC::get_unix(void) const
{
	if (!valid())
		return 0;
	Glib::DateTime dt(Glib::DateTime::create_utc(get_year(), static_cast<int>(get_month()), get_day(), 0, 0, 0));
	return dt.to_unix();
}

std::string AIRAC::get_cyclestring(void) const
{
	Year y;
	Seq s;
	if (!get_cycle(y, s))
		return "";
	std::ostringstream oss;
	oss << std::setw(4) << std::setfill('0') << static_cast<unsigned int>(y)
	    << std::setw(2) << std::setfill('0') << static_cast<unsigned int>(s);
	return oss.str();
}
