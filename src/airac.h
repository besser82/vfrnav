//
// C++ Interface: airac
//
// Description: AIRAC date calculations
//
//
// Author: Thomas Sailer <t.sailer@alumni.ethz.ch>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef AIRAC_H
#define AIRAC_H

#include "sysdeps.h"
#include <glibmm/date.h>
#include <glibmm/datetime.h>

class AIRAC : public Glib::Date {
public:
	using Seq = guint8;

	bool set_cycle(Year y, Seq s);
	bool set_cycle(const std::string& s);
	bool get_cycle(Year& y, Seq& s) const;
	static guint32 get_first_of_year(Year y);
	bool set_iso8601(const std::string& s);
	void prev_cycle(void);
	void next_cycle(void);
	Glib::DateTime get_datetime(void) const;
	gint64 get_unix(void) const;
	std::string get_cyclestring(void) const;

protected:
	static constexpr guint32 period = 28;
	static constexpr guint32 offset = 18; // julian(1998-01-29) modulo period
};

#endif /* AIRAC_H */
