/*****************************************************************************/

/*
 *      aircraft.cc  --  Aircraft Model.
 *
 *      Copyright (C) 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020
 *        Thomas Sailer (t.sailer@alumni.ethz.ch)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

/*****************************************************************************/

#include "aircraft.h"
#include "modes.h"

#include <iostream>
#include <iomanip>

#ifdef HAVE_ICONV
#include <iconv.h>
#endif

#ifdef HAVE_JSONCPP
#include <json/json.h>
#endif

#ifdef HAVE_PQXX
#include <pqxx/connection>
#include <pqxx/transaction>
#include <pqxx/transactor>
#include <pqxx/result>
#include <pqxx/except>
#endif

Aircraft::OtherInfo::OtherInfo(const Glib::ustring& category, const Glib::ustring& text)
	: m_category(category), m_text(text)
{
}

bool Aircraft::OtherInfo::is_category_valid(const Glib::ustring& cat)
{
	Glib::ustring::const_iterator si(cat.begin()), se(cat.end());
	if (si == se)
		return false;
	if (!std::isalpha(*si))
		return false;
	for (++si; si != se; ++si)
		if (!std::isalnum(*si))
			return false;
	return true;
}

std::string Aircraft::OtherInfo::get_valid_text(const std::string& txt)
{
	typedef enum {
		spc_begin,
		spc_no,
		spc_yes
	} spc_t;
	spc_t spc(spc_begin);
	std::string r;
	r.reserve(txt.size());
	for (std::string::const_iterator si(txt.begin()), se(txt.end()); si != se; ++si) {
		if (std::isspace(*si) || *si == '/' || *si == '-') {
			if (spc != spc_begin)
				spc = spc_yes;
			continue;
		}
		if (*si < ' ' || *si >= 127)
			continue;
		if (spc == spc_yes)
			r.push_back(' ');
		spc = spc_no;
		r.push_back(*si);
	}
	return r;
}

Glib::ustring Aircraft::OtherInfo::get_valid_text(const Glib::ustring& txt)
{
	typedef enum {
		spc_begin,
		spc_no,
		spc_yes
	} spc_t;
	spc_t spc(spc_begin);
	Glib::ustring r;
	r.reserve(txt.size());
	for (Glib::ustring::const_iterator si(txt.begin()), se(txt.end()); si != se; ++si) {
		if (std::isspace(*si) || *si == '/' || *si == '-') {
			if (spc != spc_begin)
				spc = spc_yes;
			continue;
		}
		if (*si < ' ' || *si >= 127)
			continue;
		if (spc == spc_yes)
			r.push_back(' ');
		spc = spc_no;
		r.push_back(*si);
	}
	return r;
}

const std::string& to_str(Aircraft::CheckError::type_t t)
{
	switch (t) {
	case Aircraft::CheckError::type_climb:
	{
		static const std::string r("climb");
		return r;
	}

	case Aircraft::CheckError::type_descent:
	{
		static const std::string r("descent");
		return r;
	}

	case Aircraft::CheckError::type_cruise:
	{
		static const std::string r("cruise");
		return r;
	}

	case Aircraft::CheckError::type_unknown:
	default:
	{
		static const std::string r("unknown");
		return r;
	}
	}
}

const std::string& to_str(Aircraft::CheckError::severity_t s)
{
	switch (s) {
	case Aircraft::CheckError::severity_warning:
	{
		static const std::string r("warning");
		return r;
	}

	case Aircraft::CheckError::severity_error:
	{
		static const std::string r("error");
		return r;
	}

	default:
	{
		static const std::string r("invalid");
		return r;
	}
	}
}

Aircraft::CheckError::CheckError(type_t typ, severity_t sev, const std::string& n, double mass, double isaoffs, const std::string& msg)
	: m_name(n), m_message(msg), m_mass(mass), m_isaoffset(isaoffs),
	  m_timeinterval(timeinterval_t(std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN())),
	  m_type(typ), m_severity(sev)
{
}

Aircraft::CheckError::CheckError(const ClimbDescent& cd, bool descent, severity_t sev, double tmin, double tmax)
	: m_name(cd.get_name()), m_mode(::to_str(cd.get_mode())), m_mass(cd.get_mass()), m_isaoffset(cd.get_isaoffset()),
	  m_timeinterval(timeinterval_t(tmin, tmax)), m_type(descent ? type_descent : type_climb), m_severity(sev)
{
}

Aircraft::CheckError::CheckError(const Cruise::Curve& c, severity_t sev)
	: m_name(c.get_name()), m_message(""), m_mass(c.get_mass()), m_isaoffset(c.get_isaoffset()),
	  m_timeinterval(timeinterval_t(std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN())),
	  m_type(type_cruise), m_severity(sev)
{
}

Aircraft::CheckError::CheckError(const Cruise::OptimalAltitude& oa, double isaoffs, severity_t sev)
	: m_name("Optimal Altitude"), m_message(""), m_mass(std::numeric_limits<double>::quiet_NaN()), m_isaoffset(isaoffs),
	  m_timeinterval(timeinterval_t(std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN())),
	  m_type(type_cruise), m_severity(sev)
{
}

Aircraft::CheckError::CheckError(const Cruise::OptimalAltitudePoint& oap, severity_t sev)
	: m_name("Optimal Altitude Point"), m_message(""), m_mass(oap.get_mass()), m_isaoffset(oap.get_isaoffs()),
	  m_timeinterval(timeinterval_t(std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN())),
	  m_type(type_cruise), m_severity(sev)
{
}

std::string Aircraft::CheckError::to_str(void) const
{
	std::ostringstream oss;
	switch (get_severity()) {
	case severity_warning:
		oss << "[W]";
		break;

	case severity_error:
		oss << "[E]";
		break;

	default:
		oss << "[?]";
		break;
	}
	oss << ' ' << ::to_str(get_type());
	if (!get_name().empty())
		oss << " name " << get_name();
	if (!std::isnan(get_mass()))
		oss << " mass " << get_mass();
	if (!std::isnan(get_isaoffset()))
		oss << " isaoffset " << get_isaoffset();
	if (!get_mode().empty())
		oss << " mode " << get_mode();
	if (!std::isnan(get_mintime()) || !std::isnan(get_maxtime())) {
		oss << " time ";
		if (!std::isnan(get_mintime()))
			oss << get_mintime();
		oss << "...";
		if (!std::isnan(get_maxtime()))
			oss << get_maxtime();
	}
	oss << ' ' << get_message();
	return oss.str();
}

#ifdef HAVE_JSONCPP
Json::Value Aircraft::CheckError::to_json(void) const
{
	Json::Value r;
	r["type"] = ::to_str(get_type());
	r["severity"] = ::to_str(get_severity());
	if (!get_name().empty())
		r["name"] = get_name();
	if (!get_mode().empty())
		r["mode"] = get_mode();
	if (!std::isnan(get_mass()))
		r["mass"] = get_mass();
	if (!std::isnan(get_isaoffset()))
		r["isaoffset"] = get_isaoffset();
	if (!std::isnan(get_mintime()))
		r["mintime"] = get_mintime();
	if (!std::isnan(get_maxtime()))
		r["maxtime"] = get_maxtime();
	r["message"] = get_message();
	return r;
}
#endif

void Aircraft::CheckErrors::set_name(const std::string& n)
{
	for (iterator i(begin()), e(end()); i != e; ++i)
		i->set_name(n);
}

void Aircraft::CheckErrors::set_mode(const std::string& m)
{
	for (iterator i(begin()), e(end()); i != e; ++i)
		i->set_mode(m);
}

void Aircraft::CheckErrors::set_mass(double m)
{
	for (iterator i(begin()), e(end()); i != e; ++i)
		i->set_mass(m);
}

void Aircraft::CheckErrors::set_isaoffset(double io)
{
	for (iterator i(begin()), e(end()); i != e; ++i)
		i->set_isaoffset(io);
}

void Aircraft::CheckErrors::set_type(CheckError::type_t t)
{
	for (iterator i(begin()), e(end()); i != e; ++i)
		i->set_type(t);
}

void Aircraft::CheckErrors::set_severity(CheckError::severity_t s)
{
	for (iterator i(begin()), e(end()); i != e; ++i)
		i->set_severity(s);
}

Aircraft::CheckErrors::iterator Aircraft::CheckErrors::add(const CheckErrors& ce)
{
	return insert(end(), ce.begin(), ce.end());
}

Aircraft::CheckError::MessageOStream Aircraft::CheckErrors::add(const CheckError& ce)
{
	push_back(ce);
	return back().get_messageostream();
}

std::ostream& Aircraft::CheckErrors::print(std::ostream& os, const std::string& indent) const
{
	if (empty())
		return os;
	os << indent << "Aircraft Check Errors:" << std::endl;
	for (const_iterator i(begin()), e(end()); i != e; ++i)
		os << indent << i->to_str() << std::endl;
	return os;
}

#ifdef HAVE_JSONCPP
Json::Value Aircraft::CheckErrors::to_json(void) const
{
	Json::Value r(Json::arrayValue);
	for (const_iterator i(begin()), e(end()); i != e; ++i)
		r.append(i->to_json());
	return r;
}
#endif

constexpr double Aircraft::avgas_density;
constexpr double Aircraft::jeta_density;
constexpr double Aircraft::diesel_density;
constexpr double Aircraft::oil_density;
constexpr double Aircraft::quart_to_liter;
constexpr double Aircraft::usgal_to_liter;
constexpr double Aircraft::lb_to_kg;
constexpr double Aircraft::kg_to_lb;

const std::string& to_str(Aircraft::unit_t u, bool lng)
{
	switch (u) {
	case Aircraft::unit_m:
	{
		static const std::string r("m");
		return r;
	}

	case Aircraft::unit_cm:
	{
		static const std::string r("cm");
		return r;
	}

	case Aircraft::unit_km:
	{
		static const std::string r("km");
		return r;
	}

	case Aircraft::unit_in:
	{
		static const std::string r("in");
		return r;
	}

	case Aircraft::unit_ft:
	{
		static const std::string r("ft");
		return r;
	}

	case Aircraft::unit_nmi:
	{
		static const std::string r("nmi");
		return r;
	}

	case Aircraft::unit_kg:
	{
		static const std::string r("kg");
		return r;
	}

	case Aircraft::unit_lb:
	{
		static const std::string r("lb");
		return r;
	}

	case Aircraft::unit_liter:
	{
		if (lng) {
			static const std::string r("Liter");
			return r;
		}
		static const std::string r("l");
		return r;
	}

	case Aircraft::unit_usgal:
	{
		if (lng) {
			static const std::string r("USGal");
			return r;
		}
		static const std::string r("usg");
		return r;
	}

	case Aircraft::unit_quart:
	{
		if (lng) {
			static const std::string r("Quart");
			return r;
		}
		static const std::string r("qt");
		return r;
	}

	case Aircraft::unit_invalid:
	default:
	{
		static const std::string r("invalid");
		return r;
	}
	}
}

std::string to_str(Aircraft::unitmask_t um, bool lng)
{
	std::string r;
	for (Aircraft::unit_t u(Aircraft::unit_m); u <= Aircraft::unit_invalid; u = (Aircraft::unit_t)(u + 1)) {
		if (!(um & (Aircraft::unitmask_t)(1 << u)))
			continue;
		if (!r.empty())
			r += ",";
		r += to_str(u, lng);
	}
	return r;
}

const std::string& to_str(Aircraft::propulsion_t prop)
{
	switch (prop) {
	case Aircraft::propulsion_fixedpitch:
	{
		static const std::string r("fixedpitch");
		return r;
	}

	case Aircraft::propulsion_constantspeed:
	{
		static const std::string r("constantspeed");
		return r;
	}

	case Aircraft::propulsion_turboprop:
	{
		static const std::string r("turboprop");
		return r;
	}

	case Aircraft::propulsion_turbojet:
	{
		static const std::string r("turbojet");
		return r;
	}

	default:
	{
		static const std::string r("?");
		return r;
	}
	}
}

const std::string& to_str(Aircraft::opsrules_t r)
{
	switch (r) {
	case Aircraft::opsrules_easacat:
	{
		static const std::string r("easacat");
		return r;
	}

	case Aircraft::opsrules_easanco:
	{
		static const std::string r("easanco");
		return r;
	}

	case Aircraft::opsrules_auto:
	default:
	{
		static const std::string r("auto");
		return r;
	}
	}
}

Aircraft::Aircraft(void)
	: m_callsign("ZZZZZ"), m_manufacturer("Piper"), m_model("PA28R-200"), m_year("1971"), m_icaotype("P28R"),
	  m_dinghiescolor(), m_colormarking("WHITE BLUE STRIPES"),
	  m_picname(""), m_crewcontact(""), m_homebase(""),
	  m_mrm(2600), m_mtom(2600), m_mlm(2600), m_mzfm(2600),
	  m_vr0(62 * 0.86897624), m_vr1(68 * 0.86897624), m_vx0(80 * 0.86897624), m_vx1(80 * 0.86897624),
	  m_vy(95 * 0.86897624), m_vs0(64 * 0.86897624), m_vs1(70 * 0.86897624), m_vno(170 * 0.86897624), m_vne(214 * 0.86897624),
	  m_vref0(64 * 1.4 * 0.86897624), m_vref1(70 * 1.4 * 0.86897624), m_va(134 * 0.86897624), m_vfe(125 * 0.86897624),
	  m_vgearext(150 * 0.86897624), m_vgearret(125 * 0.86897624),
	  m_vdescent(150 * 0.86897624), m_fuelmass(usgal_to_liter * avgas_density * kg_to_lb), m_taxifuel(0.5), m_taxifuelflow(0.),
	  m_maxbhp(200), m_contingencyfuel(5), m_dinghiesnumber(0), m_dinghiescapacity(0),
	  m_nav(nav_lpv | nav_dme | nav_adf | nav_gnss | nav_pbn),
	  m_com(com_standard | com_vhf_833), m_transponder(transponder_modes_s), m_emergency(emergency_radio_elt),
	  m_pbn(pbn_b2 | pbn_d2), m_gnssflags(gnssflags_gps | gnssflags_sbas), m_fuelunit(unit_usgal),
	  m_propulsion(propulsion_constantspeed), m_opsrules(opsrules_auto), m_freecirculation(false)
{
	get_cruise().set_mass(get_mtom());
	get_cruise().build_altmap(get_propulsion());
	get_climb().set_point_vy(get_vy());
	get_climb().set_mass(get_mtom());
	get_descent().set_point_vy(get_vdescent());
	get_descent().set_mass(get_mtom());
	get_descent().set_default(get_propulsion(), get_cruise(), get_climb());
	//check_aircraft_type_class();
}

void Aircraft::load_xml(const xmlpp::Element *el)
{
	if (!el)
		return;
	m_wb.clear_elements();
	m_wb.clear_envelopes();
	m_dist.clear_takeoffdists();
	m_dist.clear_ldgdists();
	m_climb.clear();
	m_descent.clear();
	m_glide.clear();
	m_cruise.clear_curves();
	m_otherinfo.clear();
	m_callsign.clear();
	m_manufacturer.clear();
	m_model.clear();
	m_year.clear();
	m_icaotype.clear();
	m_dinghiescolor.clear();
	m_dinghiesnumber = 0;
	m_dinghiescapacity = 0;
	m_nav = nav_none;
	m_com = com_none;
	m_transponder = transponder_none;
	m_emergency = emergency_none;
	m_pbn = pbn_none;
	m_gnssflags = gnssflags_none;
	m_colormarking.clear();
	m_picname.clear();
	m_crewcontact.clear();
	m_homebase.clear();
	m_remark.clear();
	m_fuelunit = unit_invalid;
	m_propulsion = propulsion_fixedpitch;
	m_opsrules = opsrules_auto;
	m_mrm = m_mtom = m_mlm = m_mzfm = m_vr0 = m_vr1 = m_vx0 = m_vx1 = m_vy = m_vs0 = m_vs1 = m_vno = m_vne =
		m_vref0 = m_vref1 = m_va = m_vfe = m_vgearext = m_vgearret = m_vdescent =
		m_fuelmass = m_taxifuel = m_taxifuelflow = m_maxbhp = m_contingencyfuel = std::numeric_limits<double>::quiet_NaN();
	m_freecirculation = false;
	bool haspbn(false), hasgnssflags(false);
	xmlpp::Attribute *attr;
	if ((attr = el->get_attribute("callsign")))
		m_callsign = attr->get_value();
	if ((attr = el->get_attribute("manufacturer")))
		m_manufacturer = attr->get_value();
	if ((attr = el->get_attribute("model")))
		m_model = attr->get_value();
	else if ((attr = el->get_attribute("aircrafttype")))
		m_model = attr->get_value();
	if ((attr = el->get_attribute("year")))
		m_year = attr->get_value();
	if ((attr = el->get_attribute("icaotype")))
		m_icaotype = attr->get_value();
	if ((attr = el->get_attribute("equipment")))
		set_equipment(attr->get_value());
	if ((attr = el->get_attribute("transponder")))
		set_transponder(attr->get_value());
	haspbn = (attr = el->get_attribute("pbn"));
	if (haspbn)
		set_pbn(attr->get_value());
	hasgnssflags = (attr = el->get_attribute("gnssflags"));
	if (hasgnssflags)
		set_gnssflags(attr->get_value());
	if ((attr = el->get_attribute("colormarking")))
		m_colormarking = attr->get_value();
	if ((attr = el->get_attribute("emergencyradio")))
		set_emergencyradio(attr->get_value());
	if ((attr = el->get_attribute("survival")))
		set_survival(attr->get_value());
	if ((attr = el->get_attribute("picname")))
		m_picname = attr->get_value();
	if ((attr = el->get_attribute("crewcontact")))
		m_crewcontact = attr->get_value();
	if ((attr = el->get_attribute("homebase")))
		m_homebase = attr->get_value();
	if ((attr = el->get_attribute("lifejackets")))
		set_lifejackets(attr->get_value());
	if ((attr = el->get_attribute("dinghies")))
		set_dinghies(attr->get_value());
	if ((attr = el->get_attribute("mrm")) && attr->get_value() != "")
		m_mrm = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("mtom")) && attr->get_value() != "")
		m_mtom = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("mlm")) && attr->get_value() != "")
		m_mlm = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("mzfm")) && attr->get_value() != "")
		m_mzfm = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("vr0")) && attr->get_value() != "")
		m_vr0 = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("vr1")) && attr->get_value() != "")
		m_vr1 = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("vx0")) && attr->get_value() != "")
		m_vx0 = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("vx1")) && attr->get_value() != "")
		m_vx1 = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("vy")) && attr->get_value() != "")
		m_vy = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("vs0")) && attr->get_value() != "")
		m_vs0 = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("vs1")) && attr->get_value() != "")
		m_vs1 = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("vno")) && attr->get_value() != "")
		m_vno = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("vne")) && attr->get_value() != "")
		m_vne = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("vref0")) && attr->get_value() != "")
		m_vref0 = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("vref1")) && attr->get_value() != "")
		m_vref1 = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("va")) && attr->get_value() != "")
		m_va = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("vfe")) && attr->get_value() != "")
		m_vfe = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("vgearext")) && attr->get_value() != "")
		m_vgearext = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("vgearret")) && attr->get_value() != "")
		m_vgearret = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("vdescent")) && attr->get_value() != "")
		m_vdescent = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("fuelmass")) && attr->get_value() != "")
		m_fuelmass = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("taxifuel")) && attr->get_value() != "")
		m_taxifuel = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("taxifuelflow")) && attr->get_value() != "")
		m_taxifuelflow = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("maxbhp")) && attr->get_value() != "")
		m_maxbhp = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("contingencyfuel")) && attr->get_value() != "")
		m_contingencyfuel = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("fuelunitname")) && attr->get_value() != "")
		m_fuelunit = parse_unit(attr->get_value());
	if (!((1 << m_fuelunit) & (unitmask_mass | unitmask_volume)))
		m_fuelunit = unit_usgal;
	if ((attr = el->get_attribute("remark")))
		m_remark = attr->get_value();
	attr = el->get_attribute("propulsion");
	if (!attr) {
		xmlpp::Node::NodeList nl(el->get_children("cruise"));
		for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni) {
			attr = static_cast<const xmlpp::Element *>(*ni)->get_attribute("propulsion");
			if (attr)
				break;
		}
	}
	bool hasprop(false);
	if (attr) {
		static const propulsion_t propval[] = {
			propulsion_fixedpitch,
			propulsion_constantspeed,
			propulsion_turboprop,
			propulsion_turbojet
		};
		unsigned int i;
		for (i = 0; i < sizeof(propval)/sizeof(propval[0]); ++i) {
			if (attr->get_value() == to_str(propval[i]))
				break;
		}
		if (i < sizeof(propval)/sizeof(propval[0])) {
			m_propulsion = propval[i];
			hasprop = true;
		}
	}
	if ((attr = el->get_attribute("opsrules"))) {
		for (opsrules_t r(opsrules_auto); r <= opsrules_last; r = (opsrules_t)(r + 1)) {
			if (attr->get_value() == to_str(r)) {
				m_opsrules = r;
				break;
			}
		}
	}
	if ((attr = el->get_attribute("freecirculation")))
		m_freecirculation = attr->get_value() == std::string("Y") || attr->get_value() == std::string("y") || attr->get_value() == std::string("1");
	xmlpp::Node::NodeList nl(el->get_children("wb"));
	for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni)
		m_wb.load_xml(static_cast<const xmlpp::Element *>(*ni));
	nl = el->get_children("distances");
	for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni)
		m_dist.load_xml(static_cast<const xmlpp::Element *>(*ni));
	nl = el->get_children("climb");
	for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni)
		m_climb.load_xml(static_cast<const xmlpp::Element *>(*ni));
	nl = el->get_children("descent");
	for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni)
		m_descent.load_xml(static_cast<const xmlpp::Element *>(*ni));
	nl = el->get_children("glide");
	for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni)
		m_glide.load_xml(static_cast<const xmlpp::Element *>(*ni), get_climb().get_ceiling());
	nl = el->get_children("cruise");
	for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni)
		m_cruise.load_xml(static_cast<const xmlpp::Element *>(*ni), get_maxbhp());
	nl = el->get_children("otherinfo");
	for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni) {
		const xmlpp::Element *el2(static_cast<const xmlpp::Element *>(*ni));
		Glib::ustring category, text;
		if ((attr = el2->get_attribute("category")))
			category = attr->get_value();
		if (category.empty())
			continue;
		if ((attr = el2->get_attribute("text")))
			text = attr->get_value();
		otherinfo_add(category, text);
	}
	// fix propulsion
	{
		const Cruise& cruise(get_cruise());
		if (!hasprop && cruise.has_variablepitch())
			m_propulsion = propulsion_constantspeed;
		if (hasprop && m_propulsion == propulsion_constantspeed &&
		    !cruise.has_variablepitch() && cruise.has_fixedpitch())
			m_propulsion = propulsion_fixedpitch;
	}
	get_cruise().set_mass(get_mtom());
	get_cruise().build_altmap(get_propulsion());
	get_climb().set_point_vy(get_vy());
	get_climb().set_mass(get_mtom());
	get_descent().set_point_vy(get_vdescent());
	get_descent().set_mass(get_mtom());
	get_descent().set_default(get_propulsion(), get_cruise(), get_climb());
	{
		double vbestglide(std::numeric_limits<double>::quiet_NaN());
		double glideslope(std::numeric_limits<double>::quiet_NaN());
		if ((attr = el->get_attribute("vbestglide")) && attr->get_value() != "")
			vbestglide = Glib::Ascii::strtod(attr->get_value());
		if ((attr = el->get_attribute("glideslope")) && attr->get_value() != "")
			glideslope = Glib::Ascii::strtod(attr->get_value());
		get_glide().set_default(get_mtom(), get_climb().get_ceiling(), vbestglide, glideslope);
	}
	pbn_fix_equipment();
	if (haspbn && !hasgnssflags) {
		if (get_pbn() & pbn_gnss) {
			m_gnssflags |= gnssflags_gps;
			if (get_pbn() & (pbn_d1 | pbn_d2 | pbn_o1 | pbn_o2 | pbn_s1 | pbn_s2 | pbn_t1 | pbn_t2))
				m_gnssflags |= gnssflags_sbas;
			if (get_pbn() & pbn_s2)
				m_gnssflags |= gnssflags_baroaid;
		}
	}
	// try to load from FS xml file
	nl = el->get_children("aircraft_setup_info");
	if (nl.begin() != nl.end())
		load_xml_fs(el);
	if (std::isnan(m_fuelmass))
		m_fuelmass = convert_fuel(get_fuelunit(), get_wb().get_massunit(), 1);
	autogenerate_code();
}

void Aircraft::save_xml(xmlpp::Element *el) const
{
	if (!el)
		return;
	if (m_callsign.empty())
		el->remove_attribute("callsign");
	else
		el->set_attribute("callsign", m_callsign);
	if (m_manufacturer.empty())
		el->remove_attribute("manufacturer");
	else
		el->set_attribute("manufacturer", m_manufacturer);
	if (m_model.empty())
		el->remove_attribute("model");
	else
		el->set_attribute("model", m_model);
	if (m_year.empty())
		el->remove_attribute("year");
	else
		el->set_attribute("year", m_year);
	if (m_icaotype.empty())
		el->remove_attribute("icaotype");
	else
		el->set_attribute("icaotype", m_icaotype);
	if (!(m_com & com_all) && !(m_nav & nav_all))
		el->remove_attribute("equipment");
	else
		el->set_attribute("equipment", get_equipment_string());
	if (!(m_transponder & transponder_all))
		el->remove_attribute("transponder");
	else
		el->set_attribute("transponder", get_transponder_string());
	if (m_pbn == pbn_none)
		el->remove_attribute("pbn");
	else
		el->set_attribute("pbn", get_pbn_string());
	if (m_gnssflags == gnssflags_none)
		el->remove_attribute("gnssflags");
	else
		el->set_attribute("gnssflags", get_gnssflags_string());
	if (m_colormarking.empty())
		el->remove_attribute("colormarking");
	else
		el->set_attribute("colormarking", m_colormarking);
	if (!(m_emergency & emergency_radio_all))
		el->remove_attribute("emergencyradio");
	else
		el->set_attribute("emergencyradio", get_emergencyradio_string());
	if (!(m_emergency & emergency_survival_all))
		el->remove_attribute("survival");
	else
		el->set_attribute("survival", get_survival_string());
	if (!(m_emergency & emergency_jackets_all))
		el->remove_attribute("lifejackets");
	else
		el->set_attribute("lifejackets", get_lifejackets_string());
	if (!(m_emergency & emergency_dinghies_all))
		el->remove_attribute("dinghies");
	else
		el->set_attribute("dinghies", get_dinghies_string());
	if (m_picname.empty())
		el->remove_attribute("picname");
	else
		el->set_attribute("picname", m_picname);
	if (m_crewcontact.empty())
		el->remove_attribute("crewcontact");
	else
		el->set_attribute("crewcontact", m_crewcontact);
	if (m_homebase.empty())
		el->remove_attribute("homebase");
	else
		el->set_attribute("homebase", m_homebase);
	if (std::isnan(m_mrm)) {
		el->remove_attribute("mrm");
	} else {
		std::ostringstream oss;
		oss << m_mrm;
		el->set_attribute("mrm", oss.str());
	}
	if (std::isnan(m_mtom)) {
		el->remove_attribute("mtom");
	} else {
		std::ostringstream oss;
		oss << m_mtom;
		el->set_attribute("mtom", oss.str());
	}
	if (std::isnan(m_mlm)) {
		el->remove_attribute("mlm");
	} else {
		std::ostringstream oss;
		oss << m_mlm;
		el->set_attribute("mlm", oss.str());
	}
	if (std::isnan(m_mzfm)) {
		el->remove_attribute("mzfm");
	} else {
		std::ostringstream oss;
		oss << m_mzfm;
		el->set_attribute("mzfm", oss.str());
	}
	if (std::isnan(m_vr0)) {
		el->remove_attribute("vr0");
	} else {
		std::ostringstream oss;
		oss << m_vr0;
		el->set_attribute("vr0", oss.str());
	}
	if (std::isnan(m_vr1)) {
		el->remove_attribute("vr1");
	} else {
		std::ostringstream oss;
		oss << m_vr1;
		el->set_attribute("vr1", oss.str());
	}
	if (std::isnan(m_vx0)) {
		el->remove_attribute("vx0");
	} else {
		std::ostringstream oss;
		oss << m_vx0;
		el->set_attribute("vx0", oss.str());
	}
	if (std::isnan(m_vx1)) {
		el->remove_attribute("vx1");
	} else {
		std::ostringstream oss;
		oss << m_vx1;
		el->set_attribute("vx1", oss.str());
	}
	if (std::isnan(m_vy)) {
		el->remove_attribute("vy");
	} else {
		std::ostringstream oss;
		oss << m_vy;
		el->set_attribute("vy", oss.str());
	}
	if (std::isnan(m_vs0)) {
		el->remove_attribute("vs0");
	} else {
		std::ostringstream oss;
		oss << m_vs0;
		el->set_attribute("vs0", oss.str());
	}
	if (std::isnan(m_vs1)) {
		el->remove_attribute("vs1");
	} else {
		std::ostringstream oss;
		oss << m_vs1;
		el->set_attribute("vs1", oss.str());
	}
	if (std::isnan(m_vno)) {
		el->remove_attribute("vno");
	} else {
		std::ostringstream oss;
		oss << m_vno;
		el->set_attribute("vno", oss.str());
	}
	if (std::isnan(m_vne)) {
		el->remove_attribute("vne");
	} else {
		std::ostringstream oss;
		oss << m_vne;
		el->set_attribute("vne", oss.str());
	}
	if (std::isnan(m_vref0)) {
		el->remove_attribute("vref0");
	} else {
		std::ostringstream oss;
		oss << m_vref0;
		el->set_attribute("vref0", oss.str());
	}
	if (std::isnan(m_vref1)) {
		el->remove_attribute("vref1");
	} else {
		std::ostringstream oss;
		oss << m_vref1;
		el->set_attribute("vref1", oss.str());
	}
	if (std::isnan(m_va)) {
		el->remove_attribute("va");
	} else {
		std::ostringstream oss;
		oss << m_va;
		el->set_attribute("va", oss.str());
	}
	if (std::isnan(m_vfe)) {
		el->remove_attribute("vfe");
	} else {
		std::ostringstream oss;
		oss << m_vfe;
		el->set_attribute("vfe", oss.str());
	}
	if (std::isnan(m_vgearext)) {
		el->remove_attribute("vgearext");
	} else {
		std::ostringstream oss;
		oss << m_vgearext;
		el->set_attribute("vgearext", oss.str());
	}
	if (std::isnan(m_vgearret)) {
		el->remove_attribute("vgearret");
	} else {
		std::ostringstream oss;
		oss << m_vgearret;
		el->set_attribute("vgearret", oss.str());
	}
	if (std::isnan(m_vdescent)) {
		el->remove_attribute("vdescent");
	} else {
		std::ostringstream oss;
		oss << m_vdescent;
		el->set_attribute("vdescent", oss.str());
	}
	if (m_fuelunit == unit_invalid)
		el->remove_attribute("fuelunitname");
	else
		el->set_attribute("fuelunitname", to_str(m_fuelunit));
	if (std::isnan(m_fuelmass)) {
		el->remove_attribute("fuelmass");
	} else {
		std::ostringstream oss;
		oss << m_fuelmass;
		el->set_attribute("fuelmass", oss.str());
	}
	if (std::isnan(m_taxifuel)) {
		el->remove_attribute("taxifuel");
	} else {
		std::ostringstream oss;
		oss << m_taxifuel;
		el->set_attribute("taxifuel", oss.str());
	}
	if (std::isnan(m_taxifuelflow)) {
		el->remove_attribute("taxifuelflow");
	} else {
		std::ostringstream oss;
		oss << m_taxifuelflow;
		el->set_attribute("taxifuelflow", oss.str());
	}
	if (std::isnan(m_maxbhp)) {
		el->remove_attribute("maxbhp");
	} else {
		std::ostringstream oss;
		oss << m_maxbhp;
		el->set_attribute("maxbhp", oss.str());
	}
	if (std::isnan(m_contingencyfuel)) {
		el->remove_attribute("contingencyfuel");
	} else {
		std::ostringstream oss;
		oss << m_contingencyfuel;
		el->set_attribute("contingencyfuel", oss.str());
	}
	if (m_remark.empty())
		el->remove_attribute("remark");
	else
		el->set_attribute("remark", m_remark);
	el->set_attribute("propulsion", to_str(m_propulsion));
	el->set_attribute("opsrules", to_str(m_opsrules));
	el->set_attribute("freecirculation", is_freecirculation() ? "Y" : "N");
	m_wb.save_xml(el->add_child("wb"));
	m_dist.save_xml(el->add_child("distances"));
	m_climb.save_xml(el->add_child("climb"));
	m_descent.save_xml(el->add_child("descent"));
	m_glide.save_xml(el->add_child("glide"));
	m_cruise.save_xml(el->add_child("cruise"), get_maxbhp());
	for (otherinfo_const_iterator_t oii(otherinfo_begin()), oie(otherinfo_end()); oii != oie; ++oii) {
		if (!oii->is_valid())
			continue;
		xmlpp::Element *el2(el->add_child("otherinfo"));
		el2->set_attribute("category", oii->get_category());
		if (!oii->get_text().empty())
			el2->set_attribute("text", oii->get_text());
	}
	{
		double vbestglide(get_glide().get_vbestglide());
		if (std::isnan(vbestglide)) {
			el->remove_attribute("vbestglide");
		} else {
			std::ostringstream oss;
			oss << vbestglide;
			el->set_attribute("vbestglide", oss.str());
		}
	}
	{
		double glideslope(get_glide().get_glideslope());
		if (std::isnan(glideslope)) {
			el->remove_attribute("glideslope");
		} else {
			std::ostringstream oss;
			oss << glideslope;
			el->set_attribute("glideslope", oss.str());
		}
	}
}

std::string Aircraft::get_text(const xmlpp::Node *n)
{
	const xmlpp::Element *el(static_cast<const xmlpp::Element *>(n));
	if (!el)
		return "";
	const xmlpp::TextNode *t(el->get_child_text());
	if (!t)
		return "";
	return t->get_content();
}

void Aircraft::load_xml_fs(const xmlpp::Element *el)
{
	m_propulsion = propulsion_fixedpitch;
	double speedfactor(1); // kts
	double fuelfactor(1); // USgal
	double massfactor(0.45359237); // lb->kg
	double armfactor(1); // In
	xmlpp::Node::NodeList nl(el->get_children("aircraft_setup_info"));
	for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni) {
		const xmlpp::Element *el2(static_cast<const xmlpp::Element *>(*ni));
		xmlpp::Node::NodeList nl2(el2->get_children("aircraft_type"));
		for (xmlpp::Node::NodeList::const_iterator ni2(nl2.begin()), ne2(nl2.end()); ni2 != ne2; ++ni2) {
			std::string txt(get_text(*ni2));
			if (txt.empty()) {
				continue;
			} else if (txt == "Piston fixed pitch") {
				m_propulsion = propulsion_fixedpitch;
			} else if (txt == "Piston variable pitch") {
				m_propulsion = propulsion_constantspeed;
			} else {
				std::cerr << "load_xml_fs: unknown aircraft_type " << txt << std::endl;
			}
		}
		nl2 = el2->get_children("speed_distance_units");
		for (xmlpp::Node::NodeList::const_iterator ni2(nl2.begin()), ne2(nl2.end()); ni2 != ne2; ++ni2) {
			std::string txt(get_text(*ni2));
			if (txt.empty()) {
				continue;
			} else if (txt == "Knots") {
				speedfactor = 1;
			} else if (txt == "MPH") {
				speedfactor = 0.86897624;
			} else {
				std::cerr << "load_xml_fs: unknown speed_distance_units " << txt << std::endl;
			}
		}
		nl2 = el2->get_children("fuel_units");
		for (xmlpp::Node::NodeList::const_iterator ni2(nl2.begin()), ne2(nl2.end()); ni2 != ne2; ++ni2) {
			std::string txt(get_text(*ni2));
			if (txt.empty()) {
				continue;
			} else if (txt == "Gallons") {
				fuelfactor = 1;
			} else if (txt == "Liters") {
				fuelfactor = 0.26417152;
			} else {
				std::cerr << "load_xml_fs: unknown fuel_units " << txt << std::endl;
			}
		}
		nl2 = el2->get_children("weight_units");
		for (xmlpp::Node::NodeList::const_iterator ni2(nl2.begin()), ne2(nl2.end()); ni2 != ne2; ++ni2) {
			std::string txt(get_text(*ni2));
			if (txt.empty()) {
				continue;
			} else if (txt == "Pounds") {
				massfactor = 0.45359237;
			} else if (txt == "kg") {
				massfactor = 1;
			} else {
				std::cerr << "load_xml_fs: unknown aircraft_type " << txt << std::endl;
			}
		}
		nl2 = el2->get_children("cg_units");
		for (xmlpp::Node::NodeList::const_iterator ni2(nl2.begin()), ne2(nl2.end()); ni2 != ne2; ++ni2) {
			std::string txt(get_text(*ni2));
			if (txt.empty()) {
				continue;
			} else if (txt == "Inches") {
				armfactor = 1;
			} else {
				std::cerr << "load_xml_fs: unknown cg_units " << txt << std::endl;
			}
		}
	}
	double maxfuel(0);
	{
		double servceiling(0), servceilingroc(0), servceilingff(0), slroc(0), slff(0);
		nl = el->get_children("basic_info");
		for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni) {
			const xmlpp::Element *el2(static_cast<const xmlpp::Element *>(*ni));
			xmlpp::Node::NodeList nl2(el2->get_children("make"));
			for (xmlpp::Node::NodeList::const_iterator ni2(nl2.begin()), ne2(nl2.end()); ni2 != ne2; ++ni2) {
				std::string txt(get_text(*ni2));
				if (txt.empty()) {
					continue;
				}
				m_model = txt;
			}
			nl2 = el2->get_children("model");
			for (xmlpp::Node::NodeList::const_iterator ni2(nl2.begin()), ne2(nl2.end()); ni2 != ne2; ++ni2) {
				std::string txt(get_text(*ni2));
				if (txt.empty()) {
					continue;
				}
				m_icaotype = txt;
			}
			nl2 = el2->get_children("registration");
			for (xmlpp::Node::NodeList::const_iterator ni2(nl2.begin()), ne2(nl2.end()); ni2 != ne2; ++ni2) {
				std::string txt(get_text(*ni2));
				if (txt.empty()) {
					continue;
				}
				m_callsign = txt;
			}
			nl2 = el2->get_children("color");
			for (xmlpp::Node::NodeList::const_iterator ni2(nl2.begin()), ne2(nl2.end()); ni2 != ne2; ++ni2) {
				std::string txt(get_text(*ni2));
				if (txt.empty()) {
					continue;
				}
				m_colormarking = txt;
			}
			nl2 = el2->get_children("equipment_icao");
			for (xmlpp::Node::NodeList::const_iterator ni2(nl2.begin()), ne2(nl2.end()); ni2 != ne2; ++ni2) {
				std::string txt(get_text(*ni2));
				if (txt.empty()) {
					continue;
				}
				set_equipment(txt);
			}
			nl2 = el2->get_children("fuel_capacity");
			for (xmlpp::Node::NodeList::const_iterator ni2(nl2.begin()), ne2(nl2.end()); ni2 != ne2; ++ni2) {
				std::string txt(get_text(*ni2));
				if (txt.empty()) {
					continue;
				}
				maxfuel = Glib::Ascii::strtod(txt) * fuelfactor;
			}
			nl2 = el2->get_children("service_ceiling");
			for (xmlpp::Node::NodeList::const_iterator ni2(nl2.begin()), ne2(nl2.end()); ni2 != ne2; ++ni2) {
				std::string txt(get_text(*ni2));
				if (txt.empty()) {
					continue;
				}
				servceiling = Glib::Ascii::strtod(txt);
			}
			nl2 = el2->get_children("service_ceiling_roc");
			for (xmlpp::Node::NodeList::const_iterator ni2(nl2.begin()), ne2(nl2.end()); ni2 != ne2; ++ni2) {
				std::string txt(get_text(*ni2));
				if (txt.empty()) {
					continue;
				}
				servceilingroc = Glib::Ascii::strtod(txt);
			}
			nl2 = el2->get_children("sea_level_roc");
			for (xmlpp::Node::NodeList::const_iterator ni2(nl2.begin()), ne2(nl2.end()); ni2 != ne2; ++ni2) {
				std::string txt(get_text(*ni2));
				if (txt.empty()) {
					continue;
				}
				slroc = Glib::Ascii::strtod(txt);
			}
			nl2 = el2->get_children("indicated_climb_speed");
			for (xmlpp::Node::NodeList::const_iterator ni2(nl2.begin()), ne2(nl2.end()); ni2 != ne2; ++ni2) {
				std::string txt(get_text(*ni2));
				if (txt.empty()) {
					continue;
				}
				m_vy = Glib::Ascii::strtod(txt) * speedfactor;
			}
			nl2 = el2->get_children("sea_level_climb_fuel_rate");
			for (xmlpp::Node::NodeList::const_iterator ni2(nl2.begin()), ne2(nl2.end()); ni2 != ne2; ++ni2) {
				std::string txt(get_text(*ni2));
				if (txt.empty()) {
					continue;
				}
				slff = Glib::Ascii::strtod(txt) * fuelfactor;
			}
			nl2 = el2->get_children("service_ceiling_climb_fuel_rate");
			for (xmlpp::Node::NodeList::const_iterator ni2(nl2.begin()), ne2(nl2.end()); ni2 != ne2; ++ni2) {
				std::string txt(get_text(*ni2));
				if (txt.empty()) {
					continue;
				}
				servceilingff = Glib::Ascii::strtod(txt) * fuelfactor;
			}
		}
		if (servceiling > 0 && slroc > 0) {
			ClimbDescent cd;
			cd.clear_points();
			cd.add_point(ClimbDescent::Point(0, slroc, slff));
			cd.add_point(ClimbDescent::Point(servceiling, servceilingroc, servceilingff));
			m_climb.clear();
			m_climb.add(cd);
		}
	}
	nl = el->get_children("weight_balance_info");
	for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni) {
		const xmlpp::Element *el2(static_cast<const xmlpp::Element *>(*ni));
		xmlpp::Node::NodeList nl2(el2->get_children("fuel_density"));
		for (xmlpp::Node::NodeList::const_iterator ni2(nl2.begin()), ne2(nl2.end()); ni2 != ne2; ++ni2) {
			std::string txt(get_text(*ni2));
			if (txt.empty()) {
				continue;
			}
			m_fuelmass = Glib::Ascii::strtod(txt) / fuelfactor;
		}
		nl2 = el2->get_children("main_envelope");
		for (xmlpp::Node::NodeList::const_iterator ni2(nl2.begin()), ne2(nl2.end()); ni2 != ne2; ++ni2) {
			const xmlpp::Element *el3(static_cast<const xmlpp::Element *>(*ni2));
			{
				WeightBalance::Envelope ev;
				xmlpp::Node::NodeList nl3(el3->get_children("point"));
				for (xmlpp::Node::NodeList::const_iterator ni3(nl3.begin()), ne3(nl3.end()); ni3 != ne3; ++ni3) {
					const xmlpp::Element *el4(static_cast<const xmlpp::Element *>(*ni3));
					double w, a;
					xmlpp::Attribute *attr;
					if ((attr = el4->get_attribute("weight")) && attr->get_value() != "")
						w = Glib::Ascii::strtod(attr->get_value());
					else
						continue;
					if ((attr = el4->get_attribute("arm")) && attr->get_value() != "")
						a = Glib::Ascii::strtod(attr->get_value());
					else
						continue;
					w *= massfactor;
					a *= armfactor;
					ev.add_point(a, w);
				}
				if (ev.size()) {
					double minarm, maxarm, minmass, maxmass;
					ev.get_bounds(minarm, maxarm, minmass, maxmass);
					m_mtom = maxmass;
					m_wb.add_envelope(ev);
				}
			}
		}
		nl2 = el2->get_children("moment_item_list");
		for (xmlpp::Node::NodeList::const_iterator ni2(nl2.begin()), ne2(nl2.end()); ni2 != ne2; ++ni2) {
			const xmlpp::Element *el3(static_cast<const xmlpp::Element *>(*ni2));
			xmlpp::Node::NodeList nl3(el3->get_children("moment_item"));
			for (xmlpp::Node::NodeList::const_iterator ni3(nl3.begin()), ne3(nl3.end()); ni3 != ne3; ++ni3) {
				const xmlpp::Element *el4(static_cast<const xmlpp::Element *>(*ni3));
				Glib::ustring name;
				double arm(std::numeric_limits<double>::quiet_NaN());
				double massmin(arm), massmax(arm), massdflt(arm);
				WeightBalance::Element::flags_t flg(WeightBalance::Element::flag_none);
				xmlpp::Node::NodeList nl4(el4->get_children("type"));
				for (xmlpp::Node::NodeList::const_iterator ni4(nl4.begin()), ne4(nl4.end()); ni4 != ne4; ++ni4) {
					std::string txt(get_text(*ni2));
					if (txt.empty()) {
						continue;
					}
					if (txt == "Fixed")
						flg |= WeightBalance::Element::flag_fixed;
					if (txt == "Passenger")
						flg |= WeightBalance::Element::flag_seating;
				}
				nl4 = el4->get_children("desc");
				for (xmlpp::Node::NodeList::const_iterator ni4(nl4.begin()), ne4(nl4.end()); ni4 != ne4; ++ni4) {
					std::string txt(get_text(*ni2));
					if (txt.empty()) {
						continue;
					}
					name = txt;
				}
				nl4 = el4->get_children("default_weight");
				for (xmlpp::Node::NodeList::const_iterator ni4(nl4.begin()), ne4(nl4.end()); ni4 != ne4; ++ni4) {
					std::string txt(get_text(*ni2));
					if (txt.empty()) {
						continue;
					}
				        massdflt = Glib::Ascii::strtod(txt) * massfactor;
				}
				nl4 = el4->get_children("min_weight");
				for (xmlpp::Node::NodeList::const_iterator ni4(nl4.begin()), ne4(nl4.end()); ni4 != ne4; ++ni4) {
					std::string txt(get_text(*ni2));
					if (txt.empty()) {
						continue;
					}
				        massmin = Glib::Ascii::strtod(txt) * massfactor;
				}
				nl4 = el4->get_children("max_weight");
				for (xmlpp::Node::NodeList::const_iterator ni4(nl4.begin()), ne4(nl4.end()); ni4 != ne4; ++ni4) {
					std::string txt(get_text(*ni2));
					if (txt.empty()) {
						continue;
					}
				        massmax = Glib::Ascii::strtod(txt) * massfactor;
				}
				nl4 = el4->get_children("arm");
				for (xmlpp::Node::NodeList::const_iterator ni4(nl4.begin()), ne4(nl4.end()); ni4 != ne4; ++ni4) {
					std::string txt(get_text(*ni2));
					if (txt.empty()) {
						continue;
					}
				        arm = Glib::Ascii::strtod(txt) * armfactor;
				}
				if (flg & WeightBalance::Element::flag_fixed)
					massmin = massmax = massdflt;
				if (!std::isnan(massmin) && !std::isnan(massmax) && !std::isnan(arm)) {
					WeightBalance::Element el(name, massmin, massmax, arm, 0, flg);
					el.add_auto_units(m_wb.get_massunit(), get_fuelunit(), unit_quart);
					m_wb.add_element(el);
				}
			}
		}
	}
	nl = el->get_children("performance_info");
	for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni) {
		m_cruise = Cruise();
		const xmlpp::Element *el2(static_cast<const xmlpp::Element *>(*ni));
		xmlpp::Node::NodeList nl2(el2->get_children("constant_power_model"));
		for (xmlpp::Node::NodeList::const_iterator ni2(nl2.begin()), ne2(nl2.end()); ni2 != ne2; ++ni2) {
			const xmlpp::Element *el3(static_cast<const xmlpp::Element *>(*ni2));
			xmlpp::Node::NodeList nl3(el3->get_children("power_table"));
			for (xmlpp::Node::NodeList::const_iterator ni3(nl3.begin()), ne3(nl3.end()); ni3 != ne3; ++ni3) {
				const xmlpp::Element *el4(static_cast<const xmlpp::Element *>(*ni3));
				double pwr;
				xmlpp::Attribute *attr;
				if ((attr = el4->get_attribute("power")) && attr->get_value() != "")
					pwr = Glib::Ascii::strtod(attr->get_value());
				else
					continue;
				xmlpp::Node::NodeList nl4(el4->get_children("power_entry"));
				for (xmlpp::Node::NodeList::const_iterator ni4(nl4.begin()), ne4(nl4.end()); ni4 != ne4; ++ni4) {
					const xmlpp::Element *el5(static_cast<const xmlpp::Element *>(*ni4));
					double alt, rpm;
					if ((attr = el5->get_attribute("altitude")) && attr->get_value() != "")
						alt = Glib::Ascii::strtod(attr->get_value());
					else
						continue;
					if ((attr = el5->get_attribute("rpm")) && attr->get_value() != "")
						rpm = Glib::Ascii::strtod(attr->get_value());
					else
						continue;
					Cruise::rpmmppoints_t pts;
					xmlpp::Node::NodeList nl5(el5->get_children("isa_standard"));
					for (xmlpp::Node::NodeList::const_iterator ni5(nl5.begin()), ne5(nl5.end()); ni5 != ne5; ++ni5) {
						const xmlpp::Element *el6(static_cast<const xmlpp::Element *>(*ni5));
						double mp(std::numeric_limits<double>::quiet_NaN()), fuel, tas;
						if ((attr = el6->get_attribute("mp")) && attr->get_value() != "")
							mp = Glib::Ascii::strtod(attr->get_value());
						if ((attr = el6->get_attribute("fuel")) && attr->get_value() != "")
							fuel = Glib::Ascii::strtod(attr->get_value()) * fuelfactor;
						else
							continue;
						if ((attr = el6->get_attribute("tas")) && attr->get_value() != "")
							tas = Glib::Ascii::strtod(attr->get_value()) * speedfactor;
						else
							continue;
						m_maxbhp = 200;
						pts.push_back(Cruise::PointRPMMP(alt, rpm, mp, m_maxbhp * pwr * 0.01, tas, fuel));
					}
					m_cruise.set_points(pts, get_maxbhp());
				}
			}
		}
	}
	get_cruise().set_mass(get_mtom());
	get_cruise().build_altmap(get_propulsion());
	get_climb().set_point_vy(get_vy());
	get_climb().set_mass(get_mtom());
	get_descent().set_point_vy(get_vdescent());
	get_descent().set_mass(get_mtom());
	get_descent().set_default(get_propulsion(), get_cruise(), get_climb());
	pbn_fix_equipment();
}

void Aircraft::autogenerate_code(void)
{
	if (otherinfo_find("CODE").is_valid())
		return;
	std::string cs(get_callsign());
	for (std::string::iterator i(cs.begin()), e(cs.end()); i != e; ) {
		if (std::isalpha(*i) || std::isdigit(*i)) {
			++i;
			continue;
		}
		i = cs.erase(i);
		e = cs.end();
	}
	uint32_t code(ModeSMessage::decode_registration(cs.begin(), cs.end()));
	if (code == std::numeric_limits<uint32_t>::max())
		return;
	std::ostringstream oss;
	oss << std::hex << std::setw(6) << std::setfill('0') << std::uppercase << code;
	otherinfo_add("CODE", oss.str());
}

bool Aircraft::load_file(const Glib::ustring& filename)
{
	xmlpp::DomParser p(filename);
	if (!p)
		return false;
	xmlpp::Document *doc(p.get_document());
	xmlpp::Element *el(doc->get_root_node());
	if (el->get_name() != "aircraft")
		return false;
	load_xml(el);
	return true;
}

void Aircraft::save_file(const Glib::ustring& filename) const
{
	std::unique_ptr<xmlpp::Document> doc(new xmlpp::Document());
	save_xml(doc->create_root_node("aircraft"));
	doc->write_to_file_formatted(filename);
}

bool Aircraft::load_string(const Glib::ustring& data)
{
	xmlpp::DomParser p;
	p.parse_memory(data);
	if (!p)
		return false;
	xmlpp::Document *doc(p.get_document());
	xmlpp::Element *el(doc->get_root_node());
	if (el->get_name() != "aircraft")
		return false;
	load_xml(el);
	return true;
}

Glib::ustring Aircraft::save_string(void) const
{
	std::unique_ptr<xmlpp::Document> doc(new xmlpp::Document());
	save_xml(doc->create_root_node("aircraft"));
	return doc->write_to_string_formatted();
}

#ifdef HAVE_PQXX

bool Aircraft::load_pgdb(pqxx::connection& conn, int64_t id, int& dfltmaxfl)
{
	pqxx::read_transaction w(conn);
	pqxx::result r(w.exec("select id,xml,defaultmaxfl from public.aircraft where id = " + w.quote(id) + ";"));
	for (pqxx::result::const_iterator ri(r.begin()), re(r.end()); ri != re; ++ri) {
		if ((*ri)[0].is_null() || (*ri)[1].is_null())
			continue;
		if ((*ri)[0].as<int64_t>() != id)
			continue;
		std::string code((*ri)[1].as<std::string>());
		if (code.empty())
			continue;
		if (!load_string(code))
			continue;
		if (!(*ri)[2].is_null())
			dfltmaxfl = (*ri)[2].as<int>();
		return true;
	}
	return false;
}

bool Aircraft::load_pgdb(pqxx::connection& conn, int64_t id)
{
	int dfltmaxfl(140);
	return load_pgdb(conn, id, dfltmaxfl);
}

#endif

bool Aircraft::load_opsperf(const OperationsPerformance::Aircraft& acft)
{
	if (!acft.is_valid())
		return false;
	set_callsign("ZZZZZ");
	set_year("2015");
	set_equipment("SDFGLORY");
	set_transponder("S");
	set_pbn(pbn_b2 | pbn_d2);
	set_gnssflags(gnssflags_gps | gnssflags_sbas);
	set_colormarking("");
	set_emergencyradio("E");
	set_survival("");
	set_lifejackets("");
	set_dinghies("");
	set_picname("");
	set_crewcontact("");
	set_manufacturer(acft.get_description());
	set_model(acft.get_acfttype());
	set_icaotype(""); // must be set by synonym
	set_mrm(acft.get_massmax());
	set_mtom(acft.get_massmax());
	set_mlm(acft.get_massmax());
	set_mzfm(acft.get_massmax());
	get_wb().set_units(unit_in, unit_lb);
	get_wb().clear_elements();
	get_wb().clear_envelopes();
	get_dist() = Distances();
	get_dist().clear_takeoffdists();
	get_dist().clear_ldgdists();
	get_glide().clear();
	{
		const OperationsPerformance::Aircraft::Config& cfg(acft.get_config("LD"));
		set_vs0(cfg.get_vstall());
		set_vr0(0.97 * cfg.get_vstall());
		set_vx0(1.25 * cfg.get_vstall());
		set_vref0(1.4 * cfg.get_vstall());
	}
	{
		const OperationsPerformance::Aircraft::Config& cfg(acft.get_config("CR"));
		set_vs1(cfg.get_vstall());
		set_vr1(0.97 * cfg.get_vstall());
		set_vx1(1.25 * cfg.get_vstall());
		set_vref1(1.4 * cfg.get_vstall());
		set_va(1.8 * cfg.get_vstall());
	}
	{
		const OperationsPerformance::Aircraft::Procedure& proc(acft.get_proc(1));
		set_vy(0.5 * (proc.get_climbcaslo() + proc.get_climbcashi()));
	}
	set_vno(acft.get_vmo());
	set_vne(acft.get_vmo());
	set_vfe(acft.get_vmo());
	set_vgearext(acft.get_vmo());
	set_vgearret(acft.get_vmo());
	set_fuelmass((acft.get_propulsion() == OperationsPerformance::Aircraft::propulsion_piston) ?
		     (usgal_to_liter * avgas_density * kg_to_lb) :
		     (usgal_to_liter * jeta_density * kg_to_lb));
	set_maxbhp(0.14328 * acft.get_thrustclimb(0));
	double ffmul(60 * kg_to_lb / get_fuelmass());
	{
		ClimbDescent cd("", acft.get_massref());
		cd.clear_points();
		for (int alt = 0; alt < 66000; alt += 1000) {
			OperationsPerformance::Aircraft::ComputeResult res(acft.get_massref());
			AirData<float>& ad(res);
			ad.set_qnh_temp(); // ISA
			ad.set_pressure_altitude(alt);
			if (!acft.compute(res, OperationsPerformance::Aircraft::compute_climb))
				break;
			cd.add_point(Aircraft::ClimbDescent::Point(alt, res.get_rocd(), res.get_fuelflow() * ffmul));
		}
		cd.recalculatepoly(true);
		cd.limit_ceiling(acft.get_maxalt());
		get_climb().clear();
		get_climb().add(cd);
	}
	{
		ClimbDescent cd("", acft.get_massref());
		cd.clear_points();
		for (int alt = 0; alt < 66000; alt += 1000) {
			OperationsPerformance::Aircraft::ComputeResult res(acft.get_massref());
			AirData<float>& ad(res);
			ad.set_qnh_temp(); // ISA
			ad.set_pressure_altitude(alt);
			if (!acft.compute(res, OperationsPerformance::Aircraft::compute_descent))
				break;
			cd.add_point(Aircraft::ClimbDescent::Point(alt, res.get_rocd(), res.get_fuelflow() * ffmul));
		}
		cd.recalculatepoly(true);
		cd.limit_ceiling(acft.get_maxalt());
		get_descent().clear();
		get_descent().add(cd);
	}
	switch (acft.get_propulsion()) {
	case OperationsPerformance::Aircraft::propulsion_piston:
		m_propulsion = propulsion_fixedpitch;
		break;

	case OperationsPerformance::Aircraft::propulsion_turboprop:
		m_propulsion = propulsion_turboprop;
		break;

	case OperationsPerformance::Aircraft::propulsion_jet:
		m_propulsion = propulsion_turbojet;
		break;

	default:
		m_propulsion = propulsion_fixedpitch;
		break;
	}
	{
		Cruise::Curve c("", Cruise::Curve::flags_interpolate, acft.get_massref(),
				0, std::numeric_limits<double>::quiet_NaN());
		for (int alt = 0; alt < 66000; alt += 1000) {
			OperationsPerformance::Aircraft::ComputeResult res(acft.get_massref());
			AirData<float>& ad(res);
			ad.set_qnh_temp(); // ISA
			ad.set_pressure_altitude(alt);
			if (!acft.compute(res, OperationsPerformance::Aircraft::compute_cruise))
				break;
			c.insert(Aircraft::Cruise::Point(alt, 0.14328 * res.get_thrust(),
							 res.get_tas(), res.get_fuelflow() * ffmul));
		}
		get_cruise().add_curve(c);
	}
	get_cruise().set_mass(get_mtom());
	get_cruise().build_altmap(get_propulsion());
	get_glide().set_default(get_mtom(), get_climb().get_ceiling(), 1.8 * get_vs0(), 10);
	return true;
}

#ifdef HAVE_JSONCPP

const Json::Value& Aircraft::find_json_uuid(const Json::Value& root, const std::string& uuid)
{
	static const Json::Value invalid;
	if (!root.isArray())
		return invalid;
	for (Json::ArrayIndex i = 0; i < root.size(); ++i) {
		const Json::Value& el(root[i]);
		if (!el.isMember("uuid"))
			continue;
		const Json::Value& u(el["uuid"]);
		if (u.isString() && u.asString() == uuid)
			return el;
	}
	return invalid;
}

const Json::Value& Aircraft::find_json_uuid(const Json::Value& root, const Json::Value& obj, const std::string& member)
{
	static const Json::Value invalid;
	if (!obj.isMember(member))
		return invalid;
	const Json::Value& u(obj[member]);
	if (!u.isString())
		return invalid;
	return find_json_uuid(root, u.asString());
}

bool Aircraft::load_json(const Json::Value& root)
{
	if (load_garminpilot(root))
		return true;
	return false;
}

void Aircraft::trimleft(std::string::const_iterator& txti, std::string::const_iterator txte)
{
	std::string::const_iterator txti2(txti);
	for (; txti2 != txte && isspace(*txti2); ++txti2);
	txti = txti2;
}

void Aircraft::trimright(std::string::const_iterator txti, std::string::const_iterator& txte)
{
	std::string::const_iterator txte2(txte);
	while (txti != txte2) {
		std::string::const_iterator txte3(txte2);
		--txte3;
		if (!isspace(*txte3))
			break;
		txte2 = txte3;
	}
	txte = txte2;
}

void Aircraft::trim(std::string::const_iterator& txti, std::string::const_iterator& txte)
{
	trimleft(txti, txte);
	trimright(txti, txte);
}

bool Aircraft::parsetxt(std::string& txt, unsigned int len, std::string::const_iterator& txti, std::string::const_iterator txte, bool slashsep)
{
	trimleft(txti, txte);
	txt.clear();
	while (txti != txte) {
		if (isspace(*txti) || (slashsep && *txti == '/') || *txti == '-' || *txti == '(' || *txti == ')')
			break;
		txt.push_back(*txti);
		++txti;
		if (len && txt.size() >= len)
			break;
	}
	trimleft(txti, txte);
	return len ? txt.size() == len : !txt.empty();
}

bool Aircraft::load_garminpilot(const Json::Value& root)
{
	if (!root.isMember("dataModelVersion") || !root.isMember("packageTypeVersion") || !root.isMember("type") ||
	    !root["dataModelVersion"].isIntegral() || !root["packageTypeVersion"].isIntegral() || !root["type"].isString() ||
	    root["dataModelVersion"].asInt() != 1 || root["type"].asString() != "aircraft")
		return false;
	unsigned int version(root["packageTypeVersion"].asInt());
	if (version != 1)
		return false;
	m_otherinfo.clear();
	m_callsign.clear();
	m_manufacturer.clear();
	m_model.clear();
	m_year.clear();
	m_icaotype.clear();
	m_dinghiescolor.clear();
	m_dinghiesnumber = 0;
	m_dinghiescapacity = 0;
	m_nav = nav_none;
	m_com = com_none;
	m_transponder = transponder_none;
	m_emergency = emergency_none;
	m_pbn = pbn_none;
	m_gnssflags = gnssflags_none;
	m_colormarking.clear();
	m_picname.clear();
	m_crewcontact.clear();
	m_homebase.clear();
	m_remark.clear();
	m_fuelunit = unit_invalid;
	m_propulsion = propulsion_fixedpitch;
	m_opsrules = opsrules_auto;
	m_mrm = m_mtom = m_mlm = m_mzfm = m_vr0 = m_vr1 = m_vx0 = m_vx1 = m_vy = m_vs0 = m_vs1 = m_vno = m_vne =
		m_vref0 = m_vref1 = m_va = m_vfe = m_vgearext = m_vgearret = m_vdescent =
		m_fuelmass = m_taxifuel = m_taxifuelflow = m_maxbhp = m_contingencyfuel = std::numeric_limits<double>::quiet_NaN();
	m_freecirculation = false;
	if (root.isMember("name") && root["name"].isString())
		m_model = root["name"].asString();
	if (!root.isMember("objects"))
		return true;
	const Json::Value& objs(root["objects"]);
	if (!objs.isArray() || objs.size() < 1)
		return true;
	const Json::Value& obj1(objs[0]);
	if (!obj1.isObject())
		return true;
	if (!obj1.isMember("aircrafts") || !obj1.isMember("tables") ||
	    !obj1.isMember("profiles") || !obj1.isMember("weightAndBalanceProfiles"))
		return true;
	const Json::Value& acft(obj1["aircrafts"]);
	const Json::Value& tables(obj1["tables"]);
	const Json::Value& profiles(obj1["profiles"]);
	const Json::Value& wbprof(obj1["weightAndBalanceProfiles"]);
	if (!acft.isArray() || acft.size() < 1 || !tables.isArray() || !profiles.isArray() || !wbprof.isArray())
		return true;
	const Json::Value& acft1(acft[0]);
	if (!acft1.isObject())
		return true;
	if (acft1.isMember("aircraftId") && acft1["aircraftId"].isString())
		set_callsign(acft1["aircraftId"].asString());
	if (acft1.isMember("aircraftModel") && acft1["aircraftModel"].isString())
		set_model(acft1["aircraftModel"].asString());
	if (acft1.isMember("aircraftType") && acft1["aircraftType"].isString())
		set_icaotype(acft1["aircraftType"].asString());
	if (acft1.isMember("icaoAircraftEquipment") && acft1["icaoAircraftEquipment"].isString())
		set_equipment(acft1["icaoAircraftEquipment"].asString());
	if (acft1.isMember("surveillanceEquipment") && acft1["surveillanceEquipment"].isString())
		set_transponder(acft1["surveillanceEquipment"].asString());
	if (acft1.isMember("icaoPbn") && acft1["icaoPbn"].isString())
		set_pbn(acft1["icaoPbn"].asString());
	if (acft1.isMember("aircraftColors")) {
		const Json::Value& col(acft1["aircraftColors"]);
		if (col.isArray()) {
			std::string colmark;
			for (Json::ArrayIndex i = 0; i < col.size(); ++i) {
				const Json::Value& col1(col[i]);
				if (!col1.isString() || col1.asString().empty())
					continue;
				if (!colmark.empty())
					colmark.push_back(' ');
				colmark += col1.asString();
			}
			set_colormarking(colmark);
		}
	}
	if (acft1.isMember("emergencyRadios") && acft1["emergencyRadios"].isString())
		set_emergencyradio(acft1["emergencyRadios"].asString());
	if (acft1.isMember("survivalEquipment") && acft1["survivalEquipment"].isString())
		set_survival(acft1["survivalEquipment"].asString());
	if (acft1.isMember("lifeJackets") && acft1["lifeJackets"].isString())
		set_lifejackets(acft1["lifeJackets"].asString());
	if (acft1.isMember("numberOfDingies")) {
		set_dinghies(emergency_none);
		set_dinghiesnumber(0);
		set_dinghiescapacity(0);
		set_dinghiescolor("");
		const Json::Value& nd(acft1["numberOfDingies"]);
		if (nd.isIntegral() && nd.asInt() > 0) {
			set_dinghies(emergency_dinghies);
			set_dinghiesnumber(nd.asInt());
			set_dinghiescapacity(nd.asInt());
        		if (acft1.isMember("capacityOfDingies")) {
				const Json::Value& cd(acft1["numberOfDingies"]);
				if (cd.isIntegral() && cd.asInt() >= get_dinghiesnumber())
					set_dinghiescapacity(cd.asInt());
			}
			if (acft1.isMember("coveredDingies") && acft1["coveredDingies"].isBool() && acft1["coveredDingies"].asBool())
				set_dinghies(emergency_dinghies | emergency_dinghies_cover);
			if (acft1.isMember("colorOfDingies") && acft1["colorOfDingies"].isString())
				set_dinghiescolor(acft1["colorOfDingies"].asString());
		}
	}
	if (acft1.isMember("aircraftBase") && acft1["aircraftBase"].isString())
		set_homebase(acft1["aircraftBase"].asString());
	if (acft1.isMember("propulsionType")) {
		const Json::Value& pt(acft1["propulsionType"]);
		if (pt.isString()) {
			if (pt.asString() == "CS")
				set_propulsion(propulsion_constantspeed);
		}
	}
	if (acft1.isMember("icaoAircraftOtherInfo") && acft1["icaoAircraftOtherInfo"].isString()) {
		std::string oi(acft1["icaoAircraftOtherInfo"].asString());
		std::string::const_iterator txti(oi.begin()), txte(oi.end());
                trim(txti, txte);
                std::string cat;
                while (txti != txte && *txti != '-' && *txti != ')') {
                        std::string s;
                        if (!parsetxt(s, 0, txti, txte))
				break;
                        if (txti != txte && *txti == '/') {
                                cat.swap(s);
                                ++txti;
                                continue;
                        }
                        if (cat.empty())
				break;
                        if (cat == "PBN") {
                                set_pbn(s);
				continue;
                        }
			if (!Aircraft::OtherInfo::is_category_valid(cat))
				continue;
			otherinfo_add(cat, s);
		}
	}
	if (acft1.isMember("fuelMeasurementType")) {
		const Json::Value& ft(acft1["fuelMeasurementType"]);
		if (ft.isString()) {
			if (ft.asString() == "gal")
				set_fuelunit(unit_usgal);
		}
	}
	if (acft1.isMember("maximumTakeoffWeight") && acft1["maximumTakeoffWeight"].isNumeric()) {
		double m(convert(unit_lb, get_massunit(), acft1["maximumTakeoffWeight"].asDouble()));
		set_mrm(m);
		set_mtom(m);
		set_mlm(m);
		set_mzfm(m);
	}
	if (acft1.isMember("descentSpeed") && acft1["descentSpeed"].isNumeric())
		set_vdescent(acft1["descentSpeed"].asDouble());
	if (acft1.isMember("taxiFuel") && acft1["taxiFuel"].isNumeric())
		set_taxifuel(convert_fuel(unit_usgal, get_fuelunit(), acft1["taxiFuel"].asDouble()));
	if (acft1.isMember("maximumBrakeHorsepower") && acft1["maximumBrakeHorsepower"].isNumeric())
		set_maxbhp(acft1["maximumBrakeHorsepower"].asDouble());
	// Weight & Balance
	{
		const Json::Value& wbp(find_json_uuid(wbprof, acft1, "weightAndBalanceProfileUUID"));
		if (wbp.isObject()) {
			if (wbp.isMember("maximumTakeoffWeight") && wbp["maximumTakeoffWeight"].isNumeric())
				m_mtom = convert(unit_lb, get_massunit(), wbp["maximumTakeoffWeight"].asDouble());
			if (wbp.isMember("maximumLandingWeight") && wbp["maximumLandingWeight"].isNumeric())
				m_mzfm = m_mlm = convert(unit_lb, get_massunit(), wbp["maximumLandingWeight"].asDouble());
			if (wbp.isMember("maximumRampWeight") && wbp["maximumRampWeight"].isNumeric())
				m_mrm = convert(unit_lb, get_massunit(), wbp["maximumRampWeight"].asDouble());
			get_wb().load_garminpilot(wbp, *this, version);
		}
	}
	// Profile (Climb/Cruise/Descent Tables)
	{
		const Json::Value& prof(find_json_uuid(profiles, acft1, "perfProfileUUID"));
		if (prof.isObject()) {
			if (prof.isMember("ownerAircraftTailNumber") && prof["ownerAircraftTailNumber"].isString())
				m_callsign = prof["ownerAircraftTailNumber"].asString();
			{
				ClimbDescent x("Climb", m_mtom, 0);
				const Json::Value& tbl(find_json_uuid(tables, prof, "climbTable"));
				if (tbl.isObject() && x.load_garminpilot(tbl, *this, version)) {
					//
				} else {
					x.clear_points();
					double speed(std::numeric_limits<double>::quiet_NaN());
					double fuel(std::numeric_limits<double>::quiet_NaN());
					double rate(std::numeric_limits<double>::quiet_NaN());
					double ceil(std::numeric_limits<double>::quiet_NaN());
					if (acft1.isMember("climbSpeed") && acft1["climbSpeed"].isNumeric())
						speed = acft1["climbSpeed"].asDouble();
					if (acft1.isMember("climbBurnRate") && acft1["climbBurnRate"].isNumeric())
						fuel = convert_fuel(unit_lb, get_fuelunit(), acft1["climbBurnRate"].asDouble());
					if (acft1.isMember("climbRate") && acft1["climbRate"].isNumeric())
						rate = acft1["climbRate"].asDouble();
					if (acft1.isMember("maximumCeiling") && acft1["maximumCeiling"].isNumeric())
						ceil = acft1["maximumCeiling"].asDouble();
					if (!std::isnan(speed) && !std::isnan(fuel) && !std::isnan(rate) && !std::isnan(ceil)) {
						x.add_point(ClimbDescent::Point(0, rate, fuel, speed));
						x.add_point(ClimbDescent::Point(ceil, rate, fuel, speed));
					}
				}
				if (x.has_points()) {
					x.recalculatepoly(true);
					get_climb().add(x);
				}
			}
			{
				ClimbDescent x("Descent", m_mlm, 0);
				const Json::Value& tbl(find_json_uuid(tables, prof, "descentTable"));
				if (tbl.isObject() && x.load_garminpilot(tbl, *this, version)) {
					//
				} else {
					x.clear_points();
					double speed(std::numeric_limits<double>::quiet_NaN());
					double fuel(std::numeric_limits<double>::quiet_NaN());
					double rate(std::numeric_limits<double>::quiet_NaN());
					double ceil(std::numeric_limits<double>::quiet_NaN());
					if (acft1.isMember("descentSpeed") && acft1["descentSpeed"].isNumeric())
						speed = acft1["descentSpeed"].asDouble();
					if (acft1.isMember("descentBurnRate") && acft1["descentBurnRate"].isNumeric())
						fuel = convert_fuel(unit_lb, get_fuelunit(), acft1["descentBurnRate"].asDouble());
					if (acft1.isMember("descentRate") && acft1["descentRate"].isNumeric())
						rate = acft1["descentRate"].asDouble();
					if (acft1.isMember("maximumCeiling") && acft1["maximumCeiling"].isNumeric())
						ceil = acft1["maximumCeiling"].asDouble();
					if (!std::isnan(speed) && !std::isnan(fuel) && !std::isnan(rate) && !std::isnan(ceil)) {
						x.add_point(ClimbDescent::Point(0, rate, fuel, speed));
						x.add_point(ClimbDescent::Point(ceil, rate, fuel, speed));
					}
				}
				if (x.has_points()) {
					x.recalculatepoly(true);
					get_descent().add(x);
				}
			}
			{
				Cruise::Curve x("Cruise", Cruise::Curve::flags_none, m_mtom, 0);
				const Json::Value& tbl(find_json_uuid(tables, prof, "cruiseTable"));
	        		if (tbl.isObject() && x.load_garminpilot(tbl, *this, version)) {
					//
				} else {
					x.clear_points();
					double speed(std::numeric_limits<double>::quiet_NaN());
					double fuel(std::numeric_limits<double>::quiet_NaN());
					double alt(std::numeric_limits<double>::quiet_NaN());
					double ceil(std::numeric_limits<double>::quiet_NaN());
					if (acft1.isMember("cruiseSpeed") && acft1["cruiseSpeed"].isNumeric())
						speed = acft1["cruiseSpeed"].asDouble();
					if (acft1.isMember("cruiseBurnRate") && acft1["cruiseBurnRate"].isNumeric())
						fuel = convert_fuel(unit_lb, get_fuelunit(), acft1["cruiseBurnRate"].asDouble());
					if (acft1.isMember("cruiseAltitude") && acft1["cruiseAltitude"].isNumeric())
						alt = acft1["cruiseAltitude"].asDouble();
					if (acft1.isMember("maximumCeiling") && acft1["maximumCeiling"].isNumeric())
						ceil = acft1["maximumCeiling"].asDouble();
					if (!std::isnan(ceil) && (std::isnan(alt) || ceil < alt))
						alt = ceil;
					if (!std::isnan(speed) && !std::isnan(fuel) && !std::isnan(alt)) {
						x.add_point(Cruise::Point(0, m_maxbhp, speed, fuel));
						x.add_point(Cruise::Point(alt, m_maxbhp, speed, fuel));
					}
				}
				if (!x.empty()) {
					get_cruise().add_curve(x);
				}
			}
#if 0
			"defaultPowerSetting":null,"defaultRPM":null,"defaultManifoldPressure":null
#endif
		}
	}
	if (acft1.isMember("bestGlideSpeedKnots") && acft1["bestGlideSpeedKnots"].isNumeric() &&
	    acft1.isMember("glideRatio") && acft1["glideRatio"].isNumeric()) {
		get_glide().set_default(get_mtom(), get_climb().get_ceiling(), acft1["bestGlideSpeedKnots"].asDouble(), acft1["glideRatio"].asDouble());
	}
#if 0
	m_remark.clear();
		m_vr0 = m_vr1 = m_vx0 = m_vx1 = m_vy = m_vs0 = m_vs1 = m_vno = m_vne =
		m_vref0 = m_vref1 = m_va = m_vfe = m_vgearext = m_vgearret =
		m_fuelmass =
	m_taxifuelflow =
	m_contingencyfuel = std::numeric_limits<double>::quiet_NaN();
#endif
	return true;
}

void Aircraft::save_garminpilot(Json::Value& root, unsigned int version)
{
	if (version != 1)
		throw std::runtime_error("Aircraft::save_garminpilot: currently only version 1 supported");
	root["dataModelVersion"] = 1;
	root["packageTypeVersion"] = version;
	root["type"] = "aircraft";
	{
		std::string mdl(m_manufacturer);
		if (!mdl.empty() && !m_model.empty())
			mdl += " - ";
		mdl += m_model;
		if (!mdl.empty() && !m_year.empty())
			mdl += " - ";
		mdl += m_year;
		root["name"] = mdl;
	}
	Json::Value& objs(root["objects"]);
	objs = Json::Value(Json::arrayValue);
	Json::Value& obj1(objs[(Json::ArrayIndex)0]);
	obj1 = Json::Value(Json::objectValue);
	Json::Value& tables(obj1["tables"]);
	tables = Json::Value(Json::arrayValue);
	Json::Value& profiles(obj1["profiles"]);
	profiles = Json::Value(Json::arrayValue);
	Json::Value& aircrafts(obj1["aircrafts"]);
	aircrafts = Json::Value(Json::arrayValue);
	Json::Value& wbs(obj1["weightAndBalanceProfiles"]);
	wbs = Json::Value(Json::arrayValue);
	Json::Value& acft1(aircrafts[(Json::ArrayIndex)0]);
	acft1 = Json::Value(Json::objectValue);
	Json::Value& profile1(profiles[(Json::ArrayIndex)0]);
	profile1 = Json::Value(Json::objectValue);
	profile1["uuid"] = "profiles_1";
	acft1["perfProfileUUID"] = "profiles_1";
	Json::Value& wb1(wbs[(Json::ArrayIndex)0]);
	wb1 = Json::Value(Json::objectValue);
	wb1["uuid"] = "weightAndBalanceProfiles_1";
	acft1["weightAndBalanceProfileUUID"] = "weightAndBalanceProfiles_1";
	acft1["uuid"] = "aircrafts_1";
	acft1["aircraftId"] = get_callsign();
	acft1["aircraftModel"] = (std::string)get_model();
	acft1["aircraftType"] = get_icaotype();
	acft1["icaoAircraftEquipment"] = get_equipment_string();
	acft1["domesticEquipment"] = Json::Value(Json::nullValue);
	acft1["surveillanceEquipment"] = get_transponder_string();
	acft1["icaoPbn"] = get_pbn_string();
	{
		Json::Value& cols(acft1["aircraftColors"]);
		cols = Json::Value(Json::arrayValue);
		Glib::ustring cm(get_colormarking().casefold());
		if (cm.find(Glib::ustring("white").casefold()) != Glib::ustring::npos)
			cols.append(Json::Value("W"));
	}
	acft1["emergencyRadios"] = get_emergencyradio_string();
	acft1["survivalEquipment"] = get_survival_string();
	acft1["lifeJackets"] = get_lifejackets_string();
	if (get_dinghies() & emergency_dinghies) {
		acft1["numberOfDingies"] = Json::Value(get_dinghiesnumber());
		acft1["capacityOfDingies"] = Json::Value(get_dinghiescapacity());
		acft1["coveredDingies"] = Json::Value(!!(get_dinghies() & emergency_dinghies_cover));
		acft1["colorOfDingies"] = Json::Value(get_dinghiescolor());
	} else {
		acft1["numberOfDingies"] = Json::Value(Json::nullValue);
		acft1["capacityOfDingies"] = Json::Value(Json::nullValue);
		acft1["coveredDingies"] = Json::Value(Json::nullValue);
		acft1["colorOfDingies"] = Json::Value(Json::nullValue);
	}
	acft1["aircraftBase"] = (std::string)get_homebase();
	switch (get_propulsion()) {
	default:
		acft1["propulsionType"] = Json::Value(Json::nullValue);
		break;

	case propulsion_fixedpitch:
		acft1["propulsionType"] = "FP";
		break;

	case propulsion_constantspeed:
		acft1["propulsionType"] = "CS";
		break;

	case propulsion_turboprop:
		acft1["propulsionType"] = "TP";
		break;

	case propulsion_turbojet:
		acft1["propulsionType"] = "TJ";
		break;
	}
	{
		std::string oi;
		if (get_pbn() & pbn_all)
			oi += " " + get_pbn_string();
		for (otherinfo_const_iterator_t i(otherinfo_begin()), e(otherinfo_end()); i != e; ++i) {
			if (!i->is_valid())
				continue;
			oi += " " + i->get_category() + "/" + i->get_text();
		}
		if (!oi.empty())
			acft1["icaoAircraftOtherInfo"] = oi.substr(1);
	}
	acft1["fuelMeasurementType"] = "gal";
	acft1["maximumTakeoffWeight"] = convert(get_massunit(), unit_lb, get_mtom());
	acft1["wakeTurbulenceCategory"] = std::string(1, get_wakecategory());
	{
		double vbg(get_glide().get_vbestglide());
		if (!std::isnan(vbg))
			acft1["bestGlideSpeedKnots"] = vbg;
	}
	{
		double gs(get_glide().get_glideslope());
		if (!std::isnan(gs))
			acft1["glideRatio"] = gs;
	}
	acft1["descentSpeed"] = get_vdescent();
	acft1["taxiFuel"] = convert_fuel(get_fuelunit(), unit_usgal, get_taxifuel());
	acft1["maximumBrakeHorsepower"] = get_maxbhp();
	{
		WeightBalance::Element::flags_t flags(get_wb().get_element_flags());
		flags &= WeightBalance::Element::flag_fuel;
		switch (flags) {
		case WeightBalance::Element::flag_avgas:
			acft1["fuelType"] = "100LL";
			break;

		case WeightBalance::Element::flag_jeta:
			acft1["fuelType"] = "JetA";
			break;

		case WeightBalance::Element::flag_diesel:
			acft1["fuelType"] = "diesel";
			break;

		default:
			acft1["fuelType"] = Json::Value(Json::nullValue);
			break;
		}
	}
	acft1["fuel"] = convert_fuel(get_wb().get_massunit(), unit_usgal, get_useable_fuelmass());
	acft1["fuelTankFullAmount"] = convert_fuel(get_wb().get_massunit(), unit_usgal, get_total_fuelmass());
	acft1["fuelTankTabsAmount"] = Json::Value(Json::nullValue);
	Cruise::EngineParams ep;
	ClimbDescent climb(calculate_climb(ep, get_mtom(), 0, IcaoAtmosphere<double>::std_sealevel_pressure));
	acft1["maximumCeiling"] = climb.get_ceiling();
	{
		double rate(std::numeric_limits<double>::quiet_NaN());
		double fuelflow(std::numeric_limits<double>::quiet_NaN());
		double cas(std::numeric_limits<double>::quiet_NaN());
		climb.calculate(rate, fuelflow, cas, climb.get_ceiling() * 0.5);
		acft1["climbSpeed"] = cas;
		acft1["climbBurnRate"] = convert_fuel(get_fuelunit(), unit_lb, fuelflow);
		acft1["climbRate"] = rate;
	}
	ClimbDescent descent(calculate_descent(ep, get_mlm(), 0, IcaoAtmosphere<double>::std_sealevel_pressure));
	{
		double rate(std::numeric_limits<double>::quiet_NaN());
		double fuelflow(std::numeric_limits<double>::quiet_NaN());
		double cas(std::numeric_limits<double>::quiet_NaN());
		descent.calculate(rate, fuelflow, cas, descent.get_ceiling() * 0.5);
		acft1["descentSpeed"] = cas;
		acft1["descentBurnRate"] = convert_fuel(get_fuelunit(), unit_lb, fuelflow);
		acft1["descentRate"] = rate;
	}
	{
		double tas(std::numeric_limits<double>::quiet_NaN());
		double fuelflow(std::numeric_limits<double>::quiet_NaN());
		double pa(std::floor(climb.get_ceiling() * 0.0005 + 0.5) * 1000.0);
		double mass(get_mtom());
		double isaoffs(0);
		double qnh(IcaoAtmosphere<double>::std_sealevel_pressure);
		Cruise::CruiseEngineParams ep1(ep);
		calculate_cruise(tas, fuelflow, pa, mass, isaoffs, qnh, ep1);
		acft1["cruiseBurnRate"] = convert_fuel(get_fuelunit(), unit_lb, fuelflow);
		acft1["cruiseSpeed"] = tas;
		acft1["cruiseAltitude"] = pa;
	}
	acft1["defaultPowerSetting"] = Json::Value(Json::nullValue);
	acft1["defaultRPM"] = Json::Value(Json::nullValue);
	acft1["defaultManifoldPressure"] = Json::Value(Json::nullValue);
	acft1["aircraftShareable"] = false;
	profile1["ownerAircraftTailNumber"] = get_callsign();
	profile1["name"] = Json::Value(Json::nullValue);
	profile1["sourceTemplateUUID"] = Json::Value(Json::nullValue);
	profile1["climbTable"] = "table_climb";
       	Json::Value& tblclimb(tables.append(Json::Value(Json::objectValue)));
	tblclimb["uuid"] = "table_climb";
	tblclimb["phase"] = "climb";
	tblclimb["fuelMeasureType"] = "volume";
	climb.save_garminpilot(tblclimb, *this, version);
	profile1["descentTable"] = "table_descent";
       	Json::Value& tbldescent(tables.append(Json::Value(Json::objectValue)));
	tbldescent["uuid"] = "table_descent";
	tbldescent["phase"] = "descent";
	tbldescent["fuelMeasureType"] = "volume";
	descent.save_garminpilot(tbldescent, *this, version);
	profile1["cruiseTable"] = "table_cruise";
	Json::Value& tblcruise(tables.append(Json::Value(Json::objectValue)));
	tblcruise["uuid"] = "table_cruise";
	tblcruise["fuelMeasureType"] = "volume";
	get_cruise().save_garminpilot(tblcruise, *this, version);
	get_wb().save_garminpilot(wb1, *this, version);
	wb1["maximumRampWeight"] = convert(get_massunit(), unit_lb, get_mrm());
	wb1["maximumTakeoffWeight"] = convert(get_massunit(), unit_lb, get_mtom());
	wb1["maximumLandingWeight"] = convert(get_massunit(), unit_lb, get_mlm());
	wb1["maximumZeroFuelWeight"] = convert(get_massunit(), unit_lb, get_mzfm());
}

#endif

// CODE/ in item 18 is needed if:
// - the aircraft is equipped with CPDLC
// - the aircraft is equipped with ADS-B out (FAA international flight plan filing,
//     https://www.faa.gov/about/office_org/headquarters_offices/ato/service_units/systemops/fs/res_links/media/icao_flight_plan_filing.pdf)
//
// how to handle SUR? FAA wants:
// - SUR/260B for 1090ES ADS-B out
// - SUR/282B for UAT ADS-B out
// (maybe C etc. in the future, these are RTCA DO)
bool Aircraft::is_code_needed(void) const
{
	if ((m_com & com_cpdlc) || (m_transponder & transponder_code))
		return true;
	return false;
}

Glib::ustring Aircraft::get_description(void) const
{
	Glib::ustring r(get_manufacturer());
	if (!r.empty() || !get_model().empty())
		r += " " + get_model();
	if (!get_year().empty())
		r += " (" + get_year() + ")";
	return r;
}

char Aircraft::get_wakecategory(void) const
{
	double mtom(get_mtom_kg());
	if (mtom <= 7000)
		return 'L';
	if (mtom <= 136000)
		return 'M';
	if (mtom <= 500000)
		return 'H';
	return 'J';
}

double Aircraft::get_mrm_kg(void) const
{
	return convert(get_wb().get_massunit(), unit_kg, get_mrm());
}

double Aircraft::get_mtom_kg(void) const
{
	return convert(get_wb().get_massunit(), unit_kg, get_mtom());
}

double Aircraft::get_mlm_kg(void) const
{
	return convert(get_wb().get_massunit(), unit_kg, get_mlm());
}

double Aircraft::get_mzfm_kg(void) const
{
	return convert(get_wb().get_massunit(), unit_kg, get_mzfm());
}

void Aircraft::otherinfo_add(const OtherInfo& oi)
{
	if (!oi.is_valid())
		return;
	std::pair<otherinfo_t::iterator,bool> ins(m_otherinfo.insert(oi));
	if (ins.second)
		return;
	Glib::ustring t(ins.first->get_text());
	if (!t.empty() || !oi.get_text().empty())
		t += " ";
	t += oi.get_text();
	OtherInfo oi2(ins.first->get_category(), t);
	m_otherinfo.erase(ins.first);
	m_otherinfo.insert(oi2);
}

const Aircraft::OtherInfo& Aircraft::otherinfo_find(const Glib::ustring& category) const
{
	static const OtherInfo notfound("", "");
	otherinfo_t::const_iterator i(m_otherinfo.find(OtherInfo(category)));
	if (i == m_otherinfo.end())
		return notfound;
	return *i;
}

// PBN Classification

const char Aircraft::pbn_table[24][2] = {
	'A', '1',
	'B', '1',
	'B', '2',
	'B', '3',
	'B', '4',
	'B', '5',
	'B', '6',
	'C', '1',
	'C', '2',
	'C', '3',
	'C', '4',
	'D', '1',
	'D', '2',
	'D', '3',
	'D', '4',
	'L', '1',
	'O', '1',
	'O', '2',
	'O', '3',
	'O', '4',
	'S', '1',
	'S', '2',
	'T', '1',
	'T', '2'
};

std::string Aircraft::get_pbn_string(pbn_t pbn)
{
	std::string ret;
	for (unsigned int i = 0; i < sizeof(pbn_table)/sizeof(pbn_table[0]); ++i)
		if (pbn & (pbn_t)(1U << i))
			ret += std::string(pbn_table[i], 2);
	return ret;
}

bool Aircraft::parse_pbn(pbn_t& pbn, const std::string& x)
{
	pbn = pbn_none;
	bool ret(true);
	for (std::string::const_iterator i(x.begin()), e(x.end()); i != e;) {
		char c1(*i++);
		if (i == e) {
			ret = false;
			break;
		}
		char c2(*i);
		switch (c1) {
		case 'a':
		case 'A':
			if (c2 != '1') {
				ret = false;
				break;
			}
			++i;
			pbn |= pbn_a1;
			break;

		case 'l':
		case 'L':
			if (c2 != '1') {
				ret = false;
				break;
			}
			++i;
			pbn |= pbn_l1;
			break;

		case 'b':
		case 'B':
			if (c2 < '1' || c2 > '6') {
				ret = false;
				break;
			}
			++i;
			pbn |= (pbn_t)(pbn_b1 << (c2 - '1'));
			break;

		case 'c':
		case 'C':
			if (c2 < '1' || c2 > '4') {
				ret = false;
				break;
			}
			++i;
			pbn |= (pbn_t)(pbn_c1 << (c2 - '1'));
			break;

		case 'd':
		case 'D':
			if (c2 < '1' || c2 > '4') {
				ret = false;
				break;
			}
			++i;
			pbn |= (pbn_t)(pbn_d1 << (c2 - '1'));
			break;

		case 'o':
		case 'O':
			if (c2 < '1' || c2 > '4') {
				ret = false;
				break;
			}
			++i;
			pbn |= (pbn_t)(pbn_o1 << (c2 - '1'));
			break;

		case 's':
		case 'S':
			if (c2 < '1' || c2 > '2') {
				ret = false;
				break;
			}
			++i;
			pbn |= (pbn_t)(pbn_s1 << (c2 - '1'));
			break;

		case 't':
		case 'T':
			if (c2 < '1' || c2 > '2') {
				ret = false;
				break;
			}
			++i;
			pbn |= (pbn_t)(pbn_t1 << (c2 - '1'));
			break;

		default:
			break;
		}
	}
	return ret;
}

void Aircraft::pbn_fix_equipment(nav_t& nav, pbn_t pbn)
{
	if (pbn == pbn_none)
		nav &= ~nav_pbn;
	else
		nav |= nav_pbn;
	if (pbn & pbn_gnss)
		nav |= nav_gnss;
	if (pbn & pbn_dme)
		nav |= nav_dme;
	if (pbn & pbn_dmedmeiru)
		nav |= nav_ins;
	if (pbn & pbn_vordme)
		nav |= nav_vor;
}

void Aircraft::pbn_fix_equipment(Glib::ustring& equipment, pbn_t pbn)
{
	equipment = equipment.uppercase();
	{
		Glib::ustring::size_type n(equipment.find('R'));
		if (n == Glib::ustring::npos) {
			if (pbn != pbn_none)
				equipment += 'R';
		} else {
			if (pbn == pbn_none)
				equipment.erase(n, 1);
		}
	}
	if (pbn & pbn_gnss) {
		Glib::ustring::size_type n(equipment.find('G'));
		if (n == Glib::ustring::npos)
			equipment += 'G';
	}
	if (pbn & pbn_dme) {
		Glib::ustring::size_type n(equipment.find('D'));
		if (n == Glib::ustring::npos)
			equipment += 'D';
	}
	if (pbn & pbn_dmedmeiru) {
		Glib::ustring::size_type n(equipment.find('I'));
		if (n == Glib::ustring::npos)
			equipment += 'I';
	}
	if (pbn & pbn_vordme) {
		Glib::ustring::size_type n(equipment.find_first_of("SO"));
		if (n == Glib::ustring::npos)
			equipment += 'O';
	}
}

void Aircraft::pbn_fix_equipment(std::string& equipment, pbn_t pbn)
{
	Glib::ustring e(equipment);
	pbn_fix_equipment(e, pbn);
	equipment = e;
}

std::string Aircraft::get_gnssflags_string(gnssflags_t gnssflags)
{
	static const char * const gnssflagsstr[] = {
		"GPS",
		"SBAS",
		"GLONASS",
		"GALILEO",
		"QZSS",
		"BEIDOU",
		"BAROAID",
		0
	};
	std::string ret;
	for (unsigned int i = 0; gnssflagsstr[i]; ++i) {
		if (!(gnssflags & (gnssflags_t)(1U << i)))
			continue;
		if (!ret.empty())
			ret += ",";
		ret += gnssflagsstr[i];
	}
	return ret;
}

bool Aircraft::parse_gnssflags(Aircraft::gnssflags_t& gnssflags, const std::string& x)
{
	gnssflags = gnssflags_none;
	bool ret(true);
	for (std::string::size_type i(0), n(x.size()); i < n;) {
		std::string::size_type j(x.find(',', i));
		if (j == std::string::npos)
			j = n;
		j -= i;
		if (!j) {
			++i;
			continue;
		}
		std::string xs(x, i, j);
		i += j + 1;
		for (unsigned int s = 0;; ++s) {
			std::string gs(get_gnssflags_string((gnssflags_t)(1 << s)));
			if (gs.empty()) {
				ret = false;
				break;
			}
			if (gs != xs)
				continue;
			gnssflags |= (gnssflags_t)(1 << s);
			break;
		}
	}
	return ret;
}

// NAV/COM Flags (Field 10a)

const char Aircraft::com_table[29][2] = {
	{ 'N', 0 },
	{ 'S', 0 },
	{ 'Z', 0 },
	{ 'E', '1' },
	{ 'E', '2' },
	{ 'E', '3' },
	{ 'H', 0 },
	{ 'J', '1' },
	{ 'J', '2' },
	{ 'J', '3' },
	{ 'J', '4' },
	{ 'J', '5' },
	{ 'J', '6' },
	{ 'J', '7' },
	{ 'M', '1' },
	{ 'M', '2' },
	{ 'M', '3' },
	{ 'U', 0 },
	{ 'V', 0 },
	{ 'Y', 0 },
	{ 'P', '1' },
	{ 'P', '2' },
	{ 'P', '3' },
	{ 'P', '4' },
	{ 'P', '5' },
	{ 'P', '6' },
	{ 'P', '7' },
	{ 'P', '8' },
	{ 'P', '9' }
};

const char Aircraft::nav_table[14] = {
	'A',
	'B',
	'K',
	'L',
	'C',
	'D',
	'F',
	'G',
	'I',
	'O',
	'R',
	'T',
	'W',
	'X'
};

std::string Aircraft::get_equipment_string(nav_t nav, com_t com)
{
	if (!(nav & nav_all) && !(com & com_all))
		com = com_nil;
	std::string ret;
	for (unsigned int i = 0; i < sizeof(com_table)/sizeof(com_table[0]); ++i) {
		if (!(com & (com_t)(1U << i)))
			continue;
		const char *cp(com_table[i]);
		ret.push_back(*cp++);
		if (*cp)
			ret.push_back(*cp);
	}
	for (unsigned int i = 0; i < sizeof(nav_table)/sizeof(nav_table[0]); ++i)
		if (nav & (nav_t)(1U << i))
			ret.push_back(nav_table[i]);
	return ret;
}

bool Aircraft::parse_navcom(nav_t& nav, com_t& com, const std::string& x)
{
	nav = nav_none;
	com = com_none;
	bool ret(true);
	for (std::string::const_iterator i(x.begin()), e(x.end()); i != e; ) {
		char ch(std::toupper(*i));
		++i;
		unsigned int p = 0;
		for (; p < sizeof(nav_table)/sizeof(nav_table[0]); ++p) {
			if (nav_table[p] != ch)
				continue;
			nav |= (nav_t)(1U << p);
			break;
		}
		if (p < sizeof(nav_table)/sizeof(nav_table[0]))
			continue;
		for (p = 0; p < sizeof(com_table)/sizeof(com_table[0]); ++p) {
			const char *cp(com_table[p]);
			if (ch != *cp++)
				continue;
			if (*cp) {
				if (i == e || *i != *cp)
					continue;
				++i;
			}
			com |= (com_t)(1U << p);
			break;
		}
		if (p < sizeof(com_table)/sizeof(com_table[0]))
			continue;
		ret = false;
	}
	return ret;
}

bool Aircraft::equipment_with_standard(nav_t& nav, com_t& com)
{
	if (com & Aircraft::com_standard) {
		bool ret(false);
		if (com & Aircraft::com_vhf_rtf) {
			com &= ~Aircraft::com_vhf_rtf;
			ret = true;
		}
		if (nav & (Aircraft::nav_vor | Aircraft::nav_ils)) {
			nav &= ~(Aircraft::nav_vor | Aircraft::nav_ils);
			ret = true;
		}
		return ret;
	}
	if ((Aircraft::com_vhf_rtf & ~com) || ((Aircraft::nav_vor | Aircraft::nav_ils) & ~nav))
		return false;
	com |= Aircraft::com_standard;
	com &= ~Aircraft::com_vhf_rtf;
	nav &= ~(Aircraft::nav_vor | Aircraft::nav_ils);
	return true;
}

bool Aircraft::equipment_without_standard(nav_t& nav, com_t& com)
{
	if (!(com & Aircraft::com_standard))
		return false;
	com &= ~Aircraft::com_standard;
	com |= Aircraft::com_vhf_rtf;
	nav |= Aircraft::nav_vor | Aircraft::nav_ils;
	return true;
}

void Aircraft::equipment_canonicalize(void)
{
	equipment_with_standard(m_nav, m_com);
}

// Transponder Flags (Field 10b)

const char Aircraft::transponder_table[18][2] = {
	{ 'N', 0 },
	{ 'A', 0 },
	{ 'C', 0 },
	{ 'E', 0 },
	{ 'H', 0 },
	{ 'I', 0 },
	{ 'L', 0 },
	{ 'P', 0 },
	{ 'S', 0 },
	{ 'X', 0 },
	{ 'B', '1' },
	{ 'B', '2' },
	{ 'U', '1' },
	{ 'U', '2' },
	{ 'V', '1' },
	{ 'V', '2' },
	{ 'D', '1' },
	{ 'G', '1' }
};

std::string Aircraft::get_transponder_string(transponder_t transponder)
{
	if (!(transponder & transponder_all))
		transponder = transponder_nil;
	std::string ret;
	for (unsigned int i = 0; i < sizeof(transponder_table)/sizeof(transponder_table[0]); ++i) {
		if (!(transponder & (transponder_t)(1U << i)))
			continue;
		const char *cp(transponder_table[i]);
		ret.push_back(*cp++);
		if (*cp)
			ret.push_back(*cp);
	}
	return ret;
}

bool Aircraft::parse_transponder(transponder_t& transponder, const std::string& x)
{
	transponder = transponder_none;
	bool ret(true);
	for (std::string::const_iterator i(x.begin()), e(x.end()); i != e; ) {
		char ch(std::toupper(*i));
		++i;
		unsigned int p = 0;
		for (; p < sizeof(transponder_table)/sizeof(transponder_table[0]); ++p) {
			const char *cp(transponder_table[p]);
			if (ch != *cp++)
				continue;
			if (*cp) {
				if (i == e || *i != *cp)
					continue;
				++i;
			}
			transponder |= (transponder_t)(1U << p);
			break;
		}
		if (p < sizeof(transponder_table)/sizeof(transponder_table[0]))
			continue;
		ret = false;
	}
	return ret;
}

Aircraft::emergency_t Aircraft::get_emergencyradio(void) const
{
	return m_emergency & emergency_radio_all;
}

void Aircraft::set_emergencyradio(emergency_t x)
{
	m_emergency ^= (m_emergency ^ x) & emergency_radio_all;
}

std::string Aircraft::get_emergencyradio_string(emergency_t emerg)
{
	std::string r;
	if (emerg & emergency_radio_uhf)
		r.push_back('U');
	if (emerg & emergency_radio_vhf)
		r.push_back('V');
	if (emerg & emergency_radio_elt)
		r.push_back('E');
	return r;
}

bool Aircraft::parse_emergencyradio(emergency_t& emergency, const std::string& x)
{
	bool ok(true);
	emergency_t r(emergency_none);
	for (std::string::const_iterator i(x.begin()), e(x.end()); i != e; ++i) {
		switch (*i) {
		case 'E':
		case 'e':
			r |= emergency_radio_elt;
			break;

		case 'V':
		case 'v':
			r |= emergency_radio_vhf;
			break;

		case 'U':
		case 'u':
			r |= emergency_radio_uhf;
			break;

		default:
			ok = false;
			break;
		}
	}
	emergency ^= (emergency ^ r) & emergency_radio_all;
	return ok;
}

Aircraft::emergency_t Aircraft::get_survival(void) const
{
	return m_emergency & emergency_survival_all;
}

void Aircraft::set_survival(emergency_t x)
{
	m_emergency ^= (m_emergency ^ x) & emergency_survival_all;
}

std::string Aircraft::get_survival_options(emergency_t emerg)
{
	std::string r;
	if (emerg & emergency_survival_polar)
		r.push_back('P');
	if (emerg & emergency_survival_desert)
		r.push_back('D');
	if (emerg & emergency_survival_maritime)
		r.push_back('M');
	if (emerg & emergency_survival_jungle)
		r.push_back('J');
	return r;
}

std::string Aircraft::get_survival_string(emergency_t emerg)
{
	if (!(emerg & emergency_survival))
		return std::string();
	std::string r(get_survival_options(emerg));
	if (r.empty())
		r.push_back('-');
	return r;
}

bool Aircraft::parse_survival(emergency_t& emergency, const std::string& x)
{
	bool ok(true), dash(false);
	emergency_t r(emergency_none);
	for (std::string::const_iterator i(x.begin()), e(x.end()); i != e; ++i) {
		switch (*i) {
		case 'P':
		case 'p':
			r |= emergency_survival | emergency_survival_polar;
			break;

		case 'D':
		case 'd':
			r |= emergency_survival | emergency_survival_desert;
			break;

		case 'M':
		case 'm':
			r |= emergency_survival | emergency_survival_maritime;
			break;

		case 'J':
		case 'j':
			r |= emergency_survival | emergency_survival_jungle;
			break;

		case '-':
			r |= emergency_survival;
			dash = true;
			break;

		default:
			ok = false;
			break;
		}
	}
	if (dash && (r & emergency_survival_options))
		ok = false;
	emergency ^= (emergency ^ r) & emergency_survival_all;
	return ok;
}

Aircraft::emergency_t Aircraft::get_lifejackets(void) const
{
	return m_emergency & emergency_jackets_all;
}

void Aircraft::set_lifejackets(emergency_t x)
{
	m_emergency ^= (m_emergency ^ x) & emergency_jackets_all;
}

std::string Aircraft::get_lifejackets_options(emergency_t emerg)
{
	std::string r;
	if (emerg & emergency_jackets_light)
		r.push_back('L');
	if (emerg & emergency_jackets_fluores)
		r.push_back('F');
	if (emerg & emergency_jackets_uhf)
		r.push_back('U');
	if (emerg & emergency_jackets_vhf)
		r.push_back('V');
	return r;
}

std::string Aircraft::get_lifejackets_string(emergency_t emerg)
{
	if (!(emerg & emergency_jackets))
		return std::string();
	std::string r(get_lifejackets_options(emerg));
	if (r.empty())
		r.push_back('-');
	return r;
}

bool Aircraft::parse_lifejackets(emergency_t& emergency, const std::string& x)
{
	bool ok(true), dash(false);
	emergency_t r(emergency_none);
	for (std::string::const_iterator i(x.begin()), e(x.end()); i != e; ++i) {
		switch (*i) {
		case 'L':
		case 'l':
			r |= emergency_jackets | emergency_jackets_light;
			break;

		case 'F':
		case 'f':
			r |= emergency_jackets | emergency_jackets_fluores;
			break;

		case 'U':
		case 'u':
			r |= emergency_jackets | emergency_jackets_uhf;
			break;

		case 'V':
		case 'v':
			r |= emergency_jackets | emergency_jackets_vhf;
			break;

		case '-':
			r |= emergency_jackets;
			dash = true;
			break;

		default:
			ok = false;
			break;
		}
	}
	if (dash && (r & emergency_jackets_options))
		ok = false;
	emergency ^= (emergency ^ r) & emergency_jackets_all;
	return ok;
}

Aircraft::emergency_t Aircraft::get_dinghies(void) const
{
	return m_emergency & emergency_dinghies_all;
}

void Aircraft::set_dinghies(emergency_t x)
{
	m_emergency ^= (m_emergency ^ x) & emergency_dinghies_all;
}

std::string Aircraft::get_dinghies_string(emergency_t emerg, uint16_t nr, uint16_t cap, const std::string& col)
{
	if (!(emerg & emergency_dinghies))
		return std::string();
	std::ostringstream oss;
	oss << std::setw(2) << std::setfill('0') << nr << ' ' << std::setw(3) << std::setfill('0') << cap << ' ';
	if (emerg & emergency_dinghies_cover)
		oss << "C ";
	oss << col;
	return oss.str();
}

bool Aircraft::parse_dinghies(emergency_t& emergency, uint16_t& nr, uint16_t& capacity, std::string& col, const std::string& x)
{
	emergency_t r(emergency_dinghies);
	const char *cp(x.c_str());
	while (std::isspace(*cp))
		++cp;
	if (!*cp) {
		nr = 0;
		capacity = 0;
		col.clear();
		emergency &= ~emergency_dinghies_all;
		return true;
	}
	char *cp1;
	unsigned int nr1(strtoul(cp, &cp1, 10));
	if (cp == cp1 || !nr1 || nr1 > 99) {
		nr = 0;
		capacity = 0;
		col.clear();
		emergency &= ~emergency_dinghies_all;
		return false;
	}
	cp = cp1;
	while (std::isspace(*cp))
		++cp;
	unsigned int cap1(strtoul(cp, &cp1, 10));
	if (cp == cp1 || !cap1 || cap1 > 999) {
		nr = 0;
		capacity = 0;
		col.clear();
		emergency &= ~emergency_dinghies_all;
		return false;
	}
	cp = cp1;
	while (std::isspace(*cp))
		++cp;
	if ((cp[0] == 'C' || cp[0] == 'c') && std::isspace(cp[1])) {
		r |= emergency_dinghies_cover;
		cp += 2;
		while (std::isspace(*cp))
			++cp;
	}
	emergency ^= (emergency ^ r) & emergency_dinghies_all;
	nr = nr1;
	capacity = cap1;
	col = std::string(cp);
	return true;
}

double Aircraft::get_useable_fuel(void) const
{
	double d(get_fuelmass());
	if (std::isnan(d) || d <= 0)
		return std::numeric_limits<double>::quiet_NaN();
	return get_useable_fuelmass() / d;
}

double Aircraft::get_total_fuel(void) const
{
	double d(get_fuelmass());
	if (std::isnan(d) || d <= 0)
		return std::numeric_limits<double>::quiet_NaN();
	return get_total_fuelmass() / d;
}

Aircraft::unit_t Aircraft::parse_unit(const std::string& s)
{
	static const struct unittable {
		const char *name;
		unit_t unit;
	} unittable[] = {
		// short names
		{ "m", unit_m },
		{ "cm", unit_cm },
		{ "km", unit_km },
		{ "in", unit_in },
		{ "ft", unit_ft },
		{ "nmi", unit_nmi },
		{ "kg", unit_kg },
		{ "lb", unit_lb },
		{ "l", unit_liter },
		{ "usg", unit_usgal },
		{ "qt", unit_quart },
		// long names
		{ "Liter", unit_liter },
		{ "USGal", unit_usgal },
		{ "Quart", unit_quart },
		// other
		{ "USG", unit_usgal },
		{ "usg", unit_usgal },
		{ "usgal", unit_usgal },
		// end
		{ 0, unit_invalid }
	};
	for (const struct unittable *u = unittable;; ++u)
		if (!u->name || s == u->name)
			return u->unit;
	return unit_invalid;
}

double Aircraft::convert(unit_t fromunit, unit_t tounit, double val,
			 WeightBalance::Element::flags_t flags)
{
	if (std::isnan(val) || fromunit == tounit)
		return val;
	if (((1 << fromunit) & unitmask_length) &&
	    ((1 << tounit) & unitmask_length)) {
		switch (fromunit) {
		case unit_m:
			break;

		case unit_cm:
			val *= 0.01;
			break;

		case unit_km:
			val *= 1000.0;
			break;

		case unit_in:
			val *= 0.0254;
			break;

		case unit_ft:
			val *= Point::ft_to_m_dbl;
			break;

		case unit_nmi:
			val *= 1000.0 / Point::km_to_nmi_dbl;
			break;

		default:
			return std::numeric_limits<double>::quiet_NaN();
		}
		switch (tounit) {
		case unit_m:
			break;

		case unit_cm:
			val *= 100.0;
			break;

		case unit_km:
			val *= 0.001;
			break;

		case unit_in:
			val *= 1.0 / 0.0254;
			break;

		case unit_ft:
			val *= Point::m_to_ft_dbl;
			break;

		case unit_nmi:
			val *= Point::km_to_nmi_dbl / 1000.0;
			break;

		default:
			return std::numeric_limits<double>::quiet_NaN();
		}
		return val;
	}
	if (((1 << fromunit) & (unitmask_mass | unitmask_volume)) &&
	    ((1 << tounit) & (unitmask_mass | unitmask_volume))) {
		switch (fromunit) {
		case unit_kg:
			break;

		case unit_lb:
			val *= lb_to_kg;
			break;

		case unit_liter:
			break;

		case unit_usgal:
			val *= usgal_to_liter;
			break;

		case unit_quart:
			val *= quart_to_liter;
			break;

		default:
			return std::numeric_limits<double>::quiet_NaN();
		}
		flags &= WeightBalance::Element::flag_fuel | WeightBalance::Element::flag_oil;
		if (((1 << fromunit) & unitmask_volume) &&
		    ((1 << tounit) & unitmask_mass)) {
			switch (flags) {
			case WeightBalance::Element::flag_avgas:
				val *= avgas_density;
				break;

			case WeightBalance::Element::flag_jeta:
				val *= jeta_density;
				break;

			case WeightBalance::Element::flag_diesel:
				val *= diesel_density;
				break;

			case WeightBalance::Element::flag_oil:
				val *= oil_density;
				break;

			default:
				return std::numeric_limits<double>::quiet_NaN();
			}
		}
		if (((1 << fromunit) & unitmask_mass) &&
		    ((1 << tounit) & unitmask_volume)) {
			switch (flags) {
			case WeightBalance::Element::flag_avgas:
				val *= 1.0 / avgas_density;
				break;

			case WeightBalance::Element::flag_jeta:
				val *= 1.0 / jeta_density;
				break;

			case WeightBalance::Element::flag_diesel:
				val *= 1.0 / diesel_density;
				break;

			case WeightBalance::Element::flag_oil:
				val *= 1.0 / oil_density;
				break;

			default:
				return std::numeric_limits<double>::quiet_NaN();
			}
		}
		switch (tounit) {
		case unit_kg:
			break;

		case unit_lb:
			val *= kg_to_lb;
			break;

		case unit_liter:
			break;

		case unit_usgal:
			val *= 1.0 / usgal_to_liter;
			break;

		case unit_quart:
			val *= 1.0 / quart_to_liter;
			break;

		default:
			return std::numeric_limits<double>::quiet_NaN();
		}
		return val;
	}
	return std::numeric_limits<double>::quiet_NaN();
}

double Aircraft::convert_fuel(unit_t fromunit, unit_t tounit, double val) const
{
	if (fromunit == tounit)
		return val;
	if ((((1 << fromunit) & unitmask_length) &&
	     ((1 << tounit) & unitmask_length)) ||
	    (((1 << fromunit) & unitmask_mass) &&
	     ((1 << tounit) & unitmask_mass)) ||
	    (((1 << fromunit) & unitmask_volume) &&
	     ((1 << tounit) & unitmask_volume)))
		return convert(fromunit, tounit, val, WeightBalance::Element::flag_none);
	WeightBalance::Element::flags_t flags(get_wb().get_element_flags() & WeightBalance::Element::flag_fuel);
	if (flags == WeightBalance::Element::flag_avgas ||
	    flags == WeightBalance::Element::flag_jeta ||
	    flags == WeightBalance::Element::flag_diesel)
		return convert(fromunit, tounit, val, flags);
	if (((1 << fromunit) & unitmask_volume) &&
	    ((1 << tounit) & unitmask_mass)) {
		val = convert(fromunit, get_fuelunit(), val, WeightBalance::Element::flag_none);
		val *= get_fuelmass();
		val = convert(get_wb().get_massunit(), tounit, val, WeightBalance::Element::flag_none);
		return val;
	}
	if (((1 << fromunit) & unitmask_mass) &&
	    ((1 << tounit) & unitmask_volume)) {
		val = convert(fromunit, get_wb().get_massunit(), val, WeightBalance::Element::flag_none);
		val /= get_fuelmass();
		val = convert(get_fuelunit(), tounit, val, WeightBalance::Element::flag_none);
		return val;
	}
	return convert(fromunit, tounit, val, WeightBalance::Element::flag_none);
}

bool Aircraft::add_auto_units(void)
{
	return get_wb().add_auto_units(get_fuelunit(), unit_quart);
}

const std::string& Aircraft::get_fuelname(void) const
{
	static const std::string avgas("AVGAS");
	static const std::string jeta("Jet-A");
	static const std::string diesel("Diesel");
	static const std::string unknown("Unknown");
	{
		WeightBalance::Element::flags_t flags(get_wb().get_element_flags() & WeightBalance::Element::flag_fuel);
		if (flags == WeightBalance::Element::flag_avgas)
			return avgas;
		if (flags == WeightBalance::Element::flag_jeta)
			return jeta;
		if (flags == WeightBalance::Element::flag_diesel)
			return diesel;
	}
	{
		double density(convert(unit_liter, get_fuelunit(), convert(get_wb().get_massunit(), unit_kg, get_fuelmass(), WeightBalance::Element::flag_none), WeightBalance::Element::flag_none));
		if (!std::isnan(density)) {
			double x(density * (1.0 / avgas_density));
			if (x >= 0.92 && x <= 1.02)
				return avgas;
			x = density * (1.0 / jeta_density);
			if (x >= 0.92 && x <= 1.02)
				return jeta;
		}
	}
	return unknown;
}

Aircraft::ClimbDescent Aircraft::calculate_climb(const std::string& name, double mass, double isaoffs, double qnh) const
{
	if (std::isnan(mass) || mass <= 0)
		mass = get_mtom();
	if (std::isnan(isaoffs))
		isaoffs = 0;
	if (std::isnan(qnh) || qnh <= 0)
		qnh = IcaoAtmosphere<double>::std_sealevel_pressure;
	return get_climb().calculate(name, mass, isaoffs, qnh);
}

Aircraft::ClimbDescent Aircraft::calculate_descent(const std::string& name, double mass, double isaoffs, double qnh) const
{
	if (std::isnan(mass) || mass <= 0) {
		mass = get_mlm();
		if (std::isnan(mass) || mass <= 0)
			mass = get_mtom();
	}
	if (std::isnan(isaoffs))
		isaoffs = 0;
	if (std::isnan(qnh) || qnh <= 0)
		qnh = IcaoAtmosphere<double>::std_sealevel_pressure;
	return get_descent().calculate(name, mass, isaoffs, qnh);
}

Aircraft::ClimbDescent Aircraft::calculate_glide(const std::string& name, double mass, double isaoffs, double qnh) const
{
	if (std::isnan(mass) || mass <= 0)
		mass = get_mtom();
	if (std::isnan(isaoffs))
		isaoffs = 0;
	if (std::isnan(qnh) || qnh <= 0)
		qnh = IcaoAtmosphere<double>::std_sealevel_pressure;
	return get_glide().calculate(name, mass, isaoffs, qnh);
}

Aircraft::ClimbDescent Aircraft::calculate_climb(Cruise::EngineParams& ep, double mass, double isaoffs, double qnh) const
{
	if (std::isnan(mass) || mass <= 0)
		mass = get_mtom();
	if (std::isnan(isaoffs))
		isaoffs = 0;
	if (std::isnan(qnh) || qnh <= 0)
		qnh = IcaoAtmosphere<double>::std_sealevel_pressure;
	ClimbDescent cd(get_climb().calculate(ep.get_climbname(), ep.get_name(), mass, isaoffs, qnh));
	ep.set_climbname(cd.get_name());
	return cd;
}

Aircraft::ClimbDescent Aircraft::calculate_descent(Cruise::EngineParams& ep, double mass, double isaoffs, double qnh) const
{
	if (std::isnan(mass) || mass <= 0) {
		mass = get_mlm();
		if (std::isnan(mass) || mass <= 0)
			mass = get_mtom();
	}
	if (std::isnan(isaoffs))
		isaoffs = 0;
	if (std::isnan(qnh) || qnh <= 0)
		qnh = IcaoAtmosphere<double>::std_sealevel_pressure;
	ClimbDescent cd(get_descent().calculate(ep.get_descentname(), ep.get_name(), mass, isaoffs, qnh));
	ep.set_descentname(cd.get_name());
	return cd;
}

Aircraft::ClimbDescent Aircraft::calculate_glide(std::string& name, double mass, double isaoffs, double qnh) const
{
	if (std::isnan(mass) || mass <= 0)
		mass = get_mtom();
	if (std::isnan(isaoffs))
		isaoffs = 0;
	if (std::isnan(qnh) || qnh <= 0)
		qnh = IcaoAtmosphere<double>::std_sealevel_pressure;
	ClimbDescent cd(get_glide().calculate(name, name, mass, isaoffs, qnh));
	name = cd.get_name();
	return cd;
}

void Aircraft::calculate_cruise(double& tas, double& fuelflow, double& pa, double& mass, double& isaoffs, double& qnh, Cruise::CruiseEngineParams& ep) const
{
	static constexpr bool debug = false;
	if (debug)
		ep.print(std::cerr << "calculate_cruise: >> pa " << pa << " mass " << mass << " isaoffs " << isaoffs << " qnh " << qnh << " ep ") << std::endl;
	if (ep.get_name().empty() && !(ep.get_flags() & Cruise::Curve::flags_hold) &&
	    (get_propulsion() == propulsion_constantspeed || get_propulsion() == propulsion_fixedpitch)) {
		if (std::isnan(is_constantspeed() ? ep.get_mp() : ep.get_rpm())) {
			if (std::isnan(ep.get_bhp()))
				ep.set_bhp(0.65 * get_maxbhp());
			if (is_constantspeed() && std::isnan(ep.get_rpm()))
				ep.set_rpm(2400);
		}
	}
	if (std::isnan(mass) || mass <= 0)
		mass = get_mtom();
	if (std::isnan(isaoffs))
		isaoffs = 0;
	if (std::isnan(qnh) || qnh <= 0)
		qnh = IcaoAtmosphere<double>::std_sealevel_pressure;
	get_cruise().calculate(get_propulsion(), tas, fuelflow, pa, mass, isaoffs, ep);
	if (debug)
		ep.print(std::cerr << "calculate_cruise: << tas " << tas << " ff " << fuelflow << " pa " << pa << " mass " << mass << " isaoffs " << isaoffs << " qnh " << qnh << " ep ") << std::endl;
}

void Aircraft::calculate_cruise_maxtas(double& tas, double& fuelflow, double& pa, double& mass, double& isaoffs, double& qnh, Cruise::CruiseEngineParams& ep) const
{
	static constexpr bool debug = false;
	if (debug)
		ep.print(std::cerr << "calculate_cruise_maxtas: >> pa " << pa << " mass " << mass << " isaoffs " << isaoffs << " qnh " << qnh << " ep ") << std::endl;
	if (ep.get_name().empty() && !(ep.get_flags() & Cruise::Curve::flags_hold) &&
	    (get_propulsion() == propulsion_constantspeed || get_propulsion() == propulsion_fixedpitch)) {
		if (std::isnan(is_constantspeed() ? ep.get_mp() : ep.get_rpm())) {
			if (std::isnan(ep.get_bhp()))
				ep.set_bhp(0.65 * get_maxbhp());
			if (is_constantspeed() && std::isnan(ep.get_rpm()))
				ep.set_rpm(2400);
		}
	}
	if (std::isnan(mass) || mass <= 0)
		mass = get_mtom();
	if (std::isnan(isaoffs))
		isaoffs = 0;
	if (std::isnan(qnh) || qnh <= 0)
		qnh = IcaoAtmosphere<double>::std_sealevel_pressure;
	get_cruise().calculate_maxtas(get_propulsion(), tas, fuelflow, pa, mass, isaoffs, ep);
	if (debug)
		ep.print(std::cerr << "calculate_cruise_maxtas: << tas " << tas << " ff " << fuelflow << " pa " << pa << " mass " << mass << " isaoffs " << isaoffs << " qnh " << qnh << " ep ") << std::endl;
}

time_t Aircraft::get_opsrules_holdtime(opsrules_t r, propulsion_t p)
{
	switch (r) {
	case opsrules_easacat:
		// 30min/45min holding at alternate aerodrome altitude + 1500'
		// ICAO Annex 6 Part I
		switch (p) {
		case Aircraft::propulsion_fixedpitch:
		case Aircraft::propulsion_constantspeed:
			return 45 * 60;

		default:
			return 30 * 60;
		}
		break;

	default:
	case opsrules_easanco:
		// 45min holding at "normal cruise altitude"
		// ICAO Annex 6 Part II
		return 45 * 60;
	}
}

bool Aircraft::is_opsrules_holdcruisealt(opsrules_t r)
{
	switch (r) {
	default:
	case opsrules_easacat:
		// 30min/45min holding at alternate aerodrome altitude + 1500'
		// ICAO Annex 6 Part I
		return false;

	case opsrules_easanco:
		// 45min holding at "normal cruise altitude"
		// ICAO Annex 6 Part II
		return true;
	}
}

Aircraft::CheckErrors Aircraft::check(void) const
{
	double minmass(get_wb().get_min_mass());
	CheckErrors r(m_climb.check(minmass, get_mtom(), get_vs0()));
	r.add(m_descent.check(minmass, get_mtom(), get_vs0()));
	r.add(m_cruise.check(minmass, get_mtom()));
	return r;
}

Aircraft::Endurance::Endurance(void)
	: m_dist(std::numeric_limits<double>::quiet_NaN()),
	  m_fuel(std::numeric_limits<double>::quiet_NaN()),
	  m_tas(std::numeric_limits<double>::quiet_NaN()),
	  m_fuelflow(std::numeric_limits<double>::quiet_NaN()),
	  m_pa(std::numeric_limits<double>::quiet_NaN()),
	  m_tkoffelev(std::numeric_limits<double>::quiet_NaN()),
	  m_endurance(0)
{
}

class Aircraft::EnduranceW : public Aircraft::Endurance {
public:
	void set_engineparams(const Cruise::CruiseEngineParams& ep) { m_ep = ep; }
	void set_dist(double x) { m_dist = x; }
	void set_fuel(double x) { m_fuel = x; }
	void set_tas(double x) { m_tas = x; }
	void set_fuelflow(double x) { m_fuelflow = x; }
	void set_pa(double x) { m_pa = x; }
	void set_tkoffelev(double x) { m_tkoffelev = x; }
	void set_endurance(time_t x) { m_endurance = x; }
	void add_dist(double x) { m_dist += x; }
	void add_endurance(time_t x) { m_endurance += x; }
};

Aircraft::Endurance Aircraft::compute_endurance(double pa, const Cruise::CruiseEngineParams& ep, double tkoffelev)
{
	EnduranceW e;
	double fuel(get_useable_fuel());
	e.set_fuel(fuel);
	if (std::isnan(fuel) || fuel <= 0)
		return e;
	if (std::isnan(pa))
		return e;
	if (pa < 0)
		pa = 0;
	double mass(get_mtom()), isaoffs(0), qnh(IcaoAtmosphere<double>::std_sealevel_pressure);
	ClimbDescent climb(calculate_climb("", mass, isaoffs, qnh));
	if (pa > climb.get_ceiling())
		pa = climb.get_ceiling();
	double tas(0), fuelflow(0);
	Cruise::CruiseEngineParams ep1(ep);
	calculate_cruise(tas, fuelflow, pa, mass, isaoffs, qnh, ep1);
	e.set_tas(tas);
	e.set_fuelflow(fuelflow);
	e.set_engineparams(ep1);
	e.set_pa(pa);
	if (std::isnan(pa) || std::isnan(fuelflow) || fuelflow <= 0)
		return e;
	e.set_dist(0);
	e.set_endurance(0);
	if (!std::isnan(tkoffelev)) {
		double t0(climb.altitude_to_time(tkoffelev));
		double t1(climb.altitude_to_time(pa));
		if (!std::isnan(t0) && !std::isnan(t1) && t1 >= t0) {
			e.set_tkoffelev(climb.time_to_altitude(t0));
			e.add_dist(climb.time_to_distance(t1) - climb.time_to_distance(t0));
			e.add_endurance(Point::round<time_t,double>(t1 - t0));
			fuel -= climb.time_to_fuel(t1) - climb.time_to_fuel(t0);
		}
	}
	{
		double t(fuel / fuelflow);
		e.add_endurance(Point::round<time_t,double>(t * 3600.0));
		e.add_dist(t * tas);
	}
	return e;
}

std::string Aircraft::to_lua(void) const
{
	std::ostringstream oss;
	oss << "otherinfo = { ";
	{
		bool subseq(false);
		for (otherinfo_const_iterator_t i(otherinfo_begin()), e(otherinfo_end()); i != e; ++i) {
			if (subseq)
				oss << ", ";
			subseq = true;
			oss << i->get_category() << " = " << to_luastring(i->get_text());
		}
	}
	oss << " }, callsign = " << to_luastring(get_callsign())
	    << ", manufacturer = " << to_luastring(get_manufacturer())
	    << ", model = " << to_luastring(get_model())
	    << ", year = " << to_luastring(get_year())
	    << ", description = " << to_luastring(get_description())
	    << ", typeclass = " << to_luastring(get_aircrafttypeclass())
	    << ", icaotype = " << to_luastring(get_icaotype())
	    << ", equipment = " << to_luastring(get_equipment_string())
	    << ", rvsm = " << to_luastring(is_rvsm())
	    << ", mnps = " << to_luastring(is_mnps())
	    << ", radio833 = " << to_luastring(is_833())
	    << ", code_needed = " << to_luastring(is_code_needed())
	    << ", transponder = " << to_luastring(get_transponder_string())
	    << ", pbn = " << to_luastring(get_pbn_string())
	    << ", gnssflags = " << to_luastring(get_gnssflags_string())
	    << ", colormarking = " << to_luastring(get_colormarking())
	    << ", emergencyradio = " << to_luastring(get_emergencyradio_string())
	    << ", survival = " << to_luastring(get_survival_string())
	    << ", lifejackets = " << to_luastring(get_lifejackets_string())
	    << ", dinghies = " << to_luastring(get_dinghies_string())
	    << ", picname = " << to_luastring(get_picname())
	    << ", crewcontact = " << to_luastring(get_crewcontact())
	    << ", homebase = " << to_luastring(get_homebase())
	    << ", wakecategory = '" << get_wakecategory()
	    << "', mrm = " << to_luastring(get_mrm())
	    << ", mtom = " << to_luastring(get_mtom())
	    << ", mlm = " << to_luastring(get_mlm())
	    << ", mzfm = " << to_luastring(get_mzfm())
	    << ", mrm_kg = " << to_luastring(get_mrm_kg())
	    << ", mtom_kg = " << to_luastring(get_mtom_kg())
	    << ", mlm_kg = " << to_luastring(get_mlm_kg())
	    << ", mzfm_kg = " << to_luastring(get_mzfm_kg())
	    << ", vr0 = " << to_luastring(get_vr0())
	    << ", vr1 = " << to_luastring(get_vr1())
	    << ", vx0 = " << to_luastring(get_vx0())
	    << ", vx1 = " << to_luastring(get_vx1())
	    << ", vy = " << to_luastring(get_vy())
	    << ", vs0 = " << to_luastring(get_vs0())
	    << ", vs1 = " << to_luastring(get_vs1())
	    << ", vno = " << to_luastring(get_vno())
	    << ", vne = " << to_luastring(get_vne())
	    << ", vref0 = " << to_luastring(get_vref0())
	    << ", vref1 = " << to_luastring(get_vref1())
	    << ", va = " << to_luastring(get_va())
	    << ", vfe = " << to_luastring(get_vfe())
	    << ", vgearext = " << to_luastring(get_vgearext())
	    << ", vgearret = " << to_luastring(get_vgearret())
	    << ", vdescent = " << to_luastring(get_vdescent())
	    << ", fuelmass = " << to_luastring(get_fuelmass())
	    << ", fuelunit = " << to_luastring(to_str(get_fuelunit()))
	    << ", taxifuel = " << to_luastring(get_taxifuel())
	    << ", taxifuelflow = " << to_luastring(get_taxifuelflow())
	    << ", maxbhp = " << to_luastring(get_maxbhp())
	    << ", contingencyfuel = " << to_luastring(get_contingencyfuel())
	    << ", remark = " << to_luastring(get_remark())
	    << ", propulsion = " << to_luastring(to_str(get_propulsion()))
	    << ", constantspeed = " << to_luastring(is_constantspeed())
	    << ", freecirculation = " << to_luastring(is_freecirculation());
	return oss.str();
}

// Aircraft Type Classification "Database", ICAO Doc 8643
// First character:
// L = Landplane
// S = Seaplane
// A = Amphibian
// H = Helicopter
// G = Gyrocopter
// T = Tilt-wing aircraft
// Second character:
// 1,2,3,4,6, 8 = the number of engines
// C = 2 coupled engines driving 1 propeller
// Third character:
// P = Piston Engine
// T = Turbo Engine
// J = Jet Engine
// E = Electric Fan
// R = Rocket
// Fourth Character:
// Approach Category
// Fifth Character:
// Wake Turbulence Category

// get this data out of Achim's icao8643.db:
// select substr(icao||'    ',1,4)||substr(description,1,1)||enginecount||substr(enginetype,1,1)||'-'||wakecategory from types order by icao;

#include "icao8643.h"

#if 0

std::string Aircraft::get_aircrafttypeclass(const std::string& acfttype)
{
	std::string r("L1---");
	std::string at(acfttype);
	at.resize(4, ' ');
	AircraftTypeClassDbCompare cmp;
	const aircraft_type_class_t *it(std::lower_bound(&aircraft_type_class_db[0],
							 &aircraft_type_class_db[sizeof(aircraft_type_class_db)/sizeof(aircraft_type_class_db[0])],
							 at.c_str(), cmp));
	if (it != &aircraft_type_class_db[sizeof(aircraft_type_class_db)/sizeof(aircraft_type_class_db[0])] &&
	    !cmp(*it, acfttype.c_str()) && !cmp(acfttype.c_str(), *it))
		r = (*it) + 4;
	if (r.size() != 5)
		r.resize(5, '-');
	return r;
}

#else

std::string Aircraft::get_aircrafttypeclass(const std::string& acfttype)
{
	std::string r("L1---");
	std::string at(acfttype);
	at.resize(4, ' ');
	AircraftTypeClassDbCompare cmp;
	std::pair<const aircraft_type_class_t *, const aircraft_type_class_t *> p(std::equal_range(&aircraft_type_class_db[0],
												   &aircraft_type_class_db[sizeof(aircraft_type_class_db)/sizeof(aircraft_type_class_db[0])],
												   at.c_str(), cmp));
	if (p.first != p.second)
		r = (*p.first) + 4;
	if (r.size() != 5)
		r.resize(5, '-');
	return r;
}

#endif

std::string Aircraft::get_aircrafttypeclass(void) const
{
	std::string r(get_aircrafttypeclass(get_icaotype()));
	if (r.size() != 5)
		r.resize(5, '-');
	if (r[2] == '-') {
		switch (get_propulsion()) {
		default:
			r[2] = 'P';
			break;

		case propulsion_turboprop:
			r[2] = 'T';
			break;

		case propulsion_turbojet:
			r[2] = 'J';
			break;
		}
	}
	if (r[3] == '-') {
		static const double apprcat[] = { 90 / 1.3, 120 / 1.3, 140 / 1.3, 165 / 1.3 };
		unsigned int i(0);
		for (; i < sizeof(apprcat)/sizeof(apprcat[0]); ++i)
			if (get_vs0() <= apprcat[i])
				break;
		r[3] = 'A' + i;
	}
	if (r[4] == '-')
		r[4] = get_wakecategory();
	return r;
}

void Aircraft::check_aircraft_type_class(void)
{
	for (unsigned int i = 1; i < sizeof(aircraft_type_class_db)/sizeof(aircraft_type_class_db[0]); ++i) {
		AircraftTypeClassDbCompare cmp;
		if (cmp(aircraft_type_class_db[i - 1], aircraft_type_class_db[i]))
			continue;
		std::ostringstream oss;
		oss << "Aircraft Type/Class Database not ascending: " << aircraft_type_class_db[i - 1] << ", " << aircraft_type_class_db[i];
		std::cerr << oss.str() << std::endl;
		throw std::runtime_error(oss.str());
	}
}

#ifdef HAVE_ICONV

bool Aircraft::to_ascii_iconv(std::string& r, const Glib::ustring& x)
{
	r.clear();
	iconv_t ic(iconv_open("US-ASCII//TRANSLIT", "UTF-8"));
	if (ic == (iconv_t)-1)
		return false;
	size_t insz(x.bytes());
	char *inbuf(const_cast<char *>(x.data()));
	size_t outsz(insz * 4);
	size_t outbufsz(outsz);
	char outbuf[outsz];
	char *outbufp(outbuf);
#ifdef WINICONV_CONST
	size_t sz(iconv(ic, const_cast<WINICONV_CONST char **>(&inbuf), &insz, &outbufp, &outsz));
#else
	size_t sz(iconv(ic, &inbuf, &insz, &outbufp, &outsz));
#endif
	iconv_close(ic);
	if (outsz < outbufsz) {
		r = std::string(outbuf, outbuf + (outbufsz - outsz));
		return true;
	}
	return false;
}

#else

bool Aircraft::to_ascii_iconv(std::string& r, const Glib::ustring& x)
{
	return false;
}

#endif

const Aircraft::UnicodeMapping Aircraft::unicodemapping[] = {
	{ 0x00C0, "A" },
	{ 0x00C1, "A" },
	{ 0x00C2, "A" },
	{ 0x00C3, "A" },
	{ 0x00C4, "AE" },
	{ 0x00C5, "A" },
	{ 0x00C6, "AE" },
	{ 0x00C7, "C" },
	{ 0x00C8, "E" },
	{ 0x00C9, "E" },
	{ 0x00CA, "E" },
	{ 0x00CB, "E" },
	{ 0x00CC, "I" },
	{ 0x00CD, "I" },
	{ 0x00CE, "I" },
	{ 0x00CF, "I" },
	{ 0x00D0, "D" },
	{ 0x00D1, "N" },
	{ 0x00D2, "O" },
	{ 0x00D3, "O" },
	{ 0x00D4, "O" },
	{ 0x00D5, "O" },
	{ 0x00D6, "OE" },
	{ 0x00D8, "O" },
	{ 0x00D9, "U" },
	{ 0x00DA, "U" },
	{ 0x00DB, "U" },
	{ 0x00DC, "UE" },
	{ 0x00DD, "Y" },
	{ 0x00DF, "SS" },
	{ 0x00E0, "a" },
	{ 0x00E1, "a" },
	{ 0x00E2, "a" },
	{ 0x00E3, "a" },
	{ 0x00E4, "ae" },
	{ 0x00E5, "a" },
	{ 0x00E6, "ae" },
	{ 0x00E7, "c" },
	{ 0x00E8, "e" },
	{ 0x00E9, "e" },
	{ 0x00EA, "e" },
	{ 0x00EB, "e" },
	{ 0x00EC, "i" },
	{ 0x00ED, "i" },
	{ 0x00EE, "i" },
	{ 0x00EF, "i" },
	{ 0x00F1, "n" },
	{ 0x00F2, "o" },
	{ 0x00F3, "o" },
	{ 0x00F4, "o" },
	{ 0x00F5, "o" },
	{ 0x00F6, "oe" },
	{ 0x00F8, "o" },
	{ 0x00F9, "u" },
	{ 0x00FA, "u" },
	{ 0x00FB, "u" },
	{ 0x00FC, "ue" },
	{ 0x00FD, "y" },
	{ 0x00FF, "y" },
	{ 0x0100, "A" },
	{ 0x0101, "a" },
	{ 0x0102, "A" },
	{ 0x0103, "a" },
	{ 0x0104, "A" },
	{ 0x0105, "a" },
	{ 0x0106, "C" },
	{ 0x0107, "c" },
	{ 0x0108, "C" },
	{ 0x0109, "c" },
	{ 0x010A, "C" },
	{ 0x010B, "c" },
	{ 0x010C, "C" },
	{ 0x010D, "c" },
	{ 0x010E, "D" },
	{ 0x010F, "d" },
	{ 0x0110, "D" },
	{ 0x0111, "d" },
	{ 0x0112, "E" },
	{ 0x0113, "e" },
	{ 0x0114, "E" },
	{ 0x0115, "e" },
	{ 0x0116, "E" },
	{ 0x0117, "e" },
	{ 0x0118, "E" },
	{ 0x0119, "e" },
	{ 0x011A, "E" },
	{ 0x011B, "e" },
	{ 0x011C, "G" },
	{ 0x011D, "g" },
	{ 0x011E, "G" },
	{ 0x011F, "g" },
	{ 0x0120, "G" },
	{ 0x0121, "g" },
	{ 0x0122, "G" },
	{ 0x0123, "g" },
	{ 0x0124, "H" },
	{ 0x0125, "h" },
	{ 0x0126, "H" },
	{ 0x0127, "h" },
	{ 0x0128, "I" },
	{ 0x0129, "i" },
	{ 0x012A, "I" },
	{ 0x012B, "i" },
	{ 0x012C, "I" },
	{ 0x012D, "i" },
	{ 0x012E, "I" },
	{ 0x012F, "i" },
	{ 0x0130, "I" },
	{ 0x0131, "i" },
	{ 0x0132, "IJ" },
	{ 0x0133, "ij" },
	{ 0x0134, "J" },
	{ 0x0135, "j" },
	{ 0x0136, "K" },
	{ 0x0137, "k" },
	{ 0x0139, "L" },
	{ 0x013A, "l" },
	{ 0x013B, "L" },
	{ 0x013C, "l" },
	{ 0x013D, "L" },
	{ 0x013E, "l" },
	{ 0x013F, "L" },
	{ 0x0140, "l" },
	{ 0x0141, "L" },
	{ 0x0142, "l" },
	{ 0x0143, "N" },
	{ 0x0144, "n" },
	{ 0x0145, "N" },
	{ 0x0146, "n" },
	{ 0x0147, "N" },
	{ 0x0148, "n" },
	{ 0x0149, "'n" },
	{ 0x014C, "O" },
	{ 0x014D, "o" },
	{ 0x014E, "O" },
	{ 0x014F, "o" },
	{ 0x0150, "O" },
	{ 0x0151, "o" },
	{ 0x0152, "OE" },
	{ 0x0153, "oe" },
	{ 0x0154, "R" },
	{ 0x0155, "r" },
	{ 0x0156, "R" },
	{ 0x0157, "r" },
	{ 0x0158, "R" },
	{ 0x0159, "r" },
	{ 0x015A, "S" },
	{ 0x015B, "s" },
	{ 0x015C, "S" },
	{ 0x015D, "s" },
	{ 0x015E, "S" },
	{ 0x015F, "s" },
	{ 0x0160, "S" },
	{ 0x0161, "s" },
	{ 0x0162, "T" },
	{ 0x0163, "t" },
	{ 0x0164, "T" },
	{ 0x0165, "t" },
	{ 0x0166, "T" },
	{ 0x0167, "t" },
	{ 0x0168, "U" },
	{ 0x0169, "u" },
	{ 0x016A, "U" },
	{ 0x016B, "u" },
	{ 0x016C, "U" },
	{ 0x016D, "u" },
	{ 0x016E, "U" },
	{ 0x016F, "u" },
	{ 0x0170, "U" },
	{ 0x0171, "u" },
	{ 0x0172, "U" },
	{ 0x0173, "u" },
	{ 0x0174, "W" },
	{ 0x0175, "w" },
	{ 0x0176, "Y" },
	{ 0x0177, "y" },
	{ 0x0178, "Y" },
	{ 0x0179, "Z" },
	{ 0x017A, "z" },
	{ 0x017B, "Z" },
	{ 0x017C, "z" },
	{ 0x017D, "Z" },
	{ 0x017E, "z" },
	{ 0x0180, "b" },
	{ 0x0181, "B" },
	{ 0x0182, "b" },
	{ 0x0183, "B" },
	{ 0x0189, "D" },
	{ 0x018A, "D" },
	{ 0x018B, "D" },
	{ 0x018C, "d" },
	{ 0x01C4, "DZ" },
	{ 0x01C5, "Dz" },
	{ 0x01C6, "dz" },
	{ 0x01C7, "LJ" },
	{ 0x01C8, "Lj" },
	{ 0x01C9, "lj" },
	{ 0x01CA, "NJ" },
	{ 0x01CB, "Nj" },
	{ 0x01CC, "nj" },
	{ 0x01CD, "A" },
	{ 0x01CE, "a" },
	{ 0x01CF, "I" },
	{ 0x01D0, "i" },
	{ 0x01D1, "O" },
	{ 0x01D2, "o" },
	{ 0x01D3, "U" },
	{ 0x01D4, "u" },
	{ 0x01D5, "U" },
	{ 0x01D6, "u" },
	{ 0x01D7, "U" },
	{ 0x01D8, "u" },
	{ 0x01D9, "U" },
	{ 0x01DA, "u" },
	{ 0x01DB, "U" },
	{ 0x01DC, "u" },
	{ 0x01DE, "A" },
	{ 0x01DF, "a" },
	{ 0x01E0, "A" },
	{ 0x01E1, "a" },
	{ 0x01E2, "AE" },
	{ 0x01E3, "ae" },
	{ 0x01E4, "G" },
	{ 0x01E5, "g" },
	{ 0x01E6, "G" },
	{ 0x01E7, "g" },
	{ 0x01E8, "K" },
	{ 0x01E9, "k" },
	{ 0x01EA, "O" },
	{ 0x01EB, "o" },
	{ 0x01EC, "O" },
	{ 0x01ED, "o" },
	{ 0x01F0, "j" },
	{ 0x01F1, "DZ" },
	{ 0x01F2, "Dz" },
	{ 0x01F3, "dz" },
	{ 0x01F4, "G" },
	{ 0x01F5, "g" },
	{ 0x01F8, "N" },
	{ 0x01F9, "n" },
	{ 0x01FA, "A" },
	{ 0x01FB, "a" },
	{ 0x01FC, "AE" },
	{ 0x01FD, "ae" },
	{ 0x01FE, "O" },
	{ 0x01FF, "o" },
	{ 0x0200, "A" },
	{ 0x0201, "a" },
	{ 0x0202, "A" },
	{ 0x0203, "a" },
	{ 0x0204, "E" },
	{ 0x0205, "e" },
	{ 0x0206, "E" },
	{ 0x0207, "e" },
	{ 0x0208, "I" },
	{ 0x0209, "i" },
	{ 0x020A, "I" },
	{ 0x020B, "i" },
	{ 0x020C, "O" },
	{ 0x020D, "o" },
	{ 0x020E, "O" },
	{ 0x020F, "o" },
	{ 0x0210, "R" },
	{ 0x0211, "r" },
	{ 0x0212, "R" },
	{ 0x0213, "r" },
	{ 0x0214, "U" },
	{ 0x0215, "u" },
	{ 0x0216, "U" },
	{ 0x0217, "u" },
	{ 0x0218, "S" },
	{ 0x0219, "s" },
	{ 0x021A, "T" },
	{ 0x021B, "t" },
	{ 0x021E, "H" },
	{ 0x021F, "h" },
	{ 0x0224, "Z" },
	{ 0x0225, "z" },
	{ 0x0226, "A" },
	{ 0x0227, "a" },
	{ 0x0228, "E" },
	{ 0x0229, "e" },
	{ 0x022A, "O" },
	{ 0x022B, "o" },
	{ 0x022C, "O" },
	{ 0x022D, "o" },
	{ 0x022E, "O" },
	{ 0x022F, "o" },
	{ 0x0230, "O" },
	{ 0x0231, "o" },
	{ 0x0232, "Y" },
	{ 0x0233, "y" },
	{ 0x023A, "A" },
	{ 0x023B, "C" },
	{ 0x023C, "c" },
	{ 0x023D, "L" },
	{ 0x023E, "T" },
	{ 0x023F, "s" },
	{ 0x0243, "B" },
	{ 0x0244, "U" },
	{ 0x0246, "E" },
	{ 0x0247, "e" },
	{ 0x0248, "J" },
	{ 0x0249, "j" },
	{ 0x024C, "R" },
	{ 0x024D, "r" },
	{ 0x024E, "Y" },
	{ 0x024F, "y" },
	{ 0x0260, "g" },
	{ 0x0260, "g" },
	{ 0x0260, "G" },
	{ 0x0266, "h" },
	{ 0x0268, "i" },
	{ 0x026A, "I" },
	{ 0x026B, "l" },
	{ 0x026C, "l" },
	{ 0x026D, "l" },
	{ 0x0274, "N" },
	{ 0x0276, "OE" },
	{ 0x0280, "R" },
	{ 0x0284, "j" },
	{ 0x028F, "Y" },
	{ 0x0290, "z" },
	{ 0x0291, "z" },
	{ 0x0299, "B" },
	{ 0x029B, "G" },
	{ 0x029C, "H" },
	{ 0x029D, "j" },
	{ 0x029F, "L" },
	{ 0x02A3, "dz" },
	{ 0x02A5, "dz" },
	{ 0x02A6, "ts" },
	{ 0x02A8, "tc" },
	{ 0x02AA, "ls" },
	{ 0x02AB, "lz" }
};

std::string Aircraft::to_ascii(const Glib::ustring& x)
{

	std::string r;
	bool tryiconv(false);
	for (Glib::ustring::const_iterator i(x.begin()), e(x.end()); i != e; ++i) {
		if (*i >= ' ' && *i < 127) {
			r.push_back(*i);
			continue;
		}
		std::pair<const UnicodeMapping *, const UnicodeMapping *> p(std::equal_range(&unicodemapping[0],
											     &unicodemapping[sizeof(unicodemapping)/sizeof(unicodemapping[0])],
											     *i, UnicodeMappingCompare()));
		if (p.first != p.second) {
			r += p.first->txt;
			continue;
		}
		tryiconv = true;
	}
	if (tryiconv) {
		std::string r2;
		if (to_ascii_iconv(r2, x))
			return r2;
	}
	return r;
}

Aircraft::recompute_t Aircraft::recompute(void)
{
	recompute_t r(recompute_none);
	if (add_auto_units())
		r |= recompute_wbunits;
	if (get_dist().recalculatepoly(false))
		r |= recompute_distances;
	if (get_climb().recalculatepoly(false))
		r |= recompute_climb;
	if (get_descent().recalculatepoly(false))
		r |= recompute_descent;
	if (get_glide().recalculatepoly(false))
		r |= recompute_glide;
	return r;
}

// http://www.aircraftguru.com/aircraft/aircraft-information.php?craftid=127
// VR (no flaps, rotation): 45-55 kias (83-102 km/h)
// VR (25° flaps, rotation): 40-52 kias (74-96 km/h)
// VX (no flaps, best angle of climb): 63 kias (117 km/h)
// VX (25° flaps, best angle of climb): 44-57 kias (82-106 km/h)
// VY (best rate of climb): 79 kias (146 km/h)
// VA (maneuvering): 88-111 kias (163-206 km/h)
// VNO (max cruise): 126 kias (233 km/h)
// VNE (never exceed): 160 kias (296 km/h)
// VFE (flaps extended): 103 kias (191 km/h)
// VREF (no flaps, approach): 70 kias (130 km/h)
// VREF (40° flaps, approach): 63 kias (117 km/h)
// VS (stall, clean): 50 kias (93 km/h)
// VS0 (stall, dirty): 44 kias (82 km/h)
