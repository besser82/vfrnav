//
// C++ Implementation: acftconv
//
// Description: ICAO 8643 json database conversion
//
//
// Author: Thomas Sailer <t.sailer@alumni.ethz.ch>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//
//

// https://www.icao.int/publications/DOC8643/Pages/Search.aspx

// curl -o icao8643.json -X POST --referer https://www.icao.int/publications/DOC8643/Pages/Search.aspx -H "Origin: https://www.icao.int" -H "Content-Length: 0" -b "ASP.NET_SessionId=yafiq4nttoumiio53hmiv4ka"   https://www4.icao.int/doc8643/External/AircraftTypes

#include "sysdeps.h"

#include <getopt.h>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <set>

#ifdef HAVE_JSONCPP
#include <json/json.h>
#endif

#ifdef HAVE_SYSEXITS_H
#include <sysexits.h>
#else
#define EX_USAGE   64
#define EX_DATAERR 65
#define EX_OK      0
#endif

class AcftDB {
public:
	AcftDB(void) {}
	void parse(const Json::Value& root);
	void parse_apchcat(const char *filename);
	void parse_apchcat(std::istream& is);
	void write(std::ostream& os) const;

protected:
	class Record {
	public:
		Record(const std::string& designator = "", const std::string& mdlname = "",
		       const std::string& mfgcode = "", const char *desc = 0, char wtc = 'L', char apchcat = '-');
		const std::string& get_designator(void) const { return m_designator; }
		const std::string& get_modelfullname(void) const { return m_modelfullname; }
		const std::string& get_manufacturercode(void) const { return m_manufacturercode; }
		std::string get_description(void) const { return std::string(m_description, sizeof(m_description)); }
		char get_wtc(void) const { return m_wtc; }
		char get_apchcat(void) const { return m_apchcat; }
		void set_designator(const std::string& x) { m_designator = x; }
		void set_modelfullname(const std::string& x) { m_modelfullname = x; }
		void set_manufacturercode(const std::string& x) { m_manufacturercode = x; }
		void set_description(const std::string& x) { strncpy(m_description, x.c_str(), sizeof(m_description)); }
		void set_wtc(char x) { m_wtc = x; }
		void set_apchcat(char x) { m_apchcat = x; }
		void parse(const Json::Value& root);
		bool parse_cc(const std::string& line);
		void parse_dbstr(const std::string& str);
		void write(std::ostream& os, char sep = ',') const;
		int compare(const Record& x) const { return get_designator().compare(x.get_designator()); }
		bool operator==(const Record& x) const { return compare(x) == 0; }
		bool operator!=(const Record& x) const { return compare(x) != 0; }
		bool operator<(const Record& x) const { return compare(x) < 0; }
		bool operator<=(const Record& x) const { return compare(x) <= 0; }
		bool operator>(const Record& x) const { return compare(x) > 0; }
		bool operator>=(const Record& x) const { return compare(x) >= 0; }
		bool check_designator(void) const;
		bool check_description(void) const;
		bool check_wtc(void) const;
		bool check_apchcat(void) const;
		void verify_designator(void) const;
		void verify_description(void) const;
		void verify_wtc(void) const;
		void verify_apchcat(void) const;

	protected:
		std::string m_designator;
		std::string m_modelfullname;
		std::string m_manufacturercode;
		char m_description[3];
		char m_wtc;
		char m_apchcat;
	};

	typedef std::set<Record> db_t;
	db_t m_db;
	db_t m_apchcatdb;
};

AcftDB::Record::Record(const std::string& designator, const std::string& mdlname,
		       const std::string& mfgcode, const char *desc, char wtc, char apchcat)
	: m_designator(designator), m_modelfullname(mdlname), m_manufacturercode(mfgcode),
	  m_wtc(wtc), m_apchcat(apchcat)
{
	memset(m_description, 0, sizeof(m_description));
	if (desc)
		strncpy(m_description, desc, sizeof(m_description));
}

void AcftDB::Record::parse(const Json::Value& root)
{
	if (!root.isObject())
		throw std::runtime_error("json record is not an object");
	if (!root.isMember("ModelFullName"))
		throw std::runtime_error("json record ModelFullName missing");
	{
		const Json::Value& el(root["ModelFullName"]);
		if (!el.isString())
			throw std::runtime_error("json record ModelFullName not a string");
		m_modelfullname = el.asString();
	}
	if (!root.isMember("ManufacturerCode"))
		throw std::runtime_error("json record ManufacturerCode missing");
	{
		const Json::Value& el(root["ManufacturerCode"]);
		if (!el.isString())
			throw std::runtime_error("json record ManufacturerCode not a string");
		m_manufacturercode = el.asString();
	}
	if (!root.isMember("Description"))
		throw std::runtime_error("json record Description missing");
	{
		const Json::Value& el(root["Description"]);
		if (!el.isString())
			throw std::runtime_error("json record Description not a string");
		std::string desc(el.asString());
		if (desc.size() != sizeof(m_description))
			throw std::runtime_error("json record Description has invalid size");
		memcpy(m_description, &desc[0], sizeof(m_description));
		verify_description();
	}
	if (!root.isMember("Designator"))
		throw std::runtime_error("json record Designator missing");
	{
		const Json::Value& el(root["Designator"]);
		if (!el.isString())
			throw std::runtime_error("json record Designator not a string");
		m_designator = el.asString();
		if (m_designator.size() < 4)
			m_designator.resize(4, ' ');
		verify_designator();
	}
	if (!root.isMember("WTC"))
		throw std::runtime_error("json record WTC missing");
	{
		const Json::Value& el(root["WTC"]);
		if (!el.isString())
			throw std::runtime_error("json record WTC not a string");
		std::string wtc(el.asString());
		m_wtc = '-';
		for (std::string::const_iterator i(wtc.begin()), e(wtc.end()); i != e; ++i) {
			if ((*i == 'L' && m_wtc == '-') ||
			    (*i == 'M' && (m_wtc == '-' || m_wtc == 'L')) ||
			    (*i == 'H' && (m_wtc == '-' || m_wtc == 'L' || m_wtc == 'M')) ||
			    (*i == 'S' && (m_wtc == '-' || m_wtc == 'L' || m_wtc == 'M' || m_wtc == 'H')))
				m_wtc = *i;
		}
		verify_wtc();
	}
}

bool AcftDB::Record::parse_cc(const std::string& line)
{
	std::string::const_iterator i0(line.begin()), e(line.end());
	while (i0 != e && std::isspace(*i0))
		++i0;
	if (i0 == e || *i0 != '"')
		return false;
	++i0;
	std::string::const_iterator i1(i0);
	while (i1 != e && *i1 != '"')
		++i1;
	if (i1 == e || i1 - i0 != 9)
		return false;
	parse_dbstr(std::string(i0, i1));
	return true;
}

void AcftDB::Record::parse_dbstr(const std::string& str)
{
	if (str.size() != 9)
		throw std::runtime_error("dbstr: invalid size \"" + str + "\"");
	m_designator = str.substr(0, 4);
	memcpy(m_description, &str[4], sizeof(m_description));
	m_apchcat = str[7];
	m_wtc = str[8];
}

void AcftDB::Record::write(std::ostream& os, char sep) const
{
	static constexpr bool comment(false);
	os << "\t\"" << get_designator() << get_description() << get_apchcat() << get_wtc() << "\"";
	if (comment || sep != ' ')
		os << sep;
	if (comment)
		os << " // " << get_modelfullname() << ", " << get_manufacturercode();
	os << std::endl;
}

bool AcftDB::Record::check_designator(void) const
{
	if (m_designator.size() != 4)
		return false;
	std::string::const_iterator i(m_designator.begin()), e(m_designator.end());
	while (i != e && (std::isupper(*i) || std::isdigit(*i)))
		++i;
	while (i != e && *i == ' ')
		++i;
	return i == e;
}

bool AcftDB::Record::check_description(void) const
{
	if (m_description[0] != 'L' && m_description[0] != 'S' && m_description[0] != 'A' &&
	    m_description[0] != 'H' && m_description[0] != 'G' && m_description[0] != 'T')
		return false;
	if ((m_description[1] < '1' || m_description[1] > '8') && m_description[1] != 'C')
		return false;
	if (m_description[2] != 'P' && m_description[2] != 'T' && m_description[2] != 'J' &&
	    m_description[2] != 'E' && m_description[2] != 'R')
		return false;
	return true;
}

bool AcftDB::Record::check_wtc(void) const
{
	if (m_wtc != 'L' && m_wtc != 'M' && m_wtc != 'H' && m_wtc != 'S')
		return false;
	return true;
}

bool AcftDB::Record::check_apchcat(void) const
{
	if ((m_apchcat < 'A' || m_apchcat > 'D') && m_apchcat != '-')
		return false;
	return true;
}
	
void AcftDB::Record::verify_designator(void) const
{
	if (check_designator())
		return;
	throw std::runtime_error("invalid designator: " + m_designator);
}

void AcftDB::Record::verify_description(void) const
{
	if (check_description())
		return;
	throw std::runtime_error("invalid description: " + std::string(m_description, sizeof(m_description)));
}

void AcftDB::Record::verify_wtc(void) const
{
	if (check_wtc())
		return;
	throw std::runtime_error("invalid wake turbulence class: " + std::string(m_wtc, 1));
}

void AcftDB::Record::verify_apchcat(void) const
{
	if (check_apchcat())
		return;
	throw std::runtime_error("invalid approach category: " + std::string(m_apchcat, 1));
}

void AcftDB::parse(const Json::Value& root)
{
	if (!root.isArray())
		throw std::runtime_error("Json root is not an array");
	for (Json::ArrayIndex i = 0; i < root.size(); ++i) {
		Record rec;
		rec.parse(root[i]);
		{
			db_t::const_iterator i(m_apchcatdb.find(rec));
			if (i != m_apchcatdb.end())
				rec.set_apchcat(i->get_apchcat());
		}
		std::pair<db_t::const_iterator,bool> ins(m_db.insert(rec));
		if (!ins.second) {
			std::cerr << "WARNING: duplicate designator " << rec.get_designator();
			if (ins.first->get_description() != rec.get_description())
				std::cerr << ' ' << rec.get_description() << " != " << ins.first->get_description();
			if (ins.first->get_apchcat() != rec.get_apchcat())
				std::cerr << ' ' << rec.get_apchcat() << " != " << ins.first->get_apchcat();
			if (ins.first->get_wtc() != rec.get_wtc())
				std::cerr << ' ' << rec.get_wtc() << " != " << ins.first->get_wtc();
			std::cerr << std::endl;
			if (false)
				throw std::runtime_error("Duplicate designator " + rec.get_designator());
		}
	}
}

void AcftDB::parse_apchcat(std::istream& is)
{
	if (!is.good())
		throw std::runtime_error("cannot read approach category file");
	while (!is.eof()) {
		std::string line;
		getline(is, line);
		if (!is)
			break;
		Record rec;
		if (!rec.parse_cc(line))
			continue;
		if (false)
			std::cerr << "Apch cat: " << rec.get_designator() << rec.get_description() << rec.get_apchcat() << rec.get_wtc() << std::endl;
		if (!rec.check_designator() || !rec.check_apchcat())
			continue;
		if (!m_apchcatdb.insert(rec).second)
			throw std::runtime_error("Duplicate designator " + rec.get_designator());
	}
}

void AcftDB::parse_apchcat(const char *filename)
{
	if (!filename)
		return;
	std::ifstream ifs(filename);
	parse_apchcat(ifs);
}

void AcftDB::write(std::ostream& os) const
{
	os << "const Aircraft::aircraft_type_class_t Aircraft::aircraft_type_class_db[" << m_db.size() << "] = {" << std::endl;
	for (db_t::const_iterator i(m_db.begin()), e(m_db.end()); i != e;) {
		db_t::const_iterator i0(i);
		++i;
		i0->write(os, (i == e) ? ' ' : ',');
	}
	os << "};" << std::endl;
}

int main(int argc, char *argv[])
{
	static struct option long_options[] = {
		{ "approachcat", required_argument, 0, 'a' },
		{ "output", required_argument, 0, 'o' },
		{ 0, 0, 0, 0 }
	};
	int c, err(0);
	std::string outfile;
	AcftDB db;

	while ((c = getopt_long(argc, argv, "a:o:", long_options, 0)) != EOF) {
                switch (c) {
		case 'a':
			try {
				db.parse_apchcat(optarg);
			} catch (const std::exception& e) {
				std::cerr << "Error \"" << e.what() << "\" parsing approach category file \"" << optarg << "\"" << std::endl;
				return EX_DATAERR;
			}
			break;

		case 'o':
			if (optarg)
				outfile = optarg;
			break;

		default:
			++err;
			break;
                }
        }
        if (err) {
                std::cerr << "usage: genicao8643 [-a <apchcatfile>] [-o <outfile>] [<infile>]" << std::endl;
                return EX_USAGE;
        }
	if (optind >= argc) {
		try {
			Json::Value root;
			{
				Json::CharReaderBuilder rbuilder;
				rbuilder["collectComments"] = false;
				std::string errs;
				if (!Json::parseFromStream(rbuilder, std::cin, &root, &errs)) {
					std::cerr << "Cannot parse stdin: " << errs << std::endl;
					return EX_DATAERR;
				}
			}
			db.parse(root);
		} catch (const std::exception& e) {
			std::cerr << "Error \"" << e.what() << "\" parsing stdin" << std::endl;
			return EX_DATAERR;
		}
	} else {
		for (; optind < argc; ++optind) {
			try {
				Json::Value root;
				{
					std::ifstream ifs(argv[optind]);
					Json::CharReaderBuilder rbuilder;
					rbuilder["collectComments"] = false;
					std::string errs;
					if (!Json::parseFromStream(rbuilder, ifs, &root, &errs)) {
						std::cerr << "Cannot parse input file \"" << argv[optind] << "\": " << errs << std::endl;
						return EX_DATAERR;
					}
				}
				db.parse(root);
			} catch (const std::exception& e) {
				std::cerr << "Error \"" << e.what() << "\" parsing file \"" << argv[optind] << "\"" << std::endl;
				return EX_DATAERR;
			}
		}
	}
	if (outfile.empty()) {
		db.write(std::cout);
	} else {
		std::ofstream ofs(outfile.c_str());
		db.write(ofs);
	}
	return EX_OK;
}
