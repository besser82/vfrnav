//
// C++ Interface: geom
//
// Description:
//
//
// Author: Thomas Sailer <t.sailer@alumni.ethz.ch>, (C) 2007, 2009, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef GEOM_H
#define GEOM_H

#include <boost/smart_ptr/intrusive_ptr.hpp>
//#include <cstdint>
#include <stdint.h>
#include <cmath>
#include <iostream>
#include <vector>
#include <atomic>
#include <glibmm.h>
#include <interval.hh>

#ifdef __AVX__
#include <immintrin.h>
#endif

#include "sysdeps.h"
#include "alignedalloc.h"

#ifdef HAVE_PQXX
#include <pqxx/binarystring>
#endif

#ifdef HAVE_JSONCPP
namespace Json {
class Value;
};
#endif

namespace sqlite3x {
        class sqlite3_cursor;
        class sqlite3_command;
};

class Geometry;
class Rect;
class PolygonSimple;

class Point {
public:
	typedef int32_t coord_t;
	static const coord_t lon_min = 0x80000000;
	static const coord_t lon_max = 0x7FFFFFFF;
	static const coord_t lat_min = 0xC0000000;
	static const coord_t lat_max = 0x40000000;
	static const Point invalid;

	Point(coord_t lo = 0, coord_t la = 0) : m_lon(lo), m_lat(la) {}
	coord_t get_lon(void) const { return m_lon; }
	coord_t get_lat(void) const { return m_lat; }
	void set_lon(coord_t lo) { m_lon = lo; }
	void set_lat(coord_t la) { m_lat = la; }
	bool operator==(const Point& p) const { return m_lon == p.m_lon && m_lat == p.m_lat; }
	bool operator!=(const Point& p) const { return !operator==(p); }
	bool operator<(const Point& p) const { return compare(p) < 0; }
	bool operator<=(const Point& p) const { return compare(p) <= 0; }
	bool operator>(const Point& p) const { return compare(p) > 0; }
	bool operator>=(const Point& p) const { return compare(p) >= 0; }
	Point operator+(const Point& p) const { return Point(m_lon + static_cast<uint32_t>(p.m_lon), m_lat + static_cast<uint32_t>(p.m_lat)); }
	Point operator-(const Point& p) const { return Point(m_lon - static_cast<uint32_t>(p.m_lon), m_lat - static_cast<uint32_t>(p.m_lat)); }
	Point operator-() const { return Point(-m_lon, -m_lat); }
	Point& operator+=(const Point& p) { m_lon += static_cast<uint32_t>(p.m_lon); m_lat += static_cast<uint32_t>(p.m_lat); return *this; }
	Point& operator-=(const Point& p) { m_lon -= static_cast<uint32_t>(p.m_lon); m_lat -= static_cast<uint32_t>(p.m_lat); return *this; }
	bool is_latlon_less(const Point& p) const { return m_lon < p.m_lon && m_lat < p.m_lat; }
	bool is_latlon_lessthan(const Point& p) const { return m_lon <= p.m_lon && m_lat <= p.m_lat; }
	bool is_latlon_greater(const Point& p) const { return m_lon > p.m_lon && m_lat > p.m_lat; }
	bool is_latlon_greaterthan(const Point& p) const { return m_lon >= p.m_lon && m_lat >= p.m_lat; }
	int compare(const Point& x) const;
	uint64_t length2(void) const { return (uint64_t)(get_lon() * (int64_t)get_lon()) + (uint64_t)(get_lat() * (int64_t)get_lat()); }
	double length(void) const { return sqrt(length2()); }
	bool left_of(const Point& p0, const Point& p1) const;
	bool intersection(const Point& l0a, const Point& l0b, const Point& l1a, const Point& l1b);
	Point nearest_on_line(const Point& la, const Point& lb) const;
	Point spheric_nearest_on_line(const Point& la, const Point& lb) const;

	bool is_strictly_left(const Point& p0, const Point& p1) const { return area2(p0, p1, *this) > 0; }
	bool is_left_or_on(const Point& p0, const Point& p1) const { return area2(p0, p1, *this) >= 0; }
	bool is_collinear(const Point& p0, const Point& p1) const { return area2(p0, p1, *this) == 0; }
	bool is_between(const Point& p0, const Point& p1) const;
	bool is_strictly_between(const Point& p0, const Point& p1) const;
	bool is_convex(const Point& vprev, const Point& vnelont) const;
	bool is_in_cone(const Point& vprev, const Point& v, const Point& vnelont) const;
	bool is_left_of(const Point& p0, const Point& p1) const;
	bool in_box(const Point& p0, const Point& p1) const;
	bool is_invalid(void) const { return m_lat < lat_min || m_lat > lat_max; }
	void set_invalid(void);
	Rect get_bbox(void) const;

	std::ostream& print(std::ostream& os) const;

	static int64_t area2(const Point& p0, const Point& p1, const Point& p2);
	static bool is_proper_intersection(const Point& l0a, const Point& l0b, const Point& l1a, const Point& l1b);
	static bool is_intersection(const Point& l0a, const Point& l0b, const Point& l1a, const Point& l1b);
	static bool is_strict_intersection(const Point& l0a, const Point& l0b, const Point& l1a, const Point& l1b);

	Point simple_unwrap(const Point& p2) const;

public:
	static constexpr float to_deg = 90.0 / (1UL << 30);
	static constexpr float to_min = 90.0 * 60.0 / (1UL << 30);
	static constexpr float to_sec = 90.0 * 60.0 * 60.0 / (1UL << 30);
	static constexpr float to_rad = M_PI / (1UL << 31);
	static constexpr float from_deg = (1UL << 30) / 90.0;
	static constexpr float from_min = (1UL << 30) / 90.0 / 60.0;
	static constexpr float from_sec = (1UL << 30) / 90.0 / 60.0 / 60.0;
	static constexpr float from_rad = (1UL << 31) / M_PI;

	static constexpr double to_deg_dbl = 90.0 / (1UL << 30);
	static constexpr double to_min_dbl = 90.0 * 60.0 / (1UL << 30);
	static constexpr double to_sec_dbl = 90.0 * 60.0 * 60.0 / (1UL << 30);
	static constexpr double to_rad_dbl = M_PI / (1UL << 31);
	static constexpr double from_deg_dbl = (1UL << 30) / 90.0;
	static constexpr double from_min_dbl = (1UL << 30) / 90.0 / 60.0;
	static constexpr double from_sec_dbl = (1UL << 30) / 90.0 / 60.0 / 60.0;
	static constexpr double from_rad_dbl = (1UL << 31) / M_PI;

	static const int32_t pole_lat = (1UL << 30);

	static constexpr float earth_radius = 6378.155;
	static constexpr float km_to_nmi = 1.0 / 1.852;
	static constexpr float m_to_ft = 1.0 / 0.3048;
	static constexpr float ft_to_m = 0.3048;
	static constexpr float mps_to_kts = 3.6 / 1.852;

	static constexpr double earth_radius_dbl = 6378.155;
	static constexpr double km_to_nmi_dbl = 1.0 / 1.852;
	static constexpr double m_to_ft_dbl = 1.0 / 0.3048;
	static constexpr double ft_to_m_dbl = 0.3048;
	static constexpr double mps_to_kts_dbl = 3.6 / 1.852;

	static const double webmercator_maxlat_rad_dbl;
	static const double webmercator_maxlat_deg_dbl;
	static const coord_t webmercator_maxlat;
	static const double webmerclatpoly[15];

	template<typename I, typename F> static I round(F x) { return static_cast<I>((x < 0) ? (x - 0.5) : (x + 0.5)); }

	float get_lon_rad(void) const { return m_lon * to_rad; }
	float get_lat_rad(void) const { return m_lat * to_rad; }
	void set_lon_rad(float lo) { m_lon = round<int64_t,float>(lo * from_rad) & 0xffffffff; }
	void set_lat_rad(float la) { m_lat = round<int64_t,float>(la * from_rad) & 0xffffffff; }

	double get_lon_rad_dbl(void) const { return m_lon * to_rad_dbl; }
	double get_lat_rad_dbl(void) const { return m_lat * to_rad_dbl; }
	void set_lon_rad_dbl(double lo) { m_lon = round<int64_t,double>(lo * from_rad_dbl) & 0xffffffff; }
	void set_lat_rad_dbl(double la) { m_lat = round<int64_t,double>(la * from_rad_dbl) & 0xffffffff; }

	float get_lon_deg(void) const { return m_lon * to_deg; }
	float get_lat_deg(void) const { return m_lat * to_deg; }
	void set_lon_deg(float lo) { m_lon = round<int64_t,float>(lo * from_deg) & 0xffffffff; }
	void set_lat_deg(float la) { m_lat = round<int64_t,float>(la * from_deg) & 0xffffffff; }

	double get_lon_deg_dbl(void) const { return m_lon * to_deg_dbl; }
	double get_lat_deg_dbl(void) const { return m_lat * to_deg_dbl; }
	void set_lon_deg_dbl(double lo) { m_lon = round<int64_t,double>(lo * from_deg_dbl) & 0xffffffff; }
	void set_lat_deg_dbl(double la) { m_lat = round<int64_t,double>(la * from_deg_dbl) & 0xffffffff; }

	float get_lon_min(void) const { return m_lon * to_min; }
	float get_lat_min(void) const { return m_lat * to_min; }
	void set_lon_min(float lo) { m_lon = round<int64_t,float>(lo * from_min) & 0xffffffff; }
	void set_lat_min(float la) { m_lat = round<int64_t,float>(la * from_min) & 0xffffffff; }

	double get_lon_min_dbl(void) const { return m_lon * to_min_dbl; }
	double get_lat_min_dbl(void) const { return m_lat * to_min_dbl; }
	void set_lon_min_dbl(double lo) { m_lon = round<int64_t,double>(lo * from_min_dbl) & 0xffffffff; }
	void set_lat_min_dbl(double la) { m_lat = round<int64_t,double>(la * from_min_dbl) & 0xffffffff; }

	float get_lon_sec(void) const { return m_lon * to_sec; }
	float get_lat_sec(void) const { return m_lat * to_sec; }
	void set_lon_sec(float lo) { m_lon = round<int64_t,float>(lo * from_sec) & 0xffffffff; }
	void set_lat_sec(float la) { m_lat = round<int64_t,float>(la * from_sec) & 0xffffffff; }

	double get_lon_sec_dbl(void) const { return m_lon * to_sec_dbl; }
	double get_lat_sec_dbl(void) const { return m_lat * to_sec_dbl; }
	void set_lon_sec_dbl(double lo) { m_lon = round<int64_t,double>(lo * from_sec_dbl) & 0xffffffff; }
	void set_lat_sec_dbl(double la) { m_lat = round<int64_t,double>(la * from_sec_dbl) & 0xffffffff; }

	bool is_webmercator_valid(void) const;
	int32_t get_lon_webmercator(unsigned int zoomlevel) const;
	int32_t get_lat_webmercator(unsigned int zoomlevel) const;
	void set_lon_webmercator(unsigned int zoomlevel, int32_t x);
	void set_lat_webmercator(unsigned int zoomlevel, int32_t y);
	double get_lon_webmercator_dbl(unsigned int zoomlevel) const;
	double get_lat_webmercator_dbl(unsigned int zoomlevel) const;
	void set_lon_webmercator_dbl(unsigned int zoomlevel, double x);
	void set_lat_webmercator_dbl(unsigned int zoomlevel, double y);
	void transform_webmercator(void);
	void invtransform_webmercator(void);
	void approx_webmercator(void);

	float simple_true_course(const Point& p2) const;
	uint64_t simple_distance_rel(const Point& p2) const;
	float simple_distance_km(const Point& p2) const;
	float simple_distance_nmi(const Point& p2) const { return simple_distance_km(p2) * km_to_nmi; }
	Point simple_course_distance_nmi(float course, float dist) const;
	Rect simple_box_km(float centerdist) const;
	Rect simple_box_nmi(float centerdist) const;

	float spheric_true_course(const Point& p2) const;
	float spheric_distance_km(const Point& p2) const;
	float spheric_distance_nmi(const Point& p2) const { return spheric_distance_km(p2) * km_to_nmi; }
	Point spheric_course_distance_nmi(float course, float dist) const;

	double spheric_true_course_dbl(const Point& p2) const;
	double spheric_distance_km_dbl(const Point& p2) const;
	double spheric_distance_nmi_dbl(const Point& p2) const { return spheric_distance_km_dbl(p2) * km_to_nmi_dbl; }
	Point spheric_course_distance_nmi_dbl(double course, double dist) const;

	bool dwithin(const Point& g1, double dist_nmi) const;
	Point halfway(const Point& pt2) const;

	float get_initial_course(const Point& pt2) const;
	double get_initial_course_dbl(const Point& pt2) const;
	std::pair<Point,double> get_gcnav(const Point& p2, double frac) const;
	PolygonSimple make_circle(float radius = 5, float angleincr = 15, bool rawcoord = false) const;
	PolygonSimple make_fat_line(const Point& p, float radius = 5, bool roundcaps = true, float angleincr = 15, bool rawcoord = false) const;

	static Glib::ustring to_ustring(const std::wstring& str);

	Glib::ustring get_lon_str(void) const;
	Glib::ustring get_lat_str(void) const;
	std::string get_lon_str2(void) const;
	std::string get_lat_str2(void) const;
	std::string get_lon_str3(void) const;
	std::string get_lat_str3(void) const;
	std::string get_lon_adexp_str(void) const;
	std::string get_lat_adexp_str(void) const;
	typedef enum {
		fplstrmode_deg          = 0,
		fplstrmode_degmin       = 1 | 8,
		fplstrmode_degoptmin    = 1,
		fplstrmode_degminsec    = 2 | 8 | 4,
		fplstrmode_degminoptsec = 2 | 8,
		fplstrmode_degoptminsec = 2
	} fplstrmode_t;
	std::string get_lon_fpl_str(fplstrmode_t mode = fplstrmode_degmin) const;
	std::string get_lat_fpl_str(fplstrmode_t mode = fplstrmode_degmin) const;
	std::string get_fpl_str(fplstrmode_t mode = fplstrmode_degmin) const;

	bool set_lon_str(const std::string& str);
	bool set_lat_str(const std::string& str);
	static const unsigned int setstr_lat    = 1 << 0;
	static const unsigned int setstr_lon    = 1 << 1;
	static const unsigned int setstr_excess = 1 << 2;
	static const unsigned int setstr_flags_locator    = 1 << 0;
	static const unsigned int setstr_flags_ch1903     = 1 << 1;
	static const unsigned int setstr_flags_olc        = 1 << 2;
	unsigned int set_str(const std::string& str, unsigned int flags = setstr_flags_locator | setstr_flags_ch1903 | setstr_flags_olc);

	static bool is_latitude(const std::string& s);
	static bool is_longitude(const std::string& s);
	static bool is_latlon(const std::string& s);
	static bool is_fpl_latlon(std::string::const_iterator i, std::string::const_iterator e);
	static bool is_fpl_latlon(const std::string& s) { return is_fpl_latlon(s.begin(), s.end()); }

	void set_ch1903(float x, float y);
	bool get_ch1903(float& x, float& y) const;
	void set_ch1903p(double x, double y);
	bool get_ch1903p(double& x, double& y) const;
	template<typename T> static T polyeval(const T *coeff, unsigned int n, T v);
	std::string get_locator(unsigned int digits = 10) const;
	static unsigned int get_olc_digit(char ch);
	std::string get_openlocationcode(unsigned int digits = 10) const;
	std::string get_skydemon(void) const;

	Point get_round_deg(void) const;
	Point get_round_min(void) const;
	Point get_round_sec(void) const;

	// WKT / WKB
	static unsigned int get_wkbptsize(void) { return 16; }
	static unsigned int get_wkbsize(void);
	uint8_t *wkbencode(uint8_t *cp, uint8_t *ep) const;
	const uint8_t *wkbdecode(const uint8_t *cp, const uint8_t *ep, uint8_t byteorder);
	uint8_t *to_wkb(uint8_t *cp, uint8_t *ep) const;
	std::vector<uint8_t> to_wkb(void) const;
	const uint8_t *from_wkb(const uint8_t *cp, const uint8_t *ep);
	void from_wkb(const std::vector<uint8_t>& b) { from_wkb(&b[0], &b[b.size()]); }
	std::ostream& to_wktpt(std::ostream& os) const;
	std::ostream& to_wkt(std::ostream& os) const;

	// GeoJSON
#ifdef HAVE_JSONCPP
	Json::Value to_json(void) const;
	void from_json(const Json::Value& g);
#endif

	template<class Archive> void hibernate_binary(Archive& ar) {
		ar.io(m_lat);
		ar.io(m_lon);
	}

protected:
	static const unsigned int maidenhead_maxdigits = 5;
	static const unsigned int maidenhead_max[maidenhead_maxdigits];
	static const double maidenhead_factors[maidenhead_maxdigits];
	static const char olc_codes[23];

	coord_t m_lon, m_lat;

	bool is_between_helper(const Point& p0, const Point& p1) const;
	bool is_strictly_between_helper(const Point& p0, const Point& p1) const;

	static bool intersect_precheck(const Point& l0a, const Point& l0b, const Point& l1a, const Point& l1b);
	unsigned int set_str_ch1903(std::string::const_iterator& si, std::string::const_iterator se);
	unsigned int set_str_locator(std::string::const_iterator& si, std::string::const_iterator se);
	unsigned int set_str_openlocationcode(std::string::const_iterator& si, std::string::const_iterator se);

	float rawcoord_course(const Point& p2) const;
	Point rawcoord_course_distance(float course, float dist) const;

	double rawcoord_true_course_dbl(const Point& p2) const;
	Point rawcoord_course_distance(double course, double dist) const;

	friend class PolygonSimple;
};

class Rect {
public:
	static const Rect invalid;
	Rect(void) : m_southwest(Point(0, -Point::pole_lat)), m_northeast(Point(-1, Point::pole_lat)) {}
	Rect(const Point& sw, const Point& ne) : m_southwest(sw), m_northeast(ne) {}
	Point& get_southwest(void) { return m_southwest; }
	Point& get_northeast(void) { return m_northeast; }
	const Point& get_southwest(void) const { return m_southwest; }
	const Point& get_northeast(void) const { return m_northeast; }
	Point get_southeast(void) const { return Point(m_northeast.get_lon(), m_southwest.get_lat()); }
	Point get_northwest(void) const { return Point(m_southwest.get_lon(), m_northeast.get_lat()); }
	void set_southwest(const Point& p) { m_southwest = p; }
	void set_northeast(const Point& p) { m_northeast = p; }
	void set_southeast(const Point& p) { m_southwest.set_lat(p.get_lat()); m_northeast.set_lon(p.get_lon()); }
	void set_northwest(const Point& p) { m_northeast.set_lat(p.get_lat()); m_southwest.set_lon(p.get_lon()); }
	Point::coord_t get_north(void) const { return m_northeast.get_lat(); }
	Point::coord_t get_south(void) const { return m_southwest.get_lat(); }
	Point::coord_t get_east(void) const { return m_northeast.get_lon(); }
	int64_t get_east_unwrapped(void) const;
	Point::coord_t get_west(void) const { return m_southwest.get_lon(); }
	void set_north(Point::coord_t c) { m_northeast.set_lat(c); }
	void set_south(Point::coord_t c) { m_southwest.set_lat(c); }
	void set_east(Point::coord_t c) { m_northeast.set_lon(c); }
	void set_west(Point::coord_t c) { m_southwest.set_lon(c); }
	int compare(const Rect& x) const;
	bool operator==(const Rect &r) const { return get_southwest() == r.get_southwest() && get_northeast() == r.get_northeast(); }
	bool operator!=(const Rect &r) const { return !operator==(r); }
	Rect& operator+=(const Point &p) { m_southwest += p; m_northeast += p; return *this; }
	Rect& operator-=(const Point &p) { m_southwest -= p; m_northeast -= p; return *this; }
	Rect operator+(const Point &p) { Rect r(*this); r += p; return r; }
	Rect operator-(const Point &p) { Rect r(*this); r -= p; return r; }
	bool is_invalid(void) const { return get_northeast().is_invalid() || get_southwest().is_invalid(); }
	void set_invalid(void) { *this = invalid; }
	bool is_wholeworld(void) const;
	void set_wholeworld(void);
	bool is_inside(const Point& pt) const;
	bool is_inside(const Rect& r) const;
	bool is_strictly_inside(const Point& pt) const;
	bool is_strictly_inside(const Rect& r) const { return is_strictly_inside(r.get_southwest()) && is_strictly_inside(r.get_northeast()); }
	bool is_lon_inside(const Point& pt) const;
	bool is_lat_inside(const Point& pt) const;
	uint64_t simple_distance_rel(const Point& pt) const;
	float simple_distance_km(const Point& pt) const;
	float simple_distance_nmi(const Point& pt) const { return simple_distance_km(pt) * Point::km_to_nmi; }
	float spheric_distance_km(const Point& pt) const;
	float spheric_distance_nmi(const Point& pt) const { return spheric_distance_km(pt) * Point::km_to_nmi; }
	bool is_intersect(const Rect& r) const;
	bool is_intersect(const Point& la, const Point& lb) const;
	Rect intersect(const Rect& r) const;
	uint64_t simple_distance_rel(const Rect& r) const;
	float simple_distance_km(const Rect& r) const;
	float simple_distance_nmi(const Rect& r) const { return simple_distance_km(r) * Point::km_to_nmi; }
	float spheric_distance_km(const Rect& r) const;
	float spheric_distance_nmi(const Rect& r) const { return spheric_distance_km(r) * Point::km_to_nmi; }
	Point boxcenter(void) const { return m_southwest.halfway(m_northeast); }
	Rect oversize_nmi(float nmi) const;
	Rect oversize_km(float km) const;
	Rect add(const Point& pt) const;
	Rect add(const Rect& r) const;
	int64_t area2(void) const;
	double spherical_area(void) const;
	void transform_webmercator(void);
	void invtransform_webmercator(void);
	void approx_webmercator(void);
	uint64_t simple_circumference_rel(void) const;
	float simple_circumference_km(void) const;
	float simple_circumference_nmi(void) const { return simple_circumference_km() * Point::km_to_nmi; }
	float spheric_circumference_km(void) const;
	float spheric_circumference_nmi(void) const { return spheric_circumference_km() * Point::km_to_nmi; }
	double spheric_circumference_km_dbl(void) const;
	double spheric_circumference_nmi_dbl(void) const { return spheric_circumference_km_dbl() * Point::km_to_nmi_dbl; }
	double get_simple_area_km2_dbl(void) const;
	double get_simple_area_nmi2_dbl(void) const { return get_simple_area_km2_dbl() * (Point::km_to_nmi_dbl * Point::km_to_nmi_dbl); }
	operator PolygonSimple(void) const;

	bool is_empty(void) const;
	void set_empty(void);

	std::ostream& print(std::ostream& os) const;

	template<class Archive> void hibernate_binary(Archive& ar) {
		m_southwest.hibernate_binary(ar);
		m_northeast.hibernate_binary(ar);
	}

protected:
	Point m_southwest;
	Point m_northeast;

	std::pair<bool,bool> is_overlap_helper(const Rect& r) const;
};

class PolygonHole;
class MultiPolygonHole;

class BorderPoint {
public:
	typedef enum {
		flag_begin,
		flag_end,
		flag_onborder,
		flag_inc,
		flag_dec
	} flag_t;

	typedef enum {
		flagmask_begin    = 1 << flag_begin,
		flagmask_end      = 1 << flag_end,
		flagmask_onborder = 1 << flag_onborder,
		flagmask_inc      = 1 << flag_inc,
		flagmask_dec      = 1 << flag_dec,
		flagmask_none     = 0,
		flagmask_all      = flagmask_begin | flagmask_end | flagmask_onborder | flagmask_inc | flagmask_dec
	} flagmask_t;

	typedef std::vector<BorderPoint> borderpoints_t;

	BorderPoint(const Point& pt = Point::invalid, double dist = std::numeric_limits<double>::quiet_NaN(), flagmask_t flags = flagmask_none);
	const Point& get_point(void) const { return m_pt; }
	double get_dist(void) const { return m_dist; }
	flagmask_t get_flags(void) const { return m_flags; }
	static std::string get_flags_string(flagmask_t m);
	std::string get_flags_string(void) const { return get_flags_string(get_flags()); }
	int get_windingnumber(void) const;

	int compare(const BorderPoint& x) const;
	bool operator==(const BorderPoint& x) const { return compare(x) == 0; }
	bool operator!=(const BorderPoint& x) const { return compare(x) != 0; }
	bool operator<(const BorderPoint& x) const { return compare(x) < 0; }
	bool operator<=(const BorderPoint& x) const { return compare(x) <= 0; }
	bool operator>(const BorderPoint& x) const { return compare(x) > 0; }
	bool operator>=(const BorderPoint& x) const { return compare(x) >= 0; }

protected:
	Point m_pt;
	double m_dist;
	flagmask_t m_flags;
};

class MultiPoint : public std::vector<Point> {
protected:
	typedef std::vector<Point> base_t;

public:
	Rect get_bbox(void) const;
	int compare(const MultiPoint& x) const;
	void transform_webmercator(void);
	void invtransform_webmercator(void);
	void approx_webmercator(void);
	void nearest(Point& ptn, float& dist, const Point& pt) const;
	void spheric_nearest(Point& ptn, double& dist, const Point& pt) const;
	void nearest(Point& ptn, float& dist, const MultiPoint& pt) const;
	void spheric_nearest(Point& ptn, double& dist, const MultiPoint& pt) const;
	bool dwithin(const Point& g1, double dist_nmi) const;
	bool dwithin(const MultiPoint& g1, double dist_nmi) const;

	std::ostream& print(std::ostream& os) const;

	// WKT / WKB
	unsigned int get_wkbsize(void) const;
	uint8_t *wkbencode(uint8_t *cp, uint8_t *ep) const;
	const uint8_t *wkbdecode(const uint8_t *cp, const uint8_t *ep, uint8_t byteorder);
	uint8_t *to_wkb(uint8_t *cp, uint8_t *ep) const;
	std::vector<uint8_t> to_wkb(void) const;
	const uint8_t *from_wkb(const uint8_t *cp, const uint8_t *ep);
	void from_wkb(const std::vector<uint8_t>& b) { from_wkb(&b[0], &b[b.size()]); }
	std::ostream& to_wkt(std::ostream& os) const;

	// GeoJSON
#ifdef HAVE_JSONCPP
	Json::Value to_json(void) const;
	void from_json(const Json::Value& g);
#endif

	template<class Archive> void hibernate_binary(Archive& ar) {
		uint32_t sz(size());
		ar.io(sz);
		if (ar.is_load())
			resize(sz);
		for (iterator i(begin()), e(end()); i != e; ++i)
			i->hibernate_binary(ar);
	}
};

class LineString : public std::vector<Point, aligned_allocator<Point> > {
protected:
	typedef std::vector<Point, aligned_allocator<Point> > base_t;

public:
	static const std::string skyvector_url;

	bool is_on_boundary(const Point& pt) const;
	bool is_boundary_point(const Point& pt) const;
	bool is_intersection(const Point& la, const Point& lb) const;
	bool is_intersection(const LineString& ls) const;
	bool is_intersection(const PolygonSimple& ps) const;
	bool is_intersection(const Rect& r) const;
	bool is_strict_intersection(const Point& la, const Point& lb) const;
	bool is_self_intersecting(void) const;
	typedef std::set<Point> pointset_t;
	pointset_t get_intersections(const LineString& x) const;
	bool is_all_points_inside(const Rect& r) const;
	bool is_any_point_inside(const Rect& r) const;
	bool is_overlap(const Rect& r) const;
	void reverse(void);
	Rect get_bbox(void) const;
	bool operator==(const LineString &p) const;
	bool operator!=(const LineString &p) const { return !operator==(p); }
	int compare(const LineString& x) const;
	void transform_webmercator(void);
	void invtransform_webmercator(void);
	void approx_webmercator(void);
	MultiPolygonHole make_fat_line(float radius = 5, bool roundcaps = true, float angleincr = 15, bool rawcoord = false) const;
	void nearest(Point& ptn, float& dist, const Point& pt) const;
	void spheric_nearest(Point& ptn, double& dist, const Point& pt) const;
	void nearest(Point& ptn, float& dist, const MultiPoint& pt) const;
	void spheric_nearest(Point& ptn, double& dist, const MultiPoint& pt) const;
	void nearest(Point& ptn, float& dist, const LineString& ls) const;
	void spheric_nearest(Point& ptn, double& dist, const LineString& ls) const;
	bool dwithin(const Point& g1, double dist_nmi) const;
	bool dwithin(const MultiPoint& g1, double dist_nmi) const;
	bool dwithin(const LineString& g1, double dist_nmi) const;
	std::ostream& print(std::ostream& os) const;
	std::string to_skyvector(void) const;

	// WKT / WKB
	unsigned int get_wkbsize(void) const;
	uint8_t *wkbencode(uint8_t *cp, uint8_t *ep) const;
	const uint8_t *wkbdecode(const uint8_t *cp, const uint8_t *ep, uint8_t byteorder);
	uint8_t *to_wkb(uint8_t *cp, uint8_t *ep) const;
	std::vector<uint8_t> to_wkb(void) const;
	const uint8_t *from_wkb(const uint8_t *cp, const uint8_t *ep);
	void from_wkb(const std::vector<uint8_t>& b) { from_wkb(&b[0], &b[b.size()]); }
	std::ostream& to_wktlst(std::ostream& os) const;
	std::ostream& to_wkt(std::ostream& os) const;

	// GeoJSON
#ifdef HAVE_JSONCPP
	Json::Value to_json(void) const;
	void from_json(const Json::Value& g);
#endif

	template<class Archive> void hibernate_binary(Archive& ar) {
		uint32_t sz(size());
		ar.io(sz);
		if (ar.is_load())
			resize(sz);
		for (iterator i(begin()), e(end()); i != e; ++i)
			i->hibernate_binary(ar);
	}
};

class MultiLineString : public std::vector<LineString> {
protected:
	typedef std::vector<LineString> base_t;

public:
	bool is_on_boundary(const Point& pt) const;
	bool is_boundary_point(const Point& pt) const;
	bool is_intersection(const Point& la, const Point& lb) const;
	bool is_intersection(const Rect& r) const;
	bool is_strict_intersection(const Point& la, const Point& lb) const;
	typedef LineString::pointset_t pointset_t;
	pointset_t get_intersections(const LineString& x) const;
	pointset_t get_intersections(const MultiLineString& x) const;
	bool is_all_points_inside(const Rect& r) const;
	bool is_any_point_inside(const Rect& r) const;
	bool is_overlap(const Rect& r) const;
	Rect get_bbox(void) const;
	int compare(const MultiLineString& x) const;
	void transform_webmercator(void);
	void invtransform_webmercator(void);
	void approx_webmercator(void);
	void nearest(Point& ptn, float& dist, const Point& pt) const;
	void spheric_nearest(Point& ptn, double& dist, const Point& pt) const;
	void nearest(Point& ptn, float& dist, const MultiPoint& pt) const;
	void spheric_nearest(Point& ptn, double& dist, const MultiPoint& pt) const;
	void nearest(Point& ptn, float& dist, const LineString& ls) const;
	void spheric_nearest(Point& ptn, double& dist, const LineString& ls) const;
	void nearest(Point& ptn, float& dist, const MultiLineString& mls) const;
	void spheric_nearest(Point& ptn, double& dist, const MultiLineString& mls) const;
	bool dwithin(const Point& g1, double dist_nmi) const;
	bool dwithin(const MultiPoint& g1, double dist_nmi) const;
	bool dwithin(const LineString& g1, double dist_nmi) const;
	bool dwithin(const MultiLineString& g1, double dist_nmi) const;
	void join_adjoining(bool allow_reverse = true);
	std::ostream& print(std::ostream& os) const;

	// WKT / WKB
	unsigned int get_wkbsize(void) const;
	uint8_t *wkbencode(uint8_t *cp, uint8_t *ep) const;
	const uint8_t *wkbdecode(const uint8_t *cp, const uint8_t *ep, uint8_t byteorder);
	uint8_t *to_wkb(uint8_t *cp, uint8_t *ep) const;
	std::vector<uint8_t> to_wkb(void) const;
	const uint8_t *from_wkb(const uint8_t *cp, const uint8_t *ep);
	void from_wkb(const std::vector<uint8_t>& b) { from_wkb(&b[0], &b[b.size()]); }
	std::ostream& to_wkt(std::ostream& os) const;

	// GeoJSON
#ifdef HAVE_JSONCPP
	Json::Value to_json(void) const;
	void from_json(const Json::Value& g);
#endif

	template<class Archive> void hibernate_binary(Archive& ar) {
		uint32_t sz(size());
		ar.io(sz);
		if (ar.is_load())
			resize(sz);
		for (iterator i(begin()), e(end()); i != e; ++i)
			i->hibernate_binary(ar);
	}
};

class PolygonSimple : public LineString {
public:
	typedef LineString base_t;

	class ScanLine : public std::map<Point::coord_t,int> {
	protected:
		typedef std::map<Point::coord_t,int> base_t;

	public:
		using base_t::insert;
		void insert(Point::coord_t lon, int wn);
		ScanLine integrate(Point::coord_t lon) const;
		ScanLine combine(const ScanLine& sl2, int factor1 = 1, int factor2 = 1) const;
	};

	typedef enum {
		inside_none      = 0x00,
		inside_partial   = 0x01,
		inside_all       = 0x02,
		inside_error     = 0x03,
		inside_mask      = 0x03,
		inside_point0    = 0x04,
		inside_point1    = 0x08,
		// composite
		inside_full      = inside_all | inside_point0 | inside_point1
	} inside_t;

	typedef BorderPoint::borderpoints_t borderpoints_t;

	class InsideHelper : public IntervalSet<int64_t> {
	protected:
		typedef enum {
			simd_none,
			simd_avx,
			simd_avx2
		} simd_t;
	public:
		typedef IntervalSet<int64_t> interval_t;
		static const bool debug = false;
		static const int64_t bordertolerance = 1;

		InsideHelper(void);
		InsideHelper(const Point& pt0, const Point& pt1, bool enable_simd = true);
		const Point& get_origin(void) const { return m_origin; }
		const Point& get_vector(void) const { return m_vector; }
		int64_t get_end(void) const { return m_end; }
		static interval_t::Intvl get_startintvl(void) { return interval_t::Intvl(-bordertolerance, bordertolerance+1); }
		interval_t::Intvl get_endintvl(void) const { return interval_t::Intvl(get_end()-bordertolerance, get_end()+bordertolerance+1); }
		interval_t::Intvl get_insideintvl(void) const { return interval_t::Intvl(bordertolerance+1, (get_end() >= 2*bordertolerance+1) ? get_end()-bordertolerance : bordertolerance+1); }
		typedef std::pair<int64_t,int64_t> tpoint_t;
		tpoint_t transform(const Point& pt) const;
		Point transform(const tpoint_t& tp) const;
		bool is_error(void) const { return m_end < 0; }
		void set_error(void) { m_end = -1; }
		bool is_simd(void) const { return m_simd != simd_none; }
		inside_t get_result(void) const;
		borderpoints_t get_borderpoints(void) const;
		bool is_startinside(void) const { return is_inside(0); }
		bool is_endinside(void) const { return is_inside(get_end()); }
		void limit(int32_t fsmin, int32_t fsmax, int32_t min, int32_t max, bool allowswap);
		void limit(int64_t min, int64_t max);
		void clear(void);
		void invert(void);
		using interval_t::operator|=;
		using interval_t::operator&=;
		using interval_t::operator-=;
		using interval_t::operator=;
		InsideHelper& operator|=(const PolygonSimple& poly) { *this |= compute_winding(poly); return *this; }
		InsideHelper& operator&=(const PolygonSimple& poly) { *this &= compute_winding(poly); return *this; }
		InsideHelper& operator-=(const PolygonSimple& poly) { *this -= compute_winding(poly); return *this; }
		InsideHelper& operator|=(const PolygonHole& poly) { *this |= compute_winding(poly); return *this; }
		InsideHelper& operator&=(const PolygonHole& poly) { *this &= compute_winding(poly); return *this; }
		InsideHelper& operator-=(const PolygonHole& poly) { *this -= compute_winding(poly); return *this; }
		InsideHelper& operator|=(const MultiPolygonHole& poly) { *this |= compute_winding(poly); return *this; }
		InsideHelper& operator&=(const MultiPolygonHole& poly) { *this &= compute_winding(poly); return *this; }
		InsideHelper& operator-=(const MultiPolygonHole& poly) { *this -= compute_winding(poly); return *this; }
		InsideHelper& operator=(const PolygonSimple& poly) { *this = compute_winding(poly); return *this; }
		InsideHelper& operator=(const PolygonHole& poly) { *this = compute_winding(poly); return *this; }
		InsideHelper& operator=(const MultiPolygonHole& poly) { *this = compute_winding(poly); return *this; }
		std::string to_str(void) const;

	protected:
		Point m_origin;
		Point m_vector;
		int64_t m_end;
		simd_t m_simd;

		typedef std::map<int64_t,int32_t> winding_t;
		void compute_winding(winding_t& wind, const PolygonSimple& poly, int dir) const;
		void compute_winding(winding_t& wind, const PolygonHole& poly, int dir) const;
		void compute_winding(winding_t& wind, const MultiPolygonHole& poly, int dir) const;
		interval_t finalize(const winding_t& wind);
		interval_t compute_winding(const PolygonSimple& poly);
		interval_t compute_winding(const PolygonHole& poly);
		interval_t compute_winding(const MultiPolygonHole& poly);

#ifdef __AVX__
		inline void avx2_horizontal(winding_t& wind, int dir, __m256i vptprevlat, __m256i vptprevlon, __m256i vptlat, __m256i vptlon,
					    uint32_t cond, unsigned int cnt, unsigned int i) const __attribute__((always_inline));
		inline void avx2_upward(winding_t& wind, int dir, __m256i vptprevlat, __m256i vptprevlon, __m256i vptlat, __m256i vptlon,
					uint32_t cond, unsigned int cnt, unsigned int i) const __attribute__((always_inline));
		inline void avx2_downward(winding_t& wind, int dir, __m256i vptprevlat, __m256i vptprevlon, __m256i vptlat, __m256i vptlon,
					  uint32_t cond, unsigned int cnt, unsigned int i) const __attribute__((always_inline));
#endif
	};

	reference operator[](size_type x);
	const_reference operator[](size_type x) const;
	bool is_on_boundary(const Point& pt) const;
	bool is_intersection(const Point& la, const Point& lb) const;
	bool is_intersection(const LineString& ls) const;
	bool is_intersection(const PolygonSimple& ps) const;
	bool is_intersection(const Rect& r) const;
	bool is_strict_intersection(const Point& la, const Point& lb) const;
	bool is_self_intersecting(void) const;
	pointset_t get_intersections(const LineString& x) const;
	bool is_overlap(const Rect& r) const;
	int windingnumber(const Point& pt) const;
	ScanLine scanline(Point::coord_t lat) const;
	inside_t get_inside(const Point& pt0, const Point& pt1) const;
	borderpoints_t get_borderpoints(const Point& pt0, const Point& pt1) const;
	int64_t area2(void) const;
	double spherical_area(void) const;
	Point centroid(void) const;
	int is_box(Rect& bbox) const;
	bool is_counterclockwise(void) const { return area2() >= 0; }
	void make_counterclockwise(void);
	void make_clockwise(void);
	unsigned int next(unsigned int it) const;
	unsigned int prev(unsigned int it) const;
	void remove_redundant_polypoints(void);
	void simplify_outside(const Rect& bbox);
	uint64_t simple_circumference_rel(void) const;
	float simple_circumference_km(void) const;
	float simple_circumference_nmi(void) const { return simple_circumference_km() * Point::km_to_nmi; }
	float spheric_circumference_km(void) const;
	float spheric_circumference_nmi(void) const { return spheric_circumference_km() * Point::km_to_nmi; }
	float spheric_area_km2(void) const;
	float spheric_area_nmi2(void) const;
	double spheric_circumference_km_dbl(void) const;
	double spheric_circumference_nmi_dbl(void) const { return spheric_circumference_km_dbl() * Point::km_to_nmi_dbl; }
	double spheric_area_km2_dbl(void) const;
	double spheric_area_nmi2_dbl(void) const;
	// get_simple_area is signed depending on the turn direction
	double get_simple_area_km2_dbl(void) const;
	double get_simple_area_nmi2_dbl(void) const { return get_simple_area_km2_dbl() * (Point::km_to_nmi_dbl * Point::km_to_nmi_dbl); }
	static PolygonSimple line_width_rectangular(const Point& l0, const Point& l1, double width_nmi);
	void make_fat_lines_helper(MultiPolygonHole& ph, float radius = 5, bool roundcaps = true, float angleincr = 15, bool rawcoord = false) const;
	MultiPolygonHole make_fat_lines(float radius = 5, bool roundcaps = true, float angleincr = 15, bool rawcoord = false) const;
	void nearest(Point& ptn, float& dist, const Point& pt) const;
	void spheric_nearest(Point& ptn, double& dist, const Point& pt) const;
	void nearest(Point& ptn, float& dist, const MultiPoint& pt) const;
	void spheric_nearest(Point& ptn, double& dist, const MultiPoint& pt) const;
	void nearest(Point& ptn, float& dist, const LineString& ls) const;
	void spheric_nearest(Point& ptn, double& dist, const LineString& ls) const;
	void nearest(Point& ptn, float& dist, const MultiLineString& mls) const;
	void spheric_nearest(Point& ptn, double& dist, const MultiLineString& mls) const;
	void nearest(Point& ptn, float& dist, const PolygonSimple& ps) const;
	void spheric_nearest(Point& ptn, double& dist, const PolygonSimple& ps) const;
	bool dwithin(const Point& g1, double dist_nmi) const;
	bool dwithin(const MultiPoint& g1, double dist_nmi) const;
	bool dwithin(const LineString& g1, double dist_nmi) const;
	bool dwithin(const MultiLineString& g1, double dist_nmi) const;
	bool dwithin(const PolygonSimple& g1, double dist_nmi) const;
	void bindblob(sqlite3x::sqlite3_command& cmd, int index) const;
	void getblob(sqlite3x::sqlite3_cursor& cursor, int index);
#ifdef HAVE_PQXX
	pqxx::binarystring bindblob(void) const;
	void getblob(const pqxx::binarystring& blob);
#endif
	void snap_bits(unsigned int nrbits);
	void randomize_bits(unsigned int nrbits);
	unsigned int zap_leastimportant(Point& pt);
	unsigned int zap_leastimportant(void) { Point pt; return zap_leastimportant(pt); }
	std::ostream& print_nrvertices(std::ostream& os, bool prarea = false) const;
	std::string to_skyvector(void) const;
	bool geos_is_valid(void) const;
	void geos_make_valid(void);
	MultiPolygonHole geos_simplify(void);
	MultiPolygonHole geos_simplify(Point::coord_t lonoffs);

	// WKT / WKB
	unsigned int get_wkbpolysize(void) const;
	unsigned int get_wkbsize(void) const;
	uint8_t *wkbencode(uint8_t *cp, uint8_t *ep) const;
	const uint8_t *wkbdecode(const uint8_t *cp, const uint8_t *ep, uint8_t byteorder);
	uint8_t *to_wkb(uint8_t *cp, uint8_t *ep) const;
	std::vector<uint8_t> to_wkb(void) const;
	const uint8_t *from_wkb(const uint8_t *cp, const uint8_t *ep);
	void from_wkb(const std::vector<uint8_t>& b) { from_wkb(&b[0], &b[b.size()]); }
	std::ostream& to_wktpoly(std::ostream& os) const;
	std::ostream& to_wkt(std::ostream& os) const;

	// GeoJSON
#ifdef HAVE_JSONCPP
	Json::Value to_json(void) const;
	void from_json(const Json::Value& g);
#endif

protected:
	bool geos_make_valid_helper(void);
	void nearest_helper(Point& ptn, float& dist, const Point& pt) const;
	void spheric_nearest_helper(Point& ptn, double& dist, const Point& pt) const;
	friend class PolygonHole;
};

class PolygonHole {
public:
	typedef PolygonSimple::ScanLine ScanLine;
	typedef BorderPoint::borderpoints_t borderpoints_t;
	typedef PolygonSimple::inside_t inside_t;

	PolygonHole(const PolygonSimple& extr = PolygonSimple(), const std::vector<PolygonSimple>& intr = std::vector<PolygonSimple>()) : m_exterior(extr), m_interior(intr) {}
	PolygonHole(PolygonSimple&& extr, const std::vector<PolygonSimple>&& intr) : m_exterior(extr), m_interior(intr) {}
	void clear(void) { m_exterior.clear(); m_interior.clear(); }
	void swap(PolygonHole& x) { m_exterior.swap(x.m_exterior); m_interior.swap(x.m_interior); }
	PolygonSimple& get_exterior(void) { return m_exterior; }
	const PolygonSimple& get_exterior(void) const { return m_exterior; }
	void set_exterior(const PolygonSimple& extr) { m_exterior = extr; }
	unsigned int get_nrinterior(void) const { return m_interior.size(); }
	PolygonSimple& operator[](unsigned int x) { return m_interior[x]; }
	const PolygonSimple& operator[](unsigned int x) const { return m_interior[x]; }
	void add_interior(const PolygonSimple& intr) { m_interior.push_back(intr); }
	void insert_interior(const PolygonSimple& intr, int index) { m_interior.insert(m_interior.begin() + index, intr); }
	void erase_interior(int index) { m_interior.erase(m_interior.begin() + index); }
	void clear_interior(void) { m_interior.clear(); }
	bool is_on_boundary(const Point& pt) const;
	bool is_boundary_point(const Point& pt) const;
	bool is_intersection(const Point& la, const Point& lb) const;
	bool is_intersection(const LineString& ls) const;
	bool is_intersection(const PolygonSimple& ps) const;
	bool is_intersection(const Rect& r) const;
	bool is_strict_intersection(const Point& la, const Point& lb) const;
	bool is_all_points_inside(const Rect& r) const;
	bool is_any_point_inside(const Rect& r) const;
	bool is_overlap(const Rect& r) const;
	int windingnumber(const Point& pt) const;
	ScanLine scanline(Point::coord_t lat) const;
	inside_t get_inside(const Point& pt0, const Point& pt1) const;
	borderpoints_t get_borderpoints(const Point& pt0, const Point& pt1) const;
	int64_t area2(void) const;
	double spherical_area(void) const;
	void transform_webmercator(void);
	void invtransform_webmercator(void);
	void approx_webmercator(void);
	void remove_redundant_polypoints(void);
	void simplify_outside(const Rect& bbox);
	void reverse(void);
	Rect get_bbox(void) const { return m_exterior.get_bbox(); }
	bool operator==(const PolygonHole &p) const;
	bool operator!=(const PolygonHole &p) const { return !operator==(p); }
	int compare(const PolygonHole& x) const;
	void make_fat_lines_helper(MultiPolygonHole& ph, float radius = 5, bool roundcaps = true, float angleincr = 15, bool rawcoord = false) const;
	MultiPolygonHole make_fat_lines(float radius = 5, bool roundcaps = true, float angleincr = 15, bool rawcoord = false) const;
	void nearest(Point& ptn, float& dist, const Point& pt) const;
	void spheric_nearest(Point& ptn, double& dist, const Point& pt) const;
	void nearest(Point& ptn, float& dist, const MultiPoint& pt) const;
	void spheric_nearest(Point& ptn, double& dist, const MultiPoint& pt) const;
	void nearest(Point& ptn, float& dist, const LineString& ls) const;
	void spheric_nearest(Point& ptn, double& dist, const LineString& ls) const;
	void nearest(Point& ptn, float& dist, const MultiLineString& mls) const;
	void spheric_nearest(Point& ptn, double& dist, const MultiLineString& mls) const;
	void nearest(Point& ptn, float& dist, const PolygonSimple& ps) const;
	void spheric_nearest(Point& ptn, double& dist, const PolygonSimple& ps) const;
	void nearest(Point& ptn, float& dist, const PolygonHole& ph) const;
	void spheric_nearest(Point& ptn, double& dist, const PolygonHole& ph) const;
	bool dwithin(const Point& g1, double dist_nmi) const;
	bool dwithin(const MultiPoint& g1, double dist_nmi) const;
	bool dwithin(const LineString& g1, double dist_nmi) const;
	bool dwithin(const MultiLineString& g1, double dist_nmi) const;
	bool dwithin(const PolygonSimple& g1, double dist_nmi) const;
	bool dwithin(const PolygonHole& g1, double dist_nmi) const;
	void bindblob(sqlite3x::sqlite3_command& cmd, int index) const;
	void getblob(sqlite3x::sqlite3_cursor& cursor, int index);
#ifdef HAVE_PQXX
	pqxx::binarystring bindblob(void) const;
	void getblob(const pqxx::binarystring& blob);
#endif
	void normalize(void);
	void normalize_boostgeom(void);
	float spheric_area_km2(void) const;
	float spheric_area_nmi2(void) const;
	double spheric_area_km2_dbl(void) const;
	double spheric_area_nmi2_dbl(void) const;
	// get_simple_area is signed depending on the turn direction
	double get_simple_area_km2_dbl(void) const;
	double get_simple_area_nmi2_dbl(void) const { return get_simple_area_km2_dbl() * (Point::km_to_nmi_dbl * Point::km_to_nmi_dbl); }
	void snap_bits(unsigned int nrbits);
	void randomize_bits(unsigned int nrbits);
	unsigned int zap_holes(uint64_t abslim = 0, double rellim = 0);
	void sort_by_area_asc(void);
	void sort_by_area_desc(void);
	std::ostream& print_nrvertices(std::ostream& os, bool prarea = false) const;
	std::ostream& print(std::ostream& os) const;
	bool geos_is_valid(void) const;

	// needed for boost adaptation
	typedef std::vector<PolygonSimple> interior_t;
	interior_t& get_interior(void) { return m_interior; }
	const interior_t& get_interior(void) const { return m_interior; }

	// WKT / WKB
	unsigned int get_wkbsize(void) const;
	uint8_t *wkbencode(uint8_t *cp, uint8_t *ep) const;
	const uint8_t *wkbdecode(const uint8_t *cp, const uint8_t *ep, uint8_t byteorder);
	uint8_t *to_wkb(uint8_t *cp, uint8_t *ep) const;
	std::vector<uint8_t> to_wkb(void) const;
	const uint8_t *from_wkb(const uint8_t *cp, const uint8_t *ep);
	void from_wkb(const std::vector<uint8_t>& b) { from_wkb(&b[0], &b[b.size()]); }
	std::ostream& to_wktpoly(std::ostream& os) const;
	std::ostream& to_wkt(std::ostream& os) const;

	// GeoJSON
#ifdef HAVE_JSONCPP
	Json::Value to_json(void) const;
	void from_json(const Json::Value& g);
#endif

	template<class Archive> void hibernate_binary(Archive& ar) {
		m_exterior.hibernate_binary(ar);
		uint32_t sz(m_interior.size());
		ar.io(sz);
		if (ar.is_load())
			m_interior.resize(sz);
		for (interior_t::iterator i(m_interior.begin()), e(m_interior.end()); i != e; ++i)
			i->hibernate_binary(ar);
	}

protected:
	PolygonSimple m_exterior;
	interior_t m_interior;

	class AreaSorter;
	friend class MultiPolygonHole;
	static unsigned int bloblength(const PolygonSimple& p);
	static uint8_t *blobencode(const PolygonSimple& p, uint8_t *data);
	static uint8_t const *blobdecode(PolygonSimple& p, uint8_t const *data, uint8_t const *end);
};

class MultiPolygonHole : public std::vector<PolygonHole> {
public:
	typedef PolygonSimple::ScanLine ScanLine;
	typedef BorderPoint::borderpoints_t borderpoints_t;
	typedef PolygonSimple::inside_t inside_t;

	MultiPolygonHole();
	MultiPolygonHole(const PolygonHole& p);
	MultiPolygonHole(PolygonHole&& p);
	bool is_on_boundary(const Point& pt) const;
	bool is_boundary_point(const Point& pt) const;
	bool is_intersection(const Point& la, const Point& lb) const;
	bool is_intersection(const LineString& ls) const;
	bool is_intersection(const PolygonSimple& ps) const;
	bool is_intersection(const Rect& r) const;
	bool is_strict_intersection(const Point& la, const Point& lb) const;
	bool is_all_points_inside(const Rect& r) const;
	bool is_any_point_inside(const Rect& r) const;
	bool is_overlap(const Rect& r) const;
	int windingnumber(const Point& pt) const;
	ScanLine scanline(Point::coord_t lat) const;
	inside_t get_inside(const Point& pt0, const Point& pt1) const;
	borderpoints_t get_borderpoints(const Point& pt0, const Point& pt1) const;
	int64_t area2(void) const;
	double spherical_area(void) const;
	void transform_webmercator(void);
	void invtransform_webmercator(void);
	void approx_webmercator(void);
	void remove_redundant_polypoints(void);
	void simplify_outside(const Rect& bbox);
	Rect get_bbox(void) const;
	bool operator==(const MultiPolygonHole &p) const;
	bool operator!=(const MultiPolygonHole &p) const { return !operator==(p); }
	int compare(const MultiPolygonHole& x) const;
	void make_fat_lines_helper(MultiPolygonHole& ph, float radius = 5, bool roundcaps = true, float angleincr = 15, bool rawcoord = false) const;
	MultiPolygonHole make_fat_lines(float radius = 5, bool roundcaps = true, float angleincr = 15, bool rawcoord = false) const;
	void nearest(Point& ptn, float& dist, const Point& pt) const;
	void spheric_nearest(Point& ptn, double& dist, const Point& pt) const;
	void nearest(Point& ptn, float& dist, const MultiPoint& pt) const;
	void spheric_nearest(Point& ptn, double& dist, const MultiPoint& pt) const;
	void nearest(Point& ptn, float& dist, const LineString& ls) const;
	void spheric_nearest(Point& ptn, double& dist, const LineString& ls) const;
	void nearest(Point& ptn, float& dist, const MultiLineString& mls) const;
	void spheric_nearest(Point& ptn, double& dist, const MultiLineString& mls) const;
	void nearest(Point& ptn, float& dist, const PolygonSimple& ps) const;
	void spheric_nearest(Point& ptn, double& dist, const PolygonSimple& ps) const;
	void nearest(Point& ptn, float& dist, const PolygonHole& ph) const;
	void spheric_nearest(Point& ptn, double& dist, const PolygonHole& ph) const;
	void nearest(Point& ptn, float& dist, const MultiPolygonHole& mph) const;
	void spheric_nearest(Point& ptn, double& dist, const MultiPolygonHole& mph) const;
	bool dwithin(const Point& g1, double dist_nmi) const;
	bool dwithin(const MultiPoint& g1, double dist_nmi) const;
	bool dwithin(const LineString& g1, double dist_nmi) const;
	bool dwithin(const MultiLineString& g1, double dist_nmi) const;
	bool dwithin(const PolygonSimple& g1, double dist_nmi) const;
	bool dwithin(const PolygonHole& g1, double dist_nmi) const;
	bool dwithin(const MultiPolygonHole& g1, double dist_nmi) const;
	void bindblob(sqlite3x::sqlite3_command& cmd, int index) const;
	void getblob(sqlite3x::sqlite3_cursor& cursor, int index);
#ifdef HAVE_PQXX
	pqxx::binarystring bindblob(void) const;
	void getblob(const pqxx::binarystring& blob);
#endif
	void normalize(void);
	void normalize_boostgeom(void);
	float spheric_area_km2(void) const;
	float spheric_area_nmi2(void) const;
	double spheric_area_km2_dbl(void) const;
	double spheric_area_nmi2_dbl(void) const;
	// get_simple_area is signed depending on the turn direction
	double get_simple_area_km2_dbl(void) const;
	double get_simple_area_nmi2_dbl(void) const { return get_simple_area_km2_dbl() * (Point::km_to_nmi_dbl * Point::km_to_nmi_dbl); }
	void snap_bits(unsigned int nrbits);
	void randomize_bits(unsigned int nrbits);
	unsigned int zap_holes(uint64_t abslim = 0, double rellim = 0);
	unsigned int zap_polys(uint64_t abslim = 0, double rellim = 0);
	void sort_by_area_asc(void);
	void sort_by_area_desc(void);
	std::ostream& print_nrvertices(std::ostream& os, bool prarea = false) const;
	std::ostream& print(std::ostream& os) const;
	bool geos_is_valid(void) const;
	void geos_make_valid(void);
	void geos_union(const MultiPolygonHole& m);
	void geos_union(const MultiPolygonHole& m, Point::coord_t lonoffs);
	void geos_subtract(const MultiPolygonHole& m);
	void geos_subtract(const MultiPolygonHole& m, Point::coord_t lonoffs);
	void geos_intersect(const MultiPolygonHole& m);
	void geos_intersect(const MultiPolygonHole& m, Point::coord_t lonoffs);
	bool is_intersect_fat_line(const Point& pa, const Point& pb, float radius) const;
	bool is_intersect_circle(const Point& pa, float radius) const;

	// WKT / WKB
	unsigned int get_wkbsize(void) const;
	uint8_t *wkbencode(uint8_t *cp, uint8_t *ep) const;
	const uint8_t *wkbdecode(const uint8_t *cp, const uint8_t *ep, uint8_t byteorder);
	uint8_t *to_wkb(uint8_t *cp, uint8_t *ep) const;
	std::vector<uint8_t> to_wkb(void) const;
	const uint8_t *from_wkb(const uint8_t *cp, const uint8_t *ep);
	void from_wkb(const std::vector<uint8_t>& b) { from_wkb(&b[0], &b[b.size()]); }
	std::ostream& to_wkt(std::ostream& os) const;

	// GeoJSON
#ifdef HAVE_JSONCPP
	Json::Value to_json(void) const;
	void from_json(const Json::Value& g);
#endif

	template<class Archive> void hibernate_binary(Archive& ar) {
		uint32_t sz(size());
		ar.io(sz);
		if (ar.is_load())
			resize(sz);
		for (iterator i(begin()), e(end()); i != e; ++i)
			i->hibernate_binary(ar);
	}

protected:
	class AreaSorter;

	static unsigned int bloblength(const PolygonHole& p);
	static uint8_t *blobencode(const PolygonHole& p, uint8_t *data);
	static uint8_t const *blobdecode(PolygonHole& p, uint8_t const *data, uint8_t const *end);
};

class Point3D : public Point {
public:
	typedef int32_t alt_t;
	static const alt_t invalid_alt;

	Point3D(coord_t lo = 0, coord_t la = 0, alt_t alt = invalid_alt) : Point(lo, la), m_alt(alt) {}
	Point3D(const Point& pt, alt_t alt = invalid_alt) : Point(pt), m_alt(alt) {}
	void set_invalid(void);
	bool is_alt_valid(void) const { return get_alt() != invalid_alt; }
	alt_t get_alt(void) const { return m_alt; }
	void set_alt(alt_t a) { m_alt = a; }
	void unset_alt(void) { m_alt = invalid_alt; }
	bool is_valid_3d(void) const { return !is_invalid() && is_alt_valid(); }

	std::ostream& print(std::ostream& os) const;

	std::ostream& to_wktpt(std::ostream& os) const;
	std::ostream& to_wkt(std::ostream& os) const;

	// GeoJSON
#ifdef HAVE_JSONCPP
	Json::Value to_json(void) const;
	void from_json(const Json::Value& g);
#endif

	template<class Archive> void hibernate_binary(Archive& ar) {
		Point::hibernate_binary(ar);
		ar.io(m_alt);
	}

protected:
	alt_t m_alt;
};

class LineString3D : public std::vector<Point3D> {
public:
	LineString3D(void);
	LineString3D(const LineString& ls);
	Rect get_bbox(void) const;
	operator LineString(void) const;

	std::ostream& print(std::ostream& os) const;

	std::ostream& to_wktlst(std::ostream& os) const;
	std::ostream& to_wkt(std::ostream& os) const;

	// GeoJSON
#ifdef HAVE_JSONCPP
	Json::Value to_json(void) const;
	void from_json(const Json::Value& g);
#endif

	template<class Archive> void hibernate_binary(Archive& ar) {
		uint32_t sz(size());
		ar.io(sz);
		if (ar.is_load())
			resize(sz);
		for (iterator i(begin()), e(end()); i != e; ++i)
			i->hibernate_binary(ar);
	}
};

class PolygonSimple3D : public LineString3D {
public:
	typedef LineString3D base_t;

	PolygonSimple3D(void);
	PolygonSimple3D(const PolygonSimple& ps);

	reference operator[](size_type x);
	const_reference operator[](size_type x) const;
	operator PolygonSimple(void) const;

	std::ostream& to_wktpoly(std::ostream& os) const;
	std::ostream& to_wkt(std::ostream& os) const;

	// GeoJSON
#ifdef HAVE_JSONCPP
	Json::Value to_json(void) const;
	void from_json(const Json::Value& g);
#endif
};

class IntervalBoundingBox {
public:
	IntervalBoundingBox(void);
	operator Rect(void) const;
	void add(const Rect& r);
	void add(const Point& p);
	void add(const Point& p0, const Point& p1);
	void add(const PolygonSimple& p);
	void add(const PolygonHole& p);
	void add(const MultiPolygonHole& p);

protected:
	typedef IntervalSet<int64_t> int_t;
	static const int_t::type_t lmin;
	static const int_t::type_t lmax;
	static const int_t::type_t lone = 1;
	static const int_t::type_t ltotal = lone << 32;
	int_t m_lonset;
	Point::coord_t m_latmin;
	Point::coord_t m_latmax;
};

class Geometry;

class GeometryCollection {
public:
	GeometryCollection(void);
	~GeometryCollection();
	GeometryCollection(const Geometry& p);
	GeometryCollection(const GeometryCollection& g);
	GeometryCollection(const Point& pt);
	GeometryCollection(const LineString& ls);
	GeometryCollection(const PolygonSimple& ph);
	GeometryCollection(const PolygonHole& ph);
	GeometryCollection(const MultiPoint& pt);
	GeometryCollection(const MultiLineString& ls);
	GeometryCollection(const MultiPolygonHole& ph);
	GeometryCollection(const MultiPoint& pt, const MultiLineString& ls, const MultiPolygonHole& poly);
	GeometryCollection& operator=(const Geometry& p);
	GeometryCollection& operator=(const GeometryCollection& g);
	GeometryCollection& operator=(const Point& pt);
	GeometryCollection& operator=(const LineString& ls);
	GeometryCollection& operator=(const PolygonSimple& ph);
	GeometryCollection& operator=(const PolygonHole& ph);
	GeometryCollection& operator=(const MultiPoint& pt);
	GeometryCollection& operator=(const MultiLineString& ls);
	GeometryCollection& operator=(const MultiPolygonHole& ph);
	GeometryCollection& append(const Geometry& p);
	GeometryCollection& append(const GeometryCollection& g);
	GeometryCollection& append(const Point& pt);
	GeometryCollection& append(const LineString& ls);
	GeometryCollection& append(const PolygonSimple& ph);
	GeometryCollection& append(const PolygonHole& ph);
	GeometryCollection& append(const MultiPoint& pt);
	GeometryCollection& append(const MultiLineString& ls);
	GeometryCollection& append(const MultiPolygonHole& ph);
	std::size_t size(void) const { return m_el.size(); }
	const Geometry& operator[](std::size_t i) const;
	void clear(void) { m_el.clear(); }
	void swap(GeometryCollection& x) { m_el.swap(x.m_el); }
	Rect get_bbox(void) const;
	int64_t area2(void) const;
	double spherical_area(void) const;
	void transform_webmercator(void);
	void invtransform_webmercator(void);
	void approx_webmercator(void);
	void nearest(Point& ptn, float& dist, const Geometry& g) const;
	void spheric_nearest(Point& ptn, double& dist, const Geometry& g) const;
	void nearest(Point& ptn, float& dist, const GeometryCollection& g) const;
	void spheric_nearest(Point& ptn, double& dist, const GeometryCollection& g) const;
	void nearest(Point& ptn, float& dist, const Point& pt) const;
	void spheric_nearest(Point& ptn, double& dist, const Point& pt) const;
	void nearest(Point& ptn, float& dist, const LineString& ls) const;
	void spheric_nearest(Point& ptn, double& dist, const LineString& ls) const;
	void nearest(Point& ptn, float& dist, const PolygonSimple& ps) const;
	void spheric_nearest(Point& ptn, double& dist, const PolygonSimple& ps) const;
	void nearest(Point& ptn, float& dist, const PolygonHole& ph) const;
	void spheric_nearest(Point& ptn, double& dist, const PolygonHole& ph) const;
	void nearest(Point& ptn, float& dist, const MultiPoint& pt) const;
	void spheric_nearest(Point& ptn, double& dist, const MultiPoint& pt) const;
	void nearest(Point& ptn, float& dist, const MultiLineString& mls) const;
	void spheric_nearest(Point& ptn, double& dist, const MultiLineString& mls) const;
	void nearest(Point& ptn, float& dist, const MultiPolygonHole& mph) const;
	void spheric_nearest(Point& ptn, double& dist, const MultiPolygonHole& mph) const;
	void nearest(Point& ptn, float& dist, const Rect& r) const;
	void spheric_nearest(Point& ptn, double& dist, const Rect& r) const;
	bool dwithin(const Geometry& g1, double dist_nmi) const;
	bool dwithin(const GeometryCollection& g1, double dist_nmi) const;
	bool dwithin(const Point& g1, double dist_nmi) const;
	bool dwithin(const MultiPoint& g1, double dist_nmi) const;
	bool dwithin(const LineString& g1, double dist_nmi) const;
	bool dwithin(const MultiLineString& g1, double dist_nmi) const;
	bool dwithin(const PolygonSimple& g1, double dist_nmi) const;
	bool dwithin(const PolygonHole& g1, double dist_nmi) const;
	bool dwithin(const MultiPolygonHole& g1, double dist_nmi) const;
	Geometry flatten(void) const;
	Geometry coalesce(void) const;
	void coalesce(MultiPoint& pt, MultiLineString& ls, MultiPolygonHole& poly) const;
	bool is_overlap(const Geometry& g) const;
	bool is_overlap(const GeometryCollection& g) const;
	bool is_overlap(const Point& pt);
	bool is_overlap(const LineString& ls);
	bool is_overlap(const PolygonSimple& ph);
	bool is_overlap(const PolygonHole& ph);
	bool is_overlap(const MultiPoint& pt);
	bool is_overlap(const MultiLineString& ls);
	bool is_overlap(const MultiPolygonHole& ph);
	bool is_overlap(const Rect& r) const;

	// WKT / WKB
	unsigned int get_wkbsize(void) const;
	uint8_t *wkbencode(uint8_t *cp, uint8_t *ep) const;
	const uint8_t *wkbdecode(const uint8_t *cp, const uint8_t *ep, uint8_t byteorder);
	uint8_t *to_wkb(uint8_t *cp, uint8_t *ep) const;
	std::vector<uint8_t> to_wkb(void) const;
	const uint8_t *from_wkb(const uint8_t *cp, const uint8_t *ep);
	void from_wkb(const std::vector<uint8_t>& b) { from_wkb(&b[0], &b[b.size()]); }
	std::ostream& to_wkt(std::ostream& os) const;

	// GeoJSON
#ifdef HAVE_JSONCPP
	Json::Value to_json(void) const;
	void from_json(const Json::Value& g);
#endif

protected:
	typedef std::vector<Geometry> el_t;
	el_t m_el;
};

class Geometry {
public:
	enum class type_t : uint8_t {
		invalid            = 0,
		point              = 1,
		linestring         = 2,
		polygon            = 3,
		multipoint         = 4,
		multilinestring    = 5,
		multipolygon       = 6,
		geometrycollection = 7,
		last               = geometrycollection
	};

	Geometry(void);
	Geometry(const Point& x);
	Geometry(const LineString& x);
	Geometry(const PolygonSimple& x);
	Geometry(const PolygonHole& x);
	Geometry(const MultiPoint& x);
	Geometry(const MultiLineString& x);
	Geometry(const MultiPolygonHole& x);
	Geometry(const GeometryCollection& x);
	Geometry(const Geometry& x);
	Geometry(Point&& x);
	Geometry(LineString&& x);
	Geometry(PolygonSimple&& x);
	Geometry(PolygonHole&& x);
	Geometry(MultiPoint&& x);
	Geometry(MultiLineString&& x);
	Geometry(MultiPolygonHole&& x);
	Geometry(GeometryCollection&& x);
	Geometry(Geometry&& x);
	Geometry(const MultiPoint& pt, const MultiLineString& ls, const MultiPolygonHole& poly);
	~Geometry();

	Geometry& operator=(const Point& x);
	Geometry& operator=(const LineString& x);
	Geometry& operator=(const PolygonSimple& x);
	Geometry& operator=(const PolygonHole& x);
	Geometry& operator=(const MultiPoint& x);
	Geometry& operator=(const MultiLineString& x);
	Geometry& operator=(const MultiPolygonHole& x);
	Geometry& operator=(const GeometryCollection& x);
	Geometry& operator=(const Geometry& x);
	Geometry& operator=(Point&& x);
	Geometry& operator=(LineString&& x);
	Geometry& operator=(PolygonSimple&& x);
	Geometry& operator=(PolygonHole&& x);
	Geometry& operator=(MultiPoint&& x);
	Geometry& operator=(MultiLineString&& x);
	Geometry& operator=(MultiPolygonHole&& x);
	Geometry& operator=(GeometryCollection&& x);
	Geometry& operator=(Geometry&& x);

	void swap(Geometry& g);

	static Geometry create(const Point& x) { return Geometry(x); }
	static Geometry create(const LineString& x) { return Geometry(x); }
	static Geometry create(const PolygonSimple& x) { return Geometry(x); }
	static Geometry create(const PolygonHole& x) { return Geometry(x); }
	static Geometry create(const MultiPoint& x) { return Geometry(x); }
	static Geometry create(const MultiLineString& x) { return Geometry(x); }
	static Geometry create(const MultiPolygonHole& x) { return Geometry(x); }
	static Geometry create(const GeometryCollection& x) { return Geometry(x); }

	type_t get_type(void) const { return type_t::invalid; }
	operator const Point& (void) const;
	operator const LineString& (void) const;
	operator const PolygonHole& (void) const;
	operator const MultiPoint& (void) const;
	operator const MultiLineString& (void) const;
	operator const MultiPolygonHole& (void) const;
	operator const GeometryCollection& (void) const;

	Rect get_bbox(void) const;
	int64_t area2(void) const;
	double spherical_area(void) const;
	void transform_webmercator(void);
	void invtransform_webmercator(void);
	void approx_webmercator(void);
	void nearest(Point& ptn, float& dist, const Geometry& g) const;
	void spheric_nearest(Point& ptn, double& dist, const Geometry& g) const;
	void nearest(Point& ptn, float& dist, const GeometryCollection& g) const;
	void spheric_nearest(Point& ptn, double& dist, const GeometryCollection& g) const;
	void nearest(Point& ptn, float& dist, const Point& pt) const;
	void spheric_nearest(Point& ptn, double& dist, const Point& pt) const;
	void nearest(Point& ptn, float& dist, const LineString& ls) const;
	void spheric_nearest(Point& ptn, double& dist, const LineString& ls) const;
	void nearest(Point& ptn, float& dist, const PolygonSimple& ps) const;
	void spheric_nearest(Point& ptn, double& dist, const PolygonSimple& ps) const;
	void nearest(Point& ptn, float& dist, const PolygonHole& ph) const;
	void spheric_nearest(Point& ptn, double& dist, const PolygonHole& ph) const;
	void nearest(Point& ptn, float& dist, const MultiPoint& pt) const;
	void spheric_nearest(Point& ptn, double& dist, const MultiPoint& pt) const;
	void nearest(Point& ptn, float& dist, const MultiLineString& mls) const;
	void spheric_nearest(Point& ptn, double& dist, const MultiLineString& mls) const;
	void nearest(Point& ptn, float& dist, const MultiPolygonHole& mph) const;
	void spheric_nearest(Point& ptn, double& dist, const MultiPolygonHole& mph) const;
	void nearest(Point& ptn, float& dist, const Rect& r) const;
	void spheric_nearest(Point& ptn, double& dist, const Rect& r) const;
	bool dwithin(const Geometry& g1, double dist_nmi) const;
	bool dwithin(const GeometryCollection& g1, double dist_nmi) const;
	bool dwithin(const Point& g1, double dist_nmi) const;
	bool dwithin(const MultiPoint& g1, double dist_nmi) const;
	bool dwithin(const LineString& g1, double dist_nmi) const;
	bool dwithin(const MultiLineString& g1, double dist_nmi) const;
	bool dwithin(const PolygonSimple& g1, double dist_nmi) const;
	bool dwithin(const PolygonHole& g1, double dist_nmi) const;
	bool dwithin(const MultiPolygonHole& g1, double dist_nmi) const;
	Geometry flatten(void) const;
	Geometry coalesce(void) const;
	void coalesce(MultiPoint& pt, MultiLineString& ls, MultiPolygonHole& poly) const;
	bool is_overlap(const Geometry& g) const;
	bool is_overlap(const Point& pt);
	bool is_overlap(const LineString& ls);
	bool is_overlap(const PolygonSimple& ph);
	bool is_overlap(const PolygonHole& ph);
	bool is_overlap(const MultiPoint& pt);
	bool is_overlap(const MultiLineString& ls);
	bool is_overlap(const MultiPolygonHole& ph);
	bool is_overlap(const Rect& r) const;

	// WKT / WKB helper
	static unsigned int get_wkbhdrsize(void) { return 5; }
	static uint8_t *wkbencode(uint8_t *cp, uint8_t *ep, type_t typ);
	static uint8_t *wkbencode(uint8_t *cp, uint8_t *ep, uint8_t x);
	static uint8_t *wkbencode(uint8_t *cp, uint8_t *ep, uint32_t x);
	static uint8_t *wkbencode(uint8_t *cp, uint8_t *ep, double x);
	static const uint8_t *wkbdecode(const uint8_t *cp, const uint8_t *ep, type_t& typ, uint8_t& byteorder);
	static const uint8_t *wkbdecode(const uint8_t *cp, const uint8_t *ep, uint8_t& x, uint8_t byteorder);
	static const uint8_t *wkbdecode(const uint8_t *cp, const uint8_t *ep, uint32_t& x, uint8_t byteorder);
	static const uint8_t *wkbdecode(const uint8_t *cp, const uint8_t *ep, double& x, uint8_t byteorder);

	static Geometry create_from_wkb(const uint8_t * &cpout, const uint8_t *cp, const uint8_t *ep);
	static Geometry create_from_wkb(const uint8_t * &cpout, const uint8_t *cp, const uint8_t *ep, uint8_t byteorder);
	static Geometry create_from_wkb(const uint8_t * &cpout, const uint8_t *cp, const uint8_t *ep, type_t typ, uint8_t byteorder);

	// WKT / WKB
	unsigned int get_wkbsize(void) const;
	uint8_t *wkbencode(uint8_t *cp, uint8_t *ep) const;
	const uint8_t *wkbdecode(const uint8_t *cp, const uint8_t *ep, uint8_t byteorder);
	uint8_t *to_wkb(uint8_t *cp, uint8_t *ep) const;
	const uint8_t *from_wkb(const uint8_t *cp, const uint8_t *ep);
	std::ostream& to_wkt(std::ostream& os) const;

	// GeoJSON
#ifdef HAVE_JSONCPP
	Json::Value to_json(void) const;
	static Geometry create(const Json::Value& g);
#endif

	static type_t parse_type(const std::string& typ);

protected:
	union {
		Point m_point;
		LineString m_linestring;
		PolygonHole m_polygonhole;
		MultiPoint m_multipoint;
		MultiLineString m_multilinestring;
		MultiPolygonHole m_multipolygonhole;
		GeometryCollection m_geometrycollection;
	};
	type_t m_type;

	void dealloc(void);
};

template <typename T>
class PolarVector3D;

template <typename T>
class CartesianVector3D {
public:
	typedef T float_t;
	static constexpr float_t from_deg     = M_PI / 180.0;

	CartesianVector3D(float_t x = 0.0, float_t y = 0.0, float_t z = 0.0) { m_v[0] = x; m_v[1] = y; m_v[2] = z; }
	float_t getx(void) const { return m_v[0]; }
	float_t gety(void) const { return m_v[1]; }
	float_t getz(void) const { return m_v[2]; }
	void setx(float_t v) { m_v[0] = v; }
	void sety(float_t v) { m_v[1] = v; }
	void setz(float_t v) { m_v[2] = v; }
	CartesianVector3D<T>& operator+=(const CartesianVector3D<T>& v);
	CartesianVector3D<T>& operator-=(const CartesianVector3D<T>& v);
	CartesianVector3D<T>& operator*=(float_t v);
	CartesianVector3D<T> operator+(const CartesianVector3D<T>& v) const { CartesianVector3D<T> pv(*this); pv += v; return pv; }
	CartesianVector3D<T> operator-(const CartesianVector3D<T>& v) const { CartesianVector3D<T> pv(*this); pv -= v; return pv; }
	CartesianVector3D<T> operator-(void) const { CartesianVector3D<T> pv; pv -= *this; return pv; }
	CartesianVector3D<T> operator*(float_t v) const { CartesianVector3D<T> pv(*this); pv *= v; return pv; }
	float_t scalarprod(const CartesianVector3D<T>& v) const;
	CartesianVector3D<T> crossprod(const CartesianVector3D<T>& v) const;
	float_t length2(void) const;
	float_t length(void) const { return sqrt(length2()); }
	float_t nearest_fraction(const CartesianVector3D<T>& v0, const CartesianVector3D<T>& v1);
	void rotate_xz(float_t ang);
	void rotate_xy(float_t ang);
	operator PolarVector3D<T>(void) const;

	template<class Archive> void hibernate_binary(Archive& ar) {
		for (int i = 0; i < 3; ++i)
			ar.io(m_v[i]);
	}

protected:
	float_t m_v[3];
};

template <typename T>
class PolarVector3D {
public:
	typedef T float_t;
	static constexpr float_t earth_radius = 6378.155 * 1000.0; /* earth_radius in m */
	static constexpr float_t km_to_nmi    = 0.5399568;
	static constexpr float_t m_to_ft      = 3.2808399;
	static constexpr float_t ft_to_m      = 0.3048;
	static constexpr float_t from_deg     = M_PI / 180.0;
	static constexpr float_t to_deg       = 180.0 / M_PI;

	PolarVector3D(float_t lat = 0, float_t lon = 0, float_t r = 0) : m_lat(lat), m_lon(lon), m_r(r) {}
	PolarVector3D(const Point& pt, int32_t altft = 0);
	float_t getlat(void) const { return m_lat; }
	float_t getlatdeg(void) const { return m_lat * to_deg; }
	float_t getlon(void) const { return m_lon; }
	float_t getlondeg(void) const { return m_lon * to_deg; }
	Point getcoord(void) const;
	float_t getalt(void) const { return m_r - earth_radius; }
	float_t getaltft(void) const { return (m_r - earth_radius) * m_to_ft; }
	float_t getr(void) const { return m_r; }
	void setlat(float_t v) { m_lat = v; }
	void setlatdeg(float_t v) { m_lat = v * from_deg; }
	void setlon(float_t v) { m_lon = v; }
	void setlondeg(float_t v) { m_lon = v * from_deg; }
	void setcoord(const Point& pt) { m_lon = pt.get_lon_rad(); m_lat = pt.get_lat_rad(); }
	void setr(float_t v) { m_r = v; }
	void setalt(float_t v) { m_r = v + earth_radius; }
	void setaltft(float_t v) { m_r = v * ft_to_m + earth_radius; }
	PolarVector3D<T>& operator*=(float_t s) { m_r *= s; return *this; }
	PolarVector3D<T> operator*(float_t s) const { PolarVector3D<T> pv(*this); pv *= s; return pv; }
	operator CartesianVector3D<T>(void) const;

	template<class Archive> void hibernate_binary(Archive& ar) {
		ar.io(m_lat);
		ar.io(m_lon);
		ar.io(m_r);
	}

protected:
	float_t m_lat, m_lon, m_r;
};

typedef CartesianVector3D<float> CartesianVector3Df;
typedef PolarVector3D<float> PolarVector3Df;
typedef CartesianVector3D<double> CartesianVector3Dd;
typedef PolarVector3D<double> PolarVector3Dd;




std::ostream& operator<<(std::ostream& os, const Point& p);
std::ostream& operator<<(std::ostream& os, const Rect& r);

const std::string& to_str(BorderPoint::flag_t f);
inline std::ostream& operator<<(std::ostream& os, BorderPoint::flag_t f) { return os << to_str(f); }
std::string to_str(PolygonSimple::inside_t i);
inline std::ostream& operator<<(std::ostream& os, PolygonSimple::inside_t i) { return os << to_str(i); }

inline PolygonSimple::inside_t operator|(PolygonSimple::inside_t x, PolygonSimple::inside_t y) { return (PolygonSimple::inside_t)((unsigned int)x | (unsigned int)y); }
inline PolygonSimple::inside_t operator&(PolygonSimple::inside_t x, PolygonSimple::inside_t y) { return (PolygonSimple::inside_t)((unsigned int)x & (unsigned int)y); }
inline PolygonSimple::inside_t operator^(PolygonSimple::inside_t x, PolygonSimple::inside_t y) { return (PolygonSimple::inside_t)((unsigned int)x ^ (unsigned int)y); }
inline PolygonSimple::inside_t operator~(PolygonSimple::inside_t x){ return (PolygonSimple::inside_t)~(unsigned int)x; }
inline PolygonSimple::inside_t& operator|=(PolygonSimple::inside_t& x, PolygonSimple::inside_t y) { x = x | y; return x; }
inline PolygonSimple::inside_t& operator&=(PolygonSimple::inside_t& x, PolygonSimple::inside_t y) { x = x & y; return x; }
inline PolygonSimple::inside_t& operator^=(PolygonSimple::inside_t& x, PolygonSimple::inside_t y) { x = x ^ y; return x; }

inline BorderPoint::flagmask_t operator|(BorderPoint::flagmask_t x, BorderPoint::flagmask_t y) { return (BorderPoint::flagmask_t)((unsigned int)x | (unsigned int)y); }
inline BorderPoint::flagmask_t operator&(BorderPoint::flagmask_t x, BorderPoint::flagmask_t y) { return (BorderPoint::flagmask_t)((unsigned int)x & (unsigned int)y); }
inline BorderPoint::flagmask_t operator^(BorderPoint::flagmask_t x, BorderPoint::flagmask_t y) { return (BorderPoint::flagmask_t)((unsigned int)x ^ (unsigned int)y); }
inline BorderPoint::flagmask_t operator~(BorderPoint::flagmask_t x){ return (BorderPoint::flagmask_t)~(unsigned int)x; }
inline BorderPoint::flagmask_t& operator|=(BorderPoint::flagmask_t& x, BorderPoint::flagmask_t y) { x = x | y; return x; }
inline BorderPoint::flagmask_t& operator&=(BorderPoint::flagmask_t& x, BorderPoint::flagmask_t y) { x = x & y; return x; }
inline BorderPoint::flagmask_t& operator^=(BorderPoint::flagmask_t& x, BorderPoint::flagmask_t y) { x = x ^ y; return x; }

const std::string& to_str(Geometry::type_t t);
inline std::ostream& operator<<(std::ostream& os, Geometry::type_t t) { return os << to_str(t); }

#endif /* GEOM_H */
