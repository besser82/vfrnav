//
// C++ Interface: geomgdal
//
// Description:
//
//
// Author: Thomas Sailer <t.sailer@alumni.ethz.ch>, (C) 2018
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef GEOMGDAL_H
#define GEOMGDAL_H

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef HAVE_GDAL

#include <ogr_geometry.h>
#include "geom.h"

OGRPoint *to_ogr_geometry(const Point& pt);
OGRLinearRing *to_ogr_geometry(const Rect& r);
OGRMultiPoint *to_ogr_geometry(const MultiPoint& mp);
OGRLineString *to_ogr_geometry(const LineString& ls, Point& refpt);
OGRLineString *to_ogr_geometry(const LineString& ls);
OGRMultiLineString *to_ogr_geometry(const MultiLineString& mls, Point& refpt);
OGRMultiLineString *to_ogr_geometry(const MultiLineString& mls);
OGRLinearRing *to_ogr_geometry(const PolygonSimple& ps, Point& refpt);
OGRLinearRing *to_ogr_geometry(const PolygonSimple& ps);
OGRPolygon *to_ogr_geometry(const PolygonHole& ph, Point& refpt);
OGRPolygon *to_ogr_geometry(const PolygonHole& ph);
OGRMultiPolygon *to_ogr_geometry(const MultiPolygonHole& mph, Point& refpt);
OGRMultiPolygon *to_ogr_geometry(const MultiPolygonHole& mph);

#endif /* HAVE_GDAL */
#endif /* GEOMGDAL_H */
