/*****************************************************************************/

/*
 *      mapfile.cc  --  Memory Mapping of files.
 *
 *      Copyright (C) 2020  Thomas Sailer (t.sailer@alumni.ethz.ch)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

/*****************************************************************************/

#include <iomanip>
#include <fstream>

#if defined(HAVE_WINDOWS_H)
#include <windows.h>
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

#include "mapfile.h"

MapFile::MapFile(void)
	: m_ptr(nullptr), m_filesize(0), m_mapsize(0)
{
}

MapFile::MapFile(int fd, bool writable, const char *descr)
	: MapFile()
{
	open(fd, writable, descr);
}

MapFile::MapFile(int fd, std::size_t sz, bool writable, const char *descr)
	: MapFile()
{
	open(fd, sz, writable, descr);
}

MapFile::MapFile(const char *filename, bool writable, const char *descr)
	: MapFile()
{
	open(filename, writable, descr);
}

MapFile::MapFile(const char *filename, std::size_t sz, bool writable, const char *descr)
	: MapFile()
{
	open(filename, sz, writable, descr);
}

MapFile::~MapFile(void)
{
	close();
}

void MapFile::throw_strerror(const char *descr)
{
	std::ostringstream oss;
	oss << "Error: ";
	if (descr)
		oss << descr << ": ";
	oss << strerror(errno) << " (" << errno << ')';
	throw std::runtime_error(oss.str());
}

void MapFile::throw_strerror(const char *what, const char *file, const char *descr)
{
	std::ostringstream oss;
	oss << (what ? what : "unknown operation");
	if (file)
		oss << " (" << file << ')';
	if (descr)
		oss << " (" << descr << ')';
	throw_strerror(oss.str().c_str());
}

#if defined(HAVE_WINDOWS_H)

void MapFile::throw_lasterror(const char *descr)
{
	std::ostringstream oss;
	oss << "Error: ";
	if (descr)
		oss << descr << ": ";
	oss << "0x" << std::hex << GetLastError();
	throw std::runtime_error(oss.str());
}

void MapFile::throw_lasterror(const char *what, const char *file, const char *descr)
{
	std::ostringstream oss;
	oss << (what ? what : "unknown operation");
	if (file)
		oss << " (" << file << ')';
	if (descr)
		oss << " (" << descr << ')';
	throw_lasterror(oss.str().c_str());
}

void MapFile::open(int fd, bool writable, const char *descr)
{
	close();
	if (fd == -1)
		return;
	HANDLE hFile(static_cast<HANDLE>(_get_osfhandle(fd)));
	if (hFile == INVALID_HANDLE_VALUE)
		throw_strerror("Open file descriptor", 0, descr);
	{
		LARGE_INTEGER sz(0);
		if (!GetFileSizeEx(hFile, &sz))
			throw_lasterror("Getting file size", 0, descr), 0, descr;
		m_filesize = sz;
	}
	HANDLE hMap(CreateFileMapping(hFile, 0, writable ? PAGE_READWRITE : PAGE_READONLY, 0, 0, 0));
	if (hMap == INVALID_HANDLE_VALUE) {
		m_filesize = 0;
		throw_lasterror("Mapping file", 0, descr);
	}
	m_ptr = reinterpret_cast<uint8_t *>(MapViewOfFile(hMap, writable ? FILE_MAP_WRITE : FILE_MAP_READ, 0, 0, 0));
	CloseHandle(hMap);
	MEMORY_BASIC_INFORMATION meminfo;
	if (VirtualQuery(m_binfile, &meminfo, sizeof(meminfo)) != sizeof(meminfo)) {
		UnmapViewOfFile(m_ptr);
		m_ptr = nullptr;
		m_filesize = 0;
		throw std::runtime_error("Error: Cannot get file mapping information", 0, descr);
	}
	m_mapsize = meminfo.RegionSize;
}

void MapFile::open(int fd, std::size_t sz, bool writable, const char *descr)
{
	close();
	if (fd == -1)
		return;
	HANDLE hFile(static_cast<HANDLE>(_get_osfhandle(fd)));
	if (hFile == INVALID_HANDLE_VALUE)
		throw_strerror("Open file descriptor", 0, descr);
	{
		LARGE_INTEGER sz(0);
		if (!GetFileSizeEx(hFile, &sz))
			throw_lasterror("Getting file size", 0, descr);
		m_filesize = sz;
	}
	m_filesize = std::min(sz, m_filesize);
	HANDLE hMap(CreateFileMapping(hFile, 0, writable ? PAGE_READWRITE : PAGE_READONLY, 0, 0, 0));
	if (hMap == INVALID_HANDLE_VALUE) {
		m_filesize = 0;
		throw_lasterror("Mapping file", 0, descr);
	}
	m_ptr = reinterpret_cast<uint8_t *>(MapViewOfFile(hMap, writable ? FILE_MAP_WRITE : FILE_MAP_READ, 0, 0, m_filesize));
	CloseHandle(hMap);
	MEMORY_BASIC_INFORMATION meminfo;
	if (VirtualQuery(m_binfile, &meminfo, sizeof(meminfo)) != sizeof(meminfo)) {
		UnmapViewOfFile(m_ptr);
		m_ptr = nullptr;
		m_filesize = 0;
		throw std::runtime_error("Error: Cannot get file mapping information", 0, descr);
	}
	m_mapsize = meminfo.RegionSize;
}

void MapFile::open(const char *filename, bool writable, const char *descr)
{
	close();
	if (!filename)
		return;
	HANDLE hFile(CreateFile(filename,
				writable ? (GENERIC_WRITE | GENERIC_READ) : GENERIC_READ,
				writable ? (FILE_SHARE_WRITE | FILE_SHARE_READ) : FILE_SHARE_READ, 0,
				OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0));
	if (hFile == INVALID_HANDLE_VALUE)
		throw_strerror("Open file ", filename, descr);
	{
		LARGE_INTEGER sz(0);
		if (!GetFileSizeEx(hFile, &sz))
			throw_lasterror("Getting file size", filename, descr);
		m_filesize = sz;
	}
	HANDLE hMap(CreateFileMapping(hFile, 0, writable ? PAGE_READWRITE : PAGE_READONLY, 0, 0, 0));
	if (hMap == INVALID_HANDLE_VALUE) {
		CloseHandle(hFile);
		m_filesize = 0;
		throw_lasterror("Mapping file", filename, descr);
	}
	m_ptr = reinterpret_cast<uint8_t *>(MapViewOfFile(hMap, writable ? FILE_MAP_WRITE : FILE_MAP_READ, 0, 0, 0));
	CloseHandle(hFile);
	CloseHandle(hMap);
	MEMORY_BASIC_INFORMATION meminfo;
	if (VirtualQuery(m_binfile, &meminfo, sizeof(meminfo)) != sizeof(meminfo)) {
		UnmapViewOfFile(m_ptr);
		m_ptr = nullptr;
		m_filesize = 0;
		throw std::runtime_error("Error: Cannot get file mapping information");
	}
	m_mapsize = meminfo.RegionSize;
}

void MapFile::open(const char *filename, std::size_t sz, bool writable, const char *descr)
{
	close();
	if (!filename)
		return;
	HANDLE hFile(CreateFile(filename,
				writable ? (GENERIC_WRITE | GENERIC_READ) : GENERIC_READ,
				writable ? (FILE_SHARE_WRITE | FILE_SHARE_READ) : FILE_SHARE_READ, 0,
				OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0));
	if (hFile == INVALID_HANDLE_VALUE)
		throw_strerror("Open file ", filename, descr);
	{
		LARGE_INTEGER sz(0);
		if (!GetFileSizeEx(hFile, &sz))
			throw_lasterror("Getting file size", filename, descr);
		m_filesize = sz;
	}
	m_filesize = std::min(sz, m_filesize);
	HANDLE hMap(CreateFileMapping(hFile, 0, writable ? PAGE_READWRITE : PAGE_READONLY, 0, 0, 0));
	if (hMap == INVALID_HANDLE_VALUE) {
		CloseHandle(hFile);
		m_filesize = 0;
		throw_lasterror("Mapping file", filename, descr);
	}
	m_ptr = reinterpret_cast<uint8_t *>(MapViewOfFile(hMap, writable ? FILE_MAP_WRITE : FILE_MAP_READ, 0, 0, m_filesize));
	CloseHandle(hFile);
	CloseHandle(hMap);
	MEMORY_BASIC_INFORMATION meminfo;
	if (VirtualQuery(m_binfile, &meminfo, sizeof(meminfo)) != sizeof(meminfo)) {
		UnmapViewOfFile(m_ptr);
		m_ptr = nullptr;
		m_filesize = 0;
		throw std::runtime_error("Error: Cannot get file mapping information");
	}
	m_mapsize = meminfo.RegionSize;
}

void MapFile::open_if_newer(const char *filename, const char *filet, bool writable, const char *descr)
{
	close();
	if (!filename || !filet)
		return;
	HANDLE hFile(CreateFile(filet, GENERIC_READ, FILE_SHARE_READ, 0,
				OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0));
	if (hFile == INVALID_HANDLE_VALUE)
		throw_strerror("Open file ", filet, descr);
	FILETIME timet;
	if (!GetFileTime(hFile, 0, 0, &timet)) {
		CloseHandle(hFile);
		throw_lasterror("Getting file time", filet, descr);
	}
	CloseHandle(hFile);
	hFile = CreateFile(filename,
			   writable ? (GENERIC_WRITE | GENERIC_READ) : GENERIC_READ,
			   writable ? (FILE_SHARE_WRITE | FILE_SHARE_READ) : FILE_SHARE_READ, 0,
			   OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0));
	if (hFile == INVALID_HANDLE_VALUE)
		throw_strerror("Open file ", filename, descr);
	FILETIME timef;
	if (!GetFileTime(hFile, 0, 0, &timef)) {
		CloseHandle(hFile);
		throw_lasterror("Getting file time", filename, descr);
	}
	if (timet.dwHighDateTime > timef.dwHighDateTime ||
	    (timet.dwHighDateTime == timef.dwHighDateTime && timet.dwLowDateTime > timef.dwLowDateTime)) {
		std::ostringstream oss;
		oss << "Error: File " << filename << " is older than " << filet;
		throw std::runtime_error(oss.str());
	}
	{
		LARGE_INTEGER sz(0);
		if (!GetFileSizeEx(hFile, &sz))
			throw_lasterror("Getting file size", filename, descr);
		m_filesize = sz;
	}
	HANDLE hMap(CreateFileMapping(hFile, 0, writable ? PAGE_READWRITE : PAGE_READONLY, 0, 0, 0));
	if (hMap == INVALID_HANDLE_VALUE) {
		CloseHandle(hFile);
		m_filesize = 0;
		throw_lasterror("Mapping file", filename, descr);
	}
	m_ptr = reinterpret_cast<uint8_t *>(MapViewOfFile(hMap, writable ? FILE_MAP_WRITE : FILE_MAP_READ, 0, 0, m_filesize));
	CloseHandle(hFile);
	CloseHandle(hMap);
	MEMORY_BASIC_INFORMATION meminfo;
	if (VirtualQuery(m_binfile, &meminfo, sizeof(meminfo)) != sizeof(meminfo)) {
		UnmapViewOfFile(m_ptr);
		m_ptr = nullptr;
		m_filesize = 0;
		throw std::runtime_error("Error: Cannot get file mapping information");
	}
	m_mapsize = meminfo.RegionSize;
}

void MapFile::open_if_newer(const char *filename, const char *filet, std::size_t sz, bool writable, const char *descr)
{
	close();
	if (!filename || !filet)
		return;
	HANDLE hFile(CreateFile(filet, GENERIC_READ, FILE_SHARE_READ, 0,
				OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0));
	if (hFile == INVALID_HANDLE_VALUE)
		throw_strerror("Open file ", filet, descr);
	FILETIME timet;
	if (!GetFileTime(hFile, 0, 0, &timet)) {
		CloseHandle(hFile);
		throw_lasterror("Getting file time", filet, descr);
	}
	CloseHandle(hFile);
	hFile = CreateFile(filename,
			   writable ? (GENERIC_WRITE | GENERIC_READ) : GENERIC_READ,
			   writable ? (FILE_SHARE_WRITE | FILE_SHARE_READ) : FILE_SHARE_READ, 0,
			   OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0));
	if (hFile == INVALID_HANDLE_VALUE)
		throw_strerror("Open file ", filename, descr);
	FILETIME timef;
	if (!GetFileTime(hFile, 0, 0, &timef)) {
		CloseHandle(hFile);
		throw_lasterror("Getting file time", filename, descr);
	}
	if (timet.dwHighDateTime > timef.dwHighDateTime ||
	    (timet.dwHighDateTime == timef.dwHighDateTime && timet.dwLowDateTime > timef.dwLowDateTime)) {
		std::ostringstream oss;
		oss << "Error: File " << filename << " is older than " << filet;
		throw std::runtime_error(oss.str());
	}
	{
		LARGE_INTEGER sz(0);
		if (!GetFileSizeEx(hFile, &sz))
			throw_lasterror("Getting file size", filename, descr);
		m_filesize = sz;
	}
	m_filesize = std::min(sz, m_filesize);
	HANDLE hMap(CreateFileMapping(hFile, 0, writable ? PAGE_READWRITE : PAGE_READONLY, 0, 0, 0));
	if (hMap == INVALID_HANDLE_VALUE) {
		CloseHandle(hFile);
		m_filesize = 0;
		throw_lasterror("Mapping file", filename, descr);
	}
	m_ptr = reinterpret_cast<uint8_t *>(MapViewOfFile(hMap, writable ? FILE_MAP_WRITE : FILE_MAP_READ, 0, 0, m_filesize));
	CloseHandle(hFile);
	CloseHandle(hMap);
	MEMORY_BASIC_INFORMATION meminfo;
	if (VirtualQuery(m_binfile, &meminfo, sizeof(meminfo)) != sizeof(meminfo)) {
		UnmapViewOfFile(m_ptr);
		m_ptr = nullptr;
		m_filesize = 0;
		throw std::runtime_error("Error: Cannot get file mapping information");
	}
	m_mapsize = meminfo.RegionSize;
}

void MapFile::close(void)
{
	bool thr(m_ptr && !UnmapViewOfFile(m_ptr));
	m_ptr = nullptr;
	m_filesize = m_mapsize = 0;
	if (thr)
		throw_lasterror("Unmapping file");
}

#else

void MapFile::open(int fd, bool writable, const char *descr)
{
	close();
	if (fd == -1)
		return;
	{
		struct stat statbuf;
		if (fstat(fd, &statbuf))
			throw_strerror("File stat", 0, descr);
		m_filesize = statbuf.st_size;
	}
	size_t pgsz(sysconf(_SC_PAGE_SIZE));
	m_mapsize = ((m_filesize + pgsz - 1) / pgsz) * pgsz;
	m_ptr = reinterpret_cast<uint8_t *>(mmap(0, m_mapsize, writable ? (PROT_WRITE | PROT_READ) : PROT_READ, MAP_SHARED, fd, 0));
	if (m_ptr == reinterpret_cast<uint8_t *>(-1)) {
		m_ptr = nullptr;
		m_filesize = m_mapsize = 0;
		throw_strerror("Mapping file", 0, descr);
	}
}

void MapFile::open(int fd, std::size_t sz, bool writable, const char *descr)
{
	close();
	if (fd == -1)
		return;
	{
		struct stat statbuf;
		if (fstat(fd, &statbuf))
			throw_strerror("File stat", 0, descr);
		m_filesize = statbuf.st_size;
	}
	m_filesize = std::min(sz, m_filesize);
	size_t pgsz(sysconf(_SC_PAGE_SIZE));
	m_mapsize = ((m_filesize + pgsz - 1) / pgsz) * pgsz;
	m_ptr = reinterpret_cast<uint8_t *>(mmap(0, m_mapsize, writable ? (PROT_WRITE | PROT_READ) : PROT_READ, MAP_SHARED, fd, 0));
	if (m_ptr == reinterpret_cast<uint8_t *>(-1)) {
		m_ptr = nullptr;
		m_filesize = m_mapsize = 0;
		throw_strerror("Mapping file", 0, descr);
	}
}

void MapFile::open(const char *filename, bool writable, const char *descr)
{
	close();
	if (!filename)
		return;
	int fd(::open(filename, (writable ? O_RDWR : O_RDONLY) /* | O_NOATIME*/, 0));
	if (fd == -1)
		throw_strerror("Opening stat ", filename, descr);
	{
		struct stat statbuf;
		if (fstat(fd, &statbuf)) {
			::close(fd);
			throw_strerror("File stat ", filename, descr);
		}
		m_filesize = statbuf.st_size;
	}
	size_t pgsz(sysconf(_SC_PAGE_SIZE));
	m_mapsize = ((m_filesize + pgsz - 1) / pgsz) * pgsz;
	m_ptr = reinterpret_cast<uint8_t *>(mmap(0, m_mapsize, writable ? (PROT_WRITE | PROT_READ) : PROT_READ, MAP_SHARED, fd, 0));
	::close(fd);
	if (m_ptr == reinterpret_cast<uint8_t *>(-1)) {
		m_ptr = nullptr;
		m_filesize = m_mapsize = 0;
		throw_strerror("Mapping file ", filename, descr);
	}
}

void MapFile::open(const char *filename, std::size_t sz, bool writable, const char *descr)
{
	close();
	if (!filename)
		return;
	int fd(::open(filename, (writable ? O_RDWR : O_RDONLY) /* | O_NOATIME*/, 0));
	if (fd == -1)
		throw_strerror("Opening stat ", filename, descr);
	{
		struct stat statbuf;
		if (fstat(fd, &statbuf)) {
			::close(fd);
			throw_strerror("File stat ", filename, descr);
		}
		m_filesize = statbuf.st_size;
	}
	m_filesize = std::min(sz, m_filesize);
	size_t pgsz(sysconf(_SC_PAGE_SIZE));
	m_mapsize = ((m_filesize + pgsz - 1) / pgsz) * pgsz;
	m_ptr = reinterpret_cast<uint8_t *>(mmap(0, m_mapsize, writable ? (PROT_WRITE | PROT_READ) : PROT_READ, MAP_SHARED, fd, 0));
	::close(fd);
	if (m_ptr == reinterpret_cast<uint8_t *>(-1)) {
		m_ptr = nullptr;
		m_filesize = m_mapsize = 0;
		throw_strerror("Mapping file ", filename, descr);
	}
}

void MapFile::open_if_newer(const char *filename, const char *filet, bool writable, const char *descr)
{
	close();
	if (!filename || !filet)
		return;
	struct stat statt;
	if (stat(filet, &statt))
		throw_strerror((std::string("File stat ") + filet).c_str());
	int fd(::open(filename, (writable ? O_RDWR : O_RDONLY) /* | O_NOATIME*/, 0));
	if (fd == -1)
		throw_strerror("Opening stat ", filename, descr);
	{
		struct stat statbuf;
		if (fstat(fd, &statbuf)) {
			::close(fd);
			throw_strerror("File stat ", filename, descr);
		}
		if (statbuf.st_mtime < statt.st_mtime) {
			std::ostringstream oss;
			oss << "Error: File " << filename << " is older than " << filet;
			throw std::runtime_error(oss.str());
		}
		m_filesize = statbuf.st_size;
	}
	size_t pgsz(sysconf(_SC_PAGE_SIZE));
	m_mapsize = ((m_filesize + pgsz - 1) / pgsz) * pgsz;
	m_ptr = reinterpret_cast<uint8_t *>(mmap(0, m_mapsize, writable ? (PROT_WRITE | PROT_READ) : PROT_READ, MAP_SHARED, fd, 0));
	::close(fd);
	if (m_ptr == reinterpret_cast<uint8_t *>(-1)) {
		m_ptr = nullptr;
		m_filesize = m_mapsize = 0;
		throw_strerror("Mapping file ", filename, descr);
	}
}

void MapFile::open_if_newer(const char *filename, const char *filet, std::size_t sz, bool writable, const char *descr)
{
	close();
	if (!filename || !filet)
		return;
	struct stat statt;
	if (stat(filet, &statt))
		throw_strerror((std::string("File stat ") + filet).c_str());
	int fd(::open(filename, (writable ? O_RDWR : O_RDONLY) /* | O_NOATIME*/, 0));
	if (fd == -1)
		throw_strerror("Opening stat ", filename, descr);
	{
		struct stat statbuf;
		if (fstat(fd, &statbuf)) {
			::close(fd);
			throw_strerror("File stat ", filename, descr);
		}
		if (statbuf.st_mtime < statt.st_mtime) {
			std::ostringstream oss;
			oss << "Error: File " << filename << " is older than " << filet;
			throw std::runtime_error(oss.str());
		}
		m_filesize = statbuf.st_size;
	}
	m_filesize = std::min(sz, m_filesize);
	size_t pgsz(sysconf(_SC_PAGE_SIZE));
	m_mapsize = ((m_filesize + pgsz - 1) / pgsz) * pgsz;
	m_ptr = reinterpret_cast<uint8_t *>(mmap(0, m_mapsize, writable ? (PROT_WRITE | PROT_READ) : PROT_READ, MAP_SHARED, fd, 0));
	::close(fd);
	if (m_ptr == reinterpret_cast<uint8_t *>(-1)) {
		m_ptr = nullptr;
		m_filesize = m_mapsize = 0;
		throw_strerror("Mapping file ", filename, descr);
	}
}

void MapFile::close(void)
{
	bool thr(m_ptr && munmap(m_ptr, m_mapsize));
	m_ptr = nullptr;
	m_filesize = m_mapsize = 0;
	if (thr)
		throw_strerror("Unmapping file");
}

#endif

MapFilePtr::MapFilePtr(void)
	: MapFile(), m_refcount(0)
{
}

MapFilePtr::MapFilePtr(int fd, bool writable, const char *descr)
	: MapFile(fd, writable, descr), m_refcount(0)
{
}

MapFilePtr::MapFilePtr(int fd, std::size_t sz, bool writable, const char *descr)
	: MapFile(fd, sz, writable, descr), m_refcount(0)
{
}

MapFilePtr::MapFilePtr(const char *filename, bool writable, const char *descr)
	: MapFile(filename, writable, descr), m_refcount(0)
{
}

MapFilePtr::MapFilePtr(const char *filename, std::size_t sz, bool writable, const char *descr)
	: MapFile(filename, sz, writable, descr), m_refcount(0)
{
}
