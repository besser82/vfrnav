//
// C++ Interface: mapfile
//
// Description: Memory Mapping of files
//
//
// Author: Thomas Sailer <t.sailer@alumni.ethz.ch>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef MAPFILE_H
#define MAPFILE_H

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "sysdeps.h"

#include <atomic>
#include <boost/smart_ptr/intrusive_ptr.hpp>

class MapFile {
public:
	MapFile(const MapFile& ) = delete;
	MapFile(void);
	MapFile(int fd, bool writable = false, const char *descr = 0);
	MapFile(int fd, std::size_t sz, bool writable = false, const char *descr = 0);
	MapFile(const char *filename, bool writable = false, const char *descr = 0);
	MapFile(const char *filename, std::size_t sz, bool writable = false, const char *descr = 0);
	~MapFile(void);

	MapFile& operator=(const MapFile& ) = delete;

	void open(int fd, bool writable = false, const char *descr = 0);
	void open(int fd, std::size_t sz, bool writable = false, const char *descr = 0);
	void open(const char *filename, bool writable = false, const char *descr = 0);
	void open(const char *filename, std::size_t sz, bool writable = false, const char *descr = 0);
	void open_if_newer(const char *filename, const char *filet, bool writable = false, const char *descr = 0);
	void open_if_newer(const char *filename, const char *filet, std::size_t sz, bool writable = false, const char *descr = 0);
	void close(void);
	bool is_open(void) const { return !!m_ptr; }

	uint8_t *begin(void) const { return m_ptr; }
	uint8_t *end(void) const { return m_ptr + m_filesize; }
	bool empty(void) const { return !m_filesize; }
	std::size_t size(void) const { return m_filesize; }
	std::size_t mapsize(void) const { return m_mapsize; }

	template <typename T> T *begin_as(void) const { return reinterpret_cast<T *>(m_ptr); }
	template <typename T> T *end_as(void) const { return reinterpret_cast<T *>(m_ptr) + (m_filesize / sizeof(T)); }

	static void throw_strerror(const char *descr);
	static void throw_strerror(const std::string& descr) { throw_strerror(descr.c_str()); }
#if defined(HAVE_WINDOWS_H)
	static void throw_lasterror(const char *descr);
	static void throw_lasterror(const std::string& descr) { throw_lasterror(descr.c_str()); }
#endif

protected:
	uint8_t *m_ptr;
	std::size_t m_filesize;
	std::size_t m_mapsize;

	static void throw_strerror(const char *what, const char *file, const char *descr);
#if defined(HAVE_WINDOWS_H)
	static void throw_lasterror(const char *what, const char *file, const char *descr);
#endif
};

class MapFilePtr : public MapFile {
public:
	typedef boost::intrusive_ptr<MapFilePtr> ptr_t;
	typedef boost::intrusive_ptr<const MapFilePtr> const_ptr_t;

	MapFilePtr(const MapFilePtr& ) = delete;
	MapFilePtr(void);
	MapFilePtr(int fd, bool writable = false, const char *descr = 0);
	MapFilePtr(int fd, std::size_t sz, bool writable = false, const char *descr = 0);
	MapFilePtr(const char *filename, bool writable = false, const char *descr = 0);
	MapFilePtr(const char *filename, std::size_t sz, bool writable = false, const char *descr = 0);

	MapFilePtr& operator=(const MapFilePtr& ) = delete;

	unsigned int breference(void) const { return ++m_refcount; }
	unsigned int bunreference(void) const { return --m_refcount; }
	unsigned int get_refcount(void) const { return m_refcount; }
	ptr_t get_ptr(void) { return ptr_t(this); }
	const_ptr_t get_ptr(void) const { return const_ptr_t(this); }
	friend inline void intrusive_ptr_add_ref(const MapFilePtr* expr) { expr->breference(); }
	friend inline void intrusive_ptr_release(const MapFilePtr* expr) { if (!expr->bunreference()) delete expr; }

protected:
	mutable std::atomic<unsigned int> m_refcount;
};

#endif /* MAPFILE_H */

/* Local Variables: */
/* mode: c++ */
/* End: */
