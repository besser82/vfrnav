/*****************************************************************************/

/*
 *      testaircraft.cc  --  Test aircraft performance calculation
 *
 *      Copyright (C) 2020  Thomas Sailer (t.sailer@alumni.ethz.ch)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

/*****************************************************************************/

#include <getopt.h>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <cmath>

#ifdef HAVE_SYSEXITS_H
#include <sysexits.h>
#else
#define EX_USAGE   64
#define EX_DATAERR 65
#define EX_NOINPUT 66
#define EX_OK      0
#endif

#include "aircraft.h"

static bool parsefloat(std::vector<double>& v, const char *cp)
{
	v.clear();
	if (!cp)
		return false;
	char *cp1;
	for (;;) {
		double vv(strtod(cp, &cp1));
		if (cp == cp1 || std::isnan(vv))
			return false;
		v.push_back(vv);
		cp = cp1;
		while (std::isspace(*cp))
			++cp;
		if (!*cp)
			break;
		if (*cp != ':' && *cp != ',')
			return false;
		++cp;
	}
	return true;
}

static void octave_save_text(std::ostream& os, const std::string& name, const std::string& value)
{
        os << "# name: " << name << std::endl
           << "# type: scalar" << std::endl
	   << "# elements: 1" << std::endl
	   << "# length: " << value.size() << std::endl
           << value << std::endl;
}

static bool checkclimbdescentdim(double x)
{
	return !std::isnan(x) && x >= 0 && x <= 100;
}

static bool checkrocandtasandff(const Aircraft::ClimbDescent& cd, double &t)
{
	bool last(false);
	for (double tm = 0; !last; tm += 10) {
		last = tm >= cd.get_climbtime();
		if (last)
			tm = cd.get_climbtime();
		double roc(cd.time_to_climbrate(tm));
		double tas(cd.time_to_tas(tm));
		double ff(cd.time_to_fuelflow(tm));
		if (std::isnan(roc) || roc < 90 ||
		    std::isnan(tas) || tas < 1 ||
		    std::isnan(ff) || ff < 0) {
			t = tm;
			return false;
		}
	}
	return true;
}

static int testclimbdescentcalc(const Aircraft& acft, const Aircraft::Climb& climb, const std::string& name,
				double isaoffsmin, double isaoffsmax, double isaoffsstep,
				double massmin, double massmax, double massstep,
				double qnhmin, double qnhmax, double qnhstep,
				double minceiling, const std::string& datafile)
{
	if (std::isnan(isaoffsmin) || std::isnan(isaoffsmax) || std::isnan(isaoffsstep) ||
	    std::isnan(massmin) || std::isnan(massmax) || std::isnan(massstep) ||
	    std::isnan(qnhmin) || std::isnan(qnhmax) || std::isnan(qnhstep) ||
	    isaoffsstep == 0 || !checkclimbdescentdim((isaoffsmax - isaoffsmin) / isaoffsstep) ||
	    massstep == 0 || !checkclimbdescentdim((massmax - massmin) / massstep) ||
	    qnhstep == 0 || !checkclimbdescentdim((qnhmax - qnhmin) / qnhstep)) {
		std::cerr << "Parameter Error: masses=" << massmin << ':' << massstep << ':' << massmax
			  << " isaoffsets=" << isaoffsmin << ':' << isaoffsstep << ':' << isaoffsmax
			  << " qnhs=" << qnhmin << ':' << qnhstep << ':' << qnhmax << std::endl;
		return EX_NOINPUT;
	}
	Aircraft::Poly1D<double> isaoffsets, masses, qnhs;
	for (double x = isaoffsmin; x <= isaoffsmax; x += isaoffsstep)
		isaoffsets.push_back(x);
	for (double x = massmin; x <= massmax; x += massstep)
		masses.push_back(x);
	for (double x = qnhmin; x <= qnhmax; x += qnhstep)
		qnhs.push_back(x);
	Aircraft::Poly1D<double> ceilings;
	Aircraft::Poly1D<double> clbtimes;
	ceilings.resize(isaoffsets.size() * masses.size() * qnhs.size());
	clbtimes.resize(isaoffsets.size() * masses.size() * qnhs.size());
	int r(EX_OK);
	for (Aircraft::Poly1D<double>::size_type i0(0), n0(masses.size()); i0 < n0; ++i0) {
		for (Aircraft::Poly1D<double>::size_type i1(0), n1(isaoffsets.size()); i1 < n1; ++i1) {
			for (Aircraft::Poly1D<double>::size_type i2(0), n2(qnhs.size()); i2 < n2; ++i2) {
				Aircraft::Poly1D<double>::size_type ii((i0 * isaoffsets.size() + i1) * qnhs.size() + i2);
				Aircraft::ClimbDescent cd(climb.calculate(name, masses[i0], isaoffsets[i1], qnhs[i2]));
				ceilings[ii] = cd.get_ceiling();
				double t0(cd.altitude_to_time(0)), t1(cd.altitude_to_time(minceiling));
				clbtimes[ii] = t1 - t0;
				if (!std::isnan(minceiling) && cd.get_ceiling() < minceiling) {
					r = EX_DATAERR;
					std::cerr << "Aircraft " << acft.get_callsign() << " curve " << name
						  << " mass " << masses[i0] << " isaoffset " << isaoffsets[i1] << " QNH " << qnhs[i2]
						  << " ceiling error " << cd.get_ceiling() << " < " << minceiling << std::endl;
					cd.print(std::cerr, "  ");
					break;
				}
				{
					double t;
					if (!checkrocandtasandff(cd, t)) {
						r = EX_DATAERR;
						std::cerr << "Aircraft " << acft.get_callsign() << " curve " << name
							  << " mass " << masses[i0] << " isaoffset " << isaoffsets[i1] << " QNH " << qnhs[i2]
							  << " time error " << t << " roc " << cd.time_to_climbrate(t) << " tas "
							  << cd.time_to_tas(t) << " ff " << cd.time_to_fuelflow(t) << " alt "
							  << cd.time_to_altitude(t) << " dist " << cd.time_to_distance(t) << " fuel "
							  << cd.time_to_fuel(t) << std::endl;
						cd.print(std::cerr, "  ");
						break;
					}
				}
			}
		}
	}
	if (!datafile.empty()) {
		std::ofstream os(datafile);
		octave_save_text(os, "aircraft", acft.get_callsign());
		octave_save_text(os, "curve", name);
		isaoffsets.save_octave(os, "isaoffsets");
		masses.save_octave(os, "masses");
		qnhs.save_octave(os, "qnhs");
		ceilings.save_octave(os, "ceilings");
		clbtimes.save_octave(os, "clbtimes");
	}
	return r;
}

static int testclimbdescentcalc(const Aircraft& acft, double isaoffsmin, double isaoffsmax, double isaoffsstep,
				double massmin, double massmax, double massstep,
				double qnhmin, double qnhmax, double qnhstep,
				double minceiling, const std::string& datadir)
{
	unsigned int dcnt(0);
	for (const std::string& x : acft.get_climb().get_curves()) {
		std::ostringstream dfile;
		if (!datadir.empty()) {
			dfile << datadir << '/' << acft.get_callsign() << ".climb" << dcnt;
			++dcnt;
		}
		int r(testclimbdescentcalc(acft, acft.get_climb(), x, isaoffsmin, isaoffsmax, isaoffsstep,
					   massmin, massmax, massstep, qnhmin, qnhmax, qnhstep, minceiling, dfile.str()));
		if (r != EX_OK)
			return r;
	}
	dcnt = 0;
	for (const std::string& x : acft.get_descent().get_curves()) {
		std::ostringstream dfile;
		if (!datadir.empty()) {
			dfile << datadir << '/' << acft.get_callsign() << ".descent" << dcnt;
			++dcnt;
		}
		int r(testclimbdescentcalc(acft, acft.get_descent(), x, isaoffsmin, isaoffsmax, isaoffsstep,
					   massmin, massmax, massstep, qnhmin, qnhmax, qnhstep, minceiling, dfile.str()));
		if (r != EX_OK)
			return r;
	}
	return EX_OK;
}

int main(int argc, char *argv[])
{
	static struct option long_options[] = {
		{ "aircraft", required_argument, 0, 0x100 },
		{ "aircraftsave", required_argument, 0, 0x101 },
		{ "isaoffsets", required_argument, 0, 0x110 },
		{ "masses", required_argument, 0, 0x111 },
		{ "qnhs", required_argument, 0, 0x112 },
		{ "minceiling", required_argument, 0, 0x113 },
		{ "datadir", required_argument, 0, 0x114 },
		{ 0, 0, 0, 0 }
	};
	int c, err(0);

	Aircraft acft;
	double isaoffsmin(-30), isaoffsmax(30), isaoffsstep(10);
	double massmin(std::numeric_limits<double>::quiet_NaN()), massmax(std::numeric_limits<double>::quiet_NaN()), massstep(std::numeric_limits<double>::quiet_NaN());
	double qnhmin(983), qnhmax(1043), qnhstep(30);
	double minceiling(std::numeric_limits<double>::quiet_NaN());
	std::string datadir;
	while ((c = getopt_long(argc, argv, "", long_options, 0)) != EOF) {
		switch (c) {
		case 0x100:
		case 0x101:
			if (!optarg)
				break;
			try {
				if (acft.load_file(optarg)) {
					Aircraft::recompute_t r(acft.recompute());
					if (r & Aircraft::recompute_wbunits)
						std::cerr << "Aircraft " << acft.get_callsign() << " W&B auto units added" << std::endl;
					if (r & Aircraft::recompute_distances)
						std::cerr << "Aircraft " << acft.get_callsign() << " distance polynomials recalculated" << std::endl;
					if (r & Aircraft::recompute_climb)
						std::cerr << "Aircraft " << acft.get_callsign() << " climb polynomials recalculated" << std::endl;
					if (r & Aircraft::recompute_descent)
						std::cerr << "Aircraft " << acft.get_callsign() << " descent polynomials recalculated" << std::endl;
					if (r & Aircraft::recompute_glide)
						std::cerr << "Aircraft " << acft.get_callsign() << " glide polynomials recalculated" << std::endl;
					if (r && c == 0x101)
						acft.save_file(optarg);
					acft.check().print(std::cerr);
				} else {
					std::cerr << "Cannot load aircraft " << optarg << std::endl;
				}
				if (std::isnan(massmax))
					massmax = acft.get_mtom();
				if (std::isnan(massmin))
					massmin = acft.get_mtom() * 0.9;
				if (std::isnan(massstep))
					massmin = acft.get_mtom() * 0.1;
			} catch (const std::exception& e) {
				acft = Aircraft();
				std::cerr << "Unable to load aircraft file \"" << optarg << "\": " << e.what() << std::endl;
				return EX_NOINPUT;
			}
			break;

		case 0x110:
		{
			std::vector<double> v;
			if (!parsefloat(v, optarg) || v.size() < 2 || v.size() > 3) {
				std::cerr << "Cannot parse isaoffsets: \"" << (optarg ? optarg : "(null)") << "\"" << std::endl;
				return EX_NOINPUT;
			}
			isaoffsmin = v[0];
			if (v.size() >= 3) {
				isaoffsstep = v[1];
				isaoffsmax = v[2];
			} else {
				isaoffsmax = v[1];
			}
			break;
		}

		case 0x111:
		{
			std::vector<double> v;
			if (!parsefloat(v, optarg) || v.size() < 2 || v.size() > 3) {
				std::cerr << "Cannot parse masses: \"" << (optarg ? optarg : "(null)") << "\"" << std::endl;
				return EX_NOINPUT;
			}
			massmin = v[0];
			if (v.size() >= 3) {
				massstep = v[1];
				massmax = v[2];
			} else {
				massmax = v[1];
			}
			break;
		}

		case 0x112:
		{
			std::vector<double> v;
			if (!parsefloat(v, optarg) || v.size() < 2 || v.size() > 3) {
				std::cerr << "Cannot parse qnhs: \"" << (optarg ? optarg : "(null)") << "\"" << std::endl;
				return EX_NOINPUT;
			}
			qnhmin = v[0];
			if (v.size() >= 3) {
				qnhstep = v[1];
				qnhmax = v[2];
			} else {
				qnhmax = v[1];
			}
			break;
		}

		case 0x113:
			if (!optarg)
				break;
			minceiling = strtod(optarg, nullptr);
			break;

		case 0x114:
			if (!optarg)
				break;
			datadir = optarg;
			break;

		default:
			++err;
			break;
		}
	}
	if (err) {
		std::cerr << "usage: testaircraft [--aircraft <file>] [--isaoffsets=min:step:max] [--masses=min:step:max] "
			"[--qnhs=min:step:max] [--minceiling=v] [--datadir=path]" << std::endl;
		return EX_USAGE;
	}

	try {
		if (!std::isnan(minceiling) && minceiling > 0) {
			int r(testclimbdescentcalc(acft, isaoffsmin, isaoffsmax, isaoffsstep, massmin, massmax, massstep,
						   qnhmin, qnhmax, qnhstep, minceiling, datadir));
			if (r != EX_OK)
				return r;
		}
	} catch (const std::exception& e) {
		std::cerr << "Test failure: " << e.what() << std::endl;
		return EX_DATAERR;
	}
	return EX_OK;
}
