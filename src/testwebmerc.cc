/*****************************************************************************/

/*
 *      testhib.cc  --  Test hibernate
 *
 *      Copyright (C) 2020  Thomas Sailer (t.sailer@alumni.ethz.ch)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

/*****************************************************************************/

#include <getopt.h>
#include <iomanip>
#include <iostream>
#include <sstream>

#ifdef HAVE_SYSEXITS_H
#include <sysexits.h>
#else
#define EX_USAGE   64
#define EX_DATAERR 65
#define EX_OK      0
#endif

#include <cstdlib>
#include <glibmm.h>
#include "geom.h"

static void test(void)
{
	std::cout << "wm = [ ..." << std::endl;
	for (Point pt(0, Point::lat_min); pt.get_lat() <= Point::lat_max; pt.set_lat(pt.get_lat() + (1 << 22))) {
		Point pta(pt), ptf(pt);
		pta.approx_webmercator();
		ptf.transform_webmercator();
		Point ptab(pta), ptfb(ptf);
		ptab.invtransform_webmercator();
		ptfb.invtransform_webmercator();
		Point ptabe(ptab - pt), ptfbe(ptfb - pt);
		std::cout << "  " << pt.get_lat_deg_dbl() << ' '
			  << pta.get_lat_deg_dbl() << ' ' << ptf.get_lat_deg_dbl() << ' '
			  << ptab.get_lat_deg_dbl() << ' ' << ptfb.get_lat_deg_dbl() << ' '
			  << ptabe.get_lat_deg_dbl() << ' ' << ptfbe.get_lat_deg_dbl() << " ..." << std::endl;
	}
	std::cout << "];" << std::endl;
}

static void testtiming(void)
{
	static const unsigned int n = 1 << 20;
	Point *pta, *ptf;
	pta = new Point[n];
	ptf = new Point[n];
	for (unsigned int i = 0; i < n; ++i)
		pta[i] = ptf[i] = Point(0, (Point::from_deg_dbl * 170) * (drand48() - 0.5));
	Glib::TimeVal tv, tva, tvf, tvb;
	tv.assign_current_time();
	for (unsigned int i = 0; i < n; ++i)
		pta[i].approx_webmercator();
	tva.assign_current_time();
	tva -= tv;
	tv.assign_current_time();
	for (unsigned int i = 0; i < n; ++i)
		ptf[i].transform_webmercator();
	tvf.assign_current_time();
	tvf -= tv;
	tv.assign_current_time();
	for (unsigned int i = 0; i < n; ++i)
		ptf[i].invtransform_webmercator();
	tvb.assign_current_time();
	tvb -= tv;
	delete[] pta;
	delete[] ptf;
	std::cout << "# times: approx " << tva.as_double()
		  << " forward " << tvf.as_double()
		  << " backward " << tvb.as_double() << std::endl;
}

int main(int argc, char *argv[])
{
	static struct option long_options[] = {
		{ "xxxx", required_argument, 0, 'x' },
		{ 0, 0, 0, 0 }
	};
	int c, err(0);
	
	while ((c = getopt_long(argc, argv, "x:", long_options, 0)) != EOF) {
		switch (c) {
		default:
			++err;
			break;
		}
	}
	if (err) {
		std::cerr << "usage: testwebmerc" << std::endl;
		return EX_USAGE;
	}

	try {
		test();
		testtiming();
	} catch (const std::exception& e) {
		std::cerr << "Test failure: " << e.what() << std::endl;
		return EX_DATAERR;
	}
	return EX_OK;
}
