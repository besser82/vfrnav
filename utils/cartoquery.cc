/*****************************************************************************/

/*
 *      cartoquery.cc  --  Query objects from the Carto system
 *
 *      Copyright (C) 2020  Thomas Sailer (t.sailer@alumni.ethz.ch)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

/*****************************************************************************/

#include <getopt.h>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <fstream>

#ifdef HAVE_SYSEXITS_H
#include <sysexits.h>
#else
#define EX_USAGE   64
#define EX_DATAERR 65
#define EX_OK      0
#endif

#include <glibmm.h>

#include "carto.h"
#include "osmdb.h"
#include "osmsdb.h"
#include "dbobj.h"
#include "engine.h"

static OSMDB odb;
static OSMStaticDB sdb;

class CalcError : public CartoSQL::CalcOSMVariables {
public:
	CalcError(std::ostream& os, const CartoSQL *csql = nullptr) : CalcOSMVariables(csql), m_os(os), m_error(false) {}

	bool is_error(void) const { return m_error; }
	void reset(void) { m_error = false; }

	virtual void error(const CartoSQL::Expr& e, const char *msg) {
		if (is_error())
			return;
		m_error = true;
		m_os << "ERROR: " << (msg ? msg : "unknown") << std::endl;
		CartoSQL::PrintContextSQL ctx(get_csql());
		e.print(m_os, 2, ctx) << std::endl;
	}

	virtual void error(const CartoSQL::Expr& e, const CartoSQL::Value& v, const char *msg) {
		if (is_error())
			return;
		m_error = true;
		m_os << "ERROR: " << (msg ? msg : "unknown") << ": " << v.get_sqlstring() << std::endl;
		CartoSQL::PrintContextSQL ctx(get_csql());
		e.print(m_os, 2, ctx) << std::endl;
	}

	virtual void error(const CartoSQL::ExprTable& e, const char *msg) {
		if (is_error())
			return;
		m_error = true;
		m_os << "ERROR: " << (msg ? msg : "unknown") << std::endl;
		CartoSQL::PrintContextSQL ctx(get_csql());
		e.print(m_os, 2, ctx) << std::endl;
	}

	virtual void error(const CartoSQL::ExprTable& e, const CartoSQL::Value& v, const char *msg) {
		if (is_error())
			return;
		m_error = true;
		m_os << "ERROR: " << (msg ? msg : "unknown") << ": " << v.get_sqlstring() << std::endl;
		CartoSQL::PrintContextSQL ctx(get_csql());
		e.print(m_os, 2, ctx) << std::endl;
	}

protected:
	std::ostream& m_os;
	bool m_error;
};

static void print_expr(const CartoSQL& sql, const CartoSQL::Expr::const_ptr_t& expr, const Rect& bbox, Carto::zoom_t zoom, bool showexpr, bool optgeneral)
{
	if (!expr)
		return;
	if (showexpr)
		sql.print_expr(std::cout << "EXPRESSION: ", expr, 2) << std::endl;
	CartoSQL::Expr::const_ptr_t exprs;
	{
		CartoSQL::CalcOSMVariables calc(&sql);
		calc.set_bbox(bbox);
		// https://github.com/openstreetmap/mapnik-stylesheets/blob/master/zoom-to-scale.txt
		calc.set_scale_denominator(ldexp(559082264.02800, -static_cast<double>(zoom)));
		calc.set_pixel_width(1);
		calc.set_pixel_height(1);
		exprs = expr->replace_variable(calc);
		if (!exprs)
			exprs = expr;
		else if (showexpr)
			sql.print_expr(std::cout << "REPLACED VARIABLES EXPRESSION: ", exprs, 2) << std::endl;
	}
	if (optgeneral) {
		CartoSQL::Expr::const_ptr_t e(exprs->simplify(&sql));
		if (e) {
			exprs = e;
			if (showexpr)
				sql.print_expr(std::cout << "SIMPLIFIED EXPRESSION: ", exprs, 2) << std::endl;
		}
	}
	CalcError calc(std::cerr, &sql);
	calc.set_bbox(bbox);
	// https://github.com/openstreetmap/mapnik-stylesheets/blob/master/zoom-to-scale.txt
	calc.set_scale_denominator(ldexp(559082264.02800, -static_cast<double>(zoom)));
	calc.set_pixel_width(1);
	calc.set_pixel_height(1);
	CartoSQL::Value val;
	Glib::TimeVal tv, tv1;
	tv1.assign_current_time();
	if (!exprs->calculate(calc, val)) {
		std::cerr << "CALCULATE ERROR" << std::endl;
	} else {
		tv.assign_current_time();
		tv -= tv1;
		std::cout << "RESULT: " << val.get_sqlstring() << " (" << tv.as_double() << "s)" << std::endl;
	}
}

static void print_expr(const CartoSQL& sql, const CartoSQL::ExprTable::const_ptr_t& expr, const Rect& bbox, Carto::zoom_t zoom, bool showexpr,
		       bool optgeneral, bool optcolumn, bool optfilter, bool optsort, CartoSQL::Value::sqlstringflags_t flags)
{
	if (!expr)
		return;
	if (showexpr)
		sql.print_expr(std::cout << "EXPRESSION: ", expr, 2) << std::endl;
	CartoSQL::ExprTable::const_ptr_t exprs;
	{
		CartoSQL::CalcOSMVariables calc(&sql);
		calc.set_bbox(bbox);
		// https://github.com/openstreetmap/mapnik-stylesheets/blob/master/zoom-to-scale.txt
		calc.set_scale_denominator(ldexp(559082264.02800, -static_cast<double>(zoom)));
		calc.set_pixel_width(1);
		calc.set_pixel_height(1);
		exprs = expr->replace_variable(calc);
		if (!exprs)
			exprs = expr;
		else if (showexpr)
			sql.print_expr(std::cout << "REPLACED VARIABLES EXPRESSION: ", exprs, 2) << std::endl;
	}
	if (optgeneral) {
		CartoSQL::ExprTable::const_ptr_t e(exprs->simplify(&sql));
		if (e) {
			exprs = e;
			if (showexpr)
				sql.print_expr(std::cout << "SIMPLIFIED EXPRESSION: ", exprs, 2) << std::endl;
		}
	}
	if (optcolumn) {
		CartoSQL::ExprTable::const_ptr_t e(exprs->remove_unused_columns(&sql));
		if (e) {
			exprs = e;
			if (showexpr)
				sql.print_expr(std::cout << "OPTIMIZE UNUSED COLUMNS EXPRESSION: ", exprs, 2) << std::endl;
		}
	}
	if (optfilter) {
		CartoSQL::ExprTable::const_ptr_t e(exprs->push_filter_up(&sql, CartoSQL::Expr::const_ptr_t()));
		if (e) {
			exprs = e;
			if (showexpr)
				sql.print_expr(std::cout << "OPTIMIZE FILTER EXPRESSION: ", exprs, 2) << std::endl;
		}
	}
	if (optsort) {
		CartoSQL::ExprTable::const_ptr_t e(exprs->add_all_sort_columns_except("way"));
		if (e) {
			exprs = e;
			if (showexpr)
				sql.print_expr(std::cout << "SORT ALL COLUMNS EXPRESSION: ", exprs, 2) << std::endl;
		}
	}
	CalcError calc(std::cerr, &sql);
	calc.set_bbox(bbox);
	calc.set_scale_denominator(1);
	calc.set_pixel_width(1);
	calc.set_pixel_height(1);
	CartoSQL::Table tbl;
	Glib::TimeVal tv, tv1;
	tv1.assign_current_time();
	if (!exprs->calculate(calc, tbl)) {
		std::cerr << "CALCULATE ERROR" << std::endl;
	} else {
		tv.assign_current_time();
		tv -= tv1;
		std::cout << "RESULT: (" << tv.as_double() << "s)" << std::endl;
		CartoSQL::print_table(std::cout, tbl, exprs, 2, " | ", flags);
	}
}

int main(int argc, char *argv[])
{
	enum class mode_t {
		carto,
		cartodumpcss,
		cartodumplayers,
		cartodumpschema,
		cartoquerylayer,
		cartodumpqueries,
		sqlselect,
		sqlvalue,
		structsize
	};
	static struct option long_options[] = {
		{ "file", no_argument, 0, 'F' },
		{ "showexpr", no_argument, 0, 'E' },
		{ "bbox", required_argument, 0, 'B' },
		{ "pointdist", required_argument, 0, 'P' },
		{ "zoom", required_argument, 0, 'z' },
		{ "carto", required_argument, 0, 0x400 },
		{ "structsize", no_argument, 0, 0x401 },
		{ "noopt", no_argument, 0, 0x402 },
		{ "nocolumn", no_argument, 0, 0x403 },
		{ "nofilter", no_argument, 0, 0x404 },
		{ "nosort", no_argument, 0, 0x405 },
		{ "dumpcss", no_argument, 0, 0x500 + static_cast<int>(mode_t::cartodumpcss) },
		{ "dumplayers", no_argument, 0, 0x500 + static_cast<int>(mode_t::cartodumplayers) },
		{ "dumpschema", no_argument, 0, 0x500 + static_cast<int>(mode_t::cartodumpschema) },
		{ "querylayer", required_argument, 0, 0x500 + static_cast<int>(mode_t::cartoquerylayer) },
		{ "dumpqueries", required_argument, 0, 0x500 + static_cast<int>(mode_t::cartodumpqueries) },
		{ "printarray", no_argument, 0, 0x600 + static_cast<int>(CartoSQL::Value::sqlstringflags_t::array) },
		{ "printtags", no_argument, 0, 0x600 + static_cast<int>(CartoSQL::Value::sqlstringflags_t::tags) },
		{ "printgeometry", no_argument, 0, 0x600 + static_cast<int>(CartoSQL::Value::sqlstringflags_t::geometry) },
		{ 0, 0, 0, 0 }
	};
	mode_t mode(mode_t::carto);
	int c, err(0);
	std::string cartoproj(PACKAGE_DATA_DIR "/cartocss/project.mml");
	bool filemode(false), showexpr(false), optgeneral(true), optcolumn(true), optfilter(true), optsort(true);
	Rect bbox;
	std::string optparam;
	CartoSQL::Value::sqlstringflags_t sqlflags(CartoSQL::Value::sqlstringflags_t::none);
	Carto::zoom_t zoom(12);
	odb.set_path(PACKAGE_DATA_DIR "/osm.odb");
	sdb.set_path(PACKAGE_DATA_DIR "/osm.sdb");
	odb.open();
	sdb.open();
	while ((c = getopt_long(argc, argv, "SVEFB:P:z:", long_options, 0)) != EOF) {
		switch (c) {
		case 'S':
			mode = mode_t::sqlselect;
			break;

		case 'V':
			mode = mode_t::sqlvalue;
			break;

		case 'F':
			filemode = true;
			break;

		case 'E':
			showexpr = true;
			break;

		case 'B':
			if (optarg) {
				char *cp(strchr(optarg, ' '));
				if (cp) {
					*cp++ = 0;
					while (std::isspace(*cp))
						++cp;
				}
				{
					Point pt;
					unsigned int r(pt.set_str(optarg));
					if ((Point::setstr_lat | Point::setstr_lon) & ~r) {
						std::cerr << "Cannot parse coordinate " << optarg << std::endl;
						break;
					}
					bbox = Rect(pt, pt);
					if (r & Point::setstr_excess)
						std::cerr << "Warning: Excess characters in coordinate " << optarg << std::endl;
				}
				if (cp) {
					Point pt;
					unsigned int r(pt.set_str(cp));
					if ((Point::setstr_lat | Point::setstr_lon) & ~r) {
						std::cerr << "Cannot parse coordinate " << cp << std::endl;
						break;
					}
					bbox = bbox.add(pt);
					if (r & Point::setstr_excess)
						std::cerr << "Warning: Excess characters in coordinate " << cp << std::endl;
				} else {
					bbox = bbox.get_northeast().simple_box_nmi(10);
				}
				if (!false)
					std::cout << "bbox: " << bbox.get_southwest().get_lat_str2() << ' '
						  << bbox.get_southwest().get_lon_str2() << ' ' << bbox.get_northeast().get_lat_str2() << ' '
						  << bbox.get_northeast().get_lon_str2() << std::endl;
			}
			break;

		case 'P':
			if (optarg) {
				char *cp(strchr(optarg, ' '));
				if (cp) {
					*cp++ = 0;
					while (std::isspace(*cp))
						++cp;
				}
				{
					Point pt;
					unsigned int r(pt.set_str(optarg));
					if (((Point::setstr_lat | Point::setstr_lon) & ~r) || (r & Point::setstr_excess)) {
						AirportsDb arptdb(Engine::get_default_aux_dir(), AirportsDb::read_only);
						AirportsDb::elementvector_t ev(arptdb.find_by_icao(optarg, 0, AirportsDb::comp_exact, 0, AirportsDb::element_t::subtables_none));
						if (ev.size() == 1 && !ev.front().get_coord().is_invalid()) {
							pt = ev.front().get_coord();
						} else {
							std::cerr << "Cannot parse coordinate/airport " << optarg << std::endl;
							break;
						}
					}
					bbox = Rect(pt, pt);
				}
				{
					double radius(10);
					if (cp)
						radius = strtod(cp, 0);
					bbox = bbox.get_northeast().simple_box_nmi(radius);
				}
				if (!false)
					std::cout << "bbox: " << bbox.get_southwest().get_lat_str2() << ' '
						  << bbox.get_southwest().get_lon_str2() << ' ' << bbox.get_northeast().get_lat_str2() << ' '
						  << bbox.get_northeast().get_lon_str2() << std::endl;
			}
			break;

		case 'z':
			if (optarg)
				zoom = strtoul(optarg, nullptr, 0);
			break;

		case 0x400:
			if (optarg)
				cartoproj = optarg;
			break;

		case 0x401:
			mode = mode_t::structsize;
			break;

		case 0x402:
			optgeneral = false;
			break;

		case 0x403:
			optcolumn = false;
			break;

		case 0x404:
			optfilter = false;
			break;

		case 0x405:
			optsort = false;
			break;

		case 0x500 + static_cast<int>(mode_t::carto):
		case 0x500 + static_cast<int>(mode_t::cartodumpcss):
		case 0x500 + static_cast<int>(mode_t::cartodumplayers):
		case 0x500 + static_cast<int>(mode_t::cartodumpschema):
		case 0x500 + static_cast<int>(mode_t::cartoquerylayer):
		case 0x500 + static_cast<int>(mode_t::cartodumpqueries):
		case 0x500 + static_cast<int>(mode_t::sqlselect):
		case 0x500 + static_cast<int>(mode_t::sqlvalue):
		case 0x500 + static_cast<int>(mode_t::structsize):
			mode = static_cast<mode_t>(c - 0x500);
			if (optarg)
				optparam = optarg;
			break;

		case 0x600 + static_cast<int>(CartoSQL::Value::sqlstringflags_t::array):
		case 0x600 + static_cast<int>(CartoSQL::Value::sqlstringflags_t::tags):
		case 0x600 + static_cast<int>(CartoSQL::Value::sqlstringflags_t::geometry):
			sqlflags |= static_cast<CartoSQL::Value::sqlstringflags_t>(c - 0x600);
			break;

		default:
			++err;
			break;
		}
	}
	if (err) {
		std::cerr << "usage: cartoquery [-S] [-V] [-E] [-F]" << std::endl;
		return EX_USAGE;
	}

	try {
		switch (mode) {
		default:
		case mode_t::carto:
		case mode_t::cartodumpcss:
		case mode_t::cartodumplayers:
		case mode_t::cartodumpschema:
		case mode_t::cartoquerylayer:
		case mode_t::cartodumpqueries:
		{
			Carto carto;
			carto.set_osmdb(&odb);
			carto.set_osmstaticdb(&sdb);
			try {
				carto.parse_style(Glib::build_filename(Glib::path_get_dirname(cartoproj), "openstreetmap-carto.style"));
				carto.parse_project(cartoproj, &std::cerr);
			} catch (const Carto::SQLError& e) {
				{
					std::ofstream ofs(Glib::build_filename(Glib::get_tmp_dir(), "test." + e.get_layerid() + ".sql"));
					ofs << e.get_sqlexpr();
				}
				throw;
			}
			switch (mode) {
			case mode_t::cartodumpcss:
				carto.get_css().get_mapparameters().print_mss(std::cout << "MAP PARAMETERS:" << std::endl, 2);
				if (carto.get_css().get_statements())
					carto.get_css().get_statements()->print(std::cout << "CSS STATEMENTS:" << std::endl, 2);
				return EX_OK;

			case mode_t::cartodumplayers:
			{
				std::cout << "MAP:" << std::endl
					  << "  Zoom: " << carto.get_minzoom() << "..." << carto.get_maxzoom() << std::endl
					  << "  SRS: " << carto.get_srs() << std::endl
					  << "LAYERS:" << std::endl;
				const Carto::Layers& layers(carto.get_layers());
				for (const Carto::Layer& layer : layers) {
					std::cout << "  Layer: " << layer.get_id() << std::endl
						  << "    Zoom: " << layer.get_minzoom() << "..." << layer.get_maxzoom() << std::endl
						  << "    Geometry: " << layer.get_geometry() << std::endl;
					switch (layer.get_layertype()) {
					case Carto::Layer::layertype_t::osm:
						if (layer.get_sqlexpr())
							layer.get_sqlexpr()->print(std::cout << "    SQL: ", 4, CartoSQL::PrintContextSQL(&carto.get_sql()));
						else
							std::cout << "    SQL: null";
						if (layer.is_save())
							std::cout << " (save)";
						std::cout << std::endl;
						if (layer.get_sqlexpr()) {
							for (const CartoSQL::ExprTable::Column& col : layer.get_sqlexpr()->get_columns()) {
								if (col.get_name() != "?column?")
									continue;
								std::cout << "WARNING: DEFAULT COLUMN NAME(S)" << std::endl;
								break;
							}
						}
						break;

					case Carto::Layer::layertype_t::osmstatic:
						std::cout << "    Static: " << layer.get_staticlayer() << std::endl;
						break;

					case Carto::Layer::layertype_t::samelayer:
						std::cout << "    Same as Layer: " << layers[layer.get_samelayer()].get_id();
						if (!layer.is_save())
							std::cout << " (clear)";
						std::cout << std::endl;
						break;

					default:
						break;
					}
				}
				return EX_OK;
			}

			case mode_t::cartodumpschema:
				std::cout << "OSM SCHEMA:" << std::endl;
				{
					std::string::size_type width(0);
					for (const auto& x : carto.get_sql().get_osmschema())
						width = std::max(width, x.first.size());
					++width;
					std::ios_base::fmtflags flg(std::cout.setf(std::ios_base::left, std::ios_base::adjustfield));
					for (const auto& x : carto.get_sql().get_osmschema())
						std::cout << "  " << std::setw(width) << x.first << ": " << CartoSQL::Value::get_sqltypename(x.second) << std::endl;
					std::cout.flags(flg);
				}
				return EX_OK;

			case mode_t::cartoquerylayer:
			{
				const Carto::Layers& layers(carto.get_layers());
				bool work(false);
				for (const Carto::Layer& layer : layers) {
					if (layer.get_id() != optparam)
						continue;
					work = true;
					if (layer.get_layertype() != Carto::Layer::layertype_t::osm || !layer.get_sqlexpr()) {
						std::cerr << "Layer " << layer.get_id() << " is not an OSM SQL layer" << std::endl;
						continue;
					}
					print_expr(carto.get_sql(), layer.get_sqlexpr(), bbox, zoom, showexpr, optgeneral, optcolumn, optfilter, optsort, sqlflags);
				}
				if (work)
					return EX_OK;
				std::cerr << "Layer " << optparam << " not found" << std::endl;
				return EX_DATAERR;
			}

			case mode_t::cartodumpqueries:
			{
				if (optparam.empty() || !Glib::file_test(optparam, Glib::FILE_TEST_IS_DIR)) {
					std::cerr << "Parameter must be a directory" << std::endl;
					return EX_DATAERR;
				}
				typedef std::multimap<double,std::string> layerstat_t;
				layerstat_t layerstat;
				const Carto::Layers& layers(carto.get_layers());
				for (const Carto::Layer& layer : layers) {
					if (zoom < layer.get_minzoom() || zoom > layer.get_maxzoom())
						continue;
					if (layer.get_layertype() != Carto::Layer::layertype_t::osm || !layer.get_sqlexpr()) {
						std::cerr << "Layer " << layer.get_id() << " is not an OSM SQL layer, skipping" << std::endl;
						continue;
					}
					if (!layer.get_sqlexpr())
						continue;
					std::cout << "Layer " << layer.get_id() << ": ";
					std::cout.flush();
					if (showexpr) {
						std::ostringstream fn;
						fn << layer.get_id() << ".expr.txt";
						std::ofstream ofs(Glib::build_filename(optparam, fn.str()));
						carto.get_sql().print_expr(ofs << "EXPRESSION: ", layer.get_sqlexpr(), 2) << std::endl;
					}
					CartoSQL::ExprTable::const_ptr_t exprs;
					{
						CartoSQL::CalcOSMVariables calc(&carto.get_sql());
						calc.set_bbox(bbox);
						// https://github.com/openstreetmap/mapnik-stylesheets/blob/master/zoom-to-scale.txt
						calc.set_scale_denominator(ldexp(559082264.02800, -static_cast<double>(zoom)));
						calc.set_pixel_width(1);
						calc.set_pixel_height(1);
						exprs = layer.get_sqlexpr()->replace_variable(calc);
						if (!exprs) {
							exprs = layer.get_sqlexpr();
						} else if (showexpr) {
							std::ostringstream fn;
							fn << layer.get_id() << ".optexpr.txt";
							std::ofstream ofs(Glib::build_filename(optparam, fn.str()));
							carto.get_sql().print_expr(ofs << "REPLACED VARIABLES EXPRESSION: ", exprs, 2) << std::endl;
						}
					}
					if (optgeneral) {
						CartoSQL::ExprTable::const_ptr_t e(exprs->simplify(&carto.get_sql()));
						if (e) {
							exprs = e;
							if (showexpr) {
								std::ostringstream fn;
								fn << layer.get_id() << ".simplexpr.txt";
								std::ofstream ofs(Glib::build_filename(optparam, fn.str()));
								carto.get_sql().print_expr(ofs << "SIMPLIFIED EXPRESSION: ", exprs, 2) << std::endl;
							}
						}
					}
					if (optcolumn) {
						CartoSQL::ExprTable::const_ptr_t e(exprs->remove_unused_columns(&carto.get_sql()));
						if (e) {
							exprs = e;
							if (showexpr) {
								std::ostringstream fn;
								fn << layer.get_id() << ".columns.txt";
								std::ofstream ofs(Glib::build_filename(optparam, fn.str()));
								carto.get_sql().print_expr(ofs << "OPTIMIZE UNUSED COLUMNS EXPRESSION: ", exprs, 2) << std::endl;
							}
						}
					}
					if (optfilter) {
						CartoSQL::ExprTable::const_ptr_t e(exprs->push_filter_up(&carto.get_sql(), CartoSQL::Expr::const_ptr_t()));
						if (e) {
							exprs = e;
							if (showexpr) {
								std::ostringstream fn;
								fn << layer.get_id() << ".filter.txt";
								std::ofstream ofs(Glib::build_filename(optparam, fn.str()));
								carto.get_sql().print_expr(ofs << "OPTIMIZE FILTER EXPRESSION: ", exprs, 2) << std::endl;
							}
						}
					}
					if (optsort) {
						CartoSQL::ExprTable::const_ptr_t e(exprs->add_all_sort_columns_except("way"));
						if (e) {
							exprs = e;
							if (showexpr) {
								std::ostringstream fn;
								fn << layer.get_id() << ".sort.txt";
								std::ofstream ofs(Glib::build_filename(optparam, fn.str()));
								carto.get_sql().print_expr(ofs << "SORT ALL COLUMNS EXPRESSION: ", exprs, 2) << std::endl;
							}
						}
					}
					{
						std::ostringstream fn;
						fn << layer.get_id() << ".result.txt";
						std::ofstream ofs(Glib::build_filename(optparam, fn.str()));
						CalcError calc(ofs, &carto.get_sql());
						calc.set_bbox(bbox);
						calc.set_scale_denominator(1);
						calc.set_pixel_width(1);
						calc.set_pixel_height(1);
						CartoSQL::Table tbl;
						Glib::TimeVal tv, tv1;
						tv1.assign_current_time();
						if (!exprs->calculate(calc, tbl)) {
							std::cout << "error" << std::endl;
							ofs << "CALCULATE ERROR" << std::endl;
						} else {
							tv.assign_current_time();
							tv -= tv1;
							layerstat.insert(layerstat_t::value_type(tv.as_double(), layer.get_id()));
							std::cout << tv.as_double() << 's' << std::endl;
							ofs << "RESULT: (" << tv.as_double() << "s), " << tbl.size() << " rows" << std::endl;
							CartoSQL::print_table(ofs, tbl, exprs, 2, " | ", sqlflags);
						}
					}
				}
				std::cout << std::endl << "Layer Statistics:" << std::endl;
				for (const auto& x : layerstat)
					std::cout << "  " << x.second << ": " << x.first << 's' << std::endl;
				return EX_OK;
			}

			default:
			case mode_t::carto:
				break;
			}
			break;
		}

		case mode_t::sqlselect:
			for (; optind < argc; ++optind) {
				CartoSQL sql;
				sql.set_osmdb(&odb);
				sql.parse_style(Glib::build_filename(Glib::path_get_dirname(cartoproj), "openstreetmap-carto.style"), &std::cerr);
				if (filemode) {
					std::ifstream is(argv[optind]);
					CartoSQL::ExprTable::const_ptr_t expr(sql.parse_sql(is, &std::cerr));
					std::cout << "SUCCESS parsing file \"" << argv[optind] << "\"" << std::endl;
					print_expr(sql, expr, bbox, zoom, showexpr, optgeneral, optcolumn, optfilter, optsort, sqlflags);
					continue;
				}
		        	std::istringstream iss(argv[optind]);
				CartoSQL::ExprTable::const_ptr_t expr(sql.parse_sql(iss, &std::cerr));
				std::cout << "SUCCESS parsing \"" << argv[optind] << "\"" << std::endl;
				print_expr(sql, expr, bbox, zoom, showexpr, optgeneral, optcolumn, optfilter, optsort, sqlflags);
			}
			break;

		case mode_t::sqlvalue:
			for (; optind < argc; ++optind) {
				CartoSQL sql;
				sql.set_osmdb(&odb);
				sql.parse_style(Glib::build_filename(Glib::path_get_dirname(cartoproj), "openstreetmap-carto.style"), &std::cerr);
				if (filemode) {
					std::ifstream is(argv[optind]);
					CartoSQL::Expr::const_ptr_t expr(sql.parse_sql_value(is, &std::cerr));
					std::cout << "SUCCESS parsing file \"" << argv[optind] << "\"" << std::endl;
					print_expr(sql, expr, bbox, zoom, showexpr, optgeneral);
					continue;
				}
				std::istringstream iss(argv[optind]);
				CartoSQL::Expr::const_ptr_t expr(sql.parse_sql_value(iss, &std::cerr));
				std::cout << "SUCCESS parsing \"" << argv[optind] << "\"" << std::endl;
				print_expr(sql, expr, bbox, zoom, showexpr, optgeneral);
			}
			break;

		case mode_t::structsize:
			std::cout << "sizeof(Point) = " << sizeof(Point) << std::endl
				  << "sizeof(Rect) = " << sizeof(Rect) << std::endl
				  << "sizeof(MultiPoint) = " << sizeof(MultiPoint) << std::endl
				  << "sizeof(LineString) = " << sizeof(LineString) << std::endl
				  << "sizeof(MultiLineString) = " << sizeof(MultiLineString) << std::endl
				  << "sizeof(PolygonSimple) = " << sizeof(PolygonSimple) << std::endl
				  << "sizeof(PolygonHole) = " << sizeof(PolygonHole) << std::endl
				  << "sizeof(MultiPolygonHole) = " << sizeof(MultiPolygonHole) << std::endl
				  << "sizeof(Point3D) = " << sizeof(Point3D) << std::endl
				  << "sizeof(LineString3D) = " << sizeof(LineString3D) << std::endl
				  << "sizeof(PolygonSimple3D) = " << sizeof(PolygonSimple3D) << std::endl
				  << "sizeof(Geometry) = " << sizeof(Geometry) << std::endl
				  << "sizeof(GeometryCollection) = " << sizeof(GeometryCollection) << std::endl
				  << "sizeof(CartoCSS::Value) = " << sizeof(CartoCSS::Value) << std::endl
				  << "sizeof(CartoSQL::Value) = " << sizeof(CartoSQL::Value) << std::endl
				  << "sizeof(std::string) = " << sizeof(std::string) << std::endl;
			break;
		}
	} catch (const Carto::SQLError& e) {
		std::cerr << "Layer: " << e.get_layerid() << std::endl
			  << "Failure: " << e.what() << std::endl
			  << "SQL Expression: " << e.get_sqlexpr() << std::endl;
		return EX_DATAERR;
	} catch (const std::exception& e) {
		std::cerr << "Failure: " << e.what() << std::endl;
		return EX_DATAERR;
	}
	return EX_OK;
}
