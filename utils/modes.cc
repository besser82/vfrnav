/*
 * modes.cc:  Mode-S calculation utility
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "sysdeps.h"

#include <getopt.h>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <limits>
#include <sqlite3x.hpp>
#include <sqlite3.h>
#include <unistd.h>
#include <stdexcept>
#include <glibmm.h>
#include <giomm.h>
#include <stdio.h>

#include "modes.h"

/* ---------------------------------------------------------------------- */

int main(int argc, char *argv[])
{
	static struct option long_options[] = {
		{ "decode", required_argument, 0, 'd' },
		{ "encode", required_argument, 0, 'e' },
		{0, 0, 0, 0}
	};
	Glib::init();
	Gio::init();
        int c, err(0);

	while ((c = getopt_long(argc, argv, "d:e:", long_options, 0)) != EOF) {
		switch (c) {
		case 'd':
			if (optarg) {
				uint32_t hex(strtoul(optarg, 0, 16) & 0xFFFFFF);
				std::string cs(ModeSMessage::decode_registration(hex));
				std::string ctry(ModeSMessage::decode_registration_country(hex));
				std::ostringstream oss;
				oss << "Mode-S 24bit Code: 0x" << std::hex << std::setw(6) << std::setfill('0') << hex
				    << " Callsign " << cs << " Country " << ctry;
				std::cout << oss.str() << std::endl;
			}
			break;

		case 'e':
			if (optarg) {
				std::string cs(optarg);
				uint32_t hex(ModeSMessage::decode_registration(cs.begin(), cs.end()) & 0xFFFFFF);
				std::ostringstream oss;
				oss << "Mode-S 24bit Callsign " << cs << " Code: 0x"
				    << std::hex << std::setw(6) << std::setfill('0') << hex;
				std::cout << oss.str() << std::endl;
			}
			break;

		default:
			++err;
			break;
		}
	}
	if (err) {
		std::cerr << "usage: vfrnavmodes [-d <hex>] [-e <callsign>]" << std::endl;
		return EX_USAGE;
	}
	try {
	} catch (const Glib::Exception& ex) {
		std::cerr << "Glib exception: " << ex.what() << std::endl;
		return EX_DATAERR;
	} catch (const std::exception& ex) {
		std::cerr << "exception: " << ex.what() << std::endl;
		return EX_DATAERR;
	}
	return EX_OK;
}
