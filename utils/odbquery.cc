//
// C++ Implementation: odbquery
//
// Description: OSM database query utility
//
//
// Author: Thomas Sailer <t.sailer@alumni.ethz.ch>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "sysdeps.h"

#include <sstream>
#include <iostream>
#include <iomanip>

#include <pangomm/init.h>

#include "osmdb.h"
#include "osmsdb.h"
#include "osm.h"
#include "geomgdal.h"
#include "dbobj.h"
#include "engine.h"
#include "carto.h"
#include "cartorender.h"

#include "getopt.h"

#if defined(HAVE_GDAL)
#include "ogrsf_frmts.h"

class GDALOutput {
public:
	GDALOutput(void);
	~GDALOutput();

	bool open(OSMDB& db, const std::string& fn, const char *drvname = 0);
	void close(void);
	bool is_open(void) const { return !!m_ds; }
	unsigned int add_feature(const OSMDB::Object::const_ptr_t& p);

protected:
	class Field {
	public:
		Field(void);
		~Field();
		void close(void);
		void open(const std::string& name, OGRFieldType type = OFTString, int width = -1);
		bool is_open(void) const { return !!m_field; }
		const char *get_name(void) const { return m_field->GetNameRef(); }
		OGRFieldDefn *get_field(void) { return m_field; }

	protected:
		OGRFieldDefn *m_field;
	};

	typedef std::vector<Field> fields_t;
	fields_t m_fields;
	GDALDriver *m_driver;
	OGRSpatialReference *m_sref;
	GDALDataset *m_ds;
	OGRLayer *m_layer[4];
	OSMDB *m_db;

	void attr(OGRFeature *ftr, const OSMDB::Object::const_ptr_t& p);
	bool add_ftr(OGRFeature *ftr, unsigned int layidx, const OSMDB::Object::const_ptr_t& p);
	unsigned int layer_index(const OSMDB::Object::const_ptr_t& p);
};

std::string to_str(OGRErr err)
{
	switch (err) {
	case OGRERR_NOT_ENOUGH_DATA:
		return "Not enough data to deserialize";

	case OGRERR_NOT_ENOUGH_MEMORY:
		return "Not enough memory";

	case OGRERR_UNSUPPORTED_GEOMETRY_TYPE:
		return "Unsupported geometry type";

	case OGRERR_UNSUPPORTED_OPERATION:
		return "Unsupported operation";

	case OGRERR_CORRUPT_DATA:
		return "Corrupt data";

	case OGRERR_FAILURE:
		return "Failure";

	case OGRERR_UNSUPPORTED_SRS:
		return "Unsupported SRS";

	case OGRERR_INVALID_HANDLE:
		return "Invalid handle";

	case OGRERR_NON_EXISTING_FEATURE:
		return "Non existing feature";

	default:
	{
		std::ostringstream oss;
		oss << " Unknown error " << (int)err;
		return oss.str();
	}
	}
}

GDALOutput::Field::Field(void)
	: m_field(0)
{
}

GDALOutput::Field::~Field()
{
	close();
}

void GDALOutput::Field::close(void)
{
	if (!m_field)
		return;
	delete m_field;
	m_field = 0;
}
	
void GDALOutput::Field::open(const std::string& name, OGRFieldType type, int width)
{
	close();
	m_field = new OGRFieldDefn(name.c_str(), type);
	if (width >= 0)
		m_field->SetWidth(width);
}

GDALOutput::GDALOutput(void)
	: m_driver(0), m_sref(0), m_ds(0), m_db(0)
{
	for (unsigned int i(0), n(sizeof(m_layer)/sizeof(m_layer[0])); i < n; ++i)
		m_layer[i] = 0;
	GDALAllRegister();
	m_sref = new OGRSpatialReference();
	m_sref->importFromEPSG(4326);
}

GDALOutput::~GDALOutput()
{
	close();
	if (m_sref)
		m_sref->Release();
}

bool GDALOutput::open(OSMDB& db, const std::string& fn, const char *drvname)
{
	close();
	if (fn.empty())
		return false;
	if (!drvname)
		drvname = "ESRI Shapefile";
	{
		OSMDB::Statistics st(db.get_statistics());
		fields_t::size_type nrfields(1 + st.get_tagkeys());
		m_fields.resize(nrfields);
		m_fields[0].open("OSM_ID", OFTInteger64);
		for (fields_t::size_type i(1); i < nrfields; ++i) {
			const char *t(db.find_tagkey(static_cast<OSMDB::Object::tagkey_t>(i-1)));
			if (!t)
				continue;
			if (strchr(t, ':'))
				continue;
			m_fields[i].open(t, OFTString);
		}
	}
	m_driver = GetGDALDriverManager()->GetDriverByName(drvname);
	if (!m_driver)
		throw std::runtime_error(std::string("cannot get ") + drvname + " driver");
	m_ds = m_driver->Create(fn.c_str(), 0, 0, 0, GDT_Unknown, NULL);
	if (!m_ds)
		throw std::runtime_error(std::string("cannot create shapefile ") + fn);
	if (false)
		m_ds->SetProjection("+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs");
	m_db = &db;
	return true;
}

void GDALOutput::close(void)
{
	if (m_ds)
		GDALClose(m_ds);
	m_ds = 0;
	for (unsigned int i(0), n(sizeof(m_layer)/sizeof(m_layer[0])); i < n; ++i)
		m_layer[i] = 0;
	m_fields.clear();
	m_db = 0;
}

void GDALOutput::attr(OGRFeature *ftr, const OSMDB::Object::const_ptr_t& p)
{
	if (!p)
		return;
	if (!m_fields.empty() && m_fields[0].is_open())
		ftr->SetField(m_fields[0].get_name(), static_cast<GIntBig>(p->get_id()));
	if (!m_db)
		return;
	for (const auto& t : p->get_tags()) {
		fields_t::size_type i(static_cast<fields_t::size_type>(std::get<OSMDB::Object::tagkey_t>(t)) + 1);
		if (i >= m_fields.size())
			continue;
		if (!m_fields[i].is_open())
			continue;
		const char *cp(m_db->find_tagname(std::get<OSMDB::Object::tagname_t>(t)));
		if (!cp)
			continue;
		ftr->SetField(m_fields[i].get_name(), cp);
	}
}

unsigned int GDALOutput::layer_index(const OSMDB::Object::const_ptr_t& p)
{
	static const struct layer_defs {
		const char *name;
		OGRwkbGeometryType type;
	} layer_defs[] = {
		{ "miscellaneous", wkbUnknown },
		{ "points", wkbPoint },
		{ "lines", wkbLineString },
		{ "roads", wkbLineString },
		{ "areas", wkbMultiPolygon },
	};

	if (!m_ds)
		return 0;
	unsigned int layidx = 0;
	if (p)
		layidx = static_cast<unsigned int>(p->get_type()) + 1;	
	for (;;) {
		if (m_layer[layidx])
			break;
		m_layer[layidx] = m_ds->CreateLayer(layer_defs[layidx].name, m_sref, layer_defs[layidx].type, NULL);
		if (!m_layer[layidx]) {
			if (!layidx)
				throw std::runtime_error(std::string("cannot create layer ") + layer_defs[layidx].name);
			layidx = 0;
			continue;
		}
		for (fields_t::size_type i(0), n(m_fields.size()); i < n; ++i) {
			if (!m_fields[i].is_open())
				continue;
			OGRErr err(m_layer[layidx]->CreateField(m_fields[i].get_field()));
			if (err == OGRERR_NONE)
				continue;
			throw std::runtime_error(std::string("cannot create ") + m_fields[i].get_name() + " field: " + to_str(err));
		}
		break;
	}
	return layidx;
}

bool GDALOutput::add_ftr(OGRFeature *ftr, unsigned int layidx, const OSMDB::Object::const_ptr_t& p)
{
	if (!ftr || !m_layer[layidx])
		return false;
	if (false)
		std::cerr << "add_ftr: " << p->get_id() << ' ' << p->get_type() << " layer " << layidx << std::endl;
	OGRErr err(m_layer[layidx]->CreateFeature(ftr));
	OGRFeature::DestroyFeature(ftr);
	if (err == OGRERR_NONE)
		return true;
	std::ostringstream oss;
	oss << "cannot create feature " << to_str(err);
	if (p)
		oss << ' ' << p->get_id();
	if (false)
		throw std::runtime_error(oss.str());
	std::cerr << oss.str() << std::endl;
	return false;
}

unsigned int GDALOutput::add_feature(const OSMDB::Object::const_ptr_t& p)
{
	if (!m_ds || !p)
		return 0;
	unsigned int layidx(layer_index(p));
	OGRFeature *ftr(0);
	// Point
	{
		OSMDB::ObjPoint::const_ptr_t pp(boost::dynamic_pointer_cast<const OSMDB::ObjPoint>(p));
		if (pp) {
			ftr = OGRFeature::CreateFeature(m_layer[layidx]->GetLayerDefn());
			ftr->SetGeometryDirectly(to_ogr_geometry(pp->get_location()));
		}
	}
	// Line
	{
		OSMDB::ObjLine::const_ptr_t pp(boost::dynamic_pointer_cast<const OSMDB::ObjLine>(p));
		if (pp) {
			ftr = OGRFeature::CreateFeature(m_layer[layidx]->GetLayerDefn());
			ftr->SetGeometryDirectly(to_ogr_geometry(pp->get_line()));
		}
	}
	// Area
	{
		OSMDB::ObjArea::const_ptr_t pp(boost::dynamic_pointer_cast<const OSMDB::ObjArea>(p));
		if (pp) {
			ftr = OGRFeature::CreateFeature(m_layer[layidx]->GetLayerDefn());
			ftr->SetGeometryDirectly(to_ogr_geometry(pp->get_area()));
		}
	}
	attr(ftr, p);
        return add_ftr(ftr, layidx, p);
};

#else

class GDALOutput {
public:
	GDALOutput(void);
	~GDALOutput();

	bool open(OSMDB& db, const std::string& fn, const char *drvname = 0);
	void close(void);
	bool is_open(void) const { return false; }
	unsigned int add_feature(const OSMDB::Object::const_ptr_t& p);
};

GDALOutput::GDALOutput(void)
{
}

GDALOutput::~GDALOutput()
{
}

bool GDALOutput::open(OSMDB& db, const std::string& fn, const char *drvname)
{
	return false;
}

void GDALOutput::close(void)
{
}

unsigned int GDALOutput::add_feature(const OSMDB::Object::const_ptr_t& p)
{
	return 0;
}

#endif

enum class rendermode_t {
	default_,
	minimal,
	aeroonly
};

static void do_draw(const char *drawfilename, const char *cartocss, OSMDB& db, const Rect& bbox, rendermode_t drawrendermode,
		    CartoCSS::zoom_t zoom, OSMDB::Object::id_t debugobject, unsigned int cssdebuglevel, bool sqldraw)
{
	if (!drawfilename)
		return;
	if (cartocss) {
		OSMStaticDB sdb;
		sdb.set_path(PACKAGE_DATA_DIR "/osm.sdb");
		{
			std::string path(db.get_path());
			std::string path1(Glib::build_filename(Glib::path_get_dirname(path), "osm.sdb"));
			if (Glib::file_test(path1, Glib::FILE_TEST_EXISTS) && Glib::file_test(path1, Glib::FILE_TEST_IS_REGULAR))
				sdb.set_path(path1);
			if (path.size() > 4 && !path.compare(path.size()-4, 4, ".odb")) {
				path.resize(path.size() - 3);
				path += "sdb";
				if (Glib::file_test(path, Glib::FILE_TEST_EXISTS) && Glib::file_test(path, Glib::FILE_TEST_IS_REGULAR))
					sdb.set_path(path);
			}
		}
		sdb.open();
		if (!sdb.is_open())
			std::cerr << "Cannot open \"" << sdb.get_path() << '"' << std::endl;
		Carto carto;
		carto.set_osmdb(&db);
		carto.set_osmstaticdb(&sdb);
		carto.load_project(cartocss, &std::cerr);
		CartoRender render(carto);
		render.set_zoom(zoom);
		render.set_bbox(bbox);
		if (!sqldraw)
			render.load();
		render.set_debugobject(debugobject);
		render.set_cssdebuglevel(cssdebuglevel);
		Cairo::RefPtr<Cairo::PdfSurface> surface(Cairo::PdfSurface::create(drawfilename, render.get_width(), render.get_height()));
		Cairo::RefPtr<Cairo::Context> ctx(Cairo::Context::create(surface));
		if (sqldraw)
			render.drawsql(ctx, surface);
		else
			render.draw(ctx, surface);
		ctx->show_page();
		//surface->show_page();
		return;
	}
	static constexpr double cmtopt = 72 / 2.54;
	double width(18 * cmtopt), height(18 * cmtopt);
	OpenStreetMapDraw osmdraw(bbox);
	switch (drawrendermode) {
	case rendermode_t::default_:
	default:
		osmdraw.set_render_default();
		break;

	case rendermode_t::minimal:
		osmdraw.set_render_minimal();
		break;

	case rendermode_t::aeroonly:
		osmdraw.set_render_aeroonly();
		break;
	}
	osmdraw.load(db);
	{
		WaterelementsDb waterdb(Engine::get_default_aux_dir(), WaterelementsDb::read_only);
		osmdraw.load(waterdb);						
	}
	Cairo::RefPtr<Cairo::PdfSurface> surface(Cairo::PdfSurface::create(drawfilename, width, height));
	Cairo::RefPtr<Cairo::Context> ctx(Cairo::Context::create(surface));
	osmdraw.draw(ctx, width, height, surface);
	ctx->show_page();
	//surface->show_page();
}

int main(int argc, char *argv[])
{
        static struct option long_options[] = {
		{ "database", required_argument, 0, 'd' },
		{ "quiet", no_argument, 0, 'q' },
		{ "blob", no_argument, 0, 'b' },
		{ "rtree-stat", no_argument, 0, 'R' },
		{ "tags", no_argument, 0, 't' },
		{ "ids", no_argument, 0, 'i' },
		{ "index", no_argument, 0, 'I' },
		{ "pointdist", no_argument, 0, 'P' },
		{ "all", no_argument, 0, 'A' },
		{ "points", no_argument, 0, 'p' },
		{ "lines", no_argument, 0, 'l' },
		{ "roads", no_argument, 0, 'r' },
		{ "areas", no_argument, 0, 'a' },
		{ "skyvector", no_argument, 0, 0x400 },
		{ "coords", no_argument, 0, 0x401 },
		{ "notags", no_argument, 0, 0x402 },
		{ "render-default", no_argument, 0, 0x800 + static_cast<unsigned int>(rendermode_t::default_) },
		{ "render-minimal", no_argument, 0, 0x800 + static_cast<unsigned int>(rendermode_t::minimal) },
		{ "render-aero", no_argument, 0, 0x800 + static_cast<unsigned int>(rendermode_t::aeroonly) },
		{ "draw", required_argument, 0, 0x810 },
		{ "draw-pdf", required_argument, 0, 0x810 },
		{ "cartocss", optional_argument, 0, 0x820 },
		{ "zoom", required_argument, 0, 0x830 },
		{ "cssdebuglevel", required_argument, 0, 0x831 },
		{ "debugobject", required_argument, 0, 0x832 },
		{ "drawsql", no_argument, 0, 0x833 },
		{ "shape-file", required_argument, 0, 0x880 },
		{ "shape-esri", required_argument, 0, 0x880 },
		{ "shape-shp", required_argument, 0, 0x880 },
		{ "shape-gml", required_argument, 0, 0x881 },
		{ "shape-geopackage", required_argument, 0, 0x882 },
		{ "shape-gpkg", required_argument, 0, 0x882 },
		{ 0, 0, 0, 0 }
        };
	OSMDB db;
	enum class mode_t {
		area,
		id,
		index,
		all,
		pointdist,
		tags
	};
	mode_t mode(mode_t::area);
	OSMDB::typemask_t tmask(OSMDB::typemask_t::none);
	OSMDB::Object::printflags_t printflags(OSMDB::Object::printflags_t::default_);
	bool quiet(false), blob(false), rtreestat(false);
	const char *shpfilename(0), *shpdriver(0);
	const char *drawfilename(0);
	rendermode_t drawrendermode(rendermode_t::default_);
	const char *drawcartocss(0);
	bool drawcartosql(false);
	CartoCSS::zoom_t zoom(12);
	OSMDB::Object::id_t debugobject(CartoRender::debugobject_off);
	unsigned int cssdebuglevel(0U);
	int c, err(0);

	Glib::init();
	Glib::set_application_name("OSM Database Query");
	Pango::init();
	db.set_path(PACKAGE_DATA_DIR "/osm.odb");
	while ((c = getopt_long(argc, argv, "d:qbRtiIPAplra", long_options, 0)) != EOF) {
                switch (c) {
		case 'd':
			if (optarg)
				db.set_path(optarg);
			break;

		case 'q':
			quiet = true;
			break;

		case 'b':
			blob = true;
			break;

		case 'R':
			rtreestat = true;
			break;

		case 't':
			mode = mode_t::tags;
			break;

		case 'i':
			mode = mode_t::id;
			break;

		case 'I':
			mode = mode_t::index;
			break;

		case 'P':
			mode = mode_t::pointdist;
			break;

		case 'A':
			mode = mode_t::all;
			break;

		case 'p':
			tmask |= OSMDB::typemask_t::point;
			break;

		case 'l':
			tmask |= OSMDB::typemask_t::line;
			break;

		case 'r':
			tmask |= OSMDB::typemask_t::road;
			break;

		case 'a':
			tmask |= OSMDB::typemask_t::area;
			break;

		case 0x400:
			printflags |= OSMDB::Object::printflags_t::skyvector;
			break;

		case 0x401:
			printflags |= OSMDB::Object::printflags_t::coords;
			break;

		case 0x402:
			printflags &= ~OSMDB::Object::printflags_t::tags;
			break;

		case 0x800 + static_cast<unsigned int>(rendermode_t::default_):
		case 0x800 + static_cast<unsigned int>(rendermode_t::minimal):
		case 0x800 + static_cast<unsigned int>(rendermode_t::aeroonly):
			drawrendermode = static_cast<rendermode_t>(c - 0x800);
			break;

		case 0x810:
			drawfilename = optarg;
			break;

		case 0x820:
			drawcartocss = optarg ? optarg : PACKAGE_DATA_DIR "/cartocss/project.mml";
			break;

		case 0x830:
			if (optarg)
				zoom = strtoul(optarg, 0, 0);
			break;

		case 0x831:
			if (optarg)
				cssdebuglevel = strtoul(optarg, 0, 0);
			break;

		case 0x832:
			if (optarg)
				debugobject = strtoll(optarg, 0, 0);
			break;

		case 0x833:
			drawcartosql = true;
			break;

		case 0x880:
			shpfilename = optarg;
			shpdriver = 0;
			break;

		case 0x881:
			shpfilename = optarg;
			shpdriver = "GML";
			break;

		case 0x882:
			shpfilename = optarg;
			shpdriver = "GPKG";
			break;

		default:
			++err;
			break;
		}
	}
	if (err) {
		std::cerr << "usage: odbquery [-d <db>] [-q] [-b] [-t] [-i] [-P] [-A] [-p] [-l] [-r] [-a] <...>" << std::endl;
		return EX_USAGE;
	}
	db.open();
	if (!db.is_open()) {
		std::cerr << "Cannot open \"" << db.get_path() << '"' << std::endl;
		return EX_DATAERR;
	}
	{
		OSMDB::Statistics st(db.get_statistics(rtreestat));
		std::cout << db.get_path() << ": " << st.to_str() << std::endl;
	}
	if (mode == mode_t::tags) {
		for (OSMDB::Object::tagkey_t key = static_cast<OSMDB::Object::tagkey_t>(0); ;
		     key = static_cast<OSMDB::Object::tagkey_t>(static_cast<unsigned int>(key) + 1)) {
			const char *tagkey(db.find_tagkey(key));
			if (!tagkey)
				break;
			std::cout << "  " << static_cast<unsigned int>(key) << " = \"" << tagkey << '"' << std::endl;
		}
		return EX_OK;
	}
	if (tmask == OSMDB::typemask_t::none)
		tmask = OSMDB::typemask_t::all;
	GDALOutput shpoutput;
	if (shpfilename)
		shpoutput.open(db, shpfilename, shpdriver);
	if (optind >= argc)
		mode = mode_t::all;
	try {
		for (; optind < argc || mode == mode_t::all; optind += mode != mode_t::all, mode = mode == mode_t::all ? mode_t::id : mode) {
			OSMDB::objects_t objs;
			switch (mode) {
			case mode_t::id:
			default:
			{
				char *cp(0);
				OSMDB::Object::id_t id(strtol(argv[optind], &cp, 0));
				if (*cp) {
					std::cerr << "Cannot parse ID " << argv[optind] << std::endl;
					break;
				}
				objs = db.find(id, tmask);
				std::cout << "Find by ID " << id << ": " << objs.size() << " results" << std::endl;
				break;
			}

			case mode_t::index:
			{
				char *cp(0);
				uint32_t index(strtol(argv[optind], &cp, 0));
				if (*cp) {
					std::cerr << "Cannot parse index " << argv[optind] << std::endl;
					break;
				}
				if ((tmask & OSMDB::typemask_t::point) != OSMDB::typemask_t::none) {
					OSMDB::Object::const_ptr_t p(db.get(index, OSMDB::Object::type_t::point));
					if (p)
						objs.push_back(p);
				}
				if ((tmask & OSMDB::typemask_t::line) != OSMDB::typemask_t::none) {
					OSMDB::Object::const_ptr_t p(db.get(index, OSMDB::Object::type_t::line));
					if (p)
						objs.push_back(p);
				}
				if ((tmask & OSMDB::typemask_t::road) != OSMDB::typemask_t::none) {
					OSMDB::Object::const_ptr_t p(db.get(index, OSMDB::Object::type_t::road));
					if (p)
						objs.push_back(p);
				}
				if ((tmask & OSMDB::typemask_t::area) != OSMDB::typemask_t::none) {
					OSMDB::Object::const_ptr_t p(db.get(index, OSMDB::Object::type_t::area));
					if (p)
						objs.push_back(p);
				}
				std::cout << "Find by index " << index << ": " << objs.size() << " results" << std::endl;
				break;
			}

			case mode_t::all:
			{
				objs = db.find(tmask);
				std::cout << "Find all: " << objs.size() << " results" << std::endl;
				break;
			}

			case mode_t::area:
			{
				Rect bbox;
				{
					Point pt;
					unsigned int r(pt.set_str(argv[optind]));
					if ((Point::setstr_lat | Point::setstr_lon) & ~r) {
						std::cerr << "Cannot parse coordinate " << argv[optind] << std::endl;
						break;
					}
					bbox = Rect(pt, pt);
					if (r & Point::setstr_excess)
						std::cerr << "Warning: Excess characters in coordinate " << argv[optind] << std::endl;
				}
				++optind;
				if (optind < argc) {
					Point pt;
					unsigned int r(pt.set_str(argv[optind]));
					if ((Point::setstr_lat | Point::setstr_lon) & ~r) {
						std::cerr << "Cannot parse coordinate " << argv[optind] << std::endl;
						break;
					}
					bbox = bbox.add(pt);
					if (r & Point::setstr_excess)
						std::cerr << "Warning: Excess characters in coordinate " << argv[optind] << std::endl;
				} else {
					bbox = bbox.get_northeast().simple_box_nmi(10);
				}
				objs = db.find(bbox, tmask);
				do_draw(drawfilename, drawcartocss, db, bbox, drawrendermode, zoom, debugobject, cssdebuglevel, drawcartosql);
				drawfilename = 0;
				std::cout << "Find by bbox: " << bbox.get_southwest().get_lat_str2() << ' '
					  << bbox.get_southwest().get_lon_str2() << ' ' << bbox.get_northeast().get_lat_str2() << ' '
					  << bbox.get_northeast().get_lon_str2() << ": " << objs.size() << " results" << std::endl;
				break;
			}

			case mode_t::pointdist:
			{
				Rect bbox;
				{
					Point pt;
					unsigned int r(pt.set_str(argv[optind]));
					if (((Point::setstr_lat | Point::setstr_lon) & ~r) || (r & Point::setstr_excess)) {
						AirportsDb arptdb(Engine::get_default_aux_dir(), AirportsDb::read_only);
						AirportsDb::elementvector_t ev(arptdb.find_by_icao(argv[optind], 0, AirportsDb::comp_exact, 0, AirportsDb::element_t::subtables_none));
						if (ev.size() == 1 && !ev.front().get_coord().is_invalid()) {
							pt = ev.front().get_coord();
						} else {
							std::cerr << "Cannot parse coordinate/airport " << argv[optind] << std::endl;
							break;
						}
					}
					bbox = Rect(pt, pt);
				}
				++optind;
				{
					double radius(10);
					if (optind < argc)
						radius = strtod(argv[optind], 0);
					bbox = bbox.get_northeast().simple_box_nmi(radius);
				}
				objs = db.find(bbox, tmask);
				do_draw(drawfilename, drawcartocss, db, bbox, drawrendermode, zoom, debugobject, cssdebuglevel, drawcartosql);
				drawfilename = 0;
				std::cout << "Find by bbox: " << bbox.get_southwest().get_lat_str2() << ' '
					  << bbox.get_southwest().get_lon_str2() << ' ' << bbox.get_northeast().get_lat_str2() << ' '
					  << bbox.get_northeast().get_lon_str2() << ": " << objs.size() << " results" << std::endl;
				break;
			}
			}
			for (const auto& p : objs) {
				if (!p)
					continue;
				shpoutput.add_feature(p);
				if (quiet)
					continue;
				p->print(std::cout, db, printflags);
        			if (!blob)
					continue;
				std::ostringstream blobd;
				{
					HibernateWriteStream ar(blobd);
					p->save(ar);
				}
				std::ostringstream oss;
				oss << std::hex;
				for (const uint8_t *p0(reinterpret_cast<const uint8_t *>(&blobd.str()[0])), *p1(p0 + blobd.str().size()); p0 != p1; ++p0)
					oss << ' ' << std::setfill('0') << std::setw(2) << (unsigned int)*p0;
				std::cout << "  blob (" << blobd.str().size() << "):" << oss.str() << std::endl;
			}
		}
        } catch (const std::exception& ex) {
                std::cerr << "exception: " << ex.what() << std::endl;
                return EX_DATAERR;
        }
        return EX_OK;
}
