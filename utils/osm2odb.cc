//
// C++ Implementation: osm2odb
//
// Description: Convert OpenStreetMap files to proprietary database files
//
//
// Author: Thomas Sailer <t.sailer@alumni.ethz.ch>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "sysdeps.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <sstream>
#include <exception>
#include <glibmm.h>
#include <dirent.h>
#include <glib/gstdio.h>

#ifdef HAVE_SYSEXITS_H
#include <sysexits.h>
#else
#define EX_USAGE   64
#define EX_OK      0
#endif

#include <getopt.h>

#include "hibernate.h"
#include "geom.h"
#include "osmdb.h"
#include "osmstyle.h"

static void usage(void)
{
	std::cerr << "usage: osm2odb [-n] [-S] [-k] [-s <style>] [-o <osm.odb>] [<osm.pbf>]" << std::endl;
}

int main(int argc, char *argv[])
{
	try {
		std::string outputname("xx.odb");
		OSMDB::importflags_t flags(OSMDB::importflags_t::none);
		enum class mode_t {
			import,
			reindex,
			removeindex,
			statistics,
			printpointindex,
			printlineindex,
			printroadindex,
			printareaindex
		};
		mode_t mode(mode_t::import);
		// R* index parameters
		size_t max_elements = OSMDB::rtreedefaultmaxelperpage;
		size_t min_elements = std::numeric_limits<size_t>::max();
		size_t reinserted_elements = std::numeric_limits<size_t>::max();
		size_t overlap_cost_threshold = 32;
		OsmStyle style;
		{
			static struct option long_options[] = {
				{ "style",                  required_argument, 0, 's' },
				{ "output",                 required_argument, 0, 'o' },
				{ "nofilter",               no_argument,       0, 'n' },
				{ "singlepass",             no_argument,       0, 'S' },
				{ "keep",                   no_argument,       0, 'k' },
				{ "noindex",                no_argument,       0, 'I' },
				{ "reindex",                no_argument,       0, 0x400 + static_cast<unsigned int>(mode_t::reindex) },
				{ "removeindex",            no_argument,       0, 0x400 + static_cast<unsigned int>(mode_t::removeindex) },
				{ "statistics",             no_argument,       0, 0x400 + static_cast<unsigned int>(mode_t::statistics) },
				{ "printpointindex",        no_argument,       0, 0x400 + static_cast<unsigned int>(mode_t::printpointindex) },
				{ "printlineindex",         no_argument,       0, 0x400 + static_cast<unsigned int>(mode_t::printlineindex) },
				{ "printroadindex",         no_argument,       0, 0x400 + static_cast<unsigned int>(mode_t::printroadindex) },
				{ "printareaindex",         no_argument,       0, 0x400 + static_cast<unsigned int>(mode_t::printareaindex) },
				{ "max_elements",           required_argument, 0, 0x420 },
				{ "min_elements",           required_argument, 0, 0x421 },
				{ "reinserted_elements",    required_argument, 0, 0x422 },
				{ "overlap_cost_threshold", required_argument, 0, 0x423 },
				{0, 0, 0, 0}
			};
			int c, err(0);
			std::string stylef;

			while ((c = getopt_long(argc, argv, "ns:o:SkI", long_options, 0)) != EOF) {
				switch (c) {
				case 'n':
					flags |= OSMDB::importflags_t::nofilter;
					break;

				case 's':
					if (optarg)
						stylef = optarg;
					break;

				case 'o':
					if (optarg)
						outputname = optarg;
					break;

				case 'S':
		        		flags |= OSMDB::importflags_t::singlepass;
					break;

				case 'k':
		        		flags |= OSMDB::importflags_t::keep;
					break;

				case 'I':
					flags |= OSMDB::importflags_t::noindex;
					break;

				case 0x400 + static_cast<unsigned int>(mode_t::reindex):
				case 0x400 + static_cast<unsigned int>(mode_t::removeindex):
				case 0x400 + static_cast<unsigned int>(mode_t::statistics):
				case 0x400 + static_cast<unsigned int>(mode_t::printpointindex):
				case 0x400 + static_cast<unsigned int>(mode_t::printlineindex):
				case 0x400 + static_cast<unsigned int>(mode_t::printroadindex):
				case 0x400 + static_cast<unsigned int>(mode_t::printareaindex):
					mode = static_cast<mode_t>(c - 0x400);
					break;

				case 0x420:
					if (optarg)
						max_elements = strtoul(optarg, 0, 0);
					break;

				case 0x421:
					if (optarg)
						min_elements = strtoul(optarg, 0, 0);
					break;

				case 0x422:
					if (optarg)
						reinserted_elements = strtoul(optarg, 0, 0);
					break;

				case 0x423:
					if (optarg)
						overlap_cost_threshold = strtoul(optarg, 0, 0);
					break;

				default:
					++err;
					break;
				}
			}
			if (err) {
				usage();
				return EX_USAGE;
			}
			if (!stylef.empty())
				style.parse(stylef);
		}
		if (optind >= argc) {
			usage();
			return EX_USAGE;
		}
		// cache directory
		std::string tmpdir;
		switch (mode) {
		default:
		case mode_t::import:
		case mode_t::reindex:			
		{
			gchar *tmpdir1(g_dir_make_tmp("osm2odb-XXXXXX", NULL));
			tmpdir = tmpdir1;
			g_free(tmpdir1);
		}

		case mode_t::removeindex:
		case mode_t::statistics:
		case mode_t::printpointindex:
		case mode_t::printlineindex:
		case mode_t::printroadindex:
		case mode_t::printareaindex:
			break;
		}
		switch (mode) {
		case mode_t::import:
		default:
			OSMDB::import(style, tmpdir, outputname, argv[optind], flags);
			break;

		case mode_t::reindex:
			OSMDB::reindex(tmpdir, argv[optind], max_elements, min_elements, reinserted_elements, overlap_cost_threshold);
			break;

		case mode_t::removeindex:
			OSMDB::removeindex(argv[optind]);
			break;

		case mode_t::statistics:
		{
			static const char * const objnames[4] = {
				"Points:    ",
				"Lines:     ",
				"Roads:     ",
				"Areas:     "
			};
			OSMDB db(argv[optind]);
			db.open();
			OSMDB::Statistics st(db.get_statistics());
			std::cout << "Database:  " << argv[optind] << std::endl
				  << "Tag Keys:  " << st.get_tagkeys() << std::endl
				  << "Tag Names: " << st.get_tagnames() << std::endl;
			for (OSMDB::Object::type_t t(OSMDB::Object::type_t::point); t <= OSMDB::Object::type_t::area;
			     t = static_cast<OSMDB::Object::type_t>(static_cast<unsigned int>(t) + 1)) {
				std::cout << objnames[static_cast<unsigned int>(t)] << st.get_objects(t);			
				const OSMDB::Statistics::RTree& rt(st.get_rtree(t));
				if (rt.is_valid())
					std::cout << "  R-Tree: " << rt.to_str();
				std::cout << std::endl;
			}
			break;
		}

		case mode_t::printpointindex:
		{
			OSMDB db(argv[optind]);
			db.open();
			db.print_index(std::cout << "Database: " << argv[optind] << " Point Index" << std::endl, OSMDB::Object::type_t::point);
			break;
		}

		case mode_t::printlineindex:
		{
			OSMDB db(argv[optind]);
			db.open();
			db.print_index(std::cout << "Database: " << argv[optind] << " Line Index" << std::endl, OSMDB::Object::type_t::line);
			break;
		}

		case mode_t::printroadindex:
		{
			OSMDB db(argv[optind]);
			db.open();
			db.print_index(std::cout << "Database: " << argv[optind] << " Road Index" << std::endl, OSMDB::Object::type_t::road);
			break;
		}

		case mode_t::printareaindex:
		{
			OSMDB db(argv[optind]);
			db.open();
			db.print_index(std::cout << "Database: " << argv[optind] << " Area Index" << std::endl, OSMDB::Object::type_t::area);
			break;
		}

		}
		if (!tmpdir.empty() && (flags & OSMDB::importflags_t::keep) == OSMDB::importflags_t::none)
			g_remove(tmpdir.c_str());
		return EX_OK;
	} catch (const std::exception& e) {
		std::cerr << e.what() << std::endl;
		return EX_USAGE;
	} catch (...) {
		std::cerr << "Unknown Error" << std::endl;
		return EX_USAGE;
	}
	return EX_OK;
}
